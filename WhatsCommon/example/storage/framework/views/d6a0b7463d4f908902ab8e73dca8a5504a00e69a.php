<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly">
  <div class="container-fluid">
    <div class="connectionsBody">
      <div class="position-relative connectionFlip">
        <div class="leftSlidePan <?php echo e(Request::get('user') ? 'closePan' : ''); ?>">
          <a href="#" class="panelslideOpenButton <?php echo e(!Request::get('user') ? 'panelslideCloseButton' : ''); ?>"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
          <div class="connectionsDetailsInfoName connectionTitleResponsive">Connections</div>
          <div class="connectionsLeft leftPan windowHeight windowHeightLeft">
            <ul class="nav nav-tabs connectionsTab" role="tablist">
          <?php if($connections): ?>
            <?php 
              //$char = 'a';
              $connect_arr = [];
              foreach($connections as $i => $connection){
                $char = substr(strtolower($connection->firstName),0,1);
                if(isset($connect_arr[$char])){
                  array_push($connect_arr[$char],$connection);
                }else{
                  $connect_arr[$char][0] = $connection;
                }
              }

            ?>
            

            <?php if($connect_arr && !empty($connect_arr)): ?>

              <?php $i=0; ?>

              <?php $__currentLoopData = $connect_arr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $n_connections): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li class="alphabaticallyDetails">
                  <div class="alphabaticallyConnections"><?php echo e($key); ?></div>      
                </li>
                <?php if(!empty($n_connections)): ?>

                  <?php $__currentLoopData = $n_connections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $j => $connection): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li class="nav-item">
                    <a class="nav-link <?php echo e(((!Request::get('user') && $i==0) || Request::get('user')==$connection->id)? 'active' : ''); ?> openMidPanButton" data-toggle="tab" href="#connection<?php echo e($connection->id); ?>">
                      <div class="connectionsTabDetails">
                        <div class="connectionsAvtarImg">
                          <img src="<?php echo e($connection->profile_photo_path); ?>" class="img-fluid" alt="">
                        </div>
                        <div class="connectionsAvtarDetails">
                          <div class="connectionsAvtarName"><?php echo e($connection->firstName); ?> <?php echo e($connection->lastName); ?></div>
                          <div class="connectionsAvtarId"><?php echo e('@'); ?><?php echo e($connection->username); ?></div>
                          <div class="connectionsAvtarId"> <?php echo e($connection->event_type); ?> </div>
                        </div>
                        <div class="connectionsAvtarInfo">
                          <!-- <img src="new-design/img/connectionsCall.png" class="img-fluid connectionsCall" alt="">
                          <img src="new-design/img/connectionsMsg.png" class="img-fluid connectionsMsg" alt=""> -->
                          <div class="connectionsAvtarInfoCount"><?php echo e($connection->events_type_count); ?></div>
                        </div>
                      </div>  
                    </a>
                  </li>
                  <?php $i++; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <?php endif; ?>

              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <?php endif; ?>

              
            
          <?php endif; ?>
            </ul>
          </div>
        </div>
        <div class="midPan windowHeight windowHeightMid">
          <div class="connectionsRight">
            <div class="tab-content">
          <?php if($connections): ?>
            
              <?php $__currentLoopData = $connections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $connection): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <div id="connection<?php echo e($connection->id); ?>" class="tab-pane <?php echo e(((!Request::get('user') && $i==0) || Request::get('user')==$connection->id) ? 'active' : 'fade'); ?>">
                <div class="connectionsAllDetails">
                  <div class="connectionsAllDetailsTop">
                    <div class="connectionsDetailsAvtarImg">
                      <img src="<?php echo e($connection->profile_photo_path); ?>" class="img-fluid" alt="">
                    </div>

                    <div class="connectionsDetailsInfo">
                      <div class="connectionsDetailsInfoName"><?php echo e($connection->firstName); ?> <?php echo e($connection->lastName); ?></div>
                      <div class="connectionsDetailsInfoEvents myEvenntsAll">
                        <div class="myEvennts">
                          <span class="eventsNumbers"><?php echo e($connection->connection_count); ?></span> <span class="eventsText">Connections</span>
                        </div>
                        <div>
                          <span class="eventsNumbers"><?php echo e($connection->life_events_count); ?></span> <span class="eventsText">Life Events</span>
                        </div>
                        <div>
                          <span class="eventsNumbers"><?php echo e($connection->matched_count); ?></span> <span class="eventsText">Event Matches</span>
                        </div>
                      </div>
                      <div class="connectionsDetailsInfoId"><?php echo e('@'); ?><?php echo e($connection->username); ?></div>
                      <div class="connectionsDetailsPlace"><?php echo e($connection->city_name); ?> <?php echo e(($connection->city_name && $connection->province_name) ? ',' : ''); ?> <?php echo e($connection->province_name); ?> <?php echo e(($connection->country_name && $connection->province_name) ? ',' : ''); ?> <?php echo e($connection->country_name); ?></div>
                      <a class="btn btn-following" onclick="execute('api/follow-unfollow','user_id=<?php echo e($user_id); ?>&followed_user=<?php echo e($connection->id); ?>')"><?php echo e($connection->is_following==1 ? 'Following' : 'Follow'); ?></a>
                      <?php if($connection->quick_blox_id): ?>
                      <a class="btn btn-following btn-message" href="<?php echo e(url('messages?qb='.$connection->quick_blox_id)); ?>">Message</a>
                      <?php endif; ?>
                    </div>
                  </div>

                  <div class="connectionsAllDetailsBody">
                    <div class="connectionsDetailsTitle">Your Connections in Common</div>
                    
                  <?php if($connection->events_type_names): ?>

                    <?php
                      $events_type_names = explode(",",$connection->events_type_names);
                    ?>
                    <?php if($events_type_names): ?>
                    <div>
                      <?php $__currentLoopData = $events_type_names; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="connectionsDetailsBtn btn"><?php echo e($name); ?></div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <?php endif; ?>
                    
                      
                      
                    
                  <?php endif; ?>
                    
                  </div>
                </div>
              </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          <?php endif; ?>
              
            </div>    
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
$(document).ready(function(){
  $('.openMidPanButton').click(function(e){
    e.preventDefault();
    if(!$(".leftSlidePan").hasClass('closePan')){
      $(".leftSlidePan").addClass("closePan");
    }
    if(!$(".leftSlidePan .panelslideOpenButton").hasClass('panelslideCloseButton')){
      $(".leftSlidePan .panelslideOpenButton").addClass("panelslideCloseButton");
    }else{
      $(".leftSlidePan .panelslideOpenButton").removeClass("panelslideCloseButton");
    }
    /*$(".panelslideOpenButton").removeAttr('style');*/
    $(".leftSlidePan .panelslideOpenButton").css('z-index', 999);
  });
});
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/connections.blade.php ENDPATH**/ ?>