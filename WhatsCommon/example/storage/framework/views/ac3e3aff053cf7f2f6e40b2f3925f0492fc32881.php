<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly">
  <div class="container-fluid">
    <div class="settingBody">
      <div class="position-relative settingFlip">
          <div class="leftSlidePan closePan">
            <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
            <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
              <?php echo $__env->make('inc.life_event_left_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
          </div>
          <div class="midPan">
            <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
              <div class="row infoBodyRow">
                <div class="col-md-5 infoBodyCol">
                    <div class="innerHome">
                      <a href="<?php echo e(url('home')); ?>"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                    </div>
                  <div class="feedbackImg">
                    <div class="personalImgAll">
                      <img src="new-design/img/petsImg.png" class="img-fluid" alt="">
                      <div class="personalImgTitle">Pets</div>
                    </div>
                  </div>
                </div>
                <div class="col-md-7 infoBodyCol eventCol midHightControl">
                  <div class="feedbackRight windowHeight windowHeightMid" style="height: 445px;">

                      <form class="xhr_form" method="post" action="api/life-event-upload" id="life-event-upload">
                        <?php echo csrf_field(); ?>
                        <div class="eventColHeading">What </div> <img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                        <div class="whatForm">
                          <div class="whatReasonAll">
                            <div class="form-group datingFilterBtn timeOptionSet">
                              <div class="form-control timeOptionOnly activeTimeOption enterFor">Enter</div>
                              <div class="form-control timeOptionOnly searchFor">Search</div>
                              <input type="hidden" name="search_for" value="0" id="search_for" />
                              <input type="hidden" name="type_id" value="8">
                              <input type="hidden" name="user_id" value="<?php echo e(Session::get('userdata')['id']); ?>">
                            </div>
                            <div class="form-group">
                              <label for="">Pet Name</label>
                              <input type="text" class="form-control" id="" placeholder="Unknown" name="pet_name">
                            </div>
                            <label style="margin: 20px 0px 10px;">Pet Info</label>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Species</label>
                                  <select class="form-control formSpecies" id="species" name="pet_species">
                                    <option value="" hidden>Select</option>
                    <?php if($species): ?>
                    	<?php $__currentLoopData = $species; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    			<option option="<?php echo e($s->species_name); ?>" data-breeds="<?php echo e($s->breeds); ?>"><?php echo e($s->species_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Breed</label>
                                  <select class="form-control formBreed" id="breeds" name="pet_breed" title="Please select a species first">
                                    <option value="" hidden>Select</option>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Gender</label>
                                  <select class="form-control formGender" name="gender">
                                    <option value="" hidden>Select</option>
                    <?php if($genders): ?>
                    	<?php $__currentLoopData = $genders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gender): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    			<option option="<?php echo e($gender); ?>"><?php echo e($gender); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Temperment</label>
                                  <select class="form-control formTemperment" name="pet_temperment">
                                    <option value="" hidden>Select</option>
                    <?php if($pet_tempers): ?>
                    	<?php $__currentLoopData = $pet_tempers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    			<option option="<?php echo e($pt->pt_name); ?>"><?php echo e($pt->pt_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Color</label>
                                  <select class="form-control formColor" name="pet_color">
                                    <option value="" hidden>Select</option>
                    <?php if($pet_colors): ?>
                    	<?php $__currentLoopData = $pet_colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    			<option option="<?php echo e($pc->pc_name); ?>"><?php echo e($pc->pc_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Size</label>
                                  <select class="form-control formSize" name="pet_size">
                                    <option value="" hidden>Select</option>
                    <?php if($pet_sizes): ?>
                    	<?php $__currentLoopData = $pet_sizes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ps): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    			<option option="<?php echo e($ps->ps_name); ?>"><?php echo e($ps->ps_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">License Number</label>
                                  <input type="text" class="form-control formPlate" id="" placeholder="123abc" name="pet_license_number">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Collar</label>
                                  <select class="form-control formCollar" name="pet_collar">
                                    <option value="" hidden>Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                            </div>
                            <label style="margin: 10px 0px 16px;">Pet Info</label>
                            <div class="form-group topFormIcon">
                              <label for="">Veterinarian</label>
                              <input type="text" class="form-control formVeterinarian" id="" placeholder="Surname, First Name, MI" name="pet_veterinary">
                            </div>
                            <div class="form-group topFormIcon">
                              <label for="">Medical Condition</label>
                              <input type="text" class="form-control formMedical" id="" placeholder="Ex. Blind" name="pet_medical">
                            </div>
                            
                          </div>
                        </div>
                        <div class="eventColHeading">Where </div>
                        <div class="whatForm whereFrom">
                          <div class="form-group">
                            <label for="">Country</label>
                            <select class="form-control formCountry" name="country" id="country">
                              <option value="" hidden>Select</option>
            <?php if($countries): ?>
              <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($country->country_name); ?>" data-id="<?php echo e($country->_country_id); ?>"><?php echo e($country->country_name); ?></option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
                              
                            </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">State/County/Province</label>
                            <select class="form-control formState" name="state" id="province">
                              <option value="" hidden>Select</option>
                              
                            </select>
                            <img src="img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">City</label>
                            <select class="form-control formCity" name="city" id="city">
                              <option value="" hidden>Select</option>
                            </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">Street Address</label>
                            <input type="text" class="form-control formAddress" id="" placeholder="123 abc Ave." name="street">
                          </div>
                          <div class="form-group">
                            <label for="">ZIP</label>
                            <input type="text" class="form-control formZip" id="" placeholder="ZIP" name="zip">
                          </div>
                          
                        </div>
                        <div class="eventColHeading">Who, What, Where, When, Why </div>
                        <div class="whatForm whereFrom">
                          
                          	<div class="myAllEvents" id="keywords_div"></div>
	                        <input type="hidden" id="keywords" value="" name="event_keywords">
	                        <div class="input-group lifeEvent">
	                          	<input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
	                          	<div class="input-group-append">
	                            	<a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
	                           	</div>
	                        </div>
                          	<div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                        </div>
                        
                        <?php echo $__env->make('inc.life_event_img_upload', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                        
                      </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Pets</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Pets</label>
            <p>This one is near and dear to me! It's a two-way search tool; describing and
searching for your pet or other's pet. This is important, entering complete
details about all your pets could help bring them home safely. If a person finds
your pet and enters the details that match yours… PET FOUND! Same applies
with the other tool, you found a pet and you enter their matching detail and…
MATCH MADE!!</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>This can be the location where you lost your pet or found a pet.</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
</script>
<script type="text/javascript" src="js/image-uploader.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  /*$('.photoGallery').imageUploader();
  $('.upload-text').html('<img src="new-design/img/addGalleryImage.png" class="img-fluid photoGalleryImg" alt="">');*/
	$(".enterFor").click(function(){
        $(".enterFor").addClass("activeTimeOption");
        $(".searchFor").removeClass("activeTimeOption");
      $('#search_for').val(0);
    });
    $(".searchFor").click(function(){
        $(".searchFor").addClass("activeTimeOption");
        $(".enterFor").removeClass("activeTimeOption");
        $('#search_for').val(1);
    });
});
$(document).on('change','#species',function(){
	var breeds = $(this).find(':selected').data('breeds');
	/*console.log('breeds => ');
	console.log(breeds);*/
	var pet_breeds = '<option value="" hidden>Select </option>';
	if(breeds){

		$.each( breeds, function( index, breed ) {
			pet_breeds += '<option value="'+breed.breed_name+'">'+breed.breed_name+'</option>';
		});
	}

	$('#breeds').html(pet_breeds);
  $('#breeds').prop('title','Select Breed');
});
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="" hidden="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/life_events/pets.blade.php ENDPATH**/ ?>