<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<style>
.lifeDraftLeftTitle {
  margin-top: -30px !important;
}
</style>
<div class="innerBodyOnly innerBodyModify">
  <div class="container-fluid">
    <div class="settingBody">
      <div class="position-relative settingFlip">
          <div class="leftSlidePan closePan">
            <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
            <div class="lifeDraftLeftTitle"> <?php echo e($event_data->event_is_draft==0 ? 'LIFE EVENTS' : 'DRAFTS'); ?> </div>
            <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
              <ul class="list-unstyled mb-0 lifeEventMenu">
                <li class="lifeEventMenuLi lifeDraftLi <?php echo e($event_data->event_is_draft==0 ? 'lifeEventMenuLiActive' : ''); ?>">
                  <a href="<?php echo e(url('life-events')); ?>" class="lifeEventMenuLink">
                    <span class="lifeEventMenuText">Created Life Events</span>
                    <span class="lifeDraftCount"><?php echo e($event_count); ?></span>
                  </a>
                </li>
                <li class="lifeEventMenuLi lifeDraftLi <?php echo e($event_data->event_is_draft==1 ? 'lifeEventMenuLiActive' : ''); ?>">
                  <a href="<?php echo e(url('drafts')); ?>" class="lifeEventMenuLink lifeDraftMenuLink">
                    <span class="lifeEventMenuText">Drafts</span>
                    <span class="lifeDraftCount"><?php echo e($draft_count); ?></span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="midPan">
            <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                      <div class="row infoBodyRow">
                        <div class="col-md-5 infoBodyCol connectionsBody eventImgCol">
                          <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
                            <div class="innerHome">
                              <a href="<?php echo e(url('home')); ?>"><img src="img/home.png" class="img-fluid" alt=""></a>
                            </div>
                            <div class="feedbackImg">
                              <div class="personalImgAll">
                                <img src="new-design/img/userConnectImg.png" class="img-fluid" alt="">
                                <div class="personalImgTitle">Username Connect</div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7 connectionsBody infoBodyCol eventCol midHightControl">
                          <div class="connectionsBody feedbackRight windowHeight windowHeightMid">

                            <form class="xhr_form" method="post" action="api/save-life-event" id="save-life-event">
                              <?php echo csrf_field(); ?>
                              <div class="eventColHeading">What</div>
                                <div class="whatForm">
                                  <div class="form-group datingFilterBtn timeOptionSet">
                                    <div class="form-control timeOptionOnly enterFor <?php echo e($event_data->event_dating_search_for==0 ? 'activeTimeOption' : ''); ?>">Enter (you)</div>
                                    <div class="form-control timeOptionOnly searchFor <?php echo e($event_data->event_dating_search_for==1 ? 'activeTimeOption' : ''); ?>">Search (for)</div>
                                    <input type="hidden" name="search_for" value="<?php echo e($event_data->event_dating_search_for); ?>" id="search_for" />
                                    <input type="hidden" name="type_id" value="11">
                                    <input type="hidden" name="_event_id" value="<?php echo e($event_data->_event_id); ?>">
                                    <input type="hidden" name="user_id" value="<?php echo e(Session::get('userdata')['id']); ?>">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Game or Activity</label>
                                    <div class="selectCenterWithIcon" id="games_div">
                                      <select class="form-control forredline formGame " id="game_name" name="game_name">
                                        <option value="">Select Game or Activity</option>
                            <?php if($games): ?>
                              <?php $__currentLoopData = $games; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $game): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php
                                  $selected = $event_data->event_game==$game->game_name ? 'selected' : '';
                                ?>
                                      <option option="<?php echo e($game->game_name); ?>" data-id="<?php echo e($game->_game_id); ?>" data-game_teams="<?php echo e($game->game_teams); ?>" <?php echo e($selected); ?>><?php echo e($game->game_name); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                                      <option value="add">Add</option>
                                    </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="">Team/Group</label>
                                    <div class="selectCenterWithIcon" id="game_teams_div">
                                      <select class="form-control forredline formTeam " id="team_name" name="team_name">
                                        <option value="">Select Team/Group</option>
                            <?php if($game_teams): ?>
                              <?php $__currentLoopData = $game_teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <?php
                                $selected = ($event_data->event_gameteam==$team->team_name) ? 'selected' : '';
                              ?>
                              <option option="<?php echo e($team->team_name); ?>" <?php echo e($selected); ?> data-id="<?php echo e($team->_team_id); ?>"><?php echo e($team->team_name); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                                        <option value="add">Add</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="">Username</label>
                                      <input type="text" class="form-control forredline formUser selectCenterWithIcon2" name="username" value="<?php echo e($event_data->event_username); ?>">
                                  </div>
                                </div>
                                <div class="eventColHeading">When</div>
                                <div class="whatForm whereFrom">
                                  <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">From</label>
                              <select class="form-control forredline formMonth" id="" name="when_from_month">
                                <option value="">Month</option>
                                <option value="01" <?php echo e($when_data->when_from_month=='01'?'selected':''); ?>>January</option>
                                <option value="02" <?php echo e($when_data->when_from_month=='02'?'selected':''); ?>>February</option>
                                <option value="03" <?php echo e($when_data->when_from_month=='03'?'selected':''); ?>>March</option>
                                <option value="04" <?php echo e($when_data->when_from_month=='04'?'selected':''); ?>>April</option>
                                <option value="05" <?php echo e($when_data->when_from_month=='05'?'selected':''); ?>>May</option>
                                <option value="06" <?php echo e($when_data->when_from_month=='06'?'selected':''); ?>>June</option>
                                <option value="07" <?php echo e($when_data->when_from_month=='07'?'selected':''); ?>>July</option>
                                <option value="08" <?php echo e($when_data->when_from_month=='08'?'selected':''); ?>>August</option>
                                <option value="09" <?php echo e($when_data->when_from_month=='09'?'selected':''); ?>>September</option>
                                <option value="10" <?php echo e($when_data->when_from_month=='10'?'selected':''); ?>>October</option>
                                <option value="11" <?php echo e($when_data->when_from_month=='11'?'selected':''); ?>>November</option>
                                <option value="12" <?php echo e($when_data->when_from_month=='12'?'selected':''); ?>>December</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay">
                              <label for="number">Day</label>
                              <select class="form-control forredline" id="" name="when_from_day">
                                <option value="">DD</option>
                                <?php for($i=1;$i<=31;$i++): ?>
                                  <?php
                                    $j = sprintf('%02d', $i)
                                  ?>
                                 <option value="<?php echo e($j); ?>" <?php echo e($when_data->when_from_day==$j ? 'selected':''); ?> ><?php echo e($j); ?></option>
                                <?php endfor; ?>
                                
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayYear">
                              <label for="number">Year</label>
                              <select class="form-control forredline" id="" name="when_from_year">
                                <option value="">YYYY</option>
                              <?php for($i = date('Y'); $i >= 1900; $i--): ?>
                                <option value="<?php echo e($i); ?>" <?php echo e($when_data->when_from_year==$i ? 'selected':''); ?>><?php echo e($i); ?></option>
                              <?php endfor; ?>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                          </div>
                                </div>
                                <div class="eventColHeading">Who, What, Where, When, Why</div>
                                <div class="whatForm whereFrom">
                                  <div class="myAllEvents" id="keywords_div">
                        <?php if($event_data->event_keywords): ?>
                          <?php
                            $keywords = explode(',',$event_data->event_keywords);
                          ?>
                          <?php $__currentLoopData = $keywords; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $keyword): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <div class="addFieldItem keywords" id="<?php echo e($i); ?>" title="<?php echo e($keyword); ?>"><?php echo e($keyword); ?> <span class="removeField" onclick="removeKeyword('<?php echo e($i); ?>');"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                                  </div>
                                  <input type="hidden" id="keywords" value="<?php echo e($event_data->event_keywords); ?>" name="event_keywords">
                                  <div class="input-group lifeEvent">
                                    <input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
                                    <div class="input-group-append">
                                      <a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
                                     </div>
                                  </div>
                                  <div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                                </div>

                                <div class="whatForm whatFormBtnAll">
  <div class="whatFormBtnSE">
    <input type="hidden" id="is_draft" name="is_draft" value="<?php echo e($event_data->event_is_draft); ?>">
  <?php if($event_data->event_is_draft==1): ?>
    <button class="btn whatFormBtn w-100" onclick="$('#is_draft').val(0);">Publish</button>
  <?php else: ?>
    <button class="btn whatFormBtn btnUnpublish w-100" onclick="$('#is_draft').val(1);">Unpublish</button>
  <?php endif; ?>
  </div>

  <div class="btn-Edit-save btn-feedback btn-premium">
    <button class="btn btn-Edit btn-save">what'scommon <br><span>upload your life event</span></button>
  </div>
</div>
                              </form>
                          </div>
                        </div>
                      </div>
                    </div>
          </div>
      </div>
    </div>
  </div>
</div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).ready(function(){
  $("#dep_time_am").click(function(){
      $("#dep_time_am").addClass("activeTimeOption");
      $("#dep_time_pm").removeClass("activeTimeOption");
      $('#dep_time_ampm').val('AM');
  });
  $("#dep_time_pm").click(function(){
    $("#dep_time_pm").addClass("activeTimeOption");
    $("#dep_time_am").removeClass("activeTimeOption");
    $('#dep_time_ampm').val('PM');
  });

  $("#arr_time_am").click(function(){
      $("#arr_time_am").addClass("activeTimeOption");
      $("#arr_time_pm").removeClass("activeTimeOption");
      $('#arr_time_ampm').val('AM');
  });
  $("#arr_time_pm").click(function(){
    $("#arr_time_pm").addClass("activeTimeOption");
    $("#arr_time_am").removeClass("activeTimeOption");
    $('#arr_time_ampm').val('PM');
  });

  $(".enterFor").click(function(){
    $(".enterFor").addClass("activeTimeOption");
    $(".searchFor").removeClass("activeTimeOption");
    $('#search_for').val(0);
  });
  $(".searchFor").click(function(){
    $(".searchFor").addClass("activeTimeOption");
    $(".enterFor").removeClass("activeTimeOption");
    $('#search_for').val(1);
  });

});
$(document).on('change','#game_name',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '<?php echo e(Session::get("userdata")["id"]); ?>';
    open_modal('add-master','user_id='+user_id+'&db_name=games&column_name=game_name&status_column=game_is_active');
  }else{
    var game_teams = $(this).find(':selected').data('game_teams');
    /*console.log('ranks => ');
    console.log(ranks);*/
    var team_name = '<option value="">Select </option>';
    if(game_teams){

      $.each( game_teams, function( index, rank ) {
        team_name += '<option value="'+rank.team_name+'">'+rank.team_name+'</option>';
      });
    }
    team_name += '<option value="add">Add</option>';
    $('#team_name').html(team_name);
  }
  
});
$(document).on('change', '#team_name', function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '<?php echo e(Session::get("userdata")["id"]); ?>';
    var game_id = $('#game_name').find(':selected').data('id');
    open_modal('add-master','user_id='+user_id+'&db_name=game_teams&column_name=team_name&status_column=team_is_active&dependent_name=team_gameid&dependent_value='+game_id+'&is_dependent=1');
  }
});
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
function checkFieldError(){
  $(".forredline").each(function() {
    if($(this).val()==''){
      $(this).addClass('fieldError');
    }else{
      $(this).removeClass('fieldError');
    }
  });
}
$(document).ready(function(){
  checkFieldError();
});
$(document).on('change','.forredline',function(){
  checkFieldError();
});
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/life_events/edit/username_connect.blade.php ENDPATH**/ ?>