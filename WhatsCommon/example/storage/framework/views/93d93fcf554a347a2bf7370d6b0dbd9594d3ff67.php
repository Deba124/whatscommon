<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item "> 
      <a class="nav-link" href="<?php echo url('admin/dashboard') ?>">
        <img class="menu-icon" src="<?php echo e(url('/admin/assets\images\menu_icons\Icon\7.png')); ?>" alt="menu icon">
        <span class="menu-title">Dashboard</span>
      </a> 
    </li>
    <li class="nav-item ">
      <a class="nav-link" data-toggle="collapse" href="#master-layouts" aria-expanded="false" aria-controls="master-layouts"> 
        <img class="menu-icon" src="<?php echo e(url('/admin/assets\images\menu_icons\Icon\3.png')); ?>" alt="menu icon">
        <span class="menu-title">Master</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="master-layouts">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/after-schools') ?>"> After Schools</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/age-ranges') ?>"> Age Ranges</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/genders') ?>"> Genders</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/relations') ?>"> Relationships</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/marital-status') ?>"> Marital Status</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/countries') ?>"> Countries</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/provinces') ?>"> Provinces</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/cities') ?>"> Cities</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/home-styles') ?>"> Home Styles</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/home-types') ?>"> Home Types</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/vehicle-types') ?>"> Vehicle Types</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/vehicle-makers') ?>"> Vehicle Makers</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/vehicle-colors') ?>"> Vehicle Colors</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/industries') ?>"> Industries</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/categories') ?>"> Categories</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/donation-amounts') ?>"> Donation Amounts</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/military-branches') ?>"> Military Branches</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/military-ranks') ?>"> Military Ranks</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/military-bases') ?>"> Military Bases</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/military-units') ?>"> Military Units</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/military-mos') ?>"> Military Mos</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/military-mos-title') ?>"> Military Mos Title</a></li>
        </ul>
      </div>
    </li>

    <li class="nav-item"> 
      <a class="nav-link" href="<?php echo url('admin/pages') ?>">
        <img class="menu-icon" src="<?php echo e(url('/admin/assets\images\menu_icons\Icon\9.png')); ?>" alt="menu icon">
        <span class="menu-title">Pages</span>
      </a> 
    </li>


    <li class="nav-item "> 
      <a class="nav-link" href="<?php echo url('admin/users') ?>">
        <img class="menu-icon" src="<?php echo e(url('/admin/assets\images\menu_icons\Icon\15.png')); ?>" alt="menu icon">
        <span class="menu-title">Users</span>
      </a> 
    </li>

    <li class="nav-item "> 
      <a class="nav-link" href="<?php echo url('admin/testimonials') ?>">
        <img class="menu-icon" src="<?php echo e(url('/admin/assets\images\menu_icons\Icon\14.png')); ?>" alt="menu icon">
        <span class="menu-title">Testimonials</span>
      </a> 
    </li>

    <li class="nav-item ">
      <a class="nav-link" data-toggle="collapse" href="#information-layouts" aria-expanded="false" aria-controls="information-layouts"> 
        <img class="menu-icon" src="<?php echo e(url('/admin/assets\images\menu_icons\Icon\3.png')); ?>" alt="menu icon">
        <span class="menu-title">Information</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="information-layouts">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/feedbacks') ?>"> Feedbacks</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/contacts') ?>"> Contact Us</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/user-queries') ?>"> User Queries</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?php echo url('admin/donations') ?>"> Donations</a></li>
        </ul>
      </div>
    </li>
    <li class="nav-item"> 
      <a class="nav-link" href="<?php echo url('admin/news-letters') ?>">
        <img class="menu-icon" src="<?php echo e(url('/admin/assets\images\menu_icons\Icon\9.png')); ?>" alt="menu icon">
        <span class="menu-title">News Letters</span>
      </a> 
    </li>
    <li class="nav-item"> 
      <a class="nav-link" href="<?php echo url('admin/notifications') ?>">
        <img class="menu-icon" src="<?php echo e(url('/admin/assets\images\menu_icons\Icon\11.png')); ?>" alt="menu icon">
        <span class="menu-title">Notifications</span>
      </a> 
    </li>
  </ul>
</nav>
<!-- partial -->
<div class="main-panel"><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/inc/sidebar.blade.php ENDPATH**/ ?>