<?php if($users): ?>
<div class="profileForm bankForm">
	<ul class="linkAccountList">
	<?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	    <li style="border-bottom: 1px solid black;">
	        <span>
	            <img src="<?php echo e($user->profile_photo_path); ?>" class="img-fluid" alt="">
	            <?php echo e($user->firstName); ?> <?php echo e($user->lastName); ?>

	        </span>
	        <a class="unblockText" onclick="execute('api/block-user','user_id=<?php echo e($user_id); ?>&block_user_id=<?php echo e($user->id); ?>')">Block</a>
	    </li>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</ul>
</div>
<?php endif; ?><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/xhr/search_results.blade.php ENDPATH**/ ?>