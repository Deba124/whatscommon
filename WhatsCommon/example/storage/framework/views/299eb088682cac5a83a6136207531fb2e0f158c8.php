<?php
$rep = ' : All';
if(Request::get('from') && Request::get('to')){
  $rep = ' : '.date('d-m-Y', strtotime(Request::get('from'))).' - '.date('d-m-Y', strtotime(Request::get('to')));
}
?>
<?php $__env->startSection('title', 'Revenue Report'.$rep); ?>
<?php echo $__env->make('admin.inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('admin.inc.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="content-wrapper">
  <div class="card">
    <div class="card-body text-center">
      <h4 class="card-title">Revenue Report <?php echo e($rep); ?></h4>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label>From Date</label>
            <input type="text" readonly="" id="from_date" name="from_date" placeholder="DD-MM-YYYY" value="<?php echo e(Request::get('from') ? date('d-m-Y', strtotime(Request::get('from'))) : ''); ?>" class="form-control no_future_date" style="text-align: center;">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>To Date</label>
            <input type="text" readonly="" id="to_date" name="to_date" placeholder="DD-MM-YYYY" value="<?php echo e(Request::get('to') ? date('d-m-Y', strtotime(Request::get('to'))) : ''); ?>" class="form-control no_future_date" style="text-align: center;">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <a class="btn btn-secondary" style="margin-top: 25px;" href="javascript:void();" onclick="pageReload();"><i class="fa fa-search text-success"></i> Search</a>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <a class="btn btn-secondary" style="margin-top: 25px;" href="javascript:void();" onclick="clearSearch();"><i class="fa fa-eraser text-success"></i> Clear</a>
          </div>
        </div>
      </div>
      <div class="row">
  
        <div class="col-12">
          <table id="revenue_report-list" class="table table-condensed table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Month</th>
                      <th scope="col">No. of Transactions</th>
                      <th scope="col">Donation</th>
                      <th scope="col">Subscription fees</th>
                      <th scope="col">Total Amount</th>
                    </tr>
                  </thead>
      <?php if($revenue_arr): ?>
                  <tbody>

        <?php
          $monthly_donation = 0;
          $monthly_plans = 0;
          $total_count = 0;
          $net_total = 0;
          $final_total = 0;
        ?>
        <?php $__currentLoopData = $revenue_arr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $rev): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <?php 
            $monthly_donation += $rev['monthly_donation'];
            $monthly_plans += $rev['monthly_plans'];
            $total_count += $rev['total_count'];
            $net_total = $rev['monthly_donation'] + $rev['monthly_plans'];
            $final_total += $net_total;
          ?>
                  <tr>
                    <td><?php echo e($i+1); ?></td>
                    <td><?php echo e(date('Y-m',strtotime($rev['date']))); ?></td>
                    <td><?php echo e($rev['total_count']); ?></td>
                    <td><?php echo e(sprintf("%.2f",$rev['monthly_donation'])); ?></td>
                    <td><?php echo e(sprintf("%.2f",$rev['monthly_plans'])); ?></td>
                    <td><?php echo e(sprintf("%.2f",$net_total)); ?></td>
                  </tr>
          
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  

                  </tbody>
                  <tfoot>
                    <tr>
                    <td>#</td>
                    <td><b>Total</b></td>
                    <td><?php echo e($total_count); ?></td>
                    <td><?php echo e(sprintf("%.2f",$monthly_donation)); ?></td>
                    <td><?php echo e(sprintf("%.2f",$monthly_plans)); ?></td>
                    <td><?php echo e(sprintf("%.2f",$final_total)); ?></td>
                  </tr>
                  </tfoot>
      <?php endif; ?>
                </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $__env->make('admin.inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!-- content-wrapper ends -->
<script type="text/javascript">
$(document).ready(function() {  

  dataTable = $('#revenue_report-list').DataTable( {
    dom: 'lBrtip',
    responsive: true,
    ordering : false,
    /*lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
    pageLength: 10,*/
    paging:false,
    buttons: [
      {
          extend: 'print',
          footer: true,
          text: '<button class="btn btn-primary btn-sm" ><i class="fa fa-print fa-x5"> Print</i></button>',
          autoPrint: false,
          exportOptions: {
              columns: [ 0,1,2,3,4,5 ]
          }
      },
      {
          extend: 'excelHtml5',
          footer: true,
          text: '<button class="btn btn-success btn-sm"><i class="fa fa-file-excel-o fa-x5"> Excel</i></button>',
          exportOptions: {
              columns: [ 0,1,2,3,4,5 ]
          }
      },
      {
          extend: 'pdfHtml5',
          footer: true,
          text: '<button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o fa-x5"> PDF</i></button>',
          exportOptions: {
              columns:[ 0,1,2,3,4,5 ]
          }
      }
    ],
    "processing": true,
    "serverSide": false,
  });
}); 
function pageReload(){
  /*var event_type = $('#event_type').val();
  var time = $('#time').val();*/
  var from = $('#from_date').val();
  var to = $('#to_date').val();
  var page_url = site_url('admin/reports?from='+from+'&to='+to);
  $(location).attr("href", page_url);
}
function clearSearch(){
  $('#from_date').val('');
  $('#to_date').val('');
  pageReload();
}
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/reports.blade.php ENDPATH**/ ?>