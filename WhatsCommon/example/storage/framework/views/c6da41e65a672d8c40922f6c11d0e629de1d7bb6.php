 <!-- Modal starts -->       
<div class="modal fade" id="create-dialog-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" style="margin-left: 28%;">Select Connection</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal xhr_form" id="create-dialog" action="messages/create-dialog">
          <?php echo csrf_field(); ?>
          <div class="form-group whatForm">
            <!-- <label>Select User<span class="text-danger">*</span></label> -->
            <select class="form-control" name="reciever_id" style="color: #cbc6c6;border-radius: 20px;border:1px solid #f0f0f0;background-color:#fff !important; ">
              <option value="" hidden="">Select User</option>
      <?php if($connections): ?>
        <?php $__currentLoopData = $connections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $connection): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($connection->quick_blox_id); ?>"><?php echo e($connection->firstName); ?> <?php echo e($connection->lastName); ?></option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      <?php endif; ?>
            </select>
            <p class="text-danger" id="reciever_id_error"></p>
          </div>
          
          <div class="form-group whatForm text-center">
            <button type="submit" class="btn btn-success" style="width: 125px;"> Chat</button>
            <button type="button" class="btn btn-info" data-dismiss="modal" style="width: 125px;margin-left: 15px;">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends --><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/xhr/create_dialog_modal.blade.php ENDPATH**/ ?>