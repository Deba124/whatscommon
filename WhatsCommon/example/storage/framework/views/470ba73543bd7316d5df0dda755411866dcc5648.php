 <!-- Modal starts -->       
<div class="modal fade" id="edit-game-team-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content" style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Update Team/Group</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="edit-game-team">
          <div class="form-group" id="submit_status"></div>
          <?php echo csrf_field(); ?>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <input type="hidden" name="_team_id" value="<?php echo e($team_data->_team_id); ?>">
                <label>Game/Activity <span class="text-danger">*</span></label>
                <select class="form-control" name="team_gameid">
                  <option value="" hidden="">Select Game/Activity</option>
            <?php if($games): ?>
              <?php $__currentLoopData = $games; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $game): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php
                  $selected = $game->_game_id==$team_data->team_gameid ? 'selected' : '';
                ?>
                  <option value="<?php echo e($game->_game_id); ?>" <?php echo e($selected); ?>><?php echo e($game->game_name); ?></option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>

                </select>
                <p class="text-danger" id="game_name_error"></p>
              </div>
              <div class="form-group">
                <label>Team/Group Name <span class="text-danger">*</span></label>
                <input type="text" name="team_name" class="form-control" value="<?php echo e($team_data->team_name); ?>">
                <p class="text-danger" id="team_name_error"></p>
              </div>
            </div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends --><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/xhr/edit_game_team_modal.blade.php ENDPATH**/ ?>