 <!-- Modal starts -->       
<div class="modal fade" id="edit-donation-amount-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Update Donation Amount</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="edit-donation-amount">
          <div class="form-group" id="submit_status"></div>
          <?php echo csrf_field(); ?>
          <input type="hidden" name="_da_id" value="<?php echo e($donation_data->_da_id); ?>">
          <div class="form-group">
            <label>Currency <span class="text-danger">*</span></label>
            <select class="form-control" name="da_currency">
              <option value="" hidden="">Select Currency</option>
  <?php if($currencies): ?>
    <?php $__currentLoopData = $currencies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $currency): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($currency->_currency_id); ?>" <?php echo e($currency->_currency_id==$donation_data->da_currency ? 'selected' : ''); ?> ><?php echo e($currency->currency_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
            </select>
            <p class="text-danger" id="da_currency_error"></p>
          </div>
          <div class="form-group">
            <label>Amount <span class="text-danger">*</span></label>
            <input type="text" name="da_amount" class="form-control numeric" value="<?php echo e($donation_data->da_amount); ?>">
            <p class="text-danger" id="da_amount_error"></p>
          </div>
          
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends --><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/xhr/edit_donation_amount_modal.blade.php ENDPATH**/ ?>