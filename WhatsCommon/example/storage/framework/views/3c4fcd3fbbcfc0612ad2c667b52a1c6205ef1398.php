 <!-- Modal starts -->       
<div class="modal fade" id="payment-failure-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document" style="margin-top: 10%;">
    <div class="modal-content" style="border-radius: 20px;padding: 0px 30px;">
      <div class="modal-header" style="background: #fff;border: none;padding-top: 20px;">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          
          <img src="<?php echo e(url('new-design/img/close-blue_ic.png')); ?>" style="position: absolute;top: -15px;right: -13px;">
        </button>
      </div>
      <div class="modal-body" style="margin-top: -35px;">
        <div class="form-group text-center">
          <img src="<?php echo e(url('new-design/img/failure_ic.png')); ?>" class="img-fluid" >
        </div>
        <div class="form-group text-center" style="margin-top: -25px;">
          <span style="color: #000;font-size: 35px;font-weight: 600;margin-left: 10px;">Your Payment Failed!</span>
          <p style="font-size: 18px;">Don't worry, you can try again</p>
        </div>
        <div class="form-group text-center" >
          <button class="btn btn-Edit btn-Plan btn-block" style="height: 45px;border-radius: 10px;background: #0070ba;width: 100%;font-weight: 500;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color: #fff;text-transform: uppercase;" data-dismiss="modal" aria-label="Close">Ok</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends --><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/xhr/payment_failure_modal.blade.php ENDPATH**/ ?>