 <!-- Modal starts -->       
<div class="modal fade" id="show-image-from-url-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document" style="margin-top: 10%;">
    <div class="modal-content" style="border-radius: 20px;padding: 0px 30px;">
      <div class="modal-header" style="background: #fff;border: none;padding-top: 20px;">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          
          <img src="<?php echo e(url('new-design/img/close-blue_ic.png')); ?>" style="position: absolute;top: -15px;right: -13px;">
        </button>
      </div>
      <div class="modal-body" style="margin-top: -35px;">
        <div class="form-group text-center">
          <img src="<?php echo e($image_link); ?>" class="img-fluid" >
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends --><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/xhr/show_image_from_url_modal.blade.php ENDPATH**/ ?>