<?php $__env->startSection('title', 'Users'); ?>
<?php echo $__env->make('admin.inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('admin.inc.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<style type="text/css">
/*.table td, .jsgrid .jsgrid-table td, .table th, .jsgrid .jsgrid-table th{
  line-height: 1.5;
}*/
</style>
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Users</h4>
      <div class="row" style="margin-bottom: 15px;">
        <div class="col-3"></div>
        <div class="col-3">
          <select class="form-control" id="country_id" onchange="pageReload();">
            <option value="">Select Country</option>
    <?php if($countries): ?>
      <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($country->_country_id); ?>" <?php echo e($country->_country_id==Request::get('_country_id') ? 'selected' : ''); ?> ><?php echo e($country->country_name); ?></option>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
          </select>
        </div>
        <div class="col-3">
          <select class="form-control" id="province_id" onchange="pageReload();">
            <option value="">Select State</option>
    <?php if($provinces): ?>
      <?php $__currentLoopData = $provinces; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $province): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($province->_province_id); ?>" <?php echo e($province->_province_id==Request::get('_province_id') ? 'selected' : ''); ?>><?php echo e($province->province_name); ?></option>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
          </select>
        </div>
        <div class="col-3">
          <select class="form-control" id="city_id" onchange="pageReload();">
            <option value="">Select City</option>
    <?php if($cities): ?>
      <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($city->_city_id); ?>" <?php echo e($city->_city_id==Request::get('_city_id') ? 'selected' : ''); ?>><?php echo e($city->city_name); ?></option>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
          </select>
        </div>
        
        <!-- <div class="col-3 text-right">
          <a class="btn btn-secondary" href="javascript:void();" onclick="open_modal('add-company');"><i class="fa fa-plus-circle text-success"></i> Add Company</a>
        </div> -->
      </div>
      <div class="row">
  
        <div class="col-12">
          <table id="users-list" class="table table-condensed table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>User Details</th>
                <th>Address</th>
                <th>Register Type</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
<?php if($users): ?>
  <?php
    $i = 1
  ?>
  <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
              <td><?php echo e($i++); ?></td>
              <td>
                <p>
                  <b>Name: </b><?php echo e($user->firstName); ?> <?php echo e($user->lastName); ?></p>
                <p>
                  <b>Email: </b><?php echo e($user->email); ?></p>
                <p>
                  <b>Phone: </b><?php echo e($user->isd_code); ?> <?php echo e($user->phone); ?>

                </p>
              </td>
              <td>
                <p><b>City: </b><?php echo e($user->city_name); ?></p>
                <p><b>State: </b><?php echo e($user->province_name); ?></p>
                <p><b>Country: </b><?php echo e($user->country_name); ?></p>
              </td>
              <td class="text-center">
                <?php if($user->social_type): ?>
                  <?php echo e(ucwords($user->social_type)); ?>

                <?php else: ?>
                  Normal
                <?php endif; ?>
              </td>
              <td>
                <?php if($user->user_is_active==1): ?> 
                <span class="badge badge-success">Active</span>
                <?php else: ?>
                <span class="badge badge-danger">Inactive</span>
                <?php endif; ?>
              </td>
              <td>
                  <?php if($user->user_is_active==1): ?>
                  <a class="custom_btn _edit" onclick="execute('admin/active-inactive-user','_user_id=<?php echo e($user->id); ?>&user_is_active=1');" title="Inactivate">
                  <i class="fa fa-lock" aria-hidden="true"></i>
                  </a>
                  <?php else: ?>
                  <a class="custom_btn _edit" onclick="execute('admin/active-inactive-user','_user_id=<?php echo e($user->id); ?>&user_is_active=0');" title="Activate">
                  <i class="fa fa-unlock" aria-hidden="true"></i>
                  </a>
                  <?php endif; ?>
                
              </td>
            </tr>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php else: ?>
    <tr><td colspan="7" class="text-center"><b>No User Available</b></td></tr>
<?php endif; ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $__env->make('admin.inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!-- content-wrapper ends -->
<script type="text/javascript">
$(document).ready(function() {  

  dataTable = $('#users-list').DataTable( {
    dom: 'lBfrtip',
    responsive: true,
    ordering : true,
    lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
    pageLength: 10,
    buttons: [
      {
          extend: 'print',
          text: '<button class="btn btn-primary btn-sm" ><i class="fa fa-print fa-x5"> Print</i></button>',
          autoPrint: false,
          exportOptions: {
              columns: [ 0,1,2,3,4 ]
          }
      },
      {
          extend: 'excelHtml5',
          text: '<button class="btn btn-success btn-sm"><i class="fa fa-file-excel-o fa-x5"> Excel</i></button>',
          exportOptions: {
              columns: [ 0,1,2,3,4 ]
          }
      },
      {
          extend: 'pdfHtml5',
          text: '<button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o fa-x5"> PDF</i></button>',
          exportOptions: {
              columns:[ 0,1,2,3,4 ]
          }
      }
    ],
    "processing": true,
    "serverSide": false,
  });
});
function pageReload(){
  var _country_id = $('#country_id').val();
  var _province_id = $('#province_id').val();
  var _city_id = $('#city_id').val();
  var page_url = site_url('admin/users?_country_id='+_country_id+'&_province_id='+_province_id+'&_city_id='+_city_id);
  $(location).attr("href", page_url);
}
</script>
<?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/users_list.blade.php ENDPATH**/ ?>