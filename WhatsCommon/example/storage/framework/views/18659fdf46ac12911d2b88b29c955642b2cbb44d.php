<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                <div class="leftSlidePan closePan">
                  <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                  <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                    <div class="settingLeftTitle">Setting</div>
                    <ul class="settingMenu mb-0 list-unstyled">
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="<?php echo e(url('settings')); ?>"><img src="new-design/img/profileIcon.png" class="img-fluid menuIcon" alt="">Profile</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu settingMenuActive" href="<?php echo e(url('financial-settings')); ?>"><img src="new-design/img/financial-a.png" class="img-fluid menuIcon" alt="">Financial Settings</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="<?php echo e(url('change-password')); ?>"><img src="new-design/img/change-password.png" class="img-fluid menuIcon" alt="">Change Password</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="<?php echo e(url('security')); ?>"><img src="new-design/img/security.png" class="img-fluid menuIcon" alt="">Security and Privacy</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="<?php echo e(url('blocked-users')); ?>"><img src="new-design/img/blocking.png" class="img-fluid menuIcon" alt="">Blocking</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="<?php echo e(url('notification-settings')); ?>"><img src="new-design/img/notifications.png" class="img-fluid menuIcon" alt="">Notifications</a>
                      </li>
                      
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="<?php echo e(url('message-settings')); ?>"><img src="new-design/img/message-setting.png" class="img-fluid menuIcon" alt="">Message Setting</a>
                      </li>
                    </ul>
                  </div>
                </div>

                <div class="midPan">
                  <div class="connectionsBody windowHeight windowHeightMid bankDetails">
                    <div class="financialDetailsOpen">
                      <div class="addBankBackIcon addOtherBank">
                        <img src="new-design/img/addLifeEvents.png" class="img-fluid" alt="">
                      </div>
      <?php if($banks && count($banks)>0): ?>
        <?php
          $default = FALSE
        ?>
        <?php if($banks[0]->bank_is_default==1): ?>

          <?php
            $default = TRUE
          ?>
                      <div class="bankDetailsTitle text-left">Default Bank</div>
                      <div class="myBankDetails">
                        <?php if($banks[0]->bank_type=='paypal'): ?>
                        <div class="myBankDetailsOnly">
                          <div class="myBankName">
                              <?php echo e('Paypal Account'); ?>

                          </div>
                          <div class="myBankHolder"><?php echo e($banks[0]->bank_card_holder_name); ?></div>
                          <div class="myBankNumber"><?php echo e($banks[0]->bank_email); ?></div>
                        </div>
                        <?php else: ?>
                              
                        <div class="myBankDetailsOnly">
                          <div class="myBankName">
                              <?php echo e($banks[0]->bank_name); ?>

                          </div>
                          <div class="myBankHolder"><?php echo e($banks[0]->bank_card_holder_name); ?></div>
                          <div class="myBankNumber"><?php echo e($banks[0]->bank_ac_or_card_no); ?></div>
                        </div>
                        <?php endif; ?>
                        <a class="myBankDetailsDelete" onclick="execute('api/delete-bank','bank_id=<?php echo e($banks[0]->id); ?>&user_id=<?php echo e(Session::get("userdata")["id"]); ?>')">
                          <img src="new-design/img/delete.png" class="img-fluid" alt="">
                        </a>
                      </div>
        <?php endif; ?>

        <?php if(($default && count($banks)>1) || (!$default && count($banks)==1)): ?>
                      <div class="bankDetailsTitle text-left mt-5`">Other Payment Method</div>
        <?php endif; ?>
        <?php $__currentLoopData = $banks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bank): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <?php if($bank->bank_is_default==1): ?>
            <?php continue; ?>
          <?php endif; ?>

          <?php if($bank->bank_type=='paypal'): ?>
                      <div class="myBankDetails">
                        <div class="myBankDetailsOnly">
                          <div class="myBankName">Paypal Account</div>
                          <div class="myBankHolder"><?php echo e($bank->bank_card_holder_name); ?></div>
                          <div class="myBankNumber"><?php echo e($bank->bank_email); ?></div>
                        </div>
                        <a class="myBankDetailsDelete" onclick="execute('api/delete-bank','bank_id=<?php echo e($bank->id); ?>&user_id=<?php echo e(Session::get("userdata")["id"]); ?>')">
                          <img src="new-design/img/delete.png" class="img-fluid" alt="">
                        </a>
                        <a class="myBankMakeDefault" onclick="execute('api/set-default-bank','bank_id=<?php echo e($bank->id); ?>&user_id=<?php echo e(Session::get("userdata")["id"]); ?>')">Make as <br>Default</a>
                      </div>
          <?php else: ?>
                      <div class="myBankDetails">
                        <div class="myBankDetailsOnly">
                          <div class="myBankName"><?php echo e($bank->bank_name); ?></div>
                          <div class="myBankHolder"><?php echo e($bank->bank_card_holder_name); ?></div>
                          <div class="myBankNumber"><?php echo e($bank->bank_ac_or_card_no); ?></div>
                        </div>
                        <a class="myBankDetailsDelete" onclick="execute('api/delete-bank','bank_id=<?php echo e($bank->id); ?>&user_id=<?php echo e(Session::get("userdata")["id"]); ?>')">
                          <img src="new-design/img/delete.png" class="img-fluid" alt="">
                        </a>
                        <a class="myBankMakeDefault" onclick="execute('api/set-default-bank','bank_id=<?php echo e($bank->id); ?>&user_id=<?php echo e(Session::get("userdata")["id"]); ?>')">Make as <br>Default</a>
                      </div>
          <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      <?php endif; ?>
                      
                    </div>

                    <div class="financialDetailsClose removeBank">
                      <div class="addBankBackIcon text-left closeOtherBank">
                        <img src="new-design/img/back.png" class="img-fluid" alt="">
                      </div>
                      <div class="bankDetailsTitle">Bank Details</div>
                      <div class="bankDetailsTitle2">Select your method</div>
                      <div class="bankcardTab">
                        <ul class="nav nav-tabs" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#bank1">
                              <div class="tabCard">
                                <img src="new-design/img/card.png" class="img-fluid" alt="">
                                <div class="tabCardTitle">Debit/Credit</div>
                              </div>
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#bank2">
                              <div class="tabCard">
                                <img src="new-design/img/paypal.png" class="img-fluid" alt="">
                              </div>
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#bank3">
                              <div class="tabCard">
                                <img src="new-design/img/otherBank.png" class="img-fluid" alt="">
                                <div class="tabCardTitle">Other Bank</div>
                              </div>
                            </a>
                          </li>
                        </ul>

                        <div class="tab-content">
                          <div id="bank1" class="tab-pane active">
                            <div class="profileForm bankForm">
                              <form method="post" class="xhr_form" action="api/set-financial-setting" id="card-setting">
                                <?php echo csrf_field(); ?>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label for="number">Card Number</label>
                                      <input type="text" class="form-control" id="" placeholder="Card Number" name="card_number">
                                      <p class="text-danger" id="card_number_error"></p>
                                    </div>
                                  </div>
                                  <input type="hidden" name="type" value="card">
                                  <input type="hidden" name="user_id" value="<?php echo e(Session::get('userdata')['id']); ?>">
                                  <input type="hidden" name="is_default" value="0">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="number">Exp. Date (MM/YY)</label>
                                      <input type="text" class="form-control" id="" placeholder="Exp. Date (MM/YY)" 
                                      name="card_expiry_date">
                                      <p class="text-danger" id="card_expiry_date_error"></p>
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="number">CVV</label>
                                      <input type="text" class="form-control" id="" placeholder="CVV" name="card_cvv">
                                      <p class="text-danger" id="card_cvv_error"></p>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label for="number">Card Holder's Name</label>
                                      <input type="text" class="form-control" id="" placeholder="Card Holder's Name" 
                                      name="card_holder_name">
                                      <p class="text-danger" id="card_holder_name_error"></p>
                                    </div>
                                  </div>
                                </div>
                                <div class="btn-Edit-save">
                                  
                                  <button class="btn btn-Edit btn-save" onclick="makeDefault('card-setting');" >Default</button>
                                  <button class="btn btn-Edit btn-save" type="submit">Add</button>
                                </div>
                              </form>
                            </div>
                          </div>
                          <div id="bank2" class="tab-pane fade">
                            <div class="profileForm bankForm">
                              <form method="post" class="xhr_form" action="api/set-financial-setting" id="paypal-setting">
                                <?php echo csrf_field(); ?>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label for="email">Email</label>
                                      <input type="email" class="form-control" id="" placeholder="Email" name="paypal_email">
                                      <p class="text-danger" id="paypal_email_error"></p>
                                    </div>
                                  </div>
                                  <input type="hidden" name="type" value="paypal">
                                  <input type="hidden" name="user_id" value="<?php echo e(Session::get('userdata')['id']); ?>">
                                  <input type="hidden" name="is_default" value="0">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label for="password">Password</label>
                                      <input type="password" class="form-control" id="" placeholder="Password" name="paypal_password">
                                      <p class="text-danger" id="paypal_password_error"></p>
                                    </div>
                                  </div>
                                </div>
                                <div class="btn-Edit-save">
                                  
                                  <button class="btn btn-Edit btn-save" onclick="makeDefault('paypal-setting');">Default</button>
                                  <button class="btn btn-Edit btn-save" type="submit">Add</button>
                                </div>
                              </form>
                            </div>
                          </div>
                          <div id="bank3" class="tab-pane fade">
                            <div class="profileForm bankForm">
                              <form method="post" class="xhr_form" action="api/set-financial-setting" id="bank-setting">
                                <?php echo csrf_field(); ?>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label for="number">Card Holder's Name</label>
                                      <input type="text" class="form-control" id="" placeholder="Card Holder's Name" name="card_holder_name">
                                      <p class="text-danger" id="card_holder_name_error"></p>
                                    </div>
                                  </div>
                                  <input type="hidden" name="type" value="bank">
                                  <input type="hidden" name="user_id" value="<?php echo e(Session::get('userdata')['id']); ?>">
                                  <input type="hidden" name="is_default" value="0">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label for="">Bank Name</label>
                                      <input type="text" class="form-control" name="bank_name" placeholder="Bank Name">
                                      <p class="text-danger" id="bank_name_error"></p>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label for="number">Routing Number</label>
                                      <input type="text" class="form-control" id="" placeholder="Routing Number" name="routing_number">
                                      <p class="text-danger" id="routing_number_error"></p>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label for="number">Account Number</label>
                                      <input type="text" class="form-control" id="" placeholder="Account Number" name="account_number">
                                      <p class="text-danger" id="account_number_error"></p>
                                    </div>
                                  </div>
                                </div>
                                <div class="btn-Edit-save">
                                  
                                  <button class="btn btn-Edit btn-save" onclick="makeDefault('bank-setting');">Default</button>
                                  <button class="btn btn-Edit btn-save" type="submit">Add</button>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/financial_settings.blade.php ENDPATH**/ ?>