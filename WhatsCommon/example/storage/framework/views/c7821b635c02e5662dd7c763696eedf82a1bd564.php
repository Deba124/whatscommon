<?php $__env->startSection('title', 'Edit Page'); ?>
<?php echo $__env->make('admin.inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('admin.inc.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <div class="content-wrapper">
    <div class="card">
      <div class="card-body">
        <form method="POST" class="form-horizontal admin_form" id="update-page">
          <div class="form-group" id="submit_status"></div>
          <input type="hidden" name="_page_id" value="<?php echo e($page->id); ?>">
          <?php echo csrf_field(); ?>
          <div class="form-group">
            <label>Page Title<span class="text-danger">*</span></label>
            <input type="text" name="page_title" class="form-control" value="<?php echo e($page->page_title); ?>">
            <p class="text-danger" id="page_title_error"></p>
          </div>
          <div class="form-group">
            <label>Content<span class="text-danger">*</span></label>
            <textarea class="form-control ckeditor" name="page_content" rows="5"><?php echo e($page->page_content); ?></textarea>
            <p class="text-danger" id="page_content_error"></p>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- content-wrapper ends -->
<?php echo $__env->make('admin.inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/edit_page.blade.php ENDPATH**/ ?>