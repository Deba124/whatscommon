<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta name="_token" content="<?php echo e(csrf_token()); ?>">
  <title>What's Common</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo e(url('new-design/img/favicon.png')); ?>" rel="icon">
  <link href="<?php echo e(url('new-design/assets/img/apple-touch-icon.png')); ?>" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Open+Sans&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <!-- Vendor CSS Files -->
  <link href="<?php echo e(url('new-design/assets/vendor/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('new-design/assets/vendor/owl.carousel/assets/owl.carousel.min.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('new-design/assets/vendor/aos/aos.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('new-design/assets/vendor/picker/css/picker.css')); ?>" rel="stylesheet">
  <!-- Template Main CSS File -->
  <link href="<?php echo e(url('new-design/css/style.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('new-design/css/style-bkp.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('new-design/css/styleb.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('new-design/css/rangeslider.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('css/toastr.min.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('css/jquery.magnify.css')); ?>" rel="stylesheet">
  <script src="<?php echo e(url('new-design/assets/vendor/jquery/jquery.min.js')); ?>"></script>
  <!-- <link rel="stylesheet" href="css/slideMenu.css"> -->
  <!-- <link href="css/jquery.bxslider.css" rel="stylesheet"> -->

  <!-- ========================================================
  * Template Name: Virtual Event
  * Author: Biswanath hazra
  ========================================================= -->

<body id="hidenScroll">
  <div id="loading" class="ajax_loader"></div>

  <section id="login" class="signUpBg innerBg">
    <div class="wrapper d-flex align-items-stretch">
<style type="text/css">
.ajax_loader {
  position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background: url('<?php echo e(url("img/loader.gif")); ?>') center no-repeat transparent;
  background-size: 200px 200px;
  text-align: center;
  display: none;
}
#msgCount{
    position: absolute;
    background-color: #3dd495;
    width: 14px;
    height: 14px;
    font-size: 8px;
    font-weight: 400;
    color: #fff;
    border-radius: 50%;
    border: 2px solid #f2f2f2;
    display: flex;
    justify-content: center;
    align-items: center;
    left: unset;
    top: 2px;
    margin-left: 15px;
}
.magnify-modal{
  background: #fff;
  border-radius: 20px;
  margin-top: 10%;
  max-height: 350px;
}
.magnify-button-close{
  position: absolute;
  background: #3b71b9;
  color: #fff;
  border-radius: 50%;
  right: -15px;
  bottom: 15px;
}
.magnify-title{
  display: none;
}
.magnify-button-prev,.magnify-button-next{
  margin-right: 15px;
  border-radius: 50%;
  border: 1px solid darkgrey;
  box-shadow: 4px 2px grey;
  color: #3b71b9;
}
</style>
      <!-- Page Content  -->
      <div id="content" class="innerBody">
        
        <div class="innerHeader">
          <div class="container-fluid">
            <div class="innerBodyHeader">
              <?php if(!Request::is('feed-details')): ?>
              <div class="innerLogo">
                <?php if(Session::has('userdata')): ?>
                <a href="<?php echo e(url('home')); ?>">
                <?php else: ?>
                <a href="<?php echo e(url('/')); ?>">
                <?php endif; ?>
                  <img src="<?php echo e(url('new-design/img/innerLogo.png')); ?>" class="img-fluid" alt="">
                </a>
              </div>

              <div class="headerAvtar">
                <div class="innerLogoRight">
                  <div class="searchEvents">
                    <?php echo csrf_field(); ?>
                      <div class="input-group">
                        <input type="text" class="form-control" name="search_query" id="search_query" placeholder="Your Keyword Searches">
                          <div class="input-group-append">
                            <button class="btn btn-eventSearch" type="submit" onclick="searchFeed(true);"><img src="<?php echo e(url('new-design/img/search2.png')); ?>" class="img-fluid" alt=""></button>
                           </div>
                           <img src="<?php echo e(url('new-design/img/shortEvents2.png')); ?>" class="img-fluid shortEvents activeFilterOn" alt="" title="Filter">
                        </div>
                    
                  </div>

                  <div class="myFilterAll eventsFilterOff">
                    <div class="myFilter windowHeight windowHeightMid">
                      
                      <div class="bankDetailsTitle">Search</div>
                        
                        <div class="myAllEvents">
                          <div class="eventsOnly activeEvents search_type" id="search_type_1">Life Events</div>
                          <div class="eventsOnly search_type" id="search_type_2">Global Feed</div>
                        </div>
                        <input type="hidden" name="search_type" id="search_type" value="life_event">
                        <div class="bankDetailsTitle">Life Event</div>
                        <div class="feedbackTitle2">Please select life events that you like are relevant for your
                          search</div>
                        <div class="myAllEvents">
                          <div class="eventsOnly life_event_type" id="life_event_type_1">Personal</div>
          <?php if(Session::get('userdata')['user_age'] && Session::get('userdata')['user_age']>=18): ?>
                          <div class="eventsOnly life_event_type" id="life_event_type_2">Dating</div>
          <?php endif; ?>
                          <div class="eventsOnly life_event_type" id="life_event_type_3">Adoption</div>
                          <div class="eventsOnly life_event_type" id="life_event_type_4">Travel</div>
                          <div class="eventsOnly life_event_type" id="life_event_type_5">Military</div>
                          <div class="eventsOnly life_event_type" id="life_event_type_6">Education</div>
                          
                          <div class="eventsOnly life_event_type" id="life_event_type_7">Works</div>
                          <div class="eventsOnly life_event_type" id="life_event_type_8">Pets</div>
                          <div class="eventsOnly life_event_type" id="life_event_type_9">Lost/Found</div>
                          
                          <div class="eventsOnly life_event_type" id="life_event_type_all">All</div>
                        </div>
                        <input type="hidden" name="life_event_type" id="life_event_type" >
                        <div class="bankDetailsTitle mt-4">Category</div>
                        <div class="myAllEvents">
                          <div class="eventsOnly search_category" id="search_category_what">What</div>
                          <div class="eventsOnly search_category" id="search_category_where">Where</div>
                          <div class="eventsOnly search_category" id="search_category_when">When</div>
                          
                          <div class="eventsOnly search_category" id="search_category_all">All</div>
                        </div>
                        <input type="hidden" name="search_category" id="search_category">
                      <?php if(!Request::is('create-lifeevent')): ?>
                        <div class="bankDetailsTitle mt-4">keywords</div>
                        <div class="myAllEvents" id="keywords_div"></div>
                        <input type="hidden" id="keywords" value="">
                        <div class="input-group lifeEvent">
                          <input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
                          <div class="input-group-append">
                            <button onclick="addKeyword();" class="btn btn-add">Add</button>
                           </div>
                        </div>
                      <?php endif; ?>
                        <button class="btn btn-proceed w-100" onclick="searchFeed(true);" >Search</button>
                      
                    </div>
                  </div>
                </div>
                <div class="headerNotificationsTab">
                <ul class="innerHeaderIcon mb-0 list-unstyled">
                  <li class="d-inline-block">
                    <!-- <a href="#">
                      <img src="<?php echo e(url('new-design/img/detailsIcon.png')); ?>" class="img-fluid" alt="">
                    </a> -->
                    <div id="myInfo" title="Information">
                      <div class="detailsIcon"></div>
                    </div>
                  </li>
                  <li class="d-inline-block">
                    <a href="<?php echo e(url('connections')); ?>" title="Connections">
                      <img src="<?php echo e(Request::is('connections') ? url('new-design/img/connectIcon-a.png') : url('new-design/img/connectIcon.png')); ?>" class="img-fluid" alt="">
                    </a>
                  </li>
                  <li class="d-inline-block">
                    <a href="<?php echo e(url('home')); ?>" title="Home">
                      <img src="<?php echo e(Request::is('home') ? url('new-design/img/homeIcom-a.png') : url('new-design/img/homeIcom.png')); ?>" class="img-fluid" alt="">
                    </a>
                  </li>
                  <li class="d-inline-block">
                    <a href="<?php echo e(url('messages')); ?>" title="Messages">
                      <img src="<?php echo e(Request::is('messages') ? url('new-design/img/msgIcon-a.png') : url('new-design/img/msgIcon.png')); ?>" class="img-fluid" alt="">
                      <div id="msgCount"></div>
                    </a>
                  </li>
                  <li class="d-inline-block notificationsOn">
                      <a href="#" title="Notification">
                        <img src="<?php echo e(url('new-design/img/bellIcon.png')); ?>" class="img-fluid" alt="">
                        <div class="msgCount" id="msgreqCount">0</div>
                      </a>
                    </li>
                  <li class="d-inline-block">
                    <a href="<?php echo e(url('settings/?menu=1')); ?>" title="Settings">
                      <img src="<?php echo e(in_array(Request::path(),['settings','financial-settings','change-password','notification-settings','security','message-settings','blocked-users']) ? url('new-design/img/settingIcon-a.png') : url('new-design/img/settingIcon.png')); ?>" class="img-fluid" alt="">
                    </a>
                  </li>
                  <li class="d-inline-block">
                    <a href="<?php echo e(url('feedback/?menu=1')); ?>" title="Information">
                      <img src="<?php echo e(in_array(Request::path(),['feedback','contact-us','about-us','premium-plans','donate']) ? url('new-design/img/askIcon-a.png') : url('new-design/img/askIcon.png')); ?>" class="img-fluid" alt="">
                    </a>
                  </li>
                </ul>
                <div class="NotificationsAll eventsNotificationOff">
                    <div class="notificationsBox windowHeight notificationHeight" id="msgreqDiv">
                      
                    </div>
                  </div>
                </div>
                <div class="headerAvtarDetail">
                  <div class="avtarName"><?php echo e(Session::get('userdata')['firstName']); ?> <br><span><?php echo e('@'); ?><?php echo e(Session::get('userdata')['username']); ?></span></div>
                  <div class="avtarImg">
                    <a href="<?php echo e(url('my-profile')); ?>" title="Profile">
                      <img src="<?php echo e(Session::get('userdata')['profile_photo_path']); ?>" <?php if(Request::is('my-profile')): ?> style="border: 3px solid orange;" <?php endif; ?> class="img-fluid" alt="">
                    </a>
                  </div>
                </div>
              </div>
            <?php endif; ?>
            </div>
          </div>
        </div>
<?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/inc/header.blade.php ENDPATH**/ ?>