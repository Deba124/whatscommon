<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>What's Common</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <?php echo $__env->make('inc.css', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body>
  <section id="login" class="signUpBg">
    <div class="container">
      <div class="AllLogin">
        <div class="loginForm">
          <div class="loginLogo">
            <img src="img/signUpLogo.png" class="img-fluid" alt="">
          </div>

          <div class="signUpHeading">Sign Up</div>
          <div class="loginText" style="color: #000;">Fill in your login information</div>

          <form action="" method="post" class="signUpForm" id="signUpForm">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="inputText form-control" name="last_name" required/>
                  <span class="floating-label">Last Name</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="inputText form-control" name="first_name" required/>
                  <span class="floating-label">First Name</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="inputText form-control" />
                  <span class="floating-label">MI (Optional)</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="email" class="inputText form-control" required/>
                  <span class="floating-label">Email Address</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="number" class="inputText form-control" required/>
                  <span class="floating-label">Phone Number</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="inputText form-control" required/>
                  <span class="floating-label">Username</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="password" class="inputText form-control" required/>
                  <span class="floating-label">Password</span>
                  <img src="img/passwordVisiable2.png" class="img-fluid passwordVisiable" alt="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="password" class="inputText form-control" required/>
                  <span class="floating-label">Re-type Password</span>
                  <img src="img/passwordVisiable2.png" class="img-fluid passwordVisiable" alt="">
                </div>
              </div>
            </div>
            <!-- <a href="signup-details" class="btn btn-proceed">Proceed</a> -->
            <button href="signup-details" class="btn btn-proceed">Proceed</button>
          </form>
        </div>
      </div>
    </div>
  </section>

  <?php echo $__env->make('inc.script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script type="text/javascript">
$(document).on('submit','#signUpForm',function(e){
  e.preventDefault();
  var token = $('input[name="_token"]').attr('value');
  console.log(token);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token
    }
  });
  var action = site_url('register');
  $.ajax({
    type : "POST",
    data : new FormData(this),
    contentType: false,   
    cache: false,             
    processData:false, 
    url  : action,
    success : function(response){
      console.log(response);
      if(response.response_code==200){
        $('#responseDiv').show();
        $('#responseDiv').html('<p class="text-success">'+response.response_msg+'</p>');
        $(location).attr("href", site_url('signup-details'));
      }
    },error: function(jqXHR, textStatus, errorThrown){
    /*console.log(textStatus + ": " + jqXHR.status + " " + errorThrown);*/
    /*console.log(jqXHR);*/
    console.log(jqXHR.responseJSON);
    if(jqXHR.responseJSON.errors){
      var err = jqXHR.responseJSON.errors;
      $.each(err, function(key, value){

        if (err[key]) {
          $("#"+key+"_error").html(err[key]);
        }
        else{
          $("#"+key+"_error").html(''); 
        }
      });
    }
    /*$('#responseDiv').show();
    $('#responseDiv').html('<p class="alert alert-danger">'+jqXHR.responseJSON.response_msg+'</p>');*/
  }
  });
});  
</script>
</body>

</html><?php /**PATH /home/worksamples/public_html/WhatsCommon/example/resources/views/signup.blade.php ENDPATH**/ ?>