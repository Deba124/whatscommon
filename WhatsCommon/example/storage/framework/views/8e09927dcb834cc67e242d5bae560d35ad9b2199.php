<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Admin Login</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="assets\vendors\iconfonts\mdi\css\materialdesignicons.min.css">
  <link rel="stylesheet" href="assets\vendors\iconfonts\puse-icons-feather\feather.css">
  <link rel="stylesheet" href="assets\vendors\css\vendor.bundle.base.css">
  <link rel="stylesheet" href="assets\vendors\css\vendor.bundle.addons.css">
  <link rel="stylesheet" href="<?php echo e(url('/admin/assets/css/toastr.min.css')); ?>">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="assets\css\style.css">
  <!-- endinject -->
  <!-- <link rel="shortcut icon" href="assets\images\logo.png" type="image/x-icon"> -->
  <!-- <link rel="icon" href="assets/images/logo.png" type="image/x-icon"> -->
  <link href="../img/favicon.png" rel="icon">
</head>
<?php
$background = url('img/loginBg.jpg');
?>
<body style="background: url('<?php echo e($background); ?>');">
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
        <div class="row w-100">
          <div class="col-lg-12 mx-auto my-5">
            <div class="text-center">
              <img src="<?php echo e(url('new-design/img/loginLogo.png')); ?>" class="img-fluid">
            </div>
          </div>
          <div class="col-lg-4 mx-auto">
            <div class="auto-form-wrapper">
              <form method="POST" class="form-horizontal admin_form" id="submit-login">
                <?php echo csrf_field(); ?>
                <div class="form-group">
                  <label class="label">Username</label>
                  
                    <input type="text" name="admin_email" value="" class="form-control" placeholder="Username" style="border-radius: 6px;">
                    <!-- <div class="input-group-append">
                      <span class="input-group-text"><i class="mdi mdi-check-circle-outline"></i></span>
                    </div> -->
                  <p id="admin_email_error" class="text-danger"></p>
                </div>
                <div class="form-group">
                  <label class="label">Password</label>
                  <div class="input-group">
                    <input type="password" name="admin_password" id="admin_password" value="" class="form-control" placeholder="**************">
                    <div class="input-group-append">
                      <img src="<?php echo e(url('new-design/img/passwordVisiable.png')); ?>" class="img-fluid passwordVisiable input-group-text" alt="" onclick="showHidePassword('admin_password');" style="background-color: #e6e6e6;">
                    </div>
                    <p id="admin_password_error" class="text-danger"></p>
                  </div>
                </div>
                <div class="form-group">
                  <button class="btn btn-success submit-btn btn-block" type="submit">Login</button>
                </div>
                <div class="form-group d-flex justify-content-between">
                  <!-- <a href="forgot-password" class="text-small forgot-password text-black">Forgot Password</a> -->
                </div>
              </form>
            </div>
            <ul class="auth-footer">
              <li><a href="#">Conditions</a></li>
              <li><a href="#">Help</a></li>
              <li><a href="#">Terms</a></li>
            </ul>
            <p class="footer-text text-center">copyright © 2018 Bootstrapdash. All rights reserved.</p>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="assets\vendors\js\vendor.bundle.base.js"></script>
  <script src="assets\vendors\js\vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="assets\js\off-canvas.js"></script>
  <script src="assets\js\hoverable-collapse.js"></script>
  <script src="assets\js\misc.js"></script>
  <script src="assets\js\settings.js"></script>
  <script src="assets\js\todolist.js"></script>
  <script src="assets\js\admin\admin.js"></script>
  <!-- endinject -->

  <script src="assets/js/toastr.min.js"></script>
  <script>
  function site_url(page_name=''){
    var site_url = '<?php echo url('/');  ?>';
    site_url = site_url+'/'+page_name;
    console.log(site_url);
    return site_url;
  }
  $(document).ready(function () {
    toastr.options = {
      "closeButton"       : false,
      "debug"             : false,
      "newestOnTop"       : true,
      "progressBar"       : true,
      "positionClass"     : "toast-bottom-right",
      "preventDuplicates" : false,
      "onclick"           : null,
      "showDuration"      : "300",
      "hideDuration"      : "1000",
      "timeOut"           : "5000",
      "extendedTimeOut"   : "1000",
      "showEasing"        : "swing",
      "hideEasing"        : "linear",
      "showMethod"        : "fadeIn",
      "hideMethod"        : "fadeOut"
    };
  });
  function showHidePassword(id){
  if($('#'+id).attr('type')=='password'){
    $('#'+id).attr('type','text');
  }else{
    $('#'+id).attr('type','password');
  }
}
  </script>
</body>

</html>
<?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/login.blade.php ENDPATH**/ ?>