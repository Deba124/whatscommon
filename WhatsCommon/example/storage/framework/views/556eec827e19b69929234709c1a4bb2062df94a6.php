 <!-- Modal starts -->       
<div class="modal fade" id="add-vehicle-maker-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Add Vehicle Maker</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="add-vehicle-maker">
          <div class="form-group" id="submit_status"></div>
          <?php echo csrf_field(); ?>
          <div class="form-group">
            <label>Vehicle Type <span class="text-danger">*</span></label>
            <select class="form-control" name="maker_vehicle_type">
              <option value="" hidden="">Select Vehicle Type</option>
<?php if($vehicle_types): ?>

  <?php $__currentLoopData = $vehicle_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vehicle_type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($vehicle_type->id); ?>"><?php echo e($vehicle_type->vehicle_type_name); ?></option>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
            </select>
            <p class="text-danger" id="maker_vehicle_type_error"></p>
          </div>
          <div class="form-group">
            <label>Vehicle Maker <span class="text-danger">*</span></label>
            <input type="text" name="maker_name" class="form-control">
            <p class="text-danger" id="maker_name_error"></p>
          </div>
          
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends --><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/xhr/add_vehicle_maker_modal.blade.php ENDPATH**/ ?>