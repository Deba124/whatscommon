<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                <div class="leftSlidePan <?php echo e(Request::get('menu')==1 ? '' : 'closePan'); ?>">
                  <a href="#" class="panelslideOpenButton <?php echo e(Request::get('menu')==1 ? 'panelslideCloseButton' : ''); ?>"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                    <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                      <div class="settingLeftTitle">Information</div>
                      <ul class="settingMenu mb-0 list-unstyled">
                        <li class="settingMenuLi">
                          <a class="settingMenu" href="<?php echo e(url('feedback')); ?>"><img src="new-design/img/feedback.png" class="img-fluid menuIcon" alt="">Feedback</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu" href="<?php echo e(url('contact-us')); ?>"><img src="new-design/img/contact.png" class="img-fluid menuIcon" alt="">Contact Us</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu " href="<?php echo e(url('about-us')); ?>"><img src="new-design/img/about.png" class="img-fluid menuIcon" alt="">About Us</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu <?php echo e((Session::get('plan_sess')) ? 'settingMenuActive' : ''); ?> " href="<?php echo e(url('premium-plans')); ?>"><img src="new-design/img/premium-a.png" class="img-fluid menuIcon" alt="">WC Premium</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu <?php echo e((!Session::get('plan_sess')) ? 'settingMenuActive' : ''); ?>" href="<?php echo e(url('donate')); ?>"><img src="new-design/img/donate.png" class="img-fluid menuIcon" alt="">Donate</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu " style="color: #ed1c24;" href="<?php echo e(url('logout')); ?>"><img src="<?php echo e(url('new-design/img/logout.png')); ?>" class="img-fluid menuIcon" alt="">Log Out</a>
                        </li>
                      </ul>
                    </div>
                </div>
                <div class="midPan">
                  <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                    <div class="row infoBodyRow">
                      <div class="col-md-5 infoBodyCol">
                          <div class="innerHome">
                            <a href="<?php echo e(url('home')); ?>"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                          </div>
                        <div class="feedbackImg">
                        <?php if(Session::get('plan_sess')): ?>
                          <img src="new-design/img/premiumImg.png" class="img-fluid" alt="" style="width: 80%;">
                        <?php else: ?>
                          <img src="<?php echo e(url('new-design/img/donateImg.png')); ?>" class="img-fluid" alt="">
                        <?php endif; ?>
                        </div>
                      </div>
                      <!--<div class="col-md-7 infoBodyCol premiumCol">-->
                      <div class="col-md-7 infoBodyCol feedbackColFixed midHightControl" style="background: none;">
                        <div class="feedbackRight wcPremium windowHeight windowHeightMid p-0">
                          <div class="premiumBody" style="height: 73vh;padding-top: 20%;">
                            <div class="form-group text-center">
        <img src="<?php echo e(url('new-design/img/sucess_ic.png')); ?>">
        <p class="text-success">Thank You for the payment.</p>
        <!-- <button class="btn btn-Edit btn-save" type="submit">Done</button> -->
      </div>
                          </div>
                        </div>
                        
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php if(Session::get('plan_sess') && !empty(Session::get('plan_sess')['plan_id'])): ?>
<script>
  var plan_data = `user_id=<?php echo e(Session::get('userdata')['id']); ?>&plan_id=`+'<?php echo e(Session::get('plan_sess')['plan_id']); ?>&call_from=web&inapp_type=<?php echo e(Session::get('plan_sess')['inapp_type'] ? Session::get('plan_sess')['inapp_type'] : NULL); ?>&inapp_productid=<?php echo e(Session::get('plan_sess')['inapp_productid'] ? Session::get('plan_sess')['inapp_productid'] : NULL); ?>&inapp_token=<?php echo e(Session::get('plan_sess')['inapp_token'] ? Session::get('plan_sess')['inapp_token'] : NULL); ?>';
  execute('choose-plan',plan_data);
</script>
<?php elseif(Session::get('donate_sess') && !empty(Session::get('donate_sess')['currency_id'])): ?>
<script>
  var donation_data = `donation_amount=<?php echo e(Session::get('donate_sess')['donation_amount']); ?>&currency_id=`+'<?php echo e(Session::get('donate_sess')['currency_id']); ?>';
  execute('submit-donation',donation_data);
</script>
<?php endif; ?><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/payment_success.blade.php ENDPATH**/ ?>