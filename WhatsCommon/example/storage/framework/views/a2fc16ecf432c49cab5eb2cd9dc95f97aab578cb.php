<option value="" hidden="">Select State</option>
<?php if($provinces): ?>
	<?php $__currentLoopData = $provinces; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $province): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php if($for_life_event && $for_life_event==1): ?>
			<option value="<?php echo e($province->province_name); ?>" data-id="<?php echo e($province->_province_id); ?>"><?php echo e($province->province_name); ?></option>
		<?php else: ?>
			<option value="<?php echo e($province->_province_id); ?>"><?php echo e($province->province_name); ?></option>
		<?php endif; ?>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/xhr/province.blade.php ENDPATH**/ ?>