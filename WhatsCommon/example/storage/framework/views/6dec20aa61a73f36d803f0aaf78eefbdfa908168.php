<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly">
<div class="container-fluid">
<div class="settingBody">
  <div class="position-relative settingFlip">
    <div class="leftSlidePan <?php echo e(Request::get('menu')==1 ? '' : 'closePan'); ?>">
                  <a href="#" class="panelslideOpenButton <?php echo e(Request::get('menu')==1 ? 'panelslideCloseButton' : ''); ?>"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
      <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
      	<div class="settingLeftTitle">Settings</div>
      	<ul class="settingMenu mb-0 list-unstyled">
      		<li class="settingMenuLi">
      			<a class="settingMenu" href="<?php echo e(url('settings')); ?>"><img src="new-design/img/profileIcon.png" class="img-fluid menuIcon" alt="">Profile</a>
      		</li>
      		<!-- <li class="settingMenuLi">
      			<a class="settingMenu" href="<?php echo e(url('financial-settings')); ?>"><img src="new-design/img/financial.png" class="img-fluid menuIcon" alt="">Financial Settings</a>
      		</li> -->
      		<li class="settingMenuLi">
      			<a class="settingMenu" href="<?php echo e(url('change-password')); ?>"><img src="new-design/img/change-password.png" class="img-fluid menuIcon" alt="">Change Password</a>
      		</li>
      		<li class="settingMenuLi">
      			<a class="settingMenu" href="<?php echo e(url('security')); ?>"><img src="new-design/img/security.png" class="img-fluid menuIcon" alt="">Security and Privacy</a>
      		</li>
      		<li class="settingMenuLi">
      			<a class="settingMenu" href="<?php echo e(url('blocked-users')); ?>"><img src="new-design/img/blocking.png" class="img-fluid menuIcon" alt="">Blocking</a>
      		</li>
      		<li class="settingMenuLi">
      			<a class="settingMenu" href="<?php echo e(url('notification-settings')); ?>"><img src="new-design/img/notifications.png" class="img-fluid menuIcon" alt="">Notifications</a>
      		</li>
      		<li class="settingMenuLi">
      			<a class="settingMenu" href="<?php echo e(url('link-accounts')); ?>"><img src="new-design/img/link-account.png" class="img-fluid menuIcon" alt="">Link Account</a>
      		</li>
      		<li class="settingMenuLi">
      			<a class="settingMenu settingMenuActive" href="<?php echo e(url('message-settings')); ?>"><img src="new-design/img/message-setting-a.png" class="img-fluid menuIcon" alt="">Message Settings</a>
      		</li>
      	</ul>
      </div>
    </div>
    <div class="midPan">
      <div class="connectionsBody windowHeight windowHeightMid msgSettingBg">
        <div class="">
          
            

          <div class="">
            <div id="" class="">
            	<form method="post" class="xhr_form" action="set-message-setting" id="set-message-setting">
                    <?php echo csrf_field(); ?>
              <div class="bankDetailsTitle">Message Settings</div>
              <div class="profileForm bankForm">
                <div class="linkAccountTitle"><img src="new-design/img/notiMsgSetting.png" class="img-fluid msgSettingIcon" alt="">Notifications</div>
                <ul class="linkAccountList">
                    <li>
                        <span style="width: calc(100% - 48px);">
                            Allow Notifications
                        </span>
                        <label class="switch">
                            <input type="checkbox" <?php if($settings && $settings->allow_notification==1): ?> checked <?php endif; ?> name="allow_notification" class="settings" value="1">
                            <span class="slider round"></span>
                        </label>
                    </li>
                </ul>

                <div class="linkAccountTitle"><img src="new-design/img/notiMsgSetting2.png" class="img-fluid msgSettingIcon" alt="">Media</div>
                <ul class="linkAccountList">
                    <li>
                        <span style="width: calc(100% - 48px);">
                            Save Photo and Videos
                        </span>
                        <label class="switch">
                            <input type="checkbox" <?php if($settings && $settings->save_media==1): ?> checked <?php endif; ?> name="save_media" class="settings" value="1">
                            <span class="slider round"></span>
                        </label>
                    </li>
                </ul>

              <div class="feedbackTitle2">Automatically save photos and videos on your camera roll.</div>
              </div>
          </form>
            </div>
            
          </div>

        </div>
        
      </div>
    </div>
  </div>
</div>
</div>
</div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
$(document).on('change','.settings', function(){
	$('.xhr_form').submit();
});
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/message_settings.blade.php ENDPATH**/ ?>