<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>What's Common</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <?php echo $__env->make('inc.css', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <link href="css/toastr.min.css" rel="stylesheet">

<body>
  <section id="login" class="signUpBg">
    <div class="container">
      <div class="AllLogin">
        <div class="loginForm" style="max-width: fit-content;">
          <div class="loginLogo">
            <img src="img/signUpLogo.png" class="img-fluid" alt="">
          </div>

          <div class="signUpHeading">Forgot Password</div>
          <div class="loginText" style="color: #000;">Enter Verification code in space below</div>

          <form action="check-email" class="signUpForm xhr_form" id="resetForm">
            <div class="row">
              <?php echo csrf_field(); ?>
              <div class="col-md-12 ">
                <div class="form-group">
                  <input type="email" class="inputText form-control" name="email" required/>
                  <span class="floating-label">Email Address</span>
                  <p class="text-danger" id="email_error"></p>
                </div>

                <div class="form-group" id="responseDiv" style="display: none;"></div>
              </div>
              
            </div>
            <button type="submit" class="btn btn-proceed" style="width: 100%;">Proceed</button>
          </form>
        </div>
      </div>
    </div>
  </section>
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  <script src="js/toastr.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      toastr.options = {
        "closeButton"       : false,
        "debug"             : false,
        "newestOnTop"       : true,
        "progressBar"       : true,
        "positionClass"     : "toast-bottom-right",
        "preventDuplicates" : false,
        "onclick"           : null,
        "showDuration"      : "300",
        "hideDuration"      : "1000",
        "timeOut"           : "5000",
        "extendedTimeOut"   : "1000",
        "showEasing"        : "swing",
        "hideEasing"        : "linear",
        "showMethod"        : "fadeIn",
        "hideMethod"        : "fadeOut"
      };
    });
  </script>
  <?php echo $__env->make('inc.script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

</body>

</html><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/forgot_password.blade.php ENDPATH**/ ?>