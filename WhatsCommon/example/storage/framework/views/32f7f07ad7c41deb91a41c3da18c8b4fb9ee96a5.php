<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                <div class="leftSlidePan <?php echo e(Request::get('menu')==1 ? '' : 'closePan'); ?>">
                  <a href="#" class="panelslideOpenButton <?php echo e(Request::get('menu')==1 ? 'panelslideCloseButton' : ''); ?>"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                  <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                    <div class="settingLeftTitle">Settings</div>
                    <ul class="settingMenu mb-0 list-unstyled">
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="<?php echo e(url('settings')); ?>"><img src="new-design/img/profileIcon.png" class="img-fluid menuIcon" alt="">Profile</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu settingMenuActive" href="<?php echo e(url('change-password')); ?>"><img src="new-design/img/change-password-a.png" class="img-fluid menuIcon" alt="">Change Password</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="<?php echo e(url('security')); ?>"><img src="new-design/img/security.png" class="img-fluid menuIcon" alt="">Security and Privacy</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="<?php echo e(url('blocked-users')); ?>"><img src="new-design/img/blocking.png" class="img-fluid menuIcon" alt="">Blocking</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="<?php echo e(url('notification-settings')); ?>"><img src="new-design/img/notifications.png" class="img-fluid menuIcon" alt="">Notifications</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="<?php echo e(url('link-accounts')); ?>"><img src="new-design/img/link-account.png" class="img-fluid menuIcon" alt="">Link Account</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="<?php echo e(url('message-settings')); ?>"><img src="new-design/img/message-setting.png" class="img-fluid menuIcon" alt="">Message Settings</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="<?php echo e(url('feed-settings')); ?>"><img src="new-design/img/global-feed.png" class="img-fluid menuIcon" alt="">Feed Settings</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="midPan">
                  <div class="connectionsBody windowHeight windowHeightMid passDetails">
                    <div class="bankDetailsTitle">Change Password</div>
                    <div class="bankcardTab">
                      
                      <div class="">
                        <div id="" class="">
                          <div class="profileForm bankForm">
                            <form method="post" class="xhr_form" action="api/change-password" id="change-password">
                              <?php echo csrf_field(); ?>
                              <div class="row">
                                <div class="col-md-12">
                                  <input type="hidden" name="user_id" value="<?php echo e(Session::get('userdata')['id']); ?>">
                                  <div class="form-group">
                                    <label for="cPassword">Current Password</label>
                                    <input type="password" class="form-control" id="cPassword" placeholder="" 
                                    name="old_password">
                                    <p class="text-danger" id="old_password_error"></p>
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label for="nPassword">New Password</label>
                                    <input type="password" class="form-control" id="nPassword" placeholder="" 
                                    name="new_password">
                                    <p class="text-danger" id="new_password_error"></p>
                                  </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                      <label for="renPassword">Re-type New Password</label>
                                      <input type="password" class="form-control" id="renPassword" placeholder=" " 
                                      name="cnf_password">
                                      <p class="text-danger" id="cnf_password_error"></p>
                                    </div>
                                </div>
                                
                              </div>
                              <div class="btn-Edit-save">
                                <button type="submit" class="btn btn-Edit btn-save" style="width: 158px;">Save Changes</button>
                              </div>
                            </form>
                          </div>
                        </div>
                        
                      </div>

                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/change_password.blade.php ENDPATH**/ ?>