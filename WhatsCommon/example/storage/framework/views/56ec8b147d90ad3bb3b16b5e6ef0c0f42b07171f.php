<ul class="list-unstyled mb-0 lifeEventMenu">
  <li class="lifeEventMenuLi lifeEventPersonal <?php echo e((!Request::get('type') || Request::get('type')==1) ? 'lifeEventMenuLiActive' : ''); ?> ">
    <a href="<?php echo e(url('create-lifeevent')); ?>" class="lifeEventMenuLink">
      <span class="lifeEventMenuText">Personal</span>
    </a>
  </li>
<?php if(Session::get('userdata')['user_age'] && Session::get('userdata')['user_age']>=18): ?>
  <li class="lifeEventMenuLi lifeEventDating <?php echo e(Request::get('type')==2 ? 'lifeEventMenuLiActive' : ''); ?>">
    <a href="<?php echo e(url('create-lifeevent/?type=2')); ?>" class="lifeEventMenuLink">
      <span class="lifeEventMenuText">Dating</span>
    </a>
  </li>
<?php endif; ?>
  <li class="lifeEventMenuLi lifeEventAdoption <?php echo e(Request::get('type')==3 ? 'lifeEventMenuLiActive' : ''); ?>">
    <a href="<?php echo e(url('create-lifeevent/?type=3')); ?>" class="lifeEventMenuLink">
      <span class="lifeEventMenuText">Adoption</span>
    </a>
  </li>
  <li class="lifeEventMenuLi lifeEventTravel <?php echo e(Request::get('type')==4 ? 'lifeEventMenuLiActive' : ''); ?>">
    <a href="<?php echo e(url('create-lifeevent/?type=4')); ?>" class="lifeEventMenuLink">
      <span class="lifeEventMenuText">Travel</span>
    </a>
  </li>
  <li class="lifeEventMenuLi lifeEventMilitary <?php echo e(Request::get('type')==5 ? 'lifeEventMenuLiActive' : ''); ?>">
    <a href="<?php echo e(url('create-lifeevent/?type=5')); ?>" class="lifeEventMenuLink">
      <span class="lifeEventMenuText">Military</span>
    </a>
  </li>
  <li class="lifeEventMenuLi lifeEventEducation <?php echo e(Request::get('type')==6 ? 'lifeEventMenuLiActive' : ''); ?>">
    <a href="<?php echo e(url('create-lifeevent/?type=6')); ?>" class="lifeEventMenuLink">
      <span class="lifeEventMenuText">Education</span>
    </a>
  </li>
  <li class="lifeEventMenuLi lifeEventWork <?php echo e(Request::get('type')==7 ? 'lifeEventMenuLiActive' : ''); ?>">
    <a href="<?php echo e(url('create-lifeevent/?type=7')); ?>" class="lifeEventMenuLink">
      <span class="lifeEventMenuText">Work</span>
    </a>
  </li>
  <li class="lifeEventMenuLi lifeEventPets <?php echo e(Request::get('type')==8 ? 'lifeEventMenuLiActive' : ''); ?>">
    <a href="<?php echo e(url('create-lifeevent/?type=8')); ?>" class="lifeEventMenuLink">
      <span class="lifeEventMenuText">Pets</span>
    </a>
  </li>
  <li class="lifeEventMenuLi lifeEventReverse">
    <a href="<?php echo e(url('create-lifeevent/?type=11')); ?>" class="lifeEventMenuLink ">
      <span class="lifeEventMenuText">Reverse Lookup</span>
    </a>
  </li>
  <li class="lifeEventMenuLi lifeEventL_F <?php echo e(Request::get('type')==9 ? 'lifeEventMenuLiActive' : ''); ?>">
    <a href="<?php echo e(url('create-lifeevent/?type=9')); ?>" class="lifeEventMenuLink ">
      <span class="lifeEventMenuText">Lost/Found</span>
    </a>
  </li>
  <li class="lifeEventMenuLi lifeEventDoppelganger <?php echo e(Request::get('type')==10 ? 'lifeEventMenuLiActive' : ''); ?>">
    <a href="#" class="lifeEventMenuLink" onclick="open_modal('coming-soon');">
      <span class="lifeEventMenuText">Doppelganger</span>
    </a>
  </li> 
  <li class="lifeEventMenuLi lifeEventDnaMacth">
    <a href="#" class="lifeEventMenuLink" onclick="open_modal('coming-soon');">
      <span class="lifeEventMenuText">DNA Match</span>
    </a>
  </li>
</ul><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/inc/life_event_left_menu.blade.php ENDPATH**/ ?>