<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="connectionsBody">
              <div class="position-relative connectionFlip userDetails">
                
                <!--closePan-->
                <div class="midPan windowHeight windowHeightMid">
                  <div class="connectionsRight">
                    <div class="connectionsAllDetails">
                          <div class="connectionsAllDetailsTop position-relative">
                            <div class="connectionsDetailsAvtarImg">
                              <img src="<?php echo e($connection->profile_photo_path ? $connection->profile_photo_path : 'new-design/img/profile_placeholder.jpg'); ?>" class="img-fluid" alt="">
                            </div>

                            <div class="connectionsDetailsInfo">
                              <div class="connectionsDetailsInfoName"><?php echo e($connection->firstName); ?> <?php echo e($connection->lastName); ?></div>
                              <div class="connectionsDetailsInfoEvents myEvenntsAll">
                                <div class="myEvennts">
                                  <?php if($connection->show_connection_list): ?>
                          <a style="all: unset;cursor: pointer;" href="<?php echo e($connection->id==$user_id ? url('connections') : url('other-connections/?u='.$connection->id)); ?>">
                            <span class="eventsNumbers"> <?php echo e($connection->connection_count); ?>  </span> <span class="eventsText">Connections</span>
                          </a>
                          <?php else: ?>
                          <span class="eventsNumbers"> <?php echo e($connection->connection_count); ?>  </span> <span class="eventsText">Connections</span>
                          <?php endif; ?>
                                </div>
                                <div>
                                  <span class="eventsNumbers"><?php echo e($connection->life_events_count); ?></span> <span class="eventsText">Life Events</span>
                                </div>
                                <div>
                                  <span class="eventsNumbers"><?php echo e($connection->matched_count); ?></span> <span class="eventsText">Matches</span>
                                </div>
                              </div>
                              <div class="connectionsDetailsInfoId"><?php echo e('@'); ?><?php echo e($connection->username); ?></div>
                              <div class="connectionsDetailsPlace"><?php echo e($connection->city_name); ?> <?php echo e(($connection->city_name && $connection->province_name) ? ',' : ''); ?> <?php echo e($connection->province_name); ?> <?php echo e(($connection->country_name && $connection->province_name) ? ',' : ''); ?> <?php echo e($connection->country_name); ?></div>
                              
                              <?php if($connection->bio): ?><div class="connectionsDetailsPlace connectionsDetailsUser"><?php echo e($connection->bio); ?></div><?php endif; ?>
                              <?php if($connection->is_connected==0 && $connection->is_pending_connection==0 && $feed_data): ?>
                      <a class="btn btn-following" onclick="execute('api/connection-request','user_id=<?php echo e($my_id); ?>&connect_user_id=<?php echo e($connection->id); ?>&_feed_id=<?php echo e($feed_data->_feed_id); ?>')">OK to Connect</a>
                      <?php elseif($connection->is_pending_connection==1 && $connection->request_from_user==$my_id && $connection->_request_id): ?>
                      <a class="btn btn-following" onclick="execute('api/cancel-request','user_id=<?php echo e($my_id); ?>&_request_id=<?php echo e($connection->_request_id); ?>')">Cancel Connect</a>
                      <?php elseif($connection->is_pending_connection==1 && $connection->request_from_user!=$my_id && $connection->_request_id): ?>
                      <a class="btn btn-following" onclick="execute('api/accept-reject-request','user_id=<?php echo e($my_id); ?>&_request_id=<?php echo e($connection->_request_id); ?>&request_status=1')">Accept Connect</a>
                      <?php endif; ?>
                      <a class="btn btn-following" onclick="execute('api/follow-unfollow','user_id=<?php echo e($my_id); ?>&followed_user=<?php echo e($connection->id); ?>')"><?php echo e($connection->is_following==1 ? 'Following' : 'Follow'); ?></a>
                      <?php if($connection->quick_blox_id): ?>
                      <a class="btn btn-following btn-message" href="<?php echo e(url('messages?qb='.$connection->quick_blox_id)); ?>">Message</a>
                      <?php endif; ?>
                            </div>
                            <?php if($connection->created_at): ?><div class="memberJoin">Member since <?php echo e(format_date($connection->created_at)); ?></div><?php endif; ?>
                          </div>
                      <?php if($connection->show_match_feed && count($feeds)>0): ?>
                          <div class="connectionsAllDetailsBody">
                            <div class="connectionsDetailsTitle text-center"><?php echo e($connection->firstName); ?>’s Personal Feed</div>
                        <?php $__currentLoopData = $feeds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $feed): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="userConnectedInfo">
                                <div class="connectedInfo">
                                    <div class="position-relative pr-0 feedImg">
                                        <div class=""><img src="<?php echo e($feed->user1_profile_photo ? $feed->user1_profile_photo : 'new-design/img/profile_placeholder.jpg'); ?>" class="img-fluid" alt=""></div>
                                        <div class=""><img src="<?php echo e($feed->user2_profile_photo ? $feed->user2_profile_photo : 'new-design/img/profile_placeholder.jpg'); ?>" class="img-fluid" alt=""></div>
                                        <img src="new-design/img/feedadd2.png" class="feedadd" alt="">
                                    </div>
                                    <div class="rightPanText">
                                      <p>
                                      <?php if($feed->user1_id==Session::get('userdata')['id']): ?>
                                        <a href="<?php echo e(url('my-profile')); ?>"> 
                                          <i>You </i> 
                                        </a> are <?php echo e($feed->feed_type); ?> with <a href="<?php echo e(url('user-profile/?user='.$feed->user2_id)); ?>"> 
                                          <i><?php echo e($feed->user2_firstName); ?> <?php echo e($feed->user2_lastName); ?> </i> 
                                        </a>

                                      <?php elseif($feed->user2_id==Session::get('userdata')['id']): ?>
                                        <a href="<?php echo e(url('user-profile/?user='.$feed->user1_id)); ?>"> 
                                          <i><?php echo e($feed->user1_firstName); ?> <?php echo e($feed->user1_lastName); ?> </i> 
                                        </a> is <?php echo e($feed->feed_type); ?> with <a href="<?php echo e(url('my-profile')); ?>"> 
                                          <i>You </i> 
                                        </a>

                                      <?php else: ?>
                                        <a href="<?php echo e(url('user-profile/?user='.$feed->user1_id)); ?>"> 
                                          <i><?php echo e($feed->user1_firstName); ?> <?php echo e($feed->user1_lastName); ?> </i> 
                                        </a> is <?php echo e($feed->feed_type); ?> with <a href="<?php echo e(url('user-profile/?user='.$feed->user2_id)); ?>"> 
                                          <i><?php echo e($feed->user2_firstName); ?> <?php echo e($feed->user2_lastName); ?> </i> 
                                        </a>


                                      <?php endif; ?>
                                          
                                      </p>
                                      <span>About <?php echo e(DateTime_Diff($feed->feed_created)); ?> ago</span>
                                    </div>
                                    <div class="greenCountFeed"><?php echo e($feed->feed_match_count); ?></div>
                                </div>
                            </div>
                            
                            <div class="userConnectDetailsAll">
                              <div class="userConnectDetails">
                                <div class="userConnectDetailsLeft">
                                <?php if($feed->feed_what): ?>
                                  <div class="userDetailsText">What: <span style="color: #91d639;"><?php echo e($feed->feed_event_type); ?></span></div>
                                  <div class="userDetailsText ml-5"><span><?php echo e($feed->feed_what); ?></span></div>
                                  
                                <?php endif; ?>
    <?php if($feed->feed_where): ?><div class="userDetailsText">Where: <span><?php echo e($feed->feed_where); ?></span></div><?php endif; ?>
    <?php if($feed->feed_when): ?><div class="userDetailsText">When: <span><?php echo e($feed->feed_when); ?></span></div><?php endif; ?>
    <?php if($feed->feed_keywords): ?><div class="userDetailsText">W5: <span><?php echo e($feed->feed_keywords); ?></span></div><?php endif; ?>
                                  
                                </div>
                                <div class="userConnectDetailsRight">
                                <?php if($feed->feed_images && count($feed->feed_images)>0): ?>
                                <?php
                                $counter = count($feed->feed_images);
                                $remaining = 0;
                                if($counter>3){
                                  $remaining = $counter-2;
                                }
                                ?>
                                  <div class="uploadPhotoHeading">Uploaded photos:</div>
                                  <div class="galleryViewAll">
                                  <?php if($counter==1): ?>
                                    <div class="galleryViewImg1">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-12 galleryViewCol">
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[0]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php elseif($counter==2): ?>  
                                    <div class="galleryViewImg2">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[0]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[1]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php elseif($counter==3): ?>     
                                    <div class="galleryViewImg3">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[0]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[1]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[2]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[2]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[2]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php else: ?>
                                    <div class="galleryViewImg3 galleryViewImg4">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[0]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[1]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                          <div class="galleryViewImg">
                                            <img src="new-design/img/feedfoto01.png" class="img-fluid" alt="">
                                            <div class="galleryViewImgMore"><?php echo e($remaining); ?>+</div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php endif; ?>
                                  </div>
                                <?php endif; ?>
                                  <div class="row text-center rightPanBtn">
                                    
                                    <a href="#" class="dismissButton" onclick="execute('api/hide-feed','user_id=<?php echo e(Session::get('userdata')['id']); ?>&_feed_id=<?php echo e($feed->_feed_id); ?>&request_from=my-profile');"><img src="new-design/img/dismissIcon.png" class="img-fluid" alt=""> Dismiss</a>
                                      
                                      
                                  </div>
                            </div>
                          </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </div>
                    <?php endif; ?>
                    </div>    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
    $(document).ready(function(){    
        if ($(window).width() < 951) {
        $('.windowHeightLeft').css('height',$(window).height()-153);
        $('.windowHeightMid').css('height',$(window).height()-90);
      }
      else {
        $('.windowHeightLeft').css('height',$(window).height()-150);
        $('.windowHeightMid').css('height',$(window).height()-150);
      }
    });
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/user_profile.blade.php ENDPATH**/ ?>