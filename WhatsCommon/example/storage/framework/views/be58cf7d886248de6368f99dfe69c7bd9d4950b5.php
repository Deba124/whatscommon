<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>What's Common</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <?php echo $__env->make('inc.css', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<link href="css/toastr.min.css" rel="stylesheet">
<style type="text/css">
.input-group input:focus ~ .floating-label, .input-group input:not(:focus):valid ~ .floating-label {
  left: 25%;
}
</style>
<body>
  <section id="login" class="signUpBg">
    <div class="container">
      <div class="AllLogin">
        <div class="loginForm" style="max-width: fit-content;">
          <div class="loginLogo">
            <img src="img/signUpLogo.png" class="img-fluid" alt="">
          </div>

          <div class="signUpHeading">Sign Up</div>
          <div class="loginText" style="color: #000;">Fill in your login information</div>

          <form action="" method="post" class="signUpForm" id="signUpForm" autocomplete="off">
            <?php echo csrf_field(); ?>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="inputText form-control" name="last_name" required/>
                  <span class="floating-label">Last Name</span>
                </div>
                <p id="last_name_error" class="text-danger"></p>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="inputText form-control" name="first_name" required/>
                  <span class="floating-label">First Name</span>
                </div>
                <p id="first_name_error" class="text-danger"></p>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="inputText form-control" name="mi" />
                  <span class="floating-label">MI (Optional)</span>
                </div>
                <p id="mi_error" class="text-danger"></p>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="email" name="email" class="inputText form-control" required/>
                  <span class="floating-label">Email Address</span>
                </div>
                <p id="email_error" class="text-danger"></p>
              </div>
              <div class="col-md-6">
                <div class="input-group mb-3">
  
    <select class="form-control " style="max-width: max-content;height: 54px;border-top-left-radius: 10px;border-bottom-left-radius: 10px;color: #3b71b9;" name="isd_code">
<?php if($countries): ?>
  <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <?php
  if(!$country->country_isd){
    continue;
  }
  ?>
      <option value="+<?php echo e($country->country_isd); ?>" >+<?php echo e($country->country_isd); ?></option>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>    
    </select>
  
                                  
                  <input type="number" class="inputText form-control" id="" name="phone" value="" style="border-top-right-radius: 10px;border-bottom-right-radius: 10px;">
                  <span class="floating-label">Phone Number</span>
                </div>
                <p id="phone_error" class="text-danger"></p>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="inputText form-control" id="username" name="username" required autocomplete="off"/>
                  <span class="floating-label">Username</span>
                </div>
                <p id="username_error" class="text-danger"></p>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="password" class="inputText form-control" required name="password" autocomplete="off" id="password" />
                  <span class="floating-label">Password</span>
                  
                  <img src="new-design/img/eye-open.png" id="password-img" class="img-fluid passwordVisiable" alt="" onclick="showHidePassword('password');">
                </div>
                <p id="password_error" class="text-danger"></p>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="password" class="inputText form-control" name="cnf_pass" required id="cnf_pass" />
                  <span class="floating-label">Re-type Password</span>
                  
                  <img src="new-design/img/eye-open.png" id="cnf_pass-img" class="img-fluid passwordVisiable" alt="" onclick="showHidePassword('cnf_pass');">
                </div>
                <p id="cnf_pass_error" class="text-danger"></p>
              </div>
            </div>
            <div class="form-group" id="responseDiv" style="display: none;"></div>
            <!-- <a href="signup-details" class="btn btn-proceed">Proceed</a> -->
            <button type="submit" class="btn btn-proceed" style="width: 100%;">Proceed</button>
            <a href="<?php echo e(url('/')); ?>" class="btn btn-proceed" style="width: 100%;background-color: #b5afac;">Cancel</a>
          </form>
        </div>
      </div>
    </div>
  </section>
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  <script src="js/toastr.min.js"></script>
  <script>

    $(document).ready(function(){
      
      toastr.options = {
        "closeButton"       : false,
        "debug"             : false,
        "newestOnTop"       : true,
        "progressBar"       : true,
        "positionClass"     : "toast-bottom-right",
        "preventDuplicates" : false,
        "onclick"           : null,
        "showDuration"      : "300",
        "hideDuration"      : "1000",
        "timeOut"           : "5000",
        "extendedTimeOut"   : "1000",
        "showEasing"        : "swing",
        "hideEasing"        : "linear",
        "showMethod"        : "fadeIn",
        "hideMethod"        : "fadeOut"
      };
      //$('#signUpForm').reset();
      //document.forms[0].reset();
const myTimeout = setTimeout(clearElements, 2000);
      console.log('here');
    });
function clearElements(){
  $('#username').val('');
  $('#password').val('');
}
    
  </script>
  <?php echo $__env->make('inc.script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script type="text/javascript">
$(document).on('submit','#signUpForm',function(e){
  e.preventDefault();
  var token = $('input[name="_token"]').attr('value');
  console.log(token);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token
    }
  });
  var action = site_url('register');
  $.ajax({
    type : "POST",
    data : new FormData(this),
    contentType: false,   
    cache: false,             
    processData:false, 
    url  : action,
    success : function(response){
      console.log(response);
      if(response.response_code==200){
        $('#responseDiv').show();
        $('#responseDiv').html('<p class="text-success">'+response.response_msg+'</p>');
        $(location).attr("href", response.redirect);
      }
    },error: function(jqXHR, textStatus, errorThrown){
    /*console.log(textStatus + ": " + jqXHR.status + " " + errorThrown);*/
    /*console.log(jqXHR);*/
    $("p.text-danger").each(function() {
      $(this).html('');
    });
    console.log(jqXHR.responseJSON);
    if(jqXHR.responseJSON.errors){
      var err = jqXHR.responseJSON.errors;
      $.each(err, function(key, value){

        if (err[key]) {
          $("#"+key+"_error").html(err[key]);
        }
        else{
          $("#"+key+"_error").html(''); 
        }
      });
    }
    $('#responseDiv').show();
    $('#responseDiv').html('<p class="text-danger">'+jqXHR.responseJSON.response_msg+'</p>');
  }
  });
});  
</script>
</body>

</html><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/signup.blade.php ENDPATH**/ ?>