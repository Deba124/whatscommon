<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                  <div class="leftSlidePan closePan">
                    <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                    <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                      <?php echo $__env->make('inc.life_event_left_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </div>
                  </div>
                  <div class="midPan">
                    <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                      <div class="row infoBodyRow">
                        <div class="col-md-5 infoBodyCol">
                            <div class="innerHome">
                              <a href="<?php echo e(url('home')); ?>"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                            </div>
                          <div class="feedbackImg">
                            <div class="personalImgAll">
                              <img src="new-design/img/datingImg.png" class="img-fluid" alt="">
                              <div class="personalImgTitle">Dating</div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7 infoBodyCol eventCol midHightControl">
                  <div class="feedbackRight windowHeight windowHeightMid" style="height: 445px;">


                            <form class="xhr_form" method="post" action="api/life-event-upload" id="life-event-upload">
                            <?php echo csrf_field(); ?>
                              <div class="eventColHeading">What</div><img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                              <input type="hidden" name="type_id" value="2">
                              <input type="hidden" name="user_id" value="<?php echo e(Session::get('userdata')['id']); ?>">
                                <div class="whatForm whatFormDationg">
                                  <div class="form-group datingSpecific">
                                    <label for="">What</label>
                                    <select class="form-control selectCenter" id="whatReason" name="sub_category_id">
                    <?php if($what): ?>
                    	<?php $__currentLoopData = $what; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $w): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value="<?php echo e($w->id); ?>"><?php echo e($w->sub_cat_type_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                      
                                    </select>
                                  </div>
                                  <div class="form-group datingFilterBtn timeOptionSet">
                                    <div class="form-control timeOptionOnly enterFor activeTimeOption">Enter (you)</div>
                                    <div class="form-control timeOptionOnly searchFor">Search (for)</div>
                                    <input type="hidden" name="search_for" value="0" id="search_for" />
                                  </div>
                                  <div class="form-group text-center" style="color: #3b71b9;border: 2px solid #3b71b9;background: #f8f8f8;">
                                    Enter as MUCH or as little as you want. Enter <br>what you want others to search about you!
                                  </div>
                                  <div class=" vehicleForm" style="padding: 0px 30px;">
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Gender</label>
                                          <select class="form-control formGender" name="gender">
                                            <option value="" hidden>Select</option>
                    <?php if($genders): ?>
                    	<?php $__currentLoopData = $genders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gender): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($gender->gender_name); ?>"><?php echo e($gender->gender_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Age</label>
                                          <!-- <input type="number" class="form-control formAge" name="age"> -->
                                          <select class="form-control formAge" name="age">
                                            <option value="" hidden="">Select</option>
                                <?php for($i=18;$i<100;$i++): ?>
                                            <option value="<?php echo e($i); ?>"><?php echo e($i); ?><?php echo e($i==18?'+':''); ?></option>
                                <?php endfor; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      	<div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Height</label>
                                            <div class="height_weight">
                                              <div class="height_weight_Only">
                                              	<input type="number" name="" class="form-control formHeight" name="height" id="height" value="0">
                                                
                                                
                                              </div>
                                              <div class="form-group timeOptionSet">
                                                <div class="form-control timeOptionOnly activeTimeOption" id="height_measure_in">in</div>
                                                <div class="form-control timeOptionOnly" id="height_measure_cm" style="border-top-left-radius: 0 !important;border-bottom-left-radius: 0 !important;">cm</div>
                                                <input type="hidden" name="height_measure" id="height_measure" value="in">
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Height General</label>
                                            <select class="form-control formHeight" name="height_general">
                                              <option value="" hidden>Select</option>
                	<?php if($height_general): ?>
                    	<?php $__currentLoopData = $height_general; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($hg->hg_name); ?>"><?php echo e($hg->hg_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Weight</label>
                                            <div class="height_weight">
                                              <div class="height_weight_Only">
                                              	<input type="number" class="form-control formWeight" name="weight" id="weight" value="0">
                                                
                                              </div>
                                              <div class="form-group timeOptionSet">
                                                <div class="form-control timeOptionOnly activeTimeOption" id="weight_measure_kg">kg</div>
                                                <div class="form-control timeOptionOnly" id="weight_measure_lbs" style="border-top-left-radius: 0 !important;border-bottom-left-radius: 0 !important;">lbs</div>
                                                <input type="hidden" name="weight_measure" id="weight_measure" value="kg">
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Weight General</label>
                                            <select class="form-control formWeight" name="weight_general">
                                              <option value="" hidden>Select</option>
                    <?php if($weight_general): ?>
                    	<?php $__currentLoopData = $weight_general; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $wg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($wg->wg_name); ?>"><?php echo e($wg->wg_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Ethnicity</label>
                                          <select class="form-control formEthnicity" name="ethnicity">
                                            <option value="" hidden>Select</option>
                    <?php if($ethnicities): ?>
                    	<?php $__currentLoopData = $ethnicities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ethnicity): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($ethnicity->ethnicity_name); ?>"><?php echo e($ethnicity->ethnicity_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Race</label>
                                          <select class="form-control formRace" name="race">
                                            <option value="" hidden>Select</option>
                    <?php if($races): ?>
                    	<?php $__currentLoopData = $races; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $race): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($race->race_name); ?>"><?php echo e($race->race_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Nationality</label>
                                          <select class="form-control formNationality" name="nationality">
                                            <option value="" hidden>Select</option>
                    <?php if($nationalities): ?>
                    	<?php $__currentLoopData = $nationalities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($country->country_name); ?>"><?php echo e($country->country_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Facial Hair</label>
                                            <select class="form-control formFacialHair" name="facial_hair">
                                              <option value="" hidden>Select</option>
                    <?php if($facial_hairs): ?>
                    	<?php $__currentLoopData = $facial_hairs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fh): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($fh->fh_name); ?>"><?php echo e($fh->fh_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Orientation</label>
                                          <select class="form-control formOrientation" name="orientation">
                                            <option value="" hidden>Select</option>
                    <?php if($orientations): ?>
                    	<?php $__currentLoopData = $orientations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $orientation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($orientation->orientation_name); ?>"><?php echo e($orientation->orientation_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Body Style</label>
                                          <select class="form-control formBodyStyle" name="body_style">
                                            <option value="" hidden>Select</option>
                    <?php if($body_styles): ?>
                    	<?php $__currentLoopData = $body_styles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($bs->bs_name); ?>"><?php echo e($bs->bs_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Body Hair</label>
                                            <select class="form-control formBodyHair" name="body_hair">
                                              <option value="" hidden>Select</option>
                    <?php if($body_hairs): ?>
                    	<?php $__currentLoopData = $body_hairs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bh): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($bh->bh_name); ?>"><?php echo e($bh->bh_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Skin Tone</label>
                                          <select class="form-control formSkinTone" name="skin_tonne">
                                            <option value="" hidden>Select</option>
                    <?php if($skin_tonnes): ?>
                    	<?php $__currentLoopData = $skin_tonnes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $st): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($st->st_name); ?>"><?php echo e($st->st_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Glasses</label>
                                            <select class="form-control formSGlasses" name="glasses">
                                              <option value="" hidden>Select</option>
                                              <option value="Yes">Yes</option>
                                              <option value="No">No</option>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Hair Color</label>
                                          <select class="form-control formHairColor" name="hair_color">
                                            <option value="" hidden>Select</option>
                    <?php if($hair_colors): ?>
                    	<?php $__currentLoopData = $hair_colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($hc->hc_name); ?>"><?php echo e($hc->hc_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Eye Color</label>
                                          <select class="form-control formEyeColor" name="eye_color">
                                            <option value="" hidden>Select</option>
                    <?php if($eye_colors): ?>
                    	<?php $__currentLoopData = $eye_colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ec): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($ec->ec_name); ?>"><?php echo e($ec->ec_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Party</label>
                                            <select class="form-control formParty" name="party">
                                              <option value="" hidden>Select</option>
                                              <option value="Yes">Yes</option>
                                              <option value="No">No</option>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat" name="smoke">
                                          <div class="form-group">
                                            <label for="">Smoke</label>
                                            <select class="form-control formSmoke">
                                              <option value="" hidden>Select</option>
                                              <option value="Yes">Yes</option>
                                              <option value="No">No</option>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Drink</label>
                                            <select class="form-control formDrink" name="drink">
                                              <option value="" hidden>Select</option>
                                              <option value="Yes">Yes</option>
                                              <option value="No">No</option>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Tattoo</label>
                                            <select class="form-control formTatto" name="tattoo">
                                              <option value="" hidden>Select</option>
                                              <option value="Yes">Yes</option>
                                              <option value="No">No</option>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for=""># Tattoos</label>
                                            <select class="form-control formTatto" name="tattoo_type">
                                              <option value="" hidden>Select</option>
                    <?php if($tattoos): ?>
                    	<?php $__currentLoopData = $tattoos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tattoo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($tattoo->tattoo_name); ?>"><?php echo e($tattoo->tattoo_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Favorite Food</label>
                                            <select class="form-control formFood" name="fav_food">
                                              <option value="" hidden>Select</option>
                    <?php if($foods): ?>
                    	<?php $__currentLoopData = $foods; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $food): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($food->food_name); ?>"><?php echo e($food->food_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Food Type</label>
                                            <select class="form-control formFoodType" name="food_type">
                                              <option value="" hidden>Select</option>
                    <?php if($food_types): ?>
                    	<?php $__currentLoopData = $food_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ft): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($ft->ft_name); ?>"><?php echo e($ft->ft_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Sport</label>
                                            <select class="form-control formSport" name="sport">
                                              <option value="" hidden>Select</option>
                    <?php if($sports): ?>
                    	<?php $__currentLoopData = $sports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sport): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($sport->sport_name); ?>"><?php echo e($sport->sport_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Favorite Team</label>
                                            <input type="text" class="form-control formFavoriteTeam" name="fav_team">
                                            
                                          </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Political Affiliation</label>
                                          <select class="form-control formPoliticalAffiliation" name="political">
                                            <option value="" hidden>Select</option>
                    <?php if($politicals): ?>
                    	<?php $__currentLoopData = $politicals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $political): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($political->political_name); ?>"><?php echo e($political->political_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Religion</label>
                                          <select class="form-control formReligion" name="religion">
                                            <option value="" hidden>Select</option>
                    <?php if($religions): ?>
                    	<?php $__currentLoopData = $religions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $religion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($religion->religion_name); ?>"><?php echo e($religion->religion_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Zodiac Sign</label>
                                            <select class="form-control formZodiacSign" name="zodiac_sign">
                                              <option value="" hidden>Select</option>
                    <?php if($zodiac_signs): ?>
                    	<?php $__currentLoopData = $zodiac_signs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $zodiac): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($zodiac->zodiac_name); ?>"><?php echo e($zodiac->zodiac_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Birth Stone</label>
                                            <select class="form-control formBarthStone" name="birth_stone">
                                              <option value="" hidden>Select</option>
                    <?php if($zodiac_signs): ?>
                    	<?php $__currentLoopData = $zodiac_signs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $zodiac): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($zodiac->zodiac_name); ?>"><?php echo e($zodiac->zodiac_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Affiliations</label>
                                            <select class="form-control formAffiliation" name="affiliation">
                                              <option value="" hidden>Select</option>
                    <?php if($affiliations): ?>
                    	<?php $__currentLoopData = $affiliations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $affiliation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($affiliation->affiliation_name); ?>"><?php echo e($affiliation->affiliation_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Career</label>
                                            <select class="form-control formCareer" name="career">
                                              <option value="" hidden>Select</option>
                    <?php if($occupations): ?>
                    	<?php $__currentLoopData = $occupations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $occupation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($occupation->occupation_name); ?>"><?php echo e($occupation->occupation_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Financial</label>
                                            <select class="form-control formFinancial" name="financial">
                                              <option value="" hidden>Select</option>
                    <?php if($financials): ?>
                    	<?php $__currentLoopData = $financials; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($fs->fs_name); ?>"><?php echo e($fs->fs_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Relationship</label>
                                            <select class="form-control formRelationship" name="relationship">
                                              <option value="" hidden>Select</option>
                    <?php if($relationships): ?>
                    	<?php $__currentLoopData = $relationships; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $relationship): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($relationship); ?>"><?php echo e($relationship); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Age (Range)</label>
                                          <select class="form-control formAge" name="age_range">
                                            <option value="" hidden>Select</option>
                    <?php if($age_ranges): ?>
                    	<?php $__currentLoopData = $age_ranges; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $range): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($range->range_name); ?>"><?php echo e($range->range_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Loking For</label>
                                          <select class="form-control formGender" name="looking_for">
                                            <option value="" hidden>Select</option>
                    <?php if($genders): ?>
                    	<?php $__currentLoopData = $genders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gender): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($gender->gender_name); ?>"><?php echo e($gender->gender_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="interestsSet">

                                          <div class="form-group interestsPicker">
                                            <label for="">Interests</label>
                                            <select name="basic" id="ex-multiselect" multiple name="interests">
                                              <option>Select</option>
                    <?php if($interests): ?>
                    	<?php $__currentLoopData = $interests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $interest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($interest->interest_name); ?>"><?php echo e($interest->interest_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                            </select>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="eventColHeading">Where </div>
                                <div class="whatForm whereFrom">
                                  <div class="form-group">
                                    <label for="">Country</label>
                                    <select class="form-control formCountry" name="country" id="country">
                              <option value="" hidden>Select</option>
            <?php if($countries): ?>
              <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($country->country_name); ?>" data-id="<?php echo e($country->_country_id); ?>"><?php echo e($country->country_name); ?></option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
                              
                            </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">State/County/Province</label>
                                    <select class="form-control formState" name="state" id="province">
                                      <option value="" hidden>Select</option>
                                      
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">City</label>
                                    <select class="form-control formCity" name="city" id="city">
                                      <option value="" hidden>Select</option>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Radius (City)</label>
                                    
                                    <select class="form-control formAddress" id="" name="radius">
                                        <option>Select</option>
                  <?php if($radius): ?>
                      <?php $__currentLoopData = $radius; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option option="<?php echo e($r->radius_value); ?>"><?php echo e($r->radius_value); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                      </select>
                                  </div>
                                </div>
                                <div class="eventColHeading">When </div>
                                <div class="whatForm whereFrom">
                                  <div class="whenTitleText">
                                    <div class="whenTitle">Availability</div>
                                    <div class="whenText">Let people know when ready to meet</div>
                                  </div>
                                  <div class="birthdayDate timeframeSet">
                                    <div class="form-group birthdayMonth">
                                      <label for="number">Timeframe</label>
                                      <select class="form-control" id="" name="timeframe">
                                        <option value="" hidden>Select</option>
                	<?php if($availabilities): ?>
                    	<?php $__currentLoopData = $availabilities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $availability): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($availability->availability_name); ?>"><?php echo e($availability->availability_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayYear">
                                      <label for="number">&nbsp;</label>
                                      <select class="form-control" id="" name="time_month">
                                        <option value="" hidden>Select</option>
                                        <option value="January">January</option>
                                        <option value="February">February</option>
                                        <option value="March">March</option>
                                        <option value="April">April</option>
                                        <option value="May">May</option>
                                        <option value="June">June</option>
                                        <option value="July">July</option>
                                        <option value="August">August</option>
                                        <option value="September">September</option>
                                        <option value="October">October</option>
                                        <option value="November">November</option>
                                        <option value="December">December</option>
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                  </div>
                                </div>
                                <div class="eventColHeading">Who, What, Where, When, Why </div>
                                <div class="whatForm whereFrom">
                                  
                                  	<div class="myAllEvents" id="keywords_div"></div>
			                        <input type="hidden" id="keywords" value="" name="event_keywords">
			                        <div class="input-group lifeEvent">
			                          <input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
			                          <div class="input-group-append">
			                            <a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
			                           </div>
			                        </div>
                                  	<div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                                </div>
                                <?php echo $__env->make('inc.life_event_img_upload', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                               

                                
                              </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
 <!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Dating</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Dating</label>
            <p>This section will ask specific categories for your dating preference. Dating life event has two(2) options: either enter your details for others to search for you or search for someone by entering specific information you like.</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>This is where your dating findings will start. Just enter the specific location, and everything will follow. Match-making will happen when matched with other users using your preferred radius.</p>
          </div>
          <div class="form-group">
            <label>WHEN:</label>
            <p>Availability is essential to start building and working with your relationship. Just enter your availability timeframe/month.</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).ready(function(){
	$(".enterFor").click(function(){
        $(".enterFor").addClass("activeTimeOption");
        $(".searchFor").removeClass("activeTimeOption");
        $('#search_for').val(0);
    });
    $(".searchFor").click(function(){
        $(".searchFor").addClass("activeTimeOption");
        $(".enterFor").removeClass("activeTimeOption");
        $('#search_for').val(1);
    });
});
$(document).ready(function(){
  $("#weight_measure_kg").click(function(){
    $("#weight_measure_kg").addClass("activeTimeOption");
    $("#weight_measure_lbs").removeClass("activeTimeOption");
    $('#weight_measure').val('kg');
    var weight = $('#weight').val();
    var changed_weight = (weight*0.453592).toFixed(2);
    $('#weight').val(changed_weight);
  });
  $("#weight_measure_lbs").click(function(){
    $("#weight_measure_lbs").addClass("activeTimeOption");
    $("#weight_measure_kg").removeClass("activeTimeOption");
    $('#weight_measure').val('lbs');
    var weight = $('#weight').val();
    var changed_weight = (weight*2.20462).toFixed(2);
    $('#weight').val(changed_weight);
  });
  $("#height_measure_in").click(function(){
    $("#height_measure_in").addClass("activeTimeOption");
    $("#height_measure_cm").removeClass("activeTimeOption");
    $('#height_measure').val('in');
    var height = $('#height').val();
    var changed_height = (height*0.393701).toFixed(2);
    $('#height').val(changed_height);
  });
  $("#height_measure_cm").click(function(){
    $("#height_measure_cm").addClass("activeTimeOption");
    $("#height_measure_in").removeClass("activeTimeOption");
    $('#height_measure').val('cm');
    var height = $('#height').val();
    var changed_height = (height*2.54).toFixed(2);
    $('#height').val(changed_height);
  });
});

$("#whatReason").change(function(){
    if($(this).val() == "5") {
        $('.whatReason1').addClass('removedWhat');
        $('.whatReason2').removeClass('removedWhat');
    } else {
        $('.whatReason2').addClass('removedWhat');
        $('.whatReason1').removeClass('removedWhat');
     }
});
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="" hidden="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/life_events/dating.blade.php ENDPATH**/ ?>