<div class="matchHolder matchHolderShadow tagSearchFilter">
  <div class="tagSearchFilterAll">
  <?php if($country): ?>
    <div class="tagSearchFilterOnly">
      <img src="new-design/img/formCountry.png" class="img-fluid mr-2" alt=""> <?php echo e($country); ?>

      <img src="new-design/img/removeField.png" class="img-fluid tagFilterremove" alt="">
    </div>
  <?php endif; ?>
  <?php if($state): ?>
    <div class="tagSearchFilterOnly">
      <img src="new-design/img/formState.png" class="img-fluid mr-2" alt=""> <?php echo e($state); ?>

      <img src="new-design/img/removeField.png" class="img-fluid tagFilterremove" alt="">
    </div>
  <?php endif; ?>
  <?php if($tag_number): ?>
    <div class="tagSearchFilterOnly">
      <img src="new-design/img/formTag.png" class="img-fluid mr-2" alt=""> <?php echo e($tag_number); ?>

      <img src="new-design/img/removeField.png" class="img-fluid tagFilterremove" alt="">
    </div>
  <?php endif; ?>
  </div>
</div>

<div class="tagSearchTitle">All results (<?php echo e(count($events)); ?>)</div>
<?php if($events): ?>
<?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="matchHolder matchHolderShadow">
  <div class="connectionsAllDetailsTop">
    <div class="connectionsDetailsAvtarImg">
      <img src="<?php echo e($event->profile_photo_path ? $event->profile_photo_path : url('new-design/img/profile_placeholder.jpg')); ?>" class="img-fluid" alt="">
    </div>

    <div class="connectionsDetailsInfo">
      <div class="connectionsDetailsInfoName" style="color:#000;"><?php echo e($event->firstName); ?> <?php echo e($event->lastName); ?></div>
      <div class="connectionsDetailsInfoEvents myEvenntsAll">
        <div class="myEvennts mt-1">
          <span class="eventsText" style="font-weight: 600;">In Common:</span>
        </div>
      </div>
    <?php if($country == $event->event_country): ?>
      <div class="reverseLookuPlace"><?php echo e($event->event_country); ?></div>
    <?php endif; ?>
    <?php if($state == $event->event_state): ?>
      <div class="reverseLookuPlace tagSearchState"><?php echo e($event->event_state); ?></div>
    <?php endif; ?>
      <div class="reverseLookuPlace tagSearchTag"><?php echo e($event->event_tag_number); ?></div>
      <div class="reverseLookupBtn mt-3">
        <a class="btn btn-following" href="<?php echo e(url('user-profile/?user='.$event->id)); ?>">Visit</a>
    <?php if($event->quick_blox_id): ?>
        <a class="btn btn-following btn-message" href="<?php echo e(url('messages/?qb='.$event->quick_blox_id)); ?>">Message</a>
    <?php endif; ?>
      </div>
    </div>

    <!-- <div class="greenCountFeed">4</div> -->
  </div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php else: ?>
<div class="matchHolder matchHolderShadow">
  <div class="connectionsAllDetailsTop">
    <div class="connectionsDetailsInfo">
      <div class="connectionsDetailsInfoName" style="color:#000;">No Results Found</div>
    </div>
  </div>
</div>
<?php endif; ?><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/xhr/search_tag.blade.php ENDPATH**/ ?>