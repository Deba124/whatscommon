 <!-- Modal starts -->       
<div class="modal fade" id="edit-testimonial-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content" style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Update Testimonial</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="edit-testimonial">
          <div class="form-group" id="submit_status"></div>
          <?php echo csrf_field(); ?>
          <input type="hidden" name="_testimonial_id" value="<?php echo e($testimonial_data->_testimonial_id); ?>">
          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
                <label>Name <span class="text-danger">*</span></label>
                <input type="text" name="testimonial_username" class="form-control" value="<?php echo e($testimonial_data->testimonial_username); ?>">
                <p class="text-danger" id="testimonial_username_error"></p>
              </div>
              <div class="form-group">
                <label>Address <span class="text-danger">*</span></label>
                <input type="text" name="testimonial_useraddress" class="form-control" value="<?php echo e($testimonial_data->testimonial_useraddress); ?>">
                <p class="text-danger" id="testimonial_useraddress_error"></p>
              </div>
              <div class="form-group">
                <label>Text <span class="text-danger">*</span></label>
                <textarea class="form-control" name="testimonial_text" rows="5"><?php echo e($testimonial_data->testimonial_text); ?></textarea>
                <p class="text-danger" id="testimonial_text_error"></p>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Image </label>
                <input type="file" name="testimonial_userimage" class="form-control dropify" data-default-file="<?php echo e($testimonial_data->testimonial_userimage ? url($testimonial_data->testimonial_userimage) : ''); ?>">
                <p class="text-danger" id="testimonial_userimage_error"></p>
              </div>
            </div>
          </div>
          
          
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends --><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/xhr/edit_testimonial_modal.blade.php ENDPATH**/ ?>