<?php $__env->startSection('title', 'Notifications'); ?>
<?php echo $__env->make('admin.inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('admin.inc.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Notifications</h4>
      <div class="row" style="margin-bottom: 15px;">
        <div class="col-9"></div>
        
        <div class="col-3 text-right">
          <a class="btn btn-secondary" href="javascript:void" onclick="open_modal('add-notification')"><i class="fa fa-plus-circle text-success"></i> Add Notification</a>
        </div>
      </div>
      <div class="row">
  
        <div class="col-12">
          <table id="notifications-list" class="table table-condensed table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Notification</th>
                <th>User</th>
                <th>Date</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
<?php if(isset($notifications)): ?>
  <?php
    $i = 1
  ?>
  <?php $__currentLoopData = $notifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
              <td><?php echo e($i++); ?></td>
              <td>
                <p>
                  <b>Title : </b><?php echo e($nt->notification_title); ?>

                </p>
                <p><b>Content : </b><?php echo e($nt->notification_content); ?></p>
              </td>
              <td>
                <p><b>User : </b> <?php echo e($nt->firstName); ?> <?php echo e($nt->lastName); ?></p>
                <p><b>Device : </b><?php echo e(ucwords($nt->device_type)); ?></p>
              </td>
              <td>
                <span id="<?php echo e($nt->_notification_id); ?>"><script>changeDate('<?php echo e($nt->notification_created); ?>','<?php echo e($nt->_notification_id); ?>');</script></span>
                <!-- <?php echo e(formatDate($nt->notification_created)); ?> -->
              </td>
              <td>
                <?php if($nt->notification_status=='success'): ?> 
                <span class="badge badge-success">Success</span>
                <?php else: ?>
                <span class="badge badge-danger">Failed</span>
                <?php endif; ?>
              </td>
            </tr>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php else: ?>
    <tr><td colspan="5" class="text-center"><b>No Notifications Available</b></td></tr>
<?php endif; ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $__env->make('admin.inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!-- content-wrapper ends -->
<script type="text/javascript">
$(document).ready(function() {  

  dataTable = $('#notifications-list').DataTable( {
    dom: 'lBfrtip',
    responsive: true,
    ordering : true,
    lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
    pageLength: 10,
    buttons: [
      {
          extend: 'print',
          text: '<button class="btn btn-primary btn-sm" ><i class="fa fa-print fa-x5"> Print</i></button>',
          autoPrint: false,
          exportOptions: {
              columns: [ 0,1,2,3,4 ]
          }
      },
      {
          extend: 'excelHtml5',
          text: '<button class="btn btn-success btn-sm"><i class="fa fa-file-excel-o fa-x5"> Excel</i></button>',
          exportOptions: {
              columns: [ 0,1,2,3,4 ]
          }
      },
      {
          extend: 'pdfHtml5',
          text: '<button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o fa-x5"> PDF</i></button>',
          exportOptions: {
              columns:[ 0,1,2,3,4 ]
          }
      }
    ],
    "processing": true,
    "serverSide": false,
  });
}); 
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/notifications.blade.php ENDPATH**/ ?>