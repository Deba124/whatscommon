 <!-- Modal starts -->       
<div class="modal fade" id="add-game-team-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content" style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Add Team/Group</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="add-game-team">
          <div class="form-group" id="submit_status"></div>
          <?php echo csrf_field(); ?>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Game/Activity <span class="text-danger">*</span></label>
                <select class="form-control" name="team_gameid">
                  <option value="" hidden="">Select Game/Activity</option>
            <?php if($games): ?>
              <?php $__currentLoopData = $games; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $game): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option value="<?php echo e($game->_game_id); ?>"><?php echo e($game->game_name); ?></option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>

                </select>
                <p class="text-danger" id="game_name_error"></p>
              </div>
              <div class="form-group">
                <label>Team/Group Name <span class="text-danger">*</span></label>
                <input type="text" name="game_name" class="form-control">
                <p class="text-danger" id="game_name_error"></p>
              </div>
            </div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends --><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/xhr/add_game_team_modal.blade.php ENDPATH**/ ?>