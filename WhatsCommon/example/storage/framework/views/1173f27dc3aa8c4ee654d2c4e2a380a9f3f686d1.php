 <!-- Modal starts -->       
<div class="modal fade" id="edit-event-category-link-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Update Category Video Link</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="edit-event-category-link">
          <div class="form-group" id="submit_status"></div>
          <?php echo csrf_field(); ?>
          <input type="hidden" name="id" value="<?php echo e($category_data->id); ?>">
          <div class="form-group">
            <label>Video Link URL<span class="text-danger">*</span></label>
            <input type="text" name="event_video_link" class="form-control" value="<?php echo e($category_data->event_video_link); ?>">
            <p class="text-danger" id="event_video_link_error"></p>
          </div>
          
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends --><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/xhr/edit_event_category_link_modal.blade.php ENDPATH**/ ?>