<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<style type="text/css">
.priceRangeOnlyActive{
  /*border: 1px solid #3b71b9;*/
  border: 1px solid #fff;
    background: #3b71b9;
    color: #fff;
}
</style>
<div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                <div class="leftSlidePan <?php echo e(Request::get('menu')==1 ? '' : 'closePan'); ?>">
                  <a href="#" class="panelslideOpenButton <?php echo e(Request::get('menu')==1 ? 'panelslideCloseButton' : ''); ?>"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                    <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                      <div class="settingLeftTitle">Information</div>
                      <ul class="settingMenu mb-0 list-unstyled">
                        <li class="settingMenuLi">
                          <a class="settingMenu" href="<?php echo e(url('feedback')); ?>"><img src="<?php echo e(url('new-design/img/feedback.png')); ?>" class="img-fluid menuIcon" alt="">Feedback</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu" href="<?php echo e(url('contact-us')); ?>"><img src="<?php echo e(url('new-design/img/contact.png')); ?>" class="img-fluid menuIcon" alt="">Contact Us</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu " href="<?php echo e(url('about-us')); ?>"><img src="<?php echo e(url('new-design/img/about.png')); ?>" class="img-fluid menuIcon" alt="">About Us</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu " href="<?php echo e(url('premium-plans')); ?>"><img src="<?php echo e(url('new-design/img/premium.png')); ?>" class="img-fluid menuIcon" alt="">WC Premium</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu settingMenuActive" href="<?php echo e(url('donate')); ?>"><img src="<?php echo e(url('new-design/img/donate-a.png')); ?>" class="img-fluid menuIcon" alt="">Voluntary Gift</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu " style="color: #ed1c24;" href="<?php echo e(url('logout')); ?>"><img src="<?php echo e(url('new-design/img/logout.png')); ?>" class="img-fluid menuIcon" alt="">Log Out</a>
                        </li>
                      </ul>
                    </div>
                    <!-- <a href="<?php echo e(url('logout')); ?>" class="logOut" style="margin-top: -40px;"><img src="<?php echo e(url('new-design/img/logout.png')); ?>" class="img-fluid menuIcon" alt=""> Log Out</a> -->
                </div>
                <div class="midPan">
                  <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                    <div class="row infoBodyRow">
                      <div class="col-md-5 infoBodyCol">
                          <div class="innerHome">
                            <a href="<?php echo e(url('home')); ?>"><img src="<?php echo e(url('new-design/img/home.png')); ?>" class="img-fluid" alt=""></a>
                          </div>
                        <div class="feedbackImg">
                          <img src="<?php echo e(url('new-design/img/donateImg.png')); ?>" class="img-fluid" alt="">
                        </div>
                      </div>
                      <div class="col-md-7 infoBodyCol feedbackColFixed midHightControl">
                        <div class="feedbackRight donteCol windowHeight windowHeightMid">
                          <div class="feedbackTitle"><span>Voluntary Gift</span></div>

                          <form class="donateForm xhr_form" method="post" action="donate-form" id="donate-form">
                            <?php echo csrf_field(); ?>
                            <div class="donateFormTitle">Choose amount</div>
                            <div class="feedbackTitle2">Voluntary Gifts help keep cost low. Thank YOU!</div>
                            <div class="donateFormOnly">
                              <div class="form-group">
                                <select class="form-control" id="sel1" name="currency_id">
                      <?php if($currencies): ?>
                        <?php $__currentLoopData = $currencies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $currency): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($currency->_currency_id); ?>" <?php echo e(($currency_data && $currency_data->_currency_id == $currency->_currency_id) ? 'selected' : ''); ?> data-amounts="<?php echo e($currency->donation_amounts); ?>" data-symbol="<?php echo e($currency->currency_symbol); ?>"><?php echo e($currency->currency_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>
                            
                                </select>
                                <p id="currency_id_error" class="text-danger"></p>
                              </div>
                              <input type="hidden" name="donation_amount" id="donation_amount" value="">
                              <div class="priceRange">
                                <div class="row priceRangeRow" id="amounts_div">
              <?php if($currencies): ?>
                <?php
                  if($currency_data){
                    $currency_symbol = $currency_data->currency_symbol;
                    $amounts = $currency_data->donation_amounts ? $currency_data->donation_amounts : [];
                  }else{
                    $currency_symbol = $currencies[0]->currency_symbol;
                    $amounts = $currencies[0]->donation_amounts ? $currencies[0]->donation_amounts : [];
                  }
                  
                ?>
                <?php if($amounts): ?>
                  <?php $__currentLoopData = $amounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $amount): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <div class="col-md-4 priceRangeCol">
                                    <div class="priceRangeOnly" id="<?php echo e($amount->_da_id); ?>" title="<?php echo e($amount->da_amount); ?>"><span><?php echo $currency_symbol; ?> </span> <?php echo e($amount->da_amount); ?></div>
                                  </div>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
              <?php endif; ?>
                                  <p id="donation_amount_error" class="text-danger"></p>
                                  <div class="col-md-12 priceRangeCol">
                                    <div class="priceRangeOnly" id="custom" title="custom">Custom</div>
                                  </div>
                                </div>
                                <div class="row priceRangeRow" id="custom_div" style="display: none;">
                                  <div class="col-md-12 priceRangeCol">
                                    <input type="text" name="custom_amount" id="custom_amount" class="form-control" placeholder="Enter Custom Amount">
                                    <p id="custom_amount_error" class="text-danger"></p>
                                  </div>
                                </div>
                                <div class="row priceRangeRow">
                                  <div class="col-md-12 priceRangeCol">
                                    <div class="btn-Edit-save">
                                      <button class="btn btn-Edit btn-save" type="submit">Next</button>
                                      <!-- <div id="smart-button-container">
      <div style="text-align: center;">
        <div id="paypal-button-container"></div>
      </div>
    </div> -->
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </form>




  <script src="https://www.paypal.com/sdk/js?client-id=AcwL9AXx5h4aZ8VnStkVAo6DxjXtkiNg11f40SZy7TBx_Dy0s79rJc4xK87QnlfY_VDQYn6Keua6rOJ-&disable-funding=card&currency=<?php echo e($currency_data ? $currency_data->currency_shortname : 'USD'); ?>" data-sdk-integration-source="button-factory"></script>
                         
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script type="text/javascript">
$(document).ready(function(){
  $('#smart-button-container #description').val('WhatsCommon Donations');
<?php if(Request::get('amount')): ?>
$('#donation_amount').val('custom');
$('#amount').val('<?php echo e(Request::get('amount')); ?>');
$('#custom_amount').val('<?php echo e(Request::get('amount')); ?>');
$('#custom_div').show();
$(".priceRangeOnly").each(function() {
  $(this).removeClass('priceRangeOnlyActive');
});
$('#custom').addClass('priceRangeOnlyActive');
<?php endif; ?>
<?php if(Request::get('amount') && Request::get('currency') && Request::get('user_id')): ?>
/*jQuery('div.paypal-button.large').click();*/
  $('#donate-form').submit();
<?php endif; ?>
<?php if(isset($donation_success) && !empty($donation_success)): ?>
toastr.success('<?php echo e($donation_success); ?>');
<?php endif; ?>
});
$(document).on('change','#sel1',function(){
  var currency = $(this).val();
  var page_url = site_url('donate/?currency='+currency);
  $(location).attr("href", page_url);
  /*var amounts = $(this).find(':selected').data('amounts');

  var symbol = $(this).find(':selected').data('symbol');
  var amounts_div = '';
  if(amounts){

    $.each( amounts, function( index, amount ) {
      amounts_div += `<div class="col-md-4 priceRangeCol">
                                    <div class="priceRangeOnly" id="`+amount._da_id+`" title="`+amount.da_amount+`"><span>`+symbol+` </span> `+amount.da_amount+`</div>
                                  </div>`;
    });
  }

  amounts_div += `<div class="col-md-12 priceRangeCol">
                                    <div class="priceRangeOnly" id="custom" title="custom">Custom</div>
                                  </div>`;

  $('#amounts_div').html(amounts_div);
  $('#custom_div').hide();*/
});

$(document).on('click','.priceRangeOnly',function(){
  var amount_id = $(this).attr('id');
  if($(this).hasClass('priceRangeOnlyActive')==false){
    $(".priceRangeOnly").each(function() {
      $(this).removeClass('priceRangeOnlyActive');
    });
    $('#'+amount_id).addClass('priceRangeOnlyActive');
    $('#donation_amount').val($('#'+amount_id).attr('title'));
    if($('#'+amount_id).attr('title')=='custom'){
      $('#custom_amount').val('');
      $('#custom_div').show();
    }else{
      $('#custom_div').hide();
      $('#amount').val($('#'+amount_id).attr('title'));
    }
  }
});
</script>
<?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/donate.blade.php ENDPATH**/ ?>