<?php if($db_name=='vehicle_model'): ?>
<label for="">Model</label>
<select class="form-control formModel" name="vehicle_model" id="vehicle_model">
    <option value="" hidden="">Select Model</option>
    <?php $__currentLoopData = $vehicle_model; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $model): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php
    	$selected = (isset($inserted_id) && $inserted_id==$model->_model_id) ? 'selected' : '';
    ?> 
    <option value="<?php echo e($model->model_name); ?>" <?php echo e($selected); ?> data-id="<?php echo e($model->_model_id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($model->model_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <option value="add">Add</option>
</select>
<img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
<script type="text/javascript">

</script>
<?php endif; ?><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/xhr/get_master.blade.php ENDPATH**/ ?>