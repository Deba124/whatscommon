
<?php if($feeds): ?>
    <?php $__currentLoopData = $feeds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $feed): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="globalFeedList">
            <div class="feedShown" id="feedShown-<?php echo e($feed->_feed_id); ?>">
            <div class="connectedInfo">
                <div class="position-relative pr-0 feedImg">
                    <div class=""><img src="<?php echo e($feed->user1_profile_photo ? $feed->user1_profile_photo : 'new-design/img/profile_placeholder.jpg'); ?>" class="img-fluid" alt=""></div>
                    <div class=""><img src="<?php echo e($feed->user2_profile_photo ? $feed->user2_profile_photo : 'new-design/img/profile_placeholder.jpg'); ?>" class="img-fluid" alt=""></div>
                    <img src="new-design/img/feedadd.png" class="feedadd" alt="">
                </div>
                <div class="rightPanText">
                    <p><a href="<?php echo e($feed->feed_user1==$user_id ? url('my-profile') : url('user-profile/?feed='.$feed->_feed_id.'&user='.$feed->feed_user1)); ?>"> <i><?php echo e($feed->user1_firstName); ?> <?php echo e($feed->user1_lastName); ?> </i></a>is <?php echo e($feed->feed_type); ?> with <a href="<?php echo e($feed->feed_user2==$user_id ? url('my-profile') : url('user-profile/?feed='.$feed->_feed_id.'&user='.$feed->feed_user2)); ?>"><i><?php echo e($feed->user2_firstName); ?> <?php echo e($feed->user2_lastName); ?></i> </a></p>
                    <span>About <?php echo e(DateTime_Diff($feed->feed_created)); ?> ago</span>
                </div>
            </div>

            <div class="feedInfo position-relative">
                <?php if($feed->feed_what): ?>
                    <p>
                        <span>What:</span> 
                        <span style="color: #91d639;"><?php echo e($feed->feed_event_type); ?></span>
                    </p>
                <p class="pl-5"> <?php echo e($feed->feed_what); ?></p><?php endif; ?>
                <?php if($feed->feed_where): ?><p><span>Where:  </span> <?php echo e($feed->feed_where); ?></p><?php endif; ?>
                <?php if($feed->feed_when): ?><p><span>When: </span> <?php echo e($feed->feed_when); ?></p><?php endif; ?>
                <?php if($feed->feed_keywords): ?><p><span>W5: </span> <?php echo e($feed->feed_keywords); ?></p><?php endif; ?>
                <div class="greenCountFeed"><?php echo e($feed->feed_match_count); ?></div>
            </div>

            <!-- <img src="new-design/img/mapfeed.png" class="img-fluid" alt="" width="100%"> -->
        <?php if($feed->feed_images && count($feed->feed_images)>0): ?>
            <?php
                    $counter = count($feed->feed_images);
                    $remaining = 0;
                    if($counter>3){
                      $remaining = $counter-2;
                    }
                    ?>
                        <div class="uploadPhotoHeading">Uploaded photos:</div>
                        <div class="galleryViewAll">
                                  <?php if($counter==1): ?>
                                    <div class="galleryViewImg1">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-12 galleryViewCol">
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[0]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php elseif($counter==2): ?>  
                                    <div class="galleryViewImg2">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[0]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[1]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php elseif($counter==3): ?>     
                                    <div class="galleryViewImg3">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[0]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[1]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[2]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[2]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[2]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php else: ?>
                                    <div class="galleryViewImg3 galleryViewImg4">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[0]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[1]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                          <div class="galleryViewImg">
                                            <img src="new-design/img/feedfoto01.png" class="img-fluid" alt="">
                                            <div class="galleryViewImgMore"><?php echo e($remaining); ?>+</div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php endif; ?>
                                  </div>

            
        <?php endif; ?>
            <div class="row text-center rightPanBtn">
                <!-- <a href="#" class="dismissButton"><img src="new-design/img/dismissIcon.png" class="img-fluid" alt=""> Dismiss</a> -->
                <div class="col-md-6 text-left">
                    <a href="javascript:void;" onclick="open_modal('share-feed','feed_id=<?php echo e($feed->_feed_id); ?>')" class="dismissButton" style="display: inline;"><img src="new-design/img/shares_ic2.png" style="height: 15px;
    margin-top: -3px;" class="img-fluid" alt=""> Share</a>
                </div>
                <div class="col-md-6 text-right">
                    <a href="#" data-feed_id="<?php echo e($feed->_feed_id); ?>" class="dismissButton" onclick="execute('api/hide-feed','user_id=<?php echo e($user_id); ?>&_feed_id=<?php echo e($feed->_feed_id); ?>');"><!-- <img src="new-design/img/dismissIcon.png" class="img-fluid" alt=""> --> Dismiss</a>
                </div>
                
                
                <?php if($feed->is_connected==1): ?>
                    <?php
                      $receiver_id = $user_quick_blox_id == $feed->user1_quick_blox_id ? $feed->user2_quick_blox_id : $feed->user1_quick_blox_id;
                    ?>
                    
                <?php endif; ?>
                <!-- <a href="#" class="tagstarButton"> Tagster Social</a> -->
                
            </div>

        </div>
        <div class="feedDismissed" id="feedDismissed-<?php echo e($feed->_feed_id); ?>">
            <div class="feedDismissedTitle">Feed dismissed</div>
            <div class="feedDismissedUndo" data-feed_id="<?php echo e($feed->_feed_id); ?>" onclick="execute('api/unhide-feed','user_id=<?php echo e($user_id); ?>&_feed_id=<?php echo e($feed->_feed_id); ?>');">Undo</div>
        </div>
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php else: ?>
<div class="globalFeedList">
    <div class="form-group text-center">No feeds available</div>
</div>
<?php endif; ?>
<script type="text/javascript">
$(document).ready(function(){
$(".feedDismissedUndo").click(function(){
        var feed_id = $(this).data('feed_id');
        $("#feedShown-"+feed_id).slideDown("fast");
        $("#feedDismissed-"+feed_id).removeClass("showDismissedUndo");
      });
      $(".dismissButton").click(function(){
        var feed_id = $(this).data('feed_id');
        console.log('feed_id=>'+feed_id);
        $("#feedShown-"+feed_id).slideUp("fast");
        $("#feedDismissed-"+feed_id).addClass("showDismissedUndo");
      });
});
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/xhr/search_feed.blade.php ENDPATH**/ ?>