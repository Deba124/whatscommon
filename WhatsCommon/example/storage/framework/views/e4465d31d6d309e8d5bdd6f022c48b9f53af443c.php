 <!-- Modal starts -->       
<div class="modal fade" id="select-payment-for-plans-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Select Payment Mode</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <?php if($currency_data && $currency_data->currency_shortname=='USD'): ?>
        <div class="form-group">
          <div class="btn-Edit-save">
            <button class="btn btn-Edit btn-Plan btn-block" style="height: 45px;border-radius: 23px;background: #0070ba;width: 100%;font-weight: 500;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;" 
              onclick="execute('create-squareup-payment','amount=<?php echo e($amount); ?>&payment_for=<?php echo e($payment_for); ?>&redirect_url=<?php echo e($redirect_url); ?>&plan_id=<?php echo e($currency_data->id); ?>');">Pay with <span style="font-style: italic;font-weight: 800;font-size: 20px;">Square</span></button>
          </div>
        </div>
        <?php endif; ?>
        <div id="smart-button-container">
      <div style="text-align: center;">
        <div id="paypal-button-container"></div>
      </div>
    </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->

  <script>
    function initPayPalButton() {
      paypal.Buttons({
        style: {
          shape: 'pill',
          color: 'blue',
          layout: 'vertical',
          label: 'pay',
          
        },

        createOrder: function(data, actions) {
          var amount = "<?php echo e($amount ? $amount : 0); ?>";
          /*if($('#donation_amount').val()=='custom'){
            amount = $('#custom_amount').val();
          }else{
            amount = $('#donation_amount').val();
          }*/
          
          return actions.order.create({
            purchase_units: [{"amount":{"currency_code":"<?php echo e($currency_data ? $currency_data->currency_shortname : 'USD'); ?>","value":amount}}]
          });
        },

        onApprove: function(data, actions) {
          return actions.order.capture().then(function(orderData) {
            
            // Full available details
            /*console.log('Capture result=>', orderData, JSON.stringify(orderData, null, 2));

            console.log('Capture result2 =>', orderData);*/
            var plan_id = "<?php echo e($currency_data->id); ?>";
            var donation_data = `plan_id=`+plan_id+`&user_id=<?php echo e($user_id); ?>`;

            console.log('donation_data => '+donation_data);

            execute('api/choose-plan',donation_data);

            // Show a success message within this page, e.g.
            /*const element = document.getElementById('paypal-button-container');
            element.innerHTML = '';
            element.innerHTML = '<h3>Thank you for your payment!</h3>';*/

            // Or go to another URL:  actions.redirect('thank_you.html');
            
          });
        },

        onError: function(err) {
          console.log(err);
        }
      }).render('#paypal-button-container');
    }
    initPayPalButton();
  </script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/xhr/select_payment_for_plans_modal.blade.php ENDPATH**/ ?>