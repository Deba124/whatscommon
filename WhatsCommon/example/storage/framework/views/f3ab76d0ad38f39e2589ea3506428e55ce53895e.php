<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                <div class="leftSlidePan <?php echo e(Request::get('menu')==1 ? '' : 'closePan'); ?>">
                  <a href="#" class="panelslideOpenButton <?php echo e(Request::get('menu')==1 ? 'panelslideCloseButton' : ''); ?>"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                  	<div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                  		<div class="settingLeftTitle">Information</div>
	                  	<ul class="settingMenu mb-0 list-unstyled">
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu" href="<?php echo e(url('feedback')); ?>"><img src="new-design/img/feedback.png" class="img-fluid menuIcon" alt="">Feedback</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu" href="<?php echo e(url('contact-us')); ?>"><img src="new-design/img/contact.png" class="img-fluid menuIcon" alt="">Contact Us</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu " href="<?php echo e(url('about-us')); ?>"><img src="new-design/img/about.png" class="img-fluid menuIcon" alt="">About Us</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu settingMenuActive" href="<?php echo e(url('premium-plans')); ?>"><img src="new-design/img/premium-a.png" class="img-fluid menuIcon" alt="">WC Premium</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu" href="<?php echo e(url('donate')); ?>"><img src="new-design/img/donate.png" class="img-fluid menuIcon" alt="">Donate</a>
	                  		</li>
                        <li class="settingMenuLi">
                          <a class="settingMenu " style="color: #ed1c24;" href="<?php echo e(url('logout')); ?>"><img src="<?php echo e(url('new-design/img/logout.png')); ?>" class="img-fluid menuIcon" alt="">Log Out</a>
                        </li>
	                  	</ul>
                  	</div>
                  	<!-- <a href="<?php echo e(url('logout')); ?>" class="logOut" style="margin-top: -40px;"><img src="new-design/img/logout.png" class="img-fluid menuIcon" alt=""> Log Out</a> -->
                </div>
                <div class="midPan">
                  <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                    <div class="row infoBodyRow">
                      <div class="col-md-5 infoBodyCol">
                          <div class="innerHome">
                            <a href="<?php echo e(url('home')); ?>"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                          </div>
                        <div class="feedbackImg">
                          <img src="new-design/img/premiumImg.png" class="img-fluid" alt="">
                        </div>
                      </div>
                      <div class="col-md-7 infoBodyCol premiumCol">
                        <div class="feedbackRight wcPremium">
                          <div class="premiumHeader">
                            <div class="feedbackTitle"><span>WC Premium</span></div>
                            <div class="feedbackTitle2" style="color: #000;">What’scommon is 100% Member-Supported</div>
                          </div>
                          <div class="feedbackTitle3">
                            <div class="feedbackTitle"><span>Features</span></div>
                          </div>

                          <div class="premiumBody">
                            <ul class="list-unstyled mb-0 premiumFeatures">
                              <li>Everyone Is Looking For Someone</li>
                              <li>Find Anyone or Anything... <br>Anywhere at Anytime</li>
                              <li>End to End Encryption</li>
                              <li>Safe and Secure</li>
                              <li>Unlimited Interaction</li>
                              <li>NO Pop-UPs and NO Ads</li>
                              <li>Connect Globaly</li>
                              <li>ONLY site of its kind</li>
                              <li>Cross App Interactions</li>
                            </ul>
                      <?php if(isset($is_lifetime) && $is_lifetime==1): ?>
                        <div class="form-group text-center">
                          <span>Congratulations! You are a lifetime member.</span>
                        </div>
                      <?php else: ?>
                            <div class="btn-Edit-save btn-feedback btn-premium">
                              <button class="btn btn-Edit btn-save openPlan">WC Premium <br><span>Get this now</span></button>
                            </div>
                      <?php endif; ?>
                          </div>
                        </div>
                        <div class="feedbackRight wcPremiumPlan planClose">
                          <div class="premiumHeader">
                            <div class="planBackIcon">
                              <div class="addBankBackIcon text-left closePanIcon">
                                <img src="img/back.png" class="img-fluid" alt="">
                              </div>
                            </div>
                            <div class="membershipLogo">
                              <img src="img/membershipLogo.png" class="img-fluid membershipLogoImg" alt="">
                            </div>

                            <div class="feedbackTitle mainPlanTitle"><span>Choose a Plan!</span></div>
                            <div class="feedbackTitle2">What’scommon is a Member-Supported application. We appreciate if you’re loving this application.</div>
                          </div>

                          <div class="premiumBody">
                <?php if($premium_plans): ?>
                  <?php $__currentLoopData = $premium_plans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $premium_plan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($premium_plan->plan_type=='lifetime'): ?>
                            <div class="choosePlan lifetimePaln">
                              <div class="donateForm donatePlanHeading">
                                <div class="monthlyPlan">
                                  <div class="monthlyPlanTitle"><?php echo e($premium_plan->plan_type); ?></div>
                                </div>
                              </div>
                              <div class="donateForm monthlyPlan">
                                <div class="row pricePlanRow">
                                  <div class="col-6">
                                    <div class="planPriceLeft">
                                      <div class="planPrice"><?php echo $premium_plan->currency_symbol; ?><?php echo e($premium_plan->plan_amount); ?></div>
                                      <div class="planPriceTag">for a liftetime</div>
                                    </div>
                                  </div>
                                  <div class="col-6">
                                    <div class="planPriceRight">
                          <?php if($premium_plan->is_active==0): ?>
                                      <!-- <div class="btn-Edit-save" id="paypal-button-container" style="margin-left:-30px;"> -->
                                        <button class="btn btn-Plan" onclick="open_modal('select-payment-for-plans','plan_id=<?php echo e($premium_plan->id); ?>');">Start Now!</button>
                                      </div>
                          <?php endif; ?>
                                      <!-- <div class="planBtnTag">Cancel anytime!</div> -->
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                    <?php elseif($premium_plan->plan_type=='annually'): ?>
                            <div class="choosePlan annuallyPaln">
                              <div class="donateForm donatePlanHeading">
                                <div class="monthlyPlan">
                                  <div class="monthlyPlanTitle"><?php echo e($premium_plan->plan_type); ?></div>
                                </div>
                              </div>
                              <div class="donateForm monthlyPlan">
                                <div class="row pricePlanRow">
                                  <div class="col-6">
                                    <div class="planPriceLeft">
                                      <div class="planPrice"><?php echo $premium_plan->currency_symbol; ?><?php echo e($premium_plan->plan_amount); ?></div>
                                      <div class="planPriceTag">a year</div>
                                    </div>
                                  </div>
                                  <div class="col-6">
                                    <div class="planPriceRight">
                            <?php if($premium_plan->is_active==0): ?>
                                      <!-- <div class="btn-Edit-save" id="paypal-button-container2" style="margin-left:-30px;">
                                        
                                      </div> -->
                                      <button class="btn btn-Plan" onclick="open_modal('select-payment-for-plans','plan_id=<?php echo e($premium_plan->id); ?>');">Start Now!</button>
                                      <div class="planBtnTag">Cancel anytime!</div>
                                      
                              <?php else: ?>
                                <button class="planBtnTag" onclick="execute('api/cancel-plan','plan_id=<?php echo e($premium_plan->id); ?>&user_id=<?php echo e(Session::get('userdata')['id']); ?>')">Cancel anytime!</button>
                              <?php endif; ?>
                            
                                      
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                    <?php else: ?>
                            <div class="choosePlan">
                              <div class="donateForm donatePlanHeading">
                                <div class="monthlyPlan">
                                  <div class="monthlyPlanTitle"><?php echo e($premium_plan->plan_type); ?></div>
                                </div>
                              </div>
                              <div class="donateForm monthlyPlan">
                                <div class="row pricePlanRow">
                                  <div class="col-6">
                                    <div class="planPriceLeft">
                                      <div class="planPrice"><?php echo $premium_plan->currency_symbol; ?><?php echo e($premium_plan->plan_amount); ?></div>
                                      <div class="planPriceTag">per month</div>
                                    </div>
                                  </div>
                                  <div class="col-6">
                                    <div class="planPriceRight">
                                      <!-- <div class="btn-Edit-save">
                                        <button class="btn btn-Edit btn-Plan" onclick="execute('create-squareup-payment','amount=<?php echo e($premium_plan->plan_amount); ?>&payment_for=monthly')">Pay Now</button>
                                      </div> -->
                              <?php if($premium_plan->is_active==0): ?>
                                      <!-- <div class="btn-Edit-save" id="paypal-button-container3" style="margin-left:-30px;">
                                        
                                      </div> -->
                                      <button class="btn btn-Plan" onclick="open_modal('select-payment-for-plans','plan_id=<?php echo e($premium_plan->id); ?>');">Start Now!</button>
                                      <div class="planBtnTag">Cancel anytime!</div>
                                      
                              <?php else: ?>
                                
                                <button class="planBtnTag" onclick="execute('api/cancel-plan','plan_id=<?php echo e($premium_plan->id); ?>&user_id=<?php echo e(Session::get('userdata')['id']); ?>')">Cancel anytime!</button>
                              <?php endif; ?>
                                      
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                    <?php endif; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script>
$(document).ready(function(){
  $(".openPlan").click(function(){
    $(".wcPremium").addClass("planClose");
    $(".wcPremiumPlan").removeClass("planClose");
  });
  $(".closePanIcon").click(function(){
    $(".wcPremiumPlan").addClass("planClose");
    $(".wcPremium").removeClass("planClose");
  });
});
</script>
<?php if($plan_sess && !empty($plan_sess)): ?>
<script>
  var plan_data = `user_id=<?php echo e(Session::get('userdata')['id']); ?>&plan_id=`+'<?php echo e($plan_sess); ?>';
  execute('api/choose-plan',plan_data);
</script>
<?php endif; ?>
<script src="https://www.paypal.com/sdk/js?client-id=AYHDiS8TDnzx5Tpr16oDWmKIn2XGhLFqFZJ-3GWBVja6eD7jXqLKH6Dpq2TaSiLpdzPvbVSK8mxKv-JU&disable-funding=card&currency=USD&enable-funding=venmo" data-sdk-integration-source="button-factory"></script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/premium_plans.blade.php ENDPATH**/ ?>