 <!-- Modal starts -->       
<div class="modal fade" id="add-language-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Add Language</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="add-language">
          <div class="form-group" id="submit_status"></div>
          <?php echo csrf_field(); ?>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Language Name<span class="text-danger">*</span></label>
                <input type="text" name="lang_name" class="form-control">
                <p class="text-danger" id="lang_name_error"></p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Language Short Name<span class="text-danger">*</span></label>
                <input type="text" name="short_name" class="form-control">
                <p class="text-danger" id="short_name_error"></p>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Flag URL <span class="text-danger">*</span></label>
                <input type="text" name="flag_url" class="form-control">
                <p class="text-danger" id="flag_url_error"></p>
              </div>
            </div>
          </div>
          
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends --><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/xhr/add_language_modal.blade.php ENDPATH**/ ?>