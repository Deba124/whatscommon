<?php $__env->startSection('title', 'Relationships'); ?>
<?php echo $__env->make('admin.inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('admin.inc.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Relationships</h4>
      <div class="row" style="margin-bottom: 15px;">
        <div class="col-9"></div>
        <meta name="_token" content="<?php echo e(csrf_token()); ?>">
        <div class="col-3 text-right">
          <a class="btn btn-secondary" href="javascript:void();" onclick="open_modal('add-relation');"><i class="fa fa-plus-circle text-success"></i> Add Relation</a>
        </div>
      </div>
      <div class="row">
  
        <div class="col-12">
          <table id="relations-list" class="table table-condensed table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Date Created</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
<?php if($relations): ?>
  <?php
    $i = 1
  ?>
  <?php $__currentLoopData = $relations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $relation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
              <td><?php echo e($i++); ?></td>
              <td><?php echo e($relation->relation_name); ?></td>
              <td><?php echo e(format_date($relation->created_at)); ?></td>
              <td>
                <?php if($relation->relation_is_active==1): ?> 
                <span class="badge badge-success">Active</span>
                <?php else: ?>
                <span class="badge badge-danger">Inactive</span>
                <?php endif; ?>
              </td>
              <td>
                <a class="custom_btn _edit" onclick="open_modal('edit-relation','_relation_id=<?php echo e($relation->_relation_id); ?>');">
                  <i class="fa fa-edit"></i>
                </a>
                <?php if($relation->relation_is_active==1): ?>
                  <a class="custom_btn _edit" onclick="execute('admin/active-inactive-relation','_relation_id=<?php echo e($relation->_relation_id); ?>&relation_is_active=1');" title="Inactivate">
                  <i class="fa fa-lock" aria-hidden="true"></i>
                  </a>
                  <?php else: ?>
                  <a class="custom_btn _edit" onclick="execute('admin/active-inactive-relation','_relation_id=<?php echo e($relation->_relation_id); ?>&relation_is_active=0');" title="Activate">
                  <i class="fa fa-unlock" aria-hidden="true"></i>
                  </a>
                <?php endif; ?>
              </td>
            </tr>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php else: ?>
    <tr><td colspan="4" class="text-center"><b>No Relationships Available</b></td></tr>
<?php endif; ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $__env->make('admin.inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!-- content-wrapper ends -->
<script type="text/javascript">
$(document).ready(function() {  

  dataTable = $('#relations-list').DataTable( {
    dom: 'lBfrtip',
    responsive: true,
    ordering : true,
    lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
    pageLength: 10,
    buttons: [
      {
          extend: 'print',
          text: '<button class="btn btn-primary btn-sm" ><i class="fa fa-print fa-x5"> Print</i></button>',
          autoPrint: false,
          exportOptions: {
              columns: [ 0,1,2 ]
          }
      },
      {
          extend: 'excelHtml5',
          text: '<button class="btn btn-success btn-sm"><i class="fa fa-file-excel-o fa-x5"> Excel</i></button>',
          exportOptions: {
              columns: [ 0,1,2 ]
          }
      },
      {
          extend: 'pdfHtml5',
          text: '<button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o fa-x5"> PDF</i></button>',
          exportOptions: {
              columns:[ 0,1,2 ]
          }
      }
    ],
    "processing": true,
    "serverSide": false,
  });
}); 
</script><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/relations_list.blade.php ENDPATH**/ ?>