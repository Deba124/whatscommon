<div class="addBankBackIcon reverseLookupBackTwo text-left">
  <img src="new-design/img/back.png" class="img-fluid my-0" alt="">
</div>
<div class="connectionsDetailsInfo">
    <div class="connectionsDetailsInfoName">What’sCommon</div>
    <div class="connectionsDetailsInfoEvents" style="margin-bottom: -6px;">
        <span class="eventsText orangeText mr-0">Reverse Lookup</span>
    </div>
</div>

<?php if($users): ?>
	<?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<div class="reverseLookupMatch">
      	<div class="matchHolder matchHolderShadow">
        <div class="connectionsAllDetailsTop">
          <div class="connectionsDetailsAvtarImg">
            <img src=" <?php echo e($user->profile_photo_path ? $user->profile_photo_path : 'https://newcastlebeach.org/images/human-images-1.jpg'); ?> " class="img-fluid" alt="">
          </div>

          <div class="connectionsDetailsInfo">
            <div class="connectionsDetailsInfoName" style="color:#000;"><?php echo e($user->firstName); ?> <?php echo e($user->lastName); ?></div>
            <div class="connectionsDetailsInfoEvents myEvenntsAll">
              <div class="myEvennts mt-1">
                <span class="eventsText" style="font-weight: 600;">In Common:</span>
              </div>
            </div>
            <div class="reverseLookuEmail"><?php echo e($user->email); ?></div>
            <div class="reverseLookuCall"><?php echo e($user->isd_code); ?> <?php echo e($user->phone); ?></div>
            <div class="reverseLookuDate"><?php echo e(format_date2($user->dob)); ?></div>
            <div class="reverseLookuPlace"><?php echo e($user->country_name); ?></div>
            <div class="reverseLookupBtn mt-3">
          <?php if($user->is_connected==1): ?>
              <a class="btn btn-following" href="<?php echo e(url('connections/?user='.$user->id)); ?>">Visit</a>
            <?php if(!empty($user->quick_blox_id)): ?>
              <a class="btn btn-following btn-message" href="<?php echo e(url('messages/?qb='.$user->quick_blox_id)); ?>">Message</a>
            <?php endif; ?>
          <?php endif; ?>
            </div>
          </div>

          <div class="greenCountFeed"><?php echo e($user->match_count); ?></div>
        </div>
      	</div>
    </div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php else: ?>
	<div class="reverseLookupMatch">
		<p class="text-center text-danger">No matches found</p>
	</div>
<?php endif; ?><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/xhr/reverse_lookup.blade.php ENDPATH**/ ?>