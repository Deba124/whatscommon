<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly innerBodyModify">
  	<div class="container-fluid">
    	<div class="settingBody">
	      	<div class="position-relative settingFlip">
	          	<div class="leftSlidePan closePan">
	            	<a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
		            <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
		              <?php echo $__env->make('inc.life_event_left_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		            </div>
	          	</div>
	          	<div class="midPan">
		            <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
		              	<div class="row infoBodyRow">
			                <div class="col-md-5 infoBodyCol connectionsBody eventImgCol">
                        <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
			                    <div class="innerHome">
			                    	<a href="<?php echo e(url('home')); ?>"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
			                    </div>
			                  	<div class="feedbackImg">
				                    <div class="personalImgAll">
				                      	<img src="new-design/img/lostfound.png" class="img-fluid" alt="">
				                      	<!-- <div class="personalImgTitle">Lost & Found</div> -->
				                    </div>
			                  	</div>
                        </div>
			               </div>
			                <div class="col-md-7 infoBodyCol connectionsBody eventImgCol">
                          <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid" style="background-color: #FFF !important;">
			                  		<form class="xhr_form" method="post" action="api/life-event-upload" id="life-event-upload">
                              <?php echo csrf_field(); ?>
                              <div class="eventColHeading">What </div><img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                              <input type="hidden" name="type_id" value="9">
                              <input type="hidden" name="user_id" value="<?php echo e(Session::get('userdata')['id']); ?>">
                              <div class="whatForm whatFormDationg">
                                <!-- <div class="form-group datingFilterBtn timeOptionSet">
                                  <div class="form-control timeOptionOnly enterFor activeTimeOption">Enter</div>
                                  <div class="form-control timeOptionOnly searchFor">Search</div>
                                </div> -->
                                <div class="form-group datingFilterBtn timeOptionSet">
                                    <div class="form-control timeOptionOnly enterFor activeTimeOption">Enter</div>
                                    <div class="form-control timeOptionOnly searchFor">Search</div>
                                    <input type="hidden" name="search_for" value="0" id="search_for" />
                                </div>

                                <div class="form-group datingSpecific">
                                  <label for="">Item</label>
                                  <select class="form-control " id="items" name="item_name">
                                    <option value="">Select</option>
                    <?php if($items): ?>
                      <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option option="<?php echo e($item->item_name); ?>" data-sizes="<?php echo e($item->item_sizes); ?>"><?php echo e($item->item_name); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                  </select>
                                </div>

                                <div class="whatReason1 vehicleForm">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">Size</label>
                                        <select class="form-control formSize" id="sizes" name="item_size">
                                          <option value="">Select</option>
                                        </select>
                                        <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">Material</label>
                                        <select class="form-control formMaterial" name="item_material">
                                          <option value="">Select</option>
                    <?php if($materials): ?>
                      <?php $__currentLoopData = $materials; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $material): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option option="<?php echo e($material->material_name); ?>"><?php echo e($material->material_name); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                        </select>
                                        <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">Outdoor</label>
                                        <select class="form-control formOutdoor" name="outdoor">
                                          <option value="">Select</option>
                    <?php if($outdoors): ?>
                      <?php $__currentLoopData = $outdoors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $outdoor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option option="<?php echo e($outdoor->outdoor_name); ?>"><?php echo e($outdoor->outdoor_name); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                        </select>
                                        <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">Indoor</label>
                                        <select class="form-control formLocation" name="indoor">
                                          <option value="">Select</option>
                    <?php if($indoors): ?>
                      <?php $__currentLoopData = $indoors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $indoor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option option="<?php echo e($indoor->indoor_name); ?>"><?php echo e($indoor->indoor_name); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                        </select>
                                        <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">In Motion</label>
                                        <select class="form-control formAirline" name="inmotion">
                                          <option value="">Select</option>
                    <?php if($inmotions): ?>
                      <?php $__currentLoopData = $inmotions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $inmotion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option option="<?php echo e($inmotion->inmotion_name); ?>"><?php echo e($inmotion->inmotion_name); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                        </select>
                                        <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">Quantity</label>
                                        <input type="text" name="other" class="form-control formOther">
                                        <!-- <select class="form-control formOther">
                                          <option>1</option>
                                          <option>2</option>
                                        </select> -->
                                        <!-- <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt=""> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                
                              </div>
                              <div class="eventColHeading">Where </div>
                              <div class="whatForm whereFrom">
                                <div class="form-group">
                                  <label for="">Country</label>
                                  <select class="form-control formCountry" name="country" id="country">
                                    <option value="">Select</option>
                  <?php if($countries): ?>
                    <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($country->country_name); ?>" data-id="<?php echo e($country->_country_id); ?>"><?php echo e($country->country_name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php endif; ?>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                                <div class="form-group">
                                  <label for="">State/County/Province</label>
                                  <select class="form-control formState" name="state" id="province">
                                    <option value="">Select</option>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                                <div class="form-group">
                                  <label for="">City</label>
                                  <select class="form-control formCity" name="city" id="city">
                                    <option value="">Select</option>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                                <div class="form-group">
                                  <label for="">Street Address</label>
                                  <input type="text" class="form-control formAddress" id="" placeholder="123 abc Ave." name="street">
                                </div>
                                <div class="form-group">
                                  <label for="">ZIP</label>
                                  <input type="text" class="form-control formZip" id="" placeholder="ZIP" name="zip">
                                </div>
                              </div>
                              <div class="eventColHeading">When </div>
                              <div class="whatForm whereFrom">
                                <div class="birthdayDate">
                                  <div class="form-group birthdayMonth">
                                    <label for="number">From</label>
                                    <select class="form-control formMonth" id="" name="when_from_month">
                                      <option value="">Month</option>
                                      <option value="01">January</option>
                                      <option value="02">February</option>
                                      <option value="03">March</option>
                                      <option value="04">April</option>
                                      <option value="05">May</option>
                                      <option value="06">June</option>
                                      <option value="07">July</option>
                                      <option value="08">August</option>
                                      <option value="09">September</option>
                                      <option value="10">October</option>
                                      <option value="11">November</option>
                                      <option value="12">December</option>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group birthdayDay">
                                    <label for="number">Day</label>
                                    <select class="form-control" id="" name="when_from_day">
                                      <option value="">DD</option>
                                      <?php for($i=1;$i<=31;$i++): ?>
                                        <?php
                                          $j = sprintf('%02d', $i)
                                        ?>
                                       <option value="<?php echo e($j); ?>"><?php echo e($j); ?></option>
                                      <?php endfor; ?>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group birthdayYear">
                                    <label for="number">Year</label>
                                    <select class="form-control" id="" name="when_from_year">
                                      <option value="">YYYY</option>
                                    <?php for($i = date('Y'); $i >= 1900; $i--): ?>
                                      <option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                                    <?php endfor; ?>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                </div>
                              </div>
                              <div class="eventColHeading">Who, What, Where, When, Why </div>
                              <div class="whatForm whereFrom">
                                
                                <div class="myAllEvents" id="keywords_div2"></div>
                                <input type="hidden" id="keywords2" value="" name="event_keywords">
                                <div class="input-group lifeEvent">
                                  <input type="text" class="form-control" id="add_keyword2" placeholder="Life Event Keyword(s)">
                                  <div class="input-group-append">
                                    <a onclick="addKeyword2();" class="btn btn-add" style="text-decoration: none;">Add</a>
                                  </div>
                                </div>
                                <div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                              </div>
                        
                        <?php echo $__env->make('inc.life_event_img_upload', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                            </form>
			                  	</div>
			                </div>
		              	</div>
		            </div>
	          	</div>
	      	</div>
    	</div>
  	</div>
</div>
<!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow:hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Lost/Found</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Lost/Found</label>
            <p>If you lost something, this is the perfect life event for you. You can describe it
and enter your missing item or search for it. If someone found something and
they want to return it, they descried it and location and… ITEM FOUND!</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>This is the location where you lost or found the item.</p>
          </div>
          <div class="form-group">
            <label>WHEN:</label>
            <p>A specific date when you lost or found the item.</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).ready(function(){
  $(".enterFor").click(function(){
        $(".enterFor").addClass("activeTimeOption");
        $(".searchFor").removeClass("activeTimeOption");
        $('#search_for').val(0);
    });
    $(".searchFor").click(function(){
        $(".searchFor").addClass("activeTimeOption");
        $(".enterFor").removeClass("activeTimeOption");
        $('#search_for').val(1);
    });
});
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value=""="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/life_events/lost_and_found.blade.php ENDPATH**/ ?>