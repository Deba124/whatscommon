<option value="">Select City</option>
<?php if($cities): ?>
	<?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<?php if($for_life_event && $for_life_event==1): ?>
		<option value="<?php echo e($city->city_name); ?>" data-id="<?php echo e($city->_city_id); ?>"><?php echo e($city->city_name); ?></option>
	<?php else: ?>
		<option value="<?php echo e($city->_city_id); ?>"><?php echo e($city->city_name); ?></option>
	<?php endif; ?>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/xhr/city.blade.php ENDPATH**/ ?>