<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<style type="text/css">
.lifeDraftNotifCount{
	font-weight: 600;
    font-size: 16px;
    color: #fff;
    background-color: #91d639;
    height: 26px;
    width: 40px;
    border-radius: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-left: auto;
    top: 10px !important;
    right: 10px !important;
}

</style>
<div class="innerBodyOnly innerBodyModify">
    <div class="container-fluid">
        <div class="settingBody settingBodyFullScroll">
            <div class="position-relative settingFlip">
                <div class="leftSlidePan closePan">
                    <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                    <div class="lifeDraftLeftTitle">LIFE EVENTS</div>
                    <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                      <ul class="list-unstyled mb-0 lifeEventMenu">
                        <li class="lifeEventMenuLi lifeDraftLi <?php echo e(Request::is('life-events') ? 'lifeEventMenuLiActive' : ''); ?>">
                          <a href="<?php echo e(url('life-events')); ?>" class="lifeEventMenuLink">
                            <span class="lifeEventMenuText">Created Life Events</span>
                            <span class="lifeDraftCount"><?php echo e($life_event_count); ?></span>
                          </a>
                        </li>
                        <li class="lifeEventMenuLi lifeDraftLi <?php echo e(Request::is('drafts') ? 'lifeEventMenuLiActive' : ''); ?>">
                          <a href="<?php echo e(url('drafts')); ?>" class="lifeEventMenuLink lifeDraftMenuLink">
                            <span class="lifeEventMenuText">Drafts</span>
                            <span class="lifeDraftCount"><?php echo e($draft_count); ?></span>
                          </a>
                        </li>
                      </ul>
                    </div>
                </div>
<?php
$date = '';
/*if(count($drafts) > 0){
	$date = $drafts[0]->created_at;
	$date = DateTime_Diff2($date);
}*/
?>

                <div class="midPan lifeDraftLiMidPan windowHeight windowHeightMid">
<?php if(count($drafts) > 0): ?>
	<?php $__currentLoopData = $drafts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $draft): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php 
			if($date!=DateTime_Diff2($draft->event_create_time)){
				$date = DateTime_Diff2($draft->event_create_time);
				echo '<div class="lifeDraftLeftTitle lifeDraftRightTitle">'.$date.'</div>';
			}
		?>
                    
                    <div class="connectionsBody infoBodyRight">
                      <div class="row infoBodyRow">
                        <div class="col-md-12 infoBodyCol eventCol midHightControl bg-white">
                          <div class="feedbackRight position-relative">
                                
                                <?php if($draft->match_count>0): ?>
                                <div class=" lifeDraftNotifCount " onclick="show_hide_notidiv('<?php echo e($draft->_event_id); ?>');"><?php echo e($draft->match_count); ?></div>
                                <?php endif; ?>
                                <div class="eventColHeading">What</div>
                                <div class="whatForm position-relative">
                                  <ul class="mb-0 list-unstyled lifeDraftResult">
                                    <li>Life Event: <span class="wcGreen"><?php echo e($draft->event_type); ?><?php echo e($draft->sub_cat_type_name ? ' ('.$draft->sub_cat_type_name.')' : ''); ?></span></li>

                                <?php if($draft->event_pet_name): ?>
                                    <li>Name: <span><?php echo e($draft->event_pet_name); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_pet_species): ?>
                                    <li>Species: <span><?php echo e($draft->event_pet_species); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_pet_breed): ?>
                                    <li>Breed: <span><?php echo e($draft->event_pet_breed); ?></span></li>
                                <?php endif; ?>

                                <?php if($draft->event_military_branch): ?>
                                    <li>Branch: <span><?php echo e($draft->event_military_branch); ?></span></li>
                                <?php endif; ?>

                                <?php if($draft->event_bio_mother): ?>
                                    <li>Mother: <span><?php echo e($draft->event_bio_mother); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_bio_father): ?>
                                    <li>Father: <span><?php echo e($draft->event_bio_father); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_birthname): ?>
                                    <li>Name given at birth: <span><?php echo e($draft->event_birthname); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_home_type): ?>
                                    <li>Type of Home: <span><?php echo e($draft->event_home_type); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_home_style): ?>
                                    <li>Style: <span><?php echo e($draft->event_home_style); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_tag_number): ?>
                                    <li>Tag Number: <span><?php echo e($draft->event_tag_number); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_vehicle_type): ?>
                                    <li>Type of Vehicle: <span><?php echo e($draft->event_vehicle_type); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_vehicle_maker): ?>
                                    <li>Vehicle Maker: <span><?php echo e($draft->event_vehicle_maker); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_vehicle_model): ?>
                                    <li>Model: <span><?php echo e($draft->event_vehicle_model); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_vehicle_year): ?>
                                    <li>Year: <span><?php echo e($draft->event_vehicle_year); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_vehicle_color): ?>
                                    <li>Vehicle Color: <span><?php echo e($draft->event_vehicle_color); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_family_firstname): ?>
                                    <li>Firstname: <span><?php echo e($draft->event_family_firstname); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_family_lastname): ?>
                                    <li>Lastname: <span><?php echo e($draft->event_family_lastname); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_age): ?>
                                    <li>Age: <span><?php echo e($draft->event_age); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_gender): ?>
                                    <li>Gender: <span><?php echo e($draft->event_gender); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_family_relation): ?>
                                    <li>Relation: <span><?php echo e($draft->event_family_relation); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_family_marital_status): ?>
                                    <li>Marital Status: <span><?php echo e($draft->event_family_marital_status); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_family_industry): ?>
                                    <li>Industry: <span><?php echo e($draft->event_family_industry); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_family_category): ?>
                                    <li>Category: <span><?php echo e($draft->event_family_category); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_ethnicity): ?>
                                    <li>Ethnicity: <span><?php echo e($draft->event_ethnicity); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_race): ?>
                                    <li>Race: <span><?php echo e($draft->event_race); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_nationality): ?>
                                    <li>Nationality: <span><?php echo e($draft->event_nationality); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_height): ?>
                                    <li>Height: <span><?php echo e($draft->event_height); ?> <?php echo e($draft->event_height_measure ? $draft->event_height_measure : ''); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_weight): ?>
                                    <li>Weight: <span><?php echo e($draft->event_weight); ?> <?php echo e($draft->event_weight_measure ? $draft->event_weight_measure : ''); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_height_general): ?>
                                    <li>Height General: <span><?php echo e($draft->event_height_general); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_weight_general): ?>
                                    <li>Weight General: <span><?php echo e($draft->event_weight_general); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_facial_hair): ?>
                                    <li>Facial Hair: <span><?php echo e($draft->event_facial_hair); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_orientation): ?>
                                    <li>Orientation: <span><?php echo e($draft->event_orientation); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_body_style): ?>
                                    <li>Body Style: <span><?php echo e($draft->event_body_style); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_skintone): ?>
                                    <li>Skintone: <span><?php echo e($draft->event_skintone); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_hair_color): ?>
                                    <li>Hair Color: <span><?php echo e($draft->event_hair_color); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_eye_color): ?>
                                    <li>Eye Color: <span><?php echo e($draft->event_eye_color); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_political): ?>
                                    <li>Political Affiliation: <span><?php echo e($draft->event_political); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_religion): ?>
                                    <li>Religion: <span><?php echo e($draft->event_religion); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_looking_for): ?>
                                    <li>Looking For: <span><?php echo e($draft->event_looking_for); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_age_range): ?>
                                    <li>Age Range: <span><?php echo e($draft->event_age_range); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_interest): ?>
                                    <li>Interest: <span><?php echo e($draft->event_interest); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_body_hair): ?>
                                    <li>Body Hair: <span><?php echo e($draft->event_body_hair); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_glasses): ?>
                                    <li>Glasses: <span><?php echo e($draft->event_glasses); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_party): ?>
                                    <li>Party: <span><?php echo e($draft->event_party); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_smoke): ?>
                                    <li>Smoke: <span><?php echo e($draft->event_smoke); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_drink): ?>
                                    <li>Drink: <span><?php echo e($draft->event_drink); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_tattoos): ?>
                                    <li>Tattoos: <span><?php echo e($draft->event_tattoos); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_tattoo_type): ?>
                                    <li>Tattoo Type: <span><?php echo e($draft->event_tattoo_type); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_fav_food): ?>
                                    <li>Favourite Food: <span><?php echo e($draft->event_fav_food); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_food_type): ?>
                                    <li>Food Type: <span><?php echo e($draft->event_food_type); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_fav_team): ?>
                                    <li>Favourite Team: <span><?php echo e($draft->event_fav_team); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_zodiac): ?>
                                    <li>Zodiac Sign: <span><?php echo e($draft->event_zodiac); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_birth_stone): ?>
                                    <li>Birth Stone: <span><?php echo e($draft->event_birth_stone); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_affiliations): ?>
                                    <li>Affiliations: <span><?php echo e($draft->event_affiliations); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_career): ?>
                                    <li>Career: <span><?php echo e($draft->event_career); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_financial): ?>
                                    <li>Financial: <span><?php echo e($draft->event_financial); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_drink): ?>
                                    <li>Drink: <span><?php echo e($draft->event_drink); ?></span></li>
                                <?php endif; ?>
                                
                                <?php if($draft->event_time_of_birth): ?>
                                    <li>Time of Birth: <span><?php echo e($draft->event_time_of_birth); ?> <?php echo e($draft->event_time_of_birth_ampm ? $draft->event_time_of_birth_ampm : ''); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_airport): ?>
                                    <li>Travel Mode: <span><?php echo e($draft->event_airport); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_airline): ?>
                                    <li>Name: <span><?php echo e($draft->event_airline); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_flight_number): ?>
                                    <li>Number: <span><?php echo e($draft->event_flight_number); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_flight_row): ?>
                                    <li>Row: <span><?php echo e($draft->event_flight_row); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_flight_seat): ?>
                                    <li>Seat: <span><?php echo e($draft->event_flight_seat); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_military_unit1): ?>
                                    <li>Unit 1: <span><?php echo e($draft->event_military_unit1); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_military_unit2): ?>
                                    <li>Unit 2: <span><?php echo e($draft->event_military_unit2); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_military_unit3): ?>
                                    <li>Unit 3: <span><?php echo e($draft->event_military_unit3); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_military_unit4): ?>
                                    <li>Unit 4: <span><?php echo e($draft->event_military_unit4); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_military_unit5): ?>
                                    <li>Unit 5: <span><?php echo e($draft->event_military_unit5); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_military_unit6): ?>
                                    <li>Unit 6: <span><?php echo e($draft->event_military_unit6); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_military_rank): ?>
                                    <li>Rank: <span><?php echo e($draft->event_military_rank); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_military_mos): ?>
                                    <li>MOS: <span><?php echo e($draft->event_military_mos); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_military_title): ?>
                                    <li>Title: <span><?php echo e($draft->event_military_title); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_school_name): ?>
                                    <li>School Info: <span><?php echo e($draft->event_school_name); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_school_sorority): ?>
                                    <li>Sorority/Fraternity: <span><?php echo e($draft->event_school_sorority); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_school_teacher): ?>
                                    <li>Teacher/Professor: <span><?php echo e($draft->event_school_teacher); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_school_roomno): ?>
                                    <li>Room No.: <span><?php echo e($draft->event_school_roomno); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_school_subject): ?>
                                    <li>Subject: <span><?php echo e($draft->event_school_subject); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_school_curriculars): ?>
                                    <li>Curriculum: <span><?php echo e($draft->event_school_curriculars); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_school_organization): ?>
                                    <li>Organization: <span><?php echo e($draft->event_school_organization); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_school_event): ?>
                                    <li>Event: <span><?php echo e($draft->event_school_event); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_school_afterschool): ?>
                                    <li>After School: <span><?php echo e($draft->event_school_afterschool); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_career_name): ?>
                                    <li>Business: <span><?php echo e($draft->event_career_name); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_career_category): ?>
                                    <li>Category: <span><?php echo e($draft->event_career_category); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_career_title): ?>
                                    <li>Title: <span><?php echo e($draft->event_career_title); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_career_level): ?>
                                    <li>Level: <span><?php echo e($draft->event_career_level); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_pet_temperment): ?>
                                    <li>Temperment: <span><?php echo e($draft->event_pet_temperment); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_pet_color): ?>
                                    <li>Color: <span><?php echo e($draft->event_pet_color); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_pet_size): ?>
                                    <li>Size: <span><?php echo e($draft->event_pet_size); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_pet_license_number): ?>
                                    <li>License Number: <span><?php echo e($draft->event_pet_license_number); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_pet_collar): ?>
                                    <li>Collar: <span><?php echo e($draft->event_pet_collar); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_pet_veterinary): ?>
                                    <li>Veterinarian: <span><?php echo e($draft->event_pet_veterinary); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_pet_medical): ?>
                                    <li>Medical Condition: <span><?php echo e($draft->event_pet_medical); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_item_name): ?>
                                    <li>Item: <span><?php echo e($draft->event_item_name); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_item_size): ?>
                                    <li>Size: <span><?php echo e($draft->event_item_size); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_item_material): ?>
                                    <li>Material: <span><?php echo e($draft->event_item_material); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_outdoor): ?>
                                    <li>Outdoor: <span><?php echo e($draft->event_outdoor); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_indoor): ?>
                                    <li>Indoor: <span><?php echo e($draft->event_indoor); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_inmotion): ?>
                                    <li>In Motion: <span><?php echo e($draft->event_inmotion); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_other): ?>
                                    <li>Quantity: <span><?php echo e($draft->event_other); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_game): ?>
                                    <li>Game or Activity: <span><?php echo e($draft->event_game); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_gameteam): ?>
                                    <li>Team/Group: <span><?php echo e($draft->event_gameteam); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->event_username): ?>
                                    <li>Username: <span><?php echo e($draft->event_username); ?></span></li>
                                <?php endif; ?>
                                    
                                  </ul>
                                </div>
                                <div class="eventColHeading">Where</div>
                                <div class="whatForm">
                                  <ul class="mb-0 list-unstyled lifeDraftResult">
                            <?php if($draft->event_type_id==1): ?>
                            	<?php if($draft->where_country): ?>
                            		<li>Country: <span class="wcGreen"><?php echo e($draft->where_country); ?></span></li>
                            	<?php endif; ?>
                            	<?php if($draft->where_state): ?>
                                    <li>State/Province: <span><?php echo e($draft->where_state); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_city): ?>
                                    <li>City: <span><?php echo e($draft->where_city); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_street): ?>
                                    <li>Street Address: <span><?php echo e($draft->where_street); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_zip): ?>
                                    <li>Zip: <span><?php echo e($draft->where_zip); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_email): ?>
                                    <li>Email: <span><?php echo e($draft->where_email); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_phone): ?>
                                    <li>Phone: <span><?php echo e(($draft->where_isd) ? $draft->where_isd : ''); ?> <?php echo e($draft->where_phone); ?></span></li>
                                <?php endif; ?>
                            <?php elseif($draft->event_type_id==2): ?>
                            	<?php if($draft->where_country): ?>
                            		<li>Country: <span class="wcGreen"><?php echo e($draft->where_country); ?></span></li>
                            	<?php endif; ?>
                            	<?php if($draft->where_state): ?>
                                    <li>State/Province: <span><?php echo e($draft->where_state); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_city): ?>
                                    <li>City: <span><?php echo e($draft->where_city); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_radius): ?>
                                    <li>Radius: <span><?php echo e($draft->where_radius); ?></span></li>
                                <?php endif; ?>
                            <?php elseif($draft->event_type_id==3): ?>

                            	<?php if($draft->where_birth_place): ?>
                            		<li>Birth Place: <span class="wcGreen"><?php echo e($draft->where_birth_place); ?></span></li>
                            	<?php endif; ?>
                            	<?php if($draft->where_birth_country): ?>
                                    <li>Country of Birth: <span><?php echo e($draft->where_birth_country); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_birth_state): ?>
                                    <li>State: <span><?php echo e($draft->where_birth_state); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_birth_city): ?>
                                    <li>City: <span><?php echo e($draft->where_birth_city); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_birth_street): ?>
                                    <li>Street Address: <span><?php echo e($draft->where_birth_street); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_zip): ?>
                                    <li>Zip: <span><?php echo e($draft->where_zip); ?></span></li>
                                <?php endif; ?>
                                

                            	<?php if($draft->where_adoption_status): ?>
                                    <li>Adoption Status: <span><?php echo e($draft->where_adoption_status); ?></span></li>
                                <?php endif; ?>
                            	<?php if($draft->where_country): ?>
                            		<li>Country: <span class="wcGreen"><?php echo e($draft->where_country); ?></span></li>
                            	<?php endif; ?>
                            	<?php if($draft->where_state): ?>
                                    <li>State/Province: <span><?php echo e($draft->where_state); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_city): ?>
                                    <li>City: <span><?php echo e($draft->where_city); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_street): ?>
                                    <li>Street Address: <span><?php echo e($draft->where_street); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_zip): ?>
                                    <li>Zip: <span><?php echo e($draft->where_zip); ?></span></li>
                                <?php endif; ?>

                            <?php elseif($draft->event_type_id==4): ?>

                            	<?php if($draft->where_location_type): ?>
                                    <li>Location Type: <span><?php echo e($draft->where_location_type); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_hotel_name): ?>
                                    <li>Location Name: <span><?php echo e($draft->where_hotel_name); ?></span></li>
                                <?php endif; ?>
                            	<?php if($draft->where_country): ?>
                            		<li>Country: <span class="wcGreen"><?php echo e($draft->where_country); ?></span></li>
                            	<?php endif; ?>
                            	<?php if($draft->where_state): ?>
                                    <li>State/Province: <span><?php echo e($draft->where_state); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_city): ?>
                                    <li>City: <span><?php echo e($draft->where_city); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_street): ?>
                                    <li>Street Address: <span><?php echo e($draft->where_street); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_zip): ?>
                                    <li>Zip: <span><?php echo e($draft->where_zip); ?></span></li>
                                <?php endif; ?>
                            
                            <?php elseif($draft->event_type_id==5): ?>

                            	<?php if($draft->where_country): ?>
                            		<li>Country: <span class="wcGreen"><?php echo e($draft->where_country); ?></span></li>
                            	<?php endif; ?>
                            	<?php if($draft->where_state): ?>
                                    <li>State/Province: <span><?php echo e($draft->where_state); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_city): ?>
                                    <li>City: <span><?php echo e($draft->where_city); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_street): ?>
                                    <li>Street Address: <span><?php echo e($draft->where_street); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_zip): ?>
                                    <li>Zip: <span><?php echo e($draft->where_zip); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_base): ?>
                                    <li>Base: <span><?php echo e($draft->where_base); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_orders): ?>
                                    <li>Orders: <span><?php echo e($draft->where_orders); ?></span></li>
                                <?php endif; ?>

                            <?php elseif(in_array($draft->event_type_id,[6,7,8,9])): ?>

                            	<?php if($draft->where_country): ?>
                            		<li>Country: <span class="wcGreen"><?php echo e($draft->where_country); ?></span></li>
                            	<?php endif; ?>
                            	<?php if($draft->where_state): ?>
                                    <li>State/Province: <span><?php echo e($draft->where_state); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_city): ?>
                                    <li>City: <span><?php echo e($draft->where_city); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_street): ?>
                                    <li>Street Address: <span><?php echo e($draft->where_street); ?></span></li>
                                <?php endif; ?>
                                <?php if($draft->where_zip): ?>
                                    <li>Zip: <span><?php echo e($draft->where_zip); ?></span></li>
                                <?php endif; ?>

                            <?php endif; ?>
                                    
                                  </ul>
                                </div>

                                <div class="eventColHeading">When</div>
                                <div class="whatForm position-relative"> 
                            <?php if($draft->match_count>0): ?>
                                
                                 
                                
				                
				                      <!-- Modal starts -->       
<div class="modal fade matched_users" id="show-matched-users-modal-<?php echo e($draft->_event_id); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered " role="document">
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: none;">
      	<h5 class="modal-title" id="exampleModalLabel-3" style="margin-left: 35%;">Accounts</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="min-height: 350px;overflow-y: auto;">
        <div class="notificationsBox windowHeight notificationHeight">
        	<?php if($draft->users): ?>
            <?php
            /*echo '<pre>';
            print_r($draft->users);*///exit;    

            ?>
				<?php $__currentLoopData = $draft->users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        	<div class="notificationCard" style="position: relative;">
                <?php if($user->quick_blox_id): ?>
                                        <div class="notificationsBtn liftDraftNotificationsBtn">
                                          <a href="<?php echo e(url('messages/?qb='.$user->quick_blox_id)); ?>" class="blockBtn replayBtn" style="position: absolute;top:20px;"><img src="new-design/img/msgIcon2.png" class="img-fluid" alt=""> Message</a>
                                        </div>
                                        <?php endif; ?>
				                        <div class="notificationDetails" style="margin-top: -8px;">
				                          <div class="notificationCardAvtar">
				                            <img src="<?php echo e($user->profile_photo_path ? $user->profile_photo_path : url('new-design/img/profile_placeholder.jpg')); ?>" class="img-fluid" alt="">
				                          </div>
				                          <div class="notificationAvtarInfo">
				                            <div class="notificationAvtarName"><?php echo e($user->firstName); ?> <?php echo e($user->lastName); ?></div>
				                            <div class="notificationAvtarMsg">
				                              <span> <?php echo e('@'); ?><?php echo e($user->username); ?></span>
				                            </div>
				                          </div>
				                        </div>
				                        
				                      </div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				                <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
				                    
				                    
                            <?php endif; ?>
                                  <ul class="mb-0 list-unstyled lifeDraftResult">
                            <?php if($draft->event_type_id==1): ?>
                            	<?php if(in_array($draft->event_subcategory_id,[1,2])): ?>
                            		<?php if($draft->when_from_day && $draft->when_from_month && $draft->when_from_year): ?>
                            		<li>From: <span class="wcGray"><?php echo e($draft->when_from_month); ?>/<?php echo e($draft->when_from_day); ?>/<?php echo e($draft->when_from_year); ?></span></li>
                            		<?php endif; ?>
                            		<?php if($draft->when_to_day && $draft->when_to_month && $draft->when_to_year): ?>
                            		<li>To: <span class="wcGray"><?php echo e($draft->when_to_month); ?>/<?php echo e($draft->when_to_day); ?>/<?php echo e($draft->when_to_year); ?></span></li>
                            		<?php endif; ?>
                            	<?php else: ?>

                            		<?php if($draft->when_family_birth_day && $draft->when_family_birth_month && $draft->when_family_birth_year): ?>
                            		<li>Birthday: <span class="wcGray"><?php echo e($draft->when_family_birth_month); ?>/<?php echo e($draft->when_family_birth_day); ?>/<?php echo e($draft->when_family_birth_year); ?></span></li>
                            		<?php endif; ?>
                            		<?php if($draft->when_family_wedding_day && $draft->when_family_wedding_month && $draft->when_family_wedding_year): ?>
                            		<li>Wedding Day: <span class="wcGray"><?php echo e($draft->when_family_wedding_month); ?>/<?php echo e($draft->when_family_wedding_day); ?>/<?php echo e($draft->when_family_wedding_year); ?></span></li>
                            		<?php endif; ?>
                            		<?php if($draft->when_family_passed_day && $draft->when_family_passed_month && $draft->when_family_passed_year): ?>
                            		<li>Death Day: <span class="wcGray"><?php echo e($draft->when_family_passed_month); ?>/<?php echo e($draft->when_family_passed_day); ?>/<?php echo e($draft->when_family_passed_year); ?></span></li>
                            		<?php endif; ?>
                            		<?php if($draft->when_from_day && $draft->when_from_month && $draft->when_from_year): ?>
                            		<li>From: <span class="wcGray"><?php echo e($draft->when_from_month); ?>/<?php echo e($draft->when_from_day); ?>/<?php echo e($draft->when_from_year); ?></span></li>
                            		<?php endif; ?>
                            		<?php if($draft->when_to_day && $draft->when_to_month && $draft->when_to_year): ?>
                            		<li>To: <span class="wcGray"><?php echo e($draft->when_to_month); ?>/<?php echo e($draft->when_to_day); ?>/<?php echo e($draft->when_to_year); ?></span></li>
                            		<?php endif; ?>
                            	<?php endif; ?>

                            <?php elseif($draft->event_type_id==2): ?>
                            		<?php if($draft->when_timeframe): ?>
                            		<li>Timeframe: <span class="wcGray"><?php echo e($draft->when_timeframe); ?></span></li>
                            		<?php endif; ?>

                            		<?php if($draft->when_month ): ?>
                            		<li>To: <span class="wcGray"><?php echo e($draft->when_month); ?></span></li>
                            		<?php endif; ?>
                           	<?php elseif($draft->event_type_id==3): ?>
                           			<?php if($draft->when_from_day && $draft->when_from_month && $draft->when_from_year): ?>
                            		<li>From: <span class="wcGray"><?php echo e($draft->when_from_month); ?>/<?php echo e($draft->when_from_day); ?>/<?php echo e($draft->when_from_year); ?></span></li>
                            		<?php endif; ?>

                            <?php elseif($draft->event_type_id==4): ?>
                           			<?php if($draft->when_from_day && $draft->when_from_month && $draft->when_from_year): ?>
                            		<li>From: <span class="wcGray"><?php echo e($draft->when_from_month); ?>/<?php echo e($draft->when_from_day); ?>/<?php echo e($draft->when_from_year); ?></span></li>
                            		<?php endif; ?>
                            		<?php if($draft->when_to_day && $draft->when_to_month && $draft->when_to_year): ?>
                            		<li>To: <span class="wcGray"><?php echo e($draft->when_to_month); ?>/<?php echo e($draft->when_to_day); ?>/<?php echo e($draft->when_to_year); ?></span></li>
                            		<?php endif; ?>

                            		<?php if($draft->when_time_ampm && $draft->when_time): ?>
                            		<li>Departure Time: <span><?php echo e($draft->when_time); ?> <?php echo e($draft->when_time_ampm); ?></span></li>
                            		<?php endif; ?>

                            		<?php if($draft->when_arrival_time_ampm && $draft->when_arrival_time): ?>
                            		<li>Arrival Time: <span><?php echo e($draft->when_arrival_time); ?> <?php echo e($draft->when_arrival_time_ampm); ?></span></li>
                            		<?php endif; ?>

                            <?php elseif(in_array($draft->event_type_id,[5,6,7,8,9,11])): ?>

                            		<?php if($draft->when_from_day && $draft->when_from_month && $draft->when_from_year): ?>
                            		<li>From: <span class="wcGray"><?php echo e($draft->when_from_month); ?>/<?php echo e($draft->when_from_day); ?>/<?php echo e($draft->when_from_year); ?></span></li>
                            		<?php endif; ?>
                            		<?php if($draft->when_to_day && $draft->when_to_month && $draft->when_to_year): ?>
                            		<li>To: <span class="wcGray"><?php echo e($draft->when_to_month); ?>/<?php echo e($draft->when_to_day); ?>/<?php echo e($draft->when_to_year); ?></span></li>
                            		<?php endif; ?>

                            <?php endif; ?>
                                    
                                  </ul>
                            
                                
                                </div>
                            <?php if($draft->event_keywords): ?>
                                <div class="eventColHeading">Who, What, Where, When, Why</div>
                                <div class="whatForm">
                                  <ul class="mb-0 list-unstyled lifeDraftResult">
                                    <li><span><?php echo e($draft->event_keywords); ?></span></li>
                                  </ul>
                                </div>
                            <?php endif; ?>
                            <?php if($draft->images): ?>
                                <div class="eventColHeading">Photo Gallery</div>
                                <div class="whatForm whereFrom">
                                  <div class="photoGalleryScroll">
                                    <div class="photoGallery mt-0">
                                <?php $__currentLoopData = $draft->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <div class="photoGalleryOnly mb-0">
                                        <img src="<?php echo e($image->image_url); ?>" class="img-fluid photoGalleryImg" alt="">
                                      </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                      
                                    </div>
                                  </div>
                                </div>
                            <?php endif; ?>
                                <div class="editDeleteBtn">
                                  <a href="<?php echo e($draft->event_is_draft==0 ? url('edit-life-event/?event='.$draft->_event_id) : url('edit-drafts/?event='.$draft->_event_id)); ?>" class="btn editBtn"><img src="new-design/img/editBtnIcon.png" class="img-fluid mr-2" alt=""> Edit</a>
                                  <a class="btn editBtn deleteBtn" onclick="execute('api/delete-life-event','user_id=<?php echo e($draft->event_user_id); ?>&_event_id=<?php echo e($draft->_event_id); ?>');"><img src="new-design/img/deleteBtnIcon.png" class="img-fluid mr-2" alt=""> Delete</a>
                                </div>
                          </div>
                        </div>
                      </div>
                    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
function show_hide_notidiv(counter){
	//$(".NotificationsAll").toggleClass("eventsNotificationOff");
	/*if($('#show-matched-users-modal-'+counter).hasClass('eventsNotificationOff')){
		$('#show-matched-users-modal-'+counter).removeClass('eventsNotificationOff');
	}else{
		$('#show-matched-users-modal-'+counter).addClass('eventsNotificationOff');
	}*/
	$('.matched_users').modal('hide');
	$('#show-matched-users-modal-'+counter).modal('show');
}
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/drafts.blade.php ENDPATH**/ ?>