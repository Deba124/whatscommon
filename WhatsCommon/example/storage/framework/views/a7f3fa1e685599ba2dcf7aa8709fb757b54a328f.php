<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                  <div class="leftSlidePan closePan">
                    <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                    <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                      <?php echo $__env->make('inc.life_event_left_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </div>
                  </div>
                  <div class="midPan">
                    <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                      <div class="row infoBodyRow">
                        <div class="col-md-5 infoBodyCol">
                            <div class="innerHome">
                              <a href="<?php echo e(url('home')); ?>"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                            </div>
                          <div class="feedbackImg">
                            <div class="personalImgAll">
                              <img src="new-design/img/personalImg.png" class="img-fluid" alt="">
                              <div class="personalImgTitle">Personal</div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7 infoBodyCol eventCol midHightControl">
                  <div class="feedbackRight windowHeight windowHeightMid" style="height: 445px;">
                            <form class="xhr_form" method="post" action="api/life-event-upload" id="life-event-upload">
                            <?php echo csrf_field(); ?>
                              <div class="eventColHeading">What </div>
                              
                                <img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                              
                              <input type="hidden" name="type_id" value="1">
                              <input type="hidden" name="user_id" id="user_id" value="<?php echo e(Session::get('userdata')['id']); ?>">
                                <div class="whatForm">
                                  <div class="form-group">
                                    <label for="">What</label>
                                    <select class="form-control selectCenter" id="whatReason" name="sub_category_id">
                                      <option value="" disabled selected>Select</option>
                    <?php if($what): ?>
                    	<?php $__currentLoopData = $what; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $w): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value="<?php echo e($w->id); ?>"><?php echo e($w->sub_cat_type_name); ?></option>
                                      
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                    </select>
                                  </div>
                                  <div class="whatReason1">
                                    <div class="form-group">
                                      <label for="">Type of Home</label>
                                      <select class="form-control selectCenter" name="type_of_home">
                                        <option value="" hidden>Select</option>
                    <?php if($home_types): ?>
                    	<?php $__currentLoopData = $home_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $home_type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($home_type->id); ?>"><?php echo e($home_type->home_type_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                      </select>
                                    </div>
                                    <div class="form-group">
                                      <label for="">Style</label>
                                      <select class="form-control selectCenter" name="home_style">
                                        <option value="" hidden>Select</option>
                    <?php if($home_styles): ?>
                    	<?php $__currentLoopData = $home_styles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $home_style): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($home_style->id); ?>"><?php echo e($home_style->home_style_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                      </select>
                                    </div>
                                   
                                  </div>

                                  <div class="whatReason2 vehicleForm removedWhat">
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Plate Tag Number</label>
                                          <input type="text" class="form-control formPlate" id="" placeholder="123abc" name="tag_number">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Type of Vehicle</label>
                                          <select class="form-control formVehicle" id="vehicle_type" name="type_of_vehicle">
                                            <option value="" hidden>Select</option>
                    <?php if($vehicle_types): ?>
                    	<?php $__currentLoopData = $vehicle_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vehicle_type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($vehicle_type->id); ?>" data-makers="<?php echo e($vehicle_type->makers); ?>"><?php echo e($vehicle_type->vehicle_type_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Make</label>
                                          <select class="form-control formMake" id="vehicle_maker" name="vehicle_maker">
                                            <option value="" hidden="">Select Type</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="vehicle_model_div">
                                          <label for="">Model</label>
                                          
                                          <select class="form-control formModel" name="vehicle_model" id="vehicle_model">
                                            <option value="" hidden="">Select Model</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Year</label>
                                          <input type="number" class="form-control formMonth" name="year_manufactured">
                                          
                                          
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Color</label>
                                          <select class="form-control formColor" name="vehicle_color">
                                            <option value="" hidden>Select</option>
                	<?php if($vehicle_colors): ?>
                    	<?php $__currentLoopData = $vehicle_colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($color->color_name); ?>"><?php echo e($color->color_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                    </div>
                                    
                                  </div>
                                  <div class="whatReason3 vehicleForm removedWhat">
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">First Name</label>
                                          <input type="text" class="form-control formName" id="" placeholder="Name" name="first_name">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Last Name</label>
                                          <input type="text" class="form-control formName" id="" placeholder="Name" name="last_name">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Age</label>
                                          <input type="number" class="form-control formAge" name="age">
                                          
                                          
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Gender</label>
                                          <select class="form-control formGender" name="gender">
                                            <option value="" hidden>Select</option>
                    <?php if($genders): ?>
                    	<?php $__currentLoopData = $genders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gender): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($gender->gender_name); ?>"><?php echo e($gender->gender_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Relation</label>
                                          <select class="form-control formRelationship" name="relationship">
                                            <option value="" hidden>Select</option>
                    <?php if($relations): ?>
                    	<?php $__currentLoopData = $relations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $relation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($relation->relation_name); ?>"><?php echo e($relation->relation_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Status</label>
                                          <select class="form-control formStatus" name="marital_status">
                                            <option value="" hidden>Select</option>
                    <?php if($marital_status): ?>
                    	<?php $__currentLoopData = $marital_status; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ms): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($ms->ms_name); ?>"><?php echo e($ms->ms_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Industry</label>
                                          <select class="form-control formIndustry" name="industry">
                                            <option value="" hidden>Select</option>
                    <?php if($industries): ?>
                    	<?php $__currentLoopData = $industries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $industry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($industry->industry_name); ?>"><?php echo e($industry->industry_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Category</label>
                                          <select class="form-control formCategory" name="category">
                                            <option value="" hidden>Select</option>
                    <?php if($occupations): ?>
                    	<?php $__currentLoopData = $occupations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $occupation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    					<option option="<?php echo e($occupation->occupation_name); ?>"><?php echo e($occupation->occupation_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="connectTagsterAll" style="margin-bottom: 15px;">
                                  <a href="#" class="connectTagster" onclick="open_modal('coming-soon');">
                                    <img src="new-design/img/TagsterLogo.png" class="img-fluid connectTagsterLogo">
                                    <div class="connectTagsterText">Connect Tagster</div>
                                  </a>
                                </div>
                                <!-- <div class="form-group text-center">
                                  <a href="javascript:void;" onclick="open_modal('coming-soon');" style="color: #60b15d;border: 2px solid #60b15d;padding: 10px 25px;border-radius: 20px;font-weight: 700;">
                                    <img src="<?php echo e(url('new-design/img/TagsterLogo.png')); ?>"> Connect Tagster
                                  </a>
                                </div> -->
                                <div class="eventColHeading">Where</div>
                                <div class="whatForm whereFrom">
                                  <div class="form-group">
                                    <label for="">Country</label>
                                    <select class="form-control formCountry" name="country" id="country">
                              <option value="" hidden>Select</option>
            <?php if($countries): ?>
              <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($country->country_name); ?>" data-id="<?php echo e($country->_country_id); ?>"><?php echo e($country->country_name); ?></option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
                              
                            </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">State/County/Province</label>
                                    <select class="form-control formState" name="state" id="province">
                                      <option value="" hidden>Select</option>
                                      
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">City</label>
                                    <select class="form-control formCity" name="city" id="city">
                                      <option value="" hidden>Select</option>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Street Address</label>
                                    <input type="text" class="form-control formAddress" id="" placeholder="123 abc Ave." name="street">
                                  </div>
                                  <div class="form-group">
                                    <label for="">ZIP</label>
                                    <input type="text" class="form-control formZip" id="" placeholder="ZIP" name="zip">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="text" class="form-control formMail" id="" placeholder="Enter Email" name="email">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Phone</label>
                                    <input type="text" class="form-control formPh" id="" placeholder="Enter Phone" name="phone">
                                  </div>
                                </div>
                                <div class="eventColHeading">When</div>
                                <div class="whatForm whereFrom">
                                  <div class="birthdayDate">
                                    <div class="form-group birthdayMonth">
                                      <label for="number">From</label>
                                      <select class="form-control formMonth" id="" name="when_from_month">
                                        <option value="" hidden>Month</option>
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayDay">
                                      <label for="number">Day</label>
                                      <select class="form-control" id="" name="when_from_day">
                                        <option value="" hidden>DD</option>
                                        <?php for($i=1;$i<=31;$i++): ?>
                                          <?php
                                            $j = sprintf('%02d', $i)
                                          ?>
                                         <option value="<?php echo e($j); ?>"><?php echo e($j); ?></option>
                                        <?php endfor; ?>
                                        
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayYear">
                                      <label for="number">Year</label>
                                      <select class="form-control" id="" name="when_from_year">
                                        <option value="" hidden>YYYY</option>
                                      <?php for($i = 1900; $i <= date('Y'); $i++): ?>
                                        <option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                                      <?php endfor; ?>
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                  </div>
                                  <div class="birthdayDate">
                                    <div class="form-group birthdayMonth">
                                      <label for="number">To</label>
                                      <select class="form-control formMonth" id="" name="when_to_month">
                                        <option value="" hidden>Month</option>
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayDay">
                                      <label for="number">Day</label>
                                      <select class="form-control" id="" name="when_to_day">
                                <option value="" hidden>DD</option>
                                <?php for($i=1;$i<=31;$i++): ?>
                                  <?php
                                    $j = sprintf('%02d', $i)
                                  ?>
                                 <option value="<?php echo e($j); ?>"><?php echo e($j); ?></option>
                                <?php endfor; ?>
                              </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayYear">
                                      <label for="number">Year</label>
                                      <select class="form-control" id="" name="when_to_year">
                                <option value="" hidden>YYYY</option>
                              <?php for($i = 1950; $i <= date('Y'); $i++): ?>
                                <option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                              <?php endfor; ?>
                              </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                  </div>

                                </div>
                                <div class="eventColHeading">Who, What, Where, When, Why</div>
                                <div class="whatForm whereFrom">
                                  
                                  
                                  	<div class="myAllEvents" id="keywords_div"></div>
			                        <input type="hidden" id="keywords" value="" name="event_keywords">
			                        <div class="input-group lifeEvent">
			                          <input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
			                          <div class="input-group-append">
			                            <a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
			                           </div>
			                        </div>
                                  	<div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                                </div>

                                <?php echo $__env->make('inc.life_event_img_upload', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                                
                              </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
 <!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Personal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Personal</label>
            <p>This section will ask specific contents about "what" the life event is all about</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>It's interesting to look back in time and remember all the things you did and
the people you have met. This section will ask specific contents about "what"
the life event is all about. Have fun, List places you lived and match with
forgotten neighbors, friends’ childhood besties. Cars you’ve owed can match
with details people remember about you etc. keep on thinking… keep on
remembering…</p>
          </div>
          <div class="form-group">
            <label>WHEN:</label>
            <p>This section will work like your calender. You have to enter any day or date you remember. There's two(2) section available - when the event started and when done.</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$("#whatReason").change(function(){
    if($(this).val() == "2") {
        $('.whatReason1').addClass('removedWhat');
        $('.whatReason2').removeClass('removedWhat');
        $('.whatReason3').addClass('removedWhat');
    } else if($(this).val() == "3") {
        $('.whatReason1').addClass('removedWhat');
        $('.whatReason2').addClass('removedWhat');
        $('.whatReason3').removeClass('removedWhat');
    } else {
        $('.whatReason1').removeClass('removedWhat');
        $('.whatReason2').addClass('removedWhat');
        $('.whatReason3').addClass('removedWhat');
    }
});
$(document).on('change','#vehicle_type',function(){
	var makers = $(this).find(':selected').data('makers');
	/*console.log('makers => ');
	console.log(makers);*/
	var vehicle_maker = '<option value="" hidden>Select Maker</option>';
	if(makers){

		$.each( makers, function( index, maker ) {
			vehicle_maker += '<option value="'+maker.maker_name+'" data-maker_id="'+maker._maker_id+'">'+maker.maker_name+'</option>';
		});
	}

	$('#vehicle_maker').html(vehicle_maker);
});
$(document).on('change', '#vehicle_maker', function(){
  var maker_id = $(this).find(':selected').data('maker_id');
  var user_id = $('#user_id').val();
  console.log('maker_id => '+maker_id);
  execute('api/get-master','user_id='+user_id+'&db_name=vehicle_model&column_name=model_name&status_column=model_is_active&dependent_name=model_maker_id&dependent_value='+maker_id+'&is_dependent=1');
});
$(document).on('change','#vehicle_model',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = $('#user_id').val();
    var maker_id = $('#vehicle_maker').find(':selected').data('maker_id');
    open_modal('add-master','user_id='+user_id+'&db_name=vehicle_model&column_name=model_name&status_column=model_is_active&dependent_name=model_maker_id&dependent_value='+maker_id+'&is_dependent=1');
  }
});
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="" hidden="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/life_events/personal.blade.php ENDPATH**/ ?>