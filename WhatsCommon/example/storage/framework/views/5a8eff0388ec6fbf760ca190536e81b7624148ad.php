
<div class="eventColHeading">Photo Gallery</div>
<div class="whatForm whereFrom">
  <div class="photoGalleryScroll">
    <div class="photoGallery" id="photoGallery" >
      <div class="photoGalleryOnly">
        <img src="new-design/img/addGalleryImage.png" class="img-fluid photoGalleryImg toBeRemoved" alt="" onclick="chooseFile('1');">
      </div>
    </div>
    <input type="hidden" id="image_counter" name="image_counter" value="0">
    <div id="file_uploaders" style="display: none;">
      <input type="file" name="images1" class="img_input" id="1" accept=".jpg,.png,.jpeg" onchange="readURL(this);">
    </div>
    
  </div>
</div>
<div class="whatForm whatFormBtnAll">
  <div class="whatFormBtnSE">
    <input type="hidden" name="add_another" id="add_another" value="0" />
    <input type="hidden" name="show_life_event" id="show_life_event" value="0" />
    <button class="btn whatFormBtn" onclick="$('#add_another').val(1);$('#show_life_event').val(0);">Add Another</button>
    <button class="btn whatFormBtn" onclick="$('#add_another').val(0);$('#show_life_event').val(0);" type="submit">Save and Exit</button>
  </div>

  <div class="btn-Edit-save btn-feedback btn-premium">
    <button class="btn btn-Edit btn-save" onclick="$('#add_another').val(0);$('#show_life_event').val(1);">what'scommon <br><span>upload your life event</span></button>
  </div>
</div>
<script type="text/javascript">
function chooseFile(id){
  $("#"+id).click();
}
function removeThis(id){
  $('#'+id).remove();
}
var image_counter = 1;
var storedFiles = [];
var add_img = '';
var add_uploader = '';
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.readAsDataURL(input.files[0]);
    reader.onload = function (e) {
        /*$('#profile_img').attr('src', e.target.result);*/
      var this_id = input.id;
      console.log('id===> '+this_id);
      $('.toBeRemoved').parent(".photoGalleryOnly").remove();
      image_counter++;
      add_img = `<div class="photoGalleryOnly">
        <img src="new-design/img/addGalleryImage.png" class="img-fluid photoGalleryImg toBeRemoved" alt="" onclick="chooseFile('`+image_counter+`');">
      </div>`;
      add_uploader = '<input type="file" name="images'+image_counter+'" onchange="readURL(this);" class="img_input" id="'+image_counter+'" accept=".jpg,.png,.jpeg" >';
      var uploaded = "<div class=\"photoGalleryOnly\">" +
            "<img class=\"img-fluid photoGalleryImg\" src=\"" + e.target.result + "\" title=\"" + e.target.name + "\"/>" +
            "<img src='new-design/img/removeField.png' class='img-fluid photoGalleryImgRemove' alt='' onclick='removeThis("+this_id+")'>" +
            "</div>";
      $('#photoGallery').prepend(uploaded);
      $(".photoGalleryImgRemove").click(function(){
        $(this).parent(".photoGalleryOnly").remove();
      });
      $('#photoGallery').append(add_img);
      $('#file_uploaders').append(add_uploader);
      $('#image_counter').val(image_counter-1);
      
    }
  }
}
</script><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/inc/life_event_img_upload.blade.php ENDPATH**/ ?>