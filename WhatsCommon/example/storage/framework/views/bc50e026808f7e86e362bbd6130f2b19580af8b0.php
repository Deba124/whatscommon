<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<style type="text/css">
.profileTop .connectionsDetailsInfoEvents span {
  font-weight: 700;
  font-size: 16px;
}
</style>
<div class="innerBodyOnly ">
  	<div class="container-fluid windowHeightLeft" style="overflow-y: auto;">
  		<div class="row ">
  			<div class="<?php if(isset($user_data->user_is_premium) && $user_data->user_is_premium==1): ?> <?php echo e('col-md-12'); ?> <?php else: ?> <?php echo e('col-md-8'); ?><?php endif; ?>">
  				<div class="card">
  					<div class="card-body">
	      				<div class="profileTop" style="padding: 10px;">
	                    	<div class="connectionsAllDetailsTop">
	                            <div class="connectionsDetailsAvtarImg">
	                              <img src="<?php echo e(Session::get('userdata')['profile_photo_path'] ? Session::get('userdata')['profile_photo_path'] : url('new-design/img/profile_placeholder.jpg')); ?>" class="img-fluid" alt="" id="profile_img">
	                              
	                            </div>

	                            <div class="connectionsDetailsInfo">
	                              <div class="connectionsDetailsInfoName"><?php echo e(Session::get('userdata')['firstName']); ?> <?php echo e(Session::get('userdata')['lastName']); ?> </div>
	                              <?php
	                              	$background = url('new-design/img/connectionsDetailsInfoId.png');
	                              ?>
	                              
	                              <div class="connectionsDetailsInfoEvents myEvenntsAll">
			                        <div class="myEvennts">
                                <a href="<?php echo e(url('connections')); ?>">
			                           <span class="eventsNumbers"><?php echo e(Session::get('userdata')['counts'][0]->connection_count); ?></span> <span class="eventsText">Connections</span>
                                </a>
			                        </div>
			                        <div>
			                          <span class="eventsNumbers"><?php echo e(Session::get('userdata')['counts'][0]->life_events_count); ?></span> <span class="eventsText">Life Events</span>
			                        </div>
			                        <div>
			                          <span class="eventsNumbers"><?php echo e(Session::get('userdata')['counts'][0]->matched_count); ?></span> <span class="eventsText">Matches</span>
			                        </div>
			                      </div>
			                      <div class="connectionsDetailsInfoEvents" style="background: url('<?php echo e($background); ?>') no-repeat left center; padding-left: 25px;">
	                                <span class="eventsText"><?php echo e('@'); ?><?php echo e(Session::get('userdata')['username']); ?> </span>
	                              </div>
			                      <div class="connectionsDetailsPlace"><?php echo e(Session::get('userdata')['city_name']); ?> <?php echo e((Session::get('userdata')['city_name'] && Session::get('userdata')['province_name']) ? ',' : ''); ?> <?php echo e(Session::get('userdata')['province_name']); ?> <?php echo e((Session::get('userdata')['country_name'] && Session::get('userdata')['province_name']) ? ',' : ''); ?> <?php echo e(Session::get('userdata')['country_name']); ?></div>
                            <?php if(Session::get('userdata')['bio']): ?><div class="connectionsDetailsPlace connectionsDetailsUser"><?php echo e(Session::get('userdata')['bio']); ?></div><?php endif; ?>
			                      <a class="btn btn-following" href="<?php echo e(url('settings')); ?>">Edit Profile</a>
	                            </div>
                            <?php if(Session::get('userdata')['created_at']): ?><div class="memberJoin">Member since <?php echo e(format_date(Session::get('userdata')['created_at'])); ?></div><?php endif; ?>
	                        </div>
	                    </div>
                	</div>
            	</div>
            	<div class="form-group text-left">
            		<h5 style="color: #b8b8b8; padding: 30px 0px;margin-bottom: -40px;">PERSONAL FEED</h5>
            	</div>
            	<div class="card">
            		<div class="card-body my-profile-feedlist" style="overflow-y: auto;">
        <?php if($feeds): ?>
              <?php $__currentLoopData = $feeds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $feed): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="globalFeedList" style="padding: 25px 10px;border-bottom: 3px solid #f8f8f8;">

                      <div class="connectedInfo">
                        <div class="position-relative pr-0 feedImg">
                          <div class=""><img src="<?php echo e($feed->user1_profile_photo ? $feed->user1_profile_photo : 'new-design/img/profile_placeholder.jpg'); ?>" class="img-fluid" alt=""></div>
                          <div class=""><img src="<?php echo e($feed->user2_profile_photo ? $feed->user2_profile_photo : 'new-design/img/profile_placeholder.jpg'); ?>" class="img-fluid" alt=""></div>
                          <img src="new-design/img/feedadd.png" class="feedadd" alt="">
                        </div>
                        <div class="rightPanText">
                        	<p>
                            <?php if($feed->user1_id==Session::get('userdata')['id']): ?>
                            <a href="<?php echo e(url('my-profile')); ?>"> 
                              <i>You </i> 
                            </a> are <?php echo e($feed->feed_type); ?> with <a href="<?php echo e(url('user-profile/?feed='.$feed->_feed_id.'&user='.$feed->user2_id)); ?>"> 
                              <i><?php echo e($feed->user2_firstName); ?> <?php echo e($feed->user2_lastName); ?> </i> 
                            </a>

                            <?php else: ?>
                            <a href="<?php echo e(url('user-profile/?feed='.$feed->_feed_id.'&user='.$feed->user1_id)); ?>"> 
                              <i><?php echo e($feed->user1_firstName); ?> <?php echo e($feed->user1_lastName); ?> </i> 
                            </a> is <?php echo e($feed->feed_type); ?> with <a href="<?php echo e(url('my-profile')); ?>"> 
                              <i>You </i> 
                            </a>

                            <?php endif; ?>
	                          	
                          	</p>
                          <span>About <?php echo e(DateTime_Diff($feed->feed_created)); ?> ago</span>
                        </div>
                      </div>
                    	<div class="row">
                    		<div class="col-md-6">
                    			<div class="feedInfo position-relative">
		                            <?php if($feed->feed_what): ?>
		                            <p>
		                                <span>What:</span> 
		                                <span style="color: #91d639;"><?php echo e($feed->feed_event_type); ?></span>
		                            </p>
		                            <p class="pl-5"> <?php echo e($feed->feed_what); ?></p>
		                            <?php endif; ?>
		                            <?php if($feed->feed_where): ?><p><span>Where:  </span> <?php echo e($feed->feed_where); ?></p><?php endif; ?>
		                            <?php if($feed->feed_when): ?><p><span>When: </span> <?php echo e($feed->feed_when); ?></p><?php endif; ?>
		                            <?php if($feed->feed_keywords): ?><p><span>W5: </span> <?php echo e($feed->feed_keywords); ?></p><?php endif; ?>
		                            <div class="greenCountFeed"><?php echo e($feed->feed_match_count); ?></div>
		                        </div>
                    		</div>
                    		<div class="col-md-6">
                <?php if($feed->feed_images && count($feed->feed_images)>0): ?>

                <?php
                    $counter = count($feed->feed_images);
                    $remaining = 0;
                    if($counter>3){
                      $remaining = $counter-2;
                    }
                ?>
                        <div class="uploadPhotoHeading">Uploaded photos:</div>
                        <div class="galleryViewAll">
                                  <?php if($counter==1): ?>
                                    <div class="galleryViewImg1">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-12 galleryViewCol">
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[0]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php elseif($counter==2): ?>  
                                    <div class="galleryViewImg2">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[0]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[1]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php elseif($counter==3): ?>     
                                    <div class="galleryViewImg3">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[0]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[1]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[2]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[2]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[2]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php else: ?>
                                    <div class="galleryViewImg3 galleryViewImg4">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[0]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[1]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                          <div class="galleryViewImg">
                                            <img src="new-design/img/feedfoto01.png" class="img-fluid" alt="">
                                            <div class="galleryViewImgMore"><?php echo e($remaining); ?>+</div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php endif; ?>
                                  </div>

                        
                <?php endif; ?>
                        
                        </div>
                        <div class="col-md-12">
                        <div class="row text-center rightPanBtn" style="display: block;">
                            <a href="#" class="dismissButton" onclick="execute('api/hide-feed','user_id=<?php echo e(Session::get('userdata')['id']); ?>&_feed_id=<?php echo e($feed->_feed_id); ?>&request_from=my-profile');"><img src="new-design/img/dismissIcon.png" class="img-fluid" alt=""> Dismiss</a>
                <?php if($feed->is_connected==1): ?>
                    <?php
                      $receiver_id = $user_quick_blox_id == $feed->user1_quick_blox_id ? $feed->user2_quick_blox_id : $feed->user1_quick_blox_id;
                    ?>
                            
                            <a href="javascript:void;" onclick="open_modal('share-feed','feed_id=<?php echo e($feed->_feed_id); ?>')" class="dismissButton" style="display: inline;"><img src="new-design/img/shares_ic2.png" 
                              style="height: 15px;margin-top: -3px;" class="img-fluid" alt=""> Share</a>
                <?php endif; ?>
                            <!-- <a href="#" class="tagstarButton"> Tagster Social</a> -->
                        </div>
                      </div>
                    	</div>
                        <div class="row">
                    		

                    	</div>

                        <!-- <img src="new-design/img/mapfeed.png" class="img-fluid" alt="" width="100%"> -->
                  

                    </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
            		</div>
            	</div>
  			</div>
      <?php if(isset($user_data->user_is_premium) && $user_data->user_is_premium==0): ?>
  			<div class="col-md-4">
  				<div class="card">
  					<div class="card-body">
  						<div class="form-group text-center">
	                        <img src="new-design/img/premiumImg.png" style="max-height: 170px;" class="img-fluid" alt="">
  						</div>
  						<div class="feedbackRight wcPremium">
                          <div class="premiumHeader" style="padding: 15px 30px 15px;">
                            <div class="feedbackTitle" style="font-size: 18px;"><span>WC Premium</span></div>
                          </div>

                          <div class="premiumBody">
                            <ul class="list-unstyled mb-0 premiumFeatures">
                              <li>Everyone Is Looking For Someone</li>
                              <li>Find Anyone or Anything... <br>Anywhere at Anytime</li>
                              <li>End to End Encryption</li>
                              <li>Safe and Secure</li>
                              <li>Unlimited Interaction</li>
                              <li>NO Pop-UPs and NO Ads</li>
                              <li>Connect Globaly</li>
                              <li>ONLY site of its kind</li>
                              <li>Cross App Interactions</li>
                            </ul>
                            <div class="btn-Edit-save btn-feedback btn-premium">
                              <a href="<?php echo e(url('premium-plans')); ?>" class="btn btn-Edit btn-save openPlan" style="padding-top: 15px;">WC Premium </a>
                            </div>
                          </div>
                        </div>
  					</div>
  				</div>
  			</div>
      <?php endif; ?>
  		</div>
	</div>
</div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script type="text/javascript">
$(document).ready(function(){
    //section height depends on screen resolution    
    $('.my-profile-feedlist').css('max-height',$(window).height()-125);
});
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/my_profile.blade.php ENDPATH**/ ?>