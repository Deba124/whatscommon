<?php
	$date = '';
	$sender_id = Session::get('userdata')['quick_blox_id'];
	$sender_image = Session::get('userdata')['profile_photo_path'] ? Session::get('userdata')['profile_photo_path'] : url('new-design/img/profile_placeholder.jpg');
?>

<?php if($messages): ?>
	<?php
		$reciever_id = $messages[0]->sender_id==$sender_id ? $messages[0]->recipient_id : $messages[0]->sender_id;
		$reciever_data = getUserDetailsByQuickID($reciever_id);
		$reciever_image = ($reciever_data && $reciever_data->profile_photo_path) ? $reciever_data->profile_photo_path : url('new-design/img/profile_placeholder.jpg');
	?>
	<?php $__currentLoopData = $messages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php
			if($date != date('Y-m-d',strtotime($message->created_at))){
				$date = date('Y-m-d',strtotime($message->created_at));
				if($date==date('Y-m-d')){
					echo '<div class="msgTime">Today</div>';
				}else if($date==date('Y-m-d',strtotime("-1 days"))){
					echo '<div class="msgTime">Yesterday</div>';
				}else{
					echo '<div class="msgTime">'.format_date($date).'</div>';
				}
			}
		?>
		
			<div class="<?php if($message->sender_id==$sender_id): ?> <?php echo e('chatBoxOdd'); ?> <?php else: ?> <?php echo e('chatBoxEven'); ?> <?php endif; ?>">
                <div class="chatPersonBox">
                  	<div class="connectionsTabDetails">
            <?php if($message->sender_id==$sender_id): ?>
            	<?php if($message->message): ?>
	                    <div class="chatTextAll">
	                      	<div class="chatText"><?php echo e($message->message); ?></div>
	                    </div>
	            <?php endif; ?>

	            <?php if($message->attachments && !empty($message->attachments)): ?>
	            	<?php $__currentLoopData = $message->attachments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $attachment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	            	<?php
            			$attach_url = 'https://api.quickblox.com/blobs/'.$attachment->id.'?token='.$token;
            			if($attachment->type=='image'){
            				echo '<a href="'.$attach_url.'" download target="new">';
            				echo '<img style="max-width: 150px !important;margin-right: 15px;" class="img-fluid" src="'.$attach_url.'">';
            				echo '</a>';
            			}elseif($attachment->type=='Audio'){
            				echo'<audio controls style="margin-right: 15px;" class="att-player">
									<source src="'.$attach_url.'" type="audio/ogg">
								</audio>';
            			}elseif($attachment->type=='video'){
            				echo '<video width="320" height="240" controls style="margin-right: 15px;" class="att-player">
								  <source src="'.$attach_url.'" type="video/mp4">
								</video>';
            			}
	            	?>
	            	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	            <?php endif; ?>
	                    <div class="connectionsAvtarImg">
	                      <img src="<?php if($message->sender_id==$sender_id): ?> <?php echo e($sender_image); ?> <?php else: ?> <?php echo e($reciever_image); ?> <?php endif; ?>" class="img-fluid" alt="">
	                    </div>
	        <?php else: ?>
	        			<div class="connectionsAvtarImg">
	                      <img src="<?php if($message->sender_id==$sender_id): ?> <?php echo e($sender_image); ?> <?php else: ?> <?php echo e($reciever_image); ?> <?php endif; ?>" class="img-fluid" alt="">
	                    </div>
	            <?php if($message->message): ?>
	                    <div class="chatTextAll">
	                      	<div class="chatText"><?php echo e($message->message); ?></div>
	                    </div>
	            <?php endif; ?>

	            <?php if($message->attachments && !empty($message->attachments)): ?>
	            	<?php $__currentLoopData = $message->attachments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $attachment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	            		<?php
            			$attach_url = 'https://api.quickblox.com/blobs/'.$attachment->id.'?token='.$token;
            			if($attachment->type=='image'){
            				echo '<a href="'.$attach_url.'" download target="new">';
            				echo '<img style="max-width: 150px !important;margin-left: 15px;" class="img-fluid" src="'.$attach_url.'" >';
            				echo '</a>';
            			}elseif($attachment->type=='Audio'){
            				echo'<audio controls style="margin-left: 15px;" class="att-player">
									<source src="'.$attach_url.'" type="audio/ogg">
								</audio>';
            			}elseif($attachment->type=='video'){
            				echo '<video width="320" height="240" controls style="margin-left: 15px;" class="att-player">
								  <source src="'.$attach_url.'" type="video/mp4">
								</video>';
            			}
	            	?>
	            	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	            <?php endif; ?>
	        <?php endif; ?>
                  	</div>
                  	<div class="msgTime sentMsgTime" id="<?php echo e($message->_id); ?>"><script>changeDate('<?php echo e($message->created_at); ?>','<?php echo e($message->_id); ?>');</script></div>
                </div>
            </div>
		
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
<script type="text/javascript">
$('.att-player').click(function(){
  console.log('clicked');
});
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/xhr/messages.blade.php ENDPATH**/ ?>