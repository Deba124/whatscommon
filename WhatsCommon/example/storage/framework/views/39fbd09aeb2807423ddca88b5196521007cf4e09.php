<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly">
  	<div class="container-fluid">
    	<div class="settingBody">
	      	<div class="position-relative settingFlip">
	          	<div class="leftSlidePan closePan">
	            	<a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
		            <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
		              <?php echo $__env->make('inc.life_event_left_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		            </div>
	          	</div>
	          	<div class="midPan">
		            <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
		              	<div class="row infoBodyRow">
			                <div class="col-md-5 infoBodyCol">
			                    <div class="innerHome">
			                    	<a href="<?php echo e(url('home')); ?>"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
			                    </div>
			                  	<div class="feedbackImg">
				                    <div class="personalImgAll text-center">
				                      	<img src="new-design/img/services9.png" class="img-fluid" alt="" style="max-width: 125px;margin-top: 25%;">
				                      	<div class="personalImgTitle">Reverse Lookup</div>
				                    </div>
			                  	</div>
			                </div>
			                <div class="col-md-7 infoBodyCol eventCol midHightControl">
                  <div class="feedbackRight windowHeight windowHeightMid" style="height: 445px;">
			                  		<div class="reverseLookupResult reverseLookupOnlyOne ">
                        
                        <div class="reverseLookupMatch">
                          <div class="matchHolder matchHolderShadow semiLostFoundForm">
                            <!-- <div class="semiTabBanner">
                              <img src="new-design/img/lookup.png" class="img-fluid" alt="">
                            </div> -->
                            <form class="xhr_form" method="post" action="api/reverse-lookup" id="reverse-lookup">
                            	<div class="eventColHeading">What </div><img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png" style="right:0px;">
                              <div class="whenTitleText">
                                <div class="whenTitle">Quick Search</div>
                                <div class="whenText">Try to search for someone using the single information you have</div>
                              </div>
                              <input type="hidden" name="user_id" value="<?php echo e(Session::get('userdata')['id']); ?>">
                              <div class="eventColHeading">What</div>
                              <div class="whatForm">
                                <div class="form-group topFormIcon">
                                  <label for="">Email</label>
                                  <input type="text" class="form-control formMail" id="" placeholder="Email Address" name="email">
                                  <p id="email_error" class="text-danger"></p>
                                </div>
                                <div class="form-group topFormIcon">
                                  <label for="">Phone</label>
                                  <input type="text" class="form-control formPh" id="" placeholder="Phone" name="phone">
                                  <p id="phone_error" class="text-danger"></p>
                                </div>
                              </div>
                              <div class="eventColHeading">When</div>
                              <div class="whatForm whereFrom">
                                <label for="">Birthday</label>
                                <div class="birthdayDate">
                                  <div class="form-group birthdayMonth">
                                    <label for="number">From</label>
                                    <select class="form-control formMonth" id="" name="when_from_month">
                                      <option value="" hidden>Month</option>
                                      <option value="01">January</option>
                                      <option value="02">February</option>
                                      <option value="03">March</option>
                                      <option value="04">April</option>
                                      <option value="05">May</option>
                                      <option value="06">June</option>
                                      <option value="07">July</option>
                                      <option value="08">August</option>
                                      <option value="09">September</option>
                                      <option value="10">October</option>
                                      <option value="11">November</option>
                                      <option value="12">December</option>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group birthdayDay">
                                    <label for="number">Day</label>
                                    <select class="form-control" id="" name="when_from_day">
                                      <option value="" hidden>DD</option>
                                      <?php for($i=1;$i<=31;$i++): ?>
                                        <?php
                                          $j = sprintf('%02d', $i)
                                        ?>
                                       <option value="<?php echo e($j); ?>"><?php echo e($j); ?></option>
                                      <?php endfor; ?>
                                    </select>
                                    <img src="img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group birthdayYear">
                                    <label for="number">Year</label>
                                    <select class="form-control" id="" name="when_from_year">
                                      <option value="" hidden>YYYY</option>
                                    <?php for($i = 1900; $i <= date('Y'); $i++): ?>
                                      <option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                                    <?php endfor; ?>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                </div>
                              </div>
                              <div class="eventColHeading">Where</div>
                              <div class="whatForm whereFrom">
                                <div class="form-group">
                                  <label for="">Country</label>
                                  <select class="form-control formCountry" name="country" id="country">
                              <option value="" hidden>Select</option>
            <?php if($countries): ?>
              <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($country->country_name); ?>" data-id="<?php echo e($country->_country_id); ?>"><?php echo e($country->country_name); ?></option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                                <div class="form-group">
                                  <label for="">State/County/Province</label>
                                  <select class="form-control formState" name="state" id="province">
                                    <option value="" hidden>Select</option>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                                <div class="form-group">
                                  <label for="">City</label>
                                  <select class="form-control formCity" name="city" id="city">
                                    <option value="" hidden>Select</option>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                                <div class="form-group">
                                  <label for="">Street Address</label>
                                  <input type="text" class="form-control formAddress" id="" placeholder="123 abc Ave." name="street">
                                </div>
                                <div class="form-group">
                                  <label for="">ZIP</label>
                                  <input type="text" class="form-control formZip" id="" placeholder="ZIP" name="zip">
                                </div>
                              </div>

                              <div class="whatForm whatFormBtnAll">
                                <div class="btn-Edit-save btn-feedback btn-premium">
                                  <button class="btn btn-Edit btn-save btn-search searchReverseLookup" type="submit">Search</button>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>

                      <div class="reverseLookupResult reverseLookupOnlyTwo removeMainCenter" id="reverse-lookup-div">
                        <!-- <div class="addBankBackIcon reverseLookupBackTwo text-left">
                          <img src="new-design/img/back.png" class="img-fluid my-0" alt="">
                        </div>
                        <div class="connectionsDetailsInfo">
                          <div class="connectionsDetailsInfoName">What’sCommon</div>
                          <div class="connectionsDetailsInfoEvents" style="margin-bottom: -6px;">
                            <span class="eventsText orangeText mr-0">Reverse Lookup</span>
                          </div>
                        </div> -->
                      </div>
			                  	</div>
			                </div>
		              	</div>
		            </div>
	          	</div>
	      	</div>
    	</div>
  	</div>
</div>
<!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Reverse Lookup</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Reverse Lookup</label>
            <p>Tring to match a phone number or maybe a strange email address even a
physical address to a person who has shared those detail here? Could be a old
number someone has entered in here that matches their name with a current
number…This is where you can find a match.</p>
          </div>
          
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).ready(function(){
  $(".enterFor").click(function(){
        $(".enterFor").addClass("activeTimeOption");
        $(".searchFor").removeClass("activeTimeOption");
        $('#search_for').val(0);
    });
    $(".searchFor").click(function(){
        $(".searchFor").addClass("activeTimeOption");
        $(".enterFor").removeClass("activeTimeOption");
        $('#search_for').val(1);
    });
});
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/life_events/reverse_lookup.blade.php ENDPATH**/ ?>