<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                <div class="leftSlidePan closePan">
                  <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                  <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                  	<div class="settingLeftTitle">Setting</div>
                  	<ul class="settingMenu mb-0 list-unstyled">
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('settings')); ?>"><img src="new-design/img/profileIcon.png" class="img-fluid menuIcon" alt="">Profile</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('financial-settings')); ?>"><img src="new-design/img/financial.png" class="img-fluid menuIcon" alt="">Financial Settings</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('change-password')); ?>"><img src="new-design/img/change-password.png" class="img-fluid menuIcon" alt="">Change Password</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('security')); ?>"><img src="new-design/img/security.png" class="img-fluid menuIcon" alt="">Security and Privacy</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('blocked-users')); ?>"><img src="new-design/img/blocking.png" class="img-fluid menuIcon" alt="">Blocking</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu settingMenuActive" href="<?php echo e(url('notification-settings')); ?>"><img src="new-design/img/notifications-a.png" class="img-fluid menuIcon" alt="">Notifications</a>
                  		</li>
                  		
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('message-settings')); ?>"><img src="new-design/img/message-setting.png" class="img-fluid menuIcon" alt="">Message Setting</a>
                  		</li>
                  	</ul>
                  </div>
                </div>
                <div class="midPan">
                  <div class="connectionsBody windowHeight windowHeightMid linkAcc linkAccNoti">
                    <div class="bankDetailsTitle">Notifications</div>
                    <div class="feedbackTitle2">By turning on the toggle buttons, you’re allowing Whatscommon to send you notifications with a specific cetegory you choose to turn on.
                    </div>
                    <div class="bankcardTab">
                      
                        

                      <div class="">
                        <div id="" class="">
                        <form method="post" class="xhr_form" action="set-notification-setting" id="set-notification-setting">
                            <?php echo csrf_field(); ?>
                          <div class="profileForm bankForm">
                            <ul class="linkAccountList">
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        <img src="img/notiMsg.png" class="img-fluid" alt="">
                                        Messages
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" <?php if($notifications && $notifications->ns_message==1): ?> checked <?php endif; ?> name="message_setting" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        <img src="img/notiShares.png" class="img-fluid" alt="">
                                        Shares
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" <?php if($notifications && $notifications->ns_shares==1): ?> checked <?php endif; ?> name="share_setting" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        <img src="img/notiConnections.png" class="img-fluid" alt="">
                                        New Connections
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" <?php if($notifications && $notifications->ns_new_connection==1): ?> checked <?php endif; ?> name="new_connection_setting" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        <img src="img/notiMatch.png" class="img-fluid" alt="">
                                        New Match
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" <?php if($notifications && $notifications->ns_new_match==1): ?> checked <?php endif; ?> name="new_match_setting" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                            </ul>
                            
                          </div>
                      </form>
                        </div>
                        
                      </div>

                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
$(document).on('change','.settings', function(){
	$('.xhr_form').submit();
});
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/notification_settings.blade.php ENDPATH**/ ?>