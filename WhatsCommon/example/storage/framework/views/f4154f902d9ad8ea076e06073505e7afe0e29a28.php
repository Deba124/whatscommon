<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly">
  <div class="container-fluid">
    <div class="settingBody">
      <div class="position-relative settingFlip">
          <div class="leftSlidePan closePan">
            <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
            <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
              <?php echo $__env->make('inc.life_event_left_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
          </div>
          <div class="midPan">
            <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
              <div class="row infoBodyRow">
                <div class="col-md-5 infoBodyCol">
                    <div class="innerHome">
                      <a href="<?php echo e(url('home')); ?>"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                    </div>
                  <div class="feedbackImg">
                    <div class="personalImgAll">
                      <img src="new-design/img/carrersImg.png" class="img-fluid" alt="">
                      <div class="personalImgTitle">Careers / Job / Work</div>
                    </div>
                  </div>
                </div>
                <div class="col-md-7 infoBodyCol eventCol midHightControl">
                  <div class="feedbackRight windowHeight windowHeightMid" style="height: 445px !important;">

                      <form class="xhr_form" method="post" action="api/life-event-upload" id="life-event-upload">
                        <?php echo csrf_field(); ?>
                        <div class="eventColHeading">What </div><img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                        <input type="hidden" name="type_id" value="7">
                        <input type="hidden" name="user_id" value="<?php echo e(Session::get('userdata')['id']); ?>">
                        <div class="whatForm">
                          <div class="whatReasonAll">
                            <div class="form-group topFormIcon">
                              <label for="">Business</label>
                              <input type="text" class="form-control formBusiness" id="" placeholder="Name" name="career_name">
                            </div>
                            <div class="form-group topFormIcon">
                              <label for="">Category</label>
                              <select class="form-control formCategory" name="career_category">
                                <option value="" hidden>Select</option>
            		<?php if($industries): ?>
                    	<?php $__currentLoopData = $industries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $industry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    			<option option="<?php echo e($industry->industry_name); ?>"><?php echo e($industry->industry_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group topFormIcon">
                              <label for="">Title</label>
                              <select class="form-control formTitle2" name="career_title">
                                <option value="" hidden>Select</option>
                    <?php if($occupations): ?>
                    	<?php $__currentLoopData = $occupations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $occupation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    			<option option="<?php echo e($occupation->occupation_name); ?>"><?php echo e($occupation->occupation_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group topFormIcon">
                              <label for="">Level</label>
                              <select class="form-control formLevel" name="career_level">
                                <option value="" hidden>Select</option>
                    <?php if($career_levels): ?>
                    	<?php $__currentLoopData = $career_levels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    			<option option="<?php echo e($cl->cl_name); ?>"><?php echo e($cl->cl_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            
                          </div>
                        </div>
                        <div class="eventColHeading">Where <img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png"></div>
                        <div class="whatForm whereFrom">
                          <div class="form-group">
                            <label for="">Country</label>
                            <select class="form-control formCountry" name="country" id="country">
                              <option value="" hidden>Select</option>
            <?php if($countries): ?>
              <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($country->country_name); ?>" data-id="<?php echo e($country->_country_id); ?>"><?php echo e($country->country_name); ?></option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
                              
                            </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">State/County/Province</label>
                            <select class="form-control formState" name="state" id="province">
                              <option value="" hidden>Select</option>
                              
                            </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">City</label>
                            <select class="form-control formCity" name="city" id="city">
                              <option value="" hidden>Select</option>
                            </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">Street Address</label>
                            <input type="text" class="form-control formAddress" id="" placeholder="123 abc Ave." name="street">
                          </div>
                          <div class="form-group">
                            <label for="">ZIP</label>
                            <input type="text" class="form-control formZip" id="" placeholder="ZIP" name="zip">
                          </div>
                        </div>
                        <div class="eventColHeading">When </div>
                        <div class="whatForm whereFrom">
                          <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">From</label>
                              <select class="form-control formMonth" id="" name="when_from_month">
                                <option value="" hidden>Month</option>
                                <option value="01">January</option>
                                <option value="02">February</option>
                                <option value="03">March</option>
                                <option value="04">April</option>
                                <option value="05">May</option>
                                <option value="06">June</option>
                                <option value="07">July</option>
                                <option value="08">August</option>
                                <option value="09">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay">
                              <label for="number">Day</label>
                              <select class="form-control" id="" name="when_from_day">
                                <option value="" hidden>DD</option>
                                <?php for($i=1;$i<=31;$i++): ?>
                                  <?php
                                    $j = sprintf('%02d', $i)
                                  ?>
                                 <option value="<?php echo e($j); ?>"><?php echo e($j); ?></option>
                                <?php endfor; ?>
                                
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayYear">
                              <label for="number">Year</label>
                              <select class="form-control" id="" name="when_from_year">
                                <option value="" hidden>YYYY</option>
                              <?php for($i = 1900; $i <= date('Y'); $i++): ?>
                                <option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                              <?php endfor; ?>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                          </div>
                          <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">To</label>
                              <select class="form-control formMonth" id="" name="when_to_month">
                                <option value="" hidden>Month</option>
                                <option value="01">January</option>
                                <option value="02">February</option>
                                <option value="03">March</option>
                                <option value="04">April</option>
                                <option value="05">May</option>
                                <option value="06">June</option>
                                <option value="07">July</option>
                                <option value="08">August</option>
                                <option value="09">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay">
                              <label for="number">Day</label>
                              <select class="form-control" id="" name="when_to_day">
                                <option value="" hidden>DD</option>
                                <?php for($i=1;$i<=31;$i++): ?>
                                  <?php
                                    $j = sprintf('%02d', $i)
                                  ?>
                                 <option value="<?php echo e($j); ?>"><?php echo e($j); ?></option>
                                <?php endfor; ?>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayYear">
                              <label for="number">Year</label>
                              <select class="form-control" id="" name="when_to_year">
                                <option value="" hidden>YYYY</option>
                              <?php for($i = 1900; $i <= date('Y'); $i++): ?>
                                <option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                              <?php endfor; ?>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                          </div>
                          
                        </div>
                        <div class="eventColHeading">Who, What, Where, When, Why </div>
                        <div class="whatForm whereFrom">
                          
                          <div class="myAllEvents" id="keywords_div"></div>
	                        <input type="hidden" id="keywords" name="event_keywords" value="">
	                        <div class="input-group lifeEvent">
                          	<input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
                          	<div class="input-group-append">
                            	<a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
                           	</div>
	                        </div>
                          <div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                        </div>
                        <?php echo $__env->make('inc.life_event_img_upload', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                        
                      </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Careers/Jobs/Works</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Careers/Jobs/Works</label>
            <p>Enter your business or job details; it can be a company name. It's a way to be matched with your past employees or workmates.</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>This is the location of your business or work. We all remember our work address!</p>
          </div>
          <div class="form-group">
            <label>WHEN:</label>
            <p>The day you started your job and the day you resigned or</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="" hidden="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/life_events/work.blade.php ENDPATH**/ ?>