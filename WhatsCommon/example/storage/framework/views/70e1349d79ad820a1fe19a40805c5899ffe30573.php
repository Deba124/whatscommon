<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly innerBodyModify">
  	<div class="container-fluid">
    	<div class="settingBody">
	      	<div class="position-relative settingFlip">
	          	<div class="leftSlidePan closePan">
	            	<a href="#" class="panelslideOpenButton"><img src="<?php echo e(url('new-design/img/life_event_menu_arrow.png')); ?>" alt=""></a>
	            	<div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
	              		<?php echo $__env->make('inc.life_event_left_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	            	</div>
	          	</div>
	          	<div class="midPan">
                    <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                      <div class="row infoBodyRow">
                        <div class="col-md-5 infoBodyCol">
                            <div class="innerHome">
                              <a href="<?php echo e(url('home')); ?>"><img src="<?php echo e(('new-design/img/home.png')); ?>" class="img-fluid" alt=""></a>
                            </div>
                          <div class="feedbackImg">
                            <div class="personalImgAll">
                              <img src="<?php echo e(url('new-design/img/activitiesImg.png')); ?>" class="img-fluid" alt="">
                              <div class="personalImgTitle">Activities</div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7 infoBodyCol eventCol midHightControl">
                          <div class="feedbackRight windowHeight windowHeightMid">

                            <form class="xhr_form" method="post" action="api/life-event-upload" id="life-event-upload">
                        	<?php echo csrf_field(); ?>
                                <div class="eventColHeading">What</div>
                                <input type="hidden" name="type_id" value="12">
                        		<input type="hidden" name="user_id" value="<?php echo e(Session::get('userdata')['id']); ?>">
                                <div class="whatForm">
                                  <div class="form-group">
                                    <label for="">Type</label>
                                    <select class="form-control selectCenter" name="sub_category_id" id="sub_category_id">
                                      <option value="">Select Type</option>
                          <?php if($life_event_sub_categories): ?>
                            <?php $__currentLoopData = $life_event_sub_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($category->id); ?>" data-id="<?php echo e($category->id); ?>" data-activity_types="<?php echo e($category->activity_types); ?>"><?php echo e($category->sub_cat_type_name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          <?php endif; ?>
                                    </select>
                                  </div>
                                  <div class="form-group" id="activity_types_div">
                                    <label for="">Activity</label>
                                    <select class="form-control selectCenter" name="activity_type" id="activity_type">
                                      <option value="">Select Activity</option>
                                    </select>
                                  </div>
                                  
                                  <div class="whatReasonAll">
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <label for="">Game</label>
                                          <select class="form-control  formGame " id="game_name" name="game_name">
                                            <option value="">Select Game</option>
                            <?php if($games): ?>
                              <?php $__currentLoopData = $games; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $game): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option option="<?php echo e($game->game_name); ?>" data-id="<?php echo e($game->_game_id); ?>" data-game_teams="<?php echo e($game->game_teams); ?>"><?php echo e($game->game_name); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                                            <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                        
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <label for="">Team</label>
                                          <div class="selectCenterWithIcon" id="game_teams_div">
                                      <select class="form-control  formTeam " id="team_name" name="team_name">
                                        <option value="">Select Team/Group</option>
                                      </select>
                                    </div>
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <label for="">Entertainer</label>
                                          <input type="text" class="form-control formEntertainer" name="entertainer" id="" placeholder="Type here" name="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <label for="">Key Person</label>
                                          <input type="text" class="form-control formPerson" name="key_person" id="" placeholder="Type here" name="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <label for="">Row</label>
                                          <input type="text" class="form-control formRow" name="row" id="" placeholder="Type here" name="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <label for="">Seat</label>
                                          <input type="text" class="form-control formSeat" id="" name="seat" placeholder="Type here" name="">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="eventColHeading">Where</div>
                                <div class="whatForm whereFrom">
                          <div class="form-group" id="location_types_div">
                            <label for="">Location Type</label>
                            <select class="form-control formLocationType" id="location_type" name="location_type">
                              <option value="">Select</option>
                    <?php if($location_types): ?>
                    	<?php $__currentLoopData = $location_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    			<option option="<?php echo e($lt->lt_name); ?>"><?php echo e($lt->lt_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                              
                            </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">Location Name</label>
                            <input type="text" class="form-control formLocation" id="" placeholder="Type here" name="location_name">
                          </div>
                          <div class="form-group">
                            <label for="">Country</label>
                            <select class="form-control formCountry" name="country" id="country">
                              <option value="">Select</option>
            <?php if($countries): ?>
              <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($country->country_name); ?>" data-id="<?php echo e($country->_country_id); ?>"><?php echo e($country->country_name); ?></option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
                            </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">State/County/Province</label>
                            <select class="form-control formState" name="state" id="province">
                                <option value="">Select</option>
                            </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">City</label>
                            <select class="form-control formCity" name="city" id="city">
                                <option value="">Select</option>
                            </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">Street Address</label>
                            <input type="text" class="form-control formAddress" id="" placeholder="123 abc Ave." name="street">
                          </div>
                          <div class="form-group">
                            <label for="">ZIP</label>
                            <input type="text" class="form-control formZip" id="" placeholder="ZIP" name="zip">
                          </div>
                        </div>
                                <div class="eventColHeading">When</div>
                                <div class="whatForm whereFrom">
                          <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">From</label>
                              <select class="form-control formMonth" id="" name="when_from_month">
                                <option value="">Month</option>
                                <option value="01">January</option>
                                <option value="02">February</option>
                                <option value="03">March</option>
                                <option value="04">April</option>
                                <option value="05">May</option>
                                <option value="06">June</option>
                                <option value="07">July</option>
                                <option value="08">August</option>
                                <option value="09">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay">
                              <label for="number">Day</label>
                              <select class="form-control" id="" name="when_from_day">
                                        <option value="">DD</option>
                                        <?php for($i=1;$i<=31;$i++): ?>
                                          <?php
                                            $j = sprintf('%02d', $i)
                                          ?>
                                         <option value="<?php echo e($j); ?>"><?php echo e($j); ?></option>
                                        <?php endfor; ?>
                                        
                                      </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayYear">
                              <label for="number">Year</label>
                              <select class="form-control" id="" name="when_from_year">
                                        <option value="">YYYY</option>
                                      <?php for($i = date('Y'); $i >= 1900; $i--): ?>
                                        <option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                                      <?php endfor; ?>
                                      </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                          </div>
                          <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">To</label>
                              <select class="form-control formMonth" id="" name="when_to_month">
                                        <option value="">Month</option>
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                      </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay">
                              <label for="number">Day</label>
                              <select class="form-control" id="" name="when_to_day">
                                <option value="">DD</option>
                                <?php for($i=1;$i<=31;$i++): ?>
                                  <?php
                                    $j = sprintf('%02d', $i)
                                  ?>
                                 <option value="<?php echo e($j); ?>"><?php echo e($j); ?></option>
                                <?php endfor; ?>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayYear">
                              <label for="number">Year</label>
                              <select class="form-control" id="" name="when_to_year">
                                <option value="">YYYY</option>
                              <?php for($i = date('Y'); $i >= 1900; $i--): ?>
                                <option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                              <?php endfor; ?>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                          </div>
                          <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">Departure Time</label>
                              <select class="form-control formTime" id="" name="dep_time">
                                <option value="">00:00</option>
                                <option value="1:00">1:00</option>
                                <option value="1:30">1:30</option>
                                <option value="2:00">2:00</option>
                                <option value="2:30">2:30</option>
                                <option value="3:00">3:00</option>
                                <option value="3:30">3:30</option>
                                <option value="4:00">4:00</option>
                                <option value="4:30">4:30</option>
                                <option value="5:00">5:00</option>
                                <option value="5:30">5:30</option>
                                <option value="6:00">6:00</option>
                                <option value="6:30">6:30</option>
                                <option value="7:00">7:00</option>
                                <option value="7:30">7:30</option>
                                <option value="8:00">8:00</option>
                                <option value="8:30">8:30</option>
                                <option value="9:00">9:00</option>
                                <option value="9:30">9:30</option>
                                <option value="10:00">10:00</option>
                                <option value="10:30">10:30</option>
                                <option value="11:00">11:00</option>
                                <option value="11:30">11:30</option>
                                <option value="12:00">12:00</option>
                                <option value="12:30">12:30</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay timeOptionSet">
                              <div class="form-control timeOptionOnly activeTimeOption" id="dep_time_am">AM</div>
                              <div class="form-control timeOptionOnly" id="dep_time_pm" style="border-top-left-radius: 0 !important;border-bottom-left-radius: 0 !important;">PM</div>
                              <input type="hidden" name="dep_time_ampm" id="" value="AM" />
                            </div>
                            <div class="form-group birthdayYear"></div>
                          </div>
                          <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">Arrival Time</label>
                              <select class="form-control formTime" id="" name="arr_time">
                                <option value="">00:00</option>
                                <option value="1:00">1:00</option>
                                <option value="1:30">1:30</option>
                                <option value="2:00">2:00</option>
                                <option value="2:30">2:30</option>
                                <option value="3:00">3:00</option>
                                <option value="3:30">3:30</option>
                                <option value="4:00">4:00</option>
                                <option value="4:30">4:30</option>
                                <option value="5:00">5:00</option>
                                <option value="5:30">5:30</option>
                                <option value="6:00">6:00</option>
                                <option value="6:30">6:30</option>
                                <option value="7:00">7:00</option>
                                <option value="7:30">7:30</option>
                                <option value="8:00">8:00</option>
                                <option value="8:30">8:30</option>
                                <option value="9:00">9:00</option>
                                <option value="9:30">9:30</option>
                                <option value="10:00">10:00</option>
                                <option value="10:30">10:30</option>
                                <option value="11:00">11:00</option>
                                <option value="11:30">11:30</option>
                                <option value="12:00">12:00</option>
                                <option value="12:30">12:30</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay timeOptionSet">
                              <div class="form-control timeOptionOnly activeTimeOption" id="arr_time_am">AM</div>
                              <div class="form-control timeOptionOnly" id="arr_time_pm" style="border-top-left-radius: 0 !important;border-bottom-left-radius: 0 !important;">PM</div>
                              <input type="hidden" name="arr_time_ampm" id="arr_time_ampm" value="AM">
                            </div>
                            <div class="form-group birthdayYear"></div>
                          </div>
                        </div>
                                <div class="eventColHeading">Who, What, Where, When, Why</div>
                                <div class="whatForm whereFrom">
                          
                          			<div class="myAllEvents" id="keywords_div"></div>
	                        		<input type="hidden" id="keywords" value="" name="event_keywords">
	                        		<div class="input-group lifeEvent">
	                        			<input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
	                        			<div class="input-group-append">
	                            			<a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
                            			</div>
                          			</div>
                          			<div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                        		</div>
                        
                        		<?php echo $__env->make('inc.life_event_img_upload', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                            </form>
                          </div>
                        </div>
                      	</div>
                    </div>
                </div>
	        </div>
	    </div>
	</div>
</div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).ready(function(){
  $("#dep_time_am").click(function(){
      $("#dep_time_am").addClass("activeTimeOption");
      $("#dep_time_pm").removeClass("activeTimeOption");
      $('#dep_time_ampm').val('AM');
  });
  $("#dep_time_pm").click(function(){
    $("#dep_time_pm").addClass("activeTimeOption");
    $("#dep_time_am").removeClass("activeTimeOption");
    $('#dep_time_ampm').val('PM');
  });

  $("#arr_time_am").click(function(){
      $("#arr_time_am").addClass("activeTimeOption");
      $("#arr_time_pm").removeClass("activeTimeOption");
      $('#arr_time_ampm').val('AM');
  });
  $("#arr_time_pm").click(function(){
    $("#arr_time_pm").addClass("activeTimeOption");
    $("#arr_time_am").removeClass("activeTimeOption");
    $('#arr_time_ampm').val('PM');
  });

  $(".enterFor").click(function(){
    $(".enterFor").addClass("activeTimeOption");
    $(".searchFor").removeClass("activeTimeOption");
    $('#search_for').val(0);
  });
  $(".searchFor").click(function(){
    $(".searchFor").addClass("activeTimeOption");
    $(".enterFor").removeClass("activeTimeOption");
    $('#search_for').val(1);
  });

});
$(document).on('change','#sub_category_id',function(){
  
  var activity_types = $(this).find(':selected').data('activity_types');
  /*console.log('ranks => ');
  console.log(ranks);*/
  var team_name = '<option value="">Select </option>';
  if(activity_types){

    $.each( activity_types, function( index, rank ) {
      team_name += '<option value="'+rank.act_type_name+'">'+rank.act_type_name+'</option>';
    });
  }
  /*team_name += '<option value="add">Add</option>';*/
  $('#activity_type').html(team_name);
});
$(document).on('change','#game_name',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '<?php echo e(Session::get("userdata")["id"]); ?>';
    open_modal('add-master','user_id='+user_id+'&db_name=games&column_name=game_name&status_column=game_is_active');
  }else{
    var game_teams = $(this).find(':selected').data('game_teams');
    /*console.log('ranks => ');
    console.log(ranks);*/
    var team_name = '<option value="">Select </option>';
    if(game_teams){

      $.each( game_teams, function( index, rank ) {
        team_name += '<option value="'+rank.team_name+'">'+rank.team_name+'</option>';
      });
    }
    team_name += '<option value="add">Add</option>';
    $('#team_name').html(team_name);
  }
  
});
$(document).on('change', '#team_name', function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '<?php echo e(Session::get("userdata")["id"]); ?>';
    var game_id = $('#game_name').find(':selected').data('id');
    open_modal('add-master','user_id='+user_id+'&db_name=game_teams&column_name=team_name&status_column=team_is_active&dependent_name=team_gameid&dependent_value='+game_id+'&is_dependent=1');
  }
});
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/life_events/activities.blade.php ENDPATH**/ ?>