<?php if($db_name=='vehicle_model'): ?>
<label for="">Model</label>
<select class="form-control formModel" name="vehicle_model" id="vehicle_model">
    <option value="" hidden="">Select Model</option>
    <?php $__currentLoopData = $vehicle_model; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $model): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php
    	$selected = (isset($inserted_id) && $inserted_id==$model->_model_id) ? 'selected' : '';
    ?> 
    <option value="<?php echo e($model->model_name); ?>" <?php echo e($selected); ?> data-id="<?php echo e($model->_model_id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($model->model_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <option value="add">Add</option>
</select>
<img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
<?php elseif($db_name=='games'): ?>
	<select class="form-control selectCenter formGame " id="game_name" name="game_name">
    	<option value="" hidden="">Select Game or Activity</option>
   	<?php if($games): ?>
      	<?php $__currentLoopData = $games; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $game): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	      	<?php
		    	$selected = (isset($inserted_id) && $inserted_id==$game->_game_id) ? 'selected' : '';
		    ?>
        <option option="<?php echo e($game->game_name); ?>" data-game_teams="<?php echo e($game->game_teams); ?>" <?php echo e($selected); ?> data-id="<?php echo e($game->_game_id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($game->game_name); ?></option>
      	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
        <option value="add">Add</option>
    </select>
<?php elseif($db_name=='game_teams'): ?>
	<select class="form-control selectCenter formTeam " id="team_name" name="team_name">
        <option value="" hidden="">Select Team/Group</option>
        <?php if($game_teams): ?>
      	<?php $__currentLoopData = $game_teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	      	<?php
		    	$selected = (isset($inserted_id) && $inserted_id==$team->_team_id) ? 'selected' : '';
		    ?>
        <option option="<?php echo e($team->team_name); ?>" <?php echo e($selected); ?> data-id="<?php echo e($team->_team_id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($team->team_name); ?></option>
      	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
        <option value="add">Add</option>
    </select>
<?php endif; ?>

<?php if($db_name=='genders'): ?>
  <label for="">Gender</label>
  <select class="form-control formGender" name="gender" id="gender">
    <option value="" hidden>Select</option>
  <?php if($genders): ?>
    <?php $__currentLoopData = $genders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gender): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <?php
        $selected = (isset($inserted_id) && $inserted_id==$gender->_gender_id) ? 'selected' : '';
      ?>
    <option option="<?php echo e($gender->gender_name); ?>" <?php echo e($selected); ?> data-id="<?php echo e($gender->_gender_id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($gender->gender_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
    <option value="add">Add</option>
  </select>
<?php endif; ?>

<?php if($db_name=='place_of_birth'): ?>
  <label for="">Place of Birth</label>
  <select class="form-control formBarthPlace" name="birth_place" id="place_of_birth">
    <option value="" hidden>Select</option>
  <?php if($place_of_birth): ?>
    <?php $__currentLoopData = $place_of_birth; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $place): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <?php
        $selected = (isset($inserted_id) && $inserted_id==$place->_place_id) ? 'selected' : '';
      ?>
    <option option="<?php echo e($place->place_name); ?>" <?php echo e($selected); ?> data-id="<?php echo e($place->_place_id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($place->place_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
    <option value="add">Add</option>
  </select>
<?php endif; ?>

<?php if($db_name=='adoption_status'): ?>
  <label for="">Status</label>
  <select class="form-control formBarthPlace" name="adoption_status" id="adoption_status">
    <option value="" hidden>Select</option>
  <?php if($adoption_status): ?>
    <?php $__currentLoopData = $adoption_status; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <?php
        $selected = (isset($inserted_id) && $inserted_id==$status->_status_id) ? 'selected' : '';
      ?>
    <option option="<?php echo e($status->status_name); ?>" <?php echo e($selected); ?> data-id="<?php echo e($status->_status_id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($status->status_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
    <option value="add">Add</option>
  </select>
<?php endif; ?>

<?php if($db_name=='home_types'): ?>
  <label for="">Type of Home</label>
  <select class="form-control selectCenter" name="type_of_home" id="home_types">
    <option value="" hidden>Select</option>
  <?php if($home_types): ?>
    <?php $__currentLoopData = $home_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $home_type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <?php
        $selected = (isset($inserted_id) && $inserted_id==$home_type->id) ? 'selected' : '';
      ?>
    <option option="<?php echo e($home_type->home_type_name); ?>" <?php echo e($selected); ?> data-id="<?php echo e($home_type->id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($home_type->home_type_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
    <option value="add">Add</option>
  </select>
<?php endif; ?>

<?php if($db_name=='home_styles'): ?>
  <label for="">Style</label>
  <select class="form-control selectCenter" name="home_style" id="home_styles">
    <option value="" >Select</option>
  <?php if($home_styles): ?>
    <?php $__currentLoopData = $home_styles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $home_style): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <?php
        $selected = (isset($inserted_id) && $inserted_id==$home_style->id) ? 'selected' : '';
      ?>
    <option option="<?php echo e($home_style->home_style_name); ?>" <?php echo e($selected); ?> data-id="<?php echo e($home_style->id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($home_style->home_style_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
    <option value="add">Add</option>
  </select>
<?php endif; ?>

<?php if($db_name=='vehicle_makers'): ?>
  <label for="">Make</label>
  <select class="form-control formMake" id="vehicle_maker" name="vehicle_maker">
    <option value="" >Select</option>
  <?php if($vehicle_makers): ?>
    <?php $__currentLoopData = $vehicle_makers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $maker): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option option="<?php echo e($maker->maker_name); ?>" data-id="<?php echo e($maker->_maker_id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($maker->maker_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
    <option value="add">Add</option>
  </select>
<?php endif; ?>

<?php if($db_name=='occupations'): ?>
  <label for="">Category</label>
  <select class="form-control formCategory" id="category" name="category">
    <option value="">Select</option>
  <?php if($occupations): ?>
    <?php $__currentLoopData = $occupations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $occupation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option option="<?php echo e($occupation->occupation_name); ?>" data-id="<?php echo e($occupation->_occupation_id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($occupation->occupation_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
    <option value="add">Add</option>
  </select>
<?php endif; ?>

<?php if($db_name=='industries'): ?>
  <label for="">Industry</label>
  <select class="form-control formIndustry" id="industry" name="industry">
    <option value="" >Select</option>
  <?php if($industries): ?>
    <?php $__currentLoopData = $industries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $industry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option option="<?php echo e($industry->industry_name); ?>" data-id="<?php echo e($industry->_industry_id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($industry->industry_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
    <option value="add">Add</option>
  </select>
<?php endif; ?>

<?php if($db_name=='marital_status'): ?>
  <label for="">Status</label>
  <select class="form-control formStatus" id="marital_status" name="marital_status">
    <option value="" >Select</option>
  <?php if($marital_status): ?>
    <?php $__currentLoopData = $marital_status; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $industry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option option="<?php echo e($industry->ms_name); ?>" data-id="<?php echo e($industry->_ms_id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($industry->ms_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
    <option value="add">Add</option>
  </select>
<?php endif; ?>

<?php if($db_name=='relations'): ?>
  <label for="">Relation</label>
  <select class="form-control formRelationship" id="relations" name="relationship">
    <option value="" >Select</option>
  <?php if($relations): ?>
    <?php $__currentLoopData = $relations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $industry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option option="<?php echo e($industry->relation_name); ?>" data-id="<?php echo e($industry->_relation_id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($industry->relation_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
    <option value="add">Add</option>
  </select>
<?php endif; ?>

<?php if($db_name=='ethnicities'): ?>
  <label for="">Ethnicity</label>
  <select class="form-control formEthnicity" id="ethnicity" name="ethnicity">
    <option value="" hidden>Select</option>
  <?php if($ethnicities): ?>
    <?php $__currentLoopData = $ethnicities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ethnicity): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option option="<?php echo e($ethnicity->ethnicity_name); ?>" data-id="<?php echo e($ethnicity->_ethnicity_id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($ethnicity->ethnicity_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
    <option value="add">Add</option>
  </select>
<?php endif; ?>

<?php if($db_name=='races'): ?>
  <label for="">Race</label>
  <select class="form-control formRace" name="race" id="race">
    <option value="" hidden>Select</option>
  <?php if($races): ?>
    <?php $__currentLoopData = $races; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $race): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option option="<?php echo e($race->race_name); ?>" data-id="<?php echo e($race->_race_id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($race->race_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
    <option value="add">Add</option>
  </select>
<?php endif; ?>

<?php if($db_name=='travel_modes'): ?>
  <label for="">Mode</label>
  <select class="form-control formAirport " id="travel_modes" name="airport">
    <option value="" hidden>Select</option>
  <?php if($travel_modes): ?>
    <?php $__currentLoopData = $travel_modes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mode): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option option="<?php echo e($mode->mode_name); ?>" data-id="<?php echo e($mode->_mode_id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($mode->mode_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
    <option value="add">Add</option>
  </select>
<?php endif; ?>

<?php if($db_name=='location_types'): ?>
  <label for="">Location Type</label>
  <select class="form-control formLocationType" id="location_type" name="location_type">
    <option value="" hidden>Select</option>
  <?php if($location_types): ?>
    <?php $__currentLoopData = $location_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option option="<?php echo e($lt->lt_name); ?>" data-id="<?php echo e($lt->_lt_id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($lt->lt_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
    <option value="add">Add</option>
  </select>
<?php endif; ?>

<?php if($db_name=='hair_colors'): ?>
  <label for="">Hair Color</label>
  <select class="form-control formHairColor" id="hair_color" name="hair_color">
    <option value="" hidden>Select</option>
  <?php if($hair_colors): ?>
    <?php $__currentLoopData = $hair_colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option option="<?php echo e($hc->hc_name); ?>" data-id="<?php echo e($hc->_hc_id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($hc->hc_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
    <option value="add">Add</option>
  </select>
<?php endif; ?>

<?php if($db_name=='eye_colors'): ?>
  <label for="">Eye Color</label>
  <select class="form-control formEyeColor" name="eye_color" id="eye_color">
    <option value="" hidden>Select</option>
  <?php if($eye_colors): ?>
    <?php $__currentLoopData = $eye_colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ec): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option option="<?php echo e($ec->ec_name); ?>" data-id="<?php echo e($ec->_ec_id); ?>" data-ins="<?php echo e((isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found'); ?>"><?php echo e($ec->ec_name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
    <option value="add">Add</option>
  </select>
<?php endif; ?><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/xhr/get_master.blade.php ENDPATH**/ ?>