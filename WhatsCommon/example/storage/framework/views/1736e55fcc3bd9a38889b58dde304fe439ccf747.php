<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly">
    <div class="container-fluid">
        <div class="settingBody">
            <div class="position-relative settingFlip">
                <div class="leftSlidePan <?php echo e(Request::get('menu')==1 ? '' : 'closePan'); ?>">
                	<a href="#" class="panelslideOpenButton <?php echo e(Request::get('menu')==1 ? 'panelslideCloseButton' : ''); ?>"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                  	<div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                  		<div class="settingLeftTitle">Information</div>
	                  	<ul class="settingMenu mb-0 list-unstyled">
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu" href="<?php echo e(url('feedback')); ?>"><img src="new-design/img/feedback.png" class="img-fluid menuIcon" alt="">Feedback</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu" href="<?php echo e(url('contact-us')); ?>"><img src="new-design/img/contact.png" class="img-fluid menuIcon" alt="">Contact Us</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu settingMenuActive" href="<?php echo e(url('about-us')); ?>"><img src="new-design/img/about-a.png" class="img-fluid menuIcon" alt="">About Us</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu" href="<?php echo e(url('premium-plans')); ?>"><img src="new-design/img/premium.png" class="img-fluid menuIcon" alt="">WC Premium</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu" href="<?php echo e(url('donate')); ?>"><img src="new-design/img/donate.png" class="img-fluid menuIcon" alt="">Voluntary Gift</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                          <a class="settingMenu " style="color: #ed1c24;" href="<?php echo e(url('logout')); ?>"><img src="<?php echo e(url('new-design/img/logout.png')); ?>" class="img-fluid menuIcon" alt="">Log Out</a>
	                        </li>
	                  	</ul>
                  	</div>
                  	<!-- <a href="<?php echo e(url('logout')); ?>" class="logOut" style="margin-top: -40px;"><img src="new-design/img/logout.png" class="img-fluid menuIcon" alt=""> Log Out</a> -->
                </div>
                <div class="midPan">
                  	<div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
	                    <div class="row infoBodyRow">
	                      	<div class="col-md-5 infoBodyCol">
	                        	<div class="innerHome">
	                            	<a href="<?php echo e(url('home')); ?>"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
	                          	</div>
	                        	<div class="feedbackImg">
	                          		<img src="new-design/img/aboutImg.png" class="img-fluid" alt="" style="width: 80%;">
	                        	</div>
	                      	</div>
	                      	<!--<div class="col-md-7 infoBodyCol aboutCol">-->
	                      	<div class="col-md-7 infoBodyCol aboutCol feedbackColFixed midHightControl">
	                        	<div class="feedbackRight windowHeight windowHeightMid">
	                          		<div class="feedbackTitle"><span><?php echo e($page_info->page_title); ?></span></div>
	                          
		                          	<div class="aboutText">
			                            
			                            <?php echo $page_info->page_content; ?>

			                        </div>  
	                        	</div>
	                      	</div>
	                    </div>
                  	</div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/about_us.blade.php ENDPATH**/ ?>