<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly">
  <div class="container-fluid">
    <div class="connectionsBody">
      <div class="position-relative connectionFlip">
        <div class="leftSlidePan closePan">
         
        </div>
        <div class="text-center windowHeight windowHeightMid">
          <div class="connectionsRight">
            <div class="tab-content">
              <div id="connection<?php echo e($connection->id); ?>" class="tab-pane active">
                <div class="row">

                      <div class="col-md-12 text-center mb-5">
                        <div class="connectionsDetailsAvtarImg" style="left: 44%;">
                          <img src="<?php echo e($connection->profile_photo_path ? $connection->profile_photo_path : 'new-design/img/profile_placeholder.jpg'); ?>" class="img-fluid" alt="">
                        </div>
                      </div>
                    </div>
                <div class="connectionsAllDetails">
                  <div class="connectionsAllDetailsTop">                  

                    <div class="connectionsDetailsInfo" style="width: 100%;">
                      <div class="connectionsDetailsInfoName"><?php echo e($connection->firstName); ?> <?php echo e($connection->lastName); ?></div>
                      <div class="connectionsDetailsInfoEvents myEvenntsAll" style="display: inline-flex;">
                        <div class="myEvennts">
                          <span class="eventsNumbers"><?php echo e($connection->connection_count); ?></span> <span class="eventsText">Connections</span>
                        </div>
                        <div>
                          <span class="eventsNumbers"><?php echo e($connection->life_events_count); ?></span> <span class="eventsText">Life Events</span>
                        </div>
                        <div>
                          <span class="eventsNumbers"><?php echo e($connection->matched_count); ?></span> <span class="eventsText">Event Matches</span>
                        </div>
                      </div>
                      <div class="connectionsDetailsInfoId" style="background: none;"><?php echo e('@'); ?><?php echo e($connection->username); ?></div>
                      <div class="connectionsDetailsPlace" style="background: none;"><?php echo e($connection->city_name); ?> <?php echo e(($connection->city_name && $connection->province_name) ? ',' : ''); ?> <?php echo e($connection->province_name); ?> <?php echo e(($connection->country_name && $connection->province_name) ? ',' : ''); ?> <?php echo e($connection->country_name); ?></div>
                      <?php if($connection->is_connected==0 && $connection->is_pending_connection==0 && $feed_data): ?>
                      <a class="btn btn-following" onclick="execute('api/connection-request','user_id=<?php echo e($my_id); ?>&connect_user_id=<?php echo e($connection->id); ?>&_feed_id=<?php echo e($feed_data->_feed_id); ?>')">OK to Connect</a>
                      <?php elseif($connection->is_pending_connection==1 && $connection->request_from_user==$my_id && $connection->_request_id): ?>
                      <a class="btn btn-following" onclick="execute('api/cancel-request','user_id=<?php echo e($my_id); ?>&_request_id=<?php echo e($connection->_request_id); ?>')">Cancel Connect</a>
                      <?php elseif($connection->is_pending_connection==1 && $connection->request_from_user!=$my_id && $connection->_request_id): ?>
                      <a class="btn btn-following" onclick="execute('api/accept-reject-request','user_id=<?php echo e($my_id); ?>&_request_id=<?php echo e($connection->_request_id); ?>&request_status=1')">Accept Connect</a>
                      <?php endif; ?>
                      <a class="btn btn-following" onclick="execute('api/follow-unfollow','user_id=<?php echo e($my_id); ?>&followed_user=<?php echo e($connection->id); ?>')"><?php echo e($connection->is_following==1 ? 'Following' : 'Follow'); ?></a>
                      <?php if($connection->quick_blox_id): ?>
                      <a class="btn btn-following btn-message" href="<?php echo e(url('messages?qb='.$connection->quick_blox_id)); ?>">Message</a>
                      <?php endif; ?>
                    </div>
                  </div>
                <?php if($connection->events_type_names): ?>
                  <div class="connectionsAllDetailsBody">
                    <div class="connectionsDetailsTitle">Your <?php echo e($connection->is_connected==1 ? 'Connections' : 'Matches'); ?>  in Common</div>
                    <?php
                      $events_type_names = explode(",",$connection->events_type_names);
                    ?>
                    <?php if($events_type_names): ?>
                    <div>
                      <?php $__currentLoopData = $events_type_names; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="connectionsDetailsBtn btn"><?php echo e($name); ?></div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <?php endif; ?>
                  </div>
                 <?php endif; ?>
                <?php if($feed_data): ?>
                  <?php if($feed_data->feed_what): ?>
                  <div class="connectionsAllDetailsBody">
                    <div class="connectionsDetailsTitle">What</div>
                    <?php
                      $what_names = explode(",",$feed_data->feed_what);
                    ?>
                    <?php if($what_names): ?>
                    <div>
                      <?php $__currentLoopData = $what_names; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="connectionsDetailsBtn btn"><?php echo e($name); ?></div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <?php endif; ?>
                  </div>
                  <?php endif; ?>
                  <?php if($feed_data->feed_where): ?>
                  <div class="connectionsAllDetailsBody">
                    <div class="connectionsDetailsTitle">Where</div>
                    <?php
                      $where_names = explode(",",$feed_data->feed_where);
                    ?>
                    <?php if($where_names): ?>
                    <div>
                      <?php $__currentLoopData = $where_names; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="connectionsDetailsBtn btn"><?php echo e($name); ?></div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <?php endif; ?>
                  </div>
                  <?php endif; ?>
                  
                  <?php if($feed_data->feed_keywords): ?>
                  <div class="connectionsAllDetailsBody">
                    <div class="connectionsDetailsTitle">Keywords</div>
                    <?php
                      $keywords_names = explode(",",$feed_data->feed_keywords);
                    ?>
                    <?php if($keywords_names): ?>
                    <div>
                      <?php $__currentLoopData = $keywords_names; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="connectionsDetailsBtn btn"><?php echo e($name); ?></div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <?php endif; ?>
                  </div>
                  <?php endif; ?>
                <?php endif; ?>
                </div>
              </div>
              
              
            </div>    
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/user_profile.blade.php ENDPATH**/ ?>