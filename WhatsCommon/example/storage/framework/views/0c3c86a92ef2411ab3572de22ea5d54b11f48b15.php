<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly innerBodyModify">
  <div class="container-fluid">
    <div class="settingBody">
      <div class="position-relative settingFlip">
          <div class="leftSlidePan closePan">
            <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
            <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
              <?php echo $__env->make('inc.life_event_left_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
          </div>
          <div class="midPan">
            <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
              <div class="row infoBodyRow">
                <div class="col-md-5 infoBodyCol connectionsBody eventImgCol">
                  <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
                    <div class="innerHome">
                      <a href="<?php echo e(url('home')); ?>"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                    </div>
                    <div class="feedbackImg">
                      <div class="personalImgAll">
                        <img src="new-design/img/travelImg.png" class="img-fluid" alt="">
                        <div class="personalImgTitle">Travel</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-7 connectionsBody infoBodyCol eventCol midHightControl">
                          <div class="connectionsBody feedbackRight windowHeight windowHeightMid">


                      <form class="xhr_form" method="post" action="api/life-event-upload" id="life-event-upload">
                            <?php echo csrf_field(); ?>
                        <div class="eventColHeading">What </div> <img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                          <input type="hidden" name="type_id" value="4">
                          <input type="hidden" name="user_id" value="<?php echo e(Session::get('userdata')['id']); ?>">
                        <div class="whatForm">
                          <div class="form-group">
                            <label for="">Type</label>
                            <select class="form-control " name="sub_category_id">
            		<?php if($what): ?>
                    	<?php $__currentLoopData = $what; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $w): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value="<?php echo e($w->id); ?>"><?php echo e($w->sub_cat_type_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                            </select>
                          </div>
                          <div class="whatReasonAll">
                            <div class="form-group topFormIcon">
                              <label for="">Mode</label>
                              
                              <select class="form-control formAirport " name="airport" style="padding-left: 0px !important;">
                                <option value="" hidden>Select</option>
                  <?php if($travel_modes): ?>
                      <?php $__currentLoopData = $travel_modes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mode): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option option="<?php echo e($mode->mode_name); ?>"><?php echo e($mode->mode_name); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Name</label>
                                  <input type="text" class="form-control formAirline" id="" placeholder="Type here" name="airline">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Number</label>
                                  <input type="text" class="form-control formFlightNo" id="" placeholder="Type here" name="flight_number">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Row</label>
                                  <input type="text" class="form-control formRow" id="" placeholder="Type here" name="row">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Seat</label>
                                  <input type="text" class="form-control formSeat" id="" placeholder="Type here" name="seat">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="eventColHeading">Where </div>
                        <div class="whatForm whereFrom">
                          <div class="form-group">
                            <label for="">Location Type</label>
                            <select class="form-control formLocationType" name="location_type">
                              <option value="" hidden>Select</option>
                	<?php if($location_types): ?>
                    	<?php $__currentLoopData = $location_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    			<option option="<?php echo e($lt->lt_name); ?>"><?php echo e($lt->lt_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                            </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">Location Name</label>
                            <input type="text" class="form-control formLocation" id="" placeholder="Type here" name="location_name">
                          </div>
                          <div class="form-group">
                            <label for="">Country</label>
                            <select class="form-control formCountry" name="country" id="country">
                              <option value="" hidden>Select</option>
            <?php if($countries): ?>
              <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($country->country_name); ?>" data-id="<?php echo e($country->_country_id); ?>"><?php echo e($country->country_name); ?></option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
                              
                            </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">State/County/Province</label>
                            <select class="form-control formState" name="state" id="province">
                                      <option value="" hidden>Select</option>
                                      
                                    </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">City</label>
                            <select class="form-control formCity" name="city" id="city">
                                      <option value="" hidden>Select</option>
                                    </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">Street Address</label>
                            <input type="text" class="form-control formAddress" id="" placeholder="123 abc Ave." name="street">
                          </div>
                          <div class="form-group">
                            <label for="">ZIP</label>
                            <input type="text" class="form-control formZip" id="" placeholder="ZIP" name="zip">
                          </div>
                        </div>
                        <div class="eventColHeading">When </div>
                        <div class="whatForm whereFrom">
                          <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">From</label>
                              <select class="form-control formMonth" id="" name="when_from_month">
                                        <option value="" hidden>Month</option>
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                      </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay">
                              <label for="number">Day</label>
                              <select class="form-control" id="" name="when_from_day">
                                        <option value="" hidden>DD</option>
                                        <?php for($i=1;$i<=31;$i++): ?>
                                          <?php
                                            $j = sprintf('%02d', $i)
                                          ?>
                                         <option value="<?php echo e($j); ?>"><?php echo e($j); ?></option>
                                        <?php endfor; ?>
                                        
                                      </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayYear">
                              <label for="number">Year</label>
                              <select class="form-control" id="" name="when_from_year">
                                        <option value="" hidden>YYYY</option>
                                      <?php for($i = date('Y'); $i >= 1900; $i--): ?>
                                        <option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                                      <?php endfor; ?>
                                      </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                          </div>
                          <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">To</label>
                              <select class="form-control formMonth" id="" name="when_to_month">
                                        <option value="" hidden>Month</option>
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                      </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay">
                              <label for="number">Day</label>
                              <select class="form-control" id="" name="when_to_day">
                                <option value="" hidden>DD</option>
                                <?php for($i=1;$i<=31;$i++): ?>
                                  <?php
                                    $j = sprintf('%02d', $i)
                                  ?>
                                 <option value="<?php echo e($j); ?>"><?php echo e($j); ?></option>
                                <?php endfor; ?>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayYear">
                              <label for="number">Year</label>
                              <select class="form-control" id="" name="when_to_year">
                                <option value="" hidden>YYYY</option>
                              <?php for($i = date('Y'); $i >= 1900; $i--): ?>
                                <option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                              <?php endfor; ?>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                          </div>
                          <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">Departure Time</label>
                              <select class="form-control formTime" id="" name="dep_time">
                                <option value="" hidden="">00:00</option>
                                <option value="1:00">1:00</option>
                                <option value="1:30">1:30</option>
                                <option value="2:00">2:00</option>
                                <option value="2:30">2:30</option>
                                <option value="3:00">3:00</option>
                                <option value="3:30">3:30</option>
                                <option value="4:00">4:00</option>
                                <option value="4:30">4:30</option>
                                <option value="5:00">5:00</option>
                                <option value="5:30">5:30</option>
                                <option value="6:00">6:00</option>
                                <option value="6:30">6:30</option>
                                <option value="7:00">7:00</option>
                                <option value="7:30">7:30</option>
                                <option value="8:00">8:00</option>
                                <option value="8:30">8:30</option>
                                <option value="9:00">9:00</option>
                                <option value="9:30">9:30</option>
                                <option value="10:00">10:00</option>
                                <option value="10:30">10:30</option>
                                <option value="11:00">11:00</option>
                                <option value="11:30">11:30</option>
                                <option value="12:00">12:00</option>
                                <option value="12:30">12:30</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay timeOptionSet">
                              <div class="form-control timeOptionOnly activeTimeOption" id="dep_time_am">AM</div>
                              <div class="form-control timeOptionOnly" id="dep_time_pm" style="border-top-left-radius: 0 !important;border-bottom-left-radius: 0 !important;">PM</div>
                              <input type="hidden" name="dep_time_ampm" id="" value="AM" />
                            </div>
                            <div class="form-group birthdayYear"></div>
                          </div>
                          <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">Arrival Time</label>
                              <select class="form-control formTime" id="" name="arr_time">
                                <option value="" hidden="">00:00</option>
                                <option value="1:00">1:00</option>
                                <option value="1:30">1:30</option>
                                <option value="2:00">2:00</option>
                                <option value="2:30">2:30</option>
                                <option value="3:00">3:00</option>
                                <option value="3:30">3:30</option>
                                <option value="4:00">4:00</option>
                                <option value="4:30">4:30</option>
                                <option value="5:00">5:00</option>
                                <option value="5:30">5:30</option>
                                <option value="6:00">6:00</option>
                                <option value="6:30">6:30</option>
                                <option value="7:00">7:00</option>
                                <option value="7:30">7:30</option>
                                <option value="8:00">8:00</option>
                                <option value="8:30">8:30</option>
                                <option value="9:00">9:00</option>
                                <option value="9:30">9:30</option>
                                <option value="10:00">10:00</option>
                                <option value="10:30">10:30</option>
                                <option value="11:00">11:00</option>
                                <option value="11:30">11:30</option>
                                <option value="12:00">12:00</option>
                                <option value="12:30">12:30</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay timeOptionSet">
                              <div class="form-control timeOptionOnly activeTimeOption" id="arr_time_am">AM</div>
                              <div class="form-control timeOptionOnly" id="arr_time_pm" style="border-top-left-radius: 0 !important;border-bottom-left-radius: 0 !important;">PM</div>
                              <input type="hidden" name="arr_time_ampm" id="arr_time_ampm" value="AM">
                            </div>
                            <div class="form-group birthdayYear"></div>
                          </div>
                        </div>
                        <div class="eventColHeading">Who, What, Where, When, Why </div>
                        <div class="whatForm whereFrom">
                          
                          	<div class="myAllEvents" id="keywords_div"></div>
	                        <input type="hidden" id="keywords" value="" name="event_keywords">
	                        <div class="input-group lifeEvent">
	                          <input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
	                          <div class="input-group-append">
	                            <a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
	                           </div>
	                        </div>
                          	<div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                        </div>
                        <?php echo $__env->make('inc.life_event_img_upload', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                        
                      </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Travel/Vacation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Travel/Vacation</label>
            <p>This section is for your travel details. This specific information
might help you match with the person with the exact travel details. Exp: Flight
number, bus number, ticket number etc. Don’t forget " keywords" for
anything important to add.</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>This is the location where you met someone or where the event happened. It can be somewhere you stayed or visited. It can be anywhere; give it a shot.</p>
          </div>
          <div class="form-group">
            <label>WHEN:</label>
            <p>It's the scope of your travel dates, departure time, and arrival time. It's not required to be specific with the time.</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).ready(function(){
  $("#dep_time_am").click(function(){
      $("#dep_time_am").addClass("activeTimeOption");
      $("#dep_time_pm").removeClass("activeTimeOption");
      $('#dep_time_ampm').val('AM');
  });
  $("#dep_time_pm").click(function(){
    $("#dep_time_pm").addClass("activeTimeOption");
    $("#dep_time_am").removeClass("activeTimeOption");
    $('#dep_time_ampm').val('PM');
  });

  $("#arr_time_am").click(function(){
      $("#arr_time_am").addClass("activeTimeOption");
      $("#arr_time_pm").removeClass("activeTimeOption");
      $('#arr_time_ampm').val('AM');
  });
  $("#arr_time_pm").click(function(){
    $("#arr_time_pm").addClass("activeTimeOption");
    $("#arr_time_am").removeClass("activeTimeOption");
    $('#arr_time_ampm').val('PM');
  });
});
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="" hidden="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/life_events/travel.blade.php ENDPATH**/ ?>