 <!-- Modal starts -->       
<div class="modal fade" id="select-payment-for-plans-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Select Payment Mode</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <?php if($currency_data && $currency_data->currency_shortname=='USD'): ?>
        <div class="form-group">
          <div class="btn-Edit-save">
            <button class="btn btn-Edit btn-Plan btn-block" style="height: 45px;border-radius: 23px;background: #0070ba;width: 100%;font-weight: 500;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;" 
              onclick="execute('create-stripe-payment','amount=<?php echo e($amount); ?>&payment_for=<?php echo e($payment_for); ?>&redirect_url=<?php echo e($redirect_url); ?>&plan_id=<?php echo e($currency_data->id); ?>');">Pay with <span style="font-style: italic;font-weight: 800;font-size: 20px;">Stripe</span></button>
          </div>
        </div>
        <?php endif; ?>
        <div id="smart-button-container">
      <div style="text-align: center;">
        <div id="paypal-button-container"></div>
        
      </div>
    </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
<?php if($currency_data->plan_paypal_id): ?>
<script>
  paypal.Buttons({
      style: {
          shape: 'pill',
          color: 'blue',
          layout: 'vertical',
          label: 'subscribe'
      },
      createSubscription: function(data, actions) {
        return actions.subscription.create({
          // Creates the subscription 
          plan_id: '<?php echo e($currency_data->plan_paypal_id); ?>'
        });
      },
      onApprove: function(data, actions) {
        //alert(data.subscriptionID); // You can add optional success message for the subscriber here
        console.log(data);
        var plan_id = "<?php echo e($currency_data->id); ?>";
        var donation_data = `plan_id=`+plan_id+`&user_id=<?php echo e($user_id); ?>`+`&inapp_type=paypal&inapp_productid=`+data.subscriptionID;

        console.log('donation_data => '+donation_data);

        execute('api/choose-plan',donation_data);
      }
  }).render('#paypal-button-container'); // Renders the PayPal button
</script>
<?php else: ?>
  <script>
    function initPayPalButton() {
      paypal.Buttons({
        style: {
          shape: 'pill',
          color: 'blue',
          layout: 'vertical',
          label: 'pay',
        },

        createOrder: function(data, actions) {
          var amount = "<?php echo e($amount ? $amount : 0); ?>";
          
          return actions.order.create({
            purchase_units: [{"amount":{"currency_code":"<?php echo e($currency_data ? $currency_data->currency_shortname : 'USD'); ?>","value":amount}}]
          });
        },

        onApprove: function(data, actions) {
          return actions.order.capture().then(function(orderData) {
            
            // Full available details
            var plan_id = "<?php echo e($currency_data->id); ?>";
            var donation_data = `plan_id=`+plan_id+`&user_id=<?php echo e($user_id); ?>`;

            console.log('donation_data => '+donation_data);

            execute('api/choose-plan',donation_data);
            
          });
        },

        onError: function(err) {
          console.log(err);
        }
      }).render('#paypal-button-container');
    }
    initPayPalButton();
  </script>
<?php endif; ?><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/xhr/select_payment_for_plans_modal.blade.php ENDPATH**/ ?>