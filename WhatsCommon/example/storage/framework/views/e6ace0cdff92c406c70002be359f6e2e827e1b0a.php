<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>What'sCommon</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Open+Sans&display=swap" rel="stylesheet"> 
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/jquery.bxslider.css" rel="stylesheet">
  <link href="css/toastr.min.css" rel="stylesheet">
  <!-- ========================================================
  * Template Name: Virtual Event
  * Author: Biswanath hazra
  ========================================================= -->
<style type="text/css">
.feedbackTitle {
    font-weight: 600;
    font-size: 20px;
    color: #3b71b9;
    text-align: center;
    margin-bottom: 20px;
}
.aboutText {
    font-weight: 400;
    color: #b8b8b8;
    font-size: 14px;
    line-height: 18px;
    margin-top: 13px;
}
</style>
<body>
  <div class="parallax-window" data-parallax="scroll" data-image-src="img/websiteBg.jpg">
    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top d-flex">
      <div class="container-fluid">
        <div class="header-container d-flex align-items-center">
          <div class="logo mr-auto">
            <?php if(Session::has('userdata')): ?>
            <a href="<?php echo e(url('home')); ?>" >
            <?php else: ?>
            <a href="<?php echo e(url('/')); ?>" >
            <?php endif; ?>
              <img src="img/logo2.png" alt="" class="img-fluid logo1">
              <img src="img/logo.png" alt="" class="img-fluid logo2">
            </a>
          </div>

          <div class="headerRight">
          <?php if(Session::has('userdata')): ?>
            <a href="<?php echo e(url('home')); ?>">
              <img src="img/headerLogin.png" class="img-fluid" alt="">
            </a>
          <?php else: ?>
            <a href="<?php echo e(url('login')); ?>">
              <img src="img/headerLogin.png" class="img-fluid" alt="">
            </a>
          <?php endif; ?>
            
          </div>
        </div><!-- End Header Container -->
      </div>
    </header><!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section id="hero" class="d-flex align-items-center pb-0">
      <div class="container position-relative" data-aos="fade-in" data-aos-delay="200">
        <div class="row platformRow">
          <div class="col-md-7 heroCol">
            <h1>(RE)CONNECTING <br><span class="bannerHeading2" style="color: #93cc66;">FAMILY</span> <span class="bannerHeading2">and</span> <span class="bannerHeading2" style="color: #93cc66;">FRIENDS</span> <br><span class="bannerHeading3">IS NOW POSSIBLE</span> <br><span  class="bannerHeading2">BASED ON</span> <br><span class="bannerHeading2" style="color: #93cc66;">“What’sCommon”</span></h1>
            <div class="bannerText2">Because “everyone is looking for someone”, <br>
            What’sCommon has made it possible in ways that <br>
            have never done before. Who are you looking for...</div>
            <div class="bannerBtn">
            <?php if(!Session::has('userdata')): ?>
              <a href="signup" class="btn-get-started scrollto"><img src="img/bannerBtnLogo1.png" class="img-fluid mr-1" alt=""> First Timer</a>
            <?php endif; ?>
            <a href="<?php echo e(!Session::has('userdata') ? url('login') : url('home')); ?>" class="btn-get-started scrollto"><img src="img/bannerBtnLogo2.png" class="img-fluid mr-1" alt=""> welcome back</a>
            </div>
          </div>
          <div class="col-md-5">
            <div class="bannerImage">
              <img src="img/bannerimg.png" alt="" class="img-fluid">
            </div>
          </div>
        </div>


          
        <div class="platform homeAbout">
          <div class="row content platformRow">
            <div class="col-lg-4" data-aos="fade-right" data-aos-delay="100">
              <h2><span class="platformSmallFont">“WELCOME TO </span> <br><span style="color: #3b71b9;">&nbsp;&nbsp;&nbsp; WHAT’S</span> <br><span style="color: #92d054;"> &nbsp;COMMON”</span></h2>

              <div class="abtBtn">
              <a href="#" class="btn-get-started scrollto" data-toggle="modal" data-target="#about-modal">About Us</a>
            </div>
            </div>
            <div class="col-lg-8 pt-4 pt-lg-0" data-aos="fade-left" data-aos-delay="200">
              <div class="homeAboutText">
                What’sCommon concept was inspired by a television show more than 10 years ago with a big heart of wanting to reconnect biological parent with their children, then evolved more than 5 years later with the want and desire to reconnect with my military battle buddies, friends and neighbors but simply was impossible with limited information useless within the typical search platforms of today, until now! Welcome to “What’sCommon”, Who are you looking for? Who might be looking for you? <span style="color: #93cc66;">“everyone is looking for someone”</span>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </section><!-- End Hero -->

    <main id="main">

      <section id="platform" class="platform">
        <div class="platformOnly">
          
          <div class="row content platformRow">
            <div class="col-lg-7" data-aos="fade-right" data-aos-delay="100">
              <img src="img/ev_manage_platform_bg.png" alt="" class="img-fluid platformImg">
            </div>
            <div class="col-lg-5 pt-4 pt-lg-0" data-aos="fade-left" data-aos-delay="200">
              <h2><span style="color: #000;">some</span> <br>amazing <br><span class="platformFont2">Features</span> <br>THAT’LL <br>change your <br><span class="platformFont2">LIFE!</span></h2>
              
            </div>
          </div>
        </div>

        <div class="downloadBackground">
          <div id="services">
            <div class="container position-relative">
            <div class="sqr">
              <img src="img/sqr.png" class="img-fluid" alt="">
            </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="row">
                    
                    <div class="col-xl-3 col-lg-4 col-sm-6 d-flex align-items-stretch">
                      <a class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100" href="<?php echo e(Session::has('userdata') ? url('home/?type=1') : url('signup')); ?>">
                        <div class="servicesTab">
                          <img src="img/services1.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Personal</div>
                        </div>
                      </a>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-sm-6 d-flex align-items-stretch">
                      <a class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100" href="<?php echo e(Session::has('userdata') ? url('home/?type=2') : url('signup')); ?>">
                        <div class="servicesTab">
                          <img src="img/services2.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Dating</div>
                        </div>
                      </a>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-sm-6 d-flex align-items-stretch">
                      <a class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100" href="<?php echo e(Session::has('userdata') ? url('home/?type=3') : url('signup')); ?>">
                        <div class="servicesTab">
                          <img src="img/services3.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Adoption</div>
                        </div>
                      </a>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-sm-6 d-flex align-items-stretch">
                      <a class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100" href="<?php echo e(Session::has('userdata') ? url('home/?type=4') : url('signup')); ?>">
                        <div class="servicesTab">
                          <img src="img/services4.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Travel/Vacation</div>
                        </div>
                      </a>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-sm-6 d-flex align-items-stretch">
                      <a class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100" href="<?php echo e(Session::has('userdata') ? url('home/?type=5') : url('signup')); ?>">
                        <div class="servicesTab">
                          <img src="img/services5.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Military</div>
                        </div>
                      </a>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-sm-6 d-flex align-items-stretch">
                      <a class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100" href="<?php echo e(Session::has('userdata') ? url('home/?type=6') : url('signup')); ?>">
                        <div class="servicesTab">
                          <img src="img/services6.png" class="img-fluid" alt="">
                          <div class="servicesTabText">School/Education</div>
                        </div>
                      </a>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-sm-6 d-flex align-items-stretch">
                      <a class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100" href="<?php echo e(Session::has('userdata') ? url('home/?type=7') : url('signup')); ?>">
                        <div class="servicesTab">
                          <img src="img/services7.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Careers/Job/Work</div>
                        </div>
                      </a>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-sm-6 d-flex align-items-stretch">
                      <a class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100" href="<?php echo e(Session::has('userdata') ? url('home/?type=8') : url('signup')); ?>">
                        <div class="servicesTab">
                          <img src="img/services8.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Pets/Animal</div>
                        </div>
                      </a>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-sm-6 d-flex align-items-stretch">
                      <a class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100" href="<?php echo e(Session::has('userdata') ? url('home') : url('signup')); ?>">
                        <div class="servicesTab">
                          <img src="img/services9.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Reverse Lookup</div>
                        </div>
                      </a>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-sm-6 d-flex align-items-stretch">
                      <a class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100" href="<?php echo e(Session::has('userdata') ? url('home/?type=9') : url('signup')); ?>">
                        <div class="servicesTab">
                          <img src="img/services10.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Lost & Found</div>
                        </div>
                      </a>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-sm-6 d-flex align-items-stretch">
                      <a class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100" href="<?php echo e(Session::has('userdata') ? url('home') : url('signup')); ?>">
                        <div class="servicesTab">
                          <img src="new-design/img/services11.png" class="img-fluid" alt="">
                          <div class="servicesTabText">Doppelganger</div>
                        </div>
                      </a>
                    </div>

                    <div class="col-xl-3 col-lg-4 col-sm-6 d-flex align-items-stretch">
                      <a class="icon-box eventBox" data-aos="zoom-in" data-aos-delay="100" href="<?php echo e(Session::has('userdata') ? url('home') : url('signup')); ?>">
                        <div class="servicesTab">
                          <img src="img/services12.png" class="img-fluid" alt="">
                          <div class="servicesTabText">DNA Match</div>
                        </div>
                      </a>
                    </div>

                  </div>
                </div>
              </div>

            </div>
          </div>

          <div id="downloadSection">
            <div class="container">
              <div class="row content downloadSectionRow">
                <div class="col-lg-6" data-aos="fade-left" data-aos-delay="200">
                  <div class="phBg">
                    <img src="img/downloadPh.png" alt="" class="img-fluid">
                  </div>
                </div>
                <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-right" data-aos-delay="100">
                  <h2><span style="color: #000;">get</span> <br><span class="platformFont4">&nbsp;&nbsp; first month</span> <br><span class="platformFont3">free</span></h2>
                
                  <div class="homeAboutText">
                    Enjoy all the powerful benefits of What’sCommon for one month FREE on our 100% secure network that uses SSL for end-to-end encryption for your personal information.
                  </div>

                  <ul class="mb-0 list-unstyled appStoreAll">
                    <li class="d-inline-block">
                      <a href="#">
                        <img src="img/appStore.png" class="img-fluid" alt="">
                      </a>
                    </li>
                    <li class="d-inline-block">
                      <a href="#">
                        <img src="img/playStore.png" class="img-fluid" alt="">
                      </a>
                    </li>
                  </ul>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </section><!-- End Services Section -->

      <!-- ======= Testimonials Section ======= -->
      <section id="testimonials" class="testimonials platform">
  <?php if($testimonials && count($testimonials)>0): ?>
        <div class="testimonialsBg">
          <div class="container">
            <h2 class="text-center testimonialsHeading">Testimonials</h2>
            <div class="row">
              <div class="col-lg-12" data-aos="fade-up" data-aos-delay="100">

                <ul class="list-unstyled mb-0 testimonialSlider">
    <?php $__currentLoopData = $testimonials; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $testimonial): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li class="d-inline-block testimonialSliderLi">
                    <div class="testimonialQuote">
                      <img src="img/quote_up.png" class="img-fluid" alt="">
                    </div>
                    <div class="testimonialAvtarBody">
                      <div class="testimonialAvtar">
                        <img src="<?php echo e($testimonial->testimonial_userimage ? url($testimonial->testimonial_userimage) : url('new-design/img/profile_placeholder.jpg')); ?>" alt="" class="img-fluid">
                      </div>
                      <div class="homeAboutText">
                        <?php echo e($testimonial->testimonial_text); ?>


                        <div class="testimonialAvtarAddress"><?php echo e($testimonial->testimonial_username); ?></div>
                        <div class="testimonialAvtarPlace"><?php echo e($testimonial->testimonial_useraddress); ?></div>
                      </div>
                    </div>
                  </li>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <!-- <li class="d-inline-block testimonialSliderLi">
                    <div class="testimonialQuote">
                      <img src="img/quote_up.png" class="img-fluid" alt="">
                    </div>
                    <div class="testimonialAvtarBody">
                      <div class="testimonialAvtar">
                        <img src="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8ZmFjZXxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80" alt="" class="img-fluid">
                      </div>
                      <div class="homeAboutText">
                        Thank you What’sCommon, you made 30 years of looking for my Battle Budy happen with hardley any information! WOW!

                        <div class="testimonialAvtarAddress">Scott A. McCuskey</div>
                        <div class="testimonialAvtarPlace">Texas, USA</div>
                      </div>
                    </div>
                  </li>

                  <li class="d-inline-block testimonialSliderLi">
                    <div class="testimonialQuote">
                      <img src="img/quote_up.png" class="img-fluid" alt="">
                    </div>
                    <div class="testimonialAvtarBody">
                      <div class="testimonialAvtar">
                        <img src="https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8ZmFjZXxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80" alt="" class="img-fluid">
                      </div>
                      <div class="homeAboutText">
                        Thank you What’sCommon, you made 30 years of looking for my Battle Budy happen with hardley any information! WOW!

                        <div class="testimonialAvtarAddress">Scott A. McCuskey</div>
                        <div class="testimonialAvtarPlace">Texas, USA</div>
                      </div>
                    </div>
                  </li> -->

                </ul>
              </div>
            </div>

          </div>
        </div>
  <?php endif; ?>
        <div id="video" class="platform">
          <div class="container">
            <div class="row content downloadSectionRow">
              <div class="col-lg-7" data-aos="fade-left" data-aos-delay="200">
                <div class="vidImg">
                  <img src="img/vid_img.png" alt="" class="img-fluid">
                </div>
              </div>
              <div class="col-lg-5 pt-4 pt-lg-0" data-aos="fade-right" data-aos-delay="100">
                <h2>LET’S TOUR HOW <br>
                <span class="videoTitle2">Whats’</span><span class="videoTitle2" style="color: #92d054;">Common</span> <br>
                WORKS FOR YOU!</h2>
              
                <div class="homeAboutText">
                  You’re just a click away from watching how simple and easy it is to plug in basic details of your life events and combing them the powerful patented technology of What’sCommon to connect to who, what, where, when, why is important to you!
                </div>

                <div class="homeAboutTextVideo2">“everyone is looking for someone”</div>

                <div class="videoPresentationBtn" data-toggle="modal" data-target="#watchvidPop"><img src="img/videoPresentationBtn.png" class="img-fluid" alt=""> &nbsp;Video Presentation</div>
                
              </div>
            </div>
          </div>
        </div>
      </section><!-- End Testimonials Section -->

      <section id="contactUs" class="platform">
        <div class="container">
          
          <div class="row content platformRow">
            <div class="col-lg-4" data-aos="fade-right" data-aos-delay="100">
              <h2><span class="videoTitle2" style="color: #000;"> SHOOT US</span> <br>
              A NOTE</h2>

              <div class="homeAboutText">
                Questions, comments and we certainly love suggestions. We are just a quick note away! No time to be shy, we’d love to hear from you!
              </div>
            </div>
            <div class="col-lg-2 blankCol"></div>
            <div class="col-lg-6 pt-lg-0" data-aos="fade-left" data-aos-delay="200">
              <div class="contactForm">
                <div class="contactFormTitle">Message</div>
                <form action="submit-user-query" class="xhr_form" id="submit-user-query" method="POST">
                  <?php echo csrf_field(); ?>
                  <div class="form-group">
                    <input type="text" class="form-control" id="name" placeholder="Name" name="query_name">
                    <p class="text-danger" style="font-size: 16px;" id="query_name_error"></p>
                  </div>
                  <div class="form-group">
                    <input type="number" class="form-control" id="mobileNo" placeholder="Mobile No." name="query_mobile">
                    <p class="text-danger" style="font-size: 16px;" id="query_mobile_error"></p>
                  </div>
                  <div class="form-group">
                    <input type="email" class="form-control" id="email" placeholder="Enter Email" name="query_email">
                    <p class="text-danger" style="font-size: 16px;" id="query_email_error"></p>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" id="subject" placeholder="Subject" name="query_subject">
                    <p class="text-danger" style="font-size: 16px;" id="query_subject_error"></p>
                  </div>
                  <div class="form-group">
                    <!-- <input type="text" class="form-control" id="message" placeholder="Message" name="query_message"> -->
                    <textarea class="form-control" id="message" placeholder="Message" name="query_message" style="height: 70px;"></textarea>
                    <p class="text-danger" style="font-size: 16px;" id="query_message_error"></p>
                  </div>
                  
                  <div class="abtBtn">
                    <button class="btn-get-started scrollto" type="submit"><img src="img/submitIcon.png" class="img-fluid" alt=""> Shoot It!</button>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <div class="subscribeSection">
            <div class="row content platformRow">
              <div class="col-lg-6" data-aos="fade-right" data-aos-delay="100">
                <div class="subscribeTitle">Subscribe for updates</div>

                <div class="homeAboutText">
                  You’re just a click away from the latest and greatest!
                </div>
              </div>
              <div class="col-lg-6 pt-lg-0" data-aos="fade-left" data-aos-delay="200">
                <div class="subscribeForm">
                  <form action="subscribe-to-newsletter" class="xhr_form" id="subscribe-to-newsletter" method="POST">
                  <?php echo csrf_field(); ?>
                    <div class="form-group">
                      <!-- <input type="text" class="form-control" id="subscribe" placeholder="Enter email address for updates" name="subscribe"> -->
                      <!-- <img src="img/subscribeIcon.png" class="img-fluid subscribeIcon" alt=""> -->
                      <div class="input-group mb-3">
                        <input type="text" class="form-control" name="email" placeholder="Enter email address for updates">
                        <div class="input-group-append">
                          <button class="btn btn-subscribe" type="submit"><img src="img/subscribeIcon.png" class="img-fluid" alt=""></button>  
                         </div>
                      </div>
                      <p id="email_error" class="text-danger"></p>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

    </main><!-- End #main -->

  </div>

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footerTop">
      <div class="row footerRow">
        <div class="col-lg-5 footerCol">
          <div class="googleMap">
            <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3683.1574159553993!2d88.42609151459625!3d22.610596137273934!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a027589b4c398e3%3A0xbf4081ba4c57562f!2s25%2C%208%2C%20VIP%20Rd%2C%20Udayan%20Pally%2C%20Baguihati%2C%20Kolkata%2C%20West%20Bengal%20700159!5e0!3m2!1sen!2sin!4v1613736098009!5m2!1sen!2sin" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> -->

           <!--  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3010.916017879732!2d-80.65594988521673!3d41.00521132756356!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8833fa6a6dfb226d%3A0xb7f032129fa6ad6!2s8060%20Southern%20Blvd%2C%20Youngstown%2C%20OH%2044512%2C%20USA!5e0!3m2!1sen!2sin!4v1636969345537!5m2!1sen!2sin" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> -->
           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2200887.760216843!2d-83.80611266984268!3d40.39681918505367!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8836e97ab54d8ec1%3A0xe5cd64399c9fd916!2sOhio%2C%20USA!5e0!3m2!1sen!2sin!4v1651749512615!5m2!1sen!2sin" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
          </div>
        </div>
        <?php
          $phone = str_replace('Customer Service Voicemail<br>','',$contact_us->cmsc_phone);
        ?>
        <div class="col-lg-7 footerCol">
          <div class="footerDetails">
            <div class="row">
              <div class="col-lg-1 blankCol"></div>
              <div class="col-lg-6 footerDetailCol">
                <div class="footerDetailsHeading">Contact Us</div>
                <div class="footerDetailsAddress"><?php echo $contact_us->cmsc_address; ?></div>
                <div class="footerDetailsText footerDetailsPh"><a style="text-decoration: none;color: #FFF;" href="tel:<?php echo e($phone); ?>"><?php echo $contact_us->cmsc_phone; ?></a></div>
                <div class="footerDetailsText footerDetailsEmail"><a style="text-decoration: none;color: #FFF;" href="mailto:<?php echo e($contact_us->cmsc_email); ?>"><?php echo e($contact_us->cmsc_email); ?></a></div>

                <ul class="socialLink mb-0 list-unstyled">
                  <li class="d-inline-block">
                    <a href="<?php echo e($contact_us->cmsc_facebook_link); ?>" target="_blank">
                      <img src="img/facebook.png" class="img-fluid" alt="">
                    </a>
                  </li>

                  <li class="d-inline-block">
                    <a href="<?php echo e($contact_us->cmsc_youtube_link); ?>" target="_blank">
                      <img src="img/youtube.png" class="img-fluid" alt="">
                    </a>
                  </li>

                  <li class="d-inline-block">
                    <a href="<?php echo e($contact_us->cmsc_linkedin_link); ?>" target="_blank">
                      <img src="img/linkdin.png" class="img-fluid" alt="">
                    </a>
                  </li>

                  <li class="d-inline-block">
                    <a href="<?php echo e($contact_us->cmsc_insta_link); ?>" target="_blank">
                      <img src="img/instra.png" class="img-fluid" alt="">
                    </a>
                  </li>
                </ul>
              </div>
              <div class="col-lg-3 footerMenuCol">
                <div class="footerDetailsHeading">Quick Links</div>

                <ul class="footerMenu mb-0 list-unstyled">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">About</a></li>
                  <li><a href="#">Features</a></li>
                  <li><a href="#">Testimonials</a></li>
                  <li><a href="#">Contact</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    

    <div class="footerBottom">
      <div class="container d-md-flex">

        <div class="copyblink mr-md-auto text-center text-md-left">
          <div class="copyright">
            &copy; What'sCommon <span id="footerYear"></span> &nbsp;|&nbsp; All Rights Reserved.
          </div>

          <!-- <div class="bottomlink">
            Designed By<a href="https://zabingo.com"> Zabingo.com</a>
          </div> -->
        </div>
        
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>

  <div class="modal fade" id="watchvidPop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h5 class="modal-title" id="exampleModalLabel">What'sCommon</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <iframe id="playerID"
            width="100%" 
            height="315" 
            src="https://www.youtube.com/embed/LacmttuMnik" 
            frameborder="0" 
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
            allowfullscreen>
          </iframe>
        </div>
      </div>
    </div>
  </div>

<div class="modal" tabindex="-1" role="dialog" id="about-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;text-align: justify;">
      <div class="modal-header" style="display: none;">
        <h5 class="modal-title"><?php echo e($about->page_title); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body aboutText">
        <div class="feedbackTitle"><span><?php echo e($about->page_title); ?></span></div>
        <?php echo $about->page_content; ?>

      </div>
      
    </div>
  </div>
</div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

  <!-- parallax -->
  <script src="assets/js/parallax.js"></script>
  <script src="assets/js/jquery.bxslider.js"></script>
  <script src="js/toastr.min.js"></script>
<?php echo $__env->make('inc.script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <script>
    $(document).ready(function(){
      $(".testimonialSlider").bxSlider({
        slideWidth: 356,
        minSlides: 1,
        maxSlides: 8,
        slideMargin: 30,
        controls: false,
        pager: true,
        infiniteLoop: false,
        auto: true,
        autoHover: true,
        speed: 1500,
        moveSlides: 1
      });
    });

    var d = new Date();
    var n = d.getFullYear();
    document.getElementById("footerYear").innerHTML = n;

    $('#watchvidPop').on('hidden.bs.modal', function () {
      console.log('clicked')
      /*$('#playerID').get(0).stopVideo();*/
      $("#playerID").attr("src", $("#playerID").attr("src"));
    });
  </script>

</body>

</html><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/index.blade.php ENDPATH**/ ?>