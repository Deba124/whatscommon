<div id="piechart3" style="width: auto; height: 300px;"></div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart5);

function drawChart5() {

    var data = google.visualization.arrayToDataTable([
      ['Feedback Category',    'Feedback Counts'],
      ['Performance',     <?php echo e($feedback_type_graph->perform_count ? $feedback_type_graph->perform_count : 0); ?>],
      ['Bugs',            <?php echo e($feedback_type_graph->bug_count ? $feedback_type_graph->bug_count : 0); ?>],
      ['General',         <?php echo e($feedback_type_graph->general_count ? $feedback_type_graph->general_count : 0); ?>],
    ]);
<?php
$total = ($feedback_type_graph->perform_count ? $feedback_type_graph->perform_count : 0) + ($feedback_type_graph->bug_count ? $feedback_type_graph->bug_count : 0) + ($feedback_type_graph->general_count ? $feedback_type_graph->general_count : 0);
?>
    var options = {
      title: 'Feedback Categories<?php echo e(" (Total : ".$total.")"); ?>',
      colors: ['#91d444', '#3b98d8', '#ffc999'],
      /*is3D: true,*/
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart3'));

    chart.draw(data, options);
}
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/xhr/refresh_feedback_graph.blade.php ENDPATH**/ ?>