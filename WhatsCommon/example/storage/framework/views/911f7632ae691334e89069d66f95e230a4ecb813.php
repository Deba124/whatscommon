<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>What's Common</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <?php echo $__env->make('inc.css', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<link href="css/toastr.min.css" rel="stylesheet">
<body>
  <section id="login" class="signUpBg">
    <div class="container">
      <div class="AllLogin">
        <div class="loginForm" style="max-width: fit-content;">
          <div class="loginLogo">
            <img src="img/signUpLogo.png" class="img-fluid" alt="">
          </div>

          <div class="signUpHeading">Sign Up</div>
          <div class="loginText" style="color: #000;">Fill in your login information</div>

          <form action="" method="post" class="signUpForm" id="signUpForm">
            <?php echo csrf_field(); ?>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="inputText form-control" name="last_name" required/>
                  <span class="floating-label">Last Name</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="inputText form-control" name="first_name" required/>
                  <span class="floating-label">First Name</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="inputText form-control" name="mi" />
                  <span class="floating-label">MI (Optional)</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="email" name="email" class="inputText form-control" required/>
                  <span class="floating-label">Email Address</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="number" class="inputText form-control" name="phone" required/>
                  <span class="floating-label">Phone Number</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="inputText form-control" name="username" required/>
                  <span class="floating-label">Username</span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="password" class="inputText form-control" required name="password" id="password" />
                  <span class="floating-label">Password</span>
                  <img src="img/passwordVisiable2.png" class="img-fluid passwordVisiable" alt="" onclick="showHidePassword('password');">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="password" class="inputText form-control" name="cnf_pass" required id="cnf_pass" />
                  <span class="floating-label">Re-type Password</span>
                  <img src="img/passwordVisiable2.png" class="img-fluid passwordVisiable" alt="" onclick="showHidePassword('cnf_pass');">
                </div>
              </div>
            </div>
            <div class="form-group" id="responseDiv" style="display: none;"></div>
            <!-- <a href="signup-details" class="btn btn-proceed">Proceed</a> -->
            <button type="submit" class="btn btn-proceed" style="width: 100%;">Proceed</button>
            <a href="<?php echo e(url('/')); ?>" class="btn btn-proceed" style="width: 100%;background-color: #b5afac;">Cancel</a>
          </form>
        </div>
      </div>
    </div>
  </section>
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  <script src="js/toastr.min.js"></script>
  <script>

    $(document).ready(function(){
      
      toastr.options = {
        "closeButton"       : false,
        "debug"             : false,
        "newestOnTop"       : true,
        "progressBar"       : true,
        "positionClass"     : "toast-bottom-right",
        "preventDuplicates" : false,
        "onclick"           : null,
        "showDuration"      : "300",
        "hideDuration"      : "1000",
        "timeOut"           : "5000",
        "extendedTimeOut"   : "1000",
        "showEasing"        : "swing",
        "hideEasing"        : "linear",
        "showMethod"        : "fadeIn",
        "hideMethod"        : "fadeOut"
      };
    });
  </script>
  <?php echo $__env->make('inc.script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script type="text/javascript">
$(document).on('submit','#signUpForm',function(e){
  e.preventDefault();
  var token = $('input[name="_token"]').attr('value');
  console.log(token);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token
    }
  });
  var action = site_url('register');
  $.ajax({
    type : "POST",
    data : new FormData(this),
    contentType: false,   
    cache: false,             
    processData:false, 
    url  : action,
    success : function(response){
      console.log(response);
      if(response.response_code==200){
        $('#responseDiv').show();
        $('#responseDiv').html('<p class="text-success">'+response.response_msg+'</p>');
        $(location).attr("href", site_url('signup-details'));
      }
    },error: function(jqXHR, textStatus, errorThrown){
    /*console.log(textStatus + ": " + jqXHR.status + " " + errorThrown);*/
    /*console.log(jqXHR);*/
    console.log(jqXHR.responseJSON);
    if(jqXHR.responseJSON.errors){
      var err = jqXHR.responseJSON.errors;
      $.each(err, function(key, value){

        if (err[key]) {
          $("#"+key+"_error").html(err[key]);
        }
        else{
          $("#"+key+"_error").html(''); 
        }
      });
    }
    /*$('#responseDiv').show();
    $('#responseDiv').html('<p class="alert alert-danger">'+jqXHR.responseJSON.response_msg+'</p>');*/
  }
  });
});  
</script>
</body>

</html><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/signup.blade.php ENDPATH**/ ?>