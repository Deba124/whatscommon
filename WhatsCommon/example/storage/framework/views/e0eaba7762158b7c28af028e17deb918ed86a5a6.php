<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly">
<div class="container-fluid">
<div class="settingBody">
  <div class="position-relative settingFlip">
    <div class="leftSlidePan <?php echo e(Request::get('menu')==1 ? '' : 'closePan'); ?>">
                  <a href="#" class="panelslideOpenButton <?php echo e(Request::get('menu')==1 ? 'panelslideCloseButton' : ''); ?>"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
      <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
      	<div class="settingLeftTitle">Settings</div>
      	<ul class="settingMenu mb-0 list-unstyled">
      		<li class="settingMenuLi">
      			<a class="settingMenu" href="<?php echo e(url('settings')); ?>"><img src="new-design/img/profileIcon.png" class="img-fluid menuIcon" alt="">Profile</a>
      		</li>
      		<!-- <li class="settingMenuLi">
      			<a class="settingMenu" href="<?php echo e(url('financial-settings')); ?>"><img src="new-design/img/financial.png" class="img-fluid menuIcon" alt="">Financial Settings</a>
      		</li> -->
      		<li class="settingMenuLi">
      			<a class="settingMenu" href="<?php echo e(url('change-password')); ?>"><img src="new-design/img/change-password.png" class="img-fluid menuIcon" alt="">Change Password</a>
      		</li>
      		<li class="settingMenuLi">
      			<a class="settingMenu" href="<?php echo e(url('security')); ?>"><img src="new-design/img/security.png" class="img-fluid menuIcon" alt="">Security and Privacy</a>
      		</li>
      		<li class="settingMenuLi">
      			<a class="settingMenu" href="<?php echo e(url('blocked-users')); ?>"><img src="new-design/img/blocking.png" class="img-fluid menuIcon" alt="">Blocking</a>
      		</li>
      		<li class="settingMenuLi">
      			<a class="settingMenu" href="<?php echo e(url('notification-settings')); ?>"><img src="new-design/img/notifications.png" class="img-fluid menuIcon" alt="">Notifications</a>
      		</li>
      		<li class="settingMenuLi">
      			<a class="settingMenu settingMenuActive" href="<?php echo e(url('link-accounts')); ?>"><img src="new-design/img/link-account-a.png" class="img-fluid menuIcon" alt="">Link Account</a>
      		</li>
      		<li class="settingMenuLi">
      			<a class="settingMenu " href="<?php echo e(url('message-settings')); ?>"><img src="new-design/img/message-setting.png" class="img-fluid menuIcon" alt="">Message Settings</a>
      		</li>
      	</ul>
      </div>
    </div>
    <div class="midPan">
                  <div class="connectionsBody windowHeight windowHeightMid linkAcc linkAccNoti">
                    <div class="bankDetailsTitle">Link Account</div>
                    <div class="feedbackTitle2">By turning on the toggle buttons, you’re allowing What'sCommon to share your matches or connections with a specific social media you choose to turn on.
                    </div>
                    <div class="bankcardTab">
                      
                        

                      <div class="">
                        <div id="" class="">
                        <form method="post" class="xhr_form" action="set-link-account" id="set-link-account">
                            <?php echo csrf_field(); ?>
                          <div class="profileForm bankForm">
                            <ul class="linkAccountList">
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        <img src="new-design/img/fb-share.png" class="img-fluid" alt="">
                                        Facebook 
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" <?php if($settings && $settings->user_share_fb==1): ?> checked <?php endif; ?> name="user_share_fb" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        <img src="new-design/img/twitter-share.png" class="img-fluid" alt="">
                                        Twitter
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" <?php if($settings && $settings->user_share_twitter==1): ?> checked <?php endif; ?> name="user_share_twitter" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        <img src="new-design/img/tagster.png" class="img-fluid" alt="">
                                        Tagster
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" <?php if($settings && $settings->user_share_tagster==1): ?> checked <?php endif; ?> name="user_share_tagster" id="tagster" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                            </ul>
                            
                          </div>
                      </form>
                        </div>
                        
                      </div>

                    </div>
                    
                  </div>
                </div>
  </div>
</div>
</div>
</div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
$(document).on('change','.settings', function(){
	$('.xhr_form').submit();
  if($(this).attr('id')=='tagster'){
    console.log('here');
    if($(this).prop('checked')==true){
      open_modal('coming-soon');
    }
  }
});
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/link_accounts.blade.php ENDPATH**/ ?>