 <!-- Modal starts -->       
<div class="modal fade" id="profile-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Profile</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="update-profile">
          <div class="form-group" id="submit_status"></div>
          <?php echo csrf_field(); ?>
          <div class="form-group">
            <label>Name <span class="text-danger">*</span></label>
            <input type="text" name="admin_name" class="form-control" value="<?php echo e(Session::get('admin_data')->admin_name); ?>">
            <p class="text-danger" id="admin_name_error"></p>
          </div>
          <div class="form-group">
            <label>Email <span class="text-danger">*</span></label>
            <input type="text" class="form-control" disabled="" value="<?php echo e(Session::get('admin_data')->admin_email); ?>">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends --><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/xhr/admin_profile_modal.blade.php ENDPATH**/ ?>