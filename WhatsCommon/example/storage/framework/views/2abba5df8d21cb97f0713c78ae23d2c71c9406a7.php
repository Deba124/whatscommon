<!DOCTYPE html>
<html lang="en">
<?php
  $title = $feed->user1_firstName.' '.$feed->user1_lastName.' is '.$feed->feed_type.' with '.$feed->user2_firstName.' '.$feed->user2_lastName;
  $og_url = url('feed-details?feed='.$feed->_feed_id);
  $og_image = '';
  if($feed->feed_screenshot){
    $og_image = url($feed->feed_screenshot);
  }else{
    if($feed->user1_profile_photo){
      $og_image = url($feed->user1_profile_photo);
    }else if($feed->user2_profile_photo){
      $og_image = url($feed->user2_profile_photo);
    }else{
      $og_image = url('new-design/img/profile_placeholder.jpg');
    }
  }
?>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta name="_token" content="<?php echo e(csrf_token()); ?>">
  <title><?php echo e($title); ?></title>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <!--  -->
  
  <meta property="og:url" content="<?php echo e($og_url); ?>" />
  <meta property="og:type" content="article" />
  <meta property="og:title" content="<?php echo e($title); ?>" />
  <meta property="og:description" content="What'sCommon - everyone is looking for someone" />
  <meta property="og:image" content="<?php echo e($og_image); ?>" />
  <!-- <meta property="og:image:width" content="1200" />
  <meta property="og:image:height" content="630" /> -->
  
  <!-- Favicons -->
  <link href="<?php echo e(url('new-design/img/favicon.png')); ?>" rel="icon">
  <link href="<?php echo e(url('new-design/assets/img/apple-touch-icon.png')); ?>" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Open+Sans&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <!-- Vendor CSS Files -->
  <link href="<?php echo e(url('new-design/assets/vendor/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('new-design/assets/vendor/owl.carousel/assets/owl.carousel.min.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('new-design/assets/vendor/aos/aos.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('new-design/assets/vendor/picker/css/picker.css')); ?>" rel="stylesheet">
  <!-- Template Main CSS File -->
  <link href="<?php echo e(url('new-design/css/style.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('new-design/css/styleb.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('new-design/css/rangeslider.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('css/toastr.min.css')); ?>" rel="stylesheet">
  <link href="<?php echo e(url('css/jquery.magnify.css')); ?>" rel="stylesheet">
  <script src="<?php echo e(url('new-design/assets/vendor/jquery/jquery.min.js')); ?>"></script>
  <!-- <link rel="stylesheet" href="css/slideMenu.css"> -->
  <!-- <link href="css/jquery.bxslider.css" rel="stylesheet"> -->

  <!-- ========================================================
  * Template Name: Virtual Event
  * Author: Biswanath hazra
  ========================================================= -->

<body id="hidenScroll">
  <div id="loading" class="ajax_loader"></div>

  <section id="login" class="signUpBg innerBg">
    <div class="wrapper d-flex align-items-stretch">
<style type="text/css">
.ajax_loader {
  position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background: url('<?php echo e(url("img/loader.gif")); ?>') center no-repeat transparent;
  background-size: 200px 200px;
  text-align: center;
  display: none;
}
#msgCount{
    position: absolute;
    background-color: #3b71b9;
    width: 18px;
    height: 18px;
    font-size: 9px;
    font-weight: 400;
    color: #fff;
    border-radius: 50%;
    border: 2px solid #f2f2f2;
    display: flex;
    justify-content: center;
    align-items: center;
    left: unset;
    top: 20px;
    margin-left: 15px;
}
.magnify-modal{
  background: #fff;
  border-radius: 20px;
  margin-top: 10%;
  max-height: 350px;
}
.magnify-button-close{
  position: absolute;
  background: #3b71b9;
  color: #fff;
  border-radius: 50%;
  right: -15px;
  bottom: 15px;
}
.magnify-title{
  display: none;
}
.magnify-button-prev,.magnify-button-next{
  margin-right: 15px;
  border-radius: 50%;
  border: 1px solid darkgrey;
  box-shadow: 4px 2px grey;
  color: #3b71b9;
}
</style>
      <!-- Page Content  -->
      <div id="content" class="innerBody">
        
        <div class="innerHeader">
          <div class="container-fluid">
            <div class="innerBodyHeader">
            </div>
          </div>
        </div>

<div class="innerBodyOnly">
  <div class="container-fluid">
    <div class="connectionsBody" style="background: #fff;">
      <div class="position-relative connectionFlip">
        <div class="leftSlidePan closePan">
         
        </div>
        <div class="midPan windowHeight " style="margin:0 auto;height: unset;max-height: unset;width: 100%;">
          <div class="connectionsRight">
            <div class="tab-content">
              <div id="connection" class="tab-pane active">
                
                <div class="globalFeedList" >
                    <div id="div-to-image" style="margin-top: -55px;padding-bottom: 25px;background: #fff;width: 100%;">

                      <div class="connectedInfo" style="justify-content: center;">
                        <img class="img-fluid" src='<?php echo e($og_image); ?>'>
                      </div>

                      <!-- <div class="connectedInfo" style="display: block;">
                        <div class="text-center" style="padding-bottom: 15px;display: block;">
                  <img class="img-fluid" src="<?php echo e('img/full_size_logo.png'); ?>" style="max-height: 70px;" />
                </div></div>
                <div class="connectedInfo" style="justify-content: center;">
                        <div class="position-relative pr-0 feedImg">
                          <div class="" style="height: 150px;width: 150px;border: 1px solid #3b71b9;overflow: hidden;border-radius: 0;"><img style="border-radius:0;height:148px;width:148px;position: relative;" src="<?php echo e($feed->user1_profile_photo ? $feed->user1_profile_photo : 'new-design/img/profile_placeholder.jpg'); ?>" class="img-fluid" alt=""></div>
                          <div class="" style="height: 150px;width: 150px;border: 1px solid #3b71b9;overflow: hidden;border-radius: 0;"><img style="border-radius:0;height:148px;width:148px;position: relative;" src="<?php echo e($feed->user2_profile_photo ? $feed->user2_profile_photo : 'new-design/img/profile_placeholder.jpg'); ?>" class="img-fluid" alt=""></div>
                          <img src="new-design/img/feedadd.png" class="feedadd" alt="" style="left: 141px !important;
    top: 68px !important;">
                        </div>
                        
                      </div> -->
                      <div class="connectedInfo" style="justify-content: center;">
                      <div class=" position-relative connectedInfo">
                        <div class="rightPanText">
                          <p><a href="<?php echo e(url('user-profile/?feed='.$feed->_feed_id)); ?>"> <i><?php echo e($feed->user1_firstName); ?> <?php echo e($feed->user1_lastName); ?> </i></a>is <?php echo e($feed->feed_type); ?> with <a href="<?php echo e(url('user-profile/?feed='.$feed->_feed_id)); ?>"><i><?php echo e($feed->user2_firstName); ?> <?php echo e($feed->user2_lastName); ?></i> </a></p>
                        </div>
                      </div>
                    </div>
                    </div>
                    <div class="connectedInfo" style="justify-content: center;">
                        <div class="feedInfo position-relative" style="justify-content: center;">
                            <?php if($feed->feed_what): ?>
                              <p>
                                <span>What:</span> 
                                <span style="color: #91d639;"><?php echo e($feed->feed_event_type); ?></span>
                              </p>
                              <p class="pl-5"> <?php echo e($feed->feed_what); ?></p>
                            <?php endif; ?>
                            <?php if($feed->feed_where): ?><p><span>Where:  </span> <?php echo e($feed->feed_where); ?></p><?php endif; ?>
                            <?php if($feed->feed_when): ?><p><span>When: </span> <?php echo e($feed->feed_when); ?></p><?php endif; ?>
                            <?php if($feed->feed_keywords): ?><p><span>W5: </span> <?php echo e($feed->feed_keywords); ?></p><?php endif; ?>
                            
                        </div>
                    </div>


                        <!-- <img src="new-design/img/mapfeed.png" class="img-fluid" alt="" width="100%"> -->
                  <?php if($feed->feed_images): ?>
                        <div class="uploadPhotoHeading">Uploaded photos:</div>

                        <div class="feedPhoto">
                    <?php $__currentLoopData = $feed->feed_images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $img): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      
                          <a data-magnify="gallery" href="<?php echo e($img->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                            <img src="<?php echo e($img->fi_img_url); ?>" class="img-fluid" width="100%">
                          </a>
                      
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                        </div>
                  <?php endif; ?>

                    </div>
                </div>

              </div>
            </div>    
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
    </div>
  </section>
  <!-- Vendor JS Files -->
  <script src="<?php echo e(url('new-design/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>
  <script src="<?php echo e(url('new-design/assets/vendor/jquery.easing/jquery.easing.min.js')); ?>"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="<?php echo e(url('new-design/js/rangeslider.js')); ?>"></script>
  <script src="<?php echo e(url('new-design/assets/vendor/picker/js/picker.js')); ?>"></script>
  <script src="<?php echo e(url('js/toastr.min.js')); ?>"></script>
  <script src="<?php echo e(url('js/jquery.magnify.js')); ?>"></script>

  <script>
    $(document).ready(function(){
      //section height depends on screen resolution
      
      if ($(window).width() < 951) {
       $('.windowHeightLeft').css('max-height',$(window).height()-30);
       $('.windowHeightMid').css('max-height',$(window).height()-30);
       $('.windowHeightRight').css('max-height',$(window).height()-30);
       $('.windowHeightLeft').css('height',$(window).height()-30);
       $('.windowHeightMid').css('height',$(window).height()-30);
       $('.windowHeightRight').css('height',$(window).height()-30);
      }
      else {
        $('.windowHeightLeft').css('max-height',$(window).height()-110);
        $('.windowHeightMid').css('max-height',$(window).height()-110);
        $('.windowHeightRight').css('max-height',$(window).height()-110);
        $('.windowHeightLeft').css('height',$(window).height()-110);
        $('.windowHeightMid').css('height',$(window).height()-110);
        $('.windowHeightRight').css('height',$(window).height()-110);
      }

      $(".leftSlidePan .panelslideOpenButton").click(function(e) {
        e.preventDefault();
        $(".leftSlidePan").toggleClass("closePan");
        $(this).toggleClass("panelslideCloseButton");
        $(".panelslideOpenButton").removeAttr('style');
        $(this).css('z-index', 999);
        //alert('k');
      });
      $(".rightSlidePan .panelslideOpenButton").click(function(e) {
        e.preventDefault();
        $(".rightSlidePan").toggleClass("closePan");
        $(this).toggleClass("panelslideCloseButton");
        $(".panelslideOpenButton").removeAttr('style');
        $(this).css('z-index', 999);
        //alert('k');
      });


      $('input[type=range]').rangeslider({
          polyfill : false
      });

      $(".myReverseLookup").click(function(){
        $(".mainCenterOpen").addClass("removeMainCenter");
        $(".reverseLookupOnlyOne").removeClass("removeMainCenter");
      });
      $(".reverseLookupBackOne").click(function(){
        $(".reverseLookupOnlyOne").addClass("removeMainCenter");
        $(".mainCenterOpen").removeClass("removeMainCenter");
      });

      $(".searchReverseLookup").click(function(){
        $(".reverseLookupOnlyOne").addClass("removeMainCenter");
        $(".reverseLookupOnlyTwo").removeClass("removeMainCenter");
      });
      $(".reverseLookupBackTwo").click(function(){
        $(".reverseLookupOnlyTwo").addClass("removeMainCenter");
        $(".reverseLookupOnlyOne").removeClass("removeMainCenter");
      });
      
      $(".myLostFound").click(function(){
        $(".mainCenterOpen").addClass("removeMainCenter");
        $(".lostFoundOnly").removeClass("removeMainCenter");
      });
      $(".lostFoundBack").click(function(){
        $(".lostFoundOnly").addClass("removeMainCenter");
        $(".mainCenterOpen").removeClass("removeMainCenter");
      });

      $(".myDoppelganger").click(function(){
        $(".mainCenterOpen").addClass("removeMainCenter");
        $(".doppelgangerOnly").removeClass("doppelgangerClose");
      });
      $(".doppelgangerBack").click(function(){
        $(".doppelgangerOnly").addClass("doppelgangerClose");
        $(".mainCenterOpen").removeClass("removeMainCenter");
      });
      $(".activeFilterOn").click(function(){
        $(".myFilterAll").toggleClass("eventsFilterOff");
      });
      $(".eventsOnly").click(function(){
        $(this).toggleClass("activeEvents");
      });
      $(".datepicker").datepicker({
        format: "mm-dd-yyyy",
        autoclose: true,
      });
      $('#ex-multiselect').picker();
      $('[data-magnify]').magnify({
        headerToolbar: [
          
          'close'
        ],
        footerToolbar: [
          'prev',
          'next',
        ],
        modalWidth: 600,
        modalHeight: 600,
      });
    });
  </script>
<script>
    // $(document).ready(function(){
    //   $('#login').css('minHeight',$(window).height());
    // });

    $(document).ready(function(){
      //section height depends on screen resolution

      $(".addOtherBank").click(function(){
        $(".financialDetailsOpen").addClass("removeBank");
        $(".financialDetailsClose").removeClass("removeBank");
      });
      $(".closeOtherBank").click(function(){
        $(".financialDetailsClose").addClass("removeBank");
        $(".financialDetailsOpen").removeClass("removeBank");
      });

      toastr.options = {
        "closeButton"       : false,
        "debug"             : false,
        "newestOnTop"       : true,
        "progressBar"       : true,
        "positionClass"     : "toast-bottom-right",
        "preventDuplicates" : false,
        "onclick"           : null,
        "showDuration"      : "300",
        "hideDuration"      : "1000",
        "timeOut"           : "5000",
        "extendedTimeOut"   : "1000",
        "showEasing"        : "swing",
        "hideEasing"        : "linear",
        "showMethod"        : "fadeIn",
        "hideMethod"        : "fadeOut"
      };
    });
  </script>
<?php echo $__env->make('inc.script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<!-- <script src="https://unpkg.com/quickblox@2.13.0/quickblox.min.js"></script>-->

<script type="text/javascript">
$(document).ready(function(){
  /*html2canvas($("#div-to-image"), {
    onrendered: function(canvas) {
      var imgsrc = canvas.toDataURL("image/png");
      console.log(imgsrc);
      $("#newimg").attr('src', imgsrc);
      $("#img").show();
      var dataURL = canvas.toDataURL();
      var token = $('meta[name="_token"]').attr('content');
    console.log(token);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token,
        'Api-Auth-Key' : '#whts_cmn_8080'
      }
    });
      $.ajax({
          type: "POST",
          url: site_url('save-feed-screen'),
          data: {
            'imgBase64' : dataURL,
            'feed_id' : '<?php echo e(Request::get("feed")); ?>'
          }
      }).done(function(o) {
          console.log(o);
      });
    }
  });*/
});
</script>

</body>

</html><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/feed_details.blade.php ENDPATH**/ ?>