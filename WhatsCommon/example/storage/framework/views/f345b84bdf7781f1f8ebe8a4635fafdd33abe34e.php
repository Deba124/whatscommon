<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>What's Common</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <?php echo $__env->make('inc.css', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<link href="css/toastr.min.css" rel="stylesheet">
<body>
  <section id="login" class="signUpBg signUpBg2">
    <div class="container">
      <div class="AllLogin">
        <div class="loginForm signUp2Form">
          <div class="loginLogo">
            <img src="img/signUpLogo.png" class="img-fluid" alt="">
          </div>

          <form action="register-step-two" class="signUpForm2 xhr_form" id="signUpForm2">
            <?php echo csrf_field(); ?>
            <div class="row">
              <div class="col-md-6">
                <div class="signUpForm2Title">What</div>

                <div class="form-group">
                  <label for="email">Email</label>
                  <?php if(Session::has('userdata')): ?>
                  <input type="email" class="form-control formMail" placeholder="@gmail.com" name="email"
                   value="<?php echo e(Session::get('userdata')->email); ?>" readonly="">
                  <?php else: ?>
                  <input type="email" class="form-control formMail" placeholder="@gmail.com" name="email" value="">
                  <?php endif; ?>
                  <p class="text-danger" id="email_error"></p>
                </div>

                <div class="form-group">
                  <label for="number">Phone</label>
                  <?php if(Session::has('userdata')): ?>
                  <input type="number" class="form-control formPh" placeholder="000-000-000" name="phone"
                   value="<?php echo e(Session::get('userdata')->phone); ?>" >
                  <?php else: ?>
                  <input type="number" class="form-control formPh" placeholder="000-000-000" name="phone" value="">
                  <?php endif; ?>
                  <p class="text-danger" id="phone_error"></p>
                </div>

                <div class="signUpForm2Title">Where</div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="number">Country</label>
                      <select class="form-control formCountry" name="country" id="country">
                        <option value="" hidden="">Select Country</option>
            <?php if($countries): ?>
              <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($country->_country_id); ?>"><?php echo e($country->country_name); ?></option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
                      </select>
                      <p class="text-danger" id="country_error"></p>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="number">State</label>
                      <select class="form-control formState" name="state" id="province">
                        <option value="" hidden="">Select State</option>
                      
                      </select>
                      <p class="text-danger" id="state_error"></p>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="number">City</label>
                      
                      <select class="form-control formCity" name="city" id="city">
                        <option value="" hidden="">Select City</option>
                      </select>
                      <p class="text-danger" id="city_error"></p>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="text">Zip</label>
                      <input type="text" class="form-control formZip" placeholder="Zip" name="zip">
                      <p class="text-danger" id="zip_error"></p>
                    </div>
                  </div>

                  

                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="text">Street Address</label>
                      <input type="text" class="form-control formAddress" placeholder="123 abc Ave." name="street">
                      <p class="text-danger" id="street_error"></p>
                    </div>
                  </div>

                  
                </div>
              </div>

              <div class="col-md-6">
                <div class="signUpForm2Title">When</div>
                <div class="birthdayDateTitle">Birthday</div>
                <div class="birthdayDate">
                  <input type="hidden" name="dob" id="dob">
                  <div class="form-group birthdayMonth">
                    <label for="number">Month</label>
                    <select class="form-control formMonth dob" id="month" name="">
                      <option value="" hidden="">Month</option>
                      <option value="01">Jan</option>
                      <option value="02">Feb</option>
                      <option value="03">Mar</option>
                      <option value="04">Apr</option>
                      <option value="05">May</option>
                      <option value="06">Jun</option>
                      <option value="07">Jul</option>
                      <option value="08">Aug</option>
                      <option value="09">Sep</option>
                      <option value="10">Oct</option>
                      <option value="11">Nov</option>
                      <option value="12">Dec</option>
                    </select>
                    <img src="img/selectIcon.png" class="img-fluid selectIcon" alt="">
                  </div>
                  <div class="form-group birthdayDay">
                    <label for="number">Day</label>
                    <select class="form-control dob" id="day" name="">
                      <option value="" hidden="">DD</option>
                      <option value="01">1</option>
                      <option value="02">2</option>
                      <option value="03">3</option>
                      <option value="04">4</option>
                      <option value="05">5</option>
                      <option value="06">6</option>
                      <option value="07">7</option>
                      <option value="08">8</option>
                      <option value="09">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                      <option value="13">13</option>
                      <option value="14">14</option>
                      <option value="15">15</option>
                      <option value="16">16</option>
                      <option value="17">17</option>
                      <option value="18">18</option>
                      <option value="19">19</option>
                      <option value="20">20</option>
                      <option value="21">21</option>
                      <option value="22">22</option>
                      <option value="23">23</option>
                      <option value="24">24</option>
                      <option value="25">25</option>
                      <option value="26">26</option>
                      <option value="27">27</option>
                      <option value="28">28</option>
                      <option value="29">29</option>
                      <option value="30">30</option>
                      <option value="31">31</option>
                    </select>
                    <img src="img/selectIcon.png" class="img-fluid selectIcon" alt="">
                  </div>
                  <div class="form-group birthdayYear">
                    <label for="number">Year</label>
                    <select class="form-control dob" id="year" name="">
                      <option value="" hidden="">YYYY</option>
                      <?php for($i = 1950; $i <= 2021; $i++): ?>
                      <option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                      <?php endfor; ?>
                      
                    </select>
                    <img src="img/selectIcon.png" class="img-fluid selectIcon" alt="">
                  </div>
                  
                </div>
                <p class="text-danger" id="dob_error"></p>

                <div class="signUpForm2Title">Who, What, Where, When, Why</div>

                

                
                <div class="myAllEvents" id="keywords_div"></div>
                <input type="hidden" id="keywords" name="keywords" value="">
                <div class="input-group lifeEvent">
                  <input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
                  <div class="input-group-append">
                    <a onclick="addKeyword();" class="btn btn-add">Add</a>
                   </div>
                </div>
                <div class="inputBellowText">Ex: Word1, Word2, Word3</div>

                <div class="clearfix"></div>
              </div>
              <div class="form-group" id="responseDiv" style="display: none;"></div>
            </div>
            <button type="submit" class="btn btn-proceed">Sign Up</button>
          </form>
        </div>
      </div>
    </div>
  </section>
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  <script src="js/toastr.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      toastr.options = {
        "closeButton"       : false,
        "debug"             : false,
        "newestOnTop"       : true,
        "progressBar"       : true,
        "positionClass"     : "toast-bottom-right",
        "preventDuplicates" : false,
        "onclick"           : null,
        "showDuration"      : "300",
        "hideDuration"      : "1000",
        "timeOut"           : "5000",
        "extendedTimeOut"   : "1000",
        "showEasing"        : "swing",
        "hideEasing"        : "linear",
        "showMethod"        : "fadeIn",
        "hideMethod"        : "fadeOut"
      };
    });
  </script>
  <?php echo $__env->make('inc.script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <script>
$(document).on('change','.dob',function(){
  var day = $('#day').val();
  var month = $('#month').val();
  var year = $('#year').val();
  var dob = year+'-'+month+'-'+day;
  $('#dob').val(dob);
});
$(document).ready(function() {
  $(".removeField img").click(function() {
    $(this).parent().parent('.addFieldItem').remove();
  });

  $(".btn-add").click(function() {
    
  });

});
$(document).on('change', '#country', function(){
  var _country_id = $(this).val();
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id, 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="" hidden="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).val();
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id, 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
  </script>

</body>

</html><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/signup_details.blade.php ENDPATH**/ ?>