  <!-- Vendor JS Files -->


  <!-- Template Main JS File -->


  <!-- parallax -->
  <!-- <script src="assets/js/parallax.js"></script> -->
  <!-- <script src="assets/js/jquery.bxslider.js"></script> -->

  <script>
  function addKeyword(){
  var keyword = $('#add_keyword').val();
  var key_split = keyword.split(',');
  var split_count = 0;
  var keywords = $('#keywords').val();
  var split_str = keywords.split(",");
  split_count = split_str.length+1;
  console.log('split count => '+split_count);
  $.each( key_split, function( index, value ) {
    value = value.trim();
    
    if (split_str.indexOf(value) === -1) {
      
      $('#keywords_div').append('<div class="addFieldItem keywords" id="'+split_count+'" title="'+value+'">'+value+' <span class="removeField" onclick="removeKeyword('+split_count+');"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>');
      split_count += 1;
      console.log('split count2 => '+split_count);
    }
  });
  $('#add_keyword').val('');
  checkKeywords();
}

function checkKeywords(){
  var keywords = [];
  /*$('#keywords_div').html('');*/
  counter = 1;
  $(".keywords").each(function() {
    var keyval = $(this).attr('title');
    keywords.push(keyval);
  });
  $('#keywords_div').html('');
  for(i=0;i<keywords.length;++i){
    $('#keywords_div').append('<div class="addFieldItem keywords" id="'+counter+'" title="'+keywords[i]+'">'+keywords[i]+' <span class="removeField" onclick="removeKeyword('+counter+');"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>');
      counter += 1;
  }
  var keyword = keywords.join(',');
  $('#keywords').val(keyword);
  console.log('keywords => '+keyword);
}
function removeKeyword(id){
  $('#'+id).remove();
  checkKeywords();
}

function addKeyword2(){
  var keyword = $('#add_keyword2').val();
  var key_split = keyword.split(',');
  var split_count = 0;
  var keywords = $('#keywords2').val();
  var split_str = keywords.split(",");
  split_count = split_str.length+1;
  console.log('split count => '+split_count);
  $.each( key_split, function( index, value ) {
    value = value.trim();
    
    if (split_str.indexOf(value) === -1) {
      
      $('#keywords_div2').append('<div class="addFieldItem keywords" id="'+split_count+'" title="'+value+'">'+value+' <span class="removeField" onclick="removeKeyword2('+split_count+');"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>');
      split_count += 1;
      console.log('split count2 => '+split_count);
    }
  });
  $('#add_keyword2').val('');
  checkKeywords2();
}

function checkKeywords2(){
  var keywords = [];
  /*$('#keywords_div').html('');*/
  counter = 1;
  $(".keywords").each(function() {
    var keyval = $(this).attr('title');
    keywords.push(keyval);
  });
  $('#keywords_div2').html('');
  for(i=0;i<keywords.length;++i){
    $('#keywords_div2').append('<div class="addFieldItem keywords" id="'+counter+'" title="'+keywords[i]+'">'+keywords[i]+' <span class="removeField" onclick="removeKeyword2('+counter+');"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>');
      counter += 1;
  }
  var keyword = keywords.join(',');
  $('#keywords2').val(keyword);
  console.log('keywords => '+keyword);
}
function removeKeyword2(id){
  $('#'+id).remove();
  checkKeywords2();
}
  $(document).ready(function(){
    $('#login').css('minHeight',$(window).height());
  });
  function site_url(page_name=''){
    var site_url = '<?php echo url('/');  ?>';
    site_url = site_url+'/'+page_name;
    console.log(site_url);
    return site_url;
  }
  $(document).on('submit' ,'.xhr_form' , function(e){
    e.preventDefault();
    var token = $('input[name="_token"]').attr('value');
    console.log(token);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token,
        'Api-Auth-Key' : '#whts_cmn_8080'
      }
    });
    var submit_id = $(this).attr('id');
    var action    = site_url($(this).attr('action'));
    var submit_btn_content = $('#'+submit_id+' button[type="submit"]').html();

    $('#'+submit_id+' button[type="submit"]').html("<i class='fa fa-spinner fa-spin'></i> Loading");
    $('#'+submit_id+' button[type="submit"]').attr('disabled',true);
    $.ajax({
      type : "POST",
      data : new FormData(this),
      contentType: false,   
      cache: false,             
      processData:false, 
      url  : action,
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          $('#responseDiv').show();
          if(response.response_msg){
            toastr.success(response.response_msg);
            $('#responseDiv').html('<p class="text-success">'+response.response_msg+'</p>');
          }
          
          /*$(location).attr("href", site_url());*/
          if(response.close){
            $("#"+submit_id+"-modal").modal('hide');
          }
          if(response.evaluate){
            eval(response.evaluate);
          }
          if (response.redirect && response.time) {
            window.setTimeout(function(){window.location.href = response.redirect;}, response.time);
          }
          else if(response.redirect){
            $(location).attr("href", response.redirect); 
          }
          else if(response.time){
            window.setTimeout(function(){location.reload();}, response.time);
          }

          if(response.reponse_body.html_id){
            $('#'+response.reponse_body.html_id).html(response.reponse_body.html);
          }

          $('#'+submit_id+' button[type="submit"]').attr('disabled',false);
          $('#'+submit_id+' button[type="submit"]').html(submit_btn_content);
        }
      },error: function(jqXHR, textStatus, errorThrown){
        /*console.log(textStatus + ": " + jqXHR.status + " " + errorThrown);*/
        /*console.log(jqXHR);*/
        console.log(jqXHR.responseJSON);
        toastr.error(jqXHR.responseJSON.response_msg);
        $('#'+submit_id+' button[type="submit"]').attr('disabled',false);
        $('#'+submit_id+' button[type="submit"]').html(submit_btn_content);
        if(jqXHR.responseJSON.errors){
          var err = jqXHR.responseJSON.errors;
          $.each(err, function(key, value){

            if (err[key]) {
              $("#"+key+"_error").html(err[key]);
            }
            else{
              $("#"+key+"_error").html(''); 
            }
          });
        }
      }
    });
  });
function open_modal(link, data){
  /*$('#loading').show();*/

  var url = site_url(link+'-modal');
  var token = $('meta[name="_token"]').attr('content');
  console.log(token);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token,
      'Api-Auth-Key' : '#whts_cmn_8080'
    }
  });
  $.ajax({
    type : "POST",
    data : data,
    url  : url,
    success : function(response){
      console.log(response);
      
      if(response.response_code==200){
        if ($("#"+link+"-modal").length!=0) {
          $("#"+link+"-modal").html('');
        }
        jQuery('body').prepend(response.modal);
        $("#"+link+"-modal").modal('show');
      }else{
        toastr.error(response.response_msg);
      }
    },
    error: function(jqXHR, textStatus, errorThrown){
      console.log(jqXHR.responseJSON);
      toastr.error(response.response_msg);
    }
  });
}
function execute(link, data){
  $('#loading').show();
  var token = $('input[name="_token"]').attr('value');
  console.log(token);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token,
      'Api-Auth-Key' : '#whts_cmn_8080'
    }
  });
  var url = site_url(link);

  $.ajax({
    type : "POST",
    data : data,
    url  : url,
    success:function(response){
      /*console.log(response);
      return false;*/
      $('#loading').hide();
      if(response.response_code==200 || response.response_code==202){
        if(response.response_msg){
          toastr.success(response.response_msg);
        }
        
        if (response.redirect && response.time) {
          window.setTimeout(function(){window.location.href = response.redirect;}, response.time);
        }
        else if(response.redirect){
          $(location).attr("href", response.redirect); 
        }
        else if(response.time){
          window.setTimeout(function(){location.reload();}, response.time);
        }else{
          location.reload();
        }
      }
    },error: function(jqXHR, textStatus, errorThrown){
      console.log(jqXHR.responseJSON);
      toastr.error(jqXHR.responseJSON.response_msg);
    }
  });
}

function showHidePassword(id){
  if($('#'+id).attr('type')=='password'){
    $('#'+id).attr('type','text');
  }else{
    $('#'+id).attr('type','password');
  }
}
  </script>
<script>
window.fbAsyncInit = function() {
    // FB JavaScript SDK configuration and setup
  FB.init({
    appId      : '1009081199908339', // FB App ID
    cookie     : true,  // enable cookies to allow the server to access the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v3.2' // use graph api version 2.8
  });
    
    // Check whether the user already logged in
  /*FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
        //display user data
        getFbUserData();
    }
  });*/
};

// Load the JavaScript SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Facebook login with JavaScript SDK
function fbLogin() {
  FB.login(function (response) {
    if (response.authResponse) {
        // Get and display the user profile data
        getFbUserData();
    } else {
        /*document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';*/
        console.log('User cancelled login or did not fully authorize.');
    }
  }, {scope: 'email'});
}

// Fetch the user profile data from facebook
function getFbUserData(){
  FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
  function (response) {
    console.log('response => ',response);
    var name = response.first_name +' '+ response.last_name;
    var user_data = {'social_type':'facebook','social_id':response.id,'email':response.email,'name':name};
    console.log('user_data => ',user_data);
    execute('social-login',user_data);
    /*document.getElementById('fbLink').setAttribute("onclick","fbLogout()");
    document.getElementById('fbLink').innerHTML = 'Logout from Facebook';
    document.getElementById('status').innerHTML = '<p>Thanks for logging in, ' + response.first_name + '!</p>';
    document.getElementById('userData').innerHTML = '<h2>Facebook Profile Details</h2><p><img src="'+response.picture.data.url+'"/></p><p><b>FB ID:</b> '+response.id+'</p><p><b>Name:</b> '+response.first_name+' '+response.last_name+'</p><p><b>Email:</b> '+response.email+'</p><p><b>Gender:</b> '+response.gender+'</p><p><b>FB Profile:</b> <a target="_blank" href="'+response.link+'">click to view profile</a></p>';*/
  });
}
</script>
<script src="https://apis.google.com/js/api:client.js"></script>
<script>
  var googleUser = {};
  var startApp = function() {
    gapi.load('auth2', function(){
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      auth2 = gapi.auth2.init({
        client_id: '156165348207-5h3p2j108m2apnsq5f5n1mv6uto0lvr9.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        // Request scopes in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
      attachSignin(document.getElementById('googleLoginBtn'));
    });
  };

  function attachSignin(element) {
    console.log(element.id);
    auth2.attachClickHandler(element, {},
        function(googleUser) {
          console.log('googleUser => ',googleUser);
          var profile = googleUser.getBasicProfile();
          var user_data = {'social_type':'google','social_id':profile.getId(),'email':profile.getEmail(),'name':profile.getName()};
          console.log('user_data => ',user_data);
          execute('social-login',user_data);
          /*document.getElementById('name').innerText = "Signed in: " +
              googleUser.getBasicProfile().getName();*/
        }, function(error) {
          /*alert(JSON.stringify(error, undefined, 2));*/
          console.log(JSON.stringify(error, undefined, 2));
        });
  }
</script><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/inc/script.blade.php ENDPATH**/ ?>