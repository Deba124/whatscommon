 <!-- Modal starts -->       
<div class="modal fade" id="send-news-letter-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document" style="max-width: 800px;">
    <div class="modal-content " style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Send News Letter (<?php echo e($nt_data->nt_title); ?>)</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="send-news-letter">
          <div class="form-group" id="submit_status"></div>
          <?php echo csrf_field(); ?>
          <input type="hidden" name="_nt_id" value="<?php echo e($nt_data->_nt_id); ?>">
          <div class="row">
            <div class="col-md-9">
              <div class="form-group">
            <label>Users <span class="text-danger">*</span></label>
            <select class="form-control multiple" id="users" name="users[]" multiple="" data-live-search="true" style="width:100%!important">
<?php if($users): ?>

  <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e(strtolower($user->ns_user_email)); ?>"><?php echo e(strtolower($user->ns_user_email)); ?></option>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
            </select>
            <p class="text-danger" id="device_id_error"></p>
          </div>
            </div>
            <div class="col-md-3">            
              <!-- <input class="form-check-input" type="checkbox" value="" id="checkall" >
              <label class="form-check-label" for="checkall"></label>   -->
              <div class="form-check">
                <input type="radio" class="form-check-input form-control" id="checkall" style="margin-left: -80px;margin-top: 9px;">
                <label class="form-check-label" for="checkall" style="padding-top: 9px;font-size: 1.5rem;padding-left: 27px;">Select All</label>
              </div>          
            </div>
          </div>
          
          
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
<script type="text/javascript">
$(document).ready(function(){
  $("#checkall").click(function(){
    if($("#checkall").is(':checked')){
      $("#users > option").prop("selected", "selected");
      $("#users").trigger("change");
    } else {
      $("#users > option").removeAttr("selected");
      $("#users").trigger('change.select2');
      /*$("#users").each(function () { //added a each loop here
        $(this).select2('val', '')
    });*/
    }
  });
});
</script><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/xhr/send_newsletter_modal.blade.php ENDPATH**/ ?>