<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly innerBodyModify">
  <div class="container-fluid">
    <div class="settingBody">
      <div class="position-relative settingFlip">
          <div class="leftSlidePan closePan">
            <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
            <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
              <?php echo $__env->make('inc.life_event_left_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
          </div>
          <div class="midPan">
            <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                      <div class="row infoBodyRow">
                        <div class="col-md-5 infoBodyCol connectionsBody eventImgCol">
                          <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
                            <div class="innerHome">
                              <a href="<?php echo e(url('home')); ?>"><img src="img/home.png" class="img-fluid" alt=""></a>
                            </div>
                            <div class="feedbackImg">
                              <div class="personalImgAll">
                                <img src="new-design/img/userConnectImg.png" class="img-fluid" alt="">
                                <div class="personalImgTitle">Username Connect</div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7 connectionsBody infoBodyCol eventCol midHightControl">
                          <div class="connectionsBody feedbackRight windowHeight windowHeightMid">

                            <form class="xhr_form" method="post" action="api/life-event-upload" id="life-event-upload">
                              <?php echo csrf_field(); ?>
                              <div class="eventColHeading">What</div>
                                <div class="whatForm">
                                  <div class="form-group datingFilterBtn timeOptionSet">
                                    <div class="form-control timeOptionOnly enterFor activeTimeOption">Enter (you)</div>
                                    <div class="form-control timeOptionOnly searchFor">Search (for)</div>
                                    <input type="hidden" name="search_for" value="0" id="search_for" />
                                    <input type="hidden" name="type_id" value="11">
                                    <input type="hidden" name="user_id" value="<?php echo e(Session::get('userdata')['id']); ?>">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Game or Activity</label>
                                    <div class="selectCenterWithIcon" id="games_div">
                                      <select class="form-control  formGame " id="game_name" name="game_name">
                                        <option value="" hidden="">Select Game or Activity</option>
                            <?php if($games): ?>
                              <?php $__currentLoopData = $games; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $game): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option option="<?php echo e($game->game_name); ?>" data-id="<?php echo e($game->_game_id); ?>" data-game_teams="<?php echo e($game->game_teams); ?>"><?php echo e($game->game_name); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                                      <option value="add">Add</option>
                                    </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="">Team/Group</label>
                                    <div class="selectCenterWithIcon" id="game_teams_div">
                                      <select class="form-control  formTeam " id="team_name" name="team_name">
                                        <option value="" hidden="">Select Team/Group</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="">Username</label>
                                      <input type="text" class="form-control formUser selectCenterWithIcon2" name="username">
                                  </div>
                                </div>
                                <div class="eventColHeading">When</div>
                                <div class="whatForm whereFrom">
                                  <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">From</label>
                              <select class="form-control formMonth" id="" name="when_from_month">
                                <option value="" hidden>Month</option>
                                <option value="01">January</option>
                                <option value="02">February</option>
                                <option value="03">March</option>
                                <option value="04">April</option>
                                <option value="05">May</option>
                                <option value="06">June</option>
                                <option value="07">July</option>
                                <option value="08">August</option>
                                <option value="09">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay">
                              <label for="number">Day</label>
                              <select class="form-control" id="" name="when_from_day">
                                <option value="" hidden>DD</option>
                                <?php for($i=1;$i<=31;$i++): ?>
                                  <?php
                                    $j = sprintf('%02d', $i)
                                  ?>
                                 <option value="<?php echo e($j); ?>"><?php echo e($j); ?></option>
                                <?php endfor; ?>
                                
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayYear">
                              <label for="number">Year</label>
                              <select class="form-control" id="" name="when_from_year">
                                <option value="" hidden>YYYY</option>
                              <?php for($i = date('Y'); $i >= 1900; $i--): ?>
                                <option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                              <?php endfor; ?>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                          </div>
                                </div>
                                <div class="eventColHeading">Who, What, Where, When, Why</div>
                                <div class="whatForm whereFrom">
                                  <div class="myAllEvents" id="keywords_div"></div>
                                  <input type="hidden" id="keywords" value="" name="event_keywords">
                                  <div class="input-group lifeEvent">
                                    <input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
                                    <div class="input-group-append">
                                      <a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
                                     </div>
                                  </div>
                                  <div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                                </div>

                                <div class="whatForm whatFormBtnAll">
                                  <div class="whatFormBtnSE">
                                    <input type="hidden" id="is_draft" name="is_draft" value="0">
                                    <input type="hidden" name="show_life_event" id="show_life_event" value="0" />
                                    <button class="btn whatFormBtn w-100" onclick="$('#is_draft').val(1);$('#show_life_event').val(0);">Draft</button>
                                  </div>

                                  <div class="btn-Edit-save btn-feedback btn-premium">
                                    <button class="btn btn-Edit btn-save" onclick="$('#is_draft').val(0);$('#show_life_event').val(1);">what'scommon <br><span>upload your life event</span></button>
                                  </div>
                                </div>
                              </form>
                          </div>
                        </div>
                      </div>
                    </div>
          </div>
      </div>
    </div>
  </div>
</div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).ready(function(){
  $("#dep_time_am").click(function(){
      $("#dep_time_am").addClass("activeTimeOption");
      $("#dep_time_pm").removeClass("activeTimeOption");
      $('#dep_time_ampm').val('AM');
  });
  $("#dep_time_pm").click(function(){
    $("#dep_time_pm").addClass("activeTimeOption");
    $("#dep_time_am").removeClass("activeTimeOption");
    $('#dep_time_ampm').val('PM');
  });

  $("#arr_time_am").click(function(){
      $("#arr_time_am").addClass("activeTimeOption");
      $("#arr_time_pm").removeClass("activeTimeOption");
      $('#arr_time_ampm').val('AM');
  });
  $("#arr_time_pm").click(function(){
    $("#arr_time_pm").addClass("activeTimeOption");
    $("#arr_time_am").removeClass("activeTimeOption");
    $('#arr_time_ampm').val('PM');
  });

  $(".enterFor").click(function(){
    $(".enterFor").addClass("activeTimeOption");
    $(".searchFor").removeClass("activeTimeOption");
    $('#search_for').val(0);
  });
  $(".searchFor").click(function(){
    $(".searchFor").addClass("activeTimeOption");
    $(".enterFor").removeClass("activeTimeOption");
    $('#search_for').val(1);
  });

});
$(document).on('change','#game_name',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '<?php echo e(Session::get("userdata")["id"]); ?>';
    open_modal('add-master','user_id='+user_id+'&db_name=games&column_name=game_name&status_column=game_is_active');
  }else{
    var game_teams = $(this).find(':selected').data('game_teams');
    /*console.log('ranks => ');
    console.log(ranks);*/
    var team_name = '<option value="" hidden>Select </option>';
    if(game_teams){

      $.each( game_teams, function( index, rank ) {
        team_name += '<option value="'+rank.team_name+'">'+rank.team_name+'</option>';
      });
    }
    team_name += '<option value="add">Add</option>';
    $('#team_name').html(team_name);
  }
  
});
$(document).on('change', '#team_name', function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '<?php echo e(Session::get("userdata")["id"]); ?>';
    var game_id = $('#game_name').find(':selected').data('id');
    open_modal('add-master','user_id='+user_id+'&db_name=game_teams&column_name=team_name&status_column=team_is_active&dependent_name=team_gameid&dependent_value='+game_id+'&is_dependent=1');
  }
});
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="" hidden="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/life_events/username_connect.blade.php ENDPATH**/ ?>