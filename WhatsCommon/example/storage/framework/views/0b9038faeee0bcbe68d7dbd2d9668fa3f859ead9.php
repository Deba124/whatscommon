<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>What's Common</title>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <link href="<?php echo e(url('css/toastr.min.css')); ?>" rel="stylesheet">
  <?php echo $__env->make('inc.css', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<style type="text/css">
.feedbackTitle {
    font-weight: 600;
    font-size: 20px;
    color: #3b71b9;
    text-align: center;
    margin-bottom: 20px;
}
.aboutText {
    font-weight: 400;
    color: #b8b8b8;
    font-size: 14px;
    line-height: 18px;
    margin-top: 13px;
}
</style>
<body>
  <section id="login" class="loginBg">
    <div class="container">
      <div class="AllLogin">
        <div class="loginForm">
          <div class="loginLogo" >
            <a href="<?php echo e(url('/')); ?>">
              <img src="img/loginLogo.png" class="img-fluid" alt="">
            </a>
          </div>

          <form method="post" class="loginFormSection" id="loginForm">
            <?php echo csrf_field(); ?>
            <div class="form-group loginName" style="text-align: right;">
              <input type="text" class="form-control" placeholder="Username or Email Address" name="email" id="email" required="">
              <img src="img/get-otp.png" id="getOtpBtn" style="display: none;" class="img-fluid getOtp" onclick="getOtp();">
              <p id="error_email" class="text-danger"></p>
            </div>
            <input type="hidden" name="login_from" value="web">
            <input type="hidden" name="login_type" id="login_type" value="normal">
            <div class="form-group loginPass" id="pwddiv">
              <input type="password" class="form-control" id="pwd" placeholder="Password" name="password">
              <img src="img/passwordVisiable2.png" class="img-fluid passwordVisiable" alt="" onclick="showHidePassword('pwd');">
            </div>
            <div class="form-group loginPass" id="otpdiv" style="display: none;">
              <input type="text" class="form-control" placeholder="OTP" name="otp" id="otp">
            </div>
            <div class="form-group" id="responseDiv" style="display: none;"></div>
            <button type="submit" class="btn btn-block btn-welcome">Welcome Back!</button>
            <div class="row">
              <div class="col-6 col-sm-6 col-xs-6">
                <div class="loginForgot"><a href="javascript:void;" onclick="changeLoginType();" id="changeTypeBtn">Sign in with OTP</a></div>
              </div>
              <div class="col-6 col-sm-6 col-xs-6">
                <div class="loginForgot"><a href="forgot-password">Forgot password?</a></div>
              </div>
            </div>
            
          </form>
        </div>

        <div class="loginTextBelow">
          <div class="loginText">By Continuing, you agree to What’sCommon <a href="#" data-toggle="modal" data-target="#terms-modal">Terms of Service</a>.  We will manage your information as written in our <a href="#" data-toggle="modal" data-target="#privacy-modal">Privacy Policy</a> and <a href="#" data-toggle="modal" data-target="#cookie-modal">Cookie Policy</a>.</div>
        </div>

        <div class="loginForm">
          <div class="loginText">Don’t have an account yet? <a href="signup">Create Account</a></div>

          <div class="loginText loginWithText">Login with</div>

          <ul class="loginSocial list-unstyled mb-0">
            <li class="d-inline-block">
              <a href="#"><img src="img/twit.png" alt=""></a>
            </li>
            <!-- <li class="d-inline-block">
              <a href="#"><img src="img/tiktok.png" alt=""></a>
            </li>
            <li class="d-inline-block">
              <a href="#"><img src="img/ttt.png" alt=""></a>
            </li> -->
            <li class="d-inline-block">
              <a href="javascript:void" onclick="fbLogin();"><img src="img/facebookLogin.png" alt=""></a>
            </li>
            <li class="d-inline-block">
              <a href="javascript:void" id="googleLoginBtn" onclick="startApp()"><img src="img/gmail.png" alt=""></a>
            </li>
            <li class="d-inline-block">
              <a id="appleid-signin" data-color="black" data-border="true" data-type="sign in" data-mode="logo-only" data-width="36" data-height="36" data-size="36"></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="loginClose">
      <a href="/"><img src="img/loginClose.png" class="img-fluid" alt=""></a>
    </div>
  </section>
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="<?php echo e(url('js/toastr.min.js')); ?>"></script>
  <?php echo $__env->make('inc.script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
$(document).ready(function(){
  toastr.options = {
    "closeButton"       : false,
    "debug"             : false,
    "newestOnTop"       : true,
    "progressBar"       : true,
    "positionClass"     : "toast-bottom-right",
    "preventDuplicates" : false,
    "onclick"           : null,
    "showDuration"      : "300",
    "hideDuration"      : "1000",
    "timeOut"           : "5000",
    "extendedTimeOut"   : "1000",
    "showEasing"        : "swing",
    "hideEasing"        : "linear",
    "showMethod"        : "fadeIn",
    "hideMethod"        : "fadeOut"
  };
  startApp();
});
function changeLoginType(){
  var login_type = $('#login_type').val();
  $('#pwd').val('');
  $('#otp').val('');
  if(login_type=='normal'){
    $('#login_type').val('otp');
    $('#changeTypeBtn').html('Sign in with Password');
    $('#getOtpBtn').show();
    $('#otpdiv').show();
    $('#pwddiv').hide();
  }else{
    $('#login_type').val('normal');
    $('#changeTypeBtn').html('Sign in with OTP');
    $('#getOtpBtn').hide();
    $('#otpdiv').hide();
    $('#pwddiv').show();
  }
}
function getOtp(){
  var email = $('#email').val();
  if(email!=''){
    $('#error_email').html('');
    var token = $('input[name="_token"]').attr('value');
    console.log(token);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token,
        'Api-Auth-Key' : '#whts_cmn_8080'
      }
    });
    var action = site_url('get-otp');
    $.ajax({
      type : "POST",
      data : "username="+email , 
      url  : action,
      success : function(response){
        /*console.log(response);*/
        /*var obj = jQuery.parseJSON(response);*/
        $('#responseDiv').show();
        if(response.response_code==200){
          $('#responseDiv').html('<p class="text-success">'+response.response_msg+'</p>');
          toastr.success(response.response_msg);
        }else{
          $('#responseDiv').html('<p class="text-danger">'+response.response_msg+'</p>');
          toastr.error(response.response_msg);
        }
        $('#responseDiv').hide();
      },error: function(jqXHR, textStatus, errorThrown){
      /*console.log(textStatus + ": " + jqXHR.status + " " + errorThrown);*/
      /*console.log(jqXHR);*/
      console.log(jqXHR.responseJSON);
      $('#responseDiv').show();
      $('#responseDiv').html('<p class="alert alert-danger">'+jqXHR.responseJSON.response_msg+'</p>');
    }
  });
  }else{
    $('#error_email').html('Please enter Username/Email');
  }
}
$(document).on('submit','#loginForm',function(e){
  e.preventDefault();
  var token = $('input[name="_token"]').attr('value');
  console.log(token);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token,
      'Api-Auth-Key' : '#whts_cmn_8080'
    }
  });
  var action = site_url('new-login');
  $.ajax({
    type : "POST",
    data : new FormData(this),
    contentType: false,   
    cache: false,             
    processData:false, 
    url  : action,
    success : function(response){
      console.log(response);
      /*var obj = jQuery.parseJSON(response);*/
      if(response.response_code==200 || response.response_code==202){
        $('#responseDiv').html('<p class="text-success">'+response.response_msg+'</p>');
        if(response.redirect){
          $(location).attr("href", response.redirect);
        }
        
      }else{
        $('#responseDiv').html('<p class="text-danger">'+response.response_msg+'</p>');
      }
    },error: function(jqXHR, textStatus, errorThrown){
    /*console.log(textStatus + ": " + jqXHR.status + " " + errorThrown);*/
    /*console.log(jqXHR);*/
    console.log(jqXHR.responseJSON);
    $('#responseDiv').show();
    $('#responseDiv').html('<p class="alert alert-danger">'+jqXHR.responseJSON.response_msg+'</p>');
  }
  });
});

</script>
<div class="modal" tabindex="-1" role="dialog" id="terms-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;text-align: justify;">
      <div class="modal-header" style="display: none;">
        <h5 class="modal-title"><?php echo e($terms->page_title); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body aboutText">
        <div class="feedbackTitle"><span><?php echo e($terms->page_title); ?></span></div>
        <?php echo $terms->page_content; ?>

      </div>
      
    </div>
  </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="privacy-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;text-align: justify;">
      <div class="modal-header"  style="display: none;">
        <h5 class="modal-title"><?php echo e($privacy->page_title); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body aboutText">
        <div class="feedbackTitle"><span><?php echo e($privacy->page_title); ?></span></div>
        <?php echo $privacy->page_content; ?>

      </div>
      
    </div>
  </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="cookie-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;text-align: justify;">
      <div class="modal-header"  style="display: none;">
        <h5 class="modal-title"><?php echo e($cookie->page_title); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body aboutText">
        <div class="feedbackTitle"><span><?php echo e($cookie->page_title); ?></span></div>
        <?php echo $cookie->page_content; ?>

      </div>
    </div>
  </div>
</div>

</body>

</html><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/login.blade.php ENDPATH**/ ?>