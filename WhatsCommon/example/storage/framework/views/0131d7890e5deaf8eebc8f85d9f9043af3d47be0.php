<?php $__env->startSection('title', 'Dashboard'); ?>
<?php echo $__env->make('admin.inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('admin.inc.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <div class="content-wrapper">
    <div class="row">
      <div class="col grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="d-flex align-items-center justify-content-center">
              <div class="highlight-icon bg-light mr-3">
                <i class="mdi mdi-account-multiple text-danger icon-lg"></i>
              </div>
              <div class="wrapper">
                <p class="card-text mb-0"> Users</p>
                <div class="fluid-container">
                  <h5 class="card-title mb-0"> <?php echo e($counts->total_users ? $counts->total_users : 0); ?> </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="d-flex align-items-center justify-content-center">
              <div class="highlight-icon bg-light mr-3">
                <i class="mdi mdi-cube text-success icon-lg"></i>
              </div>
              <div class="wrapper">
                <p class="card-text mb-0"> Life Events </p>
                <div class="fluid-container">
                  <h5 class="card-title mb-0"> <?php echo e($counts->total_life_event ? $counts->total_life_event : 0); ?> </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="d-flex align-items-center justify-content-center">
              <div class="highlight-icon bg-light mr-3">
                <i class="mdi mdi-briefcase-check text-primary icon-lg"></i>
              </div>
              <div class="wrapper">
                <p class="card-text mb-0"> Matches</p>
                <div class="fluid-container">
                  <h5 class="card-title mb-0"> <?php echo e($counts->total_matched ? $counts->total_matched : 0); ?> </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <section class="panel">
              <div class="panel-body text-center" id="line">
                <canvas  height="300" width="450"></canvas>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <section class="panel">
              <div class="panel-body text-center" >
                <div class="row">
                  <div class="col-md-6"></div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control" id="event_type" onchange="pageReload();">
                        <option value="all" <?php echo e((!Request::get('type') || Request::get('type')=='all') ? 'selected': ''); ?>>All</option>
                        <option value="1" <?php echo e((Request::get('type')=='1') ? 'selected': ''); ?>>Personal</option>
                        <option value="2" <?php echo e((Request::get('type')=='2') ? 'selected': ''); ?>>Dating</option>
                        <option value="3" <?php echo e((Request::get('type')=='3') ? 'selected': ''); ?>>Adoption</option>
                        <option value="4" <?php echo e((Request::get('type')=='4') ? 'selected': ''); ?>>Travel</option>
                        <option value="5" <?php echo e((Request::get('type')=='5') ? 'selected': ''); ?>>Military</option>
                        <option value="6" <?php echo e((Request::get('type')=='6') ? 'selected': ''); ?>>Education</option>
                        <option value="7" <?php echo e((Request::get('type')=='7') ? 'selected': ''); ?>>Work</option>
                        <option value="8" <?php echo e((Request::get('type')=='8') ? 'selected': ''); ?>>Pets</option>
                        <option value="9" <?php echo e((Request::get('type')=='9') ? 'selected': ''); ?>>Lost & Found</option>
                        <option value="10" <?php echo e((Request::get('type')=='10') ? 'selected': ''); ?>>Doppelganger</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="text-center" id="line2">
                  <canvas  height="300" width="450" ></canvas>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php
  if(!Request::get('type') || Request::get('type')=='all'){
    $life_event_name = 'All Life Events';
  }else{
    if(Request::get('type')=='1'){
      $life_event_name = 'Personal';
    }
    if(Request::get('type')=='2'){
      $life_event_name = 'Dating';
    }
    if(Request::get('type')=='3'){
      $life_event_name = 'Adoption';
    }
    if(Request::get('type')=='4'){
      $life_event_name = 'Travel';
    }
    if(Request::get('type')=='5'){
      $life_event_name = 'Military';
    }
    if(Request::get('type')=='6'){
      $life_event_name = 'Education';
    }
    if(Request::get('type')=='7'){
      $life_event_name = 'Work';
    }
    if(Request::get('type')=='8'){
      $life_event_name = 'Pets';
    }
    if(Request::get('type')=='9'){
      $life_event_name = 'Lost & Found';
    }
    if(Request::get('type')=='10'){
      $life_event_name = 'Doppelganger';
    }
  }

?>
  <!-- content-wrapper ends -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['line']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Day');
    data.addColumn('number', 'Users');
    data.addColumn('number', 'Life Events');
    data.addColumn('number', 'Matches');
    data.addColumn('number', 'Connections');

    data.addRows([
<?php if($graph_counts): ?>
  <?php $__currentLoopData = $graph_counts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $graph): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      ['<?php echo e(date("d/m",strtotime($graph["date"]))); ?>', <?php echo e($graph["user_count"]); ?>, <?php echo e($graph["event_count"]); ?>, <?php echo e($graph["match_count"]); ?>,<?php echo e($graph["connection_count"]); ?>],
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
    ]);
    var options = {
      chart: {
        title: 'Last 10 Days Activity',
        subtitle: ''
      },
      axes: {
        x: {
          0: {side: 'top'}
        }
      },
    };

    var chart = new google.charts.Line(document.getElementById('line'));

    chart.draw(data, google.charts.Line.convertOptions(options));
  }

  google.charts.load('current', {'packages':['line']});
  google.charts.setOnLoadCallback(drawChart2);

  function drawChart2() {

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Day');
    data.addColumn('number', '<?php echo e($life_event_name); ?>');

    data.addRows([
<?php if($graph_counts): ?>
  <?php $__currentLoopData = $graph_counts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $graph): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<?php
  if(!Request::get('type') || Request::get('type')=='all'){
    $count = $graph["event_count"];
  }else if(isset($graph['check_count'])){
    $count = $graph["check_count"];
  }else{
    $count = 0;
  }

?>

      ['<?php echo e(date("d/m",strtotime($graph["date"]))); ?>', <?php echo e($count); ?>],
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
    ]);
    var options = {
      chart: {
        title: 'Last 10 Days Life Events',
        subtitle: ''
      },
      axes: {
        x: {
          0: {side: 'top'}
        }
      },
    };

    var chart = new google.charts.Line(document.getElementById('line2'));

    chart.draw(data, google.charts.Line.convertOptions(options));
  }
function pageReload(){
  var event_type = $('#event_type').val();
  var page_url = site_url('admin/dashboard?type='+event_type);
  $(location).attr("href", page_url);
}
</script>
<?php echo $__env->make('admin.inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/dashboard.blade.php ENDPATH**/ ?>