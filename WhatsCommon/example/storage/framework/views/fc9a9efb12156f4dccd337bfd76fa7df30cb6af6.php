<h5 class="leftrightHeading" id="feed_title"></h5><div class="greenCountFeed" id="feed_count" style="right: 17px;top: 2px;min-width: 60px;"><?php echo e($feed_count); ?></div>
<?php if($feeds): ?>
    <?php $__currentLoopData = $feeds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $feed): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="globalFeedList">

            <div class="connectedInfo">
                <div class="position-relative pr-0 feedImg">
                    <div class=""><img src="<?php echo e($feed->user1_profile_photo ? $feed->user1_profile_photo : 'new-design/img/profile_placeholder.jpg'); ?>" class="img-fluid" alt=""></div>
                    <div class=""><img src="<?php echo e($feed->user2_profile_photo ? $feed->user2_profile_photo : 'new-design/img/profile_placeholder.jpg'); ?>" class="img-fluid" alt=""></div>
                    <img src="new-design/img/feedadd.png" class="feedadd" alt="">
                </div>
                <div class="rightPanText">
                    <p><a href="<?php echo e($feed->feed_user1==$user_id ? url('settings') : url('user-profile/?feed='.$feed->_feed_id.'&user='.$feed->feed_user1)); ?>"> <i><?php echo e($feed->user1_firstName); ?> <?php echo e($feed->user1_lastName); ?> </i></a>is <?php echo e($feed->feed_type); ?> with <a href="<?php echo e($feed->feed_user2==$user_id ? url('settings') : url('user-profile/?feed='.$feed->_feed_id.'&user='.$feed->feed_user2)); ?>"><i><?php echo e($feed->user2_firstName); ?> <?php echo e($feed->user2_lastName); ?></i> </a></p>
                    <span>About <?php echo e(DateTime_Diff($feed->feed_created)); ?> ago</span>
                </div>
            </div>

            <div class="feedInfo position-relative">
                <?php if($feed->feed_what): ?>
                    <p>
                        <span>What:</span> 
                        <span style="color: #91d639;"><?php echo e($feed->feed_event_type); ?></span>
                    </p>
                <p class="pl-5"> <?php echo e($feed->feed_what); ?></p><?php endif; ?>
                <?php if($feed->feed_where): ?><p><span>Where:  </span> <?php echo e($feed->feed_where); ?></p><?php endif; ?>
                <?php if($feed->feed_when): ?><p><span>When: </span> <?php echo e($feed->feed_when); ?></p><?php endif; ?>
                <?php if($feed->feed_keywords): ?><p><span>W5: </span> <?php echo e($feed->feed_keywords); ?></p><?php endif; ?>
                <div class="greenCountFeed"><?php echo e($feed->feed_match_count); ?></div>
            </div>

            <!-- <img src="new-design/img/mapfeed.png" class="img-fluid" alt="" width="100%"> -->
        <?php if($feed->feed_images && count($feed->feed_images)>0): ?>
            <div class="uploadPhotoHeading">Uploaded photos:</div>

            <div class="feedPhoto">
            <?php $__currentLoopData = $feed->feed_images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $img): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <a data-magnify="gallery" data-caption="<?php echo e($img->fi_img_name); ?>" href="<?php echo e($img->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                    <img src="<?php echo e($img->fi_img_url); ?>" class="img-fluid" width="100%">
                  </a>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        <?php endif; ?>
            <div class="row text-center rightPanBtn">
                <!-- <a href="#" class="dismissButton"><img src="new-design/img/dismissIcon.png" class="img-fluid" alt=""> Dismiss</a> -->
                <a href="javascript:void;" onclick="open_modal('share-feed','feed_id=<?php echo e($feed->_feed_id); ?>')" class="dismissButton"><img src="new-design/img/shares_ic2.png" style="height: 15px;
    margin-top: -3px;" class="img-fluid" alt=""> Share</a>
                <a href="#" class="dismissButton" onclick="execute('api/hide-feed','user_id=<?php echo e($user_id); ?>&_feed_id=<?php echo e($feed->_feed_id); ?>');"><!-- <img src="new-design/img/dismissIcon.png" class="img-fluid" alt=""> --> Dismiss</a>
                <?php if($feed->is_connected==1): ?>
                    <?php
                      $receiver_id = $user_quick_blox_id == $feed->user1_quick_blox_id ? $feed->user2_quick_blox_id : $feed->user1_quick_blox_id;
                    ?>
                    <a href="<?php echo e(url('messages?qb='.$receiver_id)); ?>" class="messageButton"><img src="new-design/img/message-blue.png" class="img-fluid" alt="">Message</a>
                <?php endif; ?>
                <!-- <a href="#" class="tagstarButton"> Tagster Social</a> -->
                
            </div>

        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php else: ?>
<div class="globalFeedList">
    <div class="form-group text-center">No feeds available</div>
</div>
<?php endif; ?><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/xhr/search_feed.blade.php ENDPATH**/ ?>