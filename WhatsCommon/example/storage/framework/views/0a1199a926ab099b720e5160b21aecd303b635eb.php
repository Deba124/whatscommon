<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<style type="text/css">
.clear-rating{
  display: none !important;
}
/* Rating Star Widgets Style */
.rating-stars ul {
  list-style-type:none;
  padding:0;
  
  -moz-user-select:none;
  -webkit-user-select:none;
}
.rating-stars ul > li.star {
  display:inline-block;
  
}

/* Idle State of the stars */
.rating-stars ul > li.star > i.fa {
  font-size:50px; /* Change the size of the stars */
  color:#ccc; /* Color on idle state */
}

/* Hover state of the stars */
.rating-stars ul > li.star.hover > i.fa {
  color:#f2d370;
}

/* Selected state of the stars */
.rating-stars ul > li.star.selected > i.fa {
  color:#f2d370;
}
</style>
<div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                <div class="leftSlidePan <?php echo e(Request::get('menu')==1 ? '' : 'closePan'); ?>">
                  <a href="#" class="panelslideOpenButton <?php echo e(Request::get('menu')==1 ? 'panelslideCloseButton' : ''); ?>"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                  <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                  	<div class="settingLeftTitle">Information</div>
                  	<ul class="settingMenu mb-0 list-unstyled">
                  		<li class="settingMenuLi">
                  			<a class="settingMenu settingMenuActive" href="<?php echo e(url('feedback')); ?>"><img src="new-design/img/feedback-a.png" class="img-fluid menuIcon" alt="">Feedback</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('contact-us')); ?>"><img src="new-design/img/contact.png" class="img-fluid menuIcon" alt="">Contact Us</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('about-us')); ?>"><img src="new-design/img/about.png" class="img-fluid menuIcon" alt="">About Us</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('premium-plans')); ?>"><img src="new-design/img/premium.png" class="img-fluid menuIcon" alt="">WC Premium</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('donate')); ?>"><img src="new-design/img/donate.png" class="img-fluid menuIcon" alt="">Donate</a>
                  		</li>
                      <li class="settingMenuLi">
                          <a class="settingMenu " style="color: #ed1c24;" href="<?php echo e(url('logout')); ?>"><img src="<?php echo e(url('new-design/img/logout.png')); ?>" class="img-fluid menuIcon" alt="">Log Out</a>
                        </li>
                  	</ul>
                  </div>
                  <!-- <a href="<?php echo e(url('logout')); ?>" class="logOut" style="margin-top: -40px;"><img src="new-design/img/logout.png" class="img-fluid menuIcon" alt=""> Log Out</a> -->
                </div>
                <div class="midPan">
                  <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                    <div class="row infoBodyRow">
                      <div class="col-md-5 infoBodyCol">
                          <div class="innerHome">
                            <a href="<?php echo e(url('home')); ?>"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                          </div>
                        <div class="feedbackImg">
                          <img src="new-design/img/feedbackImg.png" class="img-fluid" alt="">
                        </div>
                      </div>
                      <div class="col-md-7 infoBodyCol">
                        <div class="feedbackRight">
                          <div class="feedbackTitle">Send Us <span>Feedbacks!</span></div>
                          <div class="feedbackTitle2">We love to hear from you!</div>
                          

                          
                          	<form class="feedbackForm xhr_form" method="POST" id="submit-feedback" action="api/feedback">
                            <?php echo csrf_field(); ?>
                            <input type="hidden" name="id" value="<?php echo e(Session::get('userdata')['id']); ?>">
                            <div class="feedbackStar">
                              
                              <div class='rating-stars text-center'>
                                <ul id='stars'>
                                  <li class='star' data-value='1'>
                                    <i class='fa fa-star fa-fw'></i>
                                  </li>
                                  <li class='star' data-value='2'>
                                    <i class='fa fa-star fa-fw'></i>
                                  </li>
                                  <li class='star' data-value='3'>
                                    <i class='fa fa-star fa-fw'></i>
                                  </li>
                                  <li class='star' data-value='4'>
                                    <i class='fa fa-star fa-fw'></i>
                                  </li>
                                  <li class='star' data-value='5'>
                                    <i class='fa fa-star fa-fw'></i>
                                  </li>
                                </ul>
                              </div>
                              <p class="text-danger" id="stars_error"></p>
                            </div>
                            <input type="hidden" name="stars" id="rating">
              <?php if($feedbacktypes): ?>
                            <div class="row">
                <?php $__currentLoopData = $feedbacktypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <div class="col-md-4 col-sm-6 col-xs-6">
                                <label class="feedBackLabel"><?php echo e($type->feed_type); ?>

                                  <input type="radio" name="type" value="<?php echo e($type->feed_id); ?>">
                                  <span class="checkmark"></span>
                                </label>
                              </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              <p class="text-danger" id="type_error"></p>
                            </div>
              <?php endif; ?>
                            <div class="form-group">
                              <textarea class="form-control" rows="4" id="comment" name="comment" placeholder="If you love us, refer a friend! If we fell short please let us know how to do better!"></textarea>
                              <p class="text-danger" id="comment_error"></p>
                            </div>

                            <div class="btn-Edit-save btn-feedback">
                              <button class="btn btn-Edit btn-save" type="submit">Send Feedback</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script type="text/javascript">
/*$(document).ready(function(){
  $("#input-id").rating();
});*/
$(document).ready(function(){
  
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
    /*var msg = "";
    if (ratingValue > 1) {
        msg = "Thanks! You rated this " + ratingValue + " stars.";
    }
    else {
        msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
    }*/
    $('#rating').val(ratingValue);
    
  });
  
  
});
</script><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/feedback.blade.php ENDPATH**/ ?>