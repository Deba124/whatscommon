 <!-- Modal starts -->       
<div class="modal fade" id="get-contact-setting-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Update Contact Setting</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="update-contact-setting">
          <div class="form-group" id="submit_status"></div>
          <?php echo csrf_field(); ?>
          <div class="form-group">
            <label>Address<span class="text-danger">*</span></label>
            <textarea class="form-control" name="cmsc_address"><?php echo e($contact_us->cmsc_address); ?></textarea>
            <p class="text-danger" id="cmsc_address_error"></p>
          </div>
          <div class="form-group">
            <label>Phone<span class="text-danger">*</span></label>
            <input type="text" name="cmsc_phone" class="form-control" value="<?php echo e($contact_us->cmsc_phone); ?>">
            <p class="text-danger" id="cmsc_phone_error"></p>
          </div>
          <div class="form-group">
            <label>Email<span class="text-danger">*</span></label>
            <input type="text" name="cmsc_email" class="form-control" value="<?php echo e($contact_us->cmsc_email); ?>">
            <p class="text-danger" id="cmsc_email_error"></p>
          </div>
          <div class="form-group row">
            <div class="col-md-6">
              <label>Facebook Link<span class="text-danger">*</span></label>
              <input type="text" name="cmsc_facebook_link" class="form-control" value="<?php echo e($contact_us->cmsc_facebook_link); ?>">
              <p class="text-danger" id="cmsc_facebook_link_error"></p>
            </div>
            <div class="col-md-6">
              <label>Youtube Link<span class="text-danger">*</span></label>
              <input type="text" name="cmsc_youtube_link" class="form-control" value="<?php echo e($contact_us->cmsc_youtube_link); ?>">
              <p class="text-danger" id="cmsc_youtube_link_error"></p>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-6">
              <label>LinkedIn Link<span class="text-danger">*</span></label>
              <input type="text" name="cmsc_linkedin_link" class="form-control" value="<?php echo e($contact_us->cmsc_linkedin_link); ?>">
              <p class="text-danger" id="cmsc_linkedin_link_error"></p>
            </div>
            <div class="col-md-6">
              <label>Instagram Link<span class="text-danger">*</span></label>
              <input type="text" name="cmsc_insta_link" class="form-control" value="<?php echo e($contact_us->cmsc_insta_link); ?>">
              <p class="text-danger" id="cmsc_insta_link_error"></p>
            </div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends --><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/xhr/get_contact_setting_modal.blade.php ENDPATH**/ ?>