<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>What's Common | <?php echo e($privacy->page_title); ?></title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <?php echo $__env->make('inc.css', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<link href="css/toastr.min.css" rel="stylesheet">
<body>
  <section id="login" class="signUpBg">
    <div class="container">
      <div class="AllLogin">
        <div class="loginForm" style="max-width: fit-content;">
          <div class="loginLogo">
            <img src="img/signUpLogo.png" class="img-fluid" alt="">
          </div>

          <div class="signUpHeading"><?php echo e($privacy->page_title); ?></div>
          <div class="signUpForm"><?php echo $privacy->page_content; ?></div>
        </div>
      </div>
    </div>
    <div class="loginClose" style="background: #3b8dcf;border-radius: 50%;">
      <a href="<?php echo e(url('/')); ?>"><img src="img/loginClose.png" class="img-fluid" alt=""></a>
    </div>
  </section>
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  <script src="js/toastr.min.js"></script>

  <?php echo $__env->make('inc.script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>

</html><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/privacy-policy.blade.php ENDPATH**/ ?>