<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                <div class="leftSlidePan <?php echo e(Request::get('menu')==1 ? '' : 'closePan'); ?>">
                  <a href="#" class="panelslideOpenButton <?php echo e(Request::get('menu')==1 ? 'panelslideCloseButton' : ''); ?>"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                  <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                  	<div class="settingLeftTitle">Settings</div>
                  	<ul class="settingMenu mb-0 list-unstyled">
                  		<li class="settingMenuLi">
                  			<a class="settingMenu settingMenuActive" href="<?php echo e(url('settings')); ?>"><img src="new-design/img/profileIcon-a.png" class="img-fluid menuIcon" alt="">Profile</a>
                  		</li>
                  		<!-- <li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('financial-settings')); ?>"><img src="new-design/img/financial.png" class="img-fluid menuIcon" alt="">Financial Settings</a>
                  		</li> -->
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('change-password')); ?>"><img src="new-design/img/change-password.png" class="img-fluid menuIcon" alt="">Change Password</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('security')); ?>"><img src="new-design/img/security.png" class="img-fluid menuIcon" alt="">Security and Privacy</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('blocked-users')); ?>"><img src="new-design/img/blocking.png" class="img-fluid menuIcon" alt="">Blocking</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('notification-settings')); ?>"><img src="new-design/img/notifications.png" class="img-fluid menuIcon" alt="">Notifications</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('link-accounts')); ?>"><img src="new-design/img/link-account.png" class="img-fluid menuIcon" alt="">Link Account</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('message-settings')); ?>"><img src="new-design/img/message-setting.png" class="img-fluid menuIcon" alt="">Message Settings</a>
                  		</li>
                  	</ul>
                  </div>
                </div>
                <div class="midPan">
                  <div class="connectionsBody settingBodyRight windowHeight windowHeightMid">
                        <div class="profileTop">
                        	<div class="connectionsAllDetailsTop">
	                            <div class="connectionsDetailsAvtarImg">
	                              <img src="<?php echo e($userdetails->profile_photo_path); ?>" class="img-fluid" alt="" id="profile_img">
	                              <img src="new-design/img/uploadImg.png" class="img-fluid uploadImg" alt="" onclick="chooseFile('img_input');">
                                <input type="file" name="user_image" id="img_input" onchange="readURL(this);" accept=".jpg,.png,.jpeg" style="display: none;">
	                            </div>

	                            <div class="connectionsDetailsInfo">
	                              <div class="connectionsDetailsInfoName"><?php echo e($userdetails->firstName); ?> <?php echo e($userdetails->lastName); ?></div>
	                              <div class="connectionsDetailsInfoEvents">
	                                <span class="eventsText"><?php echo e('@'); ?><?php echo e($userdetails->username); ?></span>
	                              </div>
	                            </div>
	                          </div>
                        </div>
                        <div class="profileForm">
                        	<form method="post" class="xhr_form" action="update-profile" id="update-profile">
                            <?php echo csrf_field(); ?>
                        		<div class="row">
                        			<div class="col-md-5">
                        				<div class="form-group">
          									      <label for="text">First Name</label>
          									      <input type="text" class="form-control" id="" value="<?php echo e($userdetails->firstName); ?>" placeholder="First Name" name="firstName">
                                  <p class="text-danger" id="firstName_error"></p>
          									    </div>
                              </div>
                              <div class="col-md-5">
                                <div class="form-group">
          									      <label for="text">Last Name</label>
          									      <input type="text" class="form-control" id="" placeholder="Last Name" name="lastName" value="<?php echo e($userdetails->lastName); ?>">
                                  <p class="text-danger" id="lastName_error"></p>
          									    </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
          									      <label for="text">M.I.</label>
          									      <input type="text" class="form-control" id="" placeholder="M.I." name="mi" value="<?php echo e($userdetails->mi); ?>">
                                  <p class="text-danger" id="mi_error"></p>
          									    </div>
                              </div>
                              <div class="col-md-5">
                                <div class="form-group">
          									      <label for="text">Username</label>
          									      <input type="text" class="form-control" id="" placeholder="Username" name="username" value="<?php echo e($userdetails->username); ?>">
                                  <p class="text-danger" id="username_error"></p>
          									    </div>
                              </div>
                              <div class="col-md-7">
                                <div class="form-group">
          									      <label for="email">Email</label>
          									      <input type="email" class="form-control" id="" placeholder="Username" name="email" value="<?php echo e($userdetails->email); ?>">
                                  <p class="text-danger" id="email_error"></p>
          									    </div>
                              </div>
                              <div class="col-md-5">
                                <label for="number">Phone Number</label>
                                <div class="input-group mb-3">
  
    <select class="form-control " style="max-width: max-content;" name="isd_code">
<?php if($countries): ?>
  <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <?php
  if(!$country->country_isd){
    continue;
  }
  ?>
      <option value="+<?php echo e($country->country_isd); ?>" <?php if($country->country_isd==$userdetails->isd_code): ?> selected <?php endif; ?>>+<?php echo e($country->country_isd); ?></option>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>    
    </select>
  
          									      
          									      <input type="text" class="form-control" id="" placeholder="Phone Number" name="phone" value="<?php echo e($userdetails->phone); ?>" style="border-top-right-radius: 6px;border-bottom-right-radius: 6px;">
                                  <p class="text-danger" id="phone_error"></p>
          									    </div>
                              </div>
                              <div class="col-md-5">
                                <div class="form-group">
          									      <label for="number">Birthday</label>
          									      <input type="text" class="form-control datepicker" readonly="" id="" placeholder="MM/DD/YYYY" name="dob" value="<?php if($userdetails->dob): ?><?php echo e(web_date_format($userdetails->dob)); ?><?php endif; ?>" style="background: #fff;">
                                  <p class="text-danger" id="dob_error"></p>
          									    </div>
                              </div>
                              
                        		</div>
          							    <div class="btn-Edit-save">
          							    	
          							    	<button type="submit" class="btn btn-Edit btn-save">Save</button>
          							    </div>
          							  </form>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">

function chooseFile(id){
  $("#"+id).click();
}
function readURL(input) {
    //alert('sasa');
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#profile_img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
        var data = new FormData();

        data.append('user_picture', input.files[0]);
        var token = $('input[name="_token"]').attr('value');
        console.log(token);
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': token,
            'Api-Auth-Key' : '#whts_cmn_8080'
          }
        });
        var action = site_url('change-profile-picture');
        $.ajax({
        type : "POST",
        data : data,
        cache: false,
        contentType: false,
        processData: false,
        url  : action,
        success:function(response){
          //console.log(result);
          if(response.response_code==200){
            toastr.success(response.response_msg);
            if(response.reponse_body.img_src){
              $('#profile_img').attr('src', response.reponse_body.img_src);
            }
          }
          /*var obj = jQuery.parseJSON(result);
          if (obj.success) {
            toastr.success(obj.success);
            $('#profile_img').attr('src', obj.img_src);
          }
          else if (obj.error_404) {
          toastr.error(obj.error_404);
        }
        else if (obj.error) {
          toastr.warning(obj.error);
        }*/
        },error: function(jqXHR, textStatus, errorThrown){
          console.log(jqXHR.responseJSON);
          toastr.error(jqXHR.responseJSON.response_msg);
          if(jqXHR.responseJSON.errors){
            var err = jqXHR.responseJSON.errors;
            $.each(err, function(key, value){
              toastr.error(err[key]);
            });
          }
        }
        });
    }
}
</script><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/profile_settings.blade.php ENDPATH**/ ?>