 <!-- Modal starts -->       
<div class="modal fade" id="edit-vehicle-type-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Update Vehicle Type</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="edit-vehicle-type">
          <div class="form-group" id="submit_status"></div>
          <?php echo csrf_field(); ?>
          <input type="hidden" name="_vehicle_type_id" value="<?php echo e($relation_data->id); ?>">
          <div class="form-group">
            <label>Vehicle Type Name<span class="text-danger">*</span></label>
            <input type="text" name="vehicle_type_name" class="form-control" value="<?php echo e($relation_data->vehicle_type_name); ?>">
            <p class="text-danger" id="vehicle_type_name_error"></p>
          </div>
          
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends --><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/xhr/edit_vehicle_type_modal.blade.php ENDPATH**/ ?>