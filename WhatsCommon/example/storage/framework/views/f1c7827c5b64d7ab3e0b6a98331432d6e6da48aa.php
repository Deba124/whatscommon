<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly innerBodyModify">
  	<div class="container-fluid">
    	<div class="settingBody">
	      <div class="position-relative settingFlip">
	       <div class="leftSlidePan closePan">
            <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
            <div class="lifeDraftLeftTitle"> <?php echo e($event_data->event_is_draft==0 ? 'LIFE EVENTS' : 'DRAFTS'); ?> </div>
            <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
              <ul class="list-unstyled mb-0 lifeEventMenu">
                <li class="lifeEventMenuLi lifeDraftLi <?php echo e($event_data->event_is_draft==0 ? 'lifeEventMenuLiActive' : ''); ?>">
                  <a href="<?php echo e(url('life-events')); ?>" class="lifeEventMenuLink">
                    <span class="lifeEventMenuText">Created Life Events</span>
                    <span class="lifeDraftCount"><?php echo e($event_count); ?></span>
                  </a>
                </li>
                <li class="lifeEventMenuLi lifeDraftLi <?php echo e($event_data->event_is_draft==1 ? 'lifeEventMenuLiActive' : ''); ?>">
                  <a href="<?php echo e(url('drafts')); ?>" class="lifeEventMenuLink lifeDraftMenuLink">
                    <span class="lifeEventMenuText">Drafts</span>
                    <span class="lifeDraftCount"><?php echo e($draft_count); ?></span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
	          	<div class="midPan">
		            <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
		              	<div class="row infoBodyRow">
			                <div class="col-md-5 infoBodyCol connectionsBody eventImgCol">
                        <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
			                    <div class="innerHome">
			                    	<a href="<?php echo e(url('home')); ?>"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
			                    </div>
			                  	<div class="feedbackImg">
				                    <div class="personalImgAll">
				                      	<img src="new-design/img/lostfound.png" class="img-fluid" alt="">
				                      	<!-- <div class="personalImgTitle">Lost & Found</div> -->
				                    </div>
			                  	</div>
                        </div>
			               </div>
			                <div class="col-md-7 infoBodyCol connectionsBody eventImgCol">
                          <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
			                  		<form class="xhr_form" method="post" action="api/save-life-event" id="save-life-event">
                              <?php echo csrf_field(); ?>
                              <div class="eventColHeading">What </div><img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                              <input type="hidden" name="type_id" value="9">
                              <input type="hidden" name="user_id" value="<?php echo e(Session::get('userdata')['id']); ?>">
                              <input type="hidden" name="_event_id" value="<?php echo e($event_data->_event_id); ?>">
                              <div class="whatForm whatFormDationg">
                                <!-- <div class="form-group datingFilterBtn timeOptionSet">
                                  <div class="form-control timeOptionOnly enterFor activeTimeOption">Enter</div>
                                  <div class="form-control timeOptionOnly searchFor">Search</div>
                                </div> -->
                                <div class="form-group datingFilterBtn timeOptionSet">
                                    <div class="form-control timeOptionOnly enterFor <?php echo e($event_data->event_dating_search_for==0 ? 'activeTimeOption' : ''); ?>">Enter (you)</div>
                                    <div class="form-control timeOptionOnly searchFor <?php echo e($event_data->event_dating_search_for==1 ? 'activeTimeOption' : ''); ?>">Search (for)</div>
                                    <input type="hidden" name="search_for" value="<?php echo e($event_data->event_dating_search_for); ?>" id="search_for" />
                                </div>

                                <div class="form-group datingSpecific">
                                  <label for="">Item</label>
                                  <select class="form-control selectCenter" id="items" name="item_name">
                                    <option value="" hidden>Select</option>
                    <?php if($items): ?>
                      <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                          $selected = $item->item_name == $event_data->event_item_name ? 'selected' : '';
                        ?>
                              <option option="<?php echo e($item->item_name); ?>" <?php echo e($selected); ?> data-sizes="<?php echo e($item->item_sizes); ?>"><?php echo e($item->item_name); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                  </select>
                                </div>

                                <div class="whatReason1 vehicleForm">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">Size</label>
                                        <select class="form-control formSize" id="sizes" name="item_size">
                                          <option value="">Select</option>
                    <?php if($item_sizes): ?>
                      <?php $__currentLoopData = $item_sizes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $size): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                          $selected = $size->is_name==$event_data->event_item_size ? 'selected' : '';
                        ?>
                                          <option value="<?php echo e($size->is_name); ?>" <?php echo e($selected); ?>><?php echo e($size->is_name); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                        </select>
                                        <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">Material</label>
                                        <select class="form-control formMaterial" name="item_material">
                                          <option value="">Select</option>
                    <?php if($materials): ?>
                      <?php $__currentLoopData = $materials; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $material): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                          $selected = $material->material_name==$event_data->event_item_material ? 'selected' : '';
                        ?>
                              <option option="<?php echo e($material->material_name); ?>" <?php echo e($selected); ?>><?php echo e($material->material_name); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                        </select>
                                        <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">Outdoor</label>
                                        <select class="form-control formOutdoor" name="outdoor">
                                          <option value="">Select</option>
                    <?php if($outdoors): ?>
                      <?php $__currentLoopData = $outdoors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $outdoor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                          $selected = $outdoor->outdoor_name==$event_data->event_outdoor ? 'selected' : '';
                        ?>
                              <option option="<?php echo e($outdoor->outdoor_name); ?>" <?php echo e($selected); ?>><?php echo e($outdoor->outdoor_name); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                        </select>
                                        <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">Indoor</label>
                                        <select class="form-control formLocation" name="indoor">
                                          <option value="">Select</option>
                    <?php if($indoors): ?>
                      <?php $__currentLoopData = $indoors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $indoor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                          $selected = $indoor->indoor_name==$event_data->event_indoor ? 'selected' : '';
                        ?>
                              <option option="<?php echo e($indoor->indoor_name); ?>" <?php echo e($selected); ?>><?php echo e($indoor->indoor_name); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                        </select>
                                        <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">In Motion</label>
                                        <select class="form-control formAirline" name="inmotion">
                                          <option value="">Select</option>
                    <?php if($inmotions): ?>
                      <?php $__currentLoopData = $inmotions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $inmotion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                          $selected = $inmotion->inmotion_name==$event_data->event_inmotion ? 'selected' : '';
                        ?>
                              <option option="<?php echo e($inmotion->inmotion_name); ?>" <?php echo e($selected); ?>><?php echo e($inmotion->inmotion_name); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                                        </select>
                                        <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">Quantity</label>
                                        <input type="text" name="other" class="form-control formOther" value="<?php echo e($event_data->event_other); ?>">
                                        <!-- <select class="form-control formOther">
                                          <option>1</option>
                                          <option>2</option>
                                        </select> -->
                                        <!-- <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt=""> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                
                              </div>
                              <div class="eventColHeading">Where </div>
                              <div class="whatForm whereFrom">
                                <div class="form-group">
                                  <label for="">Country</label>
                                  <select class="form-control formCountry" name="country" id="country2">
                                    <option value="" hidden>Select</option>
                  <?php if($countries): ?>
                    <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php
                        $selected = ($where_data && $country->country_name==$where_data->where_country) ? 'selected' : '';
                      ?>
                                    <option value="<?php echo e($country->country_name); ?>" <?php echo e($selected); ?> data-id="<?php echo e($country->_country_id); ?>"><?php echo e($country->country_name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php endif; ?>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                                <div class="form-group">
                                  <label for="">State/County/Province</label>
                                  <select class="form-control formState" name="state" id="province2">
                                    <option value="" hidden>Select</option>
                  <?php if($provinces): ?>
                    <?php $__currentLoopData = $provinces; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $province): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php
                        $selected = ($where_data && $province->province_name==$where_data->where_state) ? 'selected' : '';
                      ?>
                                    <option value="<?php echo e($province->province_name); ?>" <?php echo e($selected); ?> data-id="<?php echo e($province->_province_id); ?>"><?php echo e($province->province_name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php endif; ?>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                                <div class="form-group">
                                  <label for="">City</label>
                                  <select class="form-control formCity" name="city" id="city2">
                                    <option value="" hidden>Select</option>
                  <?php if($cities): ?>
                    <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php
                        $selected = ($where_data && $city->city_name==$where_data->where_city) ? 'selected' : '';
                      ?>
                                    <option value="<?php echo e($city->city_name); ?>" <?php echo e($selected); ?> data-id="<?php echo e($city->_city_id); ?>"><?php echo e($city->city_name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php endif; ?>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                                <div class="form-group">
                                  <label for="">Street Address</label>
                                  <input type="text" class="form-control formAddress" id="" placeholder="123 abc Ave." name="street" value="<?php echo e($where_data ? $where_data->where_street : ''); ?>">
                                </div>
                                <div class="form-group">
                                  <label for="">ZIP</label>
                                  <input type="text" class="form-control formZip" id="" placeholder="ZIP" name="zip" value="<?php echo e($where_data ? $where_data->where_zip : ''); ?>">
                                </div>
                              </div>
                              <div class="eventColHeading">When </div>
                              <div class="whatForm whereFrom">
                                <div class="birthdayDate">
                                  <div class="form-group birthdayMonth">
                                    <label for="number">From</label>
                                    <select class="form-control formMonth" id="" name="when_from_month">
                                      <option value="" hidden>Month</option>
                                <option value="01" <?php echo e($when_data->when_from_month=='01'?'selected':''); ?>>January</option>
                                <option value="02" <?php echo e($when_data->when_from_month=='02'?'selected':''); ?>>February</option>
                                <option value="03" <?php echo e($when_data->when_from_month=='03'?'selected':''); ?>>March</option>
                                <option value="04" <?php echo e($when_data->when_from_month=='04'?'selected':''); ?>>April</option>
                                <option value="05" <?php echo e($when_data->when_from_month=='05'?'selected':''); ?>>May</option>
                                <option value="06" <?php echo e($when_data->when_from_month=='06'?'selected':''); ?>>June</option>
                                <option value="07" <?php echo e($when_data->when_from_month=='07'?'selected':''); ?>>July</option>
                                <option value="08" <?php echo e($when_data->when_from_month=='08'?'selected':''); ?>>August</option>
                                <option value="09" <?php echo e($when_data->when_from_month=='09'?'selected':''); ?>>September</option>
                                <option value="10" <?php echo e($when_data->when_from_month=='10'?'selected':''); ?>>October</option>
                                <option value="11" <?php echo e($when_data->when_from_month=='11'?'selected':''); ?>>November</option>
                                <option value="12" <?php echo e($when_data->when_from_month=='12'?'selected':''); ?>>December</option>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group birthdayDay">
                                    <label for="number">Day</label>
                                    <select class="form-control" id="" name="when_from_day">
                                      <option value="" hidden>DD</option>
                                      <?php for($i=1;$i<=31;$i++): ?>
                                        <?php
                                          $j = sprintf('%02d', $i)
                                        ?>
                                       <option value="<?php echo e($j); ?>" <?php echo e($when_data->when_from_day==$j ? 'selected':''); ?>><?php echo e($j); ?></option>
                                      <?php endfor; ?>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group birthdayYear">
                                    <label for="number">Year</label>
                                    <select class="form-control" id="" name="when_from_year">
                                      <option value="" hidden>YYYY</option>
                                    <?php for($i = 1900; $i <= date('Y'); $i++): ?>
                                      <option value="<?php echo e($i); ?>" <?php echo e($when_data->when_from_year==$i ? 'selected':''); ?>><?php echo e($i); ?></option>
                                    <?php endfor; ?>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                </div>
                              </div>
                              <div class="eventColHeading">Who, What, Where, When, Why </div>
                              <div class="whatForm whereFrom">
                                
                                <div class="myAllEvents" id="keywords_div2">
                        <?php if($event_data->event_keywords): ?>
                          <?php
                            $keywords = explode(',',$event_data->event_keywords);
                          ?>
                          <?php $__currentLoopData = $keywords; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $keyword): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <div class="addFieldItem keywords" id="<?php echo e($i); ?>" title="<?php echo e($keyword); ?>"><?php echo e($keyword); ?> <span class="removeField" onclick="removeKeyword('<?php echo e($i); ?>');"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                                </div>
                                <input type="hidden" id="keywords2" value="<?php echo e($event_data->event_keywords); ?>" name="event_keywords">
                                <div class="input-group lifeEvent">
                                  <input type="text" class="form-control" id="add_keyword2" placeholder="Life Event Keyword(s)">
                                  <div class="input-group-append">
                                    <a onclick="addKeyword2();" class="btn btn-add" style="text-decoration: none;">Add</a>
                                  </div>
                                </div>
                                <div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                              </div>
                        
                        <?php echo $__env->make('inc.life_event_img_upload', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                            </form>
			                  	</div>
			                </div>
		              	</div>
		            </div>
	          	</div>
	      	</div>
    	</div>
  	</div>
</div>
<!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Lost/Found</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Lost/Found</label>
            <p>If you lost something, this is the perfect life event for you. You can describe it
and enter your missing item or search for it. If someone found something and
they want to return it, they descried it and location and… ITEM FOUND!</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>This is the location where you lost or found the item.</p>
          </div>
          <div class="form-group">
            <label>WHEN:</label>
            <p>A specific date when you lost or found the item.</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).ready(function(){
  $(".enterFor").click(function(){
        $(".enterFor").addClass("activeTimeOption");
        $(".searchFor").removeClass("activeTimeOption");
        $('#search_for').val(0);
    });
    $(".searchFor").click(function(){
        $(".searchFor").addClass("activeTimeOption");
        $(".enterFor").removeClass("activeTimeOption");
        $('#search_for').val(1);
    });
});
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/life_events/edit/lost_and_found.blade.php ENDPATH**/ ?>