<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly">
    <div class="container-fluid">
        <div class="settingBody">
            <div class="position-relative settingFlip">
                <div class="leftSlidePan closePan">
                	<a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                  	<div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                  		<div class="settingLeftTitle">Information</div>
	                  	<ul class="settingMenu mb-0 list-unstyled">
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu" href="<?php echo e(url('feedback')); ?>"><img src="new-design/img/feedback.png" class="img-fluid menuIcon" alt="">Feedback</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu settingMenuActive" href="<?php echo e(url('contact-us')); ?>"><img src="new-design/img/contact-a.png" class="img-fluid menuIcon" alt="">Contact Us</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu" href="<?php echo e(url('about-us')); ?>"><img src="new-design/img/about.png" class="img-fluid menuIcon" alt="">About Us</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu" href="<?php echo e(url('premium-plans')); ?>"><img src="new-design/img/premium.png" class="img-fluid menuIcon" alt="">WC Premium</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu" href="<?php echo e(url('donate')); ?>"><img src="new-design/img/donate.png" class="img-fluid menuIcon" alt="">Donate</a>
	                  		</li>
	                  	</ul>
                  	</div>
                  	<a href="<?php echo e(url('logout')); ?>" class="logOut" style="margin-top: -40px;"><img src="new-design/img/logout.png" class="img-fluid menuIcon" alt=""> Log Out</a>
                </div>
                <div class="midPan">
                  	<div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
	                    <div class="row infoBodyRow">
	                      	<div class="col-md-5 infoBodyCol">
		                        <div class="innerHome">
		                          	<a href="<?php echo e(url('home')); ?>"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
		                        </div>
		                        <div class="feedbackImg contactImg">
		                          	<img src="new-design/img/contactImg.png" class="img-fluid" alt="">
		                        </div>
		                        <div class="directInfo">
		                          	<div class="directInfoTitle">Direct Info</div>
		                          	<ul class="directInfoDetails list-unstyled mb-0">
		                            	<li class="mail">whatscommon@gmail.com</li>
		                            	<li class="office">Boardman, OH, USA</li>
		                          	</ul>
		                        </div>
	                      	</div>
	                      	<div class="col-md-7 infoBodyCol">
	                        	<div class="feedbackRight">
	                          		<div class="feedbackTitle"><span>Contact Us</span></div>
	                          		<div class="contactTitle2">Feel free to contact us any time. We will get back to you as soon as we can!</div>

	                          		<form class="feedbackForm contactForm xhr_form" method="post" action="api/contact-us" id="contact-us">
	                          			<?php echo csrf_field(); ?>
	                          			<input type="hidden" name="id" value="<?php echo e(Session::get('userdata')['id']); ?>">
			                            <div class="form-inline">
			                              	<div class="form-group">
			                                	<label for="text">Name</label>
			                                	<input type="text" class="form-control" id="name" placeholder="Name" name="name">
			                              	</div>
			                            </div>
			                            <div class="form-inline">
			                              	<div class="form-group">
			                                	<label for="email">Email</label>
			                                	<input type="email" class="form-control" id="email" placeholder="Email" name="email">
			                              	</div>
			                            </div>
			                            <div class="form-group contactMsg">
			                              	<label for="text">Message</label>
			                              	<textarea class="form-control" rows="4" name="message" id="comment" placeholder="Type here"></textarea>
			                            </div>

			                            <div class="btn-Edit-save btn-feedback">
			                              <button class="btn btn-Edit btn-save" type="submit">Send Message</button>
			                            </div>
	                          		</form>
	                        	</div>
	                      	</div>
	                    </div>
                  	</div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/contact_us.blade.php ENDPATH**/ ?>