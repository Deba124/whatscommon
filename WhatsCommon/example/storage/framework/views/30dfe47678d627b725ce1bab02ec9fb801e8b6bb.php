<?php $__env->startSection('title', 'Dashboard'); ?>
<?php echo $__env->make('admin.inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('admin.inc.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <div class="content-wrapper">
    <div class="row">
      <div class="col grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="d-flex align-items-center justify-content-center">
              <div class="highlight-icon bg-light mr-3">
                <i class="mdi mdi-account-multiple text-danger icon-lg"></i>
              </div>
              <div class="wrapper">
                <p class="card-text mb-0"> Users</p>
                <div class="fluid-container">
                  <h5 class="card-title mb-0"> <?php echo e($counts->total_users ? $counts->total_users : 0); ?> </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="d-flex align-items-center justify-content-center">
              <div class="highlight-icon bg-light mr-3">
                <i class="mdi mdi-cube text-success icon-lg"></i>
              </div>
              <div class="wrapper">
                <p class="card-text mb-0"> Life Events </p>
                <div class="fluid-container">
                  <h5 class="card-title mb-0"> <?php echo e($counts->total_life_event ? $counts->total_life_event : 0); ?> </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="d-flex align-items-center justify-content-center">
              <div class="highlight-icon bg-light mr-3">
                <i class="mdi mdi-briefcase-check text-primary icon-lg"></i>
              </div>
              <div class="wrapper">
                <p class="card-text mb-0"> Matches</p>
                <div class="fluid-container">
                  <h5 class="card-title mb-0"> <?php echo e($counts->total_matched ? $counts->total_matched : 0); ?> </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <section class="panel">
              <div class="panel-body text-center" id="line">
                <canvas  height="300" width="450"></canvas>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- content-wrapper ends -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['line']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Day');
    data.addColumn('number', 'Users');
    data.addColumn('number', 'Life Events');
    data.addColumn('number', 'Matches');
    data.addColumn('number', 'Connections');

    data.addRows([
<?php if($graph_counts): ?>
  <?php $__currentLoopData = $graph_counts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $graph): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      ['<?php echo e(date("d/m",strtotime($graph["date"]))); ?>', <?php echo e($graph["user_count"]); ?>, <?php echo e($graph["event_count"]); ?>, <?php echo e($graph["match_count"]); ?>,<?php echo e($graph["connection_count"]); ?>],
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
    ]);
    var options = {
      chart: {
        title: 'Last 10 Days Activity',
        subtitle: ''
      },
      axes: {
        x: {
          0: {side: 'top'}
        }
      },
    };

    var chart = new google.charts.Line(document.getElementById('line'));

    chart.draw(data, google.charts.Line.convertOptions(options));
  }
</script>
<?php echo $__env->make('admin.inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/dashboard.blade.php ENDPATH**/ ?>