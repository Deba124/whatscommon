<?php $__env->startSection('title', 'Donations'); ?>
<?php echo $__env->make('admin.inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('admin.inc.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Donations</h4>
      <div class="row" style="margin-bottom: 15px;">
        <div class="col-9"></div>
        <meta name="_token" content="<?php echo e(csrf_token()); ?>">
        <div class="col-3 text-right">
          
        </div>
      </div>
      <div class="row">
  
        <div class="col-12">
          <table id="donations-list" class="table table-condensed table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>User </th>
                <th>Amount</th>
                <th>Payment Status</th>
                <th>Bank</th>
                <th>Date</th>
                
              </tr>
            </thead>
            <tbody>
<?php if($donations): ?>
  <?php
    $i = 1
  ?>
  <?php $__currentLoopData = $donations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $donation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
              <td><?php echo e($i++); ?></td>
              <td><?php echo e($donation->firstName); ?> <?php echo e($donation->lastName); ?></td>
              <td><?php echo e($donation->currency_shortname); ?> <?php echo e($donation->donation_amount); ?></td>
              <td>
                <?php if($donation->donation_payment_status=='pending'): ?>
                  <span class="badge badge-warning">pending</span>
                <?php elseif($donation->donation_payment_status=='successful'): ?>
                  <span class="badge badge-success">Successful</span>
                <?php else: ?>
                  <span class="badge badge-danger">Failed</span>
                <?php endif; ?>
              </td>
              <td><?php echo e($donation->bank_name); ?></td>
              <td><?php echo e(format_date($donation->created_at)); ?></td>
            </tr>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php else: ?>
    <tr><td colspan="6" class="text-center"><b>No user queries Available</b></td></tr>
<?php endif; ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $__env->make('admin.inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!-- content-wrapper ends -->
<script type="text/javascript">
$(document).ready(function() {  

  dataTable = $('#donations-list').DataTable( {
    dom: 'lBfrtip',
    responsive: true,
    ordering : true,
    lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
    pageLength: 10,
    buttons: [
      {
          extend: 'print',
          text: '<button class="btn btn-primary btn-sm" ><i class="fa fa-print fa-x5"> Print</i></button>',
          autoPrint: false,
          exportOptions: {
              columns: [ 0,1,2,3,4,5 ]
          }
      },
      {
          extend: 'excelHtml5',
          text: '<button class="btn btn-success btn-sm"><i class="fa fa-file-excel-o fa-x5"> Excel</i></button>',
          exportOptions: {
              columns: [ 0,1,2,3,4,5 ]
          }
      },
      {
          extend: 'pdfHtml5',
          text: '<button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o fa-x5"> PDF</i></button>',
          exportOptions: {
              columns:[ 0,1,2,3,4,5 ]
          }
      }
    ],
    "processing": true,
    "serverSide": false,
  });
}); 
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/user_donations.blade.php ENDPATH**/ ?>