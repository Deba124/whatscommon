 <!-- Modal starts -->       
<div class="modal fade" id="add-military-rank-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Add Military Rank</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="add-military-rank">
          <div class="form-group" id="submit_status"></div>
          <?php echo csrf_field(); ?>
          <div class="form-group">
            <label>Military Branch <span class="text-danger">*</span></label>
            <select class="form-control" name="rank_branch_id">
              <option value="" hidden="">Select Military Branch</option>
<?php if($military_branches): ?>

  <?php $__currentLoopData = $military_branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($branch->_mbranch_id); ?>"><?php echo e($branch->mbranch_name); ?></option>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
            </select>
            <p class="text-danger" id="rank_branch_id_error"></p>
          </div>
          <div class="form-group">
            <label>Rank Name <span class="text-danger">*</span></label>
            <input type="text" name="rank_name" class="form-control">
            <p class="text-danger" id="rank_name_error"></p>
          </div>
          
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends --><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/admin/xhr/add_military_rank_modal.blade.php ENDPATH**/ ?>