 <!-- Modal starts -->       
<div class="modal fade" id="add-master-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
<?php
$name = ucwords(str_replace("_"," ","$db_name"));
$label = ucwords(str_replace("_"," ","$column_name"));
?>
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3">Add <?php echo e($name); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal xhr_form" id="add-master" action="api/add-master">
          <?php echo csrf_field(); ?>
          <div class="form-group whatForm">
            <label>Enter <?php echo e($label); ?> <span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="column_value" style="border:1px solid #f0f0f0;background-color:#fff !important;text-align: center;background: none; ">
            <p class="text-danger" id="column_value_error"></p>
            <input type="hidden" name="user_id" value="<?php echo e($user_id); ?>">
            <input type="hidden" name="db_name" value="<?php echo e($db_name); ?>">
            <input type="hidden" name="column_name" value="<?php echo e($column_name); ?>">
            <input type="hidden" name="dependent_name" value="<?php echo e($dependent_name); ?>">
            <input type="hidden" name="dependent_value" value="<?php echo e($dependent_value); ?>">
            <input type="hidden" name="status_column" value="<?php echo e($status_column); ?>">
            <input type="hidden" name="is_dependent" value="<?php echo e($is_dependent); ?>">
          </div>
          
          <div class="form-group whatForm text-center">
            <button type="submit" class="btn btn-success" style="width: 125px;"> Submit</button>
            <button type="button" class="btn btn-info" data-dismiss="modal" style="width: 125px;margin-left: 15px;">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends --><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/xhr/add_master_modal.blade.php ENDPATH**/ ?>