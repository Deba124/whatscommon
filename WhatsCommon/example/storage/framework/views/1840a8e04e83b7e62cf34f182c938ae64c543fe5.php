</div>
    </div>
  </section>




  <!-- Vendor JS Files -->
  <script src="<?php echo e(url('new-design/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>
  <script src="<?php echo e(url('new-design/assets/vendor/jquery.easing/jquery.easing.min.js')); ?>"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="<?php echo e(url('new-design/js/rangeslider.js')); ?>"></script>
  <script src="<?php echo e(url('new-design/assets/vendor/picker/js/picker.js')); ?>"></script>
  <script src="<?php echo e(url('js/toastr.min.js')); ?>"></script>
  <script src="<?php echo e(url('js/jquery.magnify.js')); ?>"></script>

  <script>
    $(document).ready(function(){
      //section height depends on screen resolution
      $('select').change(function(){
        $(this).addClass('color-bloack');
      });
      if ($(window).width() < 951) {
    //   $('.windowHeightLeft').css('max-height',$(window).height()-30);
    //   $('.windowHeightMid').css('max-height',$(window).height()-30);
    //   $('.windowHeightRight').css('max-height',$(window).height()-30);
    //   $('.windowHeightLeft').css('height',$(window).height()-30);
    //   $('.windowHeightMid').css('height',$(window).height()-30);
    //   $('.windowHeightRight').css('height',$(window).height()-30);
        $('.windowHeightLeft').css('height',$(window).height()-130);
        $('.windowHeightMid').css('height',$(window).height()-130);
      }
      else {
        // $('.windowHeightLeft').css('max-height',$(window).height()-110);
        // $('.windowHeightMid').css('max-height',$(window).height()-110);
        // $('.windowHeightRight').css('max-height',$(window).height()-110);
        // $('.windowHeightLeft').css('height',$(window).height()-110);
        // $('.windowHeightMid').css('height',$(window).height()-110);
        // $('.windowHeightRight').css('height',$(window).height()-110);
        $('.windowHeightLeft').css('height',$(window).height()-180);
        $('.windowHeightMid').css('height',$(window).height()-180);
      }

      $(".leftSlidePan .panelslideOpenButton").click(function(e) {
        e.preventDefault();
        $(".leftSlidePan").toggleClass("closePan");
        $(this).toggleClass("panelslideCloseButton");
        $(".panelslideOpenButton").removeAttr('style');
        $(this).css('z-index', 999);
        //alert('k');
      });
      $(".rightSlidePan .panelslideOpenButton").click(function(e) {
        e.preventDefault();
        $(".rightSlidePan").toggleClass("closePan");
        $(this).toggleClass("panelslideCloseButton");
        $(".panelslideOpenButton").removeAttr('style');
        $(this).css('z-index', 999);
        /*alert('k');*/
      });

      $(".feedDismissedUndo").click(function(){
        var feed_id = $(this).data('feed_id');
        $("#feedShown-"+feed_id).slideDown("fast");
        $("#feedDismissed-"+feed_id).removeClass("showDismissedUndo");
      });
      $(".dismissButton").click(function(){
        var feed_id = $(this).data('feed_id');
        console.log('feed_id=>'+feed_id);
        $("#feedShown-"+feed_id).slideUp("fast");
        $("#feedDismissed-"+feed_id).addClass("showDismissedUndo");
      });
      $(".notificationsOn").click(function(){
        $(".NotificationsAll").toggleClass("eventsNotificationOff");
      });


      $('input[type=range]').rangeslider({
          polyfill : false
      });

      /*$(".myReverseLookup").click(function(){
        $(".mainCenterOpen").addClass("removeMainCenter");
        $(".reverseLookupOnly").removeClass("removeMainCenter");
      });
      $(".reverseLookupBack").click(function(){
        $(".reverseLookupOnly").addClass("removeMainCenter");
        $(".mainCenterOpen").removeClass("removeMainCenter");
      });*/

      $(".myReverseLookup").click(function(){
        $(".mainCenterOpen").addClass("removeMainCenter");
        $(".reverseLookupOnlyOne").removeClass("removeMainCenter");
      });
      $(".reverseLookupBackOne").click(function(){
        $(".reverseLookupOnlyOne").addClass("removeMainCenter");
        $(".mainCenterOpen").removeClass("removeMainCenter");
      });

      $(".searchReverseLookup").click(function(){
        $(".reverseLookupOnlyOne").addClass("removeMainCenter");
        $(".reverseLookupOnlyTwo").removeClass("removeMainCenter");
      });
      $(".reverseLookupBackTwo").click(function(){
        $(".reverseLookupOnlyTwo").addClass("removeMainCenter");
        $(".reverseLookupOnlyOne").removeClass("removeMainCenter");
      });
      
      $(".myLostFound").click(function(){
        $(".mainCenterOpen").addClass("removeMainCenter");
        $(".lostFoundOnly").removeClass("removeMainCenter");
      });
      $(".lostFoundBack").click(function(){
        $(".lostFoundOnly").addClass("removeMainCenter");
        $(".mainCenterOpen").removeClass("removeMainCenter");
      });

      /*$(".myDoppelganger").click(function(){
        $(".mainCenterOpen").addClass("removeMainCenter");
        $(".doppelgangerOnly").removeClass("doppelgangerClose");
      });
      $(".doppelgangerBack").click(function(){
        $(".doppelgangerOnly").addClass("doppelgangerClose");
        $(".mainCenterOpen").removeClass("removeMainCenter");
      });*/
      $(".activeFilterOn").click(function(){
        $(".myFilterAll").toggleClass("eventsFilterOff");
      });
      $(".eventsOnly").click(function(){
        $(this).toggleClass("activeEvents");
      });
      $(".datepicker").datepicker({
        format: "mm-dd-yyyy",
        autoclose: true,
      });
      $('#ex-multiselect').picker();
      $('[data-magnify]').magnify({
        headerToolbar: [
          /*'minimize',
          'maximize',*/
          'close'
        ],
        footerToolbar: [
          'prev',
          'next',
          /*'zoomIn',
          'zoomOut',
          'fullscreen',
          'actualSize',
          'rotateLeft',
          'rotateRight',
          'myCustomButton'*/
        ],
        modalWidth: 800,
        modalHeight: 800,
      });

      $(document).on('change','#items',function(){
  var sizes = $(this).find(':selected').data('sizes');
  console.log('sizes => ');
  console.log(sizes);
  var item_sizes = '<option value="" hidden>Select </option>';
  if(sizes){

    $.each( sizes, function( index, size ) {
      item_sizes += '<option value="'+size.is_name+'">'+size.is_name+'</option>';
    });
  }

  $('#sizes').html(item_sizes);
});
      getMessageRequests();
    });
  </script>
<script>
    // $(document).ready(function(){
    //   $('#login').css('minHeight',$(window).height());
    // });

    $(document).ready(function(){
      //section height depends on screen resolution

      $(".addOtherBank").click(function(){
        $(".financialDetailsOpen").addClass("removeBank");
        $(".financialDetailsClose").removeClass("removeBank");
      });
      $(".closeOtherBank").click(function(){
        $(".financialDetailsClose").addClass("removeBank");
        $(".financialDetailsOpen").removeClass("removeBank");
      });

      toastr.options = {
        "closeButton"       : false,
        "debug"             : false,
        "newestOnTop"       : true,
        "progressBar"       : true,
        "positionClass"     : "toast-bottom-right",
        "preventDuplicates" : false,
        "onclick"           : null,
        "showDuration"      : "300",
        "hideDuration"      : "1000",
        "timeOut"           : "5000",
        "extendedTimeOut"   : "1000",
        "showEasing"        : "swing",
        "hideEasing"        : "linear",
        "showMethod"        : "fadeIn",
        "hideMethod"        : "fadeOut"
      };
    });
  </script>
<?php echo $__env->make('inc.script', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script>

$(document).on('click', '.search_type', function(){
  var search_type = '';

  $('.search_type').each(function(){
    $(this).removeClass('activeEvents');
  });
  if($(this).attr('id')=='search_type_1'){
    search_type='life_event';
    $(this).addClass('activeEvents');
  }
  if($(this).attr('id')=='search_type_2'){
    search_type='global_feed';
    $(this).addClass('activeEvents');
  }
  console.log(search_type);
  $('#search_type').val(search_type);
});

$(document).on('click', '.life_event_type', function(){
  var life_event_types = [];
  console.log('clicked');
  console.log('life_event_types => '+life_event_types);
  $(".life_event_type").each(function() {

    if($(this).hasClass('activeEvents')){
      console.log('has class');
      if($(this).attr('id')=='life_event_type_all'){
        life_event_types = ['1','2','3','4','5','6','7','8','9','10']
      }else{
        if($(this).attr('id')=='life_event_type_1'){
          life_event_types.push('1');
        }
        if($(this).attr('id')=='life_event_type_2'){
          life_event_types.push('2');
        }
        if($(this).attr('id')=='life_event_type_3'){
          life_event_types.push('3');
        }
        if($(this).attr('id')=='life_event_type_4'){
          life_event_types.push('4');
        }
        if($(this).attr('id')=='life_event_type_5'){
          life_event_types.push('5');
        }
        if($(this).attr('id')=='life_event_type_6'){
          life_event_types.push('6');
        }
        if($(this).attr('id')=='life_event_type_7'){
          life_event_types.push('7');
        }
        if($(this).attr('id')=='life_event_type_8'){
          life_event_types.push('8');
        }
        if($(this).attr('id')=='life_event_type_9'){
          life_event_types.push('9');
        }
      }
      
    }
  });
  life_event_type = life_event_types.join(',');
  console.log(life_event_type);
  $('#life_event_type').val(life_event_type);
});
$(document).on('click', '.search_category', function(){
  var search_categories = [];
  console.log('clicked');
  console.log('search_categories => '+search_categories);
  $(".search_category").each(function() {

    if($(this).hasClass('activeEvents')){
      console.log('has class');
      if($(this).attr('id')=='search_category_all'){
        search_categories = ['what','where','when']
      }else{
        if($(this).attr('id')=='search_category_what'){
          search_categories.push('what');
        }
        if($(this).attr('id')=='search_category_where'){
          search_categories.push('where');
        }
        if($(this).attr('id')=='search_category_when'){
          search_categories.push('when');
        }
      }
    }
  });
  search_category = search_categories.join(',');
  console.log(search_category);
  $('#search_category').val(search_category);
});

function searchFeed(search=true,type=''){
  var req = '';
  var user_id = '<?php echo e(Session::get("userdata")["id"]); ?>';
  if(!$(".rightSlidePan").hasClass('closePan')){
    $(".rightSlidePan").addClass("closePan");
  }else{
    $(".rightSlidePan").removeClass("closePan");
  }
  if(!$(".rightSlidePan .panelslideOpenButton").hasClass('panelslideCloseButton')){
    $(".rightSlidePan .panelslideOpenButton").addClass("panelslideCloseButton");
  }else{
    $(".rightSlidePan .panelslideOpenButton").removeClass("panelslideCloseButton");
  }
  $(".rightSlidePan .panelslideOpenButton").css('z-index', 999);
  /*console.log('user_id=>'+user_id);*/
  if(search){
    var keywords = $('#keywords').val();
    var search_type = $('#search_type').val();
    var search_query = $('#search_query').val();
    var life_event_type = $('#life_event_type').val();
    var search_category = $('#search_category').val();
    req = 'user_id='+user_id+'&keywords='+keywords+'&search_query='+search_query+'&life_event_type='+life_event_type+'&search_category='+search_category+'&search_from=web&search_type='+search_type;
    var feed_title = 'Search Feed';
  }else{
    req = 'user_id='+user_id+'&life_event_type='+type+'&search_from=web&search_type=life_event';
    var feed_title = '';
    if(type==1){
      feed_title = 'Personal Feed';
    }
    if(type==2){
      feed_title = 'Dating Feed';
    }
    if(type==3){
      feed_title = 'Adoption Feed';
    }
    if(type==4){
      feed_title = 'Travel Feed';
    }
    if(type==5){
      feed_title = 'Military Feed';
    }
    if(type==6){
      feed_title = 'Education Feed';
    }
    if(type==7){
      feed_title = 'Career Feed';
    }
    if(type==8){
      feed_title = 'Pets Feed';
    }
    if(type==9){
      feed_title = 'Lost & Found Feed';
    }
    if(type==10){
      feed_title = 'Doppelg채nger Feed';
    }
  }
  $('#loading').show();
  var token = $('input[name="_token"]').attr('value');
  console.log(token);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token
    }
  });
  $.ajax({
    type : "POST",
    data : req,
    url  : site_url('api/feeds'),
    success : function(response){
      console.log(response);
      $('#loading').hide();
      if(response.response_code==200){
        $('#responseDiv').show();
        toastr.success(response.response_msg);
        $('#responseDiv').html('<p class="text-success">'+response.response_msg+'</p>');
        /*$(location).attr("href", site_url());*/
        /*if(response.reponse_body.feeds){
          $('#feed_title').html(feed_title);
          var feeds_div = '';
          $.each(response.reponse_body.feeds, function(key, value){
            
            feeds_div += '';
          });
        }*/
        if(response.reponse_body.feed_div){
          $('#feed_div').html(response.reponse_body.feed_div);
          $('#feed_title').html(feed_title);
        }
        if (response.redirect && response.time) {
          window.setTimeout(function(){window.location.href = response.redirect;}, response.time);
        }
        else if(response.redirect){
          $(location).attr("href", response.redirect); 
        }
        else if(response.time){
          window.setTimeout(function(){location.reload();}, response.time);
        }
      }
    },error: function(jqXHR, textStatus, errorThrown){
      /*console.log(textStatus + ": " + jqXHR.status + " " + errorThrown);*/
      /*console.log(jqXHR);*/
      $('#loading').hide();
      console.log(jqXHR.responseJSON);
      toastr.error(jqXHR.responseJSON.response_msg);
      $('#'+submit_id+' button[type="submit"]').attr('disabled',false);
      $('#'+submit_id+' button[type="submit"]').html(submit_btn_content);
      if(jqXHR.responseJSON.errors){
        var err = jqXHR.responseJSON.errors;
        $.each(err, function(key, value){

          if (err[key]) {
            $("#"+key+"_error").html(err[key]);
          }
          else{
            $("#"+key+"_error").html(''); 
          }
        });
      }
    }
  });
}
function makeDefault(form_id){
  $('#'+form_id+' input[name=is_default]').val(1);
  /*$('#'+form_id).submit();*/
}
function getMessageCounts(){
  var token = $('input[name="_token"]').attr('value');
  /*console.log(token);*/
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token
    }
  });
  $.ajax({
    type : "GET",
    url  : site_url('messages/get-message-counts'),
    success : function(response){
      /*console.log(response);*/
      if(response.response_code==200){
        $('#msgCount').html(response.reponse_body.msgCount);
      }
    },error: function(jqXHR, textStatus, errorThrown){
      console.log(jqXHR.responseJSON);
      toastr.error(jqXHR.responseJSON.response_msg);
    }
  });
}

function getMessageRequests(){
  var token = $('input[name="_token"]').attr('value');
  /*console.log(token);*/
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token
    }
  });
  $.ajax({
    type : "POST",
    url  : site_url('messages/get-message-requests'),
    success : function(response){
      /*console.log(response);*/
      if(response.response_code==200){
        /*$('#msgCount').html(response.reponse_body.msgCount);*/
        $('#msgreqDiv').html(response.reponse_body.dialogsList);
      }
    },error: function(jqXHR, textStatus, errorThrown){
      console.log(jqXHR.responseJSON);
      toastr.error(jqXHR.responseJSON.response_msg);
    }
  });
}
</script>

<!-- <script src="https://unpkg.com/quickblox@2.13.0/quickblox.min.js"></script>-->
<?php if(!Request::is('messages')): ?>
<script type="text/javascript">
  setInterval(function(){ getMessageCounts(); }, 6000);
/*var QB = require('node_modules/quickblox/quickblox.min');
var APPLICATION_ID = 92154;
var AUTH_KEY = "CDpYBCU4vYjG3cJ";
var AUTH_SECRET = "YsUX6uGDZJwXbZJ";
var ACCOUNT_KEY = "5CuZCzcpeweYyz1V_6zN";
var CONFIG = { debug: true };

QB.init(APPLICATION_ID, AUTH_KEY, AUTH_SECRET, ACCOUNT_KEY, CONFIG);
console.log(QB);*/
</script>
<?php endif; ?>
</body>

</html><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/inc/footer.blade.php ENDPATH**/ ?>