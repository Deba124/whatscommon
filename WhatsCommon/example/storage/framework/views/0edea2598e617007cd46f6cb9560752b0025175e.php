 <!-- Modal starts -->       
<div class="modal fade" id="coming-soon-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background: #fff;border-bottom: none;">
        <div class="form-group text-center" style="margin-left: 50px;">
          <img src="new-design/img/logo-blurred.png" class="img-fluid">
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="form-group">
          <img src="new-design/img/coming-soon.png" style="width: 100%;" class="img-fluid">
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends --><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/xhr/coming_soon_modal.blade.php ENDPATH**/ ?>