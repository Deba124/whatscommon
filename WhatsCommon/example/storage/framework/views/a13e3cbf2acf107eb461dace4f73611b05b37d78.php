<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                <div class="leftSlidePan <?php echo e(Request::get('menu')==1 ? '' : 'closePan'); ?>">
                  <a href="#" class="panelslideOpenButton <?php echo e(Request::get('menu')==1 ? 'panelslideCloseButton' : ''); ?>"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                  <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                  	<div class="settingLeftTitle">Settings</div>
                  	<ul class="settingMenu mb-0 list-unstyled">
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('settings')); ?>"><img src="new-design/img/profileIcon.png" class="img-fluid menuIcon" alt="">Profile</a>
                  		</li>
                  		<!-- <li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('financial-settings')); ?>"><img src="new-design/img/financial.png" class="img-fluid menuIcon" alt="">Financial Settings</a>
                  		</li> -->
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('change-password')); ?>"><img src="new-design/img/change-password.png" class="img-fluid menuIcon" alt="">Change Password</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu settingMenuActive" href="<?php echo e(url('security')); ?>"><img src="new-design/img/security-a.png" class="img-fluid menuIcon" alt="">Security and Privacy</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('blocked-users')); ?>"><img src="new-design/img/blocking.png" class="img-fluid menuIcon" alt="">Blocking</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('notification-settings')); ?>"><img src="new-design/img/notifications.png" class="img-fluid menuIcon" alt="">Notifications</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('link-accounts')); ?>"><img src="new-design/img/link-account.png" class="img-fluid menuIcon" alt="">Link Account</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="<?php echo e(url('message-settings')); ?>"><img src="new-design/img/message-setting.png" class="img-fluid menuIcon" alt="">Message Settings</a>
                  		</li>
                  	</ul>
                  </div>
                </div>
                <div class="midPan">
                  <div class="connectionsBody windowHeight windowHeightMid securityBg">
                    <div class="bankcardTab">

                      <div class="">
                        <div id="" class="">
                        	<form method="post" class="xhr_form" action="set-security-setting" id="set-security-setting">
                            <?php echo csrf_field(); ?>
                          <div class="profileForm bankForm">
                            <!-- <div class="linkAccountTitle">Options</div>
                            <ul class="linkAccountList">
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        Use Two-Authentication
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" <?php if($settings && $settings->two_authentication==1): ?> checked <?php endif; ?> name="two_authentication" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                            </ul> -->

                            <div class="linkAccountTitle">Sign-In</div>
                            <ul class="linkAccountList">
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        With Touch ID
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" <?php if($settings && $settings->sign_touch_id==1): ?> checked <?php endif; ?> name="sign_touch_id" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        With Face ID
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" <?php if($settings && $settings->sign_face_id==1): ?> checked <?php endif; ?> name="sign_face_id" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                            </ul>

                            <div class="linkAccountTitle">Privacy</div>
                            <div class="feedbackTitle2 text-left">By turning on the toggle buttons, you’re allowing to show specific information on your profile and allowing people to see these information.</div>

                            <ul class="linkAccountList">
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        Email
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" <?php if($settings && $settings->privacy_email==1): ?> checked <?php endif; ?> name="privacy_email" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        Phone Number
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" <?php if($settings && $settings->privacy_phone==1): ?> checked <?php endif; ?> name="privacy_phone" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        Birthday
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" <?php if($settings && $settings->privacy_birthday==1): ?> checked <?php endif; ?> name="privacy_birthday" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        Location Sharing
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" <?php if($settings && $settings->privacy_location==1): ?> checked <?php endif; ?> name="privacy_location" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        Share Connections Globally
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" <?php if($settings && $settings->privacy_share_connection==1): ?> checked <?php endif; ?> name="privacy_share_connection" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                            </ul>
                          </div>
                        </form>
                        </div>
                        
                      </div>

                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
$(document).on('change','.settings', function(){
	$('.xhr_form').submit();
});
</script><?php /**PATH C:\gitfiles\whatscommon\WhatsCommon\example\resources\views/security_settings.blade.php ENDPATH**/ ?>