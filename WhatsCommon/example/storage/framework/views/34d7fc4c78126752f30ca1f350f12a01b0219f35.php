<?php echo $__env->make('inc.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php
$user_id = Session::get('userdata')['id'];
?>
<div class="innerBodyOnly">
  <div class="container-fluid">
    <div class="connectionsBody">
      <div class="position-relative connectionFlip">
        <div class="leftSlidePan <?php echo e(Request::get('user') ? 'closePan' : ''); ?> windowHeightLeft" style="overflow: auto;">
          <a href="#" class="panelslideOpenButton <?php echo e(!Request::get('user') ? 'panelslideCloseButton' : ''); ?>"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
          <div class="connectionsDetailsInfoName connectionTitleResponsive" style="display: block;text-align: center;margin-top: 15px;margin-bottom: 15px;"> <?php echo e($user_data->firstName ? $user_data->firstName."'s" : ""); ?> Connections</div>
          <div class="connectionsLeft leftPan windowHeight ">
            <ul class="nav nav-tabs connectionsTab" role="tablist">
          <?php if($connections): ?>
            <?php 
              //$char = 'a';
              $connect_arr = [];
              foreach($connections as $i => $connection){
                $char = substr(strtolower($connection->firstName),0,1);
                if(isset($connect_arr[$char])){
                  array_push($connect_arr[$char],$connection);
                }else{
                  $connect_arr[$char][0] = $connection;
                }
              }
            ?>
            <?php if($connect_arr && !empty($connect_arr)): ?>

              <?php $i=0; ?>

              <?php $__currentLoopData = $connect_arr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $n_connections): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li class="alphabaticallyDetails">
                  <div class="alphabaticallyConnections"><?php echo e($key); ?></div>      
                </li>
                <?php if(!empty($n_connections)): ?>

                  <?php $__currentLoopData = $n_connections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $j => $connection): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li class="nav-item">
                    <a class="nav-link <?php echo e(((!Request::get('user') && $i==0) || Request::get('user')==$connection->id)? 'active' : ''); ?> openMidPanButton" data-toggle="tab" href="#connection<?php echo e($connection->id); ?>">
                      <div class="connectionsTabDetails">
                        <div class="connectionsAvtarImg">
                          <img src="<?php echo e($connection->profile_photo_path); ?>" class="img-fluid" alt="">
                        </div>
                        <div class="connectionsAvtarDetails">
                          <div class="connectionsAvtarName"><?php echo e($connection->firstName); ?> <?php echo e($connection->lastName); ?></div>
                          <div class="connectionsAvtarId"><?php echo e('@'); ?><?php echo e($connection->username); ?></div>
                          
                        </div>
                        <div class="connectionsAvtarInfo">
                          <!-- <img src="new-design/img/connectionsCall.png" class="img-fluid connectionsCall" alt="">
                          <img src="new-design/img/connectionsMsg.png" class="img-fluid connectionsMsg" alt=""> -->
                        
                        </div>
                      </div>  
                    </a>
                  </li>
                  <?php $i++; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
          <?php else: ?>
            <li class="nav-item">
              <a class="nav-link active openMidPanButton" data-toggle="tab" href="#connection">
                <div class="connectionsTabDetails">
                  <div class="connectionsAvtarDetails">
                    <div class="connectionsAvtarName">No Connection Available</div>
                  </div>
                </div>  
              </a>
            </li>
          <?php endif; ?>
            </ul>
          </div>
        </div>
        <div class="midPan windowHeight windowHeightMid">
          <div class="connectionsRight">
            <div class="tab-content">
          <?php if($connections): ?>
            
              <?php $__currentLoopData = $connections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $connection): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <div id="connection<?php echo e($connection->id); ?>" class="tab-pane <?php echo e(((!Request::get('user') && $i==0) || Request::get('user')==$connection->id) ? 'active' : 'fade'); ?>">
                <div class="connectionsAllDetails">
                  <div class="connectionsAllDetailsTop" style="background: #FFF;padding: 10px 5px;">
                    <div class="connectionsDetailsAvtarImg">
                      <img src="<?php echo e($connection->profile_photo_path); ?>" class="img-fluid" alt="">
                    </div>

                    <div class="connectionsDetailsInfo">
                      <div class="connectionsDetailsInfoName"><a style="color: inherit;" href="<?php echo e($connection->id==$user_id ? url('my-profile') : url('user-profile?user='.$connection->id)); ?>" ><?php echo e($connection->firstName); ?> <?php echo e($connection->lastName); ?></a></div>
                      <div class="connectionsDetailsInfoEvents myEvenntsAll">
                        <div class="myEvennts">
                          <?php if($connection->show_connection_list): ?>
                          <a style="all: unset;cursor: pointer;" href="<?php echo e($connection->id==$user_id ? url('connections') : url('other-connections/?u='.$connection->id)); ?>">
                            <span class="eventsNumbers"> <?php echo e($connection->connection_count); ?>  </span> <span class="eventsText">Connections</span>
                          </a>
                          <?php else: ?>
                          <span class="eventsNumbers"> <?php echo e($connection->connection_count); ?>  </span> <span class="eventsText">Connections</span>
                          <?php endif; ?>
                        </div>
                        <div>
                          <span class="eventsNumbers"><?php echo e($connection->life_events_count); ?></span> <span class="eventsText">Life Events</span>
                        </div>
                        <div>
                          <span class="eventsNumbers"><?php echo e($connection->matched_count); ?></span> <span class="eventsText">Event Matches</span>
                        </div>
                      </div>
                      <div class="connectionsDetailsInfoId"><?php echo e('@'); ?><?php echo e($connection->username); ?></div>
                      <div class="connectionsDetailsPlace"><?php echo e($connection->city_name); ?> <?php echo e(($connection->city_name && $connection->province_name) ? ',' : ''); ?> <?php echo e($connection->province_name); ?> <?php echo e(($connection->country_name && $connection->province_name) ? ',' : ''); ?> <?php echo e($connection->country_name); ?></div>
                      <?php if($connection->bio): ?><div class="connectionsDetailsPlace connectionsDetailsUser"><?php echo e($connection->bio); ?></div><?php endif; ?>
                    <?php if($connection->id!=$user_id): ?>
                      <a class="btn btn-following" onclick="execute('api/follow-unfollow','user_id=<?php echo e($user_id); ?>&followed_user=<?php echo e($connection->id); ?>')"><?php echo e($connection->is_following==1 ? 'Following' : 'Follow'); ?></a>
                      <?php if($connection->quick_blox_id): ?>
                      <a class="btn btn-following btn-message" href="<?php echo e(url('messages?qb='.$connection->quick_blox_id)); ?>">Message</a>
                      <?php endif; ?>
                      <a class="btn btn-following" onclick="execute('api/block-user','user_id=<?php echo e($user_id); ?>&block_user_id=<?php echo e($connection->id); ?>')">Block</a>
                    <?php endif; ?>
                    </div>
                    <?php if($connection->created_at): ?><div class="memberJoin">Member since <?php echo e(format_date($connection->created_at)); ?></div><?php endif; ?>
                  </div>

                  <div class="connectionsAllDetailsBody">
                  <?php if($connection->show_match_feed): ?>
                    <?php if($connection->feeds): ?>
                    
                      <?php $__currentLoopData = $connection->feeds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $feed): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <div class="globalFeedList" style="padding: 25px 10px;border-bottom: 3px solid #fff;">

                      <div class="connectedInfo">
                        <div class="position-relative pr-0 feedImg">
                          <div class=""><img src="<?php echo e($feed->user1_profile_photo ? $feed->user1_profile_photo : 'new-design/img/profile_placeholder.jpg'); ?>" class="img-fluid" alt=""></div>
                          <div class=""><img src="<?php echo e($feed->user2_profile_photo ? $feed->user2_profile_photo : 'new-design/img/profile_placeholder.jpg'); ?>" class="img-fluid" alt=""></div>
                          <img src="new-design/img/feedadd.png" class="feedadd" alt="">
                        </div>
                        <div class="rightPanText">
                          <p>
                            <?php if($feed->user1_id==Session::get('userdata')['id']): ?>
                            <a href="<?php echo e(url('my-profile')); ?>"> 
                              <i>You </i> 
                            </a> are <?php echo e($feed->feed_type); ?> with <a href="<?php echo e(url('user-profile/?feed='.$feed->_feed_id.'&user='.$feed->user2_id)); ?>"> 
                              <i><?php echo e($feed->user2_firstName); ?> <?php echo e($feed->user2_lastName); ?> </i> 
                            </a>

                            <?php elseif($feed->user2_id==Session::get('userdata')['id']): ?>
                            <a href="<?php echo e(url('user-profile/?feed='.$feed->_feed_id.'&user='.$feed->user1_id)); ?>"> 
                              <i><?php echo e($feed->user1_firstName); ?> <?php echo e($feed->user1_lastName); ?> </i> 
                            </a> is <?php echo e($feed->feed_type); ?> with <a href="<?php echo e(url('my-profile')); ?>"> 
                              <i>You </i> 
                            </a>
                            <?php else: ?>
                            <a href="<?php echo e(url('user-profile/?feed='.$feed->_feed_id.'&user='.$feed->user1_id)); ?>"> 
                              <i><?php echo e($feed->user1_firstName); ?> <?php echo e($feed->user1_lastName); ?> </i> 
                            </a> is <?php echo e($feed->feed_type); ?> with
                            <a href="<?php echo e(url('user-profile/?feed='.$feed->_feed_id.'&user='.$feed->user2_id)); ?>"> 
                              <i><?php echo e($feed->user2_firstName); ?> <?php echo e($feed->user2_lastName); ?> </i> 
                            </a>
                            <?php endif; ?>
                              
                            </p>
                          <span>About <?php echo e(DateTime_Diff($feed->feed_created)); ?> ago</span>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="feedInfo position-relative">
                                <?php if($feed->feed_what): ?>
                                <p>
                                    <span>What:</span> 
                                    <span style="color: #91d639;"><?php echo e($feed->feed_event_type); ?></span>
                                </p>
                                <p class="pl-5"> <?php echo e($feed->feed_what); ?></p>
                                <?php endif; ?>
                                <?php if($feed->feed_where): ?><p><span>Where:  </span> <?php echo e($feed->feed_where); ?></p><?php endif; ?>
                                <?php if($feed->feed_when): ?><p><span>When: </span> <?php echo e($feed->feed_when); ?></p><?php endif; ?>
                                <?php if($feed->feed_keywords): ?><p><span>W5: </span> <?php echo e($feed->feed_keywords); ?></p><?php endif; ?>
                                <div class="greenCountFeed"><?php echo e($feed->feed_match_count); ?></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                <?php if($feed->feed_images && count($feed->feed_images)>0): ?>
                <?php

                ?>
                        <?php
                    $counter = count($feed->feed_images);
                    $remaining = 0;
                    if($counter>3){
                      $remaining = $counter-2;
                    }
                    ?>
                        <div class="uploadPhotoHeading">Uploaded photos:</div>
                        <div class="galleryViewAll">
                                  <?php if($counter==1): ?>
                                    <div class="galleryViewImg1">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-12 galleryViewCol">
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[0]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php elseif($counter==2): ?>  
                                    <div class="galleryViewImg2">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[0]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[1]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php elseif($counter==3): ?>     
                                    <div class="galleryViewImg3">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[0]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[1]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                          <div class="galleryViewImg">
                                            
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[2]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[2]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[2]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php else: ?>
                                    <div class="galleryViewImg3 galleryViewImg4">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[0]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[0]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              
                                            <a data-magnify="gallery" data-caption="<?php echo e($feed->feed_images[1]->fi_img_name); ?>" href="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" data-group="<?php echo e($feed->_feed_id); ?>">
                                              <img src="<?php echo e($feed->feed_images[1]->fi_img_url); ?>" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                          <div class="galleryViewImg">
                                            <img src="new-design/img/feedfoto01.png" class="img-fluid" alt="">
                                            <div class="galleryViewImgMore"><?php echo e($remaining); ?>+</div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php endif; ?>
                                  </div>

                        
                <?php endif; ?>
                        
                        </div>
                        <div class="col-md-12">
                        <div class="row text-center rightPanBtn" style="display: block;">
                            <a href="#" class="dismissButton" onclick="execute('api/hide-feed','user_id=<?php echo e(Session::get('userdata')['id']); ?>&_feed_id=<?php echo e($feed->_feed_id); ?>&request_from=my-profile');"><img src="new-design/img/dismissIcon.png" class="img-fluid" alt=""> Dismiss</a>
                            <!-- <a href="#" class="tagstarButton"> Tagster Social</a> -->
                        </div>
                      </div>
                      </div>
                        <div class="row">
                        

                      </div>

                        <!-- <img src="new-design/img/mapfeed.png" class="img-fluid" alt="" width="100%"> -->
                  

                    </div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                  <?php endif; ?>

                  </div>
                </div>
              </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          <?php endif; ?>
              
            </div>    
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $__env->make('inc.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="text/javascript">
$(document).ready(function(){
  $('.openMidPanButton').click(function(e){
    e.preventDefault();
    if(!$(".leftSlidePan").hasClass('closePan')){
      $(".leftSlidePan").addClass("closePan");
    }
    if(!$(".leftSlidePan .panelslideOpenButton").hasClass('panelslideCloseButton')){
      $(".leftSlidePan .panelslideOpenButton").addClass("panelslideCloseButton");
    }else{
      $(".leftSlidePan .panelslideOpenButton").removeClass("panelslideCloseButton");
    }
    /*$(".panelslideOpenButton").removeAttr('style');*/
    $(".leftSlidePan .panelslideOpenButton").css('z-index', 999);
  });
});
    $(document).ready(function(){    
        if ($(window).width() < 951) {
        $('.windowHeightLeft').css('height',$(window).height()-153);
        $('.windowHeightMid').css('height',$(window).height()-90);
      }
      else {
        $('.windowHeightLeft').css('height',$(window).height()-150);
        $('.windowHeightMid').css('height',$(window).height()-150);
      }
    });
</script><?php /**PATH D:\WhatsCommon\gitfiles\whatscommon\WhatsCommon\example\resources\views/other_connections.blade.php ENDPATH**/ ?>