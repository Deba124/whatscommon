@include('inc.header')
<div class="innerBodyOnly">
<div class="container-fluid">
<div class="settingBody">
  <div class="position-relative settingFlip">
    <div class="leftSlidePan {{ Request::get('menu')==1 ? '' : 'closePan'}}">
                  <a href="#" class="panelslideOpenButton {{ Request::get('menu')==1 ? 'panelslideCloseButton' : ''}}"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
      <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
      	<div class="settingLeftTitle">Settings</div>
      	<ul class="settingMenu mb-0 list-unstyled">
      		<li class="settingMenuLi">
      			<a class="settingMenu" href="{{ url('settings') }}"><img src="new-design/img/profileIcon.png" class="img-fluid menuIcon" alt="">Profile</a>
      		</li>
      		<li class="settingMenuLi">
      			<a class="settingMenu" href="{{ url('change-password') }}"><img src="new-design/img/change-password.png" class="img-fluid menuIcon" alt="">Change Password</a>
      		</li>
      		<li class="settingMenuLi">
      			<a class="settingMenu" href="{{ url('security') }}"><img src="new-design/img/security.png" class="img-fluid menuIcon" alt="">Security and Privacy</a>
      		</li>
      		<li class="settingMenuLi">
      			<a class="settingMenu" href="{{ url('blocked-users') }}"><img src="new-design/img/blocking.png" class="img-fluid menuIcon" alt="">Blocking</a>
      		</li>
      		<li class="settingMenuLi">
      			<a class="settingMenu" href="{{ url('notification-settings') }}"><img src="new-design/img/notifications.png" class="img-fluid menuIcon" alt="">Notifications</a>
      		</li>
      		<li class="settingMenuLi">
      			<a class="settingMenu" href="{{ url('link-accounts') }}"><img src="new-design/img/link-account.png" class="img-fluid menuIcon" alt="">Link Account</a>
      		</li>
      		<li class="settingMenuLi">
      			<a class="settingMenu settingMenuActive" href="{{ url('message-settings') }}"><img src="new-design/img/message-setting-a.png" class="img-fluid menuIcon" alt="">Message Settings</a>
      		</li>
          <li class="settingMenuLi">
            <a class="settingMenu" href="{{ url('feed-settings') }}"><img src="new-design/img/global-feed.png" class="img-fluid menuIcon" alt="">Feed Settings</a>
          </li>
      	</ul>
      </div>
    </div>
    <div class="midPan">
      <div class="connectionsBody windowHeight windowHeightMid msgSettingBg">
        <div class="">
          
            

          <div class="">
            <div id="" class="">
            	<form method="post" class="xhr_form" action="set-message-setting" id="set-message-setting">
                    @csrf
              <div class="bankDetailsTitle">Message Settings</div>
              <div class="profileForm bankForm">
                <div class="linkAccountTitle"><img src="new-design/img/notiMsgSetting.png" class="img-fluid msgSettingIcon" alt="">Notifications</div>
                <ul class="linkAccountList">
                    <li>
                        <span style="width: calc(100% - 48px);">
                            Allow Notifications
                        </span>
                        <label class="switch">
                            <input type="checkbox" @if($settings && $settings->allow_notification==1) checked @endif name="allow_notification" class="settings" value="1">
                            <span class="slider round"></span>
                        </label>
                    </li>
                </ul>

                <div class="linkAccountTitle"><img src="new-design/img/notiMsgSetting2.png" class="img-fluid msgSettingIcon" alt="">Media</div>
                <ul class="linkAccountList">
                    <li>
                        <span style="width: calc(100% - 48px);">
                            Save Photo and Videos
                        </span>
                        <label class="switch">
                            <input type="checkbox" @if($settings && $settings->save_media==1) checked @endif name="save_media" class="settings" value="1">
                            <span class="slider round"></span>
                        </label>
                    </li>
                </ul>

              <div class="feedbackTitle2">Automatically save photos and videos on your camera roll.</div>
              </div>
          </form>
            </div>
            
          </div>

        </div>
        
      </div>
    </div>
  </div>
</div>
</div>
</div>
@include('inc.footer')
<script type="text/javascript">
$(document).on('change','.settings', function(){
	$('.xhr_form').submit();
});
</script>