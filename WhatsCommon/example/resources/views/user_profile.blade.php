@include('inc.header')
<div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="connectionsBody">
              <div class="position-relative connectionFlip userDetails">
                
                <!--closePan-->
                <div class="midPan windowHeight windowHeightMid">
                  <div class="connectionsRight">
                    <div class="connectionsAllDetails">
                          <div class="connectionsAllDetailsTop position-relative">
                            <div class="connectionsDetailsAvtarImg">
                              <img src="{{ $connection->profile_photo_path ? $connection->profile_photo_path : 'new-design/img/profile_placeholder.jpg' }}" class="img-fluid" alt="">
                            </div>

                            <div class="connectionsDetailsInfo">
                              <div class="connectionsDetailsInfoName">{{ $connection->firstName }} {{ $connection->lastName }}</div>
                              <div class="connectionsDetailsInfoEvents myEvenntsAll">
                                <div class="myEvennts">
                                  @if($connection->show_connection_list)
                          <a style="all: unset;cursor: pointer;" href="{{ $connection->id==$user_id ? url('connections') : url('other-connections/?u='.$connection->id) }}">
                            <span class="eventsNumbers"> {{ $connection->connection_count }}  </span> <span class="eventsText">Connections</span>
                          </a>
                          @else
                          <span class="eventsNumbers"> {{ $connection->connection_count }}  </span> <span class="eventsText">Connections</span>
                          @endif
                                </div>
                                <div>
                                  <span class="eventsNumbers">{{ $connection->life_events_count }}</span> <span class="eventsText">Life Events</span>
                                </div>
                                <div>
                                  <span class="eventsNumbers">{{ $connection->matched_count }}</span> <span class="eventsText">Matches</span>
                                </div>
                              </div>
                              <div class="connectionsDetailsInfoId">{{'@'}}{{ $connection->username }}</div>
                              <div class="connectionsDetailsPlace">{{ $connection->city_name }} {{ ($connection->city_name && $connection->province_name) ? ',' : '' }} {{ $connection->province_name }} {{ ($connection->country_name && $connection->province_name) ? ',' : '' }} {{ $connection->country_name }}</div>
                              
                              @if($connection->bio)<div class="connectionsDetailsPlace connectionsDetailsUser">{{ $connection->bio }}</div>@endif
                              @if($connection->is_connected==0 && $connection->is_pending_connection==0 && $feed_data)
                      <a class="btn btn-following" onclick="execute('api/connection-request','user_id={{$my_id}}&connect_user_id={{$connection->id}}&_feed_id={{$feed_data->_feed_id}}')">OK to Connect</a>
                      @elseif($connection->is_pending_connection==1 && $connection->request_from_user==$my_id && $connection->_request_id)
                      <a class="btn btn-following" onclick="execute('api/cancel-request','user_id={{$my_id}}&_request_id={{$connection->_request_id}}')">Cancel Connect</a>
                      @elseif($connection->is_pending_connection==1 && $connection->request_from_user!=$my_id && $connection->_request_id)
                      <a class="btn btn-following" onclick="execute('api/accept-reject-request','user_id={{$my_id}}&_request_id={{$connection->_request_id}}&request_status=1')">Accept Connect</a>
                      @endif
                      <a class="btn btn-following" onclick="execute('api/follow-unfollow','user_id={{$my_id}}&followed_user={{$connection->id}}')">{{ $connection->is_following==1 ? 'Following' : 'Follow' }}</a>
                      @if($connection->quick_blox_id)
                      <a class="btn btn-following btn-message" href="{{ url('messages?qb='.$connection->quick_blox_id) }}">Message</a>
                      @endif
                            </div>
                            @if($connection->created_at)<div class="memberJoin">Member since {{ format_date($connection->created_at) }}</div>@endif
                          </div>
                      @if($connection->show_match_feed && count($feeds)>0)
                          <div class="connectionsAllDetailsBody">
                            <div class="connectionsDetailsTitle text-center">{{ $connection->firstName }}’s Personal Feed</div>
                        @foreach($feeds as $feed)
                            <div class="userConnectedInfo">
                                <div class="connectedInfo">
                                    <div class="position-relative pr-0 feedImg">
                                        <div class=""><img src="{{ $feed->user1_profile_photo ? $feed->user1_profile_photo : 'new-design/img/profile_placeholder.jpg' }}" class="img-fluid" alt=""></div>
                                        <div class=""><img src="{{ $feed->user2_profile_photo ? $feed->user2_profile_photo : 'new-design/img/profile_placeholder.jpg' }}" class="img-fluid" alt=""></div>
                                        <img src="new-design/img/feedadd2.png" class="feedadd" alt="">
                                    </div>
                                    <div class="rightPanText">
                                      <p>
                                      @if($feed->user1_id==Session::get('userdata')['id'])
                                        <a href="{{ url('my-profile') }}"> 
                                          <i>You </i> 
                                        </a> are {{ $feed->feed_type }} with <a href="{{ url('user-profile/?user='.$feed->user2_id) }}"> 
                                          <i>{{ $feed->user2_firstName }} {{ $feed->user2_lastName }} </i> 
                                        </a>

                                      @elseif($feed->user2_id==Session::get('userdata')['id'])
                                        <a href="{{ url('user-profile/?user='.$feed->user1_id) }}"> 
                                          <i>{{ $feed->user1_firstName }} {{ $feed->user1_lastName }} </i> 
                                        </a> is {{ $feed->feed_type }} with <a href="{{ url('my-profile') }}"> 
                                          <i>You </i> 
                                        </a>

                                      @else
                                        <a href="{{ url('user-profile/?user='.$feed->user1_id) }}"> 
                                          <i>{{ $feed->user1_firstName }} {{ $feed->user1_lastName }} </i> 
                                        </a> is {{ $feed->feed_type }} with <a href="{{ url('user-profile/?user='.$feed->user2_id) }}"> 
                                          <i>{{ $feed->user2_firstName }} {{ $feed->user2_lastName }} </i> 
                                        </a>


                                      @endif
                                          
                                      </p>
                                      <span>About {{ DateTime_Diff($feed->feed_created) }} ago</span>
                                    </div>
                                    <div class="greenCountFeed">{{ $feed->feed_match_count }}</div>
                                </div>
                            </div>
                            
                            <div class="userConnectDetailsAll">
                              <div class="userConnectDetails">
                                <div class="userConnectDetailsLeft">
                                @if($feed->feed_what)
                                  <div class="userDetailsText">What: <span style="color: #91d639;">{{$feed->feed_event_type}}</span></div>
                                  <div class="userDetailsText ml-5"><span>{{ $feed->feed_what }}</span></div>
                                  {{-- <div class="userDetailsText ml-5"><span>Airfield</span></div> --}}
                                @endif
    @if($feed->feed_where)<div class="userDetailsText">Where: <span>{{ $feed->feed_where }}</span></div>@endif
    @if($feed->feed_when)<div class="userDetailsText">When: <span>{{ $feed->feed_when }}</span></div>@endif
    @if($feed->feed_keywords)<div class="userDetailsText">W5: <span>{{ $feed->feed_keywords }}</span></div>@endif
                                  {{-- <div class="userDetailsText">Why: <span>Ranger Buddy</span></div> --}}
                                </div>
                                <div class="userConnectDetailsRight">
                                @if($feed->feed_images && count($feed->feed_images)>0)
                                @php
                                $counter = count($feed->feed_images);
                                $remaining = 0;
                                if($counter>3){
                                  $remaining = $counter-2;
                                }
                                @endphp
                                  <div class="uploadPhotoHeading">Uploaded photos:</div>
                                  <div class="galleryViewAll">
                                  @if($counter==1)
                                    <div class="galleryViewImg1">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-12 galleryViewCol">
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto03.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[0]->fi_img_name }}" href="{{ $feed->feed_images[0]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[0]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  @elseif($counter==2)  
                                    <div class="galleryViewImg2">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto03.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[0]->fi_img_name }}" href="{{ $feed->feed_images[0]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[0]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto01.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[1]->fi_img_name }}" href="{{ $feed->feed_images[1]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[1]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  @elseif($counter==3)     
                                    <div class="galleryViewImg3">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              {{-- <img src="img/feedfoto03.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[0]->fi_img_name }}" href="{{ $feed->feed_images[0]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[0]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto02.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[1]->fi_img_name }}" href="{{ $feed->feed_images[1]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[1]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto01.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[2]->fi_img_name }}" href="{{ $feed->feed_images[2]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[2]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  @else
                                    <div class="galleryViewImg3 galleryViewImg4">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              {{-- <img src="img/feedfoto03.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[0]->fi_img_name }}" href="{{ $feed->feed_images[0]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[0]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              {{-- <img src="img/feedfoto02.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[1]->fi_img_name }}" href="{{ $feed->feed_images[1]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[1]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                          <div class="galleryViewImg">
                                            <img src="new-design/img/feedfoto01.png" class="img-fluid" alt="">
                                            <div class="galleryViewImgMore">{{ $remaining }}+</div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  @endif
                                  </div>
                                @endif
                                  <div class="row text-center rightPanBtn">
                                    {{-- <a href="#" class="dismissButton"><img src="img/dismissIcon.png" class="img-fluid" alt=""> Dismiss</a> --}}
                                    <a href="#" class="dismissButton" onclick="execute('api/hide-feed','user_id={{ Session::get('userdata')['id'] }}&_feed_id={{ $feed->_feed_id }}&request_from=my-profile');"><img src="new-design/img/dismissIcon.png" class="img-fluid" alt=""> Dismiss</a>
                                      {{-- <a href="#" class="messageButton"><img src="img/message-white.png" class="img-fluid" alt="">
                                        Message</a> --}}
                                      {{-- <a href="#" class="tagstarButton"> Tagster Social</a> --}}
                                  </div>
                            </div>
                          </div>
                        </div>
                        @endforeach
                      </div>
                    @endif
                    </div>    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@include('inc.footer')
<script type="text/javascript">
    $(document).ready(function(){    
        if ($(window).width() < 951) {
        $('.windowHeightLeft').css('height',$(window).height()-153);
        $('.windowHeightMid').css('height',$(window).height()-90);
      }
      else {
        $('.windowHeightLeft').css('height',$(window).height()-150);
        $('.windowHeightMid').css('height',$(window).height()-150);
      }
    });
</script>