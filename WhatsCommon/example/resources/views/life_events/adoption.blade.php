@include('inc.header')
<div class="innerBodyOnly innerBodyModify">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                  <div class="leftSlidePan closePan">
                    <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                    <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                      @include('inc.life_event_left_menu')
                    </div>
                  </div>
                  <div class="midPan">
                    <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                      <div class="row infoBodyRow">
                        <div class="col-md-5 infoBodyCol connectionsBody eventImgCol">
                          <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
                            <div class="innerHome">
                              <a href="{{url('home')}}"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                            </div>
                            <div class="feedbackImg">
                              <div class="personalImgAll">
                                <img src="new-design/img/adoptionImg.png" class="img-fluid" alt="">
                                <div class="personalImgTitle">Adoption / Foster / Shelter</div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7 connectionsBody infoBodyCol eventCol midHightControl">
                          <div class="connectionsBody feedbackRight windowHeight windowHeightMid">
                            <form class="xhr_form" method="post" action="api/life-event-upload" id="life-event-upload">
                            @csrf
                              <div class="eventColHeading">What </div>
                              <img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                              <input type="hidden" name="type_id" value="3">
                              <input type="hidden" name="user_id" value="{{Session::get('userdata')['id']}}">
                                <div class="whatForm">
                                  <div class="form-group">
                                    <label for="">What</label>
                                    <select class="form-control " name="sub_category_id">
            		@if($what)
                    	@foreach($what as $w)
                                      <option value="{{ $w->id }}">{{ $w->sub_cat_type_name }}</option>
                        @endforeach
                    @endif
                                    </select>
                                  </div>
                                  <div class="whatReasonAll">
                                    <div class="form-group">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="" style="margin: 8px auto;">Biological Mother</label>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group adoptionCheckAll" style="padding: 0;margin: 10px auto;">
                                            <label class="adoptionCheck">I'm the mother
                                              <input type="radio" name="user_is_parent" value="1">
                                              <span class="adoptionCheckmark"></span>
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <input type="text" class="form-control" id="" placeholder="Type here" name="bio_mother">
                                    </div>
                                    
                                    <div class="form-group">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="" style="margin: 8px auto;">Biological Father</label>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group adoptionCheckAll" style="padding: 0;margin: 10px auto;">
                                            <label class="adoptionCheck">I'm the father
                                              <input type="radio" name="user_is_parent" value="2">
                                              <span class="adoptionCheckmark"></span>
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                      <input type="text" class="form-control" id="" placeholder="Type here" name="bio_father">
                                    </div>
                                    
                                    <div class="form-group">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="" style="margin: 8px auto;">Name given at birth</label>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group adoptionCheckAll" style="padding: 0;margin: 10px auto;">
                                            <label class="adoptionCheck">I'm the child
                                              <input type="radio" name="user_is_parent" value="3">
                                              <span class="adoptionCheckmark"></span>
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <input type="text" class="form-control" id="" placeholder="Type here" name="birth_name">
                                    </div>
                                    
                                    <div class="form-group">
                                      <label for="" >Weight</label>
                                      <div class="height_weight weight_adoption">
                                        <div class="height_weight_Only">
                                          <input type="text" class="form-control formWeight" id="weight" placeholder="00" name="weight" value="0" style="margin-left: 0px;">
                                        </div>
                                        <div class="form-group timeOptionSet">
                                          <div class="form-control timeOptionOnly activeTimeOption" id="weight_measure_kg">kg</div>
                                          <div class="form-control timeOptionOnly" id="weight_measure_lbs" style="border-top-left-radius: 0 !important;border-bottom-left-radius: 0 !important;">lbs</div>
                                          <input type="hidden" name="weight_measure" id="weight_measure" value="kg">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="">Length</label>
                                      <div class="height_weight weight_adoption">
                                        <div class="height_weight_Only">
                                          <input type="number" name="" class="form-control formHeight" name="height" id="height" value="0" style="margin-left: 0px;">
                                          
                                          {{-- <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt=""> --}}
                                        </div>
                                        <div class="form-group timeOptionSet">
                                          <div class="form-control timeOptionOnly activeTimeOption" id="height_measure_in">in</div>
                                          <div class="form-control timeOptionOnly" id="height_measure_cm" style="border-top-left-radius: 0 !important;border-bottom-left-radius: 0 !important;">cm</div>
                                          <input type="hidden" name="height_measure" id="height_measure" value="in">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="form-group" id="genders_div">
                                      <label for="">Gender</label>
                                      <select class="form-control formGender selector" name="gender" id="gender">
                                        <option value="" >Select</option>
                    @if($genders)
                      @foreach($genders as $gender)
                              <option option="{{ $gender->gender_name }}">{{$gender->gender_name}}</option>
                      @endforeach
                    @endif
                                        <option value="add">Add</option>
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">Time of Birth</label>
                              <select class="form-control formTime" id="" name="time_of_birth">
                                <option value="">00:00</option>
                                <option value="1:00">1:00</option>
                                <option value="1:30">1:30</option>
                                <option value="2:00">2:00</option>
                                <option value="2:30">2:30</option>
                                <option value="3:00">3:00</option>
                                <option value="3:30">3:30</option>
                                <option value="4:00">4:00</option>
                                <option value="4:30">4:30</option>
                                <option value="5:00">5:00</option>
                                <option value="5:30">5:30</option>
                                <option value="6:00">6:00</option>
                                <option value="6:30">6:30</option>
                                <option value="7:00">7:00</option>
                                <option value="7:30">7:30</option>
                                <option value="8:00">8:00</option>
                                <option value="8:30">8:30</option>
                                <option value="9:00">9:00</option>
                                <option value="9:30">9:30</option>
                                <option value="10:00">10:00</option>
                                <option value="10:30">10:30</option>
                                <option value="11:00">11:00</option>
                                <option value="11:30">11:30</option>
                                <option value="12:00">12:00</option>
                                <option value="12:30">12:30</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay timeOptionSet">
                              <div class="form-control timeOptionOnly activeTimeOption" id="time_of_birth_am">AM</div>
                              <div class="form-control timeOptionOnly" id="time_of_birth_pm" style="border-top-left-radius: 0 !important;border-bottom-left-radius: 0 !important;">PM</div>
                              <input type="hidden" name="time_of_birth_ampm" id="time_of_birth_ampm" value="AM" />
                            </div>
                            <div class="form-group birthdayYear"></div>
                          </div>
                                    
                                  </div>
                                </div>
                                <div class="eventColHeading">Where </div>
                                <div class="whatForm whereFrom">
                                  <div class="form-group" id="place_of_birth_div">
                                    <label for="">Place of Birth</label>
                                    <select class="form-control formBarthPlace" name="birth_place" id="place_of_birth">
                                      <option value="" >Select</option>
                    @if($place_of_birth)
                    	@foreach($place_of_birth as $place)
                    					<option option="{{ $place->place_name }}" data-id="{{ $place->_place_id }}">{{$place->place_name}}</option>
                    	@endforeach
                    @endif
                                      <option value="add">Add</option>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Country</label>
                                    <select class="form-control formCountry" name="birth_country" id="birth_country">
                              <option value="">Select</option>
            @if($countries)
              @foreach($countries as $country)
                              <option value="{{ $country->country_name }}" data-id="{{ $country->_country_id }}">{{ $country->country_name }}</option>
              @endforeach
            @endif
                              {{-- <option>1</option>
                              <option>2</option> --}}
                            </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">State/County/Province</label>
                                    <select class="form-control formState" name="birth_state" id="birth_province">
                                      <option value="" >Select</option>
                                      {{-- <option>1</option>
                                      <option>2</option> --}}
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">City</label>
                                    <select class="form-control formCity" name="birth_city" id="birth_city">
                                      <option value="" >Select</option>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Street Address</label>
                                    <input type="text" class="form-control formAddress" id="" placeholder="123 abc Ave." name="birth_street">
                                  </div>
                                  <div class="form-group">
                                    <label for="">ZIP</label>
                                    <input type="text" class="form-control formZip" id="" placeholder="ZIP" name="birth_zip">
                                  </div>
                                  <div class="bar"></div>
                                  <div class="form-group" id="adoption_status_div">
                                    <label for="">Status</label>
                                    <select class="form-control formBarthPlace" name="adoption_status" id="adoption_status">
                                      <option value="" >Select</option>
                	@if($adoption_status)
                    	@foreach($adoption_status as $status)
                    					<option option="{{ $status->status_name }}">{{$status->status_name}}</option>
                    	@endforeach
                    @endif
                                      <option value="add">Add</option>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Country</label>
                                    <select class="form-control formCountry" name="country" id="country">
                              <option value="" >Select</option>
            @if($countries)
              @foreach($countries as $country)
                              <option value="{{ $country->country_name }}" data-id="{{ $country->_country_id }}">{{ $country->country_name }}</option>
              @endforeach
            @endif
                              {{-- <option>1</option>
                              <option>2</option> --}}
                            </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">State/County/Province</label>
                                    <select class="form-control formState" name="state" id="province">
                                      <option value="" >Select</option>
                                      {{-- <option>1</option>
                                      <option>2</option> --}}
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">City</label>
                                    <select class="form-control formCity" name="city" id="city">
                                      <option value="" >Select</option>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Street Address</label>
                                    <input type="text" class="form-control formAddress" id="" placeholder="123 abc Ave." name="street">
                                  </div>
                                  <div class="form-group">
                                    <label for="">ZIP</label>
                                    <input type="text" class="form-control formZip" id="" placeholder="ZIP" name="zip">
                                  </div>
                                </div>
                                <div class="eventColHeading">When </div>
                                <div class="whatForm whereFrom">
                                  <div class="birthdayDate">
                                    <div class="form-group birthdayMonth">
                                      <label for="number">Month</label>
                                      <select class="form-control formMonth" id="" name="when_from_month">
                                        <option value="" >Month</option>
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayDay">
                                      <label for="number">Day</label>
                                      <select class="form-control" id="" name="when_from_day">
                                        <option value="" >DD</option>
                                        @for($i=1;$i<=31;$i++)
                                          @php
                                            $j = sprintf('%02d', $i)
                                          @endphp
                                         <option value="{{ $j }}">{{ $j }}</option>
                                        @endfor
                                        {{-- <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option> --}}
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayYear">
                                      <label for="number">Year</label>
                                      <select class="form-control" id="" name="when_from_year">
                                        <option value="" >YYYY</option>
                                      @for ($i = date('Y'); $i >= 1900; $i--)
                                        <option value="{{ $i }}">{{$i}}</option>
                                      @endfor
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                  </div>
                                </div>
                                <div class="eventColHeading">Who, What, Where, When, Why </div>
                                <div class="whatForm whereFrom">
                                  
                                  	<div class="myAllEvents" id="keywords_div"></div>
			                        <input type="hidden" id="keywords" value="" name="event_keywords">
			                        <div class="input-group lifeEvent">
			                          <input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
			                          <div class="input-group-append">
			                            <a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
			                           </div>
			                        </div>
                                  <div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                                </div>
                                
                                @include('inc.life_event_img_upload')

                                {{-- <div class="whatForm whatFormBtnAll">
                                  <div class="whatFormBtnSE">
                                    <div class="btn whatFormBtn">Add Another</div>
                                    <div class="btn whatFormBtn">Save and Exit</div>
                                  </div>

                                  <div class="btn-Edit-save btn-feedback btn-premium">
                                    <button class="btn btn-Edit btn-save">what'scommon <br><span>upload your life event</span></button>
                                  </div>
                                </div> --}}
                              </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
<!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Adoption</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Adoption</label>
            <p>NO MORE… Sealed files, government “<span class="text-danger">RED</span>-TAPE”. If you have the details and
want to find each it’s as good as DONE! If you're searching for your child or
your parents, this is the perfect life event for you. This section is very personal
and about you. If you remember anything about your weight, time of birth,
hospital, birth name, parents name or vice versa - Enter all the details you
remember about your mother/father - if you're the mother/father, specify.</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>There are two (2) section under where - Origin of birth location and
foster/shelter home location. If adopted, enter both remembered origin birth
location and foster/shelter location. If parent, specify the birth location of
your child.</p>
          </div>
          <div class="form-group">
            <label>WHEN:</label>
            <p>(Child) Enter your birthday. (Parent) Enter your child's birthday.</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
@include('inc.footer')
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).ready(function(){
  $("#weight_measure_kg").click(function(){
    $("#weight_measure_kg").addClass("activeTimeOption");
    $("#weight_measure_lbs").removeClass("activeTimeOption");
    $('#weight_measure').val('kg');
    var weight = $('#weight').val();
    var changed_weight = (weight*0.453592).toFixed(2);
    $('#weight').val(changed_weight);
  });
  $("#weight_measure_lbs").click(function(){
    $("#weight_measure_lbs").addClass("activeTimeOption");
    $("#weight_measure_kg").removeClass("activeTimeOption");
    $('#weight_measure').val('lbs');
    var weight = $('#weight').val();
    var changed_weight = (weight*2.20462).toFixed(2);
    $('#weight').val(changed_weight);
  });
  $("#height_measure_in").click(function(){
    $("#height_measure_in").addClass("activeTimeOption");
    $("#height_measure_cm").removeClass("activeTimeOption");
    $('#height_measure').val('in');
    var height = $('#height').val();
    var changed_height = (height*0.393701).toFixed(2);
    $('#height').val(changed_height);
  });
  $("#height_measure_cm").click(function(){
    $("#height_measure_cm").addClass("activeTimeOption");
    $("#height_measure_in").removeClass("activeTimeOption");
    $('#height_measure').val('cm');
    var height = $('#height').val();
    var changed_height = (height*2.54).toFixed(2);
    $('#height').val(changed_height);
  });
  $("#time_of_birth_am").click(function(){
      $("#time_of_birth_am").addClass("activeTimeOption");
      $("#time_of_birth_pm").removeClass("activeTimeOption");
      $('#time_of_birth_ampm').val('AM');
  });
  $("#time_of_birth_pm").click(function(){
    $("#time_of_birth_pm").addClass("activeTimeOption");
    $("#time_of_birth_am").removeClass("activeTimeOption");
    $('#time_of_birth_ampm').val('PM');
  });
});
$(document).on('change','#gender',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=genders&column_name=gender_name&status_column=gender_is_active');
  }
});
$(document).on('change','#place_of_birth',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=place_of_birth&column_name=place_name&status_column=place_is_active');
  }
});
$(document).on('change','#adoption_status',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=adoption_status&column_name=status_name&status_column=status_is_active');
  }
});
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="" hidden="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});

$(document).on('change', '#birth_country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#birth_province').html(response.reponse_body.province);
            $('#birth_city').html('<option value="" hidden="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#birth_province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#birth_city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script>