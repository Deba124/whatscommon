@include('inc.header')
<div class="innerBodyOnly innerBodyModify">
  <div class="container-fluid">
    <div class="settingBody">
      <div class="position-relative settingFlip">
          <div class="leftSlidePan closePan">
            <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
            <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
              @include('inc.life_event_left_menu')
            </div>
          </div>
          <div class="midPan">
            <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
              <div class="row infoBodyRow">
                <div class="col-md-5 infoBodyCol connectionsBody eventImgCol">
                  <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
                    <div class="innerHome">
                      <a href="{{url('home')}}"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                    </div>
                    <div class="feedbackImg">
                      <div class="personalImgAll">
                        <img src="new-design/img/educationImg.png" class="img-fluid" alt="">
                        <div class="personalImgTitle">School / Education</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-7 connectionsBody infoBodyCol eventCol midHightControl">
                  <div class="connectionsBody feedbackRight windowHeight windowHeightMid">
                      <form class="xhr_form" method="post" action="api/life-event-upload" id="life-event-upload">
                        @csrf
                        <div class="eventColHeading">What </div><img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                        <input type="hidden" name="type_id" value="6">
                        <input type="hidden" name="user_id" value="{{Session::get('userdata')['id']}}">
                        <div class="whatForm">
                          <div class="form-group">
                            <label for="">What</label>
                            <select class="form-control " name="sub_category_id">
            		@if($what)
                    	@foreach($what as $w)
                                      <option value="{{ $w->id }}">{{ $w->sub_cat_type_name }}</option>
                        @endforeach
                    @endif
                            </select>
                          </div>
                          <div class="whatReasonAll">
                            <div class="form-group adoptionCheckAll checkCenter">
                              <label class="adoptionCheck">I graduated
                                <input type="checkbox">
                                <span class="adoptionCheckmark"></span>
                              </label>
                            </div>
                            <div class="form-group topFormIcon">
                              <label for="">School Info</label>
                              <input type="text" class="form-control formEdu" id="" placeholder="Name of School" name="school_name">
                            </div>
                            <div class="form-group topFormIcon">
                              <label for="">Sorority/Fraternity</label>
                              <input type="text" class="form-control formSorority" id="" placeholder="Name of Group" name="school_sorority">
                            </div>
                            <div class="form-group topFormIcon">
                              <label for="">Teacher/Professor</label>
                              <input type="text" class="form-control formTeacher" id="" placeholder="Name of Teacher" name="school_teacher">
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Room No.</label>
                                  <input type="text" class="form-control formRoom" name="school_roomno">
                                  
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Subject</label>
                                  <select class="form-control formSubject" name="school_subject">
                                    <option value="">Select</option>
                	@if($subjects)
                    	@foreach($subjects as $subject)
                    					<option option="{{ $subject->subject_name }}">{{$subject->subject_name}}</option>
                    	@endforeach
                    @endif
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Curriculum</label>
                                  <select class="form-control formCurriculars" name="school_curriculars">
                                    <option value="">Select</option>
                    @if($curriculars)
                    	@foreach($curriculars as $curricular)
                    					<option option="{{ $curricular->curricular_name }}">{{$curricular->curricular_name}}</option>
                    	@endforeach
                    @endif
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Organization</label>
                                  <select class="form-control formOrganization" name="school_organization">
                                    <option value="">Select</option>
                    @if($organizations)
                    	@foreach($organizations as $organization)
                    				<option option="{{ $organization->organization_name }}">{{$organization->organization_name}}</option>
                    	@endforeach
                    @endif
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Event</label>
                                  <select class="form-control formEvent" name="school_event">
                                    <option value="">Select</option>
                    @if($events)
                    	@foreach($events as $event)
                    				<option option="{{ $event->event_name }}">{{$event->event_name}}</option>
                    	@endforeach
                    @endif
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">After School</label>
                                  <select class="form-control formAfterSchool" name="school_afterschool">
                                    <option value="">Select</option>
                    @if($after_schools)
                    	@foreach($after_schools as $as)
                    				<option option="{{ $as->as_name }}">{{$as->as_name}}</option>
                    	@endforeach
                    @endif
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="eventColHeading">Where </div>
                        <div class="whatForm whereFrom">
                          <div class="form-group">
                            <label for="">Country</label>
                            <select class="form-control formCountry" name="country" id="country">
                              <option value="">Select</option>
            @if($countries)
              @foreach($countries as $country)
                              <option value="{{ $country->country_name }}" data-id="{{ $country->_country_id }}">{{ $country->country_name }}</option>
              @endforeach
            @endif
                              {{-- <option>1</option>
                              <option>2</option> --}}
                            </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">State/County/Province</label>
                            <select class="form-control formState" name="state" id="province">
                              <option value="">Select</option>
                              {{-- <option>1</option>
                              <option>2</option> --}}
                            </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">City</label>
                            <select class="form-control formCity" name="city" id="city">
                              <option value="">Select</option>
                            </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">Street Address</label>
                            <input type="text" class="form-control formAddress" id="" placeholder="123 abc Ave." name="street">
                          </div>
                          <div class="form-group">
                            <label for="">ZIP</label>
                            <input type="text" class="form-control formZip" id="" placeholder="ZIP" name="zip">
                          </div>
                        </div>
                        <div class="eventColHeading">When </div>
                        <div class="whatForm whereFrom">
                          <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">From</label>
                              <select class="form-control formMonth" id="" name="when_from_month">
                                <option value="">Month</option>
                                <option value="01">January</option>
                                <option value="02">February</option>
                                <option value="03">March</option>
                                <option value="04">April</option>
                                <option value="05">May</option>
                                <option value="06">June</option>
                                <option value="07">July</option>
                                <option value="08">August</option>
                                <option value="09">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay">
                              <label for="number">Day</label>
                              <select class="form-control" id="" name="when_from_day">
                                <option value="">DD</option>
                                @for($i=1;$i<=31;$i++)
                                  @php
                                    $j = sprintf('%02d', $i)
                                  @endphp
                                 <option value="{{ $j }}">{{ $j }}</option>
                                @endfor
                                {{-- <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option> --}}
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayYear">
                              <label for="number">Year</label>
                              <select class="form-control" id="" name="when_from_year">
                                <option value="">YYYY</option>
                              @for ($i = date('Y'); $i >= 1900; $i--)
                                <option value="{{ $i }}">{{$i}}</option>
                              @endfor
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                          </div>
                          <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">To</label>
                              <select class="form-control formMonth" id="" name="when_to_month">
                                <option value="">Month</option>
                                <option value="01">January</option>
                                <option value="02">February</option>
                                <option value="03">March</option>
                                <option value="04">April</option>
                                <option value="05">May</option>
                                <option value="06">June</option>
                                <option value="07">July</option>
                                <option value="08">August</option>
                                <option value="09">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay">
                              <label for="number">Day</label>
                              <select class="form-control" id="" name="when_to_day">
                                <option value="">DD</option>
                                @for($i=1;$i<=31;$i++)
                                  @php
                                    $j = sprintf('%02d', $i)
                                  @endphp
                                 <option value="{{ $j }}">{{ $j }}</option>
                                @endfor
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayYear">
                              <label for="number">Year</label>
                              <select class="form-control" id="" name="when_to_year">
                                <option value="">YYYY</option>
                              @for ($i = date('Y'); $i >= 1900; $i--)
                                <option value="{{ $i }}">{{$i}}</option>
                              @endfor
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                          </div>
                          {{-- <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">Time</label>
                              <select class="form-control formTime" id="" name="">
                                <option>00:00</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay timeOptionSet">
                              <div class="form-control timeOptionOnly activeTimeOption">AM</div>
                              <div class="form-control timeOptionOnly">PM</div>
                            </div>
                            <div class="form-group birthdayYear"></div>
                          </div> --}}
                        </div>
                        <div class="eventColHeading">Who, What, Where, When, Why </div>
                        <div class="whatForm whereFrom">
                          
                          <div class="myAllEvents" id="keywords_div"></div>
	                        <input type="hidden" id="keywords" value="" name="event_keywords">
	                        <div class="input-group lifeEvent">
	                        	<input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
	                        	<div class="input-group-append">
	                            <a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
                            </div>
                          </div>
                          <div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                        </div>
                        
                        @include('inc.life_event_img_upload')

                        {{-- <div class="whatForm whatFormBtnAll">
                          <div class="whatFormBtnSE">
                            <div class="btn whatFormBtn">Add Another</div>
                            <div class="btn whatFormBtn">Save and Exit</div>
                          </div>

                          <div class="btn-Edit-save btn-feedback btn-premium">
                            <button class="btn btn-Edit btn-save">what'scommon <br><span>upload your life event</span></button>
                          </div>
                        </div> --}}
                      </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow:hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >School/Education</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: School/Education</label>
            <p>Think back in time and remember enter all the details you remember during
your school days. Classrooms, courses, sports, teachers and so much more.</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>It's easy to remember your school location. Enter anything you remember.</p>
          </div>
          <div class="form-group">
            <label>WHEN:</label>
            <p>This is the date when you started and finished your education. Maybe just
your High School time frame and separate life event for other grades. Be
creative with your life events.</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
@include('inc.footer')
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="" hidden="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script>