@include('inc.header')
<div class="innerBodyOnly innerBodyModify">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                  <div class="leftSlidePan closePan">
                    <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                    <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                      @include('inc.life_event_left_menu')
                    </div>
                  </div>
                  <div class="midPan">
                    <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                      <div class="row infoBodyRow">
                        <div class="col-md-5 infoBodyCol connectionsBody eventImgCol">
                          <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
                            <div class="innerHome">
                              <a href="{{url('home')}}"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                            </div>
                            <div class="feedbackImg">
                              <div class="personalImgAll">
                                <img src="new-design/img/personalImg.png" class="img-fluid" alt="">
                                <div class="personalImgTitle">Personal</div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7 connectionsBody infoBodyCol eventCol midHightControl">
                          <div class="connectionsBody feedbackRight windowHeight windowHeightMid">
                            <form class="xhr_form" method="post" action="api/life-event-upload" id="life-event-upload">
                            @csrf
                              <div class="eventColHeading">What </div>
                              
                                <img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                              
                              <input type="hidden" name="type_id" value="1">
                              <input type="hidden" name="user_id" id="user_id" value="{{Session::get('userdata')['id']}}">
                                <div class="whatForm">
                                  <div class="form-group">
                                    <label for="">What</label>
                                    <select class="form-control " id="whatReason" name="sub_category_id">
                                      <option value="" disabled selected>Select</option>
                    @if($what)
                      @foreach($what as $w)
                                      <option value="{{ $w->id }}">{{ $w->sub_cat_type_name }}</option>
                                      {{-- <option value="2">Vehicle Owned</option>
                                      <option value="3">Family</option> --}}
                        @endforeach
                    @endif
                                    </select>
                                  </div>
                                  <div class="whatReason1">
                                    <div class="form-group" id="home_types_div">
                                      <label for="">Type of Home</label>
                                      <select class="form-control " name="type_of_home" id="home_types">
                                        <option value="">Select</option>
                    @if($home_types)
                      @foreach($home_types as $home_type)
                              <option option="{{ $home_type->id }}">{{$home_type->home_type_name}}</option>
                      @endforeach
                    @endif
                                        <option value="add">Add</option>
                                      </select>
                                    </div>
                                    <div class="form-group" id="home_styles_div">
                                      <label for="">Style</label>
                                      <select class="form-control " name="home_style" id="home_styles">
                                        <option value="">Select</option>
                    @if($home_styles)
                      @foreach($home_styles as $home_style)
                              <option option="{{ $home_style->home_style_name }}">{{$home_style->home_style_name}}</option>
                      @endforeach
                    @endif
                                        <option value="add">Add</option>
                                      </select>
                                    </div>
                                   {{--  <div class="anotherLocation">Add next location</div> --}}
                                  </div>

                                  <div class="whatReason2 vehicleForm removedWhat">
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Plate Tag Number</label>
                                          <input type="text" class="form-control formPlate" id="" placeholder="123abc" name="tag_number">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="vehicle_types_div">
                                          <label for="">Type of Vehicle</label>
                                          <select class="form-control formVehicle" id="vehicle_type" name="type_of_vehicle">
                                            <option value="">Select</option>
                    @if($vehicle_types)
                      @foreach($vehicle_types as $vehicle_type)
                              <option option="{{ $vehicle_type->vehicle_type_name }}" data-id="{{ $vehicle_type->id }}" data-makers="{{ $vehicle_type->makers }}">{{$vehicle_type->vehicle_type_name}}</option>
                      @endforeach
                    @endif
                                            <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="vehicle_makers_div">
                                          <label for="">Make</label>
                                          <select class="form-control formMake" id="vehicle_maker" name="vehicle_maker">
                                            <option value="">Select Type</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="vehicle_model_div">
                                          <label for="">Model</label>
                                          <select class="form-control formModel" name="vehicle_model" id="vehicle_model">
                                            <option value="">Select Model</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Year</label>
                                          <input type="number" class="form-control formMonth" name="year_manufactured">
                                          {{-- <select class="form-control formMonth">
                    
                                          </select> --}}
                                          {{-- <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt=""> --}}
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="vehicle_color_div">
                                          <label for="">Color</label>
                                          <select class="form-control formColor" id="vehicle_color" name="vehicle_color">
                                            <option value="" >Select</option>
                  @if($vehicle_colors)
                      @foreach($vehicle_colors as $color)
                              <option option="{{ $color->color_name }}">{{$color->color_name}}</option>
                      @endforeach
                    @endif
                                            <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                    </div>
                                    {{-- <div class="connectTagsterAll">
                                      <a href="#" class="connectTagster">
                                        <img src="new-design/img/TagsterLogo.png" class="img-fluid connectTagsterLogo">
                                        <div class="connectTagsterText">Connect Tagster</div>
                                      </a>
                                    </div> --}}
                                  </div>
                                  <div class="whatReason3 vehicleForm removedWhat">
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">First Name</label>
                                          <input type="text" class="form-control formName" id="" placeholder="Name" name="first_name">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Last Name</label>
                                          <input type="text" class="form-control formName" id="" placeholder="Name" name="last_name">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Age</label>
                                          <input type="number" class="form-control formAge" name="age">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="genders_div">
                                          <label for="">Gender</label>
                                          <select class="form-control formGender" name="gender" id="gender">
                                            <option value="" >Select</option>
                    @if($genders)
                      @foreach($genders as $gender)
                              <option option="{{ $gender->gender_name }}">{{$gender->gender_name}}</option>
                      @endforeach
                    @endif
                                            <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="relations_div">
                                          <label for="">Relation</label>
                                          <select class="form-control formRelationship" id="relations" name="relationship">
                                            <option value="" >Select</option>
                    @if($relations)
                      @foreach($relations as $relation)
                              <option option="{{ $relation->relation_name }}">{{$relation->relation_name}}</option>
                      @endforeach
                    @endif
                                            <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="marital_status_div">
                                          <label for="">Status</label>
                                          <select class="form-control formStatus" id="marital_status" name="marital_status">
                                            <option value="" >Select</option>
                    @if($marital_status)
                      @foreach($marital_status as $ms)
                              <option option="{{ $ms->ms_name }}">{{$ms->ms_name}}</option>
                      @endforeach
                    @endif
                                            <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="industries_div">
                                          <label for="">Industry</label>
                                          <select class="form-control formIndustry" id="industry" name="industry">
                                            <option value="" >Select</option>
                    @if($industries)
                      @foreach($industries as $industry)
                              <option option="{{ $industry->industry_name }}">{{$industry->industry_name}}</option>
                      @endforeach
                    @endif
                                            <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="occupations_div">
                                          <label for="">Category</label>
                                          <select class="form-control formCategory" id="category" name="category">
                                            <option value="">Select</option>
                    @if($occupations)
                      @foreach($occupations as $occupation)
                              <option option="{{ $occupation->occupation_name }}">{{$occupation->occupation_name}}</option>
                      @endforeach
                    @endif
                                            <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <!--<div class="connectTagsterAll" style="margin-bottom: 15px;">-->
                                <!--  <a href="#" class="connectTagster" onclick="open_modal('coming-soon');">-->
                                <!--    <img src="new-design/img/TagsterLogo.png" class="img-fluid connectTagsterLogo">-->
                                <!--    <div class="connectTagsterText">Connect Tagster</div>-->
                                <!--  </a>-->
                                <!--</div>-->
                                <!-- <div class="form-group text-center">
                                  <a href="javascript:void;" onclick="open_modal('coming-soon');" style="color: #60b15d;border: 2px solid #60b15d;padding: 10px 25px;border-radius: 20px;font-weight: 700;">
                                    <img src="{{ url('new-design/img/TagsterLogo.png') }}"> Connect Tagster
                                  </a>
                                </div> -->
                                <div class="eventColHeading">Where</div>
                                <div class="whatForm whereFrom">
                                  <div class="form-group">
                                    <label for="">Country</label>
                                    <select class="form-control formCountry" name="country" id="country">
                              <option value="">Select</option>
            @if($countries)
              @foreach($countries as $country)
                              <option value="{{ $country->country_name }}" data-id="{{ $country->_country_id }}">{{ $country->country_name }}</option>
              @endforeach
            @endif
                              {{-- <option>1</option>
                              <option>2</option> --}}
                            </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">State/County/Province</label>
                                    <select class="form-control formState" name="state" id="province">
                                      <option value="">Select</option>
                                      {{-- <option>1</option>
                                      <option>2</option> --}}
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">City</label>
                                    <select class="form-control formCity" name="city" id="city">
                                      <option value="">Select</option>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Street Address</label>
                                    <input type="text" class="form-control formAddress" id="" placeholder="123 abc Ave." name="street">
                                  </div>
                                  <div class="form-group">
                                    <label for="">ZIP</label>
                                    <input type="text" class="form-control formZip" id="" placeholder="ZIP" name="zip">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="text" class="form-control formMail" id="" placeholder="Enter Email" name="email">
                                  </div>
                                  {{-- <div class="form-group">
                                    <label for="">Phone</label>
                                    <input type="text" class="form-control formPh" id="" placeholder="Enter Phone" name="phone">
                                  </div> --}}
                                  <div class="form-group">
                                    <label for="">Phone</label>
                                    <div class="input-group ">
                                      <select class="form-control " style="max-width: max-content;border-top-left-radius: 10px;border-bottom-left-radius: 10px;color: #3b71b9;" name="isd_code">
                                        <option value="">Select</option>
                                  @if($countries)
                                    @foreach($countries as $country)
                                      @php
                                      if(!$country->country_isd){
                                        continue;
                                      }
                                      @endphp
                                        <option value="+{{ $country->country_isd }}" >+{{ $country->country_isd }}</option>
                                    @endforeach
                                  @endif    
                                      </select>
                                      <input type="number" class="form-control formPh" id="" name="phone" value="" style="border-top-right-radius: 10px;border-bottom-right-radius: 10px;">
                                    </div>
                                  </div>
                                </div>
                                <div class="eventColHeading">When</div>
                                <div class="whatForm whereFrom">
                                  <div class="birthdayDate">
                                    <div class="form-group birthdayMonth">
                                      <label for="number">From</label>
                                      <select class="form-control formMonth" id="" name="when_from_month">
                                        <option value="">Month</option>
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayDay">
                                      <label for="number">Day</label>
                                      <select class="form-control" id="" name="when_from_day">
                                        <option value="">DD</option>
                                        @for($i=1;$i<=31;$i++)
                                          @php
                                            $j = sprintf('%02d', $i)
                                          @endphp
                                         <option value="{{ $j }}">{{ $j }}</option>
                                        @endfor
                                        {{-- <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option> --}}
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayYear">
                                      <label for="number">Year</label>
                                      <select class="form-control" id="" name="when_from_year">
                                        <option value="">YYYY</option>
                                      @for ($i = date('Y'); $i >= 1900; $i--)
                                        <option value="{{ $i }}">{{$i}}</option>
                                      @endfor
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                  </div>
                                  <div class="birthdayDate">
                                    <div class="form-group birthdayMonth">
                                      <label for="number">To</label>
                                      <select class="form-control formMonth" id="" name="when_to_month">
                                        <option value="">Month</option>
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayDay">
                                      <label for="number">Day</label>
                                      <select class="form-control" id="" name="when_to_day">
                                <option value="">DD</option>
                                @for($i=1;$i<=31;$i++)
                                  @php
                                    $j = sprintf('%02d', $i)
                                  @endphp
                                 <option value="{{ $j }}">{{ $j }}</option>
                                @endfor
                              </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayYear">
                                      <label for="number">Year</label>
                                      <select class="form-control" id="" name="when_to_year">
                                <option value="">YYYY</option>
                              @for ($i = date('Y'); $i >= 1900; $i--)
                                <option value="{{ $i }}">{{$i}}</option>
                              @endfor
                              </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                  </div>

                                </div>
                                <div class="eventColHeading">Who, What, Where, When, Why</div>
                                <div class="whatForm whereFrom">
                                  {{-- <div class="addFieldItem">Samgsung <span class="removeField"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>
                                  <div class="addFieldItem">Dog <span class="removeField"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>
                                  <div class="addFieldItem">Treehouse <span class="removeField"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div> --}}
                                  {{-- <div class="input-group lifeEvent">
                                    <input type="text" class="form-control" placeholder="Life Event Keyword(s)">
                                    <div class="input-group-append">
                                      <div class="btn btn-add">Add</div>
                                     </div>
                                  </div> --}}
                                    <div class="myAllEvents" id="keywords_div"></div>
                              <input type="hidden" id="keywords" value="" name="event_keywords">
                              <div class="input-group lifeEvent">
                                <input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
                                <div class="input-group-append">
                                  <a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
                                 </div>
                              </div>
                                    <div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                                </div>

                                @include('inc.life_event_img_upload')

                                {{-- <div class="whatForm whatFormBtnAll">
                                  <div class="whatFormBtnSE">
                                    <div class="btn whatFormBtn">Add Another</div>
                                    <div class="btn whatFormBtn">Save and Exit</div>
                                  </div>

                                  <div class="btn-Edit-save btn-feedback btn-premium">
                                    <button class="btn btn-Edit btn-save">what'scommon <br><span>upload your life event</span></button>
                                  </div>
                                </div> --}}
                              </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
 <!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow:hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Personal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Personal</label>
            <p>It's interesting to look back in time and remember all the things you did and
the people you have met. This section will ask specific contents about "what"
the life event is all about. Have fun, List places you lived and match with
forgotten neighbors, friends’ childhood besties. Cars you’ve owed can match
with details people remember about you etc. keep on thinking… keep on
remembering…</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>This section will ask specific contents about "where" the life event happened</p>
          </div>
          <div class="form-group">
            <label>WHEN:</label>
            <p>This section will work like your calender. You have to enter any day or date you remember. There's two(2) section available - when the event started and when done.</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
@include('inc.footer')
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$("#whatReason").change(function(){
    if($(this).val() == "2") {
        $('.whatReason1').addClass('removedWhat');
        $('.whatReason2').removeClass('removedWhat');
        $('.whatReason3').addClass('removedWhat');
    } else if($(this).val() == "3") {
        $('.whatReason1').addClass('removedWhat');
        $('.whatReason2').addClass('removedWhat');
        $('.whatReason3').removeClass('removedWhat');
    } else {
        $('.whatReason1').removeClass('removedWhat');
        $('.whatReason2').addClass('removedWhat');
        $('.whatReason3').addClass('removedWhat');
    }
});

$(document).on('change','#vehicle_color',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=vehicle_color&column_name=color_name&status_column=color_is_active');
  }
});
$(document).on('change','#gender',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=genders&column_name=gender_name&status_column=gender_is_active');
  }
});
$(document).on('change','#relations',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=relations&column_name=relation_name&status_column=relation_is_active');
  }
});
$(document).on('change','#industry',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=industries&column_name=industry_name&status_column=industry_is_active');
  }
});
$(document).on('change','#marital_status',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=marital_status&column_name=ms_name&status_column=ms_is_active');
  }
});
$(document).on('change','#category',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=occupations&column_name=occupation_name&status_column=occupation_is_active');
  }
});
$(document).on('change','#home_types',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=home_types&column_name=home_type_name&status_column=home_type_is_active');
  }
});
$(document).on('change','#home_styles',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=home_styles&column_name=home_style_name&status_column=home_style_is_active');
  }
});
$(document).on('change','#vehicle_type',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=vehicle_types&column_name=vehicle_type_name&status_column=vehicle_type_is_active');
  }else{
    var makers = $(this).find(':selected').data('makers');
    var vehicle_maker = '<option value="" hidden>Select Maker</option>';
    if(makers){
      $.each( makers, function( index, maker ) {
        vehicle_maker += '<option value="'+maker.maker_name+'" data-maker_id="'+maker._maker_id+'">'+maker.maker_name+'</option>';
      });
    }
    vehicle_maker +='<option value="add">Add</option>';
    $('#vehicle_maker').html(vehicle_maker);
  }
});
$(document).on('change', '#vehicle_maker', function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    var vehicle_type = $('#vehicle_type').find(':selected').data('id');
    open_modal('add-master', 'user_id='+user_id+'&db_name=vehicle_makers&column_name=maker_name&status_column=maker_is_active&dependent_name=maker_vehicle_type&dependent_value='+vehicle_type+'&is_dependent=1');
  }else{
    var maker_id = $(this).find(':selected').data('maker_id');
    var user_id = $('#user_id').val();
    console.log('maker_id => '+maker_id);
    execute('api/get-master','user_id='+user_id+'&db_name=vehicle_model&column_name=model_name&status_column=model_is_active& dependent_name=model_maker_id&dependent_value='+maker_id+'&is_dependent=1');
  }
});
$(document).on('change','#vehicle_model',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = $('#user_id').val();
    var maker_id = $('#vehicle_maker').find(':selected').data('maker_id');
    open_modal('add-master','user_id='+user_id+'&db_name=vehicle_model&column_name=model_name&status_column=model_is_active&dependent_name=model_maker_id&dependent_value='+maker_id+'&is_dependent=1');
  }
});
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="" hidden="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script>