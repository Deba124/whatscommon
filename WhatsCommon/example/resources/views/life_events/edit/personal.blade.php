@include('inc.header')
<div class="innerBodyOnly innerBodyModify">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                <div class="leftSlidePan closePan">
            <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
            <div class="lifeDraftLeftTitle"> {{ $event_data->event_is_draft==0 ? 'LIFE EVENTS' : 'DRAFTS' }} </div>
            <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
              <ul class="list-unstyled mb-0 lifeEventMenu">
                <li class="lifeEventMenuLi lifeDraftLi {{ $event_data->event_is_draft==0 ? 'lifeEventMenuLiActive' : '' }}">
                  <a href="{{ url('life-events') }}" class="lifeEventMenuLink">
                    <span class="lifeEventMenuText">Created Life Events</span>
                    <span class="lifeDraftCount">{{ $event_count }}</span>
                  </a>
                </li>
                <li class="lifeEventMenuLi lifeDraftLi {{ $event_data->event_is_draft==1 ? 'lifeEventMenuLiActive' : '' }}">
                  <a href="{{ url('drafts') }}" class="lifeEventMenuLink lifeDraftMenuLink">
                    <span class="lifeEventMenuText">Drafts</span>
                    <span class="lifeDraftCount">{{ $draft_count }}</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
                  <div class="midPan">
                    <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                      <div class="row infoBodyRow">
                        <div class="col-md-5 infoBodyCol connectionsBody eventImgCol">
                          <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
                            <div class="innerHome">
                              <a href="{{url('home')}}"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                            </div>
                            <div class="feedbackImg">
                              <div class="personalImgAll">
                                <img src="new-design/img/personalImg.png" class="img-fluid" alt="">
                                <div class="personalImgTitle">Personal</div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7 connectionsBody infoBodyCol eventCol midHightControl">
                          <div class="connectionsBody feedbackRight windowHeight windowHeightMid">
                            <form class="xhr_form" method="post" action="api/save-life-event" id="save-life-event">
                            @csrf
                              <div class="eventColHeading">What </div>
                              
                                <img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                              
                              <input type="hidden" name="type_id" value="1">
                              <input type="hidden" name="user_id" id="user_id" value="{{Session::get('userdata')['id']}}">
                              <input type="hidden" name="_event_id" value="{{ $event_data->_event_id }}">
                                <div class="whatForm">
                                  <div class="form-group">
                                    <label for="">What</label>
                                    <select class="form-control forredline" id="whatReason" name="sub_category_id">
                                      <option value="" disabled >Select</option>
                    @if($what)
                      @foreach($what as $w)
                        @php
                          $selected = $w->id==$event_data->event_subcategory_id ? 'selected' : '';
                        @endphp
                                      <option value="{{ $w->id }}" {{ $selected }}>{{ $w->sub_cat_type_name }}</option>
                                      {{-- <option value="2">Vehicle Owned</option>
                                      <option value="3">Family</option> --}}
                        @endforeach
                    @endif
                                    </select>
                                  </div>
                                  <div class="whatReason1 {{ $event_data->event_subcategory_id==1 ? '' : 'removedWhat' }}">
                                    <div class="form-group" id="home_types_div">
                                      <label for="">Type of Home</label>
                                      <select class="form-control forredline" name="type_of_home" id="home_types">
                                        <option value="">Select</option>
                    @if($home_types)
                      @foreach($home_types as $home_type)
                        @php
                          $selected = $home_type->home_type_name==$event_data->event_home_type ? 'selected' : '';
                        @endphp
                              <option option="{{ $home_type->id }}" {{ $selected }}>{{$home_type->home_type_name}}</option>
                      @endforeach
                    @endif
                                        <option value="add">Add</option>
                                      </select>
                                    </div>
                                    <div class="form-group" id="home_styles_div">
                                      <label for="">Style</label>
                                      <select class="form-control forredline" name="home_style" id="home_styles">
                                        <option value="">Select</option>
                    @if($home_styles)
                      @foreach($home_styles as $home_style)
                        @php
                          $selected = $home_style->home_style_name==$event_data->event_home_style ? 'selected' : '';
                        @endphp
                        <option option="{{ $home_style->id }}" {{ $selected }}>{{$home_style->home_style_name}}</option>
                      @endforeach
                    @endif
                                        <option value="add">Add</option>
                                      </select>
                                    </div>
                                   {{--  <div class="anotherLocation">Add next location</div> --}}
                                  </div>

                                  <div class="whatReason2 vehicleForm {{ $event_data->event_subcategory_id==2 ? '' : 'removedWhat' }}">
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Plate Tag Number</label>
                                          <input type="text" class="form-control forredline formPlate" id="" placeholder="123abc" name="tag_number" value="{{ $event_data->event_tag_number }}">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="vehicle_types_div">
                                          <label for="">Type of Vehicle</label>
                                          <select class="form-control forredline formVehicle" id="vehicle_type" name="type_of_vehicle">
                                            <option value="">Select</option>
                    @if($vehicle_types)
                      @foreach($vehicle_types as $vehicle_type)
                        @php
                          $selected = $vehicle_type->vehicle_type_name==$event_data->event_vehicle_type ? 'selected' : '';
                        @endphp
                              <option option="{{ $vehicle_type->id }}" {{ $selected }} data-makers="{{ $vehicle_type->makers }}">{{$vehicle_type->vehicle_type_name}}</option>
                      @endforeach
                    @endif
                                            <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="vehicle_makers_div">
                                          <label for="">Make</label>
                                          <select class="form-control forredline formMake" id="vehicle_maker" name="vehicle_maker">
                                            <option value="">Select Type</option>
                    @if($vehicle_makers)
                      @foreach($vehicle_makers as $maker)
                        @php
                          $selected = $maker->maker_name==$event_data->event_vehicle_maker ? 'selected' : '';
                        @endphp
                        <option value="{{ $maker->maker_name }}" {{ $selected }}>{{ $maker->maker_name }}</option>
                      @endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="vehicle_model_div">
                                          <label for="">Model</label>
                                          <select class="form-control forredline formModel" name="vehicle_model" id="vehicle_model">
                                            <option value="">Select Model</option>
                    @if($vehicle_model)
                      @foreach($vehicle_model as $model)
                        @php
                          $selected = $model->model_name==$event_data->event_vehicle_model ? 'selected' : '';
                        @endphp
                        <option value="{{ $model->model_name }}" {{ $selected }}>{{ $model->model_name }}</option>
                      @endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Year</label>
                                          <input type="number" class="form-control forredline formMonth" name="year_manufactured" value="{{ $event_data->event_vehicle_year }}">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="vehicle_color_div">
                                          <label for="">Color</label>
                                          <select class="form-control forredline formColor" id="vehicle_color" name="vehicle_color">
                                            <option value="">Select</option>
                  @if($vehicle_colors)
                      @foreach($vehicle_colors as $color)
                        @php
                          $selected = $color->color_name==$event_data->event_vehicle_color ? 'selected' : '';
                        @endphp
                              <option option="{{ $color->color_name }}" {{ $selected }}>{{$color->color_name}}</option>
                      @endforeach
                    @endif
                                            <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="whatReason3 vehicleForm {{ $event_data->event_subcategory_id==3 ? '' : 'removedWhat' }}">
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">First Name</label>
                                          <input type="text" class="form-control forredline formName" id="" placeholder="Name" name="first_name" value="{{ $event_data->event_family_firstname }}">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Last Name</label>
                                          <input type="text" class="form-control forredline formName" id="" placeholder="Name" name="last_name" value="{{ $event_data->event_family_lastname }}">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Age</label>
                                          <input type="number" class="form-control forredline formAge" name="age" value="{{ $event_data->event_age }}">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="genders_div">
                                          <label for="">Gender</label>
                                          <select class="form-control forredline formGender" name="gender" id="gender">
                                            <option value="">Select</option>
                    @if($genders)
                      @foreach($genders as $gender)
                        @php
                          $selected = $gender->gender_name==$event_data->event_gender ? 'selected' : '';
                        @endphp
                              <option option="{{ $gender->gender_name }}" {{ $selected }}>{{$gender->gender_name}}</option>
                      @endforeach
                    @endif
                    <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="relations_div">
                                          <label for="">Relation</label>
                                          <select class="form-control forredline formRelationship" id="relations" name="relationship">
                                            <option value="">Select</option>
                    @if($relations)
                      @foreach($relations as $relation)
                        @php
                          $selected = $relation->relation_name==$event_data->event_family_relation ? 'selected' : '';
                        @endphp
                              <option option="{{ $relation->relation_name }}" {{ $selected }}>{{$relation->relation_name}}</option>
                      @endforeach
                    @endif
                    <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="marital_status_div">
                                          <label for="">Status</label>
                                          <select class="form-control forredline formStatus" id="marital_status" name="marital_status">
                                            <option value="">Select</option>
                    @if($marital_status)
                      @foreach($marital_status as $ms)
                        @php
                          $selected = $ms->ms_name==$event_data->event_family_marital_status ? 'selected' : '';
                        @endphp
                              <option option="{{ $ms->ms_name }}" {{ $selected }}>{{$ms->ms_name}}</option>
                      @endforeach
                    @endif
                    <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="industries_div">
                                          <label for="">Industry</label>
                                          <select class="form-control forredline formIndustry" id="industry" name="industry">
                                            <option value="">Select</option>
                    @if($industries)
                      @foreach($industries as $industry)
                        @php
                          $selected = $industry->industry_name==$event_data->event_family_industry ? 'selected' : '';
                        @endphp
                              <option option="{{ $industry->industry_name }}" {{ $selected }}>{{$industry->industry_name}}</option>
                      @endforeach
                    @endif
                    <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="occupations_div">
                                          <label for="">Category</label>
                                          <select class="form-control forredline formCategory" id="category" name="category">
                                            <option value="">Select</option>
                    @if($occupations)
                      @foreach($occupations as $occupation)
                        @php
                          $selected = $occupation->occupation_name==$event_data->event_family_category ? 'selected' : '';
                        @endphp
                              <option option="{{ $occupation->occupation_name }}" {{ $selected }}>{{$occupation->occupation_name}}</option>
                      @endforeach
                    @endif
                            <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <!--<div class="connectTagsterAll" style="margin-bottom: 15px;">-->
                                <!--  <a href="#" class="connectTagster" onclick="open_modal('coming-soon');">-->
                                <!--    <img src="new-design/img/TagsterLogo.png" class="img-fluid connectTagsterLogo">-->
                                <!--    <div class="connectTagsterText">Connect Tagster</div>-->
                                <!--  </a>-->
                                <!--</div>-->
                                <!-- <div class="form-group text-center">
                                  <a href="javascript:void;" onclick="open_modal('coming-soon');" style="color: #60b15d;border: 2px solid #60b15d;padding: 10px 25px;border-radius: 20px;font-weight: 700;">
                                    <img src="{{ url('new-design/img/TagsterLogo.png') }}"> Connect Tagster
                                  </a>
                                </div> -->
                                <div class="eventColHeading">Where</div>
                                <div class="whatForm whereFrom">
                                  <div class="form-group">
                                    <label for="">Country</label>
                                    <select class="form-control forredline formCountry" name="country" id="country">
                              <option value="">Select</option>
            @if($countries)
              @foreach($countries as $country)
                @php
                  $selected = $country->country_name==$where_data->where_country ? 'selected' : '';
                @endphp
                              <option value="{{ $country->country_name }}" {{ $selected }} data-id="{{ $country->_country_id }}">{{ $country->country_name }}</option>
              @endforeach
            @endif
                            </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">State/County/Province</label>
                                    <select class="form-control forredline formState" name="state" id="province">
                                      <option value="">Select</option>
                  @if($provinces)
                    @foreach($provinces as $province)
                      @php
                        $selected = ($where_data && $province->province_name==$where_data->where_state) ? 'selected' : '';
                      @endphp
                                    <option value="{{ $province->province_name }}" {{ $selected }} data-id="{{ $province->_province_id }}">{{ $province->province_name }}</option>
                    @endforeach
                  @endif
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">City</label>
                                    <select class="form-control forredline formCity" name="city" id="city">
                                      <option value="">Select</option>
                  @if($cities)
                    @foreach($cities as $city)
                      @php
                        $selected = ($where_data && $city->city_name==$where_data->where_city) ? 'selected' : '';
                      @endphp
                                    <option value="{{ $city->city_name }}" {{ $selected }} data-id="{{ $city->_city_id }}">{{ $city->city_name }}</option>
                    @endforeach
                  @endif
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Street Address</label>
                                    <input type="text" class="form-control forredline formAddress" id="" placeholder="123 abc Ave." name="street" value="{{ $where_data ? $where_data->where_street : '' }}">
                                  </div>
                                  <div class="form-group">
                                    <label for="">ZIP</label>
                                    <input type="text" class="form-control forredline formZip" id="" placeholder="ZIP" name="zip" value="{{ $where_data ? $where_data->where_zip : '' }}">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="text" class="form-control forredline formMail" id="" placeholder="Enter Email" name="email" value="{{ $where_data ? $where_data->where_email : '' }}">
                                  </div>
                                  {{-- <div class="form-group">
                                    <label for="">Phone</label>
                                    <input type="text" class="form-control forredline formPh" id="" placeholder="Enter Phone" name="phone" value="{{ $where_data ? $where_data->where_phone : '' }}">
                                  </div> --}}
                                  <div class="form-group">
                                    <label for="">Phone</label>
                                    <div class="input-group ">
                                      <select class="form-control " style="max-width: max-content;border-top-left-radius: 10px;border-bottom-left-radius: 10px;color: #3b71b9;" name="isd_code">
                                        <option value="">Select</option>
                                  @if($countries)
                                    @foreach($countries as $country)
                                      @php
                                      if(!$country->country_isd){
                                        continue;
                                      }
                                      $selected = $country->country_isd==$where_data->where_isd ? 'selected' : '';
                                      @endphp
                                        <option value="+{{ $country->country_isd }}" {{ $selected }}>+{{ $country->country_isd }}</option>
                                    @endforeach
                                  @endif    
                                      </select>
                                      <input type="number" class="form-control formPh" id="" name="phone" value="{{ $where_data ? $where_data->where_phone : '' }}" style="border-top-right-radius: 10px;border-bottom-right-radius: 10px;">
                                    </div>
                                  </div>
                                </div>
                                <div class="eventColHeading">When</div>
                                <div class="whatForm whereFrom">
                                  <div class="birthdayDate">
                                    <div class="form-group birthdayMonth">
                                      <label for="number">From</label>
                                      <select class="form-control forredline formMonth" id="" name="when_from_month">
                                        <option value="">Month</option>
                                <option value="01" {{ $when_data->when_from_month=='01'?'selected':'' }}>January</option>
                                <option value="02" {{ $when_data->when_from_month=='02'?'selected':'' }}>February</option>
                                <option value="03" {{ $when_data->when_from_month=='03'?'selected':'' }}>March</option>
                                <option value="04" {{ $when_data->when_from_month=='04'?'selected':'' }}>April</option>
                                <option value="05" {{ $when_data->when_from_month=='05'?'selected':'' }}>May</option>
                                <option value="06" {{ $when_data->when_from_month=='06'?'selected':'' }}>June</option>
                                <option value="07" {{ $when_data->when_from_month=='07'?'selected':'' }}>July</option>
                                <option value="08" {{ $when_data->when_from_month=='08'?'selected':'' }}>August</option>
                                <option value="09" {{ $when_data->when_from_month=='09'?'selected':'' }}>September</option>
                                <option value="10" {{ $when_data->when_from_month=='10'?'selected':'' }}>October</option>
                                <option value="11" {{ $when_data->when_from_month=='11'?'selected':'' }}>November</option>
                                <option value="12" {{ $when_data->when_from_month=='12'?'selected':'' }}>December</option>
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayDay">
                                      <label for="number">Day</label>
                                      <select class="form-control forredline" id="" name="when_from_day">
                                        <option value="">DD</option>
                                        @for($i=1;$i<=31;$i++)
                                          @php
                                            $j = sprintf('%02d', $i)
                                          @endphp
                                         <option value="{{ $j }}" {{ $when_data->when_from_day==$j ? 'selected':'' }}>{{ $j }}</option>
                                        @endfor
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayYear">
                                      <label for="number">Year</label>
                                      <select class="form-control forredline" id="" name="when_from_year">
                                        <option value="">YYYY</option>
                                      @for ($i = 1900; $i <= date('Y'); $i++)
                                        <option value="{{ $i }}" {{ $when_data->when_from_year==$i ? 'selected':'' }}>{{$i}}</option>
                                      @endfor
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                  </div>
                                  <div class="birthdayDate">
                                    <div class="form-group birthdayMonth">
                                      <label for="number">To</label>
                                      <select class="form-control forredline formMonth" id="" name="when_to_month">
                                        <option value="">Month</option>
                                <option value="01" {{ $when_data->when_to_month=='01'?'selected':'' }}>January</option>
                                <option value="02" {{ $when_data->when_to_month=='02'?'selected':'' }}>February</option>
                                <option value="03" {{ $when_data->when_to_month=='03'?'selected':'' }}>March</option>
                                <option value="04" {{ $when_data->when_to_month=='04'?'selected':'' }}>April</option>
                                <option value="05" {{ $when_data->when_to_month=='05'?'selected':'' }}>May</option>
                                <option value="06" {{ $when_data->when_to_month=='06'?'selected':'' }}>June</option>
                                <option value="07" {{ $when_data->when_to_month=='07'?'selected':'' }}>July</option>
                                <option value="08" {{ $when_data->when_to_month=='08'?'selected':'' }}>August</option>
                                <option value="09" {{ $when_data->when_to_month=='09'?'selected':'' }}>September</option>
                                <option value="10" {{ $when_data->when_to_month=='10'?'selected':'' }}>October</option>
                                <option value="11" {{ $when_data->when_to_month=='11'?'selected':'' }}>November</option>
                                <option value="12" {{ $when_data->when_to_month=='12'?'selected':'' }}>December</option>
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayDay">
                                      <label for="number">Day</label>
                                      <select class="form-control forredline" id="" name="when_to_day">
                                <option value="">DD</option>
                                @for($i=1;$i<=31;$i++)
                                  @php
                                    $j = sprintf('%02d', $i)
                                  @endphp
                                 <option value="{{ $j }}" {{ $when_data->when_to_day==$j ? 'selected':'' }}>{{ $j }}</option>
                                @endfor
                              </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayYear">
                                      <label for="number">Year</label>
                                      <select class="form-control forredline" id="" name="when_to_year">
                                <option value="">YYYY</option>
                              @for ($i = date('Y'); $i >= 1900; $i--)
                                <option value="{{ $i }}" {{ $when_data->when_to_year==$i ? 'selected':'' }}>{{$i}}</option>
                              @endfor
                              </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                  </div>

                                </div>
                                <div class="eventColHeading">Who, What, Where, When, Why</div>
                                <div class="whatForm whereFrom">
                                    <div class="myAllEvents" id="keywords_div">
                        @if($event_data->event_keywords)
                          @php
                            $keywords = explode(',',$event_data->event_keywords);
                          @endphp
                          @foreach($keywords as $i => $keyword)
                                  <div class="addFieldItem keywords" id="{{ $i }}" title="{{ $keyword }}">{{ $keyword }} <span class="removeField" onclick="removeKeyword('{{ $i }}');"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>
                          @endforeach
                        @endif
                                  </div>
                                  <input type="hidden" id="keywords" value="{{ $event_data->event_keywords }}" name="event_keywords">
                              <div class="input-group lifeEvent">
                                <input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
                                <div class="input-group-append">
                                  <a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
                                 </div>
                              </div>
                                    <div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                                </div>

                                @include('inc.life_event_img_upload2',['images' => $images, 'event_data' => $event_data])
                              </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
 <!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Personal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Personal</label>
            <p>It's interesting to look back in time and remember all the things you did and
the people you have met. This section will ask specific contents about "what"
the life event is all about. Have fun, List places you lived and match with
forgotten neighbors, friends’ childhood besties. Cars you’ve owed can match
with details people remember about you etc. keep on thinking… keep on
remembering…</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>This section will ask specific contents about "where" the life event happened</p>
          </div>
          <div class="form-group">
            <label>WHEN:</label>
            <p>This section will work like your calender. You have to enter any day or date you remember. There's two(2) section available - when the event started and when done.</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
@include('inc.footer')
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$("#whatReason").change(function(){
    if($(this).val() == "2") {
        $('.whatReason1').addClass('removedWhat');
        $('.whatReason2').removeClass('removedWhat');
        $('.whatReason3').addClass('removedWhat');
    } else if($(this).val() == "3") {
        $('.whatReason1').addClass('removedWhat');
        $('.whatReason2').addClass('removedWhat');
        $('.whatReason3').removeClass('removedWhat');
    } else {
        $('.whatReason1').removeClass('removedWhat');
        $('.whatReason2').addClass('removedWhat');
        $('.whatReason3').addClass('removedWhat');
    }
});
$(document).on('change','#vehicle_color',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=vehicle_color&column_name=color_name&status_column=color_is_active');
  }
});
$(document).on('change','#gender',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=genders&column_name=gender_name&status_column=gender_is_active');
  }
});
$(document).on('change','#relations',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=relations&column_name=relation_name&status_column=relation_is_active');
  }
});
$(document).on('change','#industry',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=industries&column_name=industry_name&status_column=industry_is_active');
  }
});
$(document).on('change','#marital_status',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=marital_status&column_name=ms_name&status_column=ms_is_active');
  }
});
$(document).on('change','#category',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=occupations&column_name=occupation_name&status_column=occupation_is_active');
  }
});
$(document).on('change','#home_types',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=home_types&column_name=home_type_name&status_column=home_type_is_active');
  }
});
$(document).on('change','#home_styles',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=home_styles&column_name=home_style_name&status_column=home_style_is_active');
  }
});
$(document).on('change','#vehicle_type',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=vehicle_types&column_name=vehicle_type_name&status_column=vehicle_type_is_active');
  }else{
    var makers = $(this).find(':selected').data('makers');
    var vehicle_maker = '<option value="">Select Maker</option>';
    if(makers){
      $.each( makers, function( index, maker ) {
        vehicle_maker += '<option value="'+maker.maker_name+'" data-maker_id="'+maker._maker_id+'">'+maker.maker_name+'</option>';
      });
    }
    vehicle_maker +='<option value="add">Add</option>';
    $('#vehicle_maker').html(vehicle_maker);
  }
});
$(document).on('change', '#vehicle_maker', function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    var vehicle_type = $('#vehicle_type').find(':selected').data('id');
    open_modal('add-master', 'user_id='+user_id+'&db_name=vehicle_makers&column_name=maker_name&status_column=maker_is_active&dependent_name=maker_vehicle_type&dependent_value='+vehicle_type+'&is_dependent=1');
  }else{
    var maker_id = $(this).find(':selected').data('maker_id');
    var user_id = $('#user_id').val();
    console.log('maker_id => '+maker_id);
    execute('api/get-master','user_id='+user_id+'&db_name=vehicle_model&column_name=model_name&status_column=model_is_active& dependent_name=model_maker_id&dependent_value='+maker_id+'&is_dependent=1');
  }
});
$(document).on('change','#vehicle_model',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = $('#user_id').val();
    var maker_id = $('#vehicle_maker').find(':selected').data('maker_id');
    open_modal('add-master','user_id='+user_id+'&db_name=vehicle_model&column_name=model_name&status_column=model_is_active&dependent_name=model_maker_id&dependent_value='+maker_id+'&is_dependent=1');
  }
});
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script>