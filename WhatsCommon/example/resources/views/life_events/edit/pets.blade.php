@include('inc.header')
<div class="innerBodyOnly innerBodyModify">
  <div class="container-fluid">
    <div class="settingBody">
      <div class="position-relative settingFlip">
          <div class="leftSlidePan closePan">
            <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
            <div class="lifeDraftLeftTitle"> {{ $event_data->event_is_draft==0 ? 'LIFE EVENTS' : 'DRAFTS' }} </div>
            <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
              <ul class="list-unstyled mb-0 lifeEventMenu">
                <li class="lifeEventMenuLi lifeDraftLi {{ $event_data->event_is_draft==0 ? 'lifeEventMenuLiActive' : '' }}">
                  <a href="{{ url('life-events') }}" class="lifeEventMenuLink">
                    <span class="lifeEventMenuText">Created Life Events</span>
                    <span class="lifeDraftCount">{{ $event_count }}</span>
                  </a>
                </li>
                <li class="lifeEventMenuLi lifeDraftLi {{ $event_data->event_is_draft==1 ? 'lifeEventMenuLiActive' : '' }}">
                  <a href="{{ url('drafts') }}" class="lifeEventMenuLink lifeDraftMenuLink">
                    <span class="lifeEventMenuText">Drafts</span>
                    <span class="lifeDraftCount">{{ $draft_count }}</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="midPan">
            <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
              <div class="row infoBodyRow">
                <div class="col-md-5 infoBodyCol connectionsBody eventImgCol">
                  <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
                    <div class="innerHome">
                      <a href="{{url('home')}}"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                    </div>
                    <div class="feedbackImg">
                      <div class="personalImgAll">
                        <img src="new-design/img/petsImg.png" class="img-fluid" alt="">
                        <div class="personalImgTitle">Pets</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-7 connectionsBody infoBodyCol eventCol midHightControl">
                  <div class="connectionsBody feedbackRight windowHeight windowHeightMid">

                      <form class="xhr_form" method="post" action="api/save-life-event" id="save-life-event">
                        @csrf
                        <div class="eventColHeading">What </div> <img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                        <div class="whatForm">
                          <div class="whatReasonAll">
                            <div class="form-group datingFilterBtn timeOptionSet">
                              <div class="form-control timeOptionOnly activeTimeOption enterFor">Enter</div>
                              <div class="form-control timeOptionOnly searchFor">Search</div>
                              <input type="hidden" name="search_for" value="0" id="search_for" />
                              <input type="hidden" name="type_id" value="8">
                              <input type="hidden" name="user_id" value="{{Session::get('userdata')['id']}}">
                              <input type="hidden" name="_event_id" value="{{ $event_data->_event_id }}">
                            </div>
                            <div class="form-group">
                              <label for="">Pet Name</label>
                              <input type="text" class="form-control forredline" id="" placeholder="Unknown" name="pet_name" value="{{ $event_data->event_pet_name }}">
                            </div>
                            <label style="margin: 20px 0px 10px;">Pet Info</label>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Species</label>
                                  <select class="form-control forredline formSpecies" id="species" name="pet_species">
                                    <option value="">Select</option>
                    @if($species)
                    	@foreach($species as $s)
                        @php
                          $selected = $s->species_name==$event_data->event_pet_species ? 'selected' : '';
                        @endphp
                    			<option option="{{ $s->species_name }}" {{ $selected }} data-breeds="{{$s->breeds}}">{{$s->species_name}}</option>
                    	@endforeach
                    @endif
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Breed</label>
                                  <select class="form-control forredline formBreed" id="breeds" name="pet_breed" title="Please select a species first">
                                    <option value="">Select</option>
                    @if($breeds)
                      @foreach($breeds as $breed)
                        @php
                          $selected = $breed->breed_name==$event_data->event_pet_breed ? 'selected' : '';
                        @endphp
                          <option option="{{ $breed->breed_name }}" {{ $selected }} >{{$breed->breed_name}}</option>
                      @endforeach
                    @endif
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Gender</label>
                                  <select class="form-control forredline formGender" name="gender">
                                    <option value="">Select</option>
                    @if($genders)
                    	@foreach($genders as $gender)
                        @php
                          $selected = $gender==$event_data->event_gender ? 'selected' : '';
                        @endphp
                    			<option option="{{ $gender }}" {{ $selected }}>{{$gender}}</option>
                    	@endforeach
                    @endif
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Temperment</label>
                                  <select class="form-control forredline formTemperment" name="pet_temperment">
                                    <option value="">Select</option>
                    @if($pet_tempers)
                    	@foreach($pet_tempers as $pt)
                        @php
                          $selected = $pt->pt_name==$event_data->event_pet_temperment ? 'selected' : '';
                        @endphp
                    			<option option="{{ $pt->pt_name }}" {{ $selected }}>{{$pt->pt_name}}</option>
                    	@endforeach
                    @endif
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Color</label>
                                  <select class="form-control forredline formColor" name="pet_color">
                                    <option value="">Select</option>
                    @if($pet_colors)
                    	@foreach($pet_colors as $pc)
                        @php
                          $selected = $pc->pc_name==$event_data->event_pet_color ? 'selected' : '';
                        @endphp
                    			<option option="{{ $pc->pc_name }}" {{ $selected }}>{{$pc->pc_name}}</option>
                    	@endforeach
                    @endif
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Size</label>
                                  <select class="form-control forredline formSize" name="pet_size">
                                    <option value="">Select</option>
                    @if($pet_sizes)
                    	@foreach($pet_sizes as $ps)
                        @php
                          $selected = $ps->ps_name==$event_data->event_pet_size ? 'selected' : '';
                        @endphp
                    			<option option="{{ $ps->ps_name }}" {{ $selected }}>{{$ps->ps_name}}</option>
                    	@endforeach
                    @endif
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">License Number</label>
                                  <input type="text" class="form-control forredline formPlate" id="" placeholder="123abc" name="pet_license_number" value="{{ $event_data->event_pet_license_number }}">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Collar</label>
                                  <select class="form-control forredline formCollar" name="pet_collar">
                                    <option value="">Select</option>
                                    <option value="Yes" {{ $event_data->event_pet_collar=='Yes'?'selected':'' }}>Yes</option>
                                    <option value="No" {{ $event_data->event_pet_collar=='No'?'selected':'' }}>No</option>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                              </div>
                            </div>
                            <label style="margin: 10px 0px 16px;">Pet Info</label>
                            <div class="form-group topFormIcon">
                              <label for="">Veterinarian</label>
                              <input type="text" class="form-control forredline formVeterinarian" id="" placeholder="Surname, First Name, MI" name="pet_veterinary" value="{{ $event_data->event_pet_veterinary }}">
                            </div>
                            <div class="form-group topFormIcon">
                              <label for="">Medical Condition</label>
                              <input type="text" class="form-control forredline formMedical" id="" placeholder="Ex. Blind" name="pet_medical" value="{{ $event_data->event_pet_medical }}">
                            </div>
                            
                          </div>
                        </div>
                        <div class="eventColHeading">Where </div>
                        <div class="whatForm whereFrom">
                          <div class="form-group">
                            <label for="">Country</label>
                            <select class="form-control forredline formCountry" name="country" id="country">
                              <option value="">Select</option>
            @if($countries)
              @foreach($countries as $country)
                @php
                  $selected = $country->country_name==$where_data->where_country ? 'selected' : '';
                @endphp
                              <option value="{{ $country->country_name }}" {{ $selected }} data-id="{{ $country->_country_id }}">{{ $country->country_name }}</option>
              @endforeach
            @endif
                            </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">State/County/Province</label>
                            <select class="form-control forredline formState" name="state" id="province">
                              <option value="">Select</option>
            @if($provinces)
              @foreach($provinces as $province)
                @php
                  $selected = ($where_data && $province->province_name==$where_data->where_state) ? 'selected' : '';
                @endphp
                              <option value="{{ $province->province_name }}" {{ $selected }} data-id="{{ $province->_province_id }}">{{ $province->province_name }}</option>
              @endforeach
            @endif
                            </select>
                            <img src="img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">City</label>
                            <select class="form-control forredline formCity" name="city" id="city">
                              <option value="">Select</option>
            @if($cities)
              @foreach($cities as $city)
                @php
                  $selected = ($where_data && $city->city_name==$where_data->where_city) ? 'selected' : '';
                @endphp
                              <option value="{{ $city->city_name }}" {{ $selected }} data-id="{{ $city->_city_id }}">{{ $city->city_name }}</option>
              @endforeach
            @endif
                            </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">Street Address</label>
                            <input type="text" class="form-control forredline formAddress" id="" placeholder="123 abc Ave." name="street" value="{{ $where_data ? $where_data->where_street : '' }}">
                          </div>
                          <div class="form-group">
                            <label for="">ZIP</label>
                            <input type="text" class="form-control forredline formZip" id="" placeholder="ZIP" name="zip" value="{{ $where_data ? $where_data->where_zip : '' }}">
                          </div>
                        </div>
                        <div class="eventColHeading">Who, What, Where, When, Why </div>
                        <div class="whatForm whereFrom">
                          <div class="myAllEvents" id="keywords_div">
                        @if($event_data->event_keywords)
                          @php
                            $keywords = explode(',',$event_data->event_keywords);
                          @endphp
                          @foreach($keywords as $i => $keyword)
                                  <div class="addFieldItem keywords" id="{{ $i }}" title="{{ $keyword }}">{{ $keyword }} <span class="removeField" onclick="removeKeyword('{{ $i }}');"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>
                          @endforeach
                        @endif
                                  </div>
                          <input type="hidden" id="keywords" value="{{ $event_data->event_keywords }}" name="event_keywords">
	                        <div class="input-group lifeEvent">
	                          	<input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
	                          	<div class="input-group-append">
	                            	<a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
	                           	</div>
	                        </div>
                          	<div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                        </div>
                        
                        @include('inc.life_event_img_upload2',['images' => $images, 'event_data' => $event_data])
                      </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Pets</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Pets</label>
            <p>This one is near and dear to me! It's a two-way search tool; describing and
searching for your pet or other's pet. This is important, entering complete
details about all your pets could help bring them home safely. If a person finds
your pet and enters the details that match yours… PET FOUND! Same applies
with the other tool, you found a pet and you enter their matching detail and…
MATCH MADE!!</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>This can be the location where you lost your pet or found a pet.</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
@include('inc.footer')
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
</script>
<script type="text/javascript" src="js/image-uploader.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  /*$('.photoGallery').imageUploader();
  $('.upload-text').html('<img src="new-design/img/addGalleryImage.png" class="img-fluid photoGalleryImg" alt="">');*/
	$(".enterFor").click(function(){
        $(".enterFor").addClass("activeTimeOption");
        $(".searchFor").removeClass("activeTimeOption");
      $('#search_for').val(0);
    });
    $(".searchFor").click(function(){
        $(".searchFor").addClass("activeTimeOption");
        $(".enterFor").removeClass("activeTimeOption");
        $('#search_for').val(1);
    });
});
$(document).on('change','#species',function(){
	var breeds = $(this).find(':selected').data('breeds');
	/*console.log('breeds => ');
	console.log(breeds);*/
	var pet_breeds = '<option value="">Select </option>';
	if(breeds){

		$.each( breeds, function( index, breed ) {
			pet_breeds += '<option value="'+breed.breed_name+'">'+breed.breed_name+'</option>';
		});
	}

	$('#breeds').html(pet_breeds);
  $('#breeds').prop('title','Select Breed');
});
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script>