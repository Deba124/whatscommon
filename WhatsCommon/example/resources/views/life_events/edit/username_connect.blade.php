@include('inc.header')
<style>
.lifeDraftLeftTitle {
  margin-top: -30px !important;
}
</style>
<div class="innerBodyOnly innerBodyModify">
  <div class="container-fluid">
    <div class="settingBody">
      <div class="position-relative settingFlip">
          <div class="leftSlidePan closePan">
            <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
            <div class="lifeDraftLeftTitle"> {{ $event_data->event_is_draft==0 ? 'LIFE EVENTS' : 'DRAFTS' }} </div>
            <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
              <ul class="list-unstyled mb-0 lifeEventMenu">
                <li class="lifeEventMenuLi lifeDraftLi {{ $event_data->event_is_draft==0 ? 'lifeEventMenuLiActive' : '' }}">
                  <a href="{{ url('life-events') }}" class="lifeEventMenuLink">
                    <span class="lifeEventMenuText">Created Life Events</span>
                    <span class="lifeDraftCount">{{ $event_count }}</span>
                  </a>
                </li>
                <li class="lifeEventMenuLi lifeDraftLi {{ $event_data->event_is_draft==1 ? 'lifeEventMenuLiActive' : '' }}">
                  <a href="{{ url('drafts') }}" class="lifeEventMenuLink lifeDraftMenuLink">
                    <span class="lifeEventMenuText">Drafts</span>
                    <span class="lifeDraftCount">{{ $draft_count }}</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="midPan">
            <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                      <div class="row infoBodyRow">
                        <div class="col-md-5 infoBodyCol connectionsBody eventImgCol">
                          <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
                            <div class="innerHome">
                              <a href="{{ url('home') }}"><img src="img/home.png" class="img-fluid" alt=""></a>
                            </div>
                            <div class="feedbackImg">
                              <div class="personalImgAll">
                                <img src="new-design/img/userConnectImg.png" class="img-fluid" alt="">
                                <div class="personalImgTitle">Username Connect</div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7 connectionsBody infoBodyCol eventCol midHightControl">
                          <div class="connectionsBody feedbackRight windowHeight windowHeightMid">

                            <form class="xhr_form" method="post" action="api/save-life-event" id="save-life-event">
                              @csrf
                              <div class="eventColHeading">What</div>
                                <div class="whatForm">
                                  <div class="form-group datingFilterBtn timeOptionSet">
                                    <div class="form-control timeOptionOnly enterFor {{ $event_data->event_dating_search_for==0 ? 'activeTimeOption' : '' }}">Enter (you)</div>
                                    <div class="form-control timeOptionOnly searchFor {{ $event_data->event_dating_search_for==1 ? 'activeTimeOption' : '' }}">Search (for)</div>
                                    <input type="hidden" name="search_for" value="{{ $event_data->event_dating_search_for }}" id="search_for" />
                                    <input type="hidden" name="type_id" value="11">
                                    <input type="hidden" name="_event_id" value="{{ $event_data->_event_id }}">
                                    <input type="hidden" name="user_id" value="{{Session::get('userdata')['id']}}">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Game or Activity</label>
                                    <div class="selectCenterWithIcon" id="games_div">
                                      <select class="form-control forredline formGame " id="game_name" name="game_name">
                                        <option value="">Select Game or Activity</option>
                            @if($games)
                              @foreach($games as $game)

                                @php
                                  $selected = $event_data->event_game==$game->game_name ? 'selected' : '';
                                @endphp
                                      <option option="{{ $game->game_name }}" data-id="{{ $game->_game_id }}" data-game_teams="{{ $game->game_teams }}" {{ $selected }}>{{$game->game_name}}</option>
                              @endforeach
                            @endif
                                      <option value="add">Add</option>
                                    </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="">Team/Group</label>
                                    <div class="selectCenterWithIcon" id="game_teams_div">
                                      <select class="form-control forredline formTeam " id="team_name" name="team_name">
                                        <option value="">Select Team/Group</option>
                            @if($game_teams)
                              @foreach($game_teams as $team)
                              @php
                                $selected = ($event_data->event_gameteam==$team->team_name) ? 'selected' : '';
                              @endphp
                              <option option="{{ $team->team_name }}" {{ $selected }} data-id="{{ $team->_team_id }}">{{$team->team_name}}</option>
                              @endforeach
                            @endif
                                        <option value="add">Add</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="">Username</label>
                                      <input type="text" class="form-control forredline formUser selectCenterWithIcon2" name="username" value="{{ $event_data->event_username }}">
                                  </div>
                                </div>
                                <div class="eventColHeading">When</div>
                                <div class="whatForm whereFrom">
                                  <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">From</label>
                              <select class="form-control forredline formMonth" id="" name="when_from_month">
                                <option value="">Month</option>
                                <option value="01" {{ $when_data->when_from_month=='01'?'selected':'' }}>January</option>
                                <option value="02" {{ $when_data->when_from_month=='02'?'selected':'' }}>February</option>
                                <option value="03" {{ $when_data->when_from_month=='03'?'selected':'' }}>March</option>
                                <option value="04" {{ $when_data->when_from_month=='04'?'selected':'' }}>April</option>
                                <option value="05" {{ $when_data->when_from_month=='05'?'selected':'' }}>May</option>
                                <option value="06" {{ $when_data->when_from_month=='06'?'selected':'' }}>June</option>
                                <option value="07" {{ $when_data->when_from_month=='07'?'selected':'' }}>July</option>
                                <option value="08" {{ $when_data->when_from_month=='08'?'selected':'' }}>August</option>
                                <option value="09" {{ $when_data->when_from_month=='09'?'selected':'' }}>September</option>
                                <option value="10" {{ $when_data->when_from_month=='10'?'selected':'' }}>October</option>
                                <option value="11" {{ $when_data->when_from_month=='11'?'selected':'' }}>November</option>
                                <option value="12" {{ $when_data->when_from_month=='12'?'selected':'' }}>December</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay">
                              <label for="number">Day</label>
                              <select class="form-control forredline" id="" name="when_from_day">
                                <option value="">DD</option>
                                @for($i=1;$i<=31;$i++)
                                  @php
                                    $j = sprintf('%02d', $i)
                                  @endphp
                                 <option value="{{ $j }}" {{ $when_data->when_from_day==$j ? 'selected':'' }} >{{ $j }}</option>
                                @endfor
                                
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayYear">
                              <label for="number">Year</label>
                              <select class="form-control forredline" id="" name="when_from_year">
                                <option value="">YYYY</option>
                              @for ($i = date('Y'); $i >= 1900; $i--)
                                <option value="{{ $i }}" {{ $when_data->when_from_year==$i ? 'selected':'' }}>{{$i}}</option>
                              @endfor
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                          </div>
                                </div>
                                <div class="eventColHeading">Who, What, Where, When, Why</div>
                                <div class="whatForm whereFrom">
                                  <div class="myAllEvents" id="keywords_div">
                        @if($event_data->event_keywords)
                          @php
                            $keywords = explode(',',$event_data->event_keywords);
                          @endphp
                          @foreach($keywords as $i => $keyword)
                                  <div class="addFieldItem keywords" id="{{ $i }}" title="{{ $keyword }}">{{ $keyword }} <span class="removeField" onclick="removeKeyword('{{ $i }}');"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>
                          @endforeach
                        @endif
                                  </div>
                                  <input type="hidden" id="keywords" value="{{ $event_data->event_keywords }}" name="event_keywords">
                                  <div class="input-group lifeEvent">
                                    <input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
                                    <div class="input-group-append">
                                      <a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
                                     </div>
                                  </div>
                                  <div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                                </div>

                                <div class="whatForm whatFormBtnAll">
  <div class="whatFormBtnSE">
    <input type="hidden" id="is_draft" name="is_draft" value="{{ $event_data->event_is_draft }}">
  @if($event_data->event_is_draft==1)
    <button class="btn whatFormBtn w-100" onclick="$('#is_draft').val(0);">Publish</button>
  @else
    <button class="btn whatFormBtn btnUnpublish w-100" onclick="$('#is_draft').val(1);">Unpublish</button>
  @endif
  </div>

  <div class="btn-Edit-save btn-feedback btn-premium">
    <button class="btn btn-Edit btn-save">what'scommon <br><span>upload your life event</span></button>
  </div>
</div>
                              </form>
                          </div>
                        </div>
                      </div>
                    </div>
          </div>
      </div>
    </div>
  </div>
</div>
@include('inc.footer')
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).ready(function(){
  $("#dep_time_am").click(function(){
      $("#dep_time_am").addClass("activeTimeOption");
      $("#dep_time_pm").removeClass("activeTimeOption");
      $('#dep_time_ampm').val('AM');
  });
  $("#dep_time_pm").click(function(){
    $("#dep_time_pm").addClass("activeTimeOption");
    $("#dep_time_am").removeClass("activeTimeOption");
    $('#dep_time_ampm').val('PM');
  });

  $("#arr_time_am").click(function(){
      $("#arr_time_am").addClass("activeTimeOption");
      $("#arr_time_pm").removeClass("activeTimeOption");
      $('#arr_time_ampm').val('AM');
  });
  $("#arr_time_pm").click(function(){
    $("#arr_time_pm").addClass("activeTimeOption");
    $("#arr_time_am").removeClass("activeTimeOption");
    $('#arr_time_ampm').val('PM');
  });

  $(".enterFor").click(function(){
    $(".enterFor").addClass("activeTimeOption");
    $(".searchFor").removeClass("activeTimeOption");
    $('#search_for').val(0);
  });
  $(".searchFor").click(function(){
    $(".searchFor").addClass("activeTimeOption");
    $(".enterFor").removeClass("activeTimeOption");
    $('#search_for').val(1);
  });

});
$(document).on('change','#game_name',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=games&column_name=game_name&status_column=game_is_active');
  }else{
    var game_teams = $(this).find(':selected').data('game_teams');
    /*console.log('ranks => ');
    console.log(ranks);*/
    var team_name = '<option value="">Select </option>';
    if(game_teams){

      $.each( game_teams, function( index, rank ) {
        team_name += '<option value="'+rank.team_name+'">'+rank.team_name+'</option>';
      });
    }
    team_name += '<option value="add">Add</option>';
    $('#team_name').html(team_name);
  }
  
});
$(document).on('change', '#team_name', function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    var game_id = $('#game_name').find(':selected').data('id');
    open_modal('add-master','user_id='+user_id+'&db_name=game_teams&column_name=team_name&status_column=team_is_active&dependent_name=team_gameid&dependent_value='+game_id+'&is_dependent=1');
  }
});
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
function checkFieldError(){
  $(".forredline").each(function() {
    if($(this).val()==''){
      $(this).addClass('fieldError');
    }else{
      $(this).removeClass('fieldError');
    }
  });
}
$(document).ready(function(){
  checkFieldError();
});
$(document).on('change','.forredline',function(){
  checkFieldError();
});
</script>