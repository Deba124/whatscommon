@include('inc.header')
<div class="innerBodyOnly innerBodyModify">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                  <div class="leftSlidePan closePan">
            <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
            <div class="lifeDraftLeftTitle"> {{ $event_data->event_is_draft==0 ? 'LIFE EVENTS' : 'DRAFTS' }} </div>
            <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
              <ul class="list-unstyled mb-0 lifeEventMenu">
                <li class="lifeEventMenuLi lifeDraftLi {{ $event_data->event_is_draft==0 ? 'lifeEventMenuLiActive' : '' }}">
                  <a href="{{ url('life-events') }}" class="lifeEventMenuLink">
                    <span class="lifeEventMenuText">Created Life Events</span>
                    <span class="lifeDraftCount">{{ $event_count }}</span>
                  </a>
                </li>
                <li class="lifeEventMenuLi lifeDraftLi {{ $event_data->event_is_draft==1 ? 'lifeEventMenuLiActive' : '' }}">
                  <a href="{{ url('drafts') }}" class="lifeEventMenuLink lifeDraftMenuLink">
                    <span class="lifeEventMenuText">Drafts</span>
                    <span class="lifeDraftCount">{{ $draft_count }}</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
                  <div class="midPan">
                    <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                      <div class="row infoBodyRow">
                        <div class="col-md-5 infoBodyCol connectionsBody eventImgCol">
                          <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
                            <div class="innerHome">
                              <a href="{{url('home')}}"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                            </div>
                            <div class="feedbackImg">
                              <div class="personalImgAll">
                                <img src="new-design/img/datingImg.png" class="img-fluid" alt="">
                                <div class="personalImgTitle">Dating</div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7 connectionsBody infoBodyCol eventCol midHightControl">
                          <div class="connectionsBody feedbackRight windowHeight windowHeightMid">


                            <form class="xhr_form" method="post" action="api/save-life-event" id="save-life-event">
                            @csrf
                              <div class="eventColHeading">What</div><img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                              <input type="hidden" name="type_id" value="2">
                              <input type="hidden" name="user_id" value="{{Session::get('userdata')['id']}}">
                              <input type="hidden" name="_event_id" value="{{ $event_data->_event_id }}">
                                <div class="whatForm whatFormDationg">
                                  <div class="form-group datingSpecific">
                                    <label for="">What</label>
                                    <select class="form-control forredline " id="whatReason" name="sub_category_id">
                    @if($what)
                    	@foreach($what as $w)
                        @php
                          $selected = $w->id==$event_data->event_subcategory_id ? 'selected' : '';
                        @endphp
                                      <option value="{{ $w->id }}" {{ $selected }}>{{ $w->sub_cat_type_name }}</option>
                        @endforeach
                    @endif
                            
                                    </select>
                                  </div>
                                <div class="form-group datingFilterBtn timeOptionSet">
                                  <div class="form-control timeOptionOnly enterFor {{ $event_data->event_dating_search_for==0 ? 'activeTimeOption' : '' }}">Enter (you)</div>
                                  <div class="form-control timeOptionOnly searchFor {{ $event_data->event_dating_search_for==1 ? 'activeTimeOption' : '' }}">Search (for)</div>
                                  <input type="hidden" name="search_for" value="{{ $event_data->event_dating_search_for }}" id="search_for" />
                                </div>
                                  <div class="form-group text-center" style="color: #3b71b9;border: 2px solid #3b71b9;background: #f8f8f8;">
                                    Enter as MUCH or as little as you want. Enter <br>what you want others to search about you!
                                  </div>
                                  <div class=" vehicleForm" style="padding: 0px 30px;">
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Gender</label>
                                          <select class="form-control forredline formGender" name="gender">
                                            <option value="">Select</option>
                    @if($genders)
                    	@foreach($genders as $gender)
                        @php
                          $selected = $gender->gender_name==$event_data->event_gender ? 'selected' : '';
                        @endphp
                    		<option option="{{ $gender->gender_name }}" {{ $selected }}>{{$gender->gender_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Age</label>
                                          <!-- <input type="number" class="form-control formAge" name="age"> -->
                                          <select class="form-control forredline formAge" name="age">
                                            <option value=""="">Select</option>
                                @for($i=18;$i<100;$i++)
                                  @php
                                    $selected = $i==$event_data->event_age ? 'selected' : '';
                                  @endphp
                                            <option value="{{ $i }}" {{ $selected }}>{{ $i }}{{$i==18?'+':''}}</option>
                                @endfor
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      	<div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Height</label>
                                            <div class="height_weight">
                                              <div class="height_weight_Only">
                                              	<input type="number" name="" class="form-control forredline formHeight" name="height" id="height" value="{{ $event_data->event_height ? $event_data->event_height : '' }}">
                                              </div>
                                              <div class="form-group timeOptionSet">
                                                <div class="form-control timeOptionOnly {{ $event_data->event_height_measure=='in' ? 'activeTimeOption' : '' }} " id="height_measure_in">in</div>
                                                <div class="form-control timeOptionOnly {{ $event_data->event_height_measure=='cm' ? 'activeTimeOption' : '' }}" id="height_measure_cm" style="border-top-left-radius: 0 !important;border-bottom-left-radius: 0 !important;">cm</div>
                                                <input type="hidden" name="height_measure" id="height_measure" value="{{ $event_data->event_height_measure}}">
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Height General</label>
                                            <select class="form-control forredline formHeight" name="height_general">
                                              <option value="">Select</option>
                	@if($height_general)
                    	@foreach($height_general as $hg)
                        @php
                          $selected = $hg->hg_name==$event_data->event_height_general ? 'selected' : '';
                        @endphp
                    					<option option="{{ $hg->hg_name }}" {{ $selected }}>{{$hg->hg_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Weight</label>
                                            <div class="height_weight">
                                              <div class="height_weight_Only">
                                              	<input type="number" class="form-control forredline formWeight" name="weight" id="weight" value="{{ $event_data->event_weight }}">
                                              </div>
                                              <div class="form-group timeOptionSet">
                                                <div class="form-control timeOptionOnly {{ $event_data->event_weight_measure=='kg' ? 'activeTimeOption' : '' }}" id="weight_measure_kg">kg</div>
                                                <div class="form-control timeOptionOnly {{ $event_data->event_weight_measure=='lbs' ? 'activeTimeOption' : '' }}" id="weight_measure_lbs" style="border-top-left-radius: 0 !important;border-bottom-left-radius: 0 !important;">lbs</div>
                                                <input type="hidden" name="weight_measure" id="weight_measure" value="{{ $event_data->event_weight_measure }}">
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Weight General</label>
                                            <select class="form-control forredline formWeight" name="weight_general">
                                              <option value="">Select</option>
                    @if($weight_general)
                    	@foreach($weight_general as $wg)
                        @php
                          $selected = $wg->wg_name==$event_data->event_weight_general ? 'selected' : '';
                        @endphp
                    					<option option="{{ $wg->wg_name }}" {{ $selected }}>{{$wg->wg_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Ethnicity</label>
                                          <select class="form-control forredline formEthnicity" name="ethnicity">
                                            <option value="">Select</option>
                    @if($ethnicities)
                    	@foreach($ethnicities as $ethnicity)
                        @php
                          $selected = $ethnicity->ethnicity_name==$event_data->event_ethnicity ? 'selected' : '';
                        @endphp
                    					<option option="{{ $ethnicity->ethnicity_name }}" {{ $selected }}>{{$ethnicity->ethnicity_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Race</label>
                                          <select class="form-control forredline formRace" name="race">
                                            <option value="">Select</option>
                    @if($races)
                    	@foreach($races as $race)
                        @php
                          $selected = $race->race_name==$event_data->event_race ? 'selected' : '';
                        @endphp
                    					<option option="{{ $race->race_name }}" {{ $selected }}>{{$race->race_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Nationality</label>
                                          <select class="form-control forredline formNationality" name="nationality">
                                            <option value="">Select</option>
                    @if($nationalities)
                    	@foreach($nationalities as $country)
                        @php
                          $selected = $country->country_name==$event_data->event_nationality ? 'selected' : '';
                        @endphp
                    					<option option="{{ $country->country_name }}" {{ $selected }}>{{$country->country_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Facial Hair</label>
                                            <select class="form-control forredline formFacialHair" name="facial_hair">
                                              <option value="">Select</option>
                    @if($facial_hairs)
                    	@foreach($facial_hairs as $fh)
                        @php
                          $selected = $fh->fh_name==$event_data->event_facial_hair ? 'selected' : '';
                        @endphp
                    					<option option="{{ $fh->fh_name }}" {{ $selected }}>{{$fh->fh_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Orientation</label>
                                          <select class="form-control forredline formOrientation" name="orientation">
                                            <option value="">Select</option>
                    @if($orientations)
                    	@foreach($orientations as $orientation)
                        @php
                          $selected = $orientation->orientation_name==$event_data->event_orientation ? 'selected' : '';
                        @endphp
                    					<option option="{{ $orientation->orientation_name }}" {{ $selected }}>{{$orientation->orientation_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Body Style</label>
                                          <select class="form-control forredline formBodyStyle" name="body_style">
                                            <option value="">Select</option>
                    @if($body_styles)
                    	@foreach($body_styles as $bs)
                        @php
                          $selected = $bs->bs_name==$event_data->event_body_style ? 'selected' : '';
                        @endphp
                    					<option option="{{ $bs->bs_name }}" {{ $selected }}>{{$bs->bs_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Body Hair</label>
                                            <select class="form-control forredline formBodyHair" name="body_hair">
                                              <option value="">Select</option>
                    @if($body_hairs)
                    	@foreach($body_hairs as $bh)
                        @php
                          $selected = $bh->bh_name==$event_data->event_body_hair ? 'selected' : '';
                        @endphp
                    					<option option="{{ $bh->bh_name }}" {{ $selected }}>{{$bh->bh_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Skin Tone</label>
                                          <select class="form-control forredline formSkinTone" name="skin_tonne">
                                            <option value="">Select</option>
                    @if($skin_tonnes)
                    	@foreach($skin_tonnes as $st)
                        @php
                          $selected = $st->st_name==$event_data->event_skintone ? 'selected' : '';
                        @endphp
                    					<option option="{{ $st->st_name }}" {{ $selected }}>{{$st->st_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Glasses</label>
                                            <select class="form-control forredline formSGlasses" name="glasses">
                                              <option value="">Select</option>
                                              <option value="Yes" {{ $event_data->event_glasses=='Yes' ? 'selected' : '' }}>Yes</option>
                                              <option value="No" {{ $event_data->event_glasses=='No' ? 'selected' : '' }}>No</option>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Hair Color</label>
                                          <select class="form-control forredline formHairColor" name="hair_color">
                                            <option value="">Select</option>
                    @if($hair_colors)
                    	@foreach($hair_colors as $hc)
                        @php
                          $selected = $hc->hc_name==$event_data->event_hair_color ? 'selected' : '';
                        @endphp
                    					<option option="{{ $hc->hc_name }}" {{ $selected }}>{{$hc->hc_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Eye Color</label>
                                          <select class="form-control forredline formEyeColor" name="eye_color">
                                            <option value="">Select</option>
                    @if($eye_colors)
                    	@foreach($eye_colors as $ec)
                        @php
                          $selected = $ec->ec_name==$event_data->event_eye_color ? 'selected' : '';
                        @endphp
                    					<option option="{{ $ec->ec_name }}" {{ $selected }}>{{$ec->ec_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Party</label>
                                            <select class="form-control forredline formParty" name="party">
                                              <option value="">Select</option>
                                              <option value="Yes" {{ $event_data->event_party=='Yes' ? 'selected' : '' }}>Yes</option>
                                              <option value="No" {{ $event_data->event_party=='No' ? 'selected' : '' }}>No</option>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}" name="smoke">
                                          <div class="form-group">
                                            <label for="">Smoke</label>
                                            <select class="form-control forredline formSmoke">
                                              <option value="">Select</option>
                                              <option value="Yes" {{ $event_data->event_smoke=='Yes' ? 'selected' : '' }}>Yes</option>
                                              <option value="No" {{ $event_data->event_smoke=='No' ? 'selected' : '' }}>No</option>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Drink</label>
                                            <select class="form-control forredline formDrink" name="drink">
                                              <option value="">Select</option>
                                              <option value="Yes" {{ $event_data->event_drink=='Yes' ? 'selected' : '' }}>Yes</option>
                                              <option value="No" {{ $event_data->event_drink=='No' ? 'selected' : '' }}>No</option>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Tattoo</label>
                                            <select class="form-control forredline formTatto" name="tattoo">
                                              <option value="">Select</option>
                                              <option value="Yes" {{ $event_data->event_tattoos=='Yes' ? 'selected' : '' }}>Yes</option>
                                              <option value="No" {{ $event_data->event_tattoos=='No' ? 'selected' : '' }}>No</option>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for=""># Tattoos</label>
                                            <select class="form-control forredline formTatto" name="tattoo_type">
                                              <option value="">Select</option>
                    @if($tattoos)
                    	@foreach($tattoos as $tattoo)
                        @php
                          $selected = $tattoo->tattoo_name==$event_data->event_tattoo_type ? 'selected' : '';
                        @endphp
                    					<option option="{{ $tattoo->tattoo_name }}" {{ $selected }}>{{$tattoo->tattoo_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Favorite Food</label>
                                            <select class="form-control forredline formFood" name="fav_food">
                                              <option value="">Select</option>
                    @if($foods)
                    	@foreach($foods as $food)
                        @php
                          $selected = $food->food_name==$event_data->event_fav_food ? 'selected' : '';
                        @endphp
                    					<option option="{{ $food->food_name }}" {{ $selected }}>{{$food->food_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Food Type</label>
                                            <select class="form-control forredline formFoodType" name="food_type">
                                              <option value="">Select</option>
                    @if($food_types)
                    	@foreach($food_types as $ft)
                        @php
                          $selected = $ft->ft_name==$event_data->event_food_type ? 'selected' : '';
                        @endphp
                    					<option option="{{ $ft->ft_name }}" {{ $selected }}>{{$ft->ft_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Sport</label>
                                            <select class="form-control forredline formSport" name="sport">
                                              <option value="">Select</option>
                    @if($sports)
                    	@foreach($sports as $sport)
                        @php
                          $selected = $sport->sport_name==$event_data->event_sports ? 'selected' : '';
                        @endphp
                    					<option option="{{ $sport->sport_name }}" {{ $selected }}>{{$sport->sport_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Favorite Team</label>
                                            <input type="text" class="form-control forredline formFavoriteTeam" name="fav_team" value="{{ $event_data->event_fav_team }}">
                                            
                                          </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Political Affiliation</label>
                                          <select class="form-control forredline formPoliticalAffiliation" name="political">
                                            <option value="">Select</option>
                    @if($politicals)
                    	@foreach($politicals as $political)
                        @php
                          $selected = $political->political_name==$event_data->event_political ? 'selected' : '';
                        @endphp
                    					<option option="{{ $political->political_name }}" {{ $selected }}>{{$political->political_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Religion</label>
                                          <select class="form-control forredline formReligion" name="religion">
                                            <option value="">Select</option>
                    @if($religions)
                    	@foreach($religions as $religion)
                        @php
                          $selected = $religion->religion_name==$event_data->event_religion ? 'selected' : '';
                        @endphp
                    					<option option="{{ $religion->religion_name }}" {{ $selected }}>{{$religion->religion_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Zodiac Sign</label>
                                            <select class="form-control forredline formZodiacSign" name="zodiac_sign">
                                              <option value="">Select</option>
                    @if($zodiac_signs)
                    	@foreach($zodiac_signs as $zodiac)
                        @php
                          $selected = $zodiac->zodiac_name==$event_data->event_zodiac ? 'selected' : '';
                        @endphp
                    					<option option="{{ $zodiac->zodiac_name }}" {{ $selected }}>{{$zodiac->zodiac_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Birth Stone</label>
                                            <select class="form-control forredline formBarthStone" name="birth_stone">
                                              <option value="">Select</option>
                    @if($zodiac_signs)
                    	@foreach($zodiac_signs as $zodiac)
                        @php
                          $selected = $zodiac->zodiac_name==$event_data->event_birth_stone ? 'selected' : '';
                        @endphp
                    					<option option="{{ $zodiac->zodiac_name }}" {{ $selected }}>{{$zodiac->zodiac_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Affiliations</label>
                                            <select class="form-control forredline formAffiliation" name="affiliation">
                                              <option value="">Select</option>
                    @if($affiliations)
                    	@foreach($affiliations as $affiliation)
                        @php
                          $selected = $affiliation->affiliation_name==$event_data->event_affiliations ? 'selected' : '';
                        @endphp
                    					<option option="{{ $affiliation->affiliation_name }}" {{ $selected }}>{{$affiliation->affiliation_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Career</label>
                                            <select class="form-control forredline formCareer" name="career">
                                              <option value="">Select</option>
                    @if($occupations)
                    	@foreach($occupations as $occupation)
                        @php
                          $selected = $occupation->occupation_name==$event_data->event_career ? 'selected' : '';
                        @endphp
                    					<option option="{{ $occupation->occupation_name }}" {{ $selected }}>{{$occupation->occupation_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Financial</label>
                                            <select class="form-control forredline formFinancial" name="financial">
                                              <option value="">Select</option>
                    @if($financials)
                    	@foreach($financials as $fs)
                        @php
                          $selected = $fs->fs_name==$event_data->event_financial ? 'selected' : '';
                        @endphp
                    					<option option="{{ $fs->fs_name }}" {{ $selected }}>{{$fs->fs_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 {{ $event_data->event_subcategory_id==5 ? '' : 'removedWhat' }}">
                                          <div class="form-group">
                                            <label for="">Relationship</label>
                                            <select class="form-control forredline formRelationship" name="relationship">
                                              <option value="">Select</option>
                    @if($relationships)
                    	@foreach($relationships as $relationship)
                        @php
                          $selected = $relationship==$event_data->event_family_relation ? 'selected' : '';
                        @endphp
                    					<option option="{{ $relationship }}" {{ $selected }}>{{$relationship}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Age (Range)</label>
                                          <select class="form-control forredline formAge" name="age_range">
                                            <option value="">Select</option>
                    @if($age_ranges)
                    	@foreach($age_ranges as $range)
                        @php
                          $selected = $range->range_name==$event_data->event_age_range ? 'selected' : '';
                        @endphp
                    					<option option="{{ $range->range_name }}" {{ $selected }}>{{$range->range_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Loking For</label>
                                          <select class="form-control forredline formGender" name="looking_for">
                                            <option value="">Select</option>
                    @if($genders)
                    	@foreach($genders as $gender)
                        @php
                          $selected = $gender->gender_name==$event_data->event_looking_for ? 'selected' : '';
                        @endphp
                    					<option option="{{ $gender->gender_name }}" {{ $selected }}>{{$gender->gender_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="interestsSet">

                                          <div class="form-group interestsPicker">
                                            <label for="">Interests</label>
                                            <select class="form-control forredline" name="basic" id="ex-multiselect" multiple name="interests">
                                              <option>Select</option>
                    @if($interests)
                    	@foreach($interests as $interest)
                        @php
                          $selected = $interest->interest_name==$event_data->event_interest ? 'selected' : '';
                        @endphp
                    					<option option="{{ $interest->interest_name }}" {{ $selected }}>{{$interest->interest_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="eventColHeading">Where </div>
                                <div class="whatForm whereFrom">
                                  <div class="form-group">
                                    <label for="">Country</label>
                                    <select class="form-control forredline formCountry" name="country" id="country">
                              <option value="">Select</option>
            @if($countries)
              @foreach($countries as $country)
                @php
                  $selected = $country->country_name==$where_data->where_country ? 'selected' : '';
                @endphp
                              <option value="{{ $country->country_name }}" {{ $selected }} data-id="{{ $country->_country_id }}">{{ $country->country_name }}</option>
              @endforeach
            @endif
                              {{-- <option>1</option>
                              <option>2</option> --}}
                            </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">State/County/Province</label>
                                    <select class="form-control forredline formState" name="state" id="province">
                                      <option value="">Select</option>
            @if($provinces)
              @foreach($provinces as $province)
                @php
                  $selected = ($where_data && $province->province_name==$where_data->where_state) ? 'selected' : '';
                @endphp
                              <option value="{{ $province->province_name }}" {{ $selected }} data-id="{{ $province->_province_id }}">{{ $province->province_name }}</option>
              @endforeach
            @endif
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">City</label>
                                    <select class="form-control forredline formCity" name="city" id="city">
                                      <option value="">Select</option>
            @if($cities)
              @foreach($cities as $city)
                @php
                  $selected = ($where_data && $city->city_name==$where_data->where_city) ? 'selected' : '';
                @endphp
                              <option value="{{ $city->city_name }}" {{ $selected }} data-id="{{ $city->_city_id }}">{{ $city->city_name }}</option>
              @endforeach
            @endif
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Radius (City)</label>
                                    {{-- <input type="text" class="form-control forredline formAddress" id="" placeholder="Radius" name=""> --}}
                                    <select class="form-control forredline formAddress" id="" name="radius">
                                        <option value=""="">Select</option>
                  @if($radius)
                      @foreach($radius as $r)
                        @php
                          $selected = $r->radius_value==$where_data->where_radius ? 'selected' : '';
                        @endphp
                              <option value="{{ $r->radius_value }}" {{ $selected }}>{{$r->radius_value}}</option>
                      @endforeach
                    @endif
                                      </select>
                                  </div>
                                </div>
                                <div class="eventColHeading">When </div>
                                <div class="whatForm whereFrom">
                                  <div class="whenTitleText">
                                    <div class="whenTitle">Availability</div>
                                    <div class="whenText">Let people know when ready to meet</div>
                                  </div>
                                  <div class="birthdayDate timeframeSet">
                                    <div class="form-group birthdayMonth">
                                      <label for="number">Timeframe</label>
                                      <select class="form-control forredline" id="" name="timeframe">
                                        <option value="">Select</option>
                	@if($availabilities)
                    	@foreach($availabilities as $availability)
                        @php
                          $selected = $availability->availability_name==$when_data->when_timeframe ? 'selected' : '';
                        @endphp
                    					<option option="{{ $availability->availability_name }}" {{ $selected }}>{{$availability->availability_name}}</option>
                    	@endforeach
                    @endif
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayYear">
                                      <label for="number">&nbsp;</label>
                                      <select class="form-control forredline" id="" name="time_month">
                                        <option value="">Select</option>
                  <option value="January" {{ $when_data->when_from_month=='January'?'selected':'' }}>January</option>
                  <option value="February" {{ $when_data->when_from_month=='February'?'selected':'' }}>February</option>
                  <option value="March" {{ $when_data->when_from_month=='March'?'selected':'' }}>March</option>
                  <option value="April" {{ $when_data->when_from_month=='April'?'selected':'' }}>April</option>
                  <option value="May" {{ $when_data->when_from_month=='May'?'selected':'' }}>May</option>
                  <option value="June" {{ $when_data->when_from_month=='June'?'selected':'' }}>June</option>
                  <option value="July" {{ $when_data->when_from_month=='July'?'selected':'' }}>July</option>
                  <option value="August" {{ $when_data->when_from_month=='August'?'selected':'' }}>August</option>
                  <option value="September" {{ $when_data->when_from_month=='September'?'selected':'' }}>September</option>
                  <option value="October" {{ $when_data->when_from_month=='October'?'selected':'' }}>October</option>
                  <option value="November" {{ $when_data->when_from_month=='November'?'selected':'' }}>November</option>
                  <option value="December" {{ $when_data->when_from_month=='December'?'selected':'' }}>December</option>
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                  </div>
                                </div>
                                <div class="eventColHeading">Who, What, Where, When, Why </div>
                                <div class="whatForm whereFrom">
                            
                                  	<div class="myAllEvents" id="keywords_div">
                        @if($event_data->event_keywords)
                          @php
                            $keywords = explode(',',$event_data->event_keywords);
                          @endphp
                          @foreach($keywords as $i => $keyword)
                                  <div class="addFieldItem keywords" id="{{ $i }}" title="{{ $keyword }}">{{ $keyword }} <span class="removeField" onclick="removeKeyword('{{ $i }}');"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>
                          @endforeach
                        @endif
                                  </div>
                                  <input type="hidden" id="keywords" value="{{ $event_data->event_keywords }}" name="event_keywords">
			                        <div class="input-group lifeEvent">
			                          <input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
			                          <div class="input-group-append">
			                            <a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
			                           </div>
			                        </div>
                                  	<div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                                </div>
                                @include('inc.life_event_img_upload2',['images' => $images, 'event_data' => $event_data])
                              </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
 <!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Dating</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Dating</label>
            <p>This section will ask specific categories for your dating preference. Dating life event has two(2) options: either enter your details for others to search for you or search for someone by entering specific information you like.</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>This is where your dating findings will start. Just enter the specific location, and everything will follow. Match-making will happen when matched with other users using your preferred radius.</p>
          </div>
          <div class="form-group">
            <label>WHEN:</label>
            <p>Availability is essential to start building and working with your relationship. Just enter your availability timeframe/month.</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
@include('inc.footer')
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).ready(function(){
	$(".enterFor").click(function(){
        $(".enterFor").addClass("activeTimeOption");
        $(".searchFor").removeClass("activeTimeOption");
        $('#search_for').val(0);
    });
    $(".searchFor").click(function(){
        $(".searchFor").addClass("activeTimeOption");
        $(".enterFor").removeClass("activeTimeOption");
        $('#search_for').val(1);
    });
});
$(document).ready(function(){
  $("#weight_measure_kg").click(function(){
    $("#weight_measure_kg").addClass("activeTimeOption");
    $("#weight_measure_lbs").removeClass("activeTimeOption");
    $('#weight_measure').val('kg');
    var weight = $('#weight').val();
    var changed_weight = (weight*0.453592).toFixed(2);
    $('#weight').val(changed_weight);
  });
  $("#weight_measure_lbs").click(function(){
    $("#weight_measure_lbs").addClass("activeTimeOption");
    $("#weight_measure_kg").removeClass("activeTimeOption");
    $('#weight_measure').val('lbs');
    var weight = $('#weight').val();
    var changed_weight = (weight*2.20462).toFixed(2);
    $('#weight').val(changed_weight);
  });
  $("#height_measure_in").click(function(){
    $("#height_measure_in").addClass("activeTimeOption");
    $("#height_measure_cm").removeClass("activeTimeOption");
    $('#height_measure').val('in');
    var height = $('#height').val();
    var changed_height = (height*0.393701).toFixed(2);
    $('#height').val(changed_height);
  });
  $("#height_measure_cm").click(function(){
    $("#height_measure_cm").addClass("activeTimeOption");
    $("#height_measure_in").removeClass("activeTimeOption");
    $('#height_measure').val('cm');
    var height = $('#height').val();
    var changed_height = (height*2.54).toFixed(2);
    $('#height').val(changed_height);
  });
});

$("#whatReason").change(function(){
    if($(this).val() == "5") {
        $('.whatReason1').addClass('removedWhat');
        $('.whatReason2').removeClass('removedWhat');
    } else {
        $('.whatReason2').addClass('removedWhat');
        $('.whatReason1').removeClass('removedWhat');
     }
});
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script>