@include('inc.header')
<div class="innerBodyOnly innerBodyModify">
  <div class="container-fluid">
    <div class="settingBody">
      <div class="position-relative settingFlip">
          <div class="leftSlidePan closePan">
            <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
            <div class="lifeDraftLeftTitle"> {{ $event_data->event_is_draft==0 ? 'LIFE EVENTS' : 'DRAFTS' }} </div>
            <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
              <ul class="list-unstyled mb-0 lifeEventMenu">
                <li class="lifeEventMenuLi lifeDraftLi {{ $event_data->event_is_draft==0 ? 'lifeEventMenuLiActive' : '' }}">
                  <a href="{{ url('life-events') }}" class="lifeEventMenuLink">
                    <span class="lifeEventMenuText">Created Life Events</span>
                    <span class="lifeDraftCount">{{ $event_count }}</span>
                  </a>
                </li>
                <li class="lifeEventMenuLi lifeDraftLi {{ $event_data->event_is_draft==1 ? 'lifeEventMenuLiActive' : '' }}">
                  <a href="{{ url('drafts') }}" class="lifeEventMenuLink lifeDraftMenuLink">
                    <span class="lifeEventMenuText">Drafts</span>
                    <span class="lifeDraftCount">{{ $draft_count }}</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="midPan">
            <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
              <div class="row infoBodyRow">
                <div class="col-md-5 infoBodyCol connectionsBody eventImgCol">
                  <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
                    <div class="innerHome">
                      <a href="{{ url('home') }}"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                    </div>
                    <div class="feedbackImg">
                      <div class="personalImgAll">
                        <img src="new-design/img/travelImg.png" class="img-fluid" alt="">
                        <div class="personalImgTitle">Travel</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-7 connectionsBody infoBodyCol eventCol midHightControl">
                          <div class="connectionsBody feedbackRight windowHeight windowHeightMid">


                      <form class="xhr_form" method="post" action="api/save-life-event" id="save-life-event">
                            @csrf
                        <div class="eventColHeading">What </div> <img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                          <input type="hidden" name="type_id" value="4">
                          <input type="hidden" name="user_id" value="{{Session::get('userdata')['id']}}">
                          <input type="hidden" name="_event_id" value="{{ $event_data->_event_id }}">
                        <div class="whatForm">
                          <div class="form-group">
                            <label for="">Type</label>
                            <select class="form-control forredline" name="sub_category_id">
            		@if($what)
                  @foreach($what as $w)
                      @php
                        $selected = $w->id==$event_data->event_subcategory_id ? 'selected' : '';
                      @endphp
                              <option value="{{ $w->id }}" {{ $selected }}>{{ $w->sub_cat_type_name }}</option>
                  @endforeach
                @endif
                            </select>
                          </div>
                          <div class="whatReasonAll">
                            <div class="form-group topFormIcon">
                              <label for="">Mode</label>
                              
                              <select class="form-control forredline formAirport" name="airport">
                                <option value="">Select</option>
                  @if($travel_modes)
                    @foreach($travel_modes as $mode)
                      @php
                        $selected = $mode->mode_name==$event_data->event_airport ? 'selected' : '';
                      @endphp
                          <option option="{{ $mode->mode_name }}" {{ $selected }}>{{$mode->mode_name}}</option>
                    @endforeach
                  @endif
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Name</label>
                                  <input type="text" class="form-control forredline formAirline" id="" placeholder="Type here" name="airline" value="{{ $event_data->event_airline }}">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Number</label>
                                  <input type="text" class="form-control forredline formFlightNo" id="" placeholder="Type here" name="flight_number" value="{{ $event_data->event_flight_number }}">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Row</label>
                                  <input type="text" class="form-control forredline formRow" id="" placeholder="Type here" name="row" value="{{ $event_data->event_flight_row }}">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group topFormIcon">
                                  <label for="">Seat</label>
                                  <input type="text" class="form-control forredline formSeat" id="" placeholder="Type here" name="seat" value="{{ $event_data->event_flight_seat }}">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="eventColHeading">Where </div>
                        <div class="whatForm whereFrom">
                          <div class="form-group">
                            <label for="">Location Type</label>
                            <select class="form-control forredline formLocationType" name="location_type">
                              <option value="">Select</option>
                	@if($location_types)
                    	@foreach($location_types as $lt)
                      @php
                        $selected = ($where_data && $lt->lt_name==$where_data->where_location_type) ? 'selected' : '';
                      @endphp
                    			<option option="{{ $lt->lt_name }}" {{ $selected }}>{{$lt->lt_name}}</option>
                    	@endforeach
                    @endif
                            </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">Location Name</label>
                            <input type="text" class="form-control forredline formLocation" id="" placeholder="Type here" name="location_name" value="{{ $where_data->where_hotel_name }}">
                          </div>
                          <div class="form-group">
                            <label for="">Country</label>
                            <select class="form-control forredline formCountry" name="country" id="country">
                              <option value="">Select</option>
            @if($countries)
              @foreach($countries as $country)
                @php
                  $selected = $country->country_name==$where_data->where_country ? 'selected' : '';
                @endphp
                              <option value="{{ $country->country_name }}" {{ $selected }} data-id="{{ $country->_country_id }}">{{ $country->country_name }}</option>
              @endforeach
            @endif
                              
                            </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">State/County/Province</label>
                            <select class="form-control forredline formState" name="state" id="province">
                                      <option value="">Select</option>
            @if($provinces)
              @foreach($provinces as $province)
                @php
                  $selected = ($where_data && $province->province_name==$where_data->where_state) ? 'selected' : '';
                @endphp
                              <option value="{{ $province->province_name }}" {{ $selected }} data-id="{{ $province->_province_id }}">{{ $province->province_name }}</option>
              @endforeach
            @endif
                                    </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">City</label>
                            <select class="form-control forredline formCity" name="city" id="city">
                                      <option value="">Select</option>
            @if($cities)
              @foreach($cities as $city)
                @php
                  $selected = ($where_data && $city->city_name==$where_data->where_city) ? 'selected' : '';
                @endphp
                              <option value="{{ $city->city_name }}" {{ $selected }} data-id="{{ $city->_city_id }}">{{ $city->city_name }}</option>
              @endforeach
            @endif
                                    </select>
                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                          </div>
                          <div class="form-group">
                            <label for="">Street Address</label>
                            <input type="text" class="form-control forredline formAddress" id="" placeholder="123 abc Ave." name="street" value="{{ $where_data ? $where_data->where_street : '' }}">
                          </div>
                          <div class="form-group">
                            <label for="">ZIP</label>
                            <input type="text" class="form-control forredline formZip" id="" placeholder="ZIP" name="zip" value="{{ $where_data ? $where_data->where_zip : '' }}">
                          </div>
                        </div>
                        <div class="eventColHeading">When </div>
                        <div class="whatForm whereFrom">
                          <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">From</label>
                              <select class="form-control forredline formMonth" id="" name="when_from_month">
                                <option value="">Month</option>
                                <option value="01" {{ $when_data->when_from_month=='01'?'selected':'' }}>January</option>
                                <option value="02" {{ $when_data->when_from_month=='02'?'selected':'' }}>February</option>
                                <option value="03" {{ $when_data->when_from_month=='03'?'selected':'' }}>March</option>
                                <option value="04" {{ $when_data->when_from_month=='04'?'selected':'' }}>April</option>
                                <option value="05" {{ $when_data->when_from_month=='05'?'selected':'' }}>May</option>
                                <option value="06" {{ $when_data->when_from_month=='06'?'selected':'' }}>June</option>
                                <option value="07" {{ $when_data->when_from_month=='07'?'selected':'' }}>July</option>
                                <option value="08" {{ $when_data->when_from_month=='08'?'selected':'' }}>August</option>
                                <option value="09" {{ $when_data->when_from_month=='09'?'selected':'' }}>September</option>
                                <option value="10" {{ $when_data->when_from_month=='10'?'selected':'' }}>October</option>
                                <option value="11" {{ $when_data->when_from_month=='11'?'selected':'' }}>November</option>
                                <option value="12" {{ $when_data->when_from_month=='12'?'selected':'' }}>December</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay">
                              <label for="number">Day</label>
                              <select class="form-control forredline" id="" name="when_from_day">
                                <option value="">DD</option>
                              @for($i=1;$i<=31;$i++)
                                @php
                                  $j = sprintf('%02d', $i)
                                @endphp
                               <option value="{{ $j }}" {{ $when_data->when_from_day==$j ? 'selected':'' }}>{{ $j }}</option>
                              @endfor
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayYear">
                              <label for="number">Year</label>
                              <select class="form-control forredline" id="" name="when_from_year">
                                <option value="">YYYY</option>
                              @for ($i = date('Y'); $i >= 1900; $i--)
                                <option value="{{ $i }}" {{ $when_data->when_from_year==$i ? 'selected':'' }}>{{$i}}</option>
                              @endfor
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                          </div>
                          <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">To</label>
                              <select class="form-control forredline formMonth" id="" name="when_to_month">
                                <option value="">Month</option>
                                <option value="01" {{ $when_data->when_to_month=='01'?'selected':'' }}>January</option>
                                <option value="02" {{ $when_data->when_to_month=='02'?'selected':'' }}>February</option>
                                <option value="03" {{ $when_data->when_to_month=='03'?'selected':'' }}>March</option>
                                <option value="04" {{ $when_data->when_to_month=='04'?'selected':'' }}>April</option>
                                <option value="05" {{ $when_data->when_to_month=='05'?'selected':'' }}>May</option>
                                <option value="06" {{ $when_data->when_to_month=='06'?'selected':'' }}>June</option>
                                <option value="07" {{ $when_data->when_to_month=='07'?'selected':'' }}>July</option>
                                <option value="08" {{ $when_data->when_to_month=='08'?'selected':'' }}>August</option>
                                <option value="09" {{ $when_data->when_to_month=='09'?'selected':'' }}>September</option>
                                <option value="10" {{ $when_data->when_to_month=='10'?'selected':'' }}>October</option>
                                <option value="11" {{ $when_data->when_to_month=='11'?'selected':'' }}>November</option>
                                <option value="12" {{ $when_data->when_to_month=='12'?'selected':'' }}>December</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay">
                              <label for="number">Day</label>
                              <select class="form-control forredline" id="" name="when_to_day">
                                <option value="">DD</option>
                                @for($i=1;$i<=31;$i++)
                                  @php
                                    $j = sprintf('%02d', $i)
                                  @endphp
                                 <option value="{{ $j }}" {{ $when_data->when_to_day==$j ? 'selected':'' }}>{{ $j }}</option>
                                @endfor
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayYear">
                              <label for="number">Year</label>
                              <select class="form-control forredline" id="" name="when_to_year">
                                <option value="">YYYY</option>
                              @for ($i = date('Y'); $i >= 1900; $i--)
                                <option value="{{ $i }}" {{ $when_data->when_to_year==$i ? 'selected':'' }}>{{$i}}</option>
                              @endfor
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                          </div>
                          <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">Departure Time</label>
                              <select class="form-control forredline formTime" id="" name="dep_time">
                                <option value="">00:00</option>
                    <option value="1:00" {{ $when_data->when_time=='1:00'?'selected':'' }}>1:00</option>
                    <option value="1:30" {{ $when_data->when_time=='1:30'?'selected':'' }}>1:30</option>
                    <option value="2:00" {{ $when_data->when_time=='2:00'?'selected':'' }}>2:00</option>
                    <option value="2:30" {{ $when_data->when_time=='2:30'?'selected':'' }}>2:30</option>
                    <option value="3:00" {{ $when_data->when_time=='3:00'?'selected':'' }}>3:00</option>
                    <option value="3:30" {{ $when_data->when_time=='3:30'?'selected':'' }}>3:30</option>
                    <option value="4:00" {{ $when_data->when_time=='4:00'?'selected':'' }}>4:00</option>
                    <option value="4:30" {{ $when_data->when_time=='4:30'?'selected':'' }}>4:30</option>
                    <option value="5:00" {{ $when_data->when_time=='5:00'?'selected':'' }}>5:00</option>
                    <option value="5:30" {{ $when_data->when_time=='5:30'?'selected':'' }}>5:30</option>
                    <option value="6:00" {{ $when_data->when_time=='6:00'?'selected':'' }}>6:00</option>
                    <option value="6:30" {{ $when_data->when_time=='6:30'?'selected':'' }}>6:30</option>
                    <option value="7:00" {{ $when_data->when_time=='7:00'?'selected':'' }}>7:00</option>
                    <option value="7:30" {{ $when_data->when_time=='7:30'?'selected':'' }}>7:30</option>
                    <option value="8:00" {{ $when_data->when_time=='8:00'?'selected':'' }}>8:00</option>
                    <option value="8:30" {{ $when_data->when_time=='8:30'?'selected':'' }}>8:30</option>
                    <option value="9:00" {{ $when_data->when_time=='9:00'?'selected':'' }}>9:00</option>
                    <option value="9:30" {{ $when_data->when_time=='9:30'?'selected':'' }}>9:30</option>
                    <option value="10:00" {{ $when_data->when_time=='10:00'?'selected':'' }}>10:00</option>
                    <option value="10:30" {{ $when_data->when_time=='10:30'?'selected':'' }}>10:30</option>
                    <option value="11:00" {{ $when_data->when_time=='11:00'?'selected':'' }}>11:00</option>
                    <option value="11:30" {{ $when_data->when_time=='11:30'?'selected':'' }}>11:30</option>
                    <option value="12:00" {{ $when_data->when_time=='12:00'?'selected':'' }}>12:00</option>
                    <option value="12:30" {{ $when_data->when_time=='12:30'?'selected':'' }}>12:30</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay timeOptionSet">
                              <div class="form-control timeOptionOnly {{ $when_data->when_time_ampm == 'AM' ? 'activeTimeOption' : '' }}" id="dep_time_am">AM</div>
                              <div class="form-control timeOptionOnly {{ $when_data->when_time_ampm == 'PM' ? 'activeTimeOption' : '' }}" id="dep_time_pm" style="border-top-left-radius: 0 !important;border-bottom-left-radius: 0 !important;">PM</div>
                              <input type="hidden" name="dep_time_ampm" id="" value="{{ $when_data->when_time_ampm }}" />
                            </div>
                            <div class="form-group birthdayYear"></div>
                          </div>
                          <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">Arrival Time</label>
                              <select class="form-control forredline formTime" id="" name="arr_time">
                                <option value="">00:00</option>
                    <option value="1:00" {{ $when_data->when_arrival_time=='1:00'?'selected':'' }}>1:00</option>
                    <option value="1:30" {{ $when_data->when_arrival_time=='1:30'?'selected':'' }}>1:30</option>
                    <option value="2:00" {{ $when_data->when_arrival_time=='2:00'?'selected':'' }}>2:00</option>
                    <option value="2:30" {{ $when_data->when_arrival_time=='2:30'?'selected':'' }}>2:30</option>
                    <option value="3:00" {{ $when_data->when_arrival_time=='3:00'?'selected':'' }}>3:00</option>
                    <option value="3:30" {{ $when_data->when_arrival_time=='3:30'?'selected':'' }}>3:30</option>
                    <option value="4:00" {{ $when_data->when_arrival_time=='4:00'?'selected':'' }}>4:00</option>
                    <option value="4:30" {{ $when_data->when_arrival_time=='4:30'?'selected':'' }}>4:30</option>
                    <option value="5:00" {{ $when_data->when_arrival_time=='5:00'?'selected':'' }}>5:00</option>
                    <option value="5:30" {{ $when_data->when_arrival_time=='5:30'?'selected':'' }}>5:30</option>
                    <option value="6:00" {{ $when_data->when_arrival_time=='6:00'?'selected':'' }}>6:00</option>
                    <option value="6:30" {{ $when_data->when_arrival_time=='6:30'?'selected':'' }}>6:30</option>
                    <option value="7:00" {{ $when_data->when_arrival_time=='7:00'?'selected':'' }}>7:00</option>
                    <option value="7:30" {{ $when_data->when_arrival_time=='7:30'?'selected':'' }}>7:30</option>
                    <option value="8:00" {{ $when_data->when_arrival_time=='8:00'?'selected':'' }}>8:00</option>
                    <option value="8:30" {{ $when_data->when_arrival_time=='8:30'?'selected':'' }}>8:30</option>
                    <option value="9:00" {{ $when_data->when_arrival_time=='9:00'?'selected':'' }}>9:00</option>
                    <option value="9:30" {{ $when_data->when_arrival_time=='9:30'?'selected':'' }}>9:30</option>
                    <option value="10:00" {{ $when_data->when_arrival_time=='10:00'?'selected':'' }}>10:00</option>
                    <option value="10:30" {{ $when_data->when_arrival_time=='10:30'?'selected':'' }}>10:30</option>
                    <option value="11:00" {{ $when_data->when_arrival_time=='11:00'?'selected':'' }}>11:00</option>
                    <option value="11:30" {{ $when_data->when_arrival_time=='11:30'?'selected':'' }}>11:30</option>
                    <option value="12:00" {{ $when_data->when_arrival_time=='12:00'?'selected':'' }}>12:00</option>
                    <option value="12:30" {{ $when_data->when_arrival_time=='12:30'?'selected':'' }}>12:30</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay timeOptionSet">
                              <div class="form-control timeOptionOnly  {{ $when_data->when_arrival_time_ampm == 'AM' ? 'activeTimeOption' : '' }}" id="arr_time_am">AM</div>
                              <div class="form-control timeOptionOnly  {{ $when_data->when_arrival_time_ampm == 'PM' ? 'activeTimeOption' : '' }}" id="arr_time_pm" style="border-top-left-radius: 0 !important;border-bottom-left-radius: 0 !important;">PM</div>
                              <input type="hidden" name="arr_time_ampm" id="arr_time_ampm" value="{{ $when_data->when_arrival_time_ampm }}">
                            </div>
                            <div class="form-group birthdayYear"></div>
                          </div>
                        </div>
                        <div class="eventColHeading">Who, What, Where, When, Why </div>
                        <div class="whatForm whereFrom">
                          
                          <div class="myAllEvents" id="keywords_div">
                        @if($event_data->event_keywords)
                          @php
                            $keywords = explode(',',$event_data->event_keywords);
                          @endphp
                          @foreach($keywords as $i => $keyword)
                                  <div class="addFieldItem keywords" id="{{ $i }}" title="{{ $keyword }}">{{ $keyword }} <span class="removeField" onclick="removeKeyword('{{ $i }}');"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>
                          @endforeach
                        @endif
                                  </div>
                                  <input type="hidden" id="keywords" value="{{ $event_data->event_keywords }}" name="event_keywords">
	                        <div class="input-group lifeEvent">
	                          <input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
	                          <div class="input-group-append">
	                            <a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
	                           </div>
	                        </div>
                          	<div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                        </div>
                        @include('inc.life_event_img_upload2',['images' => $images, 'event_data' => $event_data])

                      </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Travel/Vacation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Travel/Vacation</label>
            <p>This section is for your travel details. This specific information
might help you match with the person with the exact travel details. Exp: Flight
number, bus number, ticket number etc. Don’t forget " keywords" for
anything important to add.</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>This is the location where you met someone or where the event happened. It can be somewhere you stayed or visited. It can be anywhere; give it a shot.</p>
          </div>
          <div class="form-group">
            <label>WHEN:</label>
            <p>It's the scope of your travel dates, departure time, and arrival time. It's not required to be specific with the time.</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
@include('inc.footer')
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).ready(function(){
  $("#dep_time_am").click(function(){
      $("#dep_time_am").addClass("activeTimeOption");
      $("#dep_time_pm").removeClass("activeTimeOption");
      $('#dep_time_ampm').val('AM');
  });
  $("#dep_time_pm").click(function(){
    $("#dep_time_pm").addClass("activeTimeOption");
    $("#dep_time_am").removeClass("activeTimeOption");
    $('#dep_time_ampm').val('PM');
  });

  $("#arr_time_am").click(function(){
      $("#arr_time_am").addClass("activeTimeOption");
      $("#arr_time_pm").removeClass("activeTimeOption");
      $('#arr_time_ampm').val('AM');
  });
  $("#arr_time_pm").click(function(){
    $("#arr_time_pm").addClass("activeTimeOption");
    $("#arr_time_am").removeClass("activeTimeOption");
    $('#arr_time_ampm').val('PM');
  });
});
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script>