@include('inc.header')
<div class="innerBodyOnly innerBodyModify">
  	<div class="container-fluid">
    	<div class="settingBody">
	      <div class="position-relative settingFlip">
	       <div class="leftSlidePan closePan">
            <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
            <div class="lifeDraftLeftTitle"> {{ $event_data->event_is_draft==0 ? 'LIFE EVENTS' : 'DRAFTS' }} </div>
            <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
              <ul class="list-unstyled mb-0 lifeEventMenu">
                <li class="lifeEventMenuLi lifeDraftLi {{ $event_data->event_is_draft==0 ? 'lifeEventMenuLiActive' : '' }}">
                  <a href="{{ url('life-events') }}" class="lifeEventMenuLink">
                    <span class="lifeEventMenuText">Created Life Events</span>
                    <span class="lifeDraftCount">{{ $event_count }}</span>
                  </a>
                </li>
                <li class="lifeEventMenuLi lifeDraftLi {{ $event_data->event_is_draft==1 ? 'lifeEventMenuLiActive' : '' }}">
                  <a href="{{ url('drafts') }}" class="lifeEventMenuLink lifeDraftMenuLink">
                    <span class="lifeEventMenuText">Drafts</span>
                    <span class="lifeDraftCount">{{ $draft_count }}</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
	          	<div class="midPan">
		            <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
		              	<div class="row infoBodyRow">
			                <div class="col-md-5 infoBodyCol connectionsBody eventImgCol">
                        <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
			                    <div class="innerHome">
			                    	<a href="{{url('home')}}"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
			                    </div>
			                  	<div class="feedbackImg">
				                    <div class="personalImgAll">
				                      	<img src="new-design/img/lostfound.png" class="img-fluid" alt="">
				                      	<!-- <div class="personalImgTitle">Lost & Found</div> -->
				                    </div>
			                  	</div>
                        </div>
			               </div>
			                <div class="col-md-7 infoBodyCol connectionsBody eventImgCol">
                          <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid" style="background-color: #FFF !important;">
			                  		<form class="xhr_form" method="post" action="api/save-life-event" id="save-life-event">
                              @csrf
                              <div class="eventColHeading">What </div><img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                              <input type="hidden" name="type_id" value="9">
                              <input type="hidden" name="user_id" value="{{Session::get('userdata')['id']}}">
                              <input type="hidden" name="_event_id" value="{{ $event_data->_event_id }}">
                              <div class="whatForm whatFormDationg">
                                <!-- <div class="form-group datingFilterBtn timeOptionSet">
                                  <div class="form-control timeOptionOnly enterFor activeTimeOption">Enter</div>
                                  <div class="form-control timeOptionOnly searchFor">Search</div>
                                </div> -->
                                <div class="form-group datingFilterBtn timeOptionSet">
                                    <div class="form-control timeOptionOnly enterFor {{ $event_data->event_dating_search_for==0 ? 'activeTimeOption' : '' }}">Enter (you)</div>
                                    <div class="form-control timeOptionOnly searchFor {{ $event_data->event_dating_search_for==1 ? 'activeTimeOption' : '' }}">Search (for)</div>
                                    <input type="hidden" name="search_for" value="{{ $event_data->event_dating_search_for }}" id="search_for" />
                                </div>

                                <div class="form-group datingSpecific">
                                  <label for="">Item</label>
                                  <select class="form-control forredline" id="items" name="item_name">
                                    <option value="">Select</option>
                    @if($items)
                      @foreach($items as $item)
                        @php
                          $selected = $item->item_name == $event_data->event_item_name ? 'selected' : '';
                        @endphp
                              <option option="{{ $item->item_name }}" {{ $selected }} data-sizes="{{ $item->item_sizes }}">{{$item->item_name}}</option>
                      @endforeach
                    @endif
                                  </select>
                                </div>

                                <div class="whatReason1 vehicleForm">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">Size</label>
                                        <select class="form-control formSize forredline" id="sizes" name="item_size">
                                          <option value="">Select</option>
                    @if($item_sizes)
                      @foreach($item_sizes as $size)
                        @php
                          $selected = $size->is_name==$event_data->event_item_size ? 'selected' : '';
                        @endphp
                                          <option value="{{ $size->is_name }}" {{ $selected }}>{{ $size->is_name }}</option>
                      @endforeach
                    @endif
                                        </select>
                                        <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">Material</label>
                                        <select class="form-control forredline formMaterial" name="item_material">
                                          <option value="">Select</option>
                    @if($materials)
                      @foreach($materials as $material)
                        @php
                          $selected = $material->material_name==$event_data->event_item_material ? 'selected' : '';
                        @endphp
                              <option option="{{ $material->material_name }}" {{ $selected }}>{{$material->material_name}}</option>
                      @endforeach
                    @endif
                                        </select>
                                        <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">Outdoor</label>
                                        <select class="form-control forredline formOutdoor" name="outdoor">
                                          <option value="">Select</option>
                    @if($outdoors)
                      @foreach($outdoors as $outdoor)
                        @php
                          $selected = $outdoor->outdoor_name==$event_data->event_outdoor ? 'selected' : '';
                        @endphp
                              <option option="{{ $outdoor->outdoor_name }}" {{ $selected }}>{{$outdoor->outdoor_name}}</option>
                      @endforeach
                    @endif
                                        </select>
                                        <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">Indoor</label>
                                        <select class="form-control forredline formLocation" name="indoor">
                                          <option value="">Select</option>
                    @if($indoors)
                      @foreach($indoors as $indoor)
                        @php
                          $selected = $indoor->indoor_name==$event_data->event_indoor ? 'selected' : '';
                        @endphp
                              <option option="{{ $indoor->indoor_name }}" {{ $selected }}>{{$indoor->indoor_name}}</option>
                      @endforeach
                    @endif
                                        </select>
                                        <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">In Motion</label>
                                        <select class="form-control forredline formAirline" name="inmotion">
                                          <option value="">Select</option>
                    @if($inmotions)
                      @foreach($inmotions as $inmotion)
                        @php
                          $selected = $inmotion->inmotion_name==$event_data->event_inmotion ? 'selected' : '';
                        @endphp
                              <option option="{{ $inmotion->inmotion_name }}" {{ $selected }}>{{$inmotion->inmotion_name}}</option>
                      @endforeach
                    @endif
                                        </select>
                                        <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="">Quantity</label>
                                        <input type="text" name="other" class="form-control forredline formOther" value="{{ $event_data->event_other }}">
                                        <!-- <select class="form-control forredline formOther">
                                          <option>1</option>
                                          <option>2</option>
                                        </select> -->
                                        <!-- <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt=""> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                
                              </div>
                              <div class="eventColHeading">Where </div>
                              <div class="whatForm whereFrom">
                                <div class="form-group">
                                  <label for="">Country</label>
                                  <select class="form-control forredline formCountry" name="country" id="country">
                                    <option value="">Select</option>
                  @if($countries)
                    @foreach($countries as $country)
                      @php
                        $selected = ($where_data && $country->country_name==$where_data->where_country) ? 'selected' : '';
                      @endphp
                                    <option value="{{ $country->country_name }}" {{ $selected }} data-id="{{ $country->_country_id }}">{{ $country->country_name }}</option>
                    @endforeach
                  @endif
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                                <div class="form-group">
                                  <label for="">State/County/Province</label>
                                  <select class="form-control forredline formState" name="state" id="province">
                                    <option value="">Select</option>
                  @if($provinces)
                    @foreach($provinces as $province)
                      @php
                        $selected = ($where_data && $province->province_name==$where_data->where_state) ? 'selected' : '';
                      @endphp
                                    <option value="{{ $province->province_name }}" {{ $selected }} data-id="{{ $province->_province_id }}">{{ $province->province_name }}</option>
                    @endforeach
                  @endif
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                                <div class="form-group">
                                  <label for="">City</label>
                                  <select class="form-control forredline formCity" name="city" id="city">
                                    <option value="">Select</option>
                  @if($cities)
                    @foreach($cities as $city)
                      @php
                        $selected = ($where_data && $city->city_name==$where_data->where_city) ? 'selected' : '';
                      @endphp
                                    <option value="{{ $city->city_name }}" {{ $selected }} data-id="{{ $city->_city_id }}">{{ $city->city_name }}</option>
                    @endforeach
                  @endif
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                                <div class="form-group">
                                  <label for="">Street Address</label>
                                  <input type="text" class="form-control forredline formAddress" id="" placeholder="123 abc Ave." name="street" value="{{ $where_data ? $where_data->where_street : '' }}">
                                </div>
                                <div class="form-group">
                                  <label for="">ZIP</label>
                                  <input type="text" class="form-control forredline formZip" id="" placeholder="ZIP" name="zip" value="{{ $where_data ? $where_data->where_zip : '' }}">
                                </div>
                              </div>
                              <div class="eventColHeading">When </div>
                              <div class="whatForm whereFrom">
                                <div class="birthdayDate">
                                  <div class="form-group birthdayMonth">
                                    <label for="number">From</label>
                                    <select class="form-control forredline formMonth" id="" name="when_from_month">
                                      <option value="">Month</option>
                                <option value="01" {{ $when_data->when_from_month=='01'?'selected':'' }}>January</option>
                                <option value="02" {{ $when_data->when_from_month=='02'?'selected':'' }}>February</option>
                                <option value="03" {{ $when_data->when_from_month=='03'?'selected':'' }}>March</option>
                                <option value="04" {{ $when_data->when_from_month=='04'?'selected':'' }}>April</option>
                                <option value="05" {{ $when_data->when_from_month=='05'?'selected':'' }}>May</option>
                                <option value="06" {{ $when_data->when_from_month=='06'?'selected':'' }}>June</option>
                                <option value="07" {{ $when_data->when_from_month=='07'?'selected':'' }}>July</option>
                                <option value="08" {{ $when_data->when_from_month=='08'?'selected':'' }}>August</option>
                                <option value="09" {{ $when_data->when_from_month=='09'?'selected':'' }}>September</option>
                                <option value="10" {{ $when_data->when_from_month=='10'?'selected':'' }}>October</option>
                                <option value="11" {{ $when_data->when_from_month=='11'?'selected':'' }}>November</option>
                                <option value="12" {{ $when_data->when_from_month=='12'?'selected':'' }}>December</option>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group birthdayDay">
                                    <label for="number">Day</label>
                                    <select class="form-control forredline" id="" name="when_from_day">
                                      <option value="">DD</option>
                                      @for($i=1;$i<=31;$i++)
                                        @php
                                          $j = sprintf('%02d', $i)
                                        @endphp
                                       <option value="{{ $j }}" {{ $when_data->when_from_day==$j ? 'selected':'' }}>{{ $j }}</option>
                                      @endfor
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group birthdayYear">
                                    <label for="number">Year</label>
                                    <select class="form-control forredline" id="" name="when_from_year">
                                      <option value="">YYYY</option>
                                    @for ($i = date('Y'); $i >= 1900; $i--)
                                      <option value="{{ $i }}" {{ $when_data->when_from_year==$i ? 'selected':'' }}>{{$i}}</option>
                                    @endfor
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                </div>
                              </div>
                              <div class="eventColHeading">Who, What, Where, When, Why </div>
                              <div class="whatForm whereFrom">
                                
                                <div class="myAllEvents" id="keywords_div2">
                        @if($event_data->event_keywords)
                          @php
                            $keywords = explode(',',$event_data->event_keywords);
                          @endphp
                          @foreach($keywords as $i => $keyword)
                                  <div class="addFieldItem keywords" id="{{ $i }}" title="{{ $keyword }}">{{ $keyword }} <span class="removeField" onclick="removeKeyword2('{{ $i }}');"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>
                          @endforeach
                        @endif
                                </div>
                                <input type="hidden" id="keywords2" value="{{ $event_data->event_keywords }}" name="event_keywords">
                                <div class="input-group lifeEvent">
                                  <input type="text" class="form-control" id="add_keyword2" placeholder="Life Event Keyword(s)">
                                  <div class="input-group-append">
                                    <a onclick="addKeyword2();" class="btn btn-add" style="text-decoration: none;">Add</a>
                                  </div>
                                </div>
                                <div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                              </div>
                        
                        @include('inc.life_event_img_upload2',['images' => $images, 'event_data' => $event_data])
                            </form>
			                  	</div>
			                </div>
		              	</div>
		            </div>
	          	</div>
	      	</div>
    	</div>
  	</div>
</div>
<!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Lost/Found</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Lost/Found</label>
            <p>If you lost something, this is the perfect life event for you. You can describe it
and enter your missing item or search for it. If someone found something and
they want to return it, they descried it and location and… ITEM FOUND!</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>This is the location where you lost or found the item.</p>
          </div>
          <div class="form-group">
            <label>WHEN:</label>
            <p>A specific date when you lost or found the item.</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
@include('inc.footer')
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).ready(function(){
  $(".enterFor").click(function(){
        $(".enterFor").addClass("activeTimeOption");
        $(".searchFor").removeClass("activeTimeOption");
        $('#search_for').val(0);
    });
    $(".searchFor").click(function(){
        $(".searchFor").addClass("activeTimeOption");
        $(".enterFor").removeClass("activeTimeOption");
        $('#search_for').val(1);
    });
});
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value=""="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script>