@include('inc.header')
<div class="innerBodyOnly innerBodyModify">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                  <div class="leftSlidePan closePan">
            <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
            <div class="lifeDraftLeftTitle"> {{ $event_data->event_is_draft==0 ? 'LIFE EVENTS' : 'DRAFTS' }} </div>
            <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
              <ul class="list-unstyled mb-0 lifeEventMenu">
                <li class="lifeEventMenuLi lifeDraftLi {{ $event_data->event_is_draft==0 ? 'lifeEventMenuLiActive' : '' }}">
                  <a href="{{ url('life-events') }}" class="lifeEventMenuLink">
                    <span class="lifeEventMenuText">Created Life Events</span>
                    <span class="lifeDraftCount">{{ $event_count }}</span>
                  </a>
                </li>
                <li class="lifeEventMenuLi lifeDraftLi {{ $event_data->event_is_draft==1 ? 'lifeEventMenuLiActive' : '' }}">
                  <a href="{{ url('drafts') }}" class="lifeEventMenuLink lifeDraftMenuLink">
                    <span class="lifeEventMenuText">Drafts</span>
                    <span class="lifeDraftCount">{{ $draft_count }}</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
                  <div class="midPan">
                    <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                      <div class="row infoBodyRow">
                        <div class="col-md-5 infoBodyCol connectionsBody eventImgCol">
                          <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
                            <div class="innerHome">
                              <a href="{{url('home')}}"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                            </div>
                            <div class="feedbackImg">
                              <div class="personalImgAll">
                                <img src="new-design/img/adoptionImg.png" class="img-fluid" alt="">
                                <div class="personalImgTitle">Adoption / Foster / Shelter</div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7 connectionsBody infoBodyCol eventCol midHightControl">
                          <div class="connectionsBody feedbackRight windowHeight windowHeightMid">
                            <form class="xhr_form" method="post" action="api/save-life-event" id="save-life-event">
                            @csrf
                              <div class="eventColHeading">What </div>
                              <img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                              <input type="hidden" name="type_id" value="3">
                              <input type="hidden" name="user_id" value="{{Session::get('userdata')['id']}}">
                              <input type="hidden" name="_event_id" value="{{ $event_data->_event_id }}">
                                <div class="whatForm">
                                  <div class="form-group">
                                    <label for="">What</label>
                                    <select class="form-control forredline " name="sub_category_id">
            		@if($what)
                  @foreach($what as $w)
                        @php
                          $selected = $w->id==$event_data->event_subcategory_id ? 'selected' : '';
                        @endphp
                                      <option value="{{ $w->id }}" {{ $selected }}>{{ $w->sub_cat_type_name }}</option>
                  @endforeach
                @endif
                                    </select>
                                  </div>
                                  <div class="whatReasonAll">
                                    <div class="form-group">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="" style="margin: 8px auto;">Biological Mother</label>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group adoptionCheckAll" style="padding: 0;margin: 10px auto;">
                                            <label class="adoptionCheck">I'm the mother
                                              <input type="radio" name="user_is_parent" value="1" {{ $event_data->event_user_is_mother == 1 ? 'checked' : '' }}>
                                              <span class="adoptionCheckmark"></span>
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <input type="text" class="form-control forredline" id="" placeholder="Type here" name="bio_mother" value="{{ $event_data->event_bio_mother }}">
                                    </div>
                                    
                                    <div class="form-group">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="" style="margin: 8px auto;">Biological Father</label>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group adoptionCheckAll" style="padding: 0;margin: 10px auto;">
                                            <label class="adoptionCheck">I'm the father
                                              <input type="radio" name="user_is_parent" value="2" {{ $event_data->event_user_is_father == 1 ? 'checked' : '' }}>
                                              <span class="adoptionCheckmark"></span>
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                      <input type="text" class="form-control forredline" id="" placeholder="Type here" name="bio_father" value="{{ $event_data->event_bio_father }}">
                                    </div>
                                    
                                    <div class="form-group">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="" style="margin: 8px auto;">Name given at birth</label>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group adoptionCheckAll" style="padding: 0;margin: 10px auto;">
                                            <label class="adoptionCheck">I'm the child
                                              <input type="radio" name="user_is_parent" value="3" {{ $event_data->event_user_is_child == 1 ? 'checked' : '' }}>
                                              <span class="adoptionCheckmark"></span>
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <input type="text" class="form-control forredline" id="" placeholder="Type here" name="birth_name" value="{{ $event_data->event_birthname }}">
                                    </div>
                                    
                                    <div class="form-group">
                                      <label for="" >Weight</label>
                                      <div class="height_weight weight_adoption">
                                        <div class="height_weight_Only">
                                          <input type="text" class="form-control forredline formWeight" id="weight" placeholder="00" name="weight" value="{{ $event_data->event_weight }}" style="margin-left: 0px;">
                                        </div>
                                        <div class="form-group timeOptionSet">
                                          <div class="form-control timeOptionOnly  {{ $event_data->event_weight_measure=='kg' ? 'activeTimeOption' : '' }}" id="weight_measure_kg">kg</div>
                                          <div class="form-control timeOptionOnly {{ $event_data->event_weight_measure=='lbs' ? 'activeTimeOption' : '' }}" id="weight_measure_lbs" style="border-top-left-radius: 0 !important;border-bottom-left-radius: 0 !important;">lbs</div>
                                          <input type="hidden" name="weight_measure" id="weight_measure" value="{{ $event_data->event_weight_measure }}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="">Length</label>
                                      <div class="height_weight weight_adoption">
                                        <div class="height_weight_Only">
                                          <input type="number" name="" class="form-control forredline formHeight" name="height" id="height" value="{{ $event_data->event_height }}" style="margin-left: 0px;">
                                        </div>
                                        <div class="form-group timeOptionSet">
                                          <div class="form-control timeOptionOnly  {{ $event_data->event_height_measure=='in' ? 'activeTimeOption' : '' }}" id="height_measure_in">in</div>
                                          <div class="form-control timeOptionOnly {{ $event_data->event_height_measure=='cm' ? 'activeTimeOption' : '' }}" id="height_measure_cm" style="border-top-left-radius: 0 !important;border-bottom-left-radius: 0 !important;">cm</div>
                                          <input type="hidden" name="height_measure" id="height_measure" value="{{ $event_data->event_height_measure }}">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                          <label for="">Gender</label>
                                          <select class="form-control forredline formGender" name="gender">
                                            <option value=">Select</option>
                    @if($genders)
                      @foreach($genders as $gender)
                        @php
                          $selected = $gender->gender_name==$event_data->event_looking_for ? 'selected' : '';
                        @endphp
                              <option option="{{ $gender->gender_name }}" {{ $selected }}>{{$gender->gender_name}}</option>
                      @endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                    <div class="birthdayDate">
                            <div class="form-group birthdayMonth">
                              <label for="number">Time of Birth</label>
                              <select class="form-control forredline formTime" id="" name="time_of_birth">
                                <option value="="">00:00</option>
                    <option value="1:00" {{ $event_data->event_time_of_birth=='1:00'?'selected':'' }}>1:00</option>
                    <option value="1:30" {{ $event_data->event_time_of_birth=='1:30'?'selected':'' }}>1:30</option>
                    <option value="2:00" {{ $event_data->event_time_of_birth=='2:00'?'selected':'' }}>2:00</option>
                    <option value="2:30" {{ $event_data->event_time_of_birth=='2:30'?'selected':'' }}>2:30</option>
                    <option value="3:00" {{ $event_data->event_time_of_birth=='3:00'?'selected':'' }}>3:00</option>
                    <option value="3:30" {{ $event_data->event_time_of_birth=='3:30'?'selected':'' }}>3:30</option>
                    <option value="4:00" {{ $event_data->event_time_of_birth=='4:00'?'selected':'' }}>4:00</option>
                    <option value="4:30" {{ $event_data->event_time_of_birth=='4:30'?'selected':'' }}>4:30</option>
                    <option value="5:00" {{ $event_data->event_time_of_birth=='5:00'?'selected':'' }}>5:00</option>
                    <option value="5:30" {{ $event_data->event_time_of_birth=='5:30'?'selected':'' }}>5:30</option>
                    <option value="6:00" {{ $event_data->event_time_of_birth=='6:00'?'selected':'' }}>6:00</option>
                    <option value="6:30" {{ $event_data->event_time_of_birth=='6:30'?'selected':'' }}>6:30</option>
                    <option value="7:00" {{ $event_data->event_time_of_birth=='7:00'?'selected':'' }}>7:00</option>
                    <option value="7:30" {{ $event_data->event_time_of_birth=='7:30'?'selected':'' }}>7:30</option>
                    <option value="8:00" {{ $event_data->event_time_of_birth=='8:00'?'selected':'' }}>8:00</option>
                    <option value="8:30" {{ $event_data->event_time_of_birth=='8:30'?'selected':'' }}>8:30</option>
                    <option value="9:00" {{ $event_data->event_time_of_birth=='9:00'?'selected':'' }}>9:00</option>
                    <option value="9:30" {{ $event_data->event_time_of_birth=='9:30'?'selected':'' }}>9:30</option>
                    <option value="10:00" {{ $event_data->event_time_of_birth=='10:00'?'selected':'' }}>10:00</option>
                    <option value="10:30" {{ $event_data->event_time_of_birth=='10:30'?'selected':'' }}>10:30</option>
                    <option value="11:00" {{ $event_data->event_time_of_birth=='11:00'?'selected':'' }}>11:00</option>
                    <option value="11:30" {{ $event_data->event_time_of_birth=='11:30'?'selected':'' }}>11:30</option>
                    <option value="12:00" {{ $event_data->event_time_of_birth=='12:00'?'selected':'' }}>12:00</option>
                    <option value="12:30" {{ $event_data->event_time_of_birth=='12:30'?'selected':'' }}>12:30</option>
                              </select>
                              <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                            </div>
                            <div class="form-group birthdayDay timeOptionSet">
                              <div class="form-control timeOptionOnly activeTimeOption" id="time_of_birth_am">AM</div>
                              <div class="form-control timeOptionOnly" id="time_of_birth_pm" style="border-top-left-radius: 0 !important;border-bottom-left-radius: 0 !important;">PM</div>
                              <input type="hidden" name="time_of_birth_ampm" id="time_of_birth_ampm" value="{{ $event_data->event_time_of_birth_ampm }}" />
                            </div>
                            <div class="form-group birthdayYear"></div>
                          </div>
                                    
                                  </div>
                                </div>
                                <div class="eventColHeading">Where </div>
                                <div class="whatForm whereFrom">
                                  <div class="form-group">
                                    <label for="">Place of Birth</label>
                                    <select class="form-control forredline formBarthPlace" name="birth_place">
                                      <option value=">Select</option>
                    @if($place_of_birth)
                    	@foreach($place_of_birth as $place)
                        @php
                          $selected = $place->place_name==$where_data->where_birth_place ? 'selected' : '';
                        @endphp
                    					<option option="{{ $place->place_name }}">{{$place->place_name}}</option>
                    	@endforeach
                    @endif
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Country</label>
                                    <select class="form-control forredline formCountry" name="birth_country" id="birth_country">
                              <option value=">Select</option>
            @if($countries)
              @foreach($countries as $country)
                @php
                  $selected = $country->country_name==$where_data->where_birth_country ? 'selected' : '';
                @endphp
                              <option value="{{ $country->country_name }}" data-id="{{ $country->_country_id }}">{{ $country->country_name }}</option>
              @endforeach
            @endif
                              {{-- <option>1</option>
                              <option>2</option> --}}
                            </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">State/County/Province</label>
                                    <select class="form-control forredline formState" name="birth_state" id="birth_province">
                                      <option value=">Select</option>
            @if($birth_provinces)
              @foreach($birth_provinces as $province)
                @php
                  $selected = ($where_data && $province->province_name==$where_data->where_birth_state) ? 'selected' : '';
                @endphp
                              <option value="{{ $province->province_name }}" {{ $selected }} data-id="{{ $province->_province_id }}">{{ $province->province_name }}</option>
              @endforeach
            @endif
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">City</label>
                                    <select class="form-control forredline formCity" name="birth_city" id="birth_city">
                                      <option value=">Select</option>
            @if($birth_cities)
              @foreach($birth_cities as $city)
                @php
                  $selected = ($where_data && $city->city_name==$where_data->where_birth_city) ? 'selected' : '';
                @endphp
                              <option value="{{ $city->city_name }}" {{ $selected }} data-id="{{ $city->_city_id }}">{{ $city->city_name }}</option>
              @endforeach
            @endif
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Street Address</label>
                                    <input type="text" class="form-control forredline formAddress" id="" placeholder="123 abc Ave." name="birth_street" value="{{ $where_data->where_birth_street }}">
                                  </div>
                                  <div class="form-group">
                                    <label for="">ZIP</label>
                                    <input type="text" class="form-control forredline formZip" id="" placeholder="ZIP" name="birth_zip" value="{{ $where_data->where_birth_zip }}">
                                  </div>
                                  <div class="bar"></div>
                                  <div class="form-group">
                                    <label for="">Status</label>
                                    <select class="form-control forredline formBarthPlace" name="adoption_status">
                                      <option value=">Select</option>
                	@if($adoption_status)
                    	@foreach($adoption_status as $status)
                @php
                  $selected = ($where_data && $status->status_name==$where_data->where_adoption_status) ? 'selected' : '';
                @endphp
                    					<option option="{{ $status->status_name }}" {{ $selected }}>{{$status->status_name}}</option>
                    	@endforeach
                    @endif
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Country</label>
                                    <select class="form-control forredline formCountry" name="country" id="country">
                              <option value=">Select</option>
            @if($countries)
              @foreach($countries as $country)
                @php
                  $selected = $country->country_name==$where_data->where_country ? 'selected' : '';
                @endphp
                              <option value="{{ $country->country_name }}" {{ $selected }} data-id="{{ $country->_country_id }}">{{ $country->country_name }}</option>
              @endforeach
            @endif
                              {{-- <option>1</option>
                              <option>2</option> --}}
                            </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">State/County/Province</label>
                                    <select class="form-control forredline formState" name="state" id="province">
                                      <option value=">Select</option>
            @if($provinces)
              @foreach($provinces as $province)
                @php
                  $selected = ($where_data && $province->province_name==$where_data->where_state) ? 'selected' : '';
                @endphp
                              <option value="{{ $province->province_name }}" {{ $selected }} data-id="{{ $province->_province_id }}">{{ $province->province_name }}</option>
              @endforeach
            @endif
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">City</label>
                                    <select class="form-control forredline formCity" name="city" id="city">
                                      <option value=">Select</option>
            @if($cities)
              @foreach($cities as $city)
                @php
                  $selected = ($where_data && $city->city_name==$where_data->where_city) ? 'selected' : '';
                @endphp
                              <option value="{{ $city->city_name }}" {{ $selected }} data-id="{{ $city->_city_id }}">{{ $city->city_name }}</option>
              @endforeach
            @endif
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Street Address</label>
                                    <input type="text" class="form-control forredline formAddress" id="" placeholder="123 abc Ave." name="street" value="{{ $where_data->where_street }}">
                                  </div>
                                  <div class="form-group">
                                    <label for="">ZIP</label>
                                    <input type="text" class="form-control forredline formZip" id="" placeholder="ZIP" name="zip" value="{{ $where_data->where_zip }}">
                                  </div>
                                </div>
                                <div class="eventColHeading">When </div>
                                <div class="whatForm whereFrom">
                                  <div class="birthdayDate">
                                    <div class="form-group birthdayMonth">
                                      <label for="number">Month</label>
                                      <select class="form-control forredline formMonth" id="" name="when_from_month">
                                        <option value=">Month</option>
                                <option value="01" {{ $when_data->when_from_month=='01'?'selected':'' }}>January</option>
                                <option value="02" {{ $when_data->when_from_month=='02'?'selected':'' }}>February</option>
                                <option value="03" {{ $when_data->when_from_month=='03'?'selected':'' }}>March</option>
                                <option value="04" {{ $when_data->when_from_month=='04'?'selected':'' }}>April</option>
                                <option value="05" {{ $when_data->when_from_month=='05'?'selected':'' }}>May</option>
                                <option value="06" {{ $when_data->when_from_month=='06'?'selected':'' }}>June</option>
                                <option value="07" {{ $when_data->when_from_month=='07'?'selected':'' }}>July</option>
                                <option value="08" {{ $when_data->when_from_month=='08'?'selected':'' }}>August</option>
                                <option value="09" {{ $when_data->when_from_month=='09'?'selected':'' }}>September</option>
                                <option value="10" {{ $when_data->when_from_month=='10'?'selected':'' }}>October</option>
                                <option value="11" {{ $when_data->when_from_month=='11'?'selected':'' }}>November</option>
                                <option value="12" {{ $when_data->when_from_month=='12'?'selected':'' }}>December</option>
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayDay">
                                      <label for="number">Day</label>
                                      <select class="form-control forredline" id="" name="when_from_day">
                                        <option value=">DD</option>
                                        @for($i=1;$i<=31;$i++)
                                          @php
                                            $j = sprintf('%02d', $i)
                                          @endphp
                                         <option value="{{ $j }}" {{ $when_data->when_from_day==$j ? 'selected':'' }}>{{ $j }}</option>
                                        @endfor
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayYear">
                                      <label for="number">Year</label>
                                      <select class="form-control forredline" id="" name="when_from_year">
                                        <option value=">YYYY</option>
                                      @for ($i = date('Y'); $i >= 1900; $i--)
                                        <option value="{{ $i }}" {{ $when_data->when_from_year==$i ? 'selected':'' }}>{{$i}}</option>
                                      @endfor
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                  </div>
                                </div>
                                <div class="eventColHeading">Who, What, Where, When, Why </div>
                                <div class="whatForm whereFrom">
                                  
                                  	<div class="myAllEvents" id="keywords_div">
                        @if($event_data->event_keywords)
                          @php
                            $keywords = explode(',',$event_data->event_keywords);
                          @endphp
                          @foreach($keywords as $i => $keyword)
                                  <div class="addFieldItem keywords" id="{{ $i }}" title="{{ $keyword }}">{{ $keyword }} <span class="removeField" onclick="removeKeyword('{{ $i }}');"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>
                          @endforeach
                        @endif
                                  </div>
                                  <input type="hidden" id="keywords" value="{{ $event_data->event_keywords }}" name="event_keywords">
			                        <div class="input-group lifeEvent">
			                          <input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
			                          <div class="input-group-append">
			                            <a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
			                           </div>
			                        </div>
                                  <div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                                </div>
                                
                              @include('inc.life_event_img_upload2',['images' => $images, 'event_data' => $event_data])

                              </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
<!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Adoption</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Adoption</label>
            <p>NO MORE… Sealed files, government “<span class="text-danger">RED</span>-TAPE”. If you have the details and
want to find each it’s as good as DONE! If you're searching for your child or
your parents, this is the perfect life event for you. This section is very personal
and about you. If you remember anything about your weight, time of birth,
hospital, birth name, parents name or vice versa - Enter all the details you
remember about your mother/father - if you're the mother/father, specify.</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>There are two (2) section under where - Origin of birth location and
foster/shelter home location. If adopted, enter both remembered origin birth
location and foster/shelter location. If parent, specify the birth location of
your child.</p>
          </div>
          <div class="form-group">
            <label>WHEN:</label>
            <p>(Child) Enter your birthday. (Parent) Enter your child's birthday.</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
@include('inc.footer')
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).ready(function(){
  $("#weight_measure_kg").click(function(){
    $("#weight_measure_kg").addClass("activeTimeOption");
    $("#weight_measure_lbs").removeClass("activeTimeOption");
    $('#weight_measure').val('kg');
    var weight = $('#weight').val();
    var changed_weight = (weight*0.453592).toFixed(2);
    $('#weight').val(changed_weight);
  });
  $("#weight_measure_lbs").click(function(){
    $("#weight_measure_lbs").addClass("activeTimeOption");
    $("#weight_measure_kg").removeClass("activeTimeOption");
    $('#weight_measure').val('lbs');
    var weight = $('#weight').val();
    var changed_weight = (weight*2.20462).toFixed(2);
    $('#weight').val(changed_weight);
  });
  $("#height_measure_in").click(function(){
    $("#height_measure_in").addClass("activeTimeOption");
    $("#height_measure_cm").removeClass("activeTimeOption");
    $('#height_measure').val('in');
    var height = $('#height').val();
    var changed_height = (height*0.393701).toFixed(2);
    $('#height').val(changed_height);
  });
  $("#height_measure_cm").click(function(){
    $("#height_measure_cm").addClass("activeTimeOption");
    $("#height_measure_in").removeClass("activeTimeOption");
    $('#height_measure').val('cm');
    var height = $('#height').val();
    var changed_height = (height*2.54).toFixed(2);
    $('#height').val(changed_height);
  });
  $("#time_of_birth_am").click(function(){
      $("#time_of_birth_am").addClass("activeTimeOption");
      $("#time_of_birth_pm").removeClass("activeTimeOption");
      $('#time_of_birth_ampm').val('AM');
  });
  $("#time_of_birth_pm").click(function(){
    $("#time_of_birth_pm").addClass("activeTimeOption");
    $("#time_of_birth_am").removeClass("activeTimeOption");
    $('#time_of_birth_ampm').val('PM');
  });
});

$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value=">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});

$(document).on('change', '#birth_country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#birth_province').html(response.reponse_body.province);
            $('#birth_city').html('<option value=">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#birth_province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#birth_city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script>