@include('inc.header')
<div class="innerBodyOnly innerBodyModify">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                  <div class="leftSlidePan closePan">
            <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
            <div class="lifeDraftLeftTitle"> {{ $event_data->event_is_draft==0 ? 'LIFE EVENTS' : 'DRAFTS' }} </div>
            <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
              <ul class="list-unstyled mb-0 lifeEventMenu">
                <li class="lifeEventMenuLi lifeDraftLi {{ $event_data->event_is_draft==0 ? 'lifeEventMenuLiActive' : '' }}">
                  <a href="{{ url('life-events') }}" class="lifeEventMenuLink">
                    <span class="lifeEventMenuText">Created Life Events</span>
                    <span class="lifeDraftCount">{{ $event_count }}</span>
                  </a>
                </li>
                <li class="lifeEventMenuLi lifeDraftLi {{ $event_data->event_is_draft==1 ? 'lifeEventMenuLiActive' : '' }}">
                  <a href="{{ url('drafts') }}" class="lifeEventMenuLink lifeDraftMenuLink">
                    <span class="lifeEventMenuText">Drafts</span>
                    <span class="lifeDraftCount">{{ $draft_count }}</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
                  <div class="midPan">
                    <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                      <div class="row infoBodyRow">
                        <div class="col-md-5 infoBodyCol connectionsBody eventImgCol">
                          <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
                            <div class="innerHome">
                              <a href="{{url('home')}}"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                            </div>
                            <div class="feedbackImg">
                              <div class="personalImgAll">
                                <img src="new-design/img/militaryImg.png" class="img-fluid" alt="">
                                <div class="personalImgTitle">Military</div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7 connectionsBody infoBodyCol eventCol midHightControl">
                          <div class="connectionsBody feedbackRight windowHeight windowHeightMid">


                            <form class="xhr_form" method="post" action="api/save-life-event" id="save-life-event">
                            @csrf
                              <div class="eventColHeading">What </div> <img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                              <input type="hidden" name="type_id" value="5">
                              <input type="hidden" name="user_id" value="{{Session::get('userdata')['id']}}">
                              <input type="hidden" name="_event_id" value="{{ $event_data->_event_id }}">
                              <div class="whatForm">
                                  <div class="form-group">
                                    <label for="">What</label>
                                    <select class="form-control forredline" name="sub_category_id">
                    @if($what)
                    	@foreach($what as $w)
                        @php
                          $selected = $w->id==$event_data->event_subcategory_id ? 'selected' : '';
                        @endphp
                                      <option value="{{ $w->id }}" {{ $selected }}>{{ $w->sub_cat_type_name }}</option>
                        @endforeach
                    @endif
                                    </select>
                                  </div>
                                <div class="form-group datingFilterBtn timeOptionSet">
                                  <div class="form-control timeOptionOnly enterFor {{ $event_data->event_dating_search_for==0 ? 'activeTimeOption' : '' }}">Enter (you)</div>
                                  <div class="form-control timeOptionOnly searchFor {{ $event_data->event_dating_search_for==1 ? 'activeTimeOption' : '' }}">Search (for)</div>
                                  <input type="hidden" name="search_for" value="{{ $event_data->event_dating_search_for }}" id="search_for" />
                                </div>
                                  <div class="whatReasonAll">
                                    <div class="form-group">
                                      <label for="">Branch</label>
                                      <select class="form-control forredline" name="military_branch" id="military_branches">
                                        <option value="">Select</option>
                    @if($military_branches)
                    	@foreach($military_branches as $mbranch)
                        @php
                          $selected = $mbranch->mbranch_name==$event_data->event_military_branch ? 'selected' : '';
                        @endphp
                    					<option option="{{ $mbranch->mbranch_name }}" {{ $selected }} data-ranks="{{ $mbranch->military_ranks }}">{{$mbranch->mbranch_name}}</option>
                    	@endforeach
                    @endif
                                      </select>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <label for="">Gender</label>
                                          <select class="form-control forredline formGender" name="gender">
                                            <option value="">Select</option>
                    @if($genders)
                    	@foreach($genders as $gender)
                        @php
                          $selected = $gender->gender_name==$event_data->event_gender ? 'selected' : '';
                        @endphp
                    					<option option="{{ $gender->gender_name }}" {{ $selected }}>{{$gender->gender_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <label for="">Age </label>
                                          <select class="form-control forredline formAge" name="age">
                                            <option value="">Select</option>
                                        @if($age_ranges)
                                          @foreach($age_ranges as $range)
                        @php
                          $selected = $range->m_range_name==$event_data->event_age ? 'selected' : '';
                        @endphp
                                          <option value="{{ $range->m_range_name }}" {{ $selected }}>{{ $range->m_range_name }}</option>
                                          @endforeach
                                        @endif
                                            
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group topFormIcon">
                                          <label for="">Unit Structure</label>
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          
                                          <select class="form-control forredline formUnit" name="military_unit1">
                                            <option value="">Select(1)</option>
                    @if($military_units)
                      @foreach($military_units as $unit)
                        @php
                          $selected = $unit->unit_name==$event_data->event_military_unit1 ? 'selected' : '';
                        @endphp
                              <option option="{{ $unit->unit_name }}" {{ $selected }}>{{$unit->unit_name}}</option>
                      @endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <select class="form-control forredline formUnit" name="military_unit2">
                                            <option value="">Select(2)</option>
                    @if($military_units)
                      @foreach($military_units as $unit)
                        @php
                          $selected = $unit->unit_name==$event_data->event_military_unit2 ? 'selected' : '';
                        @endphp
                              <option option="{{ $unit->unit_name }}" {{ $selected }}>{{$unit->unit_name}}</option>
                      @endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <select class="form-control forredline formUnit" name="military_unit3">
                                            <option value="">Select(3)</option>
                    @if($military_units)
                      @foreach($military_units as $unit)
                        @php
                          $selected = $unit->unit_name==$event_data->event_military_unit3 ? 'selected' : '';
                        @endphp
                              <option option="{{ $unit->unit_name }}" {{ $selected }}>{{$unit->unit_name}}</option>
                      @endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <select class="form-control forredline formUnit" name="military_unit4">
                                            <option value="">Select(4)</option>
                    @if($military_units)
                      @foreach($military_units as $unit)
                        @php
                          $selected = $unit->unit_name==$event_data->event_military_unit4 ? 'selected' : '';
                        @endphp
                              <option option="{{ $unit->unit_name }}" {{ $selected }}>{{$unit->unit_name}}</option>
                      @endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <select class="form-control forredline formUnit" name="military_unit5">
                                            <option value="">Select(5)</option>
                    @if($military_units)
                      @foreach($military_units as $unit)
                        @php
                          $selected = $unit->unit_name==$event_data->event_military_unit5 ? 'selected' : '';
                        @endphp
                              <option option="{{ $unit->unit_name }}" {{ $selected }}>{{$unit->unit_name}}</option>
                      @endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <select class="form-control forredline formUnit" name="military_unit6">
                                            <option value="">Select(6)</option>
                    @if($military_units)
                      @foreach($military_units as $unit)
                        @php
                          $selected = $unit->unit_name==$event_data->event_military_unit6 ? 'selected' : '';
                        @endphp
                              <option option="{{ $unit->unit_name }}" {{ $selected }}>{{$unit->unit_name}}</option>
                      @endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group topFormIcon">
                                          <label style="margin: 10px 0px;">Title</label>
                                          <div class="row">
                                            <div class="col-md-6">
                                              <div class="form-group topFormIcon">
                                                <label for="">Rank/Grade</label>
                                                <select class="form-control forredline formGrade" name="military_rank" id="military_ranks">
                                                  <option value="">Select</option>
                    @if($military_ranks)
                      @foreach($military_ranks as $rank)
                        @php
                          $selected = $rank->rank_name==$event_data->event_military_rank ? 'selected' : '';
                        @endphp
                                  <option option="{{ $rank->rank_name }}" {{ $selected }}>{{$rank->rank_name}}</option>
                      @endforeach
                    @endif
                                                </select>
                                                <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                              </div>
                                            </div>
                                            <div class="col-md-6">
                                              <div class="form-group topFormIcon">
                                                <label for="">MOS</label>
                                                <select class="form-control forredline formMos" name="military_mos">
                                                  <option value="">Select</option>
                    @if($military_mos)
                    	@foreach($military_mos as $mos)
                        @php
                          $selected = $mos->mos_name==$event_data->event_military_mos ? 'selected' : '';
                        @endphp
                    					<option option="{{ $mos->mos_name }}" {{ $selected }}>{{$mos->mos_name}}</option>
                    	@endforeach
                    @endif
                                                </select>
                                                <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                              </div>
                                            </div>
                                            <div class="col-md-6">
                                              <div class="form-group topFormIcon">
                                                <label for="">Title</label>
                                                <select class="form-control forredline formTitle" name="military_title">
                                                  <option value="">Select</option>
                    @if($military_mos_titles)
                    	@foreach($military_mos_titles as $mtitle)
                        @php
                          $selected = $mtitle->mtitle_name==$event_data->event_military_title ? 'selected' : '';
                        @endphp
                    					<option option="{{ $mtitle->mtitle_name }}" {{ $selected }}>{{$mtitle->mtitle_name}}</option>
                    	@endforeach
                    @endif
                                                </select>
                                                <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="eventColHeading">Where </div>
                                <div class="whatForm whereFrom">
                                  <div class="form-group">
                                    <label for="">Country</label>
                                    <select class="form-control forredline formCountry" name="country" id="country">
                              <option value="">Select</option>
            @if($countries)
              @foreach($countries as $country)
                @php
                  $selected = $country->country_name==$where_data->where_country ? 'selected' : '';
                @endphp
                              <option value="{{ $country->country_name }}" {{ $selected }} data-id="{{ $country->_country_id }}">{{ $country->country_name }}</option>
              @endforeach
            @endif
                            </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">State/County/Province</label>
                                    <select class="form-control forredline formState" name="state" id="province">
                                      <option value="">Select</option>
            @if($provinces)
              @foreach($provinces as $province)
                @php
                  $selected = ($where_data && $province->province_name==$where_data->where_state) ? 'selected' : '';
                @endphp
                              <option value="{{ $province->province_name }}" {{ $selected }} data-id="{{ $province->_province_id }}">{{ $province->province_name }}</option>
              @endforeach
            @endif
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">City</label>
                                    <select class="form-control forredline formCity" name="city" id="city">
                                      <option value="">Select</option>
            @if($cities)
              @foreach($cities as $city)
                @php
                  $selected = ($where_data && $city->city_name==$where_data->where_city) ? 'selected' : '';
                @endphp
                              <option value="{{ $city->city_name }}" {{ $selected }} data-id="{{ $city->_city_id }}">{{ $city->city_name }}</option>
              @endforeach
            @endif
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Street Address</label>
                                    <input type="text" class="form-control forredline formAddress" id="" placeholder="123 abc Ave." 
                                    name="street" value="{{ $where_data->where_street }}">
                                  </div>
                                  <div class="form-group">
                                    <label for="">ZIP</label>
                                    <input type="text" class="form-control forredline formZip" id="" placeholder="ZIP" name="zip" value="{{ $where_data->where_zip }}">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Base</label>
                                    <select class="form-control forredline formBase" name="military_base">
                                      <option value="">Select</option>
            @if($military_bases)
              @foreach($military_bases as $base)
                @php
                  $selected = ($where_data && $base->base_name==$where_data->where_base) ? 'selected' : '';
                @endphp
                    					<option option="{{ $base->base_name }}" {{ $selected }}>{{$base->base_name}}</option>
              @endforeach
            @endif
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Orders</label>
                                    <input type="text" class="form-control forredline formOrders" name="military_order" value="{{ $where_data->where_orders }}">
                                  </div>
                                </div>
                                <div class="eventColHeading">When </div>
                                <div class="whatForm whereFrom">
                                  <div class="birthdayDate">
                                    <div class="form-group birthdayMonth">
                                      <label for="number">From</label>
                                      <select class="form-control forredline formMonth" id="" name="when_from_month">
                                        <option value="">Month</option>
                                <option value="01" {{ $when_data->when_from_month=='01'?'selected':'' }}>January</option>
                                <option value="02" {{ $when_data->when_from_month=='02'?'selected':'' }}>February</option>
                                <option value="03" {{ $when_data->when_from_month=='03'?'selected':'' }}>March</option>
                                <option value="04" {{ $when_data->when_from_month=='04'?'selected':'' }}>April</option>
                                <option value="05" {{ $when_data->when_from_month=='05'?'selected':'' }}>May</option>
                                <option value="06" {{ $when_data->when_from_month=='06'?'selected':'' }}>June</option>
                                <option value="07" {{ $when_data->when_from_month=='07'?'selected':'' }}>July</option>
                                <option value="08" {{ $when_data->when_from_month=='08'?'selected':'' }}>August</option>
                                <option value="09" {{ $when_data->when_from_month=='09'?'selected':'' }}>September</option>
                                <option value="10" {{ $when_data->when_from_month=='10'?'selected':'' }}>October</option>
                                <option value="11" {{ $when_data->when_from_month=='11'?'selected':'' }}>November</option>
                                <option value="12" {{ $when_data->when_from_month=='12'?'selected':'' }}>December</option>
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayDay">
                                      <label for="number">Day</label>
                                      <select class="form-control forredline" id="" name="when_from_day">
                                        <option value="">DD</option>
                                        @for($i=1;$i<=31;$i++)
                                          @php
                                            $j = sprintf('%02d', $i)
                                          @endphp
                                         <option value="{{ $j }}" {{ $when_data->when_from_day==$j ? 'selected':'' }}>{{ $j }}</option>
                                        @endfor
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayYear">
                                      <label for="number">Year</label>
                                      <select class="form-control forredline" id="" name="when_from_year">
                                        <option value="">YYYY</option>
                                      @for ($i = date('Y'); $i >= 1900; $i--)
                                        <option value="{{ $i }}" {{ $when_data->when_from_year==$i ? 'selected':'' }}>{{$i}}</option>
                                      @endfor
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                  </div>
                                  <div class="birthdayDate">
                                    <div class="form-group birthdayMonth">
                                      <label for="number">To</label>
                                      <select class="form-control forredline formMonth" id="" name="when_to_month">
                                        <option value="">Month</option>
                                <option value="01" {{ $when_data->when_to_month=='01'?'selected':'' }}>January</option>
                                <option value="02" {{ $when_data->when_to_month=='02'?'selected':'' }}>February</option>
                                <option value="03" {{ $when_data->when_to_month=='03'?'selected':'' }}>March</option>
                                <option value="04" {{ $when_data->when_to_month=='04'?'selected':'' }}>April</option>
                                <option value="05" {{ $when_data->when_to_month=='05'?'selected':'' }}>May</option>
                                <option value="06" {{ $when_data->when_to_month=='06'?'selected':'' }}>June</option>
                                <option value="07" {{ $when_data->when_to_month=='07'?'selected':'' }}>July</option>
                                <option value="08" {{ $when_data->when_to_month=='08'?'selected':'' }}>August</option>
                                <option value="09" {{ $when_data->when_to_month=='09'?'selected':'' }}>September</option>
                                <option value="10" {{ $when_data->when_to_month=='10'?'selected':'' }}>October</option>
                                <option value="11" {{ $when_data->when_to_month=='11'?'selected':'' }}>November</option>
                                <option value="12" {{ $when_data->when_to_month=='12'?'selected':'' }}>December</option>
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayDay">
                                      <label for="number">Day</label>
                                      <select class="form-control forredline" id="" name="when_to_day">
                                <option value="">DD</option>
                                @for($i=1;$i<=31;$i++)
                                  @php
                                    $j = sprintf('%02d', $i)
                                  @endphp
                                 <option value="{{ $j }}" {{ $when_data->when_to_day==$j ? 'selected':'' }}>{{ $j }}</option>
                                @endfor
                              </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayYear">
                                      <label for="number">Year</label>
                                      <select class="form-control forredline" id="" name="when_to_year">
                                <option value="">YYYY</option>
                              @for ($i = date('Y'); $i >= 1900; $i--)
                                <option value="{{ $i }}" {{ $when_data->when_to_year==$i ? 'selected':'' }}>{{$i}}</option>
                              @endfor
                              </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                  </div>
                                </div>
                                <div class="eventColHeading">Who, What, Where, When, Why </div>
                                <div class="whatForm whereFrom">
                                  
                                  <div class="myAllEvents" id="keywords_div">
                        @if($event_data->event_keywords)
                          @php
                            $keywords = explode(',',$event_data->event_keywords);
                          @endphp
                          @foreach($keywords as $i => $keyword)
                                  <div class="addFieldItem keywords" id="{{ $i }}" title="{{ $keyword }}">{{ $keyword }} <span class="removeField" onclick="removeKeyword('{{ $i }}');"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>
                          @endforeach
                        @endif
                                  </div>
                                  <input type="hidden" id="keywords" value="{{ $event_data->event_keywords }}" name="event_keywords">
			                        <div class="input-group lifeEvent">
			                          <input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
			                          <div class="input-group-append">
			                            <a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
			                           </div>
			                        </div>
                                  	<div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                                </div>
                                
                            @include('inc.life_event_img_upload2',['images' => $images, 'event_data' => $event_data])
                              </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
<!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Military</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Military</label>
            <p>This section is based on your service through any of the military branches. This
specific information will help you match with people that you served with.</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>This is the location where you served.</p>
          </div>
          <div class="form-group">
            <label>WHEN:</label>
            <p>This is the period of time when you served.</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
@include('inc.footer')
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).ready(function(){
  $(".enterFor").click(function(){
        $(".enterFor").addClass("activeTimeOption");
        $(".searchFor").removeClass("activeTimeOption");
        $('#search_for').val(0);
    });
    $(".searchFor").click(function(){
        $(".searchFor").addClass("activeTimeOption");
        $(".enterFor").removeClass("activeTimeOption");
        $('#search_for').val(1);
    });
});
$(document).on('change','#military_branches',function(){
	var ranks = $(this).find(':selected').data('ranks');
	/*console.log('ranks => ');
	console.log(ranks);*/
	var military_ranks = '<option value="">Select </option>';
	if(ranks){

		$.each( ranks, function( index, rank ) {
			military_ranks += '<option value="'+rank.rank_name+'">'+rank.rank_name+'</option>';
		});
	}

	$('#military_ranks').html(military_ranks);
});
</script>
<script>
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script>