@include('inc.header')
<div class="innerBodyOnly innerBodyModify">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                  <div class="leftSlidePan closePan">
                    <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                    <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                      @include('inc.life_event_left_menu')
                    </div>
                  </div>
                  <div class="midPan">
                    <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                      <div class="row infoBodyRow">
                        <div class="col-md-5 infoBodyCol connectionsBody eventImgCol">
                          <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
                            <div class="innerHome">
                              <a href="{{url('home')}}"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                            </div>
                            <div class="feedbackImg">
                              <div class="personalImgAll">
                                <img src="new-design/img/datingImg.png" class="img-fluid" alt="">
                                <div class="personalImgTitle">Dating</div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7 connectionsBody infoBodyCol eventCol midHightControl">
                          <div class="connectionsBody feedbackRight windowHeight windowHeightMid">


                            <form class="xhr_form" method="post" action="api/life-event-upload" id="life-event-upload">
                            @csrf
                              <div class="eventColHeading">What</div><img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                              <input type="hidden" name="type_id" value="2">
                              <input type="hidden" name="user_id" value="{{Session::get('userdata')['id']}}">
                                <div class="whatForm whatFormDationg">
                                  <div class="form-group datingSpecific">
                                    <label for="">What</label>
                                    <select class="form-control" id="whatReason" name="sub_category_id">
                    @if($what)
                    	@foreach($what as $w)
                                      <option value="{{ $w->id }}">{{ $w->sub_cat_type_name }}</option>
                        @endforeach
                    @endif
                                      {{-- <option value="0">General</option>
                                      <option value="1">Specific</option> --}}
                                    </select>
                                  </div>
                                  <div class="form-group datingFilterBtn timeOptionSet">
                                    <div class="form-control timeOptionOnly enterFor activeTimeOption">Enter (you)</div>
                                    <div class="form-control timeOptionOnly searchFor">Search (for)</div>
                                    <input type="hidden" name="search_for" value="0" id="search_for" />
                                  </div>
                                  <div class="form-group text-center" style="color: #3b71b9;border: 2px solid #3b71b9;background: #f8f8f8;">
                                    Enter as MUCH or as little as you want. Enter <br>what you want others to search about you!
                                  </div>
                                  <div class=" vehicleForm" style="padding: 0px 30px;">
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group" id="genders_div">
                                          <label for="">Gender</label>
                                          <select class="form-control formGender" name="gender" id="gender">
                                            <option value="" >Select</option>
                    @if($genders)
                    	@foreach($genders as $gender)
                    					<option option="{{ $gender->gender_name }}">{{$gender->gender_name}}</option>
                    	@endforeach
                    @endif
                                            <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Age</label>
                                          <!-- <input type="number" class="form-control formAge" name="age"> -->
                                          <select class="form-control formAge" name="age">
                                            <option value="">Select</option>
                                @for($i=18;$i<100;$i++)
                                            <option value="{{ $i }}">{{ $i }}{{$i==18?'+':''}}</option>
                                @endfor
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      	<div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Height</label>
                                            <div class="height_weight">
                                              <div class="height_weight_Only">
                                              	<input type="number" name="" class="form-control formHeight" name="height" id="height" value="0">
                                                
                                                {{-- <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt=""> --}}
                                              </div>
                                              <div class="form-group timeOptionSet">
                                                <div class="form-control timeOptionOnly activeTimeOption" id="height_measure_in">in</div>
                                                <div class="form-control timeOptionOnly" id="height_measure_cm" style="border-top-left-radius: 0 !important;border-bottom-left-radius: 0 !important;">cm</div>
                                                <input type="hidden" name="height_measure" id="height_measure" value="in">
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Height General</label>
                                            <select class="form-control formHeight" name="height_general">
                                              <option value="">Select</option>
                	@if($height_general)
                    	@foreach($height_general as $hg)
                    					<option option="{{ $hg->hg_name }}">{{$hg->hg_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Weight</label>
                                            <div class="height_weight">
                                              <div class="height_weight_Only">
                                              	<input type="number" class="form-control formWeight" name="weight" id="weight" value="0">
                                                {{-- <select >
                                                  <option>1</option>
                                                  <option>2</option>
                                                </select>
                                                <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt=""> --}}
                                              </div>
                                              <div class="form-group timeOptionSet">
                                                <div class="form-control timeOptionOnly activeTimeOption" id="weight_measure_kg">kg</div>
                                                <div class="form-control timeOptionOnly" id="weight_measure_lbs" style="border-top-left-radius: 0 !important;border-bottom-left-radius: 0 !important;">lbs</div>
                                                <input type="hidden" name="weight_measure" id="weight_measure" value="kg">
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Weight General</label>
                                            <select class="form-control formWeight" name="weight_general">
                                              <option value="" >Select</option>
                    @if($weight_general)
                    	@foreach($weight_general as $wg)
                    					<option option="{{ $wg->wg_name }}">{{$wg->wg_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="ethnicities_div">
                                          <label for="">Ethnicity</label>
                                          <select class="form-control formEthnicity" id="ethnicity" name="ethnicity">
                                            <option value="" >Select</option>
                    @if($ethnicities)
                    	@foreach($ethnicities as $ethnicity)
                    					<option option="{{ $ethnicity->ethnicity_name }}">{{$ethnicity->ethnicity_name}}</option>
                    	@endforeach
                    @endif
                                            <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="races_div">
                                          <label for="">Race</label>
                                          <select class="form-control formRace" name="race" id="race">
                                            <option value="" >Select</option>
                    @if($races)
                    	@foreach($races as $race)
                    					<option option="{{ $race->race_name }}">{{$race->race_name}}</option>
                    	@endforeach
                    @endif
                                            <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Nationality</label>
                                          <select class="form-control formNationality" name="nationality">
                                            <option value="" >Select</option>
                    @if($nationalities)
                    	@foreach($nationalities as $country)
                    					<option option="{{ $country->country_name }}">{{$country->country_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Facial Hair</label>
                                            <select class="form-control formFacialHair" name="facial_hair">
                                              <option value="" >Select</option>
                    @if($facial_hairs)
                    	@foreach($facial_hairs as $fh)
                    					<option option="{{ $fh->fh_name }}">{{$fh->fh_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Orientation</label>
                                          <select class="form-control formOrientation" name="orientation">
                                            <option value="" >Select</option>
                    @if($orientations)
                    	@foreach($orientations as $orientation)
                    					<option option="{{ $orientation->orientation_name }}">{{$orientation->orientation_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Body Style</label>
                                          <select class="form-control formBodyStyle" name="body_style">
                                            <option value="" >Select</option>
                    @if($body_styles)
                    	@foreach($body_styles as $bs)
                    					<option option="{{ $bs->bs_name }}">{{$bs->bs_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Body Hair</label>
                                            <select class="form-control formBodyHair" name="body_hair">
                                              <option value="" >Select</option>
                    @if($body_hairs)
                    	@foreach($body_hairs as $bh)
                    					<option option="{{ $bh->bh_name }}">{{$bh->bh_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Skin Tone</label>
                                          <select class="form-control formSkinTone" name="skin_tonne">
                                            <option value="" >Select</option>
                    @if($skin_tonnes)
                    	@foreach($skin_tonnes as $st)
                    					<option option="{{ $st->st_name }}">{{$st->st_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Glasses</label>
                                            <select class="form-control formSGlasses" name="glasses">
                                              <option value="">Select</option>
                                              <option value="Yes">Yes</option>
                                              <option value="No">No</option>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="hair_colors_div">
                                          <label for="">Hair Color</label>
                                          <select class="form-control formHairColor" id="hair_color" name="hair_color">
                                            <option value="">Select</option>
                    @if($hair_colors)
                    	@foreach($hair_colors as $hc)
                    					<option option="{{ $hc->hc_name }}">{{$hc->hc_name}}</option>
                    	@endforeach
                    @endif
                              <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group" id="eye_colors_div">
                                          <label for="">Eye Color</label>
                                          <select class="form-control formEyeColor" name="eye_color" id="eye_color">
                                            <option value="">Select</option>
                    @if($eye_colors)
                    	@foreach($eye_colors as $ec)
                    					<option option="{{ $ec->ec_name }}">{{$ec->ec_name}}</option>
                    	@endforeach
                    @endif
                              <option value="add">Add</option>
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Party</label>
                                            <select class="form-control formParty" name="party">
                                              <option value="">Select</option>
                                              <option value="Yes">Yes</option>
                                              <option value="No">No</option>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat" name="smoke">
                                          <div class="form-group">
                                            <label for="">Smoke</label>
                                            <select class="form-control formSmoke">
                                              <option value="">Select</option>
                                              <option value="Yes">Yes</option>
                                              <option value="No">No</option>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Drink</label>
                                            <select class="form-control formDrink" name="drink">
                                              <option value="">Select</option>
                                              <option value="Yes">Yes</option>
                                              <option value="No">No</option>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Tattoo</label>
                                            <select class="form-control formTatto" name="tattoo">
                                              <option value="">Select</option>
                                              <option value="Yes">Yes</option>
                                              <option value="No">No</option>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for=""># Tattoos</label>
                                            <select class="form-control formTatto" name="tattoo_type">
                                              <option value="">Select</option>
                    @if($tattoos)
                    	@foreach($tattoos as $tattoo)
                    					<option option="{{ $tattoo->tattoo_name }}">{{$tattoo->tattoo_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Favorite Food</label>
                                            <select class="form-control formFood" name="fav_food">
                                              <option value="">Select</option>
                    @if($foods)
                    	@foreach($foods as $food)
                    					<option option="{{ $food->food_name }}">{{$food->food_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Food Type</label>
                                            <select class="form-control formFoodType" name="food_type">
                                              <option value="">Select</option>
                    @if($food_types)
                    	@foreach($food_types as $ft)
                    					<option option="{{ $ft->ft_name }}">{{$ft->ft_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Sport</label>
                                            <select class="form-control formSport" name="sport">
                                              <option value="">Select</option>
                    @if($sports)
                    	@foreach($sports as $sport)
                    					<option option="{{ $sport->sport_name }}">{{$sport->sport_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Favorite Team</label>
                                            <input type="text" class="form-control formFavoriteTeam" name="fav_team">
                                            {{-- <select >
                                              <option>1</option>
                                              <option>2</option>
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt=""> --}}
                                          </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Political Affiliation</label>
                                          <select class="form-control formPoliticalAffiliation" name="political">
                                            <option value="">Select</option>
                    @if($politicals)
                    	@foreach($politicals as $political)
                    					<option option="{{ $political->political_name }}">{{$political->political_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Religion</label>
                                          <select class="form-control formReligion" name="religion">
                                            <option value="">Select</option>
                    @if($religions)
                    	@foreach($religions as $religion)
                    					<option option="{{ $religion->religion_name }}">{{$religion->religion_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Zodiac Sign</label>
                                            <select class="form-control formZodiacSign" name="zodiac_sign">
                                              <option value="">Select</option>
                    @if($zodiac_signs)
                    	@foreach($zodiac_signs as $zodiac)
                    					<option option="{{ $zodiac->zodiac_name }}">{{$zodiac->zodiac_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Birth Stone</label>
                                            <select class="form-control formBarthStone" name="birth_stone">
                                              <option value="">Select</option>
                    @if($zodiac_signs)
                    	@foreach($zodiac_signs as $zodiac)
                    					<option option="{{ $zodiac->zodiac_name }}">{{$zodiac->zodiac_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Affiliations</label>
                                            <select class="form-control formAffiliation" name="affiliation">
                                              <option value="">Select</option>
                    @if($affiliations)
                    	@foreach($affiliations as $affiliation)
                    					<option option="{{ $affiliation->affiliation_name }}">{{$affiliation->affiliation_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Career</label>
                                            <select class="form-control formCareer" name="career">
                                              <option value="">Select</option>
                    @if($occupations)
                    	@foreach($occupations as $occupation)
                    					<option option="{{ $occupation->occupation_name }}">{{$occupation->occupation_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Financial</label>
                                            <select class="form-control formFinancial" name="financial">
                                              <option value="">Select</option>
                    @if($financials)
                    	@foreach($financials as $fs)
                    					<option option="{{ $fs->fs_name }}">{{$fs->fs_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                        <div class="col-md-6 whatReason2 removedWhat">
                                          <div class="form-group">
                                            <label for="">Relationship</label>
                                            <select class="form-control formRelationship" name="relationship">
                                              <option value="">Select</option>
                    @if($relationships)
                    	@foreach($relationships as $relationship)
                    					<option option="{{ $relationship }}">{{$relationship}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                            <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                          </div>
                                        </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Age (Range)</label>
                                          <select class="form-control formAge" name="age_range">
                                            <option value="">Select</option>
                    @if($age_ranges)
                    	@foreach($age_ranges as $range)
                    					<option option="{{ $range->range_name }}">{{$range->range_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="">Loking For</label>
                                          <select class="form-control formGender" name="looking_for">
                                            <option value="">Select</option>
                    @if($genders)
                    	@foreach($genders as $gender)
                    					<option option="{{ $gender->gender_name }}">{{$gender->gender_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="interestsSet">

                                          <div class="form-group interestsPicker">
                                            <label for="">Interests</label>
                                            <select name="basic" id="ex-multiselect" multiple name="interests">
                                              <option value="">Select</option>
                    @if($interests)
                    	@foreach($interests as $interest)
                    					<option option="{{ $interest->interest_name }}">{{$interest->interest_name}}</option>
                    	@endforeach
                    @endif
                                            </select>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="eventColHeading">Where </div>
                                <div class="whatForm whereFrom">
                                  <div class="form-group">
                                    <label for="">Country</label>
                                    <select class="form-control formCountry" name="country" id="country">
                              <option value="">Select</option>
            @if($countries)
              @foreach($countries as $country)
                              <option value="{{ $country->country_name }}" data-id="{{ $country->_country_id }}">{{ $country->country_name }}</option>
              @endforeach
            @endif
                              {{-- <option>1</option>
                              <option>2</option> --}}
                            </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">State/County/Province</label>
                                    <select class="form-control formState" name="state" id="province">
                                      <option value="">Select</option>
                                      {{-- <option>1</option>
                                      <option>2</option> --}}
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">City</label>
                                    <select class="form-control formCity" name="city" id="city">
                                      <option value="">Select</option>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Radius (City)</label>
                                    <select class="form-control formAddress" id="" name="radius">
                                        <option value="">Select</option>
                  @if($radius)
                      @foreach($radius as $r)
                              <option option="{{ $r->radius_value }}">{{$r->radius_value}}</option>
                      @endforeach
                    @endif
                                      </select>
                                  </div>
                                </div>
                                <div class="eventColHeading">When </div>
                                <div class="whatForm whereFrom">
                                  <div class="whenTitleText">
                                    <div class="whenTitle">Availability</div>
                                    <div class="whenText">Let people know when ready to meet</div>
                                  </div>
                                  <div class="birthdayDate timeframeSet">
                                    <div class="form-group birthdayMonth">
                                      <label for="number">Timeframe</label>
                                      <select class="form-control" id="" name="timeframe">
                                        <option value="">Select</option>
                	@if($availabilities)
                    	@foreach($availabilities as $availability)
                    					<option option="{{ $availability->availability_name }}">{{$availability->availability_name}}</option>
                    	@endforeach
                    @endif
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayYear">
                                      <label for="number">&nbsp;</label>
                                      <select class="form-control" id="" name="time_month">
                                        <option value="">Select</option>
                                        <option value="January">January</option>
                                        <option value="February">February</option>
                                        <option value="March">March</option>
                                        <option value="April">April</option>
                                        <option value="May">May</option>
                                        <option value="June">June</option>
                                        <option value="July">July</option>
                                        <option value="August">August</option>
                                        <option value="September">September</option>
                                        <option value="October">October</option>
                                        <option value="November">November</option>
                                        <option value="December">December</option>
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                  </div>
                                </div>
                                <div class="eventColHeading">Who, What, Where, When, Why </div>
                                <div class="whatForm whereFrom">
                                  {{-- <div class="addFieldItem">Samgsung <span class="removeField"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>
                                  <div class="addFieldItem">Dog <span class="removeField"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>
                                  <div class="addFieldItem">Treehouse <span class="removeField"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>
                                  <div class="input-group lifeEvent">
                                    <input type="text" class="form-control" placeholder="Life Event Keyword(s)">
                                    <div class="input-group-append">
                                      <div class="btn btn-add">Add</div>
                                     </div>
                                  </div> --}}
                                  	<div class="myAllEvents" id="keywords_div"></div>
			                        <input type="hidden" id="keywords" value="" name="event_keywords">
			                        <div class="input-group lifeEvent">
			                          <input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
			                          <div class="input-group-append">
			                            <a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
			                           </div>
			                        </div>
                                  	<div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                                </div>
                                @include('inc.life_event_img_upload')
                               {{--  <div class="eventColHeading">Photo Gallery</div>
                                <div class="whatForm whereFrom">
                                  <div class="photoGalleryScroll">
                                    <div class="photoGallery">
                                      <div class="photoGalleryOnly">
                                        <img src="https://cdn.pixabay.com/photo/2017/09/25/13/12/dog-2785074__340.jpg" class="img-fluid photoGalleryImg" alt="">
                                        <img src="new-design/img/removeField.png" class="img-fluid photoGalleryImgRemove" alt="">
                                      </div>
                                      <div class="photoGalleryOnly">
                                        <img src="new-design/img/addGalleryImage.png" class="img-fluid photoGalleryImg" alt="">
                                      </div>
                                      <!-- <div class="anotherLocation">Add more photo</div> -->
                                    </div>
                                  </div>
                                </div> --}}

                                {{-- <div class="whatForm whatFormBtnAll">
                                  <div class="whatFormBtnSE">
                                    <div class="btn whatFormBtn">Add Another</div>
                                    <div class="btn whatFormBtn">Save and Exit</div>
                                  </div>

                                  <div class="btn-Edit-save btn-feedback btn-premium">
                                    <button class="btn btn-Edit btn-save">what'scommon <br><span>upload your life event</span></button>
                                  </div>
                                </div> --}}
                              </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
 <!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Dating</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Dating</label>
            <p>This section will ask specific categories for your dating preference. Dating life event has two(2) options: either enter your details for others to search for you or search for someone by entering specific information you like.</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>This is where your dating findings will start. Just enter the specific location, and everything will follow. Match-making will happen when matched with other users using your preferred radius.</p>
          </div>
          <div class="form-group">
            <label>WHEN:</label>
            <p>Availability is essential to start building and working with your relationship. Just enter your availability timeframe/month.</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
@include('inc.footer')
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).ready(function(){
	$(".enterFor").click(function(){
        $(".enterFor").addClass("activeTimeOption");
        $(".searchFor").removeClass("activeTimeOption");
        $('#search_for').val(0);
    });
    $(".searchFor").click(function(){
        $(".searchFor").addClass("activeTimeOption");
        $(".enterFor").removeClass("activeTimeOption");
        $('#search_for').val(1);
    });
});
$(document).ready(function(){
  $("#weight_measure_kg").click(function(){
    $("#weight_measure_kg").addClass("activeTimeOption");
    $("#weight_measure_lbs").removeClass("activeTimeOption");
    $('#weight_measure').val('kg');
    var weight = $('#weight').val();
    var changed_weight = (weight*0.453592).toFixed(2);
    $('#weight').val(changed_weight);
  });
  $("#weight_measure_lbs").click(function(){
    $("#weight_measure_lbs").addClass("activeTimeOption");
    $("#weight_measure_kg").removeClass("activeTimeOption");
    $('#weight_measure').val('lbs');
    var weight = $('#weight').val();
    var changed_weight = (weight*2.20462).toFixed(2);
    $('#weight').val(changed_weight);
  });
  $("#height_measure_in").click(function(){
    $("#height_measure_in").addClass("activeTimeOption");
    $("#height_measure_cm").removeClass("activeTimeOption");
    $('#height_measure').val('in');
    var height = $('#height').val();
    var changed_height = (height*0.393701).toFixed(2);
    $('#height').val(changed_height);
  });
  $("#height_measure_cm").click(function(){
    $("#height_measure_cm").addClass("activeTimeOption");
    $("#height_measure_in").removeClass("activeTimeOption");
    $('#height_measure').val('cm');
    var height = $('#height').val();
    var changed_height = (height*2.54).toFixed(2);
    $('#height').val(changed_height);
  });
});

$("#whatReason").change(function(){
    if($(this).val() == "5") {
        $('.whatReason1').addClass('removedWhat');
        $('.whatReason2').removeClass('removedWhat');
    } else {
        $('.whatReason2').addClass('removedWhat');
        $('.whatReason1').removeClass('removedWhat');
     }
});
$(document).on('change','#eye_color',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=eye_colors&column_name=ec_name&status_column=ec_is_active');
  }
});
$(document).on('change','#hair_color',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=hair_colors&column_name=hc_name&status_column=hc_is_active');
  }
});
$(document).on('change','#race',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=races&column_name=race_name&status_column=race_is_active');
  }
});
$(document).on('change','#ethnicity',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=ethnicities&column_name=ethnicity_name&status_column=ethnicity_is_active');
  }
});
$(document).on('change','#gender',function(){
  if($(this).find(':selected').val()=='add'){
    var user_id = '{{ Session::get("userdata")["id"] }}';
    open_modal('add-master','user_id='+user_id+'&db_name=genders&column_name=gender_name&status_column=gender_is_active');
  }
});
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="" hidden="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script>