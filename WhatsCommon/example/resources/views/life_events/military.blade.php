@include('inc.header')
<div class="innerBodyOnly innerBodyModify">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                  <div class="leftSlidePan closePan">
                    <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                    <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                     @include('inc.life_event_left_menu')
                    </div>
                  </div>
                  <div class="midPan">
                    <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                      <div class="row infoBodyRow">
                        <div class="col-md-5 infoBodyCol connectionsBody eventImgCol">
                          <div class="connectionsBody feedbackEventImg windowHeight windowHeightMid">
                            <div class="innerHome">
                              <a href="{{url('home')}}"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                            </div>
                            <div class="feedbackImg">
                              <div class="personalImgAll">
                                <img src="new-design/img/militaryImg.png" class="img-fluid" alt="">
                                <div class="personalImgTitle">Military</div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7 connectionsBody infoBodyCol eventCol midHightControl">
                          <div class="connectionsBody feedbackRight windowHeight windowHeightMid">


                            <form class="xhr_form" method="post" action="api/life-event-upload" id="life-event-upload">
                            @csrf
                              <div class="eventColHeading">What </div> <img onclick="open_popup();" class="img-fluid iSign" src="new-design/img/i-sign.png">
                              <input type="hidden" name="type_id" value="5">
                              <input type="hidden" name="user_id" value="{{Session::get('userdata')['id']}}">
                              <div class="whatForm">
                                  <div class="form-group">
                                    <label for="">What</label>
                                    <select class="form-control " name="sub_category_id">
                    @if($what)
                    	@foreach($what as $w)
                                      <option value="{{ $w->id }}">{{ $w->sub_cat_type_name }}</option>
                        @endforeach
                    @endif
                                    </select>
                                  </div>
                                  <div class="form-group datingFilterBtn timeOptionSet">
                                    <div class="form-control timeOptionOnly enterFor activeTimeOption">Enter (you)</div>
                                    <div class="form-control timeOptionOnly searchFor">Search (for)</div>
                                    <input type="hidden" name="search_for" value="0" id="search_for" />
                                  </div>
                                  <div class="whatReasonAll">
                                    <div class="form-group">
                                      <label for="">Branch</label>
                                      <select class="form-control " name="military_branch" id="military_branches">
                                        <option value="">Select</option>
                    @if($military_branches)
                    	@foreach($military_branches as $mbranch)
                    					<option option="{{ $mbranch->mbranch_name }}" data-ranks="{{ $mbranch->military_ranks }}">{{$mbranch->mbranch_name}}</option>
                    	@endforeach
                    @endif
                                      </select>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <label for="">Gender</label>
                                          <select class="form-control formGender" name="gender">
                                            <option value="">Select</option>
                    @if($genders)
                    	@foreach($genders as $gender)
                    					<option option="{{ $gender->gender_name }}">{{$gender->gender_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <label for="">Age </label>
                                          <select class="form-control formAge" name="age">
                                            <option value="">Select</option>
                                        @if($age_ranges)
                                          @foreach($age_ranges as $range)
                                          <option value="{{ $range->m_range_name }}">{{ $range->m_range_name }}</option>
                                          @endforeach
                                        @endif
                                            
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group topFormIcon">
                                          <label for="">Unit Structure</label>
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          
                                          <select class="form-control formUnit" name="military_unit1">
                                            <option value="">Select(1)</option>
                    @if($military_units)
                    	@foreach($military_units as $unit)
                    					<option option="{{ $unit->unit_name }}">{{$unit->unit_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <select class="form-control formUnit" name="military_unit2">
                                            <option value="">Select(2)</option>
                    @if($military_units)
                    	@foreach($military_units as $unit)
                    					<option option="{{ $unit->unit_name }}">{{$unit->unit_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <select class="form-control formUnit" name="military_unit3">
                                            <option value="">Select(3)</option>
                    @if($military_units)
                    	@foreach($military_units as $unit)
                    					<option option="{{ $unit->unit_name }}">{{$unit->unit_name}}</option>
                    	@endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <select class="form-control formUnit" name="military_unit4">
                                            <option value="">Select(4)</option>
                    @if($military_units)
                      @foreach($military_units as $unit)
                              <option option="{{ $unit->unit_name }}">{{$unit->unit_name}}</option>
                      @endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <select class="form-control formUnit" name="military_unit5">
                                            <option value="">Select(5)</option>
                    @if($military_units)
                      @foreach($military_units as $unit)
                              <option option="{{ $unit->unit_name }}">{{$unit->unit_name}}</option>
                      @endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group topFormIcon">
                                          <select class="form-control formUnit" name="military_unit6">
                                            <option value="">Select(6)</option>
                    @if($military_units)
                      @foreach($military_units as $unit)
                              <option option="{{ $unit->unit_name }}">{{$unit->unit_name}}</option>
                      @endforeach
                    @endif
                                          </select>
                                          <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group topFormIcon">
                                          <label style="margin: 10px 0px;">Title</label>
                                          <div class="row">
                                            <div class="col-md-6">
                                              <div class="form-group topFormIcon">
                                                <label for="">Rank/Grade</label>
                                                <select class="form-control formGrade" name="military_rank" id="military_ranks">
                                                  <option value=""="">Select</option>
                                                </select>
                                                <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                              </div>
                                            </div>
                                            {{-- <div class="col-md-6">
                                              <div class="form-group topFormIcon">
                                                <label for="">Grade</label>
                                                <select class="form-control formGrade">
                                                  <option>Select</option>
                                                </select>
                                                <img src="img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                              </div>
                                            </div> --}}
                                            <div class="col-md-6">
                                              <div class="form-group topFormIcon">
                                                <label for="">MOS</label>
                                                <select class="form-control formMos" name="military_mos">
                                                  <option value="">Select</option>
                    @if($military_mos)
                    	@foreach($military_mos as $mos)
                    					<option option="{{ $mos->mos_name }}">{{$mos->mos_name}}</option>
                    	@endforeach
                    @endif
                                                </select>
                                                <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                              </div>
                                            </div>
                                            <div class="col-md-6">
                                              <div class="form-group topFormIcon">
                                                <label for="">Title</label>
                                                <select class="form-control formTitle" name="military_title">
                                                  <option value="">Select</option>
                    @if($military_mos_titles)
                    	@foreach($military_mos_titles as $mtitle)
                    					<option option="{{ $mtitle->mtitle_name }}">{{$mtitle->mtitle_name}}</option>
                    	@endforeach
                    @endif
                                                </select>
                                                <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="eventColHeading">Where </div>
                                <div class="whatForm whereFrom">
                                  <div class="form-group">
                                    <label for="">Country</label>
                                    <select class="form-control formCountry" name="country" id="country">
                              <option value="">Select</option>
            @if($countries)
              @foreach($countries as $country)
                              <option value="{{ $country->country_name }}" data-id="{{ $country->_country_id }}">{{ $country->country_name }}</option>
              @endforeach
            @endif
                              {{-- <option>1</option>
                              <option>2</option> --}}
                            </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">State/County/Province</label>
                                    <select class="form-control formState" name="state" id="province">
                                      <option value="">Select</option>
                                      {{-- <option>1</option>
                                      <option>2</option> --}}
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">City</label>
                                    <select class="form-control formCity" name="city" id="city">
                                      <option value="">Select</option>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Street Address</label>
                                    <input type="text" class="form-control formAddress" id="" placeholder="123 abc Ave." 
                                    name="street">
                                  </div>
                                  <div class="form-group">
                                    <label for="">ZIP</label>
                                    <input type="text" class="form-control formZip" id="" placeholder="ZIP" name="zip">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Base</label>
                                    <select class="form-control formBase" name="military_base">
                                      <option value="">Select</option>
            		@if($military_bases)
                    	@foreach($military_bases as $base)
                    					<option option="{{ $base->base_name }}">{{$base->base_name}}</option>
                    	@endforeach
                    @endif
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group">
                                    <label for="">Orders</label>
                                    <input type="text" class="form-control formOrders" name="military_order">
                                    {{-- <select class="form-control formOrders">
                                      <option>1</option>
                                      <option>2</option>
                                    </select>
                                    <img src="img/selectIcon.png" class="img-fluid selectIcon" alt=""> --}}
                                  </div>
                                </div>
                                <div class="eventColHeading">When </div>
                                <div class="whatForm whereFrom">
                                  <div class="birthdayDate">
                                    <div class="form-group birthdayMonth">
                                      <label for="number">From</label>
                                      <select class="form-control formMonth" id="" name="when_from_month">
                                        <option value="">Month</option>
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayDay">
                                      <label for="number">Day</label>
                                      <select class="form-control" id="" name="when_from_day">
                                        <option value="">DD</option>
                                        @for($i=1;$i<=31;$i++)
                                          @php
                                            $j = sprintf('%02d', $i)
                                          @endphp
                                         <option value="{{ $j }}">{{ $j }}</option>
                                        @endfor
                                        {{-- <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option> --}}
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayYear">
                                      <label for="number">Year</label>
                                      <select class="form-control" id="" name="when_from_year">
                                        <option value="">YYYY</option>
                                      @for ($i = date('Y'); $i >= 1900; $i--)
                                        <option value="{{ $i }}">{{$i}}</option>
                                      @endfor
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                  </div>
                                  <div class="birthdayDate">
                                    <div class="form-group birthdayMonth">
                                      <label for="number">To</label>
                                      <select class="form-control formMonth" id="" name="when_to_month">
                                        <option value="">Month</option>
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                      </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayDay">
                                      <label for="number">Day</label>
                                      <select class="form-control" id="" name="when_to_day">
                                <option value="">DD</option>
                                @for($i=1;$i<=31;$i++)
                                  @php
                                    $j = sprintf('%02d', $i)
                                  @endphp
                                 <option value="{{ $j }}">{{ $j }}</option>
                                @endfor
                              </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                    <div class="form-group birthdayYear">
                                      <label for="number">Year</label>
                                      <select class="form-control" id="" name="when_to_year">
                                <option value="">YYYY</option>
                              @for ($i = date('Y'); $i >= 1900; $i--)
                                <option value="{{ $i }}">{{$i}}</option>
                              @endfor
                              </select>
                                      <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                    </div>
                                  </div>
                                </div>
                                <div class="eventColHeading">Who, What, Where, When, Why </div>
                                <div class="whatForm whereFrom">
                                  
                                  	<div class="myAllEvents" id="keywords_div"></div>
			                        <input type="hidden" id="keywords" value="" name="event_keywords">
			                        <div class="input-group lifeEvent">
			                          <input type="text" class="form-control" id="add_keyword" placeholder="Life Event Keyword(s)">
			                          <div class="input-group-append">
			                            <a onclick="addKeyword();" class="btn btn-add" style="text-decoration: none;">Add</a>
			                           </div>
			                        </div>
                                  	<div class="inputBellowText">Ex: Word1, Word2, Word3</div>
                                </div>
                                
                                @include('inc.life_event_img_upload')

                                {{-- <div class="whatForm whatFormBtnAll">
                                  <div class="whatFormBtnSE">
                                    <div class="btn whatFormBtn">Add Another</div>
                                    <div class="btn whatFormBtn">Save and Exit</div>
                                  </div>

                                  <div class="btn-Edit-save btn-feedback btn-premium">
                                    <button class="btn btn-Edit btn-save">what'scommon <br><span>upload your life event</span></button>
                                  </div>
                                </div> --}}
                              </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
<!-- Modal starts -->       
<div class="modal fade" id="life-event-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header text-center" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3" >Military</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center" >
        <div class="whatForm">
          <div class="form-group">
            <label>WHAT: Military</label>
            <p>This section is based on your service through any of the military branches. This
specific information will help you match with people that you served with.</p>
          </div>
          <div class="form-group">
            <label>WHERE:</label>
            <p>This is the location where you served.</p>
          </div>
          <div class="form-group">
            <label>WHEN:</label>
            <p>This is the period of time when you served.</p>
          </div>
          <div class="form-group">
            <label>W5:</label>
            <p>You're free to enter any keyword(s) that might connect to your life event. All the keywords you're going to enter will possibly match someone with the same life event, and keyword(s) entered. In that way, you can see all your possible connections. You do it yourself: enter facts only.</p>
          </div>
          <div class="form-group">
            <label>PHOTO GALLERY:</label>
            <p>The images or photos uploaded works like your reciept. Once matched with other users who have the same Life Event, it will be easy to find their perfect match or connection if uploaded photos are familiar. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
@include('inc.footer')
<script type="text/javascript">
function open_popup(){
  $('#life-event-popup').modal('show');
}
$(document).ready(function(){
  $(".enterFor").click(function(){
        $(".enterFor").addClass("activeTimeOption");
        $(".searchFor").removeClass("activeTimeOption");
        $('#search_for').val(0);
    });
    $(".searchFor").click(function(){
        $(".searchFor").addClass("activeTimeOption");
        $(".enterFor").removeClass("activeTimeOption");
        $('#search_for').val(1);
    });
});
$(document).on('change','#military_branches',function(){
	var ranks = $(this).find(':selected').data('ranks');
	/*console.log('ranks => ');
	console.log(ranks);*/
	var military_ranks = '<option value="">Select </option>';
	if(ranks){

		$.each( ranks, function( index, rank ) {
			military_ranks += '<option value="'+rank.rank_name+'">'+rank.rank_name+'</option>';
		});
	}

	$('#military_ranks').html(military_ranks);
});
</script>
<script>
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value=""="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
</script>