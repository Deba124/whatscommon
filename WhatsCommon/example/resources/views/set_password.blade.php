<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>What's Common</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  @include('inc.css')
  <link href="css/toastr.min.css" rel="stylesheet">
<body>
  <section id="login" class="signUpBg">
    <div class="container">
      <div class="AllLogin">
        <div class="loginForm" style="max-width: fit-content;">
          <div class="loginLogo">
            <img src="img/signUpLogo.png" class="img-fluid" alt="">
          </div>

          <div class="signUpHeading">Change Password</div>
          <div class="loginText" style="color: #000;">Fill in your information</div>

          <form action="set-password" class="signUpForm xhr_form" id="setPassword">
            @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="inputText form-control" name="otp" id="otp" required/>
                  <span class="floating-label">Verification Code</span>
                </div>
                <p class="text-danger" id="otp_error"></p>
              </div>
              
              <div class="col-md-6">
                <div class="form-group">
                  <input type="password" class="inputText form-control" name="password" id="password" required/>
                  <span class="floating-label">Password</span>
                  {{-- <img src="img/passwordVisiable2.png" class="img-fluid passwordVisiable" alt="" onclick="showHidePassword('password');"> --}}
                  <img src="new-design/img/eye-open.png" id="password-img" class="img-fluid passwordVisiable" alt="" onclick="showHidePassword('password');">
                </div>
                <p class="text-danger" id="password_error"></p>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="password" class="inputText form-control" name="confirm_password" id="confirm_password" required/>
                  <span class="floating-label">Re-type Password</span>
                  {{-- <img src="img/passwordVisiable2.png" class="img-fluid passwordVisiable" alt="" onclick="showHidePassword('confirm_password');"> --}}
                  <img src="new-design/img/eye-open.png" id="confirm_password-img" class="img-fluid passwordVisiable" alt="" onclick="showHidePassword('confirm_password');">
                </div>
                <p class="text-danger" id="confirm_password_error"></p>
              </div>
              
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group" id="responseDiv" style="display: none;"></div>
              </div>
            </div>
            <button type="submit" class="btn btn-proceed">Proceed</button>
          </form>
        </div>
      </div>
    </div>
  </section>
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  <script src="js/toastr.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      toastr.options = {
        "closeButton"       : false,
        "debug"             : false,
        "newestOnTop"       : true,
        "progressBar"       : true,
        "positionClass"     : "toast-bottom-right",
        "preventDuplicates" : false,
        "onclick"           : null,
        "showDuration"      : "300",
        "hideDuration"      : "1000",
        "timeOut"           : "5000",
        "extendedTimeOut"   : "1000",
        "showEasing"        : "swing",
        "hideEasing"        : "linear",
        "showMethod"        : "fadeIn",
        "hideMethod"        : "fadeOut"
      };
      const myTimeout = setTimeout(clearElements, 3000);
      console.log('here');
    });
    function clearElements(){
      $('#otp').val('');
      $('#password').val('');
    }
  </script>
  @include('inc.script')

</body>

</html>