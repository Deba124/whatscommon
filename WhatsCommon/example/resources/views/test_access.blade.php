<!DOCTYPE html>
<html lang="en" style="background: transparent;">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>What's Common</title>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <link href="{{ url('css/toastr.min.css') }}" rel="stylesheet">
  @include('inc.css')
<style type="text/css">
.feedbackTitle {
    font-weight: 600;
    font-size: 20px;
    color: #3b71b9;
    text-align: center;
    margin-bottom: 20px;
}
.aboutText {
    font-weight: 400;
    color: #b8b8b8;
    font-size: 14px;
    line-height: 18px;
    margin-top: 13px;
}
.leftrightHeading {
    font-size: 20px;
    color: #3b71b9;
    text-align: center;
    /*font-weight: 600;*/
    margin-bottom: 15px;
}
.leftrightHeading span{
    color: #000;
    font-size: 30px;
    font-weight: 600;
}
</style>
</head>
<body style="background: transparent;">
  <section id="login" class="loginBg" style="min-height: 100vh;display: flex;align-items: center;background: transparent;">
    <div class="container">
      <div class="AllLogin">
        <div class="loginForm">
          <form method="POST" class="form-horizontal xhr_form" id="create-dialog" action="messages/create-dialog">
          @csrf
          <div class="form-group whatForm">
            <label>Package Name<span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="packageName">
            <p class="text-danger" id="packageName_error"></p>
          </div>
          <div class="form-group whatForm">
            <label>Subscription Id<span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="subscriptionId">
            <p class="text-danger" id="subscriptionId_error"></p>
          </div>
          <div class="form-group whatForm">
            <label>token<span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="sub_token">
            <p class="text-danger" id="sub_token_error"></p>
          </div>

          <div class="form-group whatForm">
            <label>Access Token<span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="access_token">
            <p class="text-danger" id="access_token_error"></p>
          </div>
          
          <div class="form-group whatForm text-center">
            <button type="submit" class="btn btn-success" style="width: 125px;"> Submit</button>
            
          </div>
        </form>
        </div>
      </div>
    </div>
  </section>
  <script src="{{ url('assets/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ url('assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
  <script src="{{ url('assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ url('assets/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{ url('assets/vendor/aos/aos.js') }}"></script>
  <script src="{{ url('assets/js/main.js') }}"></script>
  <script src="{{ url('js/toastr.min.js') }}"></script>
@include('inc.script')

  <!-- Live credetial -->
  <!-- AYHDiS8TDnzx5Tpr16oDWmKIn2XGhLFqFZJ-3GWBVja6eD7jXqLKH6Dpq2TaSiLpdzPvbVSK8mxKv-JU -->

<script type="text/javascript">
$(document).ready(function(){
  toastr.options = {
    "closeButton"       : false,
    "debug"             : false,
    "newestOnTop"       : true,
    "progressBar"       : true,
    "positionClass"     : "toast-bottom-right",
    "preventDuplicates" : false,
    "onclick"           : null,
    "showDuration"      : "300",
    "hideDuration"      : "1000",
    "timeOut"           : "5000",
    "extendedTimeOut"   : "1000",
    "showEasing"        : "swing",
    "hideEasing"        : "linear",
    "showMethod"        : "fadeIn",
    "hideMethod"        : "fadeOut"
  };
});
</script>

</body>
</html>