@include('inc.header')
<div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                <div class="leftSlidePan {{ Request::get('menu')==1 ? '' : 'closePan'}}">
                  <a href="#" class="panelslideOpenButton {{ Request::get('menu')==1 ? 'panelslideCloseButton' : ''}}"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                  <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                  	<div class="settingLeftTitle">Settings</div>
                  	<ul class="settingMenu mb-0 list-unstyled">
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="{{ url('settings') }}"><img src="new-design/img/profileIcon.png" class="img-fluid menuIcon" alt="">Profile</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="{{ url('change-password') }}"><img src="new-design/img/change-password.png" class="img-fluid menuIcon" alt="">Change Password</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="{{ url('security') }}"><img src="new-design/img/security.png" class="img-fluid menuIcon" alt="">Security and Privacy</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="{{ url('blocked-users') }}"><img src="new-design/img/blocking.png" class="img-fluid menuIcon" alt="">Blocking</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu settingMenuActive" href="{{ url('notification-settings') }}"><img src="new-design/img/notifications-a.png" class="img-fluid menuIcon" alt="">Notifications</a>
                  		</li>
                  		
                      <li class="settingMenuLi">
                  			<a class="settingMenu" href="{{ url('link-accounts') }}"><img src="new-design/img/link-account.png" class="img-fluid menuIcon" alt="">Link Account</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="{{ url('message-settings') }}"><img src="new-design/img/message-setting.png" class="img-fluid menuIcon" alt="">Message Settings</a>
                  		</li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="{{ url('feed-settings') }}"><img src="new-design/img/global-feed.png" class="img-fluid menuIcon" alt="">Feed Settings</a>
                      </li>
                  	</ul>
                  </div>
                </div>
                <div class="midPan">
                  <div class="connectionsBody windowHeight windowHeightMid linkAcc linkAccNoti">
                    <div class="bankDetailsTitle">Notifications</div>
                    <div class="feedbackTitle2">By turning on the toggle buttons, you’re allowing What'sCommon to send you notifications with a specific category you choose to turn on.
                    </div>
                    <div class="bankcardTab">
                      
                        

                      <div class="">
                        <div id="" class="">
                        <form method="post" class="xhr_form" action="set-notification-setting" id="set-notification-setting">
                            @csrf
                          <div class="profileForm bankForm">
                            <ul class="linkAccountList">
                                <!-- <li>
                                    <span style="width: calc(100% - 48px);">
                                        <img src="new-design/img/notiMsg.png" class="img-fluid" alt="">
                                        Messages
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" @if($notifications && $notifications->ns_message==1) checked @endif name="message_setting" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li> -->
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        <img src="new-design/img/notiShares.png" class="img-fluid" alt="">
                                        Shares
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" @if($notifications && $notifications->ns_shares==1) checked @endif name="share_setting" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        <img src="new-design/img/notiConnections.png" class="img-fluid" alt="">
                                        New Connections
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" @if($notifications && $notifications->ns_new_connection==1) checked @endif name="new_connection_setting" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        <img src="new-design/img/notiMatch.png" class="img-fluid" alt="">
                                        New Match
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" @if($notifications && $notifications->ns_new_match==1) checked @endif name="new_match_setting" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                            </ul>
                            
                          </div>
                      </form>
                        </div>
                        
                      </div>

                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@include('inc.footer')
<script type="text/javascript">
$(document).on('change','.settings', function(){
	$('.xhr_form').submit();
});
</script>