@include('inc.header')
<style type="text/css">
.lifeDraftNotifCount{
	font-weight: 600;
    font-size: 16px;
    color: #fff;
    background-color: #91d639;
    height: 26px;
    width: 40px;
    border-radius: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-left: auto;
    top: 10px !important;
    right: 10px !important;
}

</style>
<div class="innerBodyOnly innerBodyModify">
    <div class="container-fluid">
        <div class="settingBody settingBodyFullScroll">
            <div class="position-relative settingFlip">
                <div class="leftSlidePan closePan">
                    <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                    <div class="lifeDraftLeftTitle">LIFE EVENTS</div>
                    <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                      <ul class="list-unstyled mb-0 lifeEventMenu">
                        <li class="lifeEventMenuLi lifeDraftLi {{ Request::is('life-events') ? 'lifeEventMenuLiActive' : '' }}">
                          <a href="{{ url('life-events') }}" class="lifeEventMenuLink">
                            <span class="lifeEventMenuText">Created Life Events</span>
                            <span class="lifeDraftCount">{{ $life_event_count }}</span>
                          </a>
                        </li>
                        <li class="lifeEventMenuLi lifeDraftLi {{ Request::is('drafts') ? 'lifeEventMenuLiActive' : '' }}">
                          <a href="{{ url('drafts') }}" class="lifeEventMenuLink lifeDraftMenuLink">
                            <span class="lifeEventMenuText">Drafts</span>
                            <span class="lifeDraftCount">{{ $draft_count }}</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                </div>
@php
$date = '';
/*if(count($drafts) > 0){
	$date = $drafts[0]->created_at;
	$date = DateTime_Diff2($date);
}*/
@endphp

                <div class="midPan lifeDraftLiMidPan windowHeight windowHeightMid">
@if(count($drafts) > 0)
	@foreach($drafts as $draft)
		@php 
			if($date!=DateTime_Diff2($draft->event_create_time)){
				$date = DateTime_Diff2($draft->event_create_time);
				echo '<div class="lifeDraftLeftTitle lifeDraftRightTitle">'.$date.'</div>';
			}
		@endphp
                    
                    <div class="connectionsBody infoBodyRight">
                      <div class="row infoBodyRow">
                        <div class="col-md-12 infoBodyCol eventCol midHightControl bg-white">
                          <div class="feedbackRight position-relative">
                                {{-- <div class="draftOptCount">14 options left</div> --}}
                                @if($draft->match_count>0)
                                <div class=" lifeDraftNotifCount " onclick="show_hide_notidiv('{{ $draft->_event_id }}');">{{ $draft->match_count }}</div>
                                @endif
                                <div class="eventColHeading">What</div>
                                <div class="whatForm position-relative">
                                  <ul class="mb-0 list-unstyled lifeDraftResult">
                                    <li>Life Event: <span class="wcGreen">{{ $draft->event_type }}{{ $draft->sub_cat_type_name ? ' ('.$draft->sub_cat_type_name.')' : '' }}</span></li>

                                @if($draft->event_pet_name)
                                    <li>Name: <span>{{ $draft->event_pet_name }}</span></li>
                                @endif
                                @if($draft->event_pet_species)
                                    <li>Species: <span>{{ $draft->event_pet_species }}</span></li>
                                @endif
                                @if($draft->event_pet_breed)
                                    <li>Breed: <span>{{ $draft->event_pet_breed }}</span></li>
                                @endif

                                @if($draft->event_military_branch)
                                    <li>Branch: <span>{{ $draft->event_military_branch }}</span></li>
                                @endif

                                @if($draft->event_bio_mother)
                                    <li>Mother: <span>{{ $draft->event_bio_mother }}</span></li>
                                @endif
                                @if($draft->event_bio_father)
                                    <li>Father: <span>{{ $draft->event_bio_father }}</span></li>
                                @endif
                                @if($draft->event_birthname)
                                    <li>Name given at birth: <span>{{ $draft->event_birthname }}</span></li>
                                @endif
                                @if($draft->event_home_type)
                                    <li>Type of Home: <span>{{ $draft->event_home_type }}</span></li>
                                @endif
                                @if($draft->event_home_style)
                                    <li>Style: <span>{{ $draft->event_home_style }}</span></li>
                                @endif
                                @if($draft->event_tag_number)
                                    <li>Tag Number: <span>{{ $draft->event_tag_number }}</span></li>
                                @endif
                                @if($draft->event_vehicle_type)
                                    <li>Type of Vehicle: <span>{{ $draft->event_vehicle_type }}</span></li>
                                @endif
                                @if($draft->event_vehicle_maker)
                                    <li>Vehicle Maker: <span>{{ $draft->event_vehicle_maker }}</span></li>
                                @endif
                                @if($draft->event_vehicle_model)
                                    <li>Model: <span>{{ $draft->event_vehicle_model }}</span></li>
                                @endif
                                @if($draft->event_vehicle_year)
                                    <li>Year: <span>{{ $draft->event_vehicle_year }}</span></li>
                                @endif
                                @if($draft->event_vehicle_color)
                                    <li>Vehicle Color: <span>{{ $draft->event_vehicle_color }}</span></li>
                                @endif
                                @if($draft->event_family_firstname)
                                    <li>Firstname: <span>{{ $draft->event_family_firstname }}</span></li>
                                @endif
                                @if($draft->event_family_lastname)
                                    <li>Lastname: <span>{{ $draft->event_family_lastname }}</span></li>
                                @endif
                                @if($draft->event_age)
                                    <li>Age: <span>{{ $draft->event_age }}</span></li>
                                @endif
                                @if($draft->event_gender)
                                    <li>Gender: <span>{{ $draft->event_gender }}</span></li>
                                @endif
                                @if($draft->event_family_relation)
                                    <li>Relation: <span>{{ $draft->event_family_relation }}</span></li>
                                @endif
                                @if($draft->event_family_marital_status)
                                    <li>Marital Status: <span>{{ $draft->event_family_marital_status }}</span></li>
                                @endif
                                @if($draft->event_family_industry)
                                    <li>Industry: <span>{{ $draft->event_family_industry }}</span></li>
                                @endif
                                @if($draft->event_family_category)
                                    <li>Category: <span>{{ $draft->event_family_category }}</span></li>
                                @endif
                                @if($draft->event_ethnicity)
                                    <li>Ethnicity: <span>{{ $draft->event_ethnicity }}</span></li>
                                @endif
                                @if($draft->event_race)
                                    <li>Race: <span>{{ $draft->event_race }}</span></li>
                                @endif
                                @if($draft->event_nationality)
                                    <li>Nationality: <span>{{ $draft->event_nationality }}</span></li>
                                @endif
                                @if($draft->event_height)
                                    <li>Height: <span>{{ $draft->event_height }} {{ $draft->event_height_measure ? $draft->event_height_measure : '' }}</span></li>
                                @endif
                                @if($draft->event_weight)
                                    <li>Weight: <span>{{ $draft->event_weight }} {{ $draft->event_weight_measure ? $draft->event_weight_measure : '' }}</span></li>
                                @endif
                                @if($draft->event_height_general)
                                    <li>Height General: <span>{{ $draft->event_height_general }}</span></li>
                                @endif
                                @if($draft->event_weight_general)
                                    <li>Weight General: <span>{{ $draft->event_weight_general }}</span></li>
                                @endif
                                @if($draft->event_facial_hair)
                                    <li>Facial Hair: <span>{{ $draft->event_facial_hair }}</span></li>
                                @endif
                                @if($draft->event_orientation)
                                    <li>Orientation: <span>{{ $draft->event_orientation }}</span></li>
                                @endif
                                @if($draft->event_body_style)
                                    <li>Body Style: <span>{{ $draft->event_body_style }}</span></li>
                                @endif
                                @if($draft->event_skintone)
                                    <li>Skintone: <span>{{ $draft->event_skintone }}</span></li>
                                @endif
                                @if($draft->event_hair_color)
                                    <li>Hair Color: <span>{{ $draft->event_hair_color }}</span></li>
                                @endif
                                @if($draft->event_eye_color)
                                    <li>Eye Color: <span>{{ $draft->event_eye_color }}</span></li>
                                @endif
                                @if($draft->event_political)
                                    <li>Political Affiliation: <span>{{ $draft->event_political }}</span></li>
                                @endif
                                @if($draft->event_religion)
                                    <li>Religion: <span>{{ $draft->event_religion }}</span></li>
                                @endif
                                @if($draft->event_looking_for)
                                    <li>Looking For: <span>{{ $draft->event_looking_for }}</span></li>
                                @endif
                                @if($draft->event_age_range)
                                    <li>Age Range: <span>{{ $draft->event_age_range }}</span></li>
                                @endif
                                @if($draft->event_interest)
                                    <li>Interest: <span>{{ $draft->event_interest }}</span></li>
                                @endif
                                @if($draft->event_body_hair)
                                    <li>Body Hair: <span>{{ $draft->event_body_hair }}</span></li>
                                @endif
                                @if($draft->event_glasses)
                                    <li>Glasses: <span>{{ $draft->event_glasses }}</span></li>
                                @endif
                                @if($draft->event_party)
                                    <li>Party: <span>{{ $draft->event_party }}</span></li>
                                @endif
                                @if($draft->event_smoke)
                                    <li>Smoke: <span>{{ $draft->event_smoke }}</span></li>
                                @endif
                                @if($draft->event_drink)
                                    <li>Drink: <span>{{ $draft->event_drink }}</span></li>
                                @endif
                                @if($draft->event_tattoos)
                                    <li>Tattoos: <span>{{ $draft->event_tattoos }}</span></li>
                                @endif
                                @if($draft->event_tattoo_type)
                                    <li>Tattoo Type: <span>{{ $draft->event_tattoo_type }}</span></li>
                                @endif
                                @if($draft->event_fav_food)
                                    <li>Favourite Food: <span>{{ $draft->event_fav_food }}</span></li>
                                @endif
                                @if($draft->event_food_type)
                                    <li>Food Type: <span>{{ $draft->event_food_type }}</span></li>
                                @endif
                                @if($draft->event_fav_team)
                                    <li>Favourite Team: <span>{{ $draft->event_fav_team }}</span></li>
                                @endif
                                @if($draft->event_zodiac)
                                    <li>Zodiac Sign: <span>{{ $draft->event_zodiac }}</span></li>
                                @endif
                                @if($draft->event_birth_stone)
                                    <li>Birth Stone: <span>{{ $draft->event_birth_stone }}</span></li>
                                @endif
                                @if($draft->event_affiliations)
                                    <li>Affiliations: <span>{{ $draft->event_affiliations }}</span></li>
                                @endif
                                @if($draft->event_career)
                                    <li>Career: <span>{{ $draft->event_career }}</span></li>
                                @endif
                                @if($draft->event_financial)
                                    <li>Financial: <span>{{ $draft->event_financial }}</span></li>
                                @endif
                                @if($draft->event_drink)
                                    <li>Drink: <span>{{ $draft->event_drink }}</span></li>
                                @endif
                                
                                @if($draft->event_time_of_birth)
                                    <li>Time of Birth: <span>{{ $draft->event_time_of_birth }} {{ $draft->event_time_of_birth_ampm ? $draft->event_time_of_birth_ampm : '' }}</span></li>
                                @endif
                                @if($draft->event_airport)
                                    <li>Travel Mode: <span>{{ $draft->event_airport }}</span></li>
                                @endif
                                @if($draft->event_airline)
                                    <li>Name: <span>{{ $draft->event_airline }}</span></li>
                                @endif
                                @if($draft->event_flight_number)
                                    <li>Number: <span>{{ $draft->event_flight_number }}</span></li>
                                @endif
                                @if($draft->event_flight_row)
                                    <li>Row: <span>{{ $draft->event_flight_row }}</span></li>
                                @endif
                                @if($draft->event_flight_seat)
                                    <li>Seat: <span>{{ $draft->event_flight_seat }}</span></li>
                                @endif
                                @if($draft->event_military_unit1)
                                    <li>Unit 1: <span>{{ $draft->event_military_unit1 }}</span></li>
                                @endif
                                @if($draft->event_military_unit2)
                                    <li>Unit 2: <span>{{ $draft->event_military_unit2 }}</span></li>
                                @endif
                                @if($draft->event_military_unit3)
                                    <li>Unit 3: <span>{{ $draft->event_military_unit3 }}</span></li>
                                @endif
                                @if($draft->event_military_unit4)
                                    <li>Unit 4: <span>{{ $draft->event_military_unit4 }}</span></li>
                                @endif
                                @if($draft->event_military_unit5)
                                    <li>Unit 5: <span>{{ $draft->event_military_unit5 }}</span></li>
                                @endif
                                @if($draft->event_military_unit6)
                                    <li>Unit 6: <span>{{ $draft->event_military_unit6 }}</span></li>
                                @endif
                                @if($draft->event_military_rank)
                                    <li>Rank: <span>{{ $draft->event_military_rank }}</span></li>
                                @endif
                                @if($draft->event_military_mos)
                                    <li>MOS: <span>{{ $draft->event_military_mos }}</span></li>
                                @endif
                                @if($draft->event_military_title)
                                    <li>Title: <span>{{ $draft->event_military_title }}</span></li>
                                @endif
                                @if($draft->event_school_name)
                                    <li>School Info: <span>{{ $draft->event_school_name }}</span></li>
                                @endif
                                @if($draft->event_school_sorority)
                                    <li>Sorority/Fraternity: <span>{{ $draft->event_school_sorority }}</span></li>
                                @endif
                                @if($draft->event_school_teacher)
                                    <li>Teacher/Professor: <span>{{ $draft->event_school_teacher }}</span></li>
                                @endif
                                @if($draft->event_school_roomno)
                                    <li>Room No.: <span>{{ $draft->event_school_roomno }}</span></li>
                                @endif
                                @if($draft->event_school_subject)
                                    <li>Subject: <span>{{ $draft->event_school_subject }}</span></li>
                                @endif
                                @if($draft->event_school_curriculars)
                                    <li>Curriculum: <span>{{ $draft->event_school_curriculars }}</span></li>
                                @endif
                                @if($draft->event_school_organization)
                                    <li>Organization: <span>{{ $draft->event_school_organization }}</span></li>
                                @endif
                                @if($draft->event_school_event)
                                    <li>Event: <span>{{ $draft->event_school_event }}</span></li>
                                @endif
                                @if($draft->event_school_afterschool)
                                    <li>After School: <span>{{ $draft->event_school_afterschool }}</span></li>
                                @endif
                                @if($draft->event_career_name)
                                    <li>Business: <span>{{ $draft->event_career_name }}</span></li>
                                @endif
                                @if($draft->event_career_category)
                                    <li>Category: <span>{{ $draft->event_career_category }}</span></li>
                                @endif
                                @if($draft->event_career_title)
                                    <li>Title: <span>{{ $draft->event_career_title }}</span></li>
                                @endif
                                @if($draft->event_career_level)
                                    <li>Level: <span>{{ $draft->event_career_level }}</span></li>
                                @endif
                                @if($draft->event_pet_temperment)
                                    <li>Temperment: <span>{{ $draft->event_pet_temperment }}</span></li>
                                @endif
                                @if($draft->event_pet_color)
                                    <li>Color: <span>{{ $draft->event_pet_color }}</span></li>
                                @endif
                                @if($draft->event_pet_size)
                                    <li>Size: <span>{{ $draft->event_pet_size }}</span></li>
                                @endif
                                @if($draft->event_pet_license_number)
                                    <li>License Number: <span>{{ $draft->event_pet_license_number }}</span></li>
                                @endif
                                @if($draft->event_pet_collar)
                                    <li>Collar: <span>{{ $draft->event_pet_collar }}</span></li>
                                @endif
                                @if($draft->event_pet_veterinary)
                                    <li>Veterinarian: <span>{{ $draft->event_pet_veterinary }}</span></li>
                                @endif
                                @if($draft->event_pet_medical)
                                    <li>Medical Condition: <span>{{ $draft->event_pet_medical }}</span></li>
                                @endif
                                @if($draft->event_item_name)
                                    <li>Item: <span>{{ $draft->event_item_name }}</span></li>
                                @endif
                                @if($draft->event_item_size)
                                    <li>Size: <span>{{ $draft->event_item_size }}</span></li>
                                @endif
                                @if($draft->event_item_material)
                                    <li>Material: <span>{{ $draft->event_item_material }}</span></li>
                                @endif
                                @if($draft->event_outdoor)
                                    <li>Outdoor: <span>{{ $draft->event_outdoor }}</span></li>
                                @endif
                                @if($draft->event_indoor)
                                    <li>Indoor: <span>{{ $draft->event_indoor }}</span></li>
                                @endif
                                @if($draft->event_inmotion)
                                    <li>In Motion: <span>{{ $draft->event_inmotion }}</span></li>
                                @endif
                                @if($draft->event_other)
                                    <li>Quantity: <span>{{ $draft->event_other }}</span></li>
                                @endif
                                @if($draft->event_game)
                                    <li>Game or Activity: <span>{{ $draft->event_game }}</span></li>
                                @endif
                                @if($draft->event_gameteam)
                                    <li>Team/Group: <span>{{ $draft->event_gameteam }}</span></li>
                                @endif
                                @if($draft->event_username)
                                    <li>Username: <span>{{ $draft->event_username }}</span></li>
                                @endif
                                    {{-- <li>Type of Home: <span>Apartment</span></li>
                                    <li>Style: <span>Bungalow</span></li> --}}
                                  </ul>
                                </div>
                                <div class="eventColHeading">Where</div>
                                <div class="whatForm">
                                  <ul class="mb-0 list-unstyled lifeDraftResult">
                            @if($draft->event_type_id==1)
                            	@if($draft->where_country)
                            		<li>Country: <span class="wcGreen">{{ $draft->where_country }}</span></li>
                            	@endif
                            	@if($draft->where_state)
                                    <li>State/Province: <span>{{ $draft->where_state }}</span></li>
                                @endif
                                @if($draft->where_city)
                                    <li>City: <span>{{ $draft->where_city }}</span></li>
                                @endif
                                @if($draft->where_street)
                                    <li>Street Address: <span>{{ $draft->where_street }}</span></li>
                                @endif
                                @if($draft->where_zip)
                                    <li>Zip: <span>{{ $draft->where_zip }}</span></li>
                                @endif
                                @if($draft->where_email)
                                    <li>Email: <span>{{ $draft->where_email }}</span></li>
                                @endif
                                @if($draft->where_phone)
                                    <li>Phone: <span>{{ ($draft->where_isd) ? $draft->where_isd : '' }} {{ $draft->where_phone }}</span></li>
                                @endif
                            @elseif($draft->event_type_id==2)
                            	@if($draft->where_country)
                            		<li>Country: <span class="wcGreen">{{ $draft->where_country }}</span></li>
                            	@endif
                            	@if($draft->where_state)
                                    <li>State/Province: <span>{{ $draft->where_state }}</span></li>
                                @endif
                                @if($draft->where_city)
                                    <li>City: <span>{{ $draft->where_city }}</span></li>
                                @endif
                                @if($draft->where_radius)
                                    <li>Radius: <span>{{ $draft->where_radius }}</span></li>
                                @endif
                            @elseif($draft->event_type_id==3)

                            	@if($draft->where_birth_place)
                            		<li>Birth Place: <span class="wcGreen">{{ $draft->where_birth_place }}</span></li>
                            	@endif
                            	@if($draft->where_birth_country)
                                    <li>Country of Birth: <span>{{ $draft->where_birth_country }}</span></li>
                                @endif
                                @if($draft->where_birth_state)
                                    <li>State: <span>{{ $draft->where_birth_state }}</span></li>
                                @endif
                                @if($draft->where_birth_city)
                                    <li>City: <span>{{ $draft->where_birth_city }}</span></li>
                                @endif
                                @if($draft->where_birth_street)
                                    <li>Street Address: <span>{{ $draft->where_birth_street }}</span></li>
                                @endif
                                @if($draft->where_zip)
                                    <li>Zip: <span>{{ $draft->where_zip }}</span></li>
                                @endif
                                

                            	@if($draft->where_adoption_status)
                                    <li>Adoption Status: <span>{{ $draft->where_adoption_status }}</span></li>
                                @endif
                            	@if($draft->where_country)
                            		<li>Country: <span class="wcGreen">{{ $draft->where_country }}</span></li>
                            	@endif
                            	@if($draft->where_state)
                                    <li>State/Province: <span>{{ $draft->where_state }}</span></li>
                                @endif
                                @if($draft->where_city)
                                    <li>City: <span>{{ $draft->where_city }}</span></li>
                                @endif
                                @if($draft->where_street)
                                    <li>Street Address: <span>{{ $draft->where_street }}</span></li>
                                @endif
                                @if($draft->where_zip)
                                    <li>Zip: <span>{{ $draft->where_zip }}</span></li>
                                @endif

                            @elseif($draft->event_type_id==4)

                            	@if($draft->where_location_type)
                                    <li>Location Type: <span>{{ $draft->where_location_type }}</span></li>
                                @endif
                                @if($draft->where_hotel_name)
                                    <li>Location Name: <span>{{ $draft->where_hotel_name }}</span></li>
                                @endif
                            	@if($draft->where_country)
                            		<li>Country: <span class="wcGreen">{{ $draft->where_country }}</span></li>
                            	@endif
                            	@if($draft->where_state)
                                    <li>State/Province: <span>{{ $draft->where_state }}</span></li>
                                @endif
                                @if($draft->where_city)
                                    <li>City: <span>{{ $draft->where_city }}</span></li>
                                @endif
                                @if($draft->where_street)
                                    <li>Street Address: <span>{{ $draft->where_street }}</span></li>
                                @endif
                                @if($draft->where_zip)
                                    <li>Zip: <span>{{ $draft->where_zip }}</span></li>
                                @endif
                            
                            @elseif($draft->event_type_id==5)

                            	@if($draft->where_country)
                            		<li>Country: <span class="wcGreen">{{ $draft->where_country }}</span></li>
                            	@endif
                            	@if($draft->where_state)
                                    <li>State/Province: <span>{{ $draft->where_state }}</span></li>
                                @endif
                                @if($draft->where_city)
                                    <li>City: <span>{{ $draft->where_city }}</span></li>
                                @endif
                                @if($draft->where_street)
                                    <li>Street Address: <span>{{ $draft->where_street }}</span></li>
                                @endif
                                @if($draft->where_zip)
                                    <li>Zip: <span>{{ $draft->where_zip }}</span></li>
                                @endif
                                @if($draft->where_base)
                                    <li>Base: <span>{{ $draft->where_base }}</span></li>
                                @endif
                                @if($draft->where_orders)
                                    <li>Orders: <span>{{ $draft->where_orders }}</span></li>
                                @endif

                            @elseif(in_array($draft->event_type_id,[6,7,8,9]))

                            	@if($draft->where_country)
                            		<li>Country: <span class="wcGreen">{{ $draft->where_country }}</span></li>
                            	@endif
                            	@if($draft->where_state)
                                    <li>State/Province: <span>{{ $draft->where_state }}</span></li>
                                @endif
                                @if($draft->where_city)
                                    <li>City: <span>{{ $draft->where_city }}</span></li>
                                @endif
                                @if($draft->where_street)
                                    <li>Street Address: <span>{{ $draft->where_street }}</span></li>
                                @endif
                                @if($draft->where_zip)
                                    <li>Zip: <span>{{ $draft->where_zip }}</span></li>
                                @endif

                            @endif
                                    {{-- <li>Country: <span class="wcGreen">Philippines</span></li>
                                    <li>State/Province: <span>Pampanga</span></li>
                                    <li>City: <span>Maxico</span></li>
                                    <li>Street Address: <span>Unit 4, Camachili Street, Sto. Domingo Village, Lagundi</span></li>
                                    <li>Zip: <span>2020</span></li>
                                    <li>Email: <span></span></li>
                                    <li>Phone: <span></span></li> --}}
                                  </ul>
                                </div>

                                <div class="eventColHeading">When</div>
                                <div class="whatForm position-relative"> 
                            @if($draft->match_count>0)
                                
                                 
                                
				                
				                      <!-- Modal starts -->       
<div class="modal fade matched_users" id="show-matched-users-modal-{{ $draft->_event_id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered " role="document">
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: none;">
      	<h5 class="modal-title" id="exampleModalLabel-3" style="margin-left: 35%;">Accounts</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="min-height: 350px;overflow-y: auto;">
        <div class="notificationsBox windowHeight notificationHeight">
        	@if($draft->users)
            @php
            /*echo '<pre>';
            print_r($draft->users);*///exit;    

            @endphp
				@foreach($draft->users as $user)
        	<div class="notificationCard" style="position: relative;">
                @if($user->quick_blox_id)
                                        <div class="notificationsBtn liftDraftNotificationsBtn">
                                          <a href="{{ url('messages/?qb='.$user->quick_blox_id) }}" class="blockBtn replayBtn" style="position: absolute;top:20px;"><img src="new-design/img/msgIcon2.png" class="img-fluid" alt=""> Message</a>
                                        </div>
                                        @endif
				                        <div class="notificationDetails" style="margin-top: -8px;">
				                          <div class="notificationCardAvtar">
				                            <img src="{{ $user->profile_photo_path ? $user->profile_photo_path : url('new-design/img/profile_placeholder.jpg') }}" class="img-fluid" alt="">
				                          </div>
				                          <div class="notificationAvtarInfo">
				                            <div class="notificationAvtarName">{{ $user->firstName }} {{ $user->lastName }}</div>
				                            <div class="notificationAvtarMsg">
				                              <span> {{'@'}}{{ $user->username }}</span>
				                            </div>
				                          </div>
				                        </div>
				                        
				                      </div>
			@endforeach
				                @endif
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
				                    
				                    
                            @endif
                                  <ul class="mb-0 list-unstyled lifeDraftResult">
                            @if($draft->event_type_id==1)
                            	@if(in_array($draft->event_subcategory_id,[1,2]))
                            		@if($draft->when_from_day && $draft->when_from_month && $draft->when_from_year)
                            		<li>From: <span class="wcGray">{{ $draft->when_from_month }}/{{ $draft->when_from_day }}/{{ $draft->when_from_year }}</span></li>
                            		@endif
                            		@if($draft->when_to_day && $draft->when_to_month && $draft->when_to_year)
                            		<li>To: <span class="wcGray">{{ $draft->when_to_month }}/{{ $draft->when_to_day }}/{{ $draft->when_to_year }}</span></li>
                            		@endif
                            	@else

                            		@if($draft->when_family_birth_day && $draft->when_family_birth_month && $draft->when_family_birth_year)
                            		<li>Birthday: <span class="wcGray">{{ $draft->when_family_birth_month }}/{{ $draft->when_family_birth_day }}/{{ $draft->when_family_birth_year }}</span></li>
                            		@endif
                            		@if($draft->when_family_wedding_day && $draft->when_family_wedding_month && $draft->when_family_wedding_year)
                            		<li>Wedding Day: <span class="wcGray">{{ $draft->when_family_wedding_month }}/{{ $draft->when_family_wedding_day }}/{{ $draft->when_family_wedding_year }}</span></li>
                            		@endif
                            		@if($draft->when_family_passed_day && $draft->when_family_passed_month && $draft->when_family_passed_year)
                            		<li>Death Day: <span class="wcGray">{{ $draft->when_family_passed_month }}/{{ $draft->when_family_passed_day }}/{{ $draft->when_family_passed_year }}</span></li>
                            		@endif
                            		@if($draft->when_from_day && $draft->when_from_month && $draft->when_from_year)
                            		<li>From: <span class="wcGray">{{ $draft->when_from_month }}/{{ $draft->when_from_day }}/{{ $draft->when_from_year }}</span></li>
                            		@endif
                            		@if($draft->when_to_day && $draft->when_to_month && $draft->when_to_year)
                            		<li>To: <span class="wcGray">{{ $draft->when_to_month }}/{{ $draft->when_to_day }}/{{ $draft->when_to_year }}</span></li>
                            		@endif
                            	@endif

                            @elseif($draft->event_type_id==2)
                            		@if($draft->when_timeframe)
                            		<li>Timeframe: <span class="wcGray">{{ $draft->when_timeframe }}</span></li>
                            		@endif

                            		@if($draft->when_month )
                            		<li>To: <span class="wcGray">{{ $draft->when_month }}</span></li>
                            		@endif
                           	@elseif($draft->event_type_id==3)
                           			@if($draft->when_from_day && $draft->when_from_month && $draft->when_from_year)
                            		<li>From: <span class="wcGray">{{ $draft->when_from_month }}/{{ $draft->when_from_day }}/{{ $draft->when_from_year }}</span></li>
                            		@endif

                            @elseif($draft->event_type_id==4)
                           			@if($draft->when_from_day && $draft->when_from_month && $draft->when_from_year)
                            		<li>From: <span class="wcGray">{{ $draft->when_from_month }}/{{ $draft->when_from_day }}/{{ $draft->when_from_year }}</span></li>
                            		@endif
                            		@if($draft->when_to_day && $draft->when_to_month && $draft->when_to_year)
                            		<li>To: <span class="wcGray">{{ $draft->when_to_month }}/{{ $draft->when_to_day }}/{{ $draft->when_to_year }}</span></li>
                            		@endif

                            		@if($draft->when_time_ampm && $draft->when_time)
                            		<li>Departure Time: <span>{{ $draft->when_time }} {{ $draft->when_time_ampm }}</span></li>
                            		@endif

                            		@if($draft->when_arrival_time_ampm && $draft->when_arrival_time)
                            		<li>Arrival Time: <span>{{ $draft->when_arrival_time }} {{ $draft->when_arrival_time_ampm }}</span></li>
                            		@endif

                            @elseif(in_array($draft->event_type_id,[5,6,7,8,9,11]))

                            		@if($draft->when_from_day && $draft->when_from_month && $draft->when_from_year)
                            		<li>From: <span class="wcGray">{{ $draft->when_from_month }}/{{ $draft->when_from_day }}/{{ $draft->when_from_year }}</span></li>
                            		@endif
                            		@if($draft->when_to_day && $draft->when_to_month && $draft->when_to_year)
                            		<li>To: <span class="wcGray">{{ $draft->when_to_month }}/{{ $draft->when_to_day }}/{{ $draft->when_to_year }}</span></li>
                            		@endif

                            @endif
                                    {{-- <li>From: <span class="wcGray">Month/DD/YYYY</span></li>
                                    <li>To: <span class="wcGray">Month/DD/YYYY</span></li>
                                    <li>Time: <span>10:50 AM</span></li> --}}
                                  </ul>
                            
                                
                                </div>
                            @if($draft->event_keywords)
                                <div class="eventColHeading">Who, What, Where, When, Why</div>
                                <div class="whatForm">
                                  <ul class="mb-0 list-unstyled lifeDraftResult">
                                    <li><span>{{ $draft->event_keywords }}</span></li>
                                  </ul>
                                </div>
                            @endif
                            @if($draft->images)
                                <div class="eventColHeading">Photo Gallery</div>
                                <div class="whatForm whereFrom">
                                  <div class="photoGalleryScroll">
                                    <div class="photoGallery mt-0">
                                @foreach($draft->images as $image)
                                      <div class="photoGalleryOnly mb-0">
                                        <img src="{{ $image->image_url }}" class="img-fluid photoGalleryImg" alt="">
                                      </div>
                                @endforeach
                                      {{-- <div class="photoGalleryOnly mb-0">
                                        <img src="https://cdn.pixabay.com/photo/2017/09/25/13/12/dog-2785074__340.jpg" class="img-fluid photoGalleryImg" alt="">
                                      </div> --}}
                                    </div>
                                  </div>
                                </div>
                            @endif
                                <div class="editDeleteBtn">
                                  <a href="{{ $draft->event_is_draft==0 ? url('edit-life-event/?event='.$draft->_event_id) : url('edit-drafts/?event='.$draft->_event_id) }}" class="btn editBtn"><img src="new-design/img/editBtnIcon.png" class="img-fluid mr-2" alt=""> Edit</a>
                                  <a class="btn editBtn deleteBtn" onclick="execute('api/delete-life-event','user_id={{$draft->event_user_id}}&_event_id={{ $draft->_event_id }}');"><img src="new-design/img/deleteBtnIcon.png" class="img-fluid mr-2" alt=""> Delete</a>
                                </div>
                          </div>
                        </div>
                      </div>
                    </div>
    @endforeach
@endif
                </div>
            </div>
        </div>
    </div>
</div>
@include('inc.footer')
<script type="text/javascript">
function show_hide_notidiv(counter){
	//$(".NotificationsAll").toggleClass("eventsNotificationOff");
	/*if($('#show-matched-users-modal-'+counter).hasClass('eventsNotificationOff')){
		$('#show-matched-users-modal-'+counter).removeClass('eventsNotificationOff');
	}else{
		$('#show-matched-users-modal-'+counter).addClass('eventsNotificationOff');
	}*/
	$('.matched_users').modal('hide');
	$('#show-matched-users-modal-'+counter).modal('show');
}
</script>