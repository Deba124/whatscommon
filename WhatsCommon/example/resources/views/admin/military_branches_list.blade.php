@section('title', 'Military Branches')
@include('admin.inc.header')
@include('admin.inc.sidebar')
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Military Branches</h4>
      <div class="row" style="margin-bottom: 15px;">
        <div class="col-9"></div>
        <meta name="_token" content="{{ csrf_token() }}">
        <div class="col-3 text-right">
          <a class="btn btn-secondary" href="javascript:void();" onclick="open_modal('add-military-branch');"><i class="fa fa-plus-circle text-success"></i> Add Military Branch</a>
        </div>
      </div>
      <div class="row">
  
        <div class="col-12">
          <table id="military_branches-list" class="table table-condensed table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <!-- <th>Status</th> -->
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
@if($military_branches)
  @php
    $i = 1
  @endphp
  @foreach ($military_branches as $mbranch)
            <tr>
              <td>{{ $i++ }}</td>
              <td>{{ $mbranch->mbranch_name }}</td>
              
              <td>
                <a class="custom_btn _edit" href="{{url('admin/military-ranks/?branch_id='.$mbranch->_mbranch_id)}}">
                  <i class="fa fa-list"></i>
                </a>
                <a class="custom_btn _edit" onclick="open_modal('edit-military-branch','_mbranch_id={{$mbranch->_mbranch_id}}');">
                  <i class="fa fa-edit"></i>
                </a>
                
              </td>
            </tr>
  @endforeach
  @else
    <tr><td colspan="5" class="text-center"><b>No military branch Available</b></td></tr>
@endif

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@include('admin.inc.footer')
<!-- content-wrapper ends -->
<script type="text/javascript">
$(document).ready(function() {  

  dataTable = $('#military_branches-list').DataTable( {
    dom: 'lBfrtip',
    responsive: true,
    ordering : true,
    lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
    pageLength: 10,
    buttons: [
      {
          extend: 'print',
          text: '<button class="btn btn-primary btn-sm" ><i class="fa fa-print fa-x5"> Print</i></button>',
          autoPrint: false,
          exportOptions: {
              columns: [ 0,1 ]
          }
      },
      {
          extend: 'excelHtml5',
          text: '<button class="btn btn-success btn-sm"><i class="fa fa-file-excel-o fa-x5"> Excel</i></button>',
          exportOptions: {
              columns: [ 0,1 ]
          }
      },
      {
          extend: 'pdfHtml5',
          text: '<button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o fa-x5"> PDF</i></button>',
          exportOptions: {
              columns:[ 0,1 ]
          }
      }
    ],
    "processing": true,
    "serverSide": false,
  });
}); 
</script>