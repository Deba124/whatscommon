@section('title', 'Donations')
@include('admin.inc.header')
@include('admin.inc.sidebar')
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Donations</h4>
      <div class="row" style="margin-bottom: 15px;">
        <div class="col-9"></div>
        <meta name="_token" content="{{ csrf_token() }}">
        <div class="col-3 text-right">
          
        </div>
      </div>
      <div class="row">
  
        <div class="col-12">
          <table id="donations-list" class="table table-condensed table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>User </th>
                <th>Amount</th>
                <th>Payment Status</th>
                <th>Bank</th>
                <th>Date</th>
                {{-- <th>Actions</th> --}}
              </tr>
            </thead>
            <tbody>
@if($donations)
  @php
    $i = 1
  @endphp
  @foreach ($donations as $donation)
            <tr>
              <td>{{ $i++ }}</td>
              <td>{{ $donation->firstName }} {{ $donation->lastName }}</td>
              <td>{{ $donation->currency_shortname }} {{ $donation->donation_amount }}</td>
              <td>
                @if($donation->donation_payment_status=='pending')
                  <span class="badge badge-warning">pending</span>
                @elseif($donation->donation_payment_status=='successful')
                  <span class="badge badge-success">Successful</span>
                @else
                  <span class="badge badge-danger">Failed</span>
                @endif
              </td>
              <td>{{ $donation->bank_name }}</td>
              <td>{{ format_date($donation->created_at) }}</td>
            </tr>
  @endforeach
  @else
    <tr><td colspan="6" class="text-center"><b>No user queries Available</b></td></tr>
@endif

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@include('admin.inc.footer')
<!-- content-wrapper ends -->
<script type="text/javascript">
$(document).ready(function() {  

  dataTable = $('#donations-list').DataTable( {
    dom: 'lBfrtip',
    responsive: true,
    ordering : true,
    lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
    pageLength: 10,
    buttons: [
      {
          extend: 'print',
          text: '<button class="btn btn-primary btn-sm" ><i class="fa fa-print fa-x5"> Print</i></button>',
          autoPrint: false,
          exportOptions: {
              columns: [ 0,1,2,3,4,5 ]
          }
      },
      {
          extend: 'excelHtml5',
          text: '<button class="btn btn-success btn-sm"><i class="fa fa-file-excel-o fa-x5"> Excel</i></button>',
          exportOptions: {
              columns: [ 0,1,2,3,4,5 ]
          }
      },
      {
          extend: 'pdfHtml5',
          text: '<button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o fa-x5"> PDF</i></button>',
          exportOptions: {
              columns:[ 0,1,2,3,4,5 ]
          }
      }
    ],
    "processing": true,
    "serverSide": false,
  });
}); 
</script>