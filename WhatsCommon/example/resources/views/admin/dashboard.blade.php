@section('title', 'Dashboard')
@include('admin.inc.header')
@include('admin.inc.sidebar')
<meta name="_token" content="{{ csrf_token() }}">
  <div class="content-wrapper">
    <div class="row">
      <div class="col grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="d-flex align-items-center justify-content-center">
              <div class="highlight-icon bg-light mr-3">
                <i class="mdi mdi-account-multiple text-danger icon-lg"></i>
              </div>
              <div class="wrapper">
                <p class="card-text mb-0"> Users</p>
                <div class="fluid-container">
                  <h5 class="card-title mb-0"> {{ $counts->total_users ? $counts->total_users : 0 }} </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="d-flex align-items-center justify-content-center">
              <div class="highlight-icon bg-light mr-3">
                <i class="mdi mdi-cube text-success icon-lg"></i>
              </div>
              <div class="wrapper">
                <p class="card-text mb-0"> Life Events </p>
                <div class="fluid-container">
                  <h5 class="card-title mb-0"> {{ $counts->total_life_event ? $counts->total_life_event : 0 }} </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <div class="d-flex align-items-center justify-content-center">
              <div class="highlight-icon bg-light mr-3">
                <i class="mdi mdi-briefcase-check text-primary icon-lg"></i>
              </div>
              <div class="wrapper">
                <p class="card-text mb-0"> Matches</p>
                <div class="fluid-container">
                  <h5 class="card-title mb-0"> {{ $counts->total_matched ? $counts->total_matched : 0 }} </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <section class="panel">
              <div class="panel-body text-center" id="line">
                <canvas  height="300" width="450"></canvas>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <section class="panel">
              <div class="panel-body text-center" >
                <div class="row">
                  <div class="col-md-6"></div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control" id="event_type" onchange="lifeEventChange();">
                        <option value="all" {{ (!Request::get('type') || Request::get('type')=='all') ? 'selected': '' }}>All</option>
                        <option value="1" {{ (Request::get('type')=='1') ? 'selected': '' }}>Personal</option>
                        <option value="2" {{ (Request::get('type')=='2') ? 'selected': '' }}>Dating</option>
                        <option value="3" {{ (Request::get('type')=='3') ? 'selected': '' }}>Adoption</option>
                        <option value="4" {{ (Request::get('type')=='4') ? 'selected': '' }}>Travel</option>
                        <option value="5" {{ (Request::get('type')=='5') ? 'selected': '' }}>Military</option>
                        <option value="6" {{ (Request::get('type')=='6') ? 'selected': '' }}>Education</option>
                        <option value="7" {{ (Request::get('type')=='7') ? 'selected': '' }}>Work</option>
                        <option value="8" {{ (Request::get('type')=='8') ? 'selected': '' }}>Pets</option>
                        <option value="9" {{ (Request::get('type')=='9') ? 'selected': '' }}>Lost & Found</option>
                        <option value="10" {{ (Request::get('type')=='10') ? 'selected': '' }}>Doppelganger</option>
                        <option value="11" {{ (Request::get('type')=='11') ? 'selected': '' }}>Username</option>
                        <option value="12" {{ (Request::get('type')=='12') ? 'selected': '' }}>Activity</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12" id="life-events-graph">
                    <div class="text-center" id="line2">
                      <canvas  height="300" width="450" ></canvas>
                    </div>
                  </div>
                </div>
                
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <section class="panel">
              <div class="panel-body text-center" id="line3">
                <canvas  height="300" width="450"></canvas>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <section class="panel">
              <div class="panel-body text-center" id="line4">
                <canvas  height="300" width="450"></canvas>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <section class="panel">
              <div class="panel-body text-center">
                <div id="piechart" style="width: auto; height: 300px;"></div>
              </div>
            </section>
          </div>
        </div>
      </div>
      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <section class="panel">
              <div class="panel-body text-center">
                <div class="row">
                  <div class="col-md-6"></div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control" id="time" onchange="getFeedbackCategory();">
                        <option value="all" {{ (!Request::get('time') || Request::get('time')=='all') ? 'selected': '' }}>All</option>
                        {{-- <option value="1" {{ (Request::get('time')=='6') ? 'selected': '' }}>Last Month</option> --}}
                        <option value="6" {{ (Request::get('time')=='6') ? 'selected': '' }}>6 Months</option>
                        <option value="12" {{ (Request::get('time')=='12') ? 'selected': '' }}>1 Year</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12" id="feedback-category-graph">
                    <div id="piechart3" style="width: auto; height: 300px;"></div>
                  </div>
                </div>
                
              </div>
            </section>
          </div>
        </div>
      </div>
      
    </div>
    <div class="row">
      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <section class="panel">
              <div class="panel-body text-center">
                <div id="piechart4" style="width: auto; height: 300px;"></div>
              </div>
            </section>
          </div>
        </div>
      </div>
      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <section class="panel">
              <div class="panel-body text-center">
                <div id="piechart5" style="width: auto; height: 300px;"></div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
    
    <div class="row">
      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <section class="panel">
              <div class="panel-body text-center">
                <div id="piechart6" style="width: auto; height: 300px;"></div>
              </div>
            </section>
          </div>
        </div>
      </div>
      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <section class="panel">
              <div class="panel-body text-center">
                <div id="piechart2" style="width: auto; height: 300px;"></div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <section class="panel">
              <div class="panel-body text-center">
                <div id="piechart7" style="width: auto; height: 300px;"></div>
              </div>
            </section>
          </div>
        </div>
      </div>
      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card" style="display: none;">
        <div class="card card-statistics">
          <div class="card-body">
            <section class="panel">
              <div class="panel-body text-center">
                <div id="piechart8" style="width: auto; height: 300px;"></div>
              </div>
            </section>
          </div>
        </div>
      </div>
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
        <div class="card card-statistics">
          <div class="card-body">
            <section class="panel">
              <div class="panel-body text-center" id="geo">
                <h4 class="card-title">Demographic Data</h4>
                <table class="table table-bordered table-hover">
                  <thead class="thead-dark">
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Country</th>
                      <th scope="col">State</th>
                      <th scope="col">User</th>
                      <th scope="col">Premium</th>
                      <th scope="col">Donation</th>
                    </tr>
                  </thead>
                  <tbody>
    @if($demography)
      @foreach($demography as $i => $country)
                    <tr class="table-active">
                      <th scope="row">{{ $i+1 }}</th>
                      <td>{{ $country->country_name }}</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
        @foreach($country->provinces as $province)
                    <tr>
                      <th scope="row"></th>
                      <td></td>
                      <td>{{ $province->province_name }}</td>
                      <td>{{ $province->user_count }}</td>
                      <td>{{ $province->premium_count ? $province->premium_count : 0 }}</td>
                      <td>{{ $province->donation ? $province->donation : 0.00 }}</td>
                    </tr>
        @endforeach
      @endforeach
    @else
                    <tr>
                      <td colspan="6">No Data Available</td>
                    </tr>
    @endif
                  </tbody>
                </table>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
@php
  if(!Request::get('type') || Request::get('type')=='all'){
    $life_event_name = 'All Life Events';
  }else{
    if(Request::get('type')=='1'){
      $life_event_name = 'Personal';
    }
    if(Request::get('type')=='2'){
      $life_event_name = 'Dating';
    }
    if(Request::get('type')=='3'){
      $life_event_name = 'Adoption';
    }
    if(Request::get('type')=='4'){
      $life_event_name = 'Travel';
    }
    if(Request::get('type')=='5'){
      $life_event_name = 'Military';
    }
    if(Request::get('type')=='6'){
      $life_event_name = 'Education';
    }
    if(Request::get('type')=='7'){
      $life_event_name = 'Work';
    }
    if(Request::get('type')=='8'){
      $life_event_name = 'Pets';
    }
    if(Request::get('type')=='9'){
      $life_event_name = 'Lost & Found';
    }
    if(Request::get('type')=='10'){
      $life_event_name = 'Doppelganger';
    }
    if(Request::get('type')=='11'){
      $life_event_name = 'Username';
    }
    if(Request::get('type')=='12'){
      $life_event_name = 'Activity';
    }
  }

@endphp
  <!-- content-wrapper ends -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['line']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Day');
    data.addColumn('number', 'Users');
    data.addColumn('number', 'Life Events');
    data.addColumn('number', 'Matches');
    data.addColumn('number', 'Connections');

    data.addRows([
@if($graph_counts)
  @foreach($graph_counts as $graph)
      ['{{ date("d/m",strtotime($graph["date"])) }}', {{ $graph["user_count"] }}, {{ $graph["event_count"] }}, {{ $graph["match_count"] }},{{ $graph["connection_count"] }}],
  @endforeach
@endif
    ]);
    var options = {
      chart: {
        title: 'Last 10 Days Activity',
        subtitle: ''
      },
      axes: {
        x: {
          0: {side: 'top'}
        }
      },
    };

    var chart = new google.charts.Line(document.getElementById('line'));

    chart.draw(data, google.charts.Line.convertOptions(options));
  }

  google.charts.load('current', {'packages':['line']});
  google.charts.setOnLoadCallback(drawChart2);

  function drawChart2() {

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Day');
    data.addColumn('number', '{{ $life_event_name }}');

    data.addRows([
@if($graph_counts)
  @foreach($graph_counts as $graph)

@php
  if(!Request::get('type') || Request::get('type')=='all'){
    $count = $graph["event_count"];
  }else if(isset($graph['check_count'])){
    $count = $graph["check_count"];
  }else{
    $count = 0;
  }

@endphp

      ['{{ date("d/m",strtotime($graph["date"])) }}', {{ $count }}],
  @endforeach
@endif
    ]);
    var options = {
      chart: {
        title: 'Last 10 Days Life Events',
        subtitle: ''
      },
      axes: {
        x: {
          0: {side: 'top'}
        }
      },
    };

    var chart = new google.charts.Line(document.getElementById('line2'));

    chart.draw(data, google.charts.Line.convertOptions(options));
  }
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart3);

  function drawChart3() {
@php
$total = ($logintype_graphs->web_count ? $logintype_graphs->web_count : 0) + ($logintype_graphs->android_count ? $logintype_graphs->android_count : 0) + ($logintype_graphs->ios_count ? $logintype_graphs->ios_count : 0) + ($logintype_graphs->facebook_count ? $logintype_graphs->facebook_count : 0) + ($logintype_graphs->google_count ? $logintype_graphs->google_count : 0) + ($logintype_graphs->apple_count ? $logintype_graphs->apple_count : 0);
@endphp
    var data = google.visualization.arrayToDataTable([
      ['Login From',    'Counts'],
      ['Website',       {{ $logintype_graphs->web_count ? $logintype_graphs->web_count : 0 }}],
      ['App(Android)',  {{ $logintype_graphs->android_count ? $logintype_graphs->android_count : 0 }}],
      ['App(iOS)',      {{ $logintype_graphs->ios_count ? $logintype_graphs->ios_count : 0 }}],
      ['Facebook',      {{ $logintype_graphs->facebook_count ? $logintype_graphs->facebook_count : 0 }}],
      ['Google',        {{ $logintype_graphs->google_count ? $logintype_graphs->google_count : 0 }}],
      ['Apple',         {{ $logintype_graphs->apple_count ? $logintype_graphs->apple_count : 0 }}]
    ]);

    var options = {
      title: 'Login activities for last one year {{ " (Total : ".$total.")" }}',
      colors: ['#91d444', '#3b98d8', '#ffc999', '#ffaf67', '#ffe9c1', '#d0d4ff'],
      /*is3D: true,*/
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

    chart.draw(data, options);
  }

  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart4);

  function drawChart4() {

    var data = google.visualization.arrayToDataTable([
      ['Life Event Category',    'Match Counts'],
      ['Personal',          {{ $match_by_type->personal_count ? $match_by_type->personal_count : 0 }}],
      ['Dating',            {{ $match_by_type->dating_count ? $match_by_type->dating_count : 0 }}],
      ['Adoption',          {{ $match_by_type->adoption_count ? $match_by_type->adoption_count : 0 }}],
      ['Travel / Vacation', {{ $match_by_type->travel_count ? $match_by_type->travel_count : 0 }}],
      ['Military',          {{ $match_by_type->military_count ? $match_by_type->military_count : 0 }}],
      ['School / Education',{{ $match_by_type->school_count ? $match_by_type->school_count : 0 }}],
      ['Careers ',{{ $match_by_type->career_count ? $match_by_type->career_count : 0 }}],
      ['Pets',              {{ $match_by_type->pets_count ? $match_by_type->pets_count : 0 }}],
      ['Lost & Found',      {{ $match_by_type->lost_count ? $match_by_type->lost_count : 0 }}],
      ['Doppelganger',     {{ $match_by_type->dropel_count ? $match_by_type->dropel_count : 0 }}],
      ['Username Connect',  {{ $match_by_type->username_count ? $match_by_type->username_count : 0 }}],
    ]);
@php
$total = ($match_by_type->personal_count ? $match_by_type->personal_count : 0) + ($match_by_type->dating_count ? $match_by_type->dating_count : 0) + ($match_by_type->adoption_count ? $match_by_type->adoption_count : 0) + ($match_by_type->travel_count ? $match_by_type->travel_count : 0) + ($match_by_type->military_count ? $match_by_type->military_count : 0) + ($match_by_type->school_count ? $match_by_type->school_count : 0) + ($match_by_type->career_count ? $match_by_type->career_count : 0) + ($match_by_type->pets_count ? $match_by_type->pets_count : 0) + ($match_by_type->lost_count ? $match_by_type->lost_count : 0) + ($match_by_type->dropel_count ? $match_by_type->dropel_count : 0) + ($match_by_type->username_count ? $match_by_type->username_count : 0);
@endphp
    var options = {
      title: 'Life Event Matches{{ " (Total : ".$total.")" }}',
      colors: ['#91d444', '#3b98d8', '#ffc999', '#ffaf67', '#ffe9c1', '#d0d4ff', '#fec1ff', '#c1c6ff', '#ebed2e', '#99cc33', '#ffb1b1'],
      /*is3D: true,*/
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart2'));

    chart.draw(data, options);
  }
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart5);

  function drawChart5() {

    var data = google.visualization.arrayToDataTable([
      ['Feedback Category',    'Feedback Counts'],
      ['Performance',     {{ $feedback_type_graph->perform_count ? $feedback_type_graph->perform_count : 0 }}],
      ['Bugs',            {{ $feedback_type_graph->bug_count ? $feedback_type_graph->bug_count : 0 }}],
      ['General',         {{ $feedback_type_graph->general_count ? $feedback_type_graph->general_count : 0 }}],
    ]);
@php
$total = ($feedback_type_graph->perform_count ? $feedback_type_graph->perform_count : 0) + ($feedback_type_graph->bug_count ? $feedback_type_graph->bug_count : 0) + ($feedback_type_graph->general_count ? $feedback_type_graph->general_count : 0);
@endphp
    var options = {
      title: 'Feedback Categories{{ " (Total : ".$total.")" }}',
      colors: ['#91d444', '#3b98d8', '#ffc999'],
      /*is3D: true,*/
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart3'));

    chart.draw(data, options);
  }
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart6);

  function drawChart6() {

    var data = google.visualization.arrayToDataTable([
      ['User Type',    'User Counts'],
      ['Premium User',     {{ $user_types['premium_user'] ? $user_types['premium_user'] : 0 }}],
      ['Non-premium User', {{ $user_types['non_prem_user'] ? $user_types['non_prem_user'] : 0 }}],
    ]);
@php
$total = ($user_types['premium_user'] ? $user_types['premium_user'] : 0) + ($user_types['non_prem_user'] ? $user_types['non_prem_user'] : 0);
@endphp
    var options = {
      title: 'Premium Conversion Ratio{{ " (Total : ".$total.")" }}',
      colors: ['#91d444', '#3b98d8'],
      /*slices: {
        0: { color: 'blue' },
        1: { color: 'black' }
      },*/
      /*legend : {position: 'right', textStyle: {color: 'blue', fontSize: 16}},*/
      /*is3D: true,*/
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart4'));

    chart.draw(data, options);
  }
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart11);

  function drawChart11() {

    var data = google.visualization.arrayToDataTable([
      ['User Type',    'User Counts'],
      ['Frequent User',     {{ $user_types['frequent_users'] ? $user_types['frequent_users'] : 0 }}],
      ['Active User', {{ $user_types['active_users'] ? $user_types['active_users'] : 0 }}],
    ]);
@php
$total = ($user_types['frequent_users'] ? $user_types['frequent_users'] : 0) + ($user_types['active_users'] ? $user_types['active_users'] : 0);
@endphp
    var options = {
      title: 'Frequent vs Active Users{{ " (Total : ".$total.")" }}',
      colors: ['#91d444', '#3b98d8'],
      /*is3D: true,*/
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart7'));

    chart.draw(data, options);
  }
  google.charts.load('current', {'packages':['line']});
  google.charts.setOnLoadCallback(drawChart7);

  function drawChart7() {

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Month');
    data.addColumn('number', 'Amount');
    data.addRows([
@if($donation_feedback_counts)
  @foreach($donation_feedback_counts as $graph)
      ['{{ date("M/y",strtotime($graph["date"])) }}', {{ $graph["monthly_donation"] ? $graph["monthly_donation"] : 0 }}],
  @endforeach
@endif
    ]);
    var options = {
      chart: {
        title: 'Monthly Donations',
        subtitle: ''
      },
      axes: {
        x: {
          0: {side: 'top'}
        }
      },
    };

    var chart = new google.charts.Line(document.getElementById('line3'));

    chart.draw(data, google.charts.Line.convertOptions(options));
  }
  google.charts.load('current', {'packages':['line']});
  google.charts.setOnLoadCallback(drawChart8);

  function drawChart8() {

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Month');
    data.addColumn('number', 'No. of Feedbacks');
    data.addRows([
@if($donation_feedback_counts)
  @foreach($donation_feedback_counts as $graph)
      ['{{ date("M/y",strtotime($graph["date"])) }}', {{ $graph["monthly_feedback"] ? $graph["monthly_feedback"] : 0 }}],
  @endforeach
@endif
    ]);
    var options = {
      chart: {
        title: 'Monthly Feedback',
        subtitle: ''
      },
      axes: {
        x: {
          0: {side: 'top'}
        }
      },
    };

    var chart = new google.charts.Line(document.getElementById('line4'));

    chart.draw(data, google.charts.Line.convertOptions(options));
  }

  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart9);

  function drawChart9() {

    var data = google.visualization.arrayToDataTable([
      ['Premium Type',    'Premium Counts'],
      ['Monthly',     {{ $premium_users->monthly_counts ? $premium_users->monthly_counts : 0 }}],
      ['Yearly',            {{ $premium_users->yearly_counts ? $premium_users->yearly_counts : 0 }}],
      ['Lifetime',         {{ $premium_users->lifetime_counts ? $premium_users->lifetime_counts : 0 }}],
    ]);
@php
$total = ($premium_users->monthly_counts ? $premium_users->monthly_counts : 0) + ($premium_users->yearly_counts ? $premium_users->yearly_counts : 0) + ($premium_users->lifetime_counts ? $premium_users->lifetime_counts : 0);
@endphp
    var options = {
      title: 'Premium User Categories{{ " (Total : ".$total.")" }}',
      colors: ['#91d444', '#3b98d8'],
      /*is3D: true,*/
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart5'));

    chart.draw(data, options);
  }

  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart10);

  function drawChart10() {
@php
$total = 0;
@endphp
    var data = google.visualization.arrayToDataTable([
      ['Event Type',    'Connection Counts'],
  @if($event_categories)
    @foreach($event_categories as $category)
    ['{{ $category->event_type }}', {{ $category->connections_count ? $category->connections_count : 0 }}],
@php
$total += $category->connections_count ? $category->connections_count : 0;
@endphp
    @endforeach
  @endif
      
    ]);

    var options = {
      title: 'Connections Per Life Events{{ " (Total : ".$total.")" }}',
      colors: ['#91d444', '#3b98d8', '#ffc999', '#ffaf67', '#ffe9c1', '#d0d4ff', '#fec1ff', '#c1c6ff', '#ebed2e', '#99cc33', '#ffb1b1'],
      /*is3D: true,*/
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart6'));

    chart.draw(data, options);
  }
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart12);

  function drawChart12() {

    var data = google.visualization.arrayToDataTable([
      ['User Type',    'User Counts'],
      ['Premium User',     {{ $active_inactive->active ? $active_inactive->active : 0 }}],
      ['Cancelled Premiums User', {{ $active_inactive->inactive ? $active_inactive->inactive : 0 }}],
    ]);
@php
$total = ($active_inactive->active ? $active_inactive->active : 0) + ($active_inactive->inactive ? $active_inactive->inactive : 0);
@endphp
    var options = {
      title: 'Cancelled Premiums{{ " (Total : ".$total.")" }}',
      colors: ['#91d444', '#3b98d8'],
      /*slices: {
        0: { color: 'blue' },
        1: { color: 'black' }
      },*/
      /*legend : {position: 'right', textStyle: {color: 'blue', fontSize: 16}},*/
      /*is3D: true,*/
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart8'));

    chart.draw(data, options);
  }
  /*google.charts.load('current', {
    'packages': ['geochart'],
    // Note: Because markers require geocoding, you'll need a mapsApiKey.
    // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
    'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
  });
  google.charts.setOnLoadCallback(drawMarkersMap);

  function drawMarkersMap() {
    var data = google.visualization.arrayToDataTable([
      ['City',   'Population', 'Area'],
      ['Rome',      2761477,    1285.31],
      ['Milan',     1324110,    181.76],
      ['Naples',    959574,     117.27],
      ['Turin',     907563,     130.17],
      ['Palermo',   655875,     158.9],
      ['Genoa',     607906,     243.60],
      ['Bologna',   380181,     140.7],
      ['Florence',  371282,     102.41],
      ['Fiumicino', 67370,      213.44],
      ['Anzio',     52192,      43.43],
      ['Ciampino',  38262,      11]
    ]);

    var options = {
      region: 'IT',
      displayMode: 'markers',
      colorAxis: {colors: ['green', 'blue']}
    };

    var chart = new google.visualization.GeoChart(document.getElementById('geo'));
    chart.draw(data, options);
  };*/
function pageReload(){
  var event_type = $('#event_type').val();
  var time = $('#time').val();
  var from = $('#from_date').val();
  var to = $('#to_date').val();
  var page_url = site_url('admin/dashboard?type='+event_type+'&time='+time+'&from='+from+'&to='+to);
  $(location).attr("href", page_url);
}
function lifeEventChange(){
  var event_type = $('#event_type').val();
  if( event_type=="all" || $.inArray(event_type,[1,2,3,4,5,6,7,8,9,10,11,12])){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "event_type="+event_type, 
      url  : site_url('admin/refresh-life-event-graph'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.graph){
            $('#life-events-graph').html(response.reponse_body.graph);
            //$('#life-events-graph').html('');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
}
function getFeedbackCategory(){
  var time = $('#time').val();
  console.log('time => '+time);
  if(time=='all' || $.inArray(time,[6,12])){//1,
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "time="+time, 
      url  : site_url('admin/refresh-feedback-graph'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.graph){
            $('#feedback-category-graph').html(response.reponse_body.graph);
            //$('#life-events-graph').html('');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
        console.log(jqXHR.responseJSON);
      }
    });
  }
}
$(document).ready(function(){
  $('#line3').click(function(){
    //console.log('clicked');
    /*$(location).attr("href", "{{ url('admin/donations') }}");*/ 
  });
  $('#line4').click(function(){
    //console.log('clicked');
    /*$(location).attr("href", "{{ url('admin/feedbacks') }}");*/ 
  });
});
</script>
@include('admin.inc.footer')
