@section('title', 'Testimonials')
@include('admin.inc.header')
@include('admin.inc.sidebar')
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Testimonials</h4>
      <div class="row" style="margin-bottom: 15px;">
        <div class="col-9"></div>
        <meta name="_token" content="{{ csrf_token() }}">
        <div class="col-3 text-right">
          <a class="btn btn-secondary" href="javascript:void();" onclick="open_modal('add-testimonial');"><i class="fa fa-plus-circle text-success"></i> Add Testimonial</a>
        </div>
      </div>
      <div class="row">
  
        <div class="col-12">
          <table id="testimonials-list" class="table table-condensed table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Image</th>
                <th>Name & Address</th>
                <th>Text</th>
                <th>Primary</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
@if($testimonials)
  @php
    $i = 1
  @endphp
  @foreach ($testimonials as $range)
            <tr>
              <td>{{ $i++ }}</td>
              <td><img src="{{ $range->testimonial_userimage ? url($range->testimonial_userimage) : url('new-design/img/profile_placeholder.jpg') }}" class="img-fluid" style="max-height: 50px;border-radius: 0;width: auto;"></td>
              <td>
                {{ $range->testimonial_username }}<br>
                {{ $range->testimonial_useraddress }}
              </td>
              <td>{{ $range->testimonial_text }}</td>
              <td>
                @if($range->testimonial_is_primary==1) 
                <span class="badge badge-success">Yes</span>
                @else
                <span class="badge badge-danger">No</span>
                @endif
              </td>
              <td>
                @if($range->testimonial_is_active==1) 
                <span class="badge badge-success">Active</span>
                @else
                <span class="badge badge-danger">Inactive</span>
                @endif
              </td>
              <td>
                <a class="custom_btn _edit" onclick="open_modal('edit-testimonial','_testimonial_id={{$range->_testimonial_id}}');">
                  <i class="fa fa-edit"></i>
                </a>
                @if($range->testimonial_is_active==1)
                  <a class="custom_btn _edit" onclick="execute('admin/active-inactive-testimonial','_testimonial_id={{$range->_testimonial_id}}&testimonial_is_active=1');" title="Inactivate">
                  <i class="fa fa-lock" aria-hidden="true"></i>
                  </a>
                  @else
                  <a class="custom_btn _edit" onclick="execute('admin/active-inactive-testimonial','_testimonial_id={{$range->_testimonial_id}}&testimonial_is_active=0');" title="Activate">
                  <i class="fa fa-unlock" aria-hidden="true"></i>
                  </a>
                @endif
                @if($range->testimonial_is_primary==1)
                  <a class="custom_btn _edit" onclick="execute('admin/primary-nonprimary-testimonial','_testimonial_id={{$range->_testimonial_id}}&testimonial_is_primary=0');" title="Make Non-primary">
                  <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                  </a>
                  @else
                  <a class="custom_btn _edit" onclick="execute('admin/primary-nonprimary-testimonial','_testimonial_id={{$range->_testimonial_id}}&testimonial_is_primary=1');" title="Make Primary">
                  <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                  </a>
                @endif
              </td>
            </tr>
  @endforeach
  @else
    <tr><td colspan="5" class="text-center"><b>No Testimonial Available</b></td></tr>
@endif

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@include('admin.inc.footer')
<!-- content-wrapper ends -->
<script type="text/javascript">
$(document).ready(function() {  

  dataTable = $('#testimonials-list').DataTable( {
    dom: 'lBfrtip',
    responsive: true,
    ordering : true,
    lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
    pageLength: 10,
    buttons: [
      {
          extend: 'print',
          text: '<button class="btn btn-primary btn-sm" ><i class="fa fa-print fa-x5"> Print</i></button>',
          autoPrint: false,
          exportOptions: {
              columns: [ 0,2,3,4 ]
          }
      },
      {
          extend: 'excelHtml5',
          text: '<button class="btn btn-success btn-sm"><i class="fa fa-file-excel-o fa-x5"> Excel</i></button>',
          exportOptions: {
              columns: [ 0,2,3,4 ]
          }
      },
      {
          extend: 'pdfHtml5',
          text: '<button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o fa-x5"> PDF</i></button>',
          exportOptions: {
              columns:[ 0,2,3,4 ]
          }
      }
    ],
    "processing": true,
    "serverSide": false,
  });
}); 
</script>