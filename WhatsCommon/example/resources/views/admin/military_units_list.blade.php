@section('title', 'Military Units')
@include('admin.inc.header')
@include('admin.inc.sidebar')
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Military Units</h4>
      <div class="row" style="margin-bottom: 15px;">
        <div class="col-9"></div>
        <meta name="_token" content="{{ csrf_token() }}">
        <div class="col-3 text-right">
          <a class="btn btn-secondary" href="javascript:void();" onclick="open_modal('add-military-unit');"><i class="fa fa-plus-circle text-success"></i> Add Military Unit</a>
        </div>
      </div>
      <div class="row">
  
        <div class="col-12">
          <table id="military_units-list" class="table table-condensed table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <!-- <th>Status</th> -->
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
@if($military_units)
  @php
    $i = 1
  @endphp
  @foreach ($military_units as $unit)
            <tr>
              <td>{{ $i++ }}</td>
              <td>{{ $unit->unit_name }}</td>
              
              <td>
                <a class="custom_btn _edit" onclick="open_modal('edit-military-unit','_unit_id={{$unit->_unit_id}}');">
                  <i class="fa fa-edit"></i>
                </a>
                
              </td>
            </tr>
  @endforeach
  @else
    <tr><td colspan="5" class="text-center"><b>No military unit Available</b></td></tr>
@endif

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@include('admin.inc.footer')
<!-- content-wrapper ends -->
<script type="text/javascript">
$(document).ready(function() {  

  dataTable = $('#military_units-list').DataTable( {
    dom: 'lBfrtip',
    responsive: true,
    ordering : true,
    lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
    pageLength: 10,
    buttons: [
      {
          extend: 'print',
          text: '<button class="btn btn-primary btn-sm" ><i class="fa fa-print fa-x5"> Print</i></button>',
          autoPrint: false,
          exportOptions: {
              columns: [ 0,1 ]
          }
      },
      {
          extend: 'excelHtml5',
          text: '<button class="btn btn-success btn-sm"><i class="fa fa-file-excel-o fa-x5"> Excel</i></button>',
          exportOptions: {
              columns: [ 0,1 ]
          }
      },
      {
          extend: 'pdfHtml5',
          text: '<button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o fa-x5"> PDF</i></button>',
          exportOptions: {
              columns:[ 0,1 ]
          }
      }
    ],
    "processing": true,
    "serverSide": false,
  });
}); 
</script>