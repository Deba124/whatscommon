@section('title', 'Users')
@include('admin.inc.header')
@include('admin.inc.sidebar')
<style type="text/css">
/*.table td, .jsgrid .jsgrid-table td, .table th, .jsgrid .jsgrid-table th{
  line-height: 1.5;
}*/
</style>
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Users</h4>
      <div class="row" style="margin-bottom: 15px;">
        <div class="col-3"></div>
        <div class="col-3">
          <select class="form-control" id="country_id" onchange="pageReload();">
            <option value="">Select Country</option>
    @if($countries)
      @foreach($countries as $country)
            <option value="{{ $country->_country_id }}" {{ $country->_country_id==Request::get('_country_id') ? 'selected' : '' }} >{{ $country->country_name }}</option>
      @endforeach
    @endif
          </select>
        </div>
        <div class="col-3">
          <select class="form-control" id="province_id" onchange="pageReload();">
            <option value="">Select State</option>
    @if($provinces)
      @foreach($provinces as $province)
            <option value="{{ $province->_province_id }}" {{ $province->_province_id==Request::get('_province_id') ? 'selected' : '' }}>{{ $province->province_name }}</option>
      @endforeach
    @endif
          </select>
        </div>
        <div class="col-3">
          <select class="form-control" id="city_id" onchange="pageReload();">
            <option value="">Select City</option>
    @if($cities)
      @foreach($cities as $city)
            <option value="{{ $city->_city_id }}" {{ $city->_city_id==Request::get('_city_id') ? 'selected' : '' }}>{{ $city->city_name }}</option>
      @endforeach
    @endif
          </select>
        </div>
        
        <!-- <div class="col-3 text-right">
          <a class="btn btn-secondary" href="javascript:void();" onclick="open_modal('add-company');"><i class="fa fa-plus-circle text-success"></i> Add Company</a>
        </div> -->
      </div>
      <div class="row">
  
        <div class="col-12">
          <table id="users-list" class="table table-condensed table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>User Details</th>
                <th>Address</th>
                <th>Register Type</th>
                <th>Status</th>
                {{-- <th>Premium</th> --}}
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
@if($users)
  @php
    $i = 1
  @endphp
  @foreach ($users as $user)
            <tr>
              <td>{{ $i++ }}</td>
              <td>
                <p>
                  <b>Name: </b>{{ $user->firstName }} {{ $user->lastName }}</p>
                <p>
                  <b>Email: </b>{{ $user->email }}</p>
                <p>
                  <b>Phone: </b>{{ $user->isd_code }} {{ $user->phone }}
                </p>
              </td>
              <td>
                <p><b>City: </b>{{ $user->city_name }}</p>
                <p><b>State: </b>{{ $user->province_name }}</p>
                <p><b>Country: </b>{{ $user->country_name }}</p>
              </td>
              <td class="text-center">
                @if($user->social_type)
                  {{ ucwords($user->social_type) }}
                @else
                  Normal
                @endif
              </td>
              <td>
                @if($user->user_is_active==1) 
                <span class="badge badge-success">Active</span>
                @else
                <span class="badge badge-danger">Inactive</span>
                @endif
              </td>
              {{-- <td>
              @if($user->is_premium==1)
                <span class="badge badge-success">Yes</span>
              @else
                <span class="badge badge-danger">No</span>
              @endif
              </td> --}}
              <td>
              @if($user->user_is_active==1)
                <a class="custom_btn _edit" onclick="execute('admin/active-inactive-user','_user_id={{$user->id}}&user_is_active=1');" title="Inactivate">
                  <i class="fa fa-lock" aria-hidden="true"></i>
                </a>
              @else
                <a class="custom_btn _edit" onclick="execute('admin/active-inactive-user','_user_id={{$user->id}}&user_is_active=0');" title="Activate">
                  <i class="fa fa-unlock" aria-hidden="true"></i>
                </a>
              @endif

              {{-- @if($user->is_premium==1)
                <a class="custom_btn _edit" onclick="execute('admin/premium-nonpremium-user','_user_id={{$user->id}}&user_is_premium=1');" title="Remove Premium Membership">
                  <i class="fa fa-toggle-on" aria-hidden="true"></i>
                </a>
              @else
                <a class="custom_btn _edit" onclick="execute('admin/premium-nonpremium-user','_user_id={{$user->id}}&user_is_premium=0');" title="Make Premium Member">
                  <i class="fa fa-toggle-off" aria-hidden="true"></i>
                </a>
              @endif --}}
              </td>
            </tr>
  @endforeach
  @else
    <tr><td colspan="6" class="text-center"><b>No User Available</b></td></tr>
@endif

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@include('admin.inc.footer')
<!-- content-wrapper ends -->
<script type="text/javascript">
$(document).ready(function() {  

  dataTable = $('#users-list').DataTable( {
    dom: 'lBfrtip',
    responsive: true,
    ordering : true,
    lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
    pageLength: 10,
    buttons: [
      {
          extend: 'print',
          text: '<button class="btn btn-primary btn-sm" ><i class="fa fa-print fa-x5"> Print</i></button>',
          autoPrint: false,
          exportOptions: {
              columns: [ 0,1,2,3,4 ]
          }
      },
      {
          extend: 'excelHtml5',
          text: '<button class="btn btn-success btn-sm"><i class="fa fa-file-excel-o fa-x5"> Excel</i></button>',
          exportOptions: {
              columns: [ 0,1,2,3,4 ]
          }
      },
      {
          extend: 'pdfHtml5',
          text: '<button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o fa-x5"> PDF</i></button>',
          exportOptions: {
              columns:[ 0,1,2,3,4 ]
          }
      }
    ],
    "processing": true,
    "serverSide": false,
  });
});
function pageReload(){
  var _country_id = $('#country_id').val();
  var _province_id = $('#province_id').val();
  var _city_id = $('#city_id').val();
  var page_url = site_url('admin/users?_country_id='+_country_id+'&_province_id='+_province_id+'&_city_id='+_city_id);
  $(location).attr("href", page_url);
}
</script>
