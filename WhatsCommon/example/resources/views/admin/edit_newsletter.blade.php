@section('title', 'Update News Letter')
@include('admin.inc.header')
@include('admin.inc.sidebar')
  <div class="content-wrapper">
    <div class="card">
      <div class="card-body">
        <form method="POST" class="form-horizontal admin_form" id="update-news-letter">
          <div class="form-group" id="submit_status"></div>
          <input type="hidden" name="_nt_id" value="{{$nt_data->_nt_id}}">
          @csrf
          <div class="form-group">
            <label>Title<span class="text-danger">*</span></label>
            <input type="text" name="nt_title" class="form-control" value="{{ $nt_data->nt_title }}">
            <p class="text-danger" id="nt_title_error"></p>
          </div>
          <div class="form-group">
            <label>Content<span class="text-danger">*</span></label>
            <textarea class="form-control ckeditor" name="nt_content" rows="5">{{ $nt_data->nt_content }}</textarea>
            <p class="text-danger" id="nt_content_error"></p>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- content-wrapper ends -->
@include('admin.inc.footer')