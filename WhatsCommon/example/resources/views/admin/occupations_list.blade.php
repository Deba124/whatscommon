@section('title', 'Categories')
@include('admin.inc.header')
@include('admin.inc.sidebar')
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Categories</h4>
      <div class="row" style="margin-bottom: 15px;">
        <div class="col-9"></div>
        <meta name="_token" content="{{ csrf_token() }}">
        <div class="col-3 text-right">
          <a class="btn btn-secondary" href="javascript:void();" onclick="open_modal('add-category');"><i class="fa fa-plus-circle text-success"></i> Add Category</a>
        </div>
      </div>
      <div class="row">
  
        <div class="col-12">
          <table id="occupations-list" class="table table-condensed table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Date Created</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
@if($occupations)
  @php
    $i = 1
  @endphp
  @foreach ($occupations as $occupation)
            <tr>
              <td>{{ $i++ }}</td>
              <td>{{ $occupation->occupation_name }}</td>
              <td>{{date('j F, Y',strtotime($occupation->created_at)) }}</td>
              <td>
                @if($occupation->occupation_is_active==1) 
                <span class="badge badge-success">Active</span>
                @else
                <span class="badge badge-danger">Inactive</span>
                @endif
              </td>
              <td>
                <a class="custom_btn _edit" onclick="open_modal('edit-category','_occupation_id={{$occupation->_occupation_id}}');">
                  <i class="fa fa-edit"></i>
                </a>
                @if($occupation->occupation_is_active==1)
                  <a class="custom_btn _edit" onclick="execute('admin/active-inactive-occupation','_occupation_id={{$occupation->_occupation_id}}&occupation_is_active=1');" title="Inactivate">
                  <i class="fa fa-lock" aria-hidden="true"></i>
                  </a>
                  @else
                  <a class="custom_btn _edit" onclick="execute('admin/active-inactive-occupation','_occupation_id={{$occupation->_occupation_id}}&occupation_is_active=0');" title="Activate">
                  <i class="fa fa-unlock" aria-hidden="true"></i>
                  </a>
                @endif
              </td>
            </tr>
  @endforeach
  @else
    <tr><td colspan="4" class="text-center"><b>No occupation Available</b></td></tr>
@endif

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@include('admin.inc.footer')
<!-- content-wrapper ends -->
<script type="text/javascript">
$(document).ready(function() {  

  dataTable = $('#occupations-list').DataTable( {
    dom: 'lBfrtip',
    responsive: true,
    ordering : true,
    lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
    pageLength: 10,
    buttons: [
      {
          extend: 'print',
          text: '<button class="btn btn-primary btn-sm" ><i class="fa fa-print fa-x5"> Print</i></button>',
          autoPrint: false,
          exportOptions: {
              columns: [ 0,1,2 ]
          }
      },
      {
          extend: 'excelHtml5',
          text: '<button class="btn btn-success btn-sm"><i class="fa fa-file-excel-o fa-x5"> Excel</i></button>',
          exportOptions: {
              columns: [ 0,1,2 ]
          }
      },
      {
          extend: 'pdfHtml5',
          text: '<button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o fa-x5"> PDF</i></button>',
          exportOptions: {
              columns:[ 0,1,2 ]
          }
      }
    ],
    "processing": true,
    "serverSide": false,
  });
}); 
</script>