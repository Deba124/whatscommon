@section('title', 'Vehicle Types')
@include('admin.inc.header')
@include('admin.inc.sidebar')
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Vehicle Types</h4>
      <div class="row" style="margin-bottom: 15px;">
        <div class="col-9"></div>
        <meta name="_token" content="{{ csrf_token() }}">
        <div class="col-3 text-right">
          <a class="btn btn-secondary" href="javascript:void();" onclick="open_modal('add-vehicle-type');"><i class="fa fa-plus-circle text-success"></i> Add Vehicle Type</a>
        </div>
      </div>
      <div class="row">
  
        <div class="col-12">
          <table id="vehicle_types-list" class="table table-condensed table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Date Created</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
@if($vehicle_types)
  @php
    $i = 1
  @endphp
  @foreach ($vehicle_types as $vehicle_type)
            <tr>
              <td>{{ $i++ }}</td>
              <td>{{ $vehicle_type->vehicle_type_name }}</td>
              <td>{{ format_date($vehicle_type->created_at) }}</td>
              <td>
                @if($vehicle_type->vehicle_type_is_active==1) 
                <span class="badge badge-success">Active</span>
                @else
                <span class="badge badge-danger">Inactive</span>
                @endif
              </td>
              <td>
                <a class="custom_btn _edit" href="{{url('admin/vehicle-makers/?type='.$vehicle_type->id)}}">
                  <i class="fa fa-list"></i>
                </a>
                <a class="custom_btn _edit" onclick="open_modal('edit-vehicle-type','_vehicle_type_id={{$vehicle_type->id}}');">
                  <i class="fa fa-edit"></i>
                </a>
                @if($vehicle_type->vehicle_type_is_active==1)
                  <a class="custom_btn _edit" onclick="execute('admin/active-inactive-vehicle-type','_vehicle_type_id={{$vehicle_type->id}}&vehicle_type_is_active=1');" title="Inactivate">
                  <i class="fa fa-lock" aria-hidden="true"></i>
                  </a>
                  @else
                  <a class="custom_btn _edit" onclick="execute('admin/active-inactive-vehicle-type','_vehicle_type_id={{$vehicle_type->id}}&vehicle_type_is_active=0');" title="Activate">
                  <i class="fa fa-unlock" aria-hidden="true"></i>
                  </a>
                @endif
              </td>
            </tr>
  @endforeach
  @else
    <tr><td colspan="4" class="text-center"><b>No vehicle type Available</b></td></tr>
@endif

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@include('admin.inc.footer')
<!-- content-wrapper ends -->
<script type="text/javascript">
$(document).ready(function() {  

  dataTable = $('#vehicle_types-list').DataTable( {
    dom: 'lBfrtip',
    responsive: true,
    ordering : true,
    lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
    pageLength: 10,
    buttons: [
      {
          extend: 'print',
          text: '<button class="btn btn-primary btn-sm" ><i class="fa fa-print fa-x5"> Print</i></button>',
          autoPrint: false,
          exportOptions: {
              columns: [ 0,1,2 ]
          }
      },
      {
          extend: 'excelHtml5',
          text: '<button class="btn btn-success btn-sm"><i class="fa fa-file-excel-o fa-x5"> Excel</i></button>',
          exportOptions: {
              columns: [ 0,1,2 ]
          }
      },
      {
          extend: 'pdfHtml5',
          text: '<button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o fa-x5"> PDF</i></button>',
          exportOptions: {
              columns:[ 0,1,2 ]
          }
      }
    ],
    "processing": true,
    "serverSide": false,
  });
}); 
</script>