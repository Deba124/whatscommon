@section('title', 'Contact Us')
@include('admin.inc.header')
@include('admin.inc.sidebar')
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Contact Us</h4>
      <div class="row" style="margin-bottom: 15px;">
        <div class="col-9"></div>
        <meta name="_token" content="{{ csrf_token() }}">
        <div class="col-3 text-right">
          <a class="btn btn-secondary" href="javascript:void();" onclick="open_modal('get-contact-setting');"><i class="fa fa-cogs text-success"></i> Update Setting</a>
        </div>
      </div>
      <div class="row">
  
        <div class="col-12">
          <table id="contacts-list" class="table table-condensed table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Message</th>
                <th>Date Created</th>
                {{-- <th>Actions</th> --}}
              </tr>
            </thead>
            <tbody>
@if($contacts)
  @php
    $i = 1
  @endphp
  @foreach ($contacts as $contact)
            <tr>
              <td>{{ $i++ }}</td>
              <td>{{ $contact->name }}</td>
              <td>{{ $contact->email }}</td>
              <td>{{ $contact->message }}</td>
              <td>{{ format_date($contact->created_at) }}</td>
            </tr>
  @endforeach
  @else
    <tr><td colspan="5" class="text-center"><b>No Contact Us Available</b></td></tr>
@endif

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@include('admin.inc.footer')
<!-- content-wrapper ends -->
<script type="text/javascript">
$(document).ready(function() {  

  dataTable = $('#contacts-list').DataTable( {
    dom: 'lBfrtip',
    responsive: true,
    ordering : true,
    lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
    pageLength: 10,
    buttons: [
      {
          extend: 'print',
          text: '<button class="btn btn-primary btn-sm" ><i class="fa fa-print fa-x5"> Print</i></button>',
          autoPrint: false,
          exportOptions: {
              columns: [ 0,1,2,3,4 ]
          }
      },
      {
          extend: 'excelHtml5',
          text: '<button class="btn btn-success btn-sm"><i class="fa fa-file-excel-o fa-x5"> Excel</i></button>',
          exportOptions: {
              columns: [ 0,1,2,3,4 ]
          }
      },
      {
          extend: 'pdfHtml5',
          text: '<button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o fa-x5"> PDF</i></button>',
          exportOptions: {
              columns:[ 0,1,2,3,4 ]
          }
      }
    ],
    "processing": true,
    "serverSide": false,
  });
}); 
</script>