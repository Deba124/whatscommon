@section('title', 'News Letter Subscriptions')
@include('admin.inc.header')
@include('admin.inc.sidebar')
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">News Letter Subscriptions</h4>
      <div class="row" style="margin-bottom: 15px;">
        <div class="col-9"></div>
        
        <div class="col-3 text-right">
          {{-- <a class="btn btn-secondary" href="{{ url('admin/add-news-letter') }}" ><i class="fa fa-plus-circle text-success"></i> Add Template</a> --}}
        </div>
      </div>
      <div class="row">
  
        <div class="col-12">
          <table id="newsletter_subcriptions-list" class="table table-condensed table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Title</th>
                <th>Date Created</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
@if($newsletter_subcriptions)
  @php
    $i = 1
  @endphp
  @foreach ($newsletter_subcriptions as $ns)
            <tr>
              <td>{{ $i++ }}</td>
              <td>{{ $ns->ns_user_email }}</td>
              <td>{{date('j F, Y',strtotime($ns->ns_created)) }}</td>
              <td></td>
            </tr>
  @endforeach
  @else
    <tr><td colspan="4" class="text-center"><b>No News Letter Subscription Available</b></td></tr>
@endif

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@include('admin.inc.footer')
<!-- content-wrapper ends -->
<script type="text/javascript">
$(document).ready(function() {  

  dataTable = $('#newsletter_subcriptions-list').DataTable( {
    dom: 'lBfrtip',
    responsive: true,
    ordering : true,
    lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
    pageLength: 10,
    buttons: [
      {
          extend: 'print',
          text: '<button class="btn btn-primary btn-sm" ><i class="fa fa-print fa-x5"> Print</i></button>',
          autoPrint: false,
          exportOptions: {
              columns: [ 0,1,2 ]
          }
      },
      {
          extend: 'excelHtml5',
          text: '<button class="btn btn-success btn-sm"><i class="fa fa-file-excel-o fa-x5"> Excel</i></button>',
          exportOptions: {
              columns: [ 0,1,2 ]
          }
      },
      {
          extend: 'pdfHtml5',
          text: '<button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o fa-x5"> PDF</i></button>',
          exportOptions: {
              columns:[ 0,1,2 ]
          }
      }
    ],
    "processing": true,
    "serverSide": false,
  });
}); 
</script>