@php
$rep = ' : All';
if(Request::get('from') && Request::get('to')){
  $rep = ' : '.date('d-m-Y', strtotime(Request::get('from'))).' - '.date('d-m-Y', strtotime(Request::get('to')));
}
@endphp
@section('title', 'Revenue Report'.$rep)
@include('admin.inc.header')
@include('admin.inc.sidebar')
<div class="content-wrapper">
  <div class="card">
    <div class="card-body text-center">
      <h4 class="card-title">Revenue Report {{ $rep }}</h4>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label>From Date</label>
            <input type="text" readonly="" id="from_date" name="from_date" placeholder="DD-MM-YYYY" value="{{ Request::get('from') ? date('d-m-Y', strtotime(Request::get('from'))) : '' }}" class="form-control no_future_date" style="text-align: center;">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>To Date</label>
            <input type="text" readonly="" id="to_date" name="to_date" placeholder="DD-MM-YYYY" value="{{ Request::get('to') ? date('d-m-Y', strtotime(Request::get('to'))) : '' }}" class="form-control no_future_date" style="text-align: center;">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <a class="btn btn-secondary" style="margin-top: 25px;" href="javascript:void();" onclick="pageReload();"><i class="fa fa-search text-success"></i> Search</a>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <a class="btn btn-secondary" style="margin-top: 25px;" href="javascript:void();" onclick="clearSearch();"><i class="fa fa-eraser text-success"></i> Clear</a>
          </div>
        </div>
      </div>
      <div class="row">
  
        <div class="col-12">
          <table id="revenue_report-list" class="table table-condensed table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Month</th>
                      <th scope="col">No. of Transactions</th>
                      <th scope="col">Donation</th>
                      <th scope="col">Premium Plan</th>{{-- <th scope="col">Subscription fees</th> --}}
                      <th scope="col">Total Amount</th>
                    </tr>
                  </thead>
      @if($revenue_arr)
                  <tbody>

        @php
          $monthly_donation = 0;
          $monthly_plans = 0;
          $total_count = 0;
          $net_total = 0;
          $final_total = 0;
        @endphp
        @foreach($revenue_arr as $i => $rev)
          @php 
            $monthly_donation += $rev['monthly_donation'];
            $monthly_plans += $rev['monthly_plans'];
            $total_count += $rev['total_count'];
            $net_total = $rev['monthly_donation'] + $rev['monthly_plans'];
            $final_total += $net_total;
          @endphp
                  <tr>
                    <td>{{ $i+1 }}</td>
                    <td>{{ date('Y-m',strtotime($rev['date'])) }}</td>
                    <td>{{ $rev['total_count'] }}</td>
                    <td>{{ sprintf("%.2f",$rev['monthly_donation']) }}</td>
                    <td>{{ sprintf("%.2f",$rev['monthly_plans']) }}</td>
                    <td>{{ sprintf("%.2f",$net_total) }}</td>
                  </tr>
          {{-- @if($rev['plan_arr'])
            @if(isset($rev['plan_arr']['monthly_plan']) && $rev['plan_arr']['monthly_plan'] > 0)
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ 'Monthly   ' }}</td>
                    <td>{{ sprintf("%.2f",$rev['plan_arr']['monthly_plan']) }}</td>
                  </tr>
            @endif

            @if(isset($rev['plan_arr']['yearly_plan']) && $rev['plan_arr']['yearly_plan'] > 0)
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ 'Yearly   ' }}</td>
                    <td>{{sprintf("%.2f",$rev['plan_arr']['yearly_plan'])}}</td>
                  </tr>
            @endif

            @if(isset($rev['plan_arr']['lifetime_plan']) && $rev['plan_arr']['lifetime_plan'] > 0)
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ 'Lifetime   ' }}</td>
                    <td>{{ sprintf("%.2f",$rev['plan_arr']['lifetime_plan']) }}</td>
                  </tr>
            @endif

          @endif --}}
        @endforeach
                  

                  </tbody>
                  <tfoot>
                    <tr>
                    <td>#</td>
                    <td><b>Total</b></td>
                    <td>{{ $total_count }}</td>
                    <td>{{ sprintf("%.2f",$monthly_donation) }}</td>
                    <td>{{ sprintf("%.2f",$monthly_plans) }}</td>
                    <td>{{ sprintf("%.2f",$final_total) }}</td>
                  </tr>
                  </tfoot>
      @endif
                </table>
        </div>
      </div>
    </div>
  </div>
</div>
@include('admin.inc.footer')
<!-- content-wrapper ends -->
<script type="text/javascript">
$(document).ready(function() {  

  dataTable = $('#revenue_report-list').DataTable( {
    dom: 'lBrtip',
    responsive: true,
    ordering : false,
    /*lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
    pageLength: 10,*/
    paging:false,
    buttons: [
      {
          extend: 'print',
          footer: true,
          text: '<button class="btn btn-primary btn-sm" ><i class="fa fa-print fa-x5"> Print</i></button>',
          autoPrint: false,
          exportOptions: {
              columns: [ 0,1,2,3,4,5 ]
          }
      },
      {
          extend: 'excelHtml5',
          footer: true,
          text: '<button class="btn btn-success btn-sm"><i class="fa fa-file-excel-o fa-x5"> Excel</i></button>',
          exportOptions: {
              columns: [ 0,1,2,3,4,5 ]
          }
      },
      {
          extend: 'pdfHtml5',
          footer: true,
          text: '<button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o fa-x5"> PDF</i></button>',
          exportOptions: {
              columns:[ 0,1,2,3,4,5 ]
          }
      }
    ],
    "processing": true,
    "serverSide": false,
  });
}); 
function pageReload(){
  /*var event_type = $('#event_type').val();
  var time = $('#time').val();*/
  var from = $('#from_date').val();
  var to = $('#to_date').val();
  var page_url = site_url('admin/reports?from='+from+'&to='+to);
  $(location).attr("href", page_url);
}
function clearSearch(){
  $('#from_date').val('');
  $('#to_date').val('');
  pageReload();
}
</script>