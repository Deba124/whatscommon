@section('title', 'Feedbacks')
@include('admin.inc.header')
@include('admin.inc.sidebar')
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Feedbacks</h4>
      <div class="row" style="margin-bottom: 15px;">
        <div class="col-9"></div>
        <meta name="_token" content="{{ csrf_token() }}">
        <div class="col-3 text-right">
          
        </div>
      </div>
      <div class="row">
  
        <div class="col-12">
          <table id="feedbacks-list" class="table table-condensed table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>User Info</th>
                <th>Feedback Info</th>
                <th>Date Created</th>
                {{-- <th>Actions</th> --}}
              </tr>
            </thead>
            <tbody>
@if($feedbacks)
  @php
    $i = 1
  @endphp
  @foreach ($feedbacks as $feedback)
            <tr>
              <td>{{ $i++ }}</td>
              <td>{{ $feedback->firstName }} {{ $feedback->lastName }}</td>
              <td>
                <b>Type : </b> {{ $feedback->feed_type }} <br>
                <b>Stars : </b> {{ $feedback->stars }} <br>
                <b>Comment : </b> {{ $feedback->comment }}
              </td>
              <td>
                {{ format_date($feedback->updated_at) }}
              </td>
            </tr>
  @endforeach
  @else
    <tr><td colspan="4" class="text-center"><b>No Feedbacks Available</b></td></tr>
@endif

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@include('admin.inc.footer')
<!-- content-wrapper ends -->
<script type="text/javascript">
$(document).ready(function() {  

  dataTable = $('#feedbacks-list').DataTable( {
    dom: 'lBfrtip',
    responsive: true,
    ordering : true,
    lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
    pageLength: 10,
    buttons: [
      {
          extend: 'print',
          text: '<button class="btn btn-primary btn-sm" ><i class="fa fa-print fa-x5"> Print</i></button>',
          autoPrint: false,
          exportOptions: {
              columns: [ 0,1,2,3 ]
          }
      },
      {
          extend: 'excelHtml5',
          text: '<button class="btn btn-success btn-sm"><i class="fa fa-file-excel-o fa-x5"> Excel</i></button>',
          exportOptions: {
              columns: [ 0,1,2,3 ]
          }
      },
      {
          extend: 'pdfHtml5',
          text: '<button class="btn btn-danger btn-sm"><i class="fa fa-file-pdf-o fa-x5"> PDF</i></button>',
          exportOptions: {
              columns:[ 0,1,2,3 ]
          }
      }
    ],
    "processing": true,
    "serverSide": false,
  });
}); 
</script>