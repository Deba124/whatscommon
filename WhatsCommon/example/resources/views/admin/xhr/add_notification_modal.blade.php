 <!-- Modal starts -->       
<div class="modal fade" id="add-notification-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Add Notification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="add-notification">
          <div class="form-group" id="submit_status"></div>
          @csrf
          <div class="form-group">
            <label>Type <span class="text-danger">*</span></label>
            <select class="form-control" name="notification_type">
              <option value="">Select Type</option>
              <option value="connection">Connection</option>
              <option value="match">Match</option>
              <option value="test">Test</option>
            </select>
            <p class="text-danger" id="notification_type_error"></p>
          </div>
          <div class="form-group">
            <label>User <span class="text-danger">*</span></label>
            <select class="form-control multiple" name="device_id[]" multiple="" data-live-search="true" style="width:100%!important">
              <option value="" hidden="">Select User</option>
@if($users)

  @foreach ($users as $user)
              <option value="{{$user->_device_id}}">{{$user->firstName}} {{$user->lastName}} ({{ ucwords($user->device_type) }})</option>
  @endforeach
@endif
            </select>
            <p class="text-danger" id="device_id_error"></p>
          </div>
          <div class="form-group">
            <label>Title <span class="text-danger">*</span></label>
            <input type="text" name="notification_title" class="form-control">
            <p class="text-danger" id="notification_title_error"></p>
          </div>
          <div class="form-group">
            <label>Content <span class="text-danger">*</span></label>
            <textarea class="form-control" name="notification_content"></textarea>
            <p class="text-danger" id="notification_content_error"></p>
          </div>
          
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->