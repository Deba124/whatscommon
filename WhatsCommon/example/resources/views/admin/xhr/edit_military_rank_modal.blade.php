 <!-- Modal starts -->       
<div class="modal fade" id="edit-military-rank-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Update Military Rank</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="edit-military-rank">
          <div class="form-group" id="submit_status"></div>
          @csrf
          <input type="hidden" name="_rank_id" value="{{ $rank_data->_rank_id }}">
          <div class="form-group">
            <label>Military Branch <span class="text-danger">*</span></label>
            <select class="form-control" name="rank_branch_id">
              <option value="" hidden="">Select Military Branch</option>
@if($military_branches)

  @foreach ($military_branches as $branch)
              <option value="{{$branch->_mbranch_id}}" @if($branch->_mbranch_id==$rank_data->rank_branch_id) {{ 'selected' }} @endif>{{$branch->mbranch_name}}</option>
  @endforeach
@endif
            </select>
            <p class="text-danger" id="rank_branch_id_error"></p>
          </div>
          <div class="form-group">
            <label>Rank Name <span class="text-danger">*</span></label>
            <input type="text" name="rank_name" class="form-control" value="{{ $rank_data->rank_name }}">
            <p class="text-danger" id="rank_name_error"></p>
          </div>
          
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->