<div class="text-center" id="line2">
  <canvas  height="300" width="450" ></canvas>
</div>
@php
  if($event_type=='all'){
    $life_event_name = 'All Life Events';
  }else{
    if($event_type=='1'){
      $life_event_name = 'Personal';
    }
    if($event_type=='2'){
      $life_event_name = 'Dating';
    }
    if($event_type=='3'){
      $life_event_name = 'Adoption';
    }
    if($event_type=='4'){
      $life_event_name = 'Travel';
    }
    if($event_type=='5'){
      $life_event_name = 'Military';
    }
    if($event_type=='6'){
      $life_event_name = 'Education';
    }
    if($event_type=='7'){
      $life_event_name = 'Work';
    }
    if($event_type=='8'){
      $life_event_name = 'Pets';
    }
    if($event_type=='9'){
      $life_event_name = 'Lost & Found';
    }
    if($event_type=='10'){
      $life_event_name = 'Doppelganger';
    }
    if($event_type=='11'){
      $life_event_name = 'Username';
    }
    if($event_type=='12'){
      $life_event_name = 'Activity';
    }
  }
@endphp
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['line']});
  google.charts.setOnLoadCallback(drawChart2);

  function drawChart2() {

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Day');
    data.addColumn('number', '{{ $life_event_name }}');

    data.addRows([
@if($graph_counts)
  @foreach($graph_counts as $graph)

@php
  if($event_type=='all'){
    $count = $graph["event_count"];
  }else if(isset($graph['check_count'])){
    $count = $graph["check_count"];
  }else{
    $count = 0;
  }

@endphp

      ['{{ date("d/m",strtotime($graph["date"])) }}', {{ $count }}],
  @endforeach
@endif
    ]);
    var options = {
      chart: {
        title: 'Last 10 Days Life Events',
        subtitle: ''
      },
      axes: {
        x: {
          0: {side: 'top'}
        }
      },
    };

    var chart = new google.charts.Line(document.getElementById('line2'));

    chart.draw(data, google.charts.Line.convertOptions(options));
  }

</script>