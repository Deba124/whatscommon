 <!-- Modal starts -->       
<div class="modal fade" id="change-password-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Change Password</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="change-password">
          <div class="form-group" id="submit_status"></div>
          @csrf
          <div class="form-group">
            <label>Old Password <span class="text-danger">*</span></label>
            <input type="password" name="old_password" class="form-control" value="">
            <p class="text-danger" id="old_password_error"></p>
          </div>
          <div class="form-group">
            <label>New Password <span class="text-danger">*</span></label>
            <input type="password" class="form-control" name="new_password">
            <p class="text-danger" id="new_password_error"></p>
          </div>
          <div class="form-group">
            <label>Confirm Password <span class="text-danger">*</span></label>
            <input type="password" class="form-control" name="cnf_password">
            <p class="text-danger" id="cnf_password_error"></p>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->