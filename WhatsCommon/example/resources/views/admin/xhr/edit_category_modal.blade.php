 <!-- Modal starts -->       
<div class="modal fade" id="edit-category-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Update Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="edit-category">
          <div class="form-group" id="submit_status"></div>
          @csrf
          <input type="hidden" name="_occupation_id" value="{{ $relation_data->_occupation_id }}">
          <div class="form-group">
            <label>Category Name<span class="text-danger">*</span></label>
            <input type="text" name="occupation_name" class="form-control" value="{{ $relation_data->occupation_name }}">
            <p class="text-danger" id="occupation_name_error"></p>
          </div>
          
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->