 <!-- Modal starts -->       
<div class="modal fade" id="edit-game-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content" style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Update Game</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="edit-game">
          <div class="form-group" id="submit_status"></div>
          @csrf
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <input type="hidden" name="_game_id" value="{{ $game_data->_game_id }}">
                <label>Name <span class="text-danger">*</span></label>
                <input type="text" name="game_name" class="form-control" value="{{ $game_data->game_name }}">
                <p class="text-danger" id="game_name_error"></p>
              </div>
            </div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->