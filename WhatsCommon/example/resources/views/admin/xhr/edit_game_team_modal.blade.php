 <!-- Modal starts -->       
<div class="modal fade" id="edit-game-team-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content" style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Update Team/Group</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="edit-game-team">
          <div class="form-group" id="submit_status"></div>
          @csrf
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <input type="hidden" name="_team_id" value="{{ $team_data->_team_id }}">
                <label>Game/Activity <span class="text-danger">*</span></label>
                <select class="form-control" name="team_gameid">
                  <option value="" hidden="">Select Game/Activity</option>
            @if($games)
              @foreach($games as $game)
                @php
                  $selected = $game->_game_id==$team_data->team_gameid ? 'selected' : '';
                @endphp
                  <option value="{{ $game->_game_id }}" {{ $selected }}>{{ $game->game_name }}</option>
              @endforeach
            @endif

                </select>
                <p class="text-danger" id="game_name_error"></p>
              </div>
              <div class="form-group">
                <label>Team/Group Name <span class="text-danger">*</span></label>
                <input type="text" name="team_name" class="form-control" value="{{ $team_data->team_name }}">
                <p class="text-danger" id="team_name_error"></p>
              </div>
            </div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->