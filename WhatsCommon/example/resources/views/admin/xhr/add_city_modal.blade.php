 <!-- Modal starts -->       
<div class="modal fade" id="add-city-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="margin-top: -10%;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel-3">Add City</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal admin_form" id="add-city">
          <div class="form-group" id="submit_status"></div>
          @csrf
          <div class="form-group">
            <label>Province <span class="text-danger">*</span></label>
            <select class="form-control" name="city_province">
              <option value="" hidden="">Select Province</option>
@if($provinces)

  @foreach ($provinces as $province)
              <option value="{{$province->_province_id}}">{{$province->province_name}} ({{$province->country_name}})</option>
  @endforeach
@endif
            </select>
            <p class="text-danger" id="city_province_error"></p>
          </div>
          <div class="form-group">
            <label>City <span class="text-danger">*</span></label>
            <input type="text" name="city_name" class="form-control">
            <p class="text-danger" id="city_name_error"></p>
          </div>
          
          <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->