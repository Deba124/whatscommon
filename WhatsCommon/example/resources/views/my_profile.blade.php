@include('inc.header')
<style type="text/css">
.profileTop .connectionsDetailsInfoEvents span {
  font-weight: 700;
  font-size: 16px;
}
</style>
<div class="innerBodyOnly ">
  	<div class="container-fluid windowHeightLeft" style="overflow-y: auto;">
  		<div class="row ">
  			<div class="@if(isset($user_data->user_is_premium) && $user_data->user_is_premium==1) {{ 'col-md-12' }} @else {{ 'col-md-8' }}@endif">
  				<div class="card">
  					<div class="card-body">
	      				<div class="profileTop" style="padding: 10px;">
	                    	<div class="connectionsAllDetailsTop">
	                            <div class="connectionsDetailsAvtarImg">
	                              <img src="{{Session::get('userdata')['profile_photo_path'] ? Session::get('userdata')['profile_photo_path'] : url('new-design/img/profile_placeholder.jpg')}}" class="img-fluid" alt="" id="profile_img">
	                              
	                            </div>

	                            <div class="connectionsDetailsInfo">
	                              <div class="connectionsDetailsInfoName">{{Session::get('userdata')['firstName']}} {{Session::get('userdata')['lastName']}} </div>
	                              @php
	                              	$background = url('new-design/img/connectionsDetailsInfoId.png');
	                              @endphp
	                              
	                              <div class="connectionsDetailsInfoEvents myEvenntsAll">
			                        <div class="myEvennts">
                                <a href="{{ url('connections') }}">
			                           <span class="eventsNumbers">{{Session::get('userdata')['counts'][0]->connection_count}}</span> <span class="eventsText">Connections</span>
                                </a>
			                        </div>
			                        <div>
			                          <span class="eventsNumbers">{{Session::get('userdata')['counts'][0]->life_events_count}}</span> <span class="eventsText">Life Events</span>
			                        </div>
			                        <div>
			                          <span class="eventsNumbers">{{Session::get('userdata')['counts'][0]->matched_count}}</span> <span class="eventsText">Matches</span>
			                        </div>
			                      </div>
			                      <div class="connectionsDetailsInfoEvents" style="background: url('{{ $background  }}') no-repeat left center; padding-left: 25px;">
	                                <span class="eventsText">{{ '@' }}{{Session::get('userdata')['username']}} </span>
	                              </div>
			                      <div class="connectionsDetailsPlace">{{ Session::get('userdata')['city_name'] }} {{ (Session::get('userdata')['city_name'] && Session::get('userdata')['province_name']) ? ',' : '' }} {{ Session::get('userdata')['province_name'] }} {{ (Session::get('userdata')['country_name'] && Session::get('userdata')['province_name']) ? ',' : '' }} {{ Session::get('userdata')['country_name'] }}</div>
                            @if(Session::get('userdata')['bio'])<div class="connectionsDetailsPlace connectionsDetailsUser">{{ Session::get('userdata')['bio'] }}</div>@endif
			                      <a class="btn btn-following" href="{{ url('settings') }}">Edit Profile</a>
	                            </div>
                            @if(Session::get('userdata')['created_at'])<div class="memberJoin">Member since {{ format_date(Session::get('userdata')['created_at']) }}</div>@endif
	                        </div>
	                    </div>
                	</div>
            	</div>
            	<div class="form-group text-left">
            		<h5 style="color: #b8b8b8; padding: 30px 0px;margin-bottom: -40px;">PERSONAL FEED</h5>
            	</div>
            	<div class="card">
            		<div class="card-body my-profile-feedlist" style="overflow-y: auto;">
        @if($feeds)
              @foreach ($feeds as $feed)
                    <div class="globalFeedList" style="padding: 25px 10px;border-bottom: 3px solid #f8f8f8;">

                      <div class="connectedInfo">
                        <div class="position-relative pr-0 feedImg">
                          <div class=""><img src="{{ $feed->user1_profile_photo ? $feed->user1_profile_photo : 'new-design/img/profile_placeholder.jpg' }}" class="img-fluid" alt=""></div>
                          <div class=""><img src="{{ $feed->user2_profile_photo ? $feed->user2_profile_photo : 'new-design/img/profile_placeholder.jpg' }}" class="img-fluid" alt=""></div>
                          <img src="new-design/img/feedadd.png" class="feedadd" alt="">
                        </div>
                        <div class="rightPanText">
                        	<p>
                            @if($feed->user1_id==Session::get('userdata')['id'])
                            <a href="{{ url('my-profile') }}"> 
                              <i>You </i> 
                            </a> are {{ $feed->feed_type }} with <a href="{{ url('user-profile/?feed='.$feed->_feed_id.'&user='.$feed->user2_id) }}"> 
                              <i>{{ $feed->user2_firstName }} {{ $feed->user2_lastName }} </i> 
                            </a>

                            @else
                            <a href="{{ url('user-profile/?feed='.$feed->_feed_id.'&user='.$feed->user1_id) }}"> 
                              <i>{{ $feed->user1_firstName }} {{ $feed->user1_lastName }} </i> 
                            </a> is {{ $feed->feed_type }} with <a href="{{ url('my-profile') }}"> 
                              <i>You </i> 
                            </a>

                            @endif
	                          	
                          	</p>
                          <span>About {{ DateTime_Diff($feed->feed_created) }} ago</span>
                        </div>
                      </div>
                    	<div class="row">
                    		<div class="col-md-6">
                    			<div class="feedInfo position-relative">
		                            @if($feed->feed_what)
		                            <p>
		                                <span>What:</span> 
		                                <span style="color: #91d639;">{{$feed->feed_event_type}}</span>
		                            </p>
		                            <p class="pl-5"> {{ $feed->feed_what }}</p>
		                            @endif
		                            @if($feed->feed_where)<p><span>Where:  </span> {{ $feed->feed_where }}</p>@endif
		                            @if($feed->feed_when)<p><span>When: </span> {{ $feed->feed_when }}</p>@endif
		                            @if($feed->feed_keywords)<p><span>W5: </span> {{ $feed->feed_keywords }}</p>@endif
		                            <div class="greenCountFeed">{{ $feed->feed_match_count }}</div>
		                        </div>
                    		</div>
                    		<div class="col-md-6">
                @if($feed->feed_images && count($feed->feed_images)>0)

                @php
                    $counter = count($feed->feed_images);
                    $remaining = 0;
                    if($counter>3){
                      $remaining = $counter-2;
                    }
                @endphp
                        <div class="uploadPhotoHeading">Uploaded photos:</div>
                        <div class="galleryViewAll">
                                  @if($counter==1)
                                    <div class="galleryViewImg1">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-12 galleryViewCol">
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto03.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[0]->fi_img_name }}" href="{{ $feed->feed_images[0]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[0]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  @elseif($counter==2)  
                                    <div class="galleryViewImg2">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto03.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[0]->fi_img_name }}" href="{{ $feed->feed_images[0]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[0]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto01.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[1]->fi_img_name }}" href="{{ $feed->feed_images[1]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[1]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  @elseif($counter==3)     
                                    <div class="galleryViewImg3">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              {{-- <img src="img/feedfoto03.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[0]->fi_img_name }}" href="{{ $feed->feed_images[0]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[0]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto02.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[1]->fi_img_name }}" href="{{ $feed->feed_images[1]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[1]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto01.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[2]->fi_img_name }}" href="{{ $feed->feed_images[2]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[2]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  @else
                                    <div class="galleryViewImg3 galleryViewImg4">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              {{-- <img src="img/feedfoto03.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[0]->fi_img_name }}" href="{{ $feed->feed_images[0]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[0]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              {{-- <img src="img/feedfoto02.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[1]->fi_img_name }}" href="{{ $feed->feed_images[1]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[1]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                          <div class="galleryViewImg">
                                            <img src="new-design/img/feedfoto01.png" class="img-fluid" alt="">
                                            <div class="galleryViewImgMore">{{ $remaining }}+</div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  @endif
                                  </div>

                        {{-- <div class="feedPhoto">
                    @foreach($feed->feed_images as $img)
                      
                          <a data-magnify="gallery" data-caption="{{ $img->fi_img_name }}" href="{{ $img->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                            <img src="{{ $img->fi_img_url }}" class="img-fluid" width="100%">
                          </a>
                      
                    @endforeach
                            
                        </div> --}}
                @endif
                        
                        </div>
                        <div class="col-md-12">
                        <div class="row text-center rightPanBtn" style="display: block;">
                            <a href="#" class="dismissButton" onclick="execute('api/hide-feed','user_id={{ Session::get('userdata')['id'] }}&_feed_id={{ $feed->_feed_id }}&request_from=my-profile');"><img src="new-design/img/dismissIcon.png" class="img-fluid" alt=""> Dismiss</a>
                @if($feed->is_connected==1)
                    @php
                      $receiver_id = $user_quick_blox_id == $feed->user1_quick_blox_id ? $feed->user2_quick_blox_id : $feed->user1_quick_blox_id;
                    @endphp
                            {{-- <a href="{{url('messages?qb='.$receiver_id)}}" class="messageButton"><img src="new-design/img/message-blue.png" class="img-fluid" alt="">
                              Message</a> --}}
                            <a href="javascript:void;" onclick="open_modal('share-feed','feed_id={{$feed->_feed_id}}')" class="dismissButton" style="display: inline;"><img src="new-design/img/shares_ic2.png" 
                              style="height: 15px;margin-top: -3px;" class="img-fluid" alt=""> Share</a>
                @endif
                            <!-- <a href="#" class="tagstarButton"> Tagster Social</a> -->
                        </div>
                      </div>
                    	</div>
                        <div class="row">
                    		

                    	</div>

                        <!-- <img src="new-design/img/mapfeed.png" class="img-fluid" alt="" width="100%"> -->
                  

                    </div>
              @endforeach
            @endif
            		</div>
            	</div>
  			</div>
      @if(isset($user_data->user_is_premium) && $user_data->user_is_premium==0)
  			<div class="col-md-4">
  				<div class="card">
  					<div class="card-body">
  						<div class="form-group text-center">
	                        <img src="new-design/img/premiumImg.png" style="max-height: 170px;" class="img-fluid" alt="">
  						</div>
  						<div class="feedbackRight wcPremium">
                          <div class="premiumHeader" style="padding: 15px 30px 15px;">
                            <div class="feedbackTitle" style="font-size: 18px;"><span>WC Premium</span></div>
                          </div>

                          <div class="premiumBody">
                            <ul class="list-unstyled mb-0 premiumFeatures">
                              <li>Everyone Is Looking For Someone</li>
                              <li>Find Anyone or Anything... <br>Anywhere at Anytime</li>
                              <li>End to End Encryption</li>
                              <li>Safe and Secure</li>
                              <li>Unlimited Interaction</li>
                              <li>NO Pop-UPs and NO Ads</li>
                              <li>Connect Globaly</li>
                              <li>ONLY site of its kind</li>
                              <li>Cross App Interactions</li>
                            </ul>
                            <div class="btn-Edit-save btn-feedback btn-premium">
                              <a href="{{ url('premium-plans') }}" class="btn btn-Edit btn-save openPlan" style="padding-top: 15px;">WC Premium </a>
                            </div>
                          </div>
                        </div>
  					</div>
  				</div>
  			</div>
      @endif
  		</div>
	</div>
</div>
@include('inc.footer')

<script type="text/javascript">
$(document).ready(function(){
    //section height depends on screen resolution    
    $('.my-profile-feedlist').css('max-height',$(window).height()-125);
});
</script>