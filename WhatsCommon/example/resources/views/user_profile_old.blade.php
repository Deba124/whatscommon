@include('inc.header')
<div class="innerBodyOnly">
  <div class="container-fluid">
    <div class="connectionsBody">
      <div class="position-relative connectionFlip userProfileFlip">
        <div class="leftSlidePan closePan">
         
        </div>
        <div class="windowHeight windowHeightMid">
          <div class="connectionsRight">
            <div class="tab-content">
              <div id="connection{{ $connection->id }}" class="tab-pane active">
                
                <div class="connectionsAllDetails">
                  <div class="connectionsAllDetailsTop">                  
                    <div class="connectionsDetailsAvtarImg">
                      <img src="{{ $connection->profile_photo_path ? $connection->profile_photo_path : 'new-design/img/profile_placeholder.jpg' }}" class="img-fluid" alt="">
                    </div>
                    <div class="connectionsDetailsInfo">
                      <div class="connectionsDetailsInfoName">{{ $connection->firstName }} {{ $connection->lastName }}</div>
                      <div class="connectionsDetailsInfoEvents myEvenntsAll">
                        <div class="myEvennts">
                          <span class="eventsNumbers">{{ $connection->connection_count }}</span> <span class="eventsText">Connections</span>
                        </div>
                        <div>
                          <span class="eventsNumbers">{{ $connection->life_events_count }}</span> <span class="eventsText">Life Events</span>
                        </div>
                        <div>
                          <span class="eventsNumbers">{{ $connection->matched_count }}</span> <span class="eventsText">Event Matches</span>
                        </div>
                      </div>
                      <div class="connectionsDetailsInfoId">{{'@'}}{{ $connection->username }}</div>
                      <div class="connectionsDetailsPlace">{{ $connection->city_name }} {{ ($connection->city_name && $connection->province_name) ? ',' : '' }} {{ $connection->province_name }} {{ ($connection->country_name && $connection->province_name) ? ',' : '' }} {{ $connection->country_name }}</div>
                      @if($connection->is_connected==0 && $connection->is_pending_connection==0 && $feed_data)
                      <a class="btn btn-following" onclick="execute('api/connection-request','user_id={{$my_id}}&connect_user_id={{$connection->id}}&_feed_id={{$feed_data->_feed_id}}')">OK to Connect</a>
                      @elseif($connection->is_pending_connection==1 && $connection->request_from_user==$my_id && $connection->_request_id)
                      <a class="btn btn-following" onclick="execute('api/cancel-request','user_id={{$my_id}}&_request_id={{$connection->_request_id}}')">Cancel Connect</a>
                      @elseif($connection->is_pending_connection==1 && $connection->request_from_user!=$my_id && $connection->_request_id)
                      <a class="btn btn-following" onclick="execute('api/accept-reject-request','user_id={{$my_id}}&_request_id={{$connection->_request_id}}&request_status=1')">Accept Connect</a>
                      @endif
                      <a class="btn btn-following" onclick="execute('api/follow-unfollow','user_id={{$my_id}}&followed_user={{$connection->id}}')">{{ $connection->is_following==1 ? 'Following' : 'Follow' }}</a>
                      @if($connection->quick_blox_id)
                      <a class="btn btn-following btn-message" href="{{ url('messages?qb='.$connection->quick_blox_id) }}">Message</a>
                      @endif
                    </div>
                  </div>
                @if($connection->events_type_names)
                  <div class="connectionsAllDetailsBody">
                    <div class="connectionsDetailsTitle">Your {{ $connection->is_connected==1 ? 'Connections' : 'Matches' }}  in Common</div>
                    @php
                      $events_type_names = explode(",",$connection->events_type_names);
                    @endphp
                    @if($events_type_names)
                    <div>
                      @foreach($events_type_names as $name)
                        <div class="connectionsDetailsBtn btn">{{ $name }}</div>
                      @endforeach
                    </div>
                    @endif
                  </div>
                 @endif
                @if($feed_data)
                  @if($feed_data->feed_what)
                  <div class="connectionsAllDetailsBody">
                    <div class="connectionsDetailsTitle">What</div>
                    @php
                      $what_names = explode(",",$feed_data->feed_what);
                    @endphp
                    @if($what_names)
                    <div>
                      @foreach($what_names as $name)
                        <div class="connectionsDetailsBtn btn">{{ $name }}</div>
                      @endforeach
                    </div>
                    @endif
                  </div>
                  @endif
                  @if($feed_data->feed_where)
                  <div class="connectionsAllDetailsBody">
                    <div class="connectionsDetailsTitle">Where</div>
                    @php
                      $where_names = explode(",",$feed_data->feed_where);
                    @endphp
                    @if($where_names)
                    <div>
                      @foreach($where_names as $name)
                        <div class="connectionsDetailsBtn btn">{{ $name }}</div>
                      @endforeach
                    </div>
                    @endif
                  </div>
                  @endif
                  
                  @if($feed_data->feed_keywords)
                  <div class="connectionsAllDetailsBody">
                    <div class="connectionsDetailsTitle">Keywords</div>
                    @php
                      $keywords_names = explode(",",$feed_data->feed_keywords);
                    @endphp
                    @if($keywords_names)
                    <div>
                      @foreach($keywords_names as $name)
                        <div class="connectionsDetailsBtn btn">{{ $name }}</div>
                      @endforeach
                    </div>
                    @endif
                  </div>
                  @endif
                @endif
                </div>
              </div>
              
              
            </div>    
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('inc.footer')
<script type="text/javascript">
    $(document).ready(function(){    
        if ($(window).width() < 951) {
        $('.windowHeightLeft').css('height',$(window).height()-153);
        $('.windowHeightMid').css('height',$(window).height()-90);
      }
      else {
        $('.windowHeightLeft').css('height',$(window).height()-150);
        $('.windowHeightMid').css('height',$(window).height()-150);
      }
    });
</script>