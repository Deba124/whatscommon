@include('inc.header')
<div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                <div class="leftSlidePan {{ Request::get('menu')==1 ? '' : 'closePan'}}">
                  <a href="#" class="panelslideOpenButton {{ Request::get('menu')==1 ? 'panelslideCloseButton' : ''}}"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                  <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                  	<div class="settingLeftTitle">Settings</div>
                  	<ul class="settingMenu mb-0 list-unstyled">
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="{{ url('settings') }}"><img src="new-design/img/profileIcon.png" class="img-fluid menuIcon" alt="">Profile</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="{{ url('change-password') }}"><img src="new-design/img/change-password.png" class="img-fluid menuIcon" alt="">Change Password</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu settingMenuActive" href="{{ url('security') }}"><img src="new-design/img/security-a.png" class="img-fluid menuIcon" alt="">Security and Privacy</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="{{ url('blocked-users') }}"><img src="new-design/img/blocking.png" class="img-fluid menuIcon" alt="">Blocking</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="{{ url('notification-settings') }}"><img src="new-design/img/notifications.png" class="img-fluid menuIcon" alt="">Notifications</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="{{ url('link-accounts') }}"><img src="new-design/img/link-account.png" class="img-fluid menuIcon" alt="">Link Account</a>
                  		</li>
                  		<li class="settingMenuLi">
                  			<a class="settingMenu" href="{{ url('message-settings') }}"><img src="new-design/img/message-setting.png" class="img-fluid menuIcon" alt="">Message Settings</a>
                  		</li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="{{ url('feed-settings') }}"><img src="new-design/img/global-feed.png" class="img-fluid menuIcon" alt="">Feed Settings</a>
                      </li>
                  	</ul>
                  </div>
                </div>
                <div class="midPan">
                  <div class="connectionsBody windowHeight windowHeightMid securityBg">
                    <div class="bankcardTab">

                      <div class="">
                        <div id="" class="">
                        	<form method="post" class="xhr_form" action="set-security-setting" id="set-security-setting">
                            @csrf
                          <div class="profileForm bankForm">
                            <!-- <div class="linkAccountTitle">Options</div>
                            <ul class="linkAccountList">
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        Use Two-Authentication
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" @if($settings && $settings->two_authentication==1) checked @endif name="two_authentication" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                            </ul> -->

                            <div class="linkAccountTitle">Sign-In</div>
                            <ul class="linkAccountList">
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        With Touch ID
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" @if($settings && $settings->sign_touch_id==1) checked @endif name="sign_touch_id" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        With Face ID
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" @if($settings && $settings->sign_face_id==1) checked @endif name="sign_face_id" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                            </ul>

                            <div class="linkAccountTitle">Privacy</div>
                            <div class="feedbackTitle2 text-left">By turning on the toggle buttons, you’re allowing to show specific information on your profile and allowing people to see the information.</div>

                            <ul class="linkAccountList">
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        Email
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" @if($settings && $settings->privacy_email==1) checked @endif name="privacy_email" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        Phone Number
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" @if($settings && $settings->privacy_phone==1) checked @endif name="privacy_phone" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        Birthday
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" @if($settings && $settings->privacy_birthday==1) checked @endif name="privacy_birthday" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        Connections List
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" @if($settings && $settings->show_connection_list==1) checked @endif name="show_connection_list" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        Match Feed
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" @if($settings && $settings->show_match_feed==1) checked @endif name="show_match_feed" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        Connection Feed
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" @if($settings && $settings->show_connection_feed==1) checked @endif name="show_connection_feed" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li>
                                {{-- <li>
                                    <span style="width: calc(100% - 48px);">
                                        Location Sharing
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" @if($settings && $settings->privacy_location==1) checked @endif name="privacy_location" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li> --}}
                                {{-- <li>
                                    <span style="width: calc(100% - 48px);">
                                        Share Connections Globally
                                    </span>
                                    <label class="switch">
                                        <input type="checkbox" @if($settings && $settings->privacy_share_connection==1) checked @endif name="privacy_share_connection" class="settings" value="1">
                                        <span class="slider round"></span>
                                    </label>
                                </li> --}}
                                <li>
                                    <span style="width: calc(100% - 48px);">
                                        Delete Account
                                    </span>
                                    {{-- <label class="switch">
                                      <input type="checkbox" id="delete_account" name="delete_account" class="" value="1">
                                      <span class="slider round"></span>
                                    </label> --}}
                                    <button type="button" style="width: 100px;" class="btn btn-danger pull-right" onclick="open_modal('delete-user');">Delete</button>
                                </li>
                            </ul>
                          </div>
                        </form>
                        </div>
                        
                      </div>

                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@include('inc.footer')
<script type="text/javascript">
$(document).on('change','.settings', function(){
	$('.xhr_form').submit();
});

/*$(document).on('change' ,'#delete_account', function(){
  if($(this).prop('checked')==true){
    open_modal('delete-user');
    //$(this).prop('checked',false);
  }
});*/
</script>