@include('inc.header')
<div class="innerBodyOnly">
  <div class="container-fluid">
    <div class="settingBody">
      <div class="position-relative settingFlip">
        <div class="leftSlidePan {{ Request::get('menu')==1 ? '' : 'closePan'}}">
          <a href="#" class="panelslideOpenButton {{ Request::get('menu')==1 ? 'panelslideCloseButton' : ''}}"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
          <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
          	<div class="settingLeftTitle">Settings</div>
          	<ul class="settingMenu mb-0 list-unstyled">
          		<li class="settingMenuLi">
          			<a class="settingMenu" href="{{ url('settings') }}"><img src="new-design/img/profileIcon.png" class="img-fluid menuIcon" alt="">Profile</a>
          		</li>
          		
          		<li class="settingMenuLi">
          			<a class="settingMenu" href="{{ url('change-password') }}"><img src="new-design/img/change-password.png" class="img-fluid menuIcon" alt="">Change Password</a>
          		</li>
          		<li class="settingMenuLi">
          			<a class="settingMenu " href="{{ url('security') }}"><img src="new-design/img/security.png" class="img-fluid menuIcon" alt="">Security and Privacy</a>
          		</li>
          		<li class="settingMenuLi">
          			<a class="settingMenu" href="{{ url('blocked-users') }}"><img src="new-design/img/blocking.png" class="img-fluid menuIcon" alt="">Blocking</a>
          		</li>
          		<li class="settingMenuLi">
          			<a class="settingMenu" href="{{ url('notification-settings') }}"><img src="new-design/img/notifications.png" class="img-fluid menuIcon" alt="">Notifications</a>
          		</li>
          		<li class="settingMenuLi">
          			<a class="settingMenu" href="{{ url('link-accounts') }}"><img src="new-design/img/link-account.png" class="img-fluid menuIcon" alt="">Link Account</a>
          		</li>
          		<li class="settingMenuLi">
          			<a class="settingMenu" href="{{ url('message-settings') }}"><img src="new-design/img/message-setting.png" class="img-fluid menuIcon" alt="">Message Settings</a>
          		</li>
              <li class="settingMenuLi">
                <a class="settingMenu settingMenuActive" href="{{ url('feed-settings') }}"><img src="new-design/img/global-feed-a.png" class="img-fluid menuIcon" alt="">Feed Settings</a>
              </li>
          	</ul>
          </div>
        </div>
        <div class="midPan">
          <div class="connectionsBody windowHeight windowHeightMid securityBg linkAccNoti">
            <div class="bankcardTab">

              <div class="">
                <div id="" class="">
                	<form method="post" class="xhr_form" action="set-feed-setting" id="set-feed-setting">
                    @csrf
                    <div class="profileForm bankForm">
                      <div class="linkAccountTitle">Life Event Categories:</div>
                      <div class="feedbackTitle2 text-left">Please toggle on (push to the right)  if you would like to share your connections globally or NO (push to the left) to keep your results private.</div>
                    
                      <ul class="linkAccountList eventoptions">
                        <li>
                          <span style="width: calc(100% - 48px);">
                            <img src="new-design/img/lifeEventPersonal.png" class="img-fluid" alt="">Personal</span>
                          <label class="switch">
                            <input type="checkbox" @if($settings && $settings->show_personal_feed==1) checked @endif name="show_personal_feed" class="settings" value="1">
                            <span class="slider round"></span>
                          </label>
                        </li>
                        <li>
                          <span style="width: calc(100% - 48px);">
                          <img src="new-design/img/lifeEventDating.png" class="img-fluid" alt="">Dating</span>
                          <label class="switch">
                            <input type="checkbox" @if($settings && $settings->show_dating_feed==1) checked @endif name="show_dating_feed" class="settings" value="1">
                            <span class="slider round"></span>
                          </label>
                        </li>
                        <li>
                          <span style="width: calc(100% - 48px);">
                          <img src="new-design/img/lifeEventAdoption.png" class="img-fluid" alt="">Adoption/Foster</span>
                          <label class="switch">
                            <input type="checkbox" @if($settings && $settings->show_adoption_feed==1) checked @endif name="show_adoption_feed" class="settings" value="1">
                            <span class="slider round"></span>
                          </label>
                        </li>
                        <li>
                          <span style="width: calc(100% - 48px);"><img src="new-design/img/lifeEventTravel.png" class="img-fluid" alt="">
                                        Travel/Vacation</span>
                          <label class="switch">
                            <input type="checkbox" @if($settings && $settings->show_travel_feed==1) checked @endif name="show_travel_feed" class="settings" value="1">
                            <span class="slider round"></span>
                          </label>
                        </li>
                        <li>
                          <span style="width: calc(100% - 48px);"><img src="new-design/img/lifeEventMilitary.png" class="img-fluid" alt="">
                                        Military</span>
                          <label class="switch">
                            <input type="checkbox" @if($settings && $settings->show_military_feed==1) checked @endif name="show_military_feed" class="settings" value="1">
                            <span class="slider round"></span>
                          </label>
                        </li>
                        <li>
                          <span style="width: calc(100% - 48px);"><img src="new-design/img/lifeEventEducation.png" class="img-fluid" alt="">
                                        School/Education</span>
                          <label class="switch">
                            <input type="checkbox" @if($settings && $settings->show_education_feed==1) checked @endif name="show_education_feed" class="settings" value="1">
                            <span class="slider round"></span>
                          </label>
                        </li>
                        <li>
                          <span style="width: calc(100% - 48px);"><img src="new-design/img/lifeEventWork.png" class="img-fluid" alt="">
                                        Careers/Job/Works</span>
                          <label class="switch">
                            <input type="checkbox" @if($settings && $settings->show_career_feed==1) checked @endif name="show_career_feed" class="settings" value="1">
                            <span class="slider round"></span>
                          </label>
                        </li>
                        <li>
                          <span style="width: calc(100% - 48px);"><img src="new-design/img/lifeEventPets.png" class="img-fluid" alt="">
                                        Pets</span>
                          <label class="switch">
                            <input type="checkbox" @if($settings && $settings->show_pets_feed==1) checked @endif name="show_pets_feed" class="settings" value="1">
                            <span class="slider round"></span>
                          </label>
                        </li>
                        <li>
                          <span style="width: calc(100% - 48px);"><img src="new-design/img/lifeEventL_F.png" class="img-fluid" alt="">
                                        Lost/Found</span>
                          <label class="switch">
                            <input type="checkbox" @if($settings && $settings->show_lostfound_feed==1) checked @endif name="show_lostfound_feed" class="settings" value="1">
                            <span class="slider round"></span>
                          </label>
                        </li>
                        <li>
                          <span style="width: calc(100% - 48px);"><img src="new-design/img/lifeEventDoppelganger.png" class="img-fluid" alt="">
                                        Doppelganger</span>
                          <label class="switch">
                            <input type="checkbox" @if($settings && $settings->show_droppel_feed==1) checked @endif name="show_droppel_feed" class="settings" value="1">
                            <span class="slider round"></span>
                          </label>
                        </li>
                        <li>
                          <span style="width: calc(100% - 48px);"><img src="new-design/img/lifeEventUserConnect.png" class="img-fluid" alt="">
                                        Username Connect</span>
                          <label class="switch">
                            <input type="checkbox" @if($settings && $settings->show_username_feed==1) checked @endif name="show_username_feed" class="settings" value="1">
                            <span class="slider round"></span>
                          </label>
                        </li>
                        
                        
                        <li>
                          <span style="width: calc(100% - 48px);"><img src="new-design/img/lifeEventActivities.png" class="img-fluid" alt="">
                                        Activities</span>
                          <label class="switch">
                            <input type="checkbox" @if($settings && $settings->show_activity_feed==1) checked @endif name="show_activity_feed" class="settings" value="1">
                            <span class="slider round"></span>
                          </label>
                        </li>
                      </ul>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('inc.footer')
<script type="text/javascript">
$(document).on('change','.settings', function(){
	$('.xhr_form').submit();
});

/*$(document).on('change' ,'#delete_account', function(){
  if($(this).prop('checked')==true){
    open_modal('delete-user');
    //$(this).prop('checked',false);
  }
});*/
</script>