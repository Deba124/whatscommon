@include('inc.header')
        <div class="innerBodyOnly homeBody">
          <div class="container-fluid">
            <div class="connectionsBody" style="background: none; box-shadow: none;">
              <div class="position-relative">
                <div class="leftSlidePan closePan">
                  <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                  <div class="leftPan windowHeight windowHeightLeft">

                    <div class="matchCountHold row text-center">
                      <div class="col-4 pr-0">
                        <a href="{{ url('connections') }}">
                          <div class="matchCountNumber">  {{(isset($counts) && $counts[0])? $counts[0]->connection_count : 0}} </div>
                          <div class="matchCountText">Connections</div>
                        </a>
                      </div>
                      <div class="col-4 pl-0 pr-0 borderleftright">
                        <a href="{{ url('life-events') }}">
                          <div class="matchCountNumber"> {{(isset($counts) && $counts[0])? $counts[0]->life_events_count : 0}} </div>
                          <div class="matchCountText">Life Events</div>
                        </a>
                      </div>
                      <div class="col-4 pl-0">
                        <a href="javascript:void;" onclick="searchFeed(false,'matched');">
                          <div class="matchCountNumber">  {{(isset($counts) && $counts[0])? $counts[0]->matched_count : 0}} </div>
                          <div class="matchCountText">Matches</div>
                        </a>
                      </div>
                    </div>

                    <!-- <ul class="nav feedTab">
                      <li><a class="active" data-toggle="tab" href="#eveMatch">Life Event Matches</a></li>
                    </ul> -->
                    <div class="infoMsg">
                      <ul class="nav feedTab">
                        <li><a class="active" style="cursor: default;" data-toggle="tab" href="#eveMatch">Life Event Matches</a></li>
                        <!-- <li class="feedTabLine">|</li>
                        <li><a data-toggle="tab" href="#personalFeed">Personal Feed</a></li> -->
                      </ul>
                      <div class="lifeEventMsg myEventMsg">
                        <div class="infoMsgTitle"> Life Event Matches</div>
                        <div class="infoMsgText">This is a way to filter out your matches and connections. Every user has the
ability to adjust each Life Event total matches according to their liking. Every
adjustment can potentially affect your Connections, Life events and Matches.</div>
                        <div class="infoMsgBtn">
                          <button type="button" class="btn btnEvent">Exit</button>
                        </div>
                      </div>
                    </div>

                    <div class="tab-content">
                        <div id="eveMatch" class="tab-pane fade in active">
                          <form class="xhr_form" method="post" action="api/set-match-setting" id="set-match-setting">
                            @csrf
                            <div class="matchHolder matchHolderShadow">
                                <h5>Personal</h5>
                                <div class="matchSlideHold">
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Minimum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                    <input type="range" name="match_personal" id="slide1" min="1" max="10" class="slider" value="{{ $user_data->match_personal }}"  >
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Maximum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                </div>
                                <div class="slidenumb" id="slidenumb_slide1">{{ $user_data->match_personal }}</div>
                            </div>
                            <input type="hidden" name="user_id" value="{{Session::get('userdata')['id']}}">
                            @if(Session::get('userdata')['user_age'] && Session::get('userdata')['user_age']>=18)
                            <div class="matchHolder matchHolderShadow">
                                <h5>Dating</h5>
                                <div class="matchSlideHold">
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Minimum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                    <input type="range" name="match_dating" id="slide2" min="1" max="10" class="slider" value="{{ $user_data->match_dating }}"  >
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Maximum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                </div>
                                <div class="slidenumb" id="slidenumb_slide2">{{ $user_data->match_dating }}</div>
                            </div>
                            @endif
                            <div class="matchHolder matchHolderShadow">
                                <h5>Adoption / Foster</h5>
                                <div class="matchSlideHold">
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Minimum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                    <input type="range" name="match_adoption" id="slide3" min="1" max="10" class="slider" value="{{ $user_data->match_adoption }}"  >
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Maximum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                </div>
                                <div class="slidenumb" id="slidenumb_slide3">{{ $user_data->match_adoption}}</div>
                            </div>
                            <div class="matchHolder matchHolderShadow">
                                <h5>Travel /  Vacation</h5>
                                <div class="matchSlideHold">
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Minimum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                    <input type="range" name="match_travel" id="slide4" min="1" max="10" class="slider" value="{{ $user_data->match_travel }}"  >
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Maximum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                </div>
                                <div class="slidenumb" id="slidenumb_slide4">{{ $user_data->match_travel }}</div>
                            </div>
                            <div class="matchHolder matchHolderShadow">
                                <h5>Military</h5>
                                <div class="matchSlideHold">
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Minimum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                    <input type="range" name="match_military" id="slide5" min="1" max="10" class="slider" value="{{ $user_data->match_military }}"  >
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Maximum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                </div>
                                <div class="slidenumb" id="slidenumb_slide5">{{ $user_data->match_military }}</div>
                            </div>
                            <div class="matchHolder matchHolderShadow">
                                <h5>School / Education</h5>
                                <div class="matchSlideHold">
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Minimum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                    <input type="range" name="match_education" id="slide6" min="1" max="10" class="slider" value="{{ $user_data->match_education }}"  >
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Maximum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                </div>
                                <div class="slidenumb" id="slidenumb_slide6">{{ $user_data->match_education }}</div>
                            </div>
                            <div class="matchHolder matchHolderShadow">
                                <h5>Careers / Job / Work</h5>
                                <div class="matchSlideHold">
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Minimum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                    <input type="range" name="match_work" id="slide7" min="1" max="10" class="slider" value="{{ $user_data->match_work }}"  >
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Maximum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                </div>
                                <div class="slidenumb" id="slidenumb_slide7">{{ $user_data->match_work }}</div>
                            </div>
                            <div class="matchHolder matchHolderShadow">
                                <h5>Pets</h5>
                                <div class="matchSlideHold">
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Minimum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                    <input type="range" name="match_pet" id="slide8" min="1" max="10" class="slider" value="{{ $user_data->match_pet }}"  >
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Maximum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                </div>
                                <div class="slidenumb" id="slidenumb_slide8">{{ $user_data->match_pet }}</div>
                            </div>
                            <div class="matchHolder matchHolderShadow">
                                <h5>Lost & Found</h5>
                                <div class="matchSlideHold">
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Minimum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                    <input type="range" name="match_lostfound" id="slide9" min="1" max="10" class="slider" value="{{ $user_data->match_lostfound }}"  >
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Maximum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                </div>
                                <div class="slidenumb" id="slidenumb_slide9">{{ $user_data->match_lostfound }}</div>
                            </div>
                            <div class="matchHolder matchHolderShadow">
                                <h5>Doppelganger</h5>
                                <div class="matchSlideHold">
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Minimum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                    <input type="range" name="match_droppel" id="slide10" min="1" max="10" class="slider" value="{{ $user_data->match_droppel }}"  >
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Maximum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                </div>
                                <div class="slidenumb" id="slidenumb_slide10">{{ $user_data->match_droppel }}</div>
                            </div>
                            <div class="matchHolder matchHolderShadow">
                                <h5>Username Connect</h5>
                                <div class="matchSlideHold">
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Minimum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                    <input type="range" name="match_username" id="slide11" min="1" max="10" class="slider" value="{{ $user_data->match_username }}"  >
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Maximum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                </div>
                                <div class="slidenumb" id="slidenumb_slide11">{{ $user_data->match_username }}</div>
                            </div>
                            <div class="matchHolder matchHolderShadow">
                                <h5>Activities</h5>
                                <div class="matchSlideHold">
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Minimum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                    <input type="range" name="match_activity" id="slide12" min="1" max="10" class="slider" value="{{ $user_data->match_activity }}"  >
                                    <div class="matchMinMax">
                                        <span class="minMaxText">Maximum</span>
                                        <span class="minMaxInfo">(In Common)</span>
                                    </div>
                                </div>
                                <div class="slidenumb" id="slidenumb_slide12">{{ $user_data->match_activity }}</div>
                            </div>
                          </form>
                        </div>
                        <div id="personalFeed" class="tab-pane fade">
                            Personal Feed
                        </div>
                    </div>
<style>

.slider {
  -webkit-appearance: none;
  width: 100%;
  height: 10px;
  border-radius: 5px;
  background: #d3d3d3;
  outline: none;
  opacity: 0.7;
  -webkit-transition: .2s;
  transition: opacity .2s;
}

.slider:hover {
  opacity: 1;
}

.slider::-webkit-slider-thumb {
  -webkit-appearance: none;
  appearance: none;
  width: 23px;
  height: 24px;
  border: 0;
  background: url('new-design/img/sliderbutton.png');
  cursor: pointer;
}

.slider::-moz-range-thumb {
  width: 23px;
  height: 24px;
  border: 0;
  background: url('new-design/img/sliderbutton.png');
  cursor: pointer;
}
.magnify-modal{
  background: #fff;
  border-radius: 20px;
  margin-top: 10%;
  max-height: 350px;
}
.magnify-button-close{
  position: absolute;
  background: #3b71b9;
  color: #fff;
  border-radius: 50%;
  right: -15px;
  bottom: 15px;
}
.magnify-title{
  display: none;
}
.magnify-button-prev,.magnify-button-next{
  margin-right: 15px;
  border-radius: 50%;
  border: 1px solid darkgrey;
  /*box-shadow: 4px 2px grey;*/
  box-shadow: none;
  color: #3b71b9;
}
</style>
                  </div>
                </div>
                <div class="midPan windowHeight windowHeightMid">
                    <div class="eventList text-center">
                      <div class="mainCenterOpen">
                        {{-- <div class="addBankBackIcon homeTitleLeftIcon myTagSearch" title="Tag Search">
                          <img src="new-design/img/homeTitleLeftIcon.png" class="img-fluid my-0 w-auto" alt="">
                        </div> --}}
                        {{-- <div class="addBankBackIcon homeTitleRightIcon myReverseLookup" title="Reverse Lookup">
                          <img src="new-design/img/homeTitleRightIcon.png" class="img-fluid my-0 w-auto" alt="">
                        </div> --}}
                        <div class="connectionsDetailsInfo">
                            <div class="connectionsDetailsInfoName">What’sCommon</div>
                            <div class="connectionsDetailsInfoEvents" style="margin-bottom: -6px;">
                                <span class="eventsText orangeText mr-0">Home</span>
                            </div>
                            
                        </div>

                          <div class="innerLogoRight">
                            <div class="searchEvents">
                              <form action="">
                                <div class="input-group">
                                  <input type="text" class="form-control" placeholder="Your Keyword Search">
                                    <div class="input-group-append">
                                      <button class="btn btn-eventSearch" type="submit"><img src="new-design/img/search2.png" class="img-fluid" alt=""></button>
                                     </div>
                                     <img src="new-design/img/shortEvents2.png" class="img-fluid shortEvents" alt="">
                                  </div>
                              </form>
                            </div>
                          </div>
                          <div class="infoMsg">
                          <a href="#" onclick="searchFeed(false,1,'matched');" class="perEvent">
                            <div class="perEventTogether">
                              <img src="new-design/img/event01.png" class="img-fluid" alt="">
                              <span>Personal</span>
                              @if(isset($counts) && $counts[0] && $counts[0]->personal_count>0)<div class="msgCount eventCount">{{(isset($counts) && $counts[0])? $counts[0]->personal_count : 0}}</div>
                              @endif
                            </div>
                          </a>
                          @if(Session::get('userdata')['user_age'] && Session::get('userdata')['user_age']>=18)
                          <a href="#" onclick="searchFeed(false,2,'matched');" class="perEvent">
                            <div class="perEventTogether">
                              <img src="new-design/img/event02.png" class="img-fluid" alt="">
                              <span>Dating</span>
                              @if(isset($counts) && $counts[0] && $counts[0]->dating_count>0)
                              <div class="msgCount eventCount">{{(isset($counts) && $counts[0])? $counts[0]->dating_count : 0}}</div>
                              @endif
                            </div>
                          </a>
                          @endif
                          <a href="#" onclick="searchFeed(false,3,'matched');" class="perEvent">
                            <div class="perEventTogether">
                              <img src="new-design/img/event03.png" class="img-fluid" alt="">
                              <span>Adoption</span>
                              @if(isset($counts) && $counts[0] && $counts[0]->adoption_count>0)
                              <div class="msgCount eventCount">{{(isset($counts) && $counts[0])? $counts[0]->adoption_count : 0}}</div>
                              @endif
                            </div>
                          </a>
                          <a href="#" onclick="searchFeed(false,4,'matched');" class="perEvent">
                            <div class="perEventTogether">
                              <img src="new-design/img/event04.png" class="img-fluid" alt="">
                              <span>Travel</span>
                              @if(isset($counts) && $counts[0] && $counts[0]->travel_count>0)
                              <div class="msgCount eventCount">{{(isset($counts) && $counts[0])? $counts[0]->travel_count : 0}}</div>
                              @endif
                            </div>
                          </a>
                          <a href="#" onclick="searchFeed(false,5,'matched');" class="perEvent">
                            <div class="perEventTogether">
                              <img src="new-design/img/event05.png" class="img-fluid" alt="">
                              <span>Military</span>
                              @if(isset($counts) && $counts[0] && $counts[0]->military_count>0)
                              <div class="msgCount eventCount">{{(isset($counts) && $counts[0])? $counts[0]->military_count : 0}}</div>
                              @endif
                            </div>
                          </a>
                          <a href="#" onclick="searchFeed(false,6,'matched');" class="perEvent">
                            <div class="perEventTogether">
                              <img src="new-design/img/event06.png" class="img-fluid" alt="">
                              <span>Education</span>
                              @if(isset($counts) && $counts[0] && $counts[0]->school_count>0)
                              <div class="msgCount eventCount">{{(isset($counts) && $counts[0])? $counts[0]->school_count : 0}}</div>
                              @endif
                            </div>
                          </a>
                          <a href="#" onclick="searchFeed(false,7,'matched');" class="perEvent">
                            <div class="perEventTogether">
                              <img src="new-design/img/event07.png" class="img-fluid" alt="">
                              <span>Work</span>
                              @if(isset($counts) && $counts[0] && $counts[0]->career_count>0)
                              <div class="msgCount eventCount">{{(isset($counts) && $counts[0])? $counts[0]->career_count : 0}}</div>
                              @endif
                            </div>
                          </a>
                          <a href="#" onclick="searchFeed(false,8,'matched');" class="perEvent">
                            <div class="perEventTogether">
                              <img src="new-design/img/event08.png" class="img-fluid" alt="">
                              <span>Pets</span>
                              @if(isset($counts) && $counts[0] && $counts[0]->pets_count>0)
                              <div class="msgCount eventCount">{{(isset($counts) && $counts[0])? $counts[0]->pets_count : 0}}</div>
                              @endif
                            </div>
                          </a>
                          <a href="#"  onclick="searchFeed(false,11,'matched');" class="perEvent">
                            <div class="perEventTogether">
                              <img src="new-design/img/event09a.png" class="img-fluid" alt="">
                              <span>Username<br> Connect</span>
                              @if(isset($counts) && $counts[0] && $counts[0]->username_count>0)
                              <div class="msgCount eventCount">{{(isset($counts) && $counts[0])? $counts[0]->username_count : 0}}</div>
                              @endif
                            </div>
                          </a>
                          <a href="#" onclick="searchFeed(false,9,'matched');" class="perEvent">
                            <div class="perEventTogether">
                              <img src="new-design/img/event10.png" class="img-fluid" alt="">
                              <span>Lost/Found</span>
                              @if(isset($counts) && $counts[0] && $counts[0]->lost_count>0)
                              <div class="msgCount eventCount">{{(isset($counts) && $counts[0])? $counts[0]->lost_count : 0}}</div>
                              @endif
                            </div>
                          </a>
                          <!-- <a href="#" class="perEvent">
                            <div class="perEventTogether myLostFound">
                              <img src="new-design/img/event10.png" class="img-fluid" alt="">
                              <span>Lost/Found</span>
                            </div>
                          </a> -->
                          <a href="#" class="perEvent">
                            <div class="perEventTogether myDoppelganger">
                              <img src="new-design/img/doppelganger_small.png" class="img-fluid" alt="">
                              <span>Doppelganger</span>
                            </div>
                          </a>
                          <a href="#" class="perEvent">
                            <div class="perEventTogether myDNAmatch">
                              <img src="new-design/img/event12.png" class="img-fluid" alt="">
                              <span>DNA Match</span>
                            </div>
                          </a>

                          <a href="#" class="perEvent">
                            <div class="perEventTogether myReverseLookup">
                              <img src="new-design/img/event09.png" class="img-fluid" alt="">
                              <span>Reverse Lookup</span>
                            </div>
                          </a>

                          <a href="#" class="perEvent">
                            <div class="perEventTogether myTagSearch">
                              <img src="new-design/img/tag-search-event.png" class="img-fluid" alt="">
                              <span>Tag Search</span>
                            </div>
                          </a>

                          <a href="#" onclick="searchFeed(false,12,'matched');" class="perEvent">
                            <div class="perEventTogether">
                              <img src="new-design/img/activity-event.png" class="img-fluid" alt="">
                              <span>Activity</span>
                              @if(isset($counts) && $counts[0] && $counts[0]->activity_count>0)
                              <div class="msgCount eventCount">{{(isset($counts) && $counts[0])? $counts[0]->activity_count : 0}}</div>
                              @endif
                            </div>
                          </a>

                          <a href="{{ url('create-lifeevent') }}" class="text-center createLifeEvent" title="Create Life Event"><img src="new-design/img/addLifeEvents.png" class="img-fluid" alt=""></a>
                          <div class="lifeEventMsg servicesMsg">
                            <div class="infoMsgTitle">Life Events</div>
                            <div class="infoMsgText">It's interesting to look back in time and remember all the things you did and
the people you have met. Be creative! Create separate life events. Don’t go
fast and try and make I big event… Slow and steady wins the race… This is
where you’re going to challenge your memory, bring back forgotten parts of
your past that you didn’t realize could be so much fun bringing back to life.
Get ready to take a fun trip down memory lane! Where you grew up, places
you lived, where you went to schools, places you traveled, where you worked
and so much more. Our 12 Life will spark many thoughts and memories that
will keep you thinking… This is your chance to search, connect, and to be
found! The more details, Specific details, will help make your connections
possible and fun!</div>
                            <div class="infoMsgBtn">
                              <button type="button" class="btn btnEvent">Exit</button>
                            </div>
                          </div>
                        </div>

                      </div>

                      <div class="reverseLookupResult reverseLookupOnlyOne removeMainCenter">
                        <div class="addBankBackIcon reverseLookupBackOne text-left">
                          <img src="new-design/img/back.png" class="img-fluid my-0" alt="">
                        </div>
                        <div class="connectionsDetailsInfo">
                            <div class="connectionsDetailsInfoName">What’sCommon</div>
                            <div class="connectionsDetailsInfoEvents" style="margin-bottom: -6px;">
                                <span class="eventsText orangeText mr-0">Reverse Lookup</span>
                            </div>
                        </div>
                        <div class="reverseLookupMatch">
                          <div class="matchHolder matchHolderShadow semiLostFoundForm">
                            <div class="semiTabBanner">
                              <img src="new-design/img/lookup.png" class="img-fluid" alt="">
                            </div>
                            <form class="xhr_form" method="post" action="api/reverse-lookup" id="reverse-lookup">
                              <div class="whenTitleText">
                                <div class="whenTitle">Quick Search</div>
                                <div class="whenText">Try to search for someone using the single information you have</div>
                              </div>
                              <input type="hidden" name="user_id" value="{{Session::get('userdata')['id']}}">
                              <div class="eventColHeading">What</div>
                              <div class="whatForm">
                                <div class="form-group topFormIcon">
                                  <label for="">Email</label>
                                  <input type="text" class="form-control formMail" id="" placeholder="Email Address" name="email">
                                  <p id="email_error" class="text-danger"></p>
                                </div>
                                <div class="form-group topFormIcon">
                                  <label for="">Phone</label>
                                  <input type="text" class="form-control formPh" id="" placeholder="Phone" name="phone">
                                  <p id="phone_error" class="text-danger"></p>
                                </div>
                              </div>
                              <div class="eventColHeading">When</div>
                              <div class="whatForm whereFrom">
                                <label for="">Birthday</label>
                                <div class="birthdayDate">
                                  <div class="form-group birthdayMonth">
                                    <label for="number">From</label>
                                    <select class="form-control formMonth" id="" name="when_from_month">
                                      <option value="" hidden>Month</option>
                                      <option value="01">January</option>
                                      <option value="02">February</option>
                                      <option value="03">March</option>
                                      <option value="04">April</option>
                                      <option value="05">May</option>
                                      <option value="06">June</option>
                                      <option value="07">July</option>
                                      <option value="08">August</option>
                                      <option value="09">September</option>
                                      <option value="10">October</option>
                                      <option value="11">November</option>
                                      <option value="12">December</option>
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group birthdayDay">
                                    <label for="number">Day</label>
                                    <select class="form-control" id="" name="when_from_day">
                                      <option value="" hidden>DD</option>
                                      @for($i=1;$i<=31;$i++)
                                        @php
                                          $j = sprintf('%02d', $i)
                                        @endphp
                                       <option value="{{ $j }}">{{ $j }}</option>
                                      @endfor
                                    </select>
                                    <img src="img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                  <div class="form-group birthdayYear">
                                    <label for="number">Year</label>
                                    <select class="form-control" id="" name="when_from_year">
                                      <option value="" hidden>YYYY</option>
                                    @for ($i = 1900; $i <= date('Y'); $i++)
                                      <option value="{{ $i }}">{{$i}}</option>
                                    @endfor
                                    </select>
                                    <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                  </div>
                                </div>
                              </div>
                              <div class="eventColHeading">Where</div>
                              <div class="whatForm whereFrom">
                                <div class="form-group">
                                  <label for="">Country</label>
                                  <select class="form-control formCountry" name="country" id="country">
                              <option value="" hidden>Select</option>
            @if($countries)
              @foreach($countries as $country)
                              <option value="{{ $country->country_name }}" data-id="{{ $country->_country_id }}">{{ $country->country_name }}</option>
              @endforeach
            @endif
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                                <div class="form-group">
                                  <label for="">State/County/Province</label>
                                  <select class="form-control formState" name="state" id="province">
                                    <option value="" hidden>Select</option>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                                <div class="form-group">
                                  <label for="">City</label>
                                  <select class="form-control formCity" name="city" id="city">
                                    <option value="" hidden>Select</option>
                                  </select>
                                  <img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
                                </div>
                                <div class="form-group">
                                  <label for="">Street Address</label>
                                  <input type="text" class="form-control formAddress" id="" placeholder="123 abc Ave." name="street">
                                </div>
                                <div class="form-group">
                                  <label for="">ZIP</label>
                                  <input type="text" class="form-control formZip" id="" placeholder="ZIP" name="zip">
                                </div>
                              </div>

                              <div class="whatForm whatFormBtnAll">
                                <div class="btn-Edit-save btn-feedback btn-premium">
                                  <button class="btn btn-Edit btn-save btn-search searchReverseLookup" type="submit">Search</button>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>

                      <div class="reverseLookupResult reverseLookupOnlyTwo removeMainCenter" id="reverse-lookup-div">
                        <div class="addBankBackIcon reverseLookupBackTwo text-left">
                          <img src="new-design/img/back.png" class="img-fluid my-0" alt="">
                        </div>
                        <div class="connectionsDetailsInfo">
                          <div class="connectionsDetailsInfoName">What’sCommon</div>
                          <div class="connectionsDetailsInfoEvents" style="margin-bottom: -6px;">
                            <span class="eventsText orangeText mr-0">Reverse Lookup</span>
                          </div>
                        </div>
                      </div>

                      <div class="reverseLookupResult doppelgangerOnly doppelgangerClose">
                        <div class="addBankBackIcon text-left doppelgangerBack">
                          <img src="new-design/img/back.png" class="img-fluid my-0" alt="">
                        </div>
                        <div class="connectionsDetailsInfo">
                            <div class="connectionsDetailsInfoName">What’sCommon</div>
                            <div class="connectionsDetailsInfoEvents" style="margin-bottom: -6px;">
                                <span class="eventsText orangeText mr-0">Doppelganger</span>
                            </div>
                        </div>
                        <div class="reverseLookupMatch">
                          <div class="matchHolder matchHolderShadow">
                            <div class="connectionsAllDetailsTop">
                              <div class="connectionsDetailsAvtarImg">
                                <img src="{{ url('public/new-design/img/profile_placeholder.jpg') }}" class="img-fluid" alt="">
                              </div>

                              <div class="connectionsDetailsInfo">
                                <div class="connectionsDetailsInfoName" style="color:#000;">Scott McCuskey</div>
                                <div class="connectionsDetailsInfoEvents myEvenntsAll">
                                  <div class="myEvennts mt-2">
                                    <span class="eventsText" style="font-weight: 600;">Detected Image Match</span>
                                  </div>
                                </div>
                                <div class="doppelgangerImgAll mt-2">
                                  <div class="doppelgangerImg">
                                    <img src="{{ url('public/new-design/img/profile_placeholder.jpg') }}" class="img-fluid" alt="">
                                  </div>
                                  <div class="doppelgangerImg">
                                    <img src="{{ url('public/new-design/img/profile_placeholder.jpg') }}" class="img-fluid" alt="">
                                  </div>
                                </div>
                                <div class="reverseLookupBtn mt-2">
                                  <div class="btn btn-following">Visit</div>
                                  <div class="btn btn-following btn-message">Message</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="reverseLookupResult tagSearchOnlyOne removeMainCenter">
                        <div class="addBankBackIcon text-left tagSearchBackOne">
                          <img src="new-design/img/back.png" class="img-fluid my-0" alt="">
                        </div>
                        <div class="connectionsDetailsInfo">
                            <div class="connectionsDetailsInfoName">What’sCommon</div>
                            <div class="connectionsDetailsInfoEvents" style="margin-bottom: -6px;">
                                <span class="eventsText orangeText mr-0">Tag Search</span>
                            </div>
                        </div>
                        <div class="reverseLookupMatch">
                          <div class="matchHolder matchHolderShadow semiLostFoundForm">
                            <div class="semiTabBanner">
                              <img src="new-design/img/tagSearch.png" class="img-fluid" alt="">
                            </div>
                            <form class="xhr_form" method="post" action="api/search-tag" id="search-tag">
                              <div class="whenTitleText">
                                <div class="whenTitle">Tag Search</div>
                                <div class="whenText">Try to search for someone using their tag/plate number, get connected, and chat!</div>
                                <input type="hidden" name="user_id" value="{{Session::get('userdata')['id']}}">
                              </div>
                              <div class="whatForm whereFrom">
                                <div class="form-group">
                                  <label for="">Country</label>
                                  <div class="selectCenterWithIcon">
                                    <select class="form-control selectCenter formCountry selectCenterWithIcon2" id="country2" name="country">
                                      <option value="" hidden>Select</option>
            @if($countries)
              @foreach($countries as $country)
                              <option value="{{ $country->country_name }}" data-id="{{ $country->_country_id }}">{{ $country->country_name }}</option>
              @endforeach
            @endif
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="">State</label>
                                  <div class="selectCenterWithIcon">
                                    <select class="form-control selectCenter formState selectCenterWithIcon2" id="province2" name="state">
                                      <option value="" hidden="">Select State</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="">Tag/plate number</label>
                                  <input type="text" name="tag_number" class="form-control selectCenter formTag">
                                  {{-- <div class="selectCenterWithIcon">
                                    <select class="form-control selectCenter formTag selectCenterWithIcon2">
                                      <option>ABC123</option>
                                      <option>2</option>
                                    </select>
                                  </div> --}}
                                </div>
                              </div>
                              <div class="whatForm whatFormBtnAll">
                                <div class="tagText">**Be sure to enter your tag detail so others can message you too - Personal Life Event > Vehicle Owned > Tag/Plate number</div>
                                <div class="btn-Edit-save btn-feedback btn-premium">
                                  <button class="btn btn-Edit btn-save btn-search searchTag" type="submit">Search</button>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>

                      <div class="reverseLookupResult tagSearchOnlyTwo removeMainCenter">
                        <div class="addBankBackIcon tagSearchBackTwo text-left">
                          <img src="new-design/img/back.png" class="img-fluid my-0" alt="">
                        </div>
                        <div class="connectionsDetailsInfo">
                            <div class="connectionsDetailsInfoName">What’sCommon</div>
                            <div class="connectionsDetailsInfoEvents" style="margin-bottom: -6px;">
                                <span class="eventsText orangeText mr-0">Search Result</span>
                            </div>
                        </div>
                        <div class="reverseLookupMatch" id="tag_search_result">
                          
                        </div>
                      </div>
                    </div>
                </div>
                <div class="rightSlidePan closePan">
                  
                  <a href="#" class="panelslideOpenButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                  <div class="infoMsg">
                    <div class="lifeEventMsg locationMsg">
                      <div class="infoMsgTitle">Global Feed</div>
                      <div class="infoMsgText">Is an interactive way to search for people around the world. This can be a fun
way to meet and interact with people that match your interest. Example:
search military and thank them for their service or “HI” how’s the weather in
India today? “You’re limited only by your interests “.</div>
                      <div class="infoMsgBtn">
                        <button type="button" class="btn btnEvent">Exit</button>
                      </div>
                    </div>

                  </div>
                  <div class="rightPan windowHeight windowHeightRight" >
                  {{-- <div class="position-relative">
                      <h5 class="leftrightHeading" id="feed_title">Global Feed</h5><div class="greenCountFeed" id="feed_count" style="right: 10px;top: 2px;min-width: 50px !important;">{{ $feed_count }}</div>
                  </div> --}}
                    <h5 class="leftrightHeading feedTitleDynamic" id="feed_title">Global Feed</h5>
                    <div class="greenCountFeed greenCountFeedDynamic" id="feed_count" style="right: 17px;top: 2px;">{{ $feed_count }}</div>
                    <div style="clear:both;"></div>
                  <div id="feed_div" onscroll="checkScroll();">
            @if($feeds)
              @foreach ($feeds as $feed)
                    <div class="globalFeedList">
                      <div class="feedShown" id="feedShown-{{$feed->_feed_id}}">
                      <div class="connectedInfo">
                        <div class="position-relative pr-0 feedImg">
                          <div class=""><img src="{{ $feed->user1_profile_photo ? $feed->user1_profile_photo : 'new-design/img/profile_placeholder.jpg' }}" class="img-fluid" alt=""></div>
                          <div class=""><img src="{{ $feed->user2_profile_photo ? $feed->user2_profile_photo : 'new-design/img/profile_placeholder.jpg' }}" class="img-fluid" alt=""></div>
                          <img src="new-design/img/feedadd.png" class="feedadd" alt="">
                        </div>
                        <div class="rightPanText">
                          <p><a href="{{ $feed->feed_user1==Session::get('userdata')['id'] ? url('my-profile') : url('user-profile/?feed='.$feed->_feed_id.'&user='.$feed->feed_user1) }}"> <i>{{ $feed->user1_firstName }} {{ $feed->user1_lastName }} </i></a>is {{ $feed->feed_type }} with <a href="{{ $feed->feed_user2==Session::get('userdata')['id'] ? url('my-profile') : url('user-profile/?feed='.$feed->_feed_id.'&user='.$feed->feed_user2) }}"><i>{{ $feed->user2_firstName }} {{ $feed->user2_lastName }}</i> </a></p>
                          <span>About {{ DateTime_Diff($feed->feed_created) }} ago</span>
                        </div>
                      </div>

                        <div class="feedInfo position-relative">
                            @if($feed->feed_what)
                              <p>
                                <span>What:</span> 
                                <span style="color: #91d639;">{{$feed->feed_event_type}}</span>
                              </p>
                              <p class="pl-5"> {{ $feed->feed_what }}</p>
                            @endif
                            @if($feed->feed_where)<p><span>Where:  </span> {{ $feed->feed_where }}</p>@endif
                            @if($feed->feed_when)<p><span>When: </span> {{ $feed->feed_when }}</p>@endif
                            @if($feed->feed_keywords)<p><span>W5: </span> {{ $feed->feed_keywords }}</p>@endif
                            {{-- <p><span>Why:</span>  Ranger Buddy</p> --}}
                            <div class="greenCountFeed">{{ $feed->feed_match_count }}</div>
                        </div>

                        <!-- <img src="new-design/img/mapfeed.png" class="img-fluid" alt="" width="100%"> -->
                  @if($feed->feed_images && count($feed->feed_images)>0)
                    @php
                    $counter = count($feed->feed_images);
                    $remaining = 0;
                    if($counter>3){
                      $remaining = $counter-2;
                    }
                    @endphp
                        <div class="uploadPhotoHeading">Uploaded photos:</div>
                        <div class="galleryViewAll">
                                  @if($counter==1)
                                    <div class="galleryViewImg1">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-12 galleryViewCol">
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto03.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[0]->fi_img_name }}" href="{{ $feed->feed_images[0]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[0]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  @elseif($counter==2)  
                                    <div class="galleryViewImg2">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto03.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[0]->fi_img_name }}" href="{{ $feed->feed_images[0]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[0]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto01.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[1]->fi_img_name }}" href="{{ $feed->feed_images[1]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[1]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  @elseif($counter==3)     
                                    <div class="galleryViewImg3">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              {{-- <img src="img/feedfoto03.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[0]->fi_img_name }}" href="{{ $feed->feed_images[0]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[0]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto02.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[1]->fi_img_name }}" href="{{ $feed->feed_images[1]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[1]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto01.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[2]->fi_img_name }}" href="{{ $feed->feed_images[2]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[2]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  @else
                                    <div class="galleryViewImg3 galleryViewImg4">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              {{-- <img src="img/feedfoto03.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[0]->fi_img_name }}" href="{{ $feed->feed_images[0]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[0]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              {{-- <img src="img/feedfoto02.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[1]->fi_img_name }}" href="{{ $feed->feed_images[1]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[1]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                          <div class="galleryViewImg">
                                            <img src="new-design/img/feedfoto01.png" class="img-fluid" alt="">
                                            <div class="galleryViewImgMore">{{ $remaining }}+</div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  @endif
                                  </div>
                        {{-- <div class="feedPhoto">
                    @foreach($feed->feed_images as $img)
                      
                          <a data-magnify="gallery" href="{{ $img->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                            <img src="{{ $img->fi_img_url }}" class="img-fluid" width="100%">
                          </a>
                      
                    @endforeach
                            
                        </div> --}}
                  @endif
                        <div class="row text-right rightPanBtn" >
                          <div class="col-6 text-left">
                            <a href="javascript:void;" onclick="open_modal('share-feed','feed_id={{$feed->_feed_id}}')" class="dismissButton" style="display: inline;"><img src="new-design/img/shares_ic2.png" style="height: 15px;margin-top: -3px;" class="img-fluid" alt=""> Share</a>
                          </div>
                          <div class="col-6 text-right">
                            <a href="#" data-feed_id="{{$feed->_feed_id}}" class="dismissButton" onclick="execute('api/hide-feed','user_id={{ Session::get('userdata')['id'] }}&_feed_id={{ $feed->_feed_id }}');"><!-- <img src="new-design/img/dismissIcon.png" class="img-fluid" alt=""> --> Dismiss</a>
                          </div>
                          
                  @if($feed->is_connected==1)
                    @php
                      $receiver_id = $user_quick_blox_id == $feed->user1_quick_blox_id ? $feed->user2_quick_blox_id : $feed->user1_quick_blox_id;
                    @endphp
                          {{-- <a href="{{url('messages?qb='.$receiver_id)}}" class="messageButton"><img src="new-design/img/message-blue.png" class="img-fluid" alt="">
                              Message</a> --}}
                  @endif
                            
                        </div>

                    </div>
                    <div class="feedDismissed" id="feedDismissed-{{$feed->_feed_id}}">
                        <div class="feedDismissedTitle">Feed dismissed</div>
                        <div class="feedDismissedUndo" data-feed_id="{{$feed->_feed_id}}" onclick="execute('api/unhide-feed','user_id={{ Session::get('userdata')['id'] }}&_feed_id={{ $feed->_feed_id }}');">Undo</div>
                      </div>
                  </div>
              @endforeach
            @endif
                    </div>
                    <div style="clear:both;"></div>
                    <div id="load_more" style="position: absolute;bottom: 10vh;padding: 25px;width: 84%;">
@php
  $req = 'user_id='.Session::get('userdata')['id'].'&search_from=web';
@endphp
                      <input type="hidden" name="req" id="req" value="{{ $req }}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<script type="text/javascript">
$(document).ready(function(){
  /*$(".rightSlidePan .panelslideOpenButton").click(function(e) {
    e.preventDefault();
    $(".rightSlidePan").toggleClass("closePan");
    $(this).toggleClass("panelslideCloseButton");
    $(".panelslideOpenButton").removeAttr('style');
    $(this).css('z-index', 999);
    //alert('k');
  });*/
  $(".myEventMsg .btnEvent").click(function(){
    $(".lifeEventMsg.myEventMsg").fadeOut(1000);
  });
  $(".servicesMsg .btnEvent").click(function(){
    $(".lifeEventMsg.servicesMsg").fadeOut(1000);
  });
  $(".locationMsg .btnEvent").click(function(){
    $(".lifeEventMsg.locationMsg").fadeOut(1000);
  });

  $("#myInfo").click(function(){
    $(".lifeEventMsg").fadeIn(1000);
    $(".detailsIcon").addClass("detailsIconActive");
  });
  $(".myTagSearch").click(function(){
    $(".mainCenterOpen").addClass("removeMainCenter");
    $(".tagSearchOnlyOne").removeClass("removeMainCenter");
  });
  $(".tagSearchBackOne").click(function(){
    $(".tagSearchOnlyOne").addClass("removeMainCenter");
    $(".mainCenterOpen").removeClass("removeMainCenter");
  });

  $(".searchTag").click(function(){
    $(".tagSearchOnlyOne").addClass("removeMainCenter");
    $(".tagSearchOnlyTwo").removeClass("removeMainCenter");
  });
  $(".tagSearchBackTwo").click(function(){
    $(".tagSearchOnlyTwo").addClass("removeMainCenter");
    $(".tagSearchOnlyOne").removeClass("removeMainCenter");
  });
});

</script>

@include('inc.footer')
@if(Request::get('type')!='' && in_array(Request::get('type'),[1,2,3,4,5,6,7,8,9,10]))
<script type="text/javascript">
searchFeed(false,"{{ Request::get('type') }}");
</script>@endif
<script>
$(document).on('change', '#country', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province').html(response.reponse_body.province);
            $('#city').html('<option value="" hidden="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#country2', function(){
  var _country_id = $(this).find(':selected').data('id');
  if(_country_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_country_id="+_country_id+"&for_life_event=1", 
      url  : site_url('api/provinces'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.province){
            $('#province2').html(response.reponse_body.province);
            $('#city2').html('<option value="" hidden="">Select City</option>');
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});
$(document).on('change', '#province2', function(){
  var _province_id = $(this).find(':selected').data('id');
  if(_province_id !=''){
    var token = $('input[name="_token"]').attr('value');
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $.ajax({
      type : "POST",
      data : "_province_id="+_province_id+"&for_life_event=1", 
      url  : site_url('api/cities'),
      success : function(response){
        console.log(response);
        if(response.response_code==200){
          if(response.reponse_body.city){
            $('#city2').html(response.reponse_body.city);
          }
        }
      },error: function(jqXHR, textStatus, errorThrown){
      
        console.log(jqXHR.responseJSON);
      }
    });
  }
});

$(document).on('change','.slider',function(){
var slide_id = $(this).attr('id');
var slide_val = $(this).val();
$('#slidenumb_'+slide_id).html(slide_val);
$('#set-match-setting').submit();
});
$('.myDoppelganger, .myDNAmatch').click(function(){
  /*$('#coming-soon-modal').modal('show');*/
  open_modal('coming-soon');
});
    
$(document).ready(function(){    
    if ($(window).width() < 951) {
        $('.windowHeightLeft').css('height',$(window).height()-70);
        $('.windowHeightMid').css('height',$(window).height()-70);
        $('.windowHeightRight').css('height',$(window).height()-70);
        $('.notificationHeight').css('max-height',$(window).height()-70);
    }
    else {
        $('.windowHeightLeft').css('height',$(window).height()-90);
        $('.windowHeightMid').css('height',$(window).height()-90);
        $('.windowHeightRight').css('height',$(window).height()-90);
        $('.notificationHeight').css('max-height',$(window).height()-90);
    }
    let feed_div_height = $('#feed_div').height();
    console.log('feed_div_height => '+feed_div_height);
    /*$('#feed_div').scroll(function(){
        //scrollTop refers to the top of the scroll position, which will be scrollHeight - offsetHeight
        if(this.scrollTop == (this.scrollHeight - this.offsetHeight)) {
          console.log("Top of the bottom reached!");
        }
    });*/
});
function checkScroll(){
    console.log('check scroll called');
}
document.getElementById('feed_div').addEventListener("scroll", function () {
    console.log('Event Fired2');
}, false);
</script>
