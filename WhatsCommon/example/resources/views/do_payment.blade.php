<!DOCTYPE html>
<html lang="en" style="background: transparent;">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>What's Common</title>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <link href="{{ url('css/toastr.min.css') }}" rel="stylesheet">
  @include('inc.css')
<style type="text/css">
.feedbackTitle {
    font-weight: 600;
    font-size: 20px;
    color: #3b71b9;
    text-align: center;
    margin-bottom: 20px;
}
.aboutText {
    font-weight: 400;
    color: #b8b8b8;
    font-size: 14px;
    line-height: 18px;
    margin-top: 13px;
}
.leftrightHeading {
    font-size: 20px;
    color: #3b71b9;
    text-align: center;
    /*font-weight: 600;*/
    margin-bottom: 15px;
}
.leftrightHeading span{
    color: #000;
    font-size: 30px;
    font-weight: 600;
}
</style>
</head>
<body style="background: transparent;">
  <section id="login" class="loginBg" style="min-height: 100vh;display: flex;align-items: center;background: transparent;">
    <div class="container">
      <div class="AllLogin">
        <div class="loginForm">
          <div class="row">
            {{-- <div class="col-6">
              <p class="text-left">Hi, {{ $user_data->firstName }} {{ $user_data->lastName }}</p>
            </div> --}}
            <div class="col-12 text-center"><p class="leftrightHeading">Payable Amount <br> <span><b>{!! $currency_data->currency_symbol !!}{{ $amount }} </b> </span></p></div>
          </div>
        <div id="smart-button-container">
      <div style="text-align: center;">
        <div id="paypal-button-container"></div>
      </div>
    </div>
        </div>

      </div>
    </div>
  </section>
  <script src="{{ url('assets/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ url('assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
  <script src="{{ url('assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ url('assets/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{ url('assets/vendor/aos/aos.js') }}"></script>
  <script src="{{ url('assets/js/main.js') }}"></script>
  <script src="{{ url('js/toastr.min.js') }}"></script>
@include('inc.script')
<script>
$(document).ready(function(){
  /*open_modal('donate-success','currency_id='+'{{ $currency_data->_currency_id }}'+'&amount='+'{{ $amount }}');*/
  /*open_modal('payment-failure');*/
});
</script>
  <!-- Live credetial -->
  <!-- AYHDiS8TDnzx5Tpr16oDWmKIn2XGhLFqFZJ-3GWBVja6eD7jXqLKH6Dpq2TaSiLpdzPvbVSK8mxKv-JU -->
<script src="https://www.paypal.com/sdk/js?client-id=AcwL9AXx5h4aZ8VnStkVAo6DxjXtkiNg11f40SZy7TBx_Dy0s79rJc4xK87QnlfY_VDQYn6Keua6rOJ-&vault=true&disable-funding=card&currency={{$currency_data ? $currency_data->currency_shortname : 'USD'}}" data-sdk-integration-source="button-factory"></script>
<script type="text/javascript">
$(document).ready(function(){
  toastr.options = {
    "closeButton"       : false,
    "debug"             : false,
    "newestOnTop"       : true,
    "progressBar"       : true,
    "positionClass"     : "toast-bottom-right",
    "preventDuplicates" : false,
    "onclick"           : null,
    "showDuration"      : "300",
    "hideDuration"      : "1000",
    "timeOut"           : "5000",
    "extendedTimeOut"   : "1000",
    "showEasing"        : "swing",
    "hideEasing"        : "linear",
    "showMethod"        : "fadeIn",
    "hideMethod"        : "fadeOut"
  };
});
@if(isset($currency_data->plan_paypal_id) && $currency_data->plan_paypal_id)

paypal.Buttons({
      style: {
          shape: 'pill',
          color: 'blue',
          layout: 'vertical',
          label: 'subscribe'
      },
      createSubscription: function(data, actions) {
        return actions.subscription.create({
          // Creates the subscription 
          plan_id: '{{ $currency_data->plan_paypal_id }}'
        });
      },
      onApprove: function(data, actions) {
        //alert(data.subscriptionID); // You can add optional success message for the subscriber here
        console.log(data);
        var plan_id = "{{ $currency_data->id }}";
        var donation_data = `plan_id=`+plan_id+`&user_id={{$user_data->id}}`+`&inapp_type=paypal&inapp_productid=`+data.subscriptionID;

        console.log('donation_data => '+donation_data);

        execute('api/choose-plan',donation_data);
      }
}).render('#paypal-button-container'); // Renders the PayPal button

@else
function initPayPalButton() {
      paypal.Buttons({
        style: {
          shape: 'pill',
          color: 'blue',
          layout: 'vertical',
          label: 'pay',
          
        },

        createOrder: function(data, actions) {
          var amount = "{{ $amount ? $amount : 0 }}";
          
          return actions.order.create({
            purchase_units: [{"amount":{"currency_code":"{{$currency_data ? $currency_data->currency_shortname : 'USD'}}","value":amount}}]
          });
        },

        onApprove: function(data, actions) {
          return actions.order.capture().then(function(orderData) {
            /*$('#paypal-button-container').hide();
            $('#response-div').show(500);*/

@if(Request::get('payment_for')=='donate')
            var currency_id = "{{ $currency_data->_currency_id }}";
            var donation_data = `currency_id=`+currency_id+`&user_id={{$user_data->id}}&amount=`+orderData.purchase_units[0].amount.value+`&payment=`+JSON.stringify(orderData, null, 2);

            console.log('donation_data => '+donation_data);

            execute('api/donate',donation_data);
            open_modal('donate-success','currency_id='+currency_id+'&amount='+orderData.purchase_units[0].amount.value);

@elseif(Request::get('payment_for')=='premium-plans')
            var plan_id = "{{ $currency_data->id }}";
            var donation_data = `plan_id=`+plan_id+`&user_id={{$user_data->id}}`;

            console.log('donation_data => '+donation_data);

            execute('api/choose-plan',donation_data);
            open_modal('payment-success');

@endif
            
            /*var redirect = "{{ url('payment-success') }}";*/
            /*redirect = redirect.replaceAll('&amp;', '&');*/
            
            /*$(location).attr("href", redirect);*/
          });
        },

        onError: function(err) {
          console.log(err);
          open_modal('payment-failure');
        }
      }).render('#paypal-button-container');
    }
initPayPalButton();
@endif

</script>

</body>
</html>