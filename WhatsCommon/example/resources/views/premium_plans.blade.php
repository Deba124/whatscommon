@include('inc.header')
<div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                <div class="leftSlidePan {{ Request::get('menu')==1 ? '' : 'closePan'}}">
                  <a href="#" class="panelslideOpenButton {{ Request::get('menu')==1 ? 'panelslideCloseButton' : ''}}"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                    <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                      <div class="settingLeftTitle">Information</div>
                      <ul class="settingMenu mb-0 list-unstyled">
                        <li class="settingMenuLi">
                          <a class="settingMenu" href="{{ url('feedback') }}"><img src="new-design/img/feedback.png" class="img-fluid menuIcon" alt="">Feedback</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu" href="{{ url('contact-us') }}"><img src="new-design/img/contact.png" class="img-fluid menuIcon" alt="">Contact Us</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu " href="{{ url('about-us') }}"><img src="new-design/img/about.png" class="img-fluid menuIcon" alt="">About Us</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu settingMenuActive" href="{{ url('premium-plans') }}"><img src="new-design/img/premium-a.png" class="img-fluid menuIcon" alt="">WC Premium</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu" href="{{ url('donate') }}"><img src="new-design/img/donate.png" class="img-fluid menuIcon" alt="">Voluntary Gift</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu " style="color: #ed1c24;" href="{{ url('logout') }}"><img src="{{ url('new-design/img/logout.png') }}" class="img-fluid menuIcon" alt="">Log Out</a>
                        </li>
                      </ul>
                    </div>
                    <!-- <a href="{{ url('logout') }}" class="logOut" style="margin-top: -40px;"><img src="new-design/img/logout.png" class="img-fluid menuIcon" alt=""> Log Out</a> -->
                </div>
                <div class="midPan">
                  <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                    <div class="row infoBodyRow">
                      <div class="col-md-5 infoBodyCol">
                          <div class="innerHome">
                            <a href="{{ url('home') }}"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                          </div>
                        <div class="feedbackImg">
                          <img src="new-design/img/premiumImg.png" class="img-fluid" alt="" style="width: 80%;">
                        </div>
                      </div>
                      <!--<div class="col-md-7 infoBodyCol premiumCol">-->
                      <div class="col-md-7 infoBodyCol feedbackColFixed midHightControl" style="background: none;">
                        <div class="feedbackRight wcPremium windowHeight windowHeightMid p-0">
                          <div class="premiumHeader">
                            <div class="feedbackTitle"><span>WC Premium</span></div>
                            <div class="feedbackTitle2" style="color: #000;">What’sCommon is 100% Member-Supported</div>
                          </div>
                          <div class="feedbackTitle3">
                            <div class="feedbackTitle"><span>Features</span></div>
                          </div>

                          <div class="premiumBody">
                            <ul class="list-unstyled mb-0 premiumFeatures">
                              <li>Everyone Is Looking For Someone</li>
                              <li>Find Anyone or Anything... <br>Anywhere at Anytime</li>
                              <li>End to End Encryption</li>
                              <li>Safe and Secure</li>
                              <li>Unlimited Interaction</li>
                              <li>NO Pop-UPs and NO Ads</li>
                              <li>Connect Globaly</li>
                              <li>ONLY site of its kind</li>
                              <li>Cross App Interactions</li>
                            </ul>
                      @if(isset($is_lifetime) && $is_lifetime==1)
                        <div class="form-group text-center">
                          <span>Congratulations! You are a lifetime member.</span>
                        </div>
                      @else
                            <div class="btn-Edit-save btn-feedback btn-premium">
                              <button class="btn btn-Edit btn-save openPlan">WC Premium <br><span>Get this now</span></button>
                            </div>
                      @endif
                          </div>
                        </div>
                        <div class="feedbackRight wcPremiumPlan planClose windowHeight windowHeightMid p-0">
                          <div class="premiumHeader">
                            <div class="planBackIcon">
                              <div class="addBankBackIcon text-left closePanIcon">
                                <img src="img/back.png" class="img-fluid" alt="">
                              </div>
                            </div>
                            <div class="membershipLogo">
                              <img src="img/membershipLogo.png" class="img-fluid membershipLogoImg" alt="">
                            </div>

                            <div class="feedbackTitle mainPlanTitle"><span>Choose a Plan!</span></div>
                            <div class="feedbackTitle2">What’sCommon is a Member-Supported application. We appreciate if you’re loving this application.</div>
                          </div>

                          <div class="premiumBody">
                @if($premium_plans)
                  @foreach($premium_plans as $premium_plan)
                    @if($premium_plan->plan_type=='lifetime')
                            <div class="choosePlan lifetimePaln">
                              <div class="donateForm donatePlanHeading">
                                <div class="monthlyPlan">
                                  <div class="monthlyPlanTitle">{{$premium_plan->plan_type}}</div>
                                </div>
                              </div>
                              <div class="donateForm monthlyPlan">
                                <div class="row pricePlanRow">
                                  <div class="col-6">
                                    <div class="planPriceLeft">
                                      <div class="planPrice">{!! $premium_plan->currency_symbol !!}{{ $premium_plan->plan_amount }}</div>
                                      <div class="planPriceTag">for a liftetime</div>
                                    </div>
                                  </div>
                                  <div class="col-6">
                                    <div class="planPriceRight">
                          @if($premium_plan->is_active==0)
                                      <!-- <div class="btn-Edit-save" id="paypal-button-container" style="margin-left:-30px;"> -->
                                        <button class="btn btn-Plan" onclick="open_modal('select-payment-for-plans','plan_id={{$premium_plan->id}}');">Start Now!</button>
                                      </div>
                          @endif
                                      <!-- <div class="planBtnTag">Cancel anytime!</div> -->
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                    @elseif($premium_plan->plan_type=='annually')
                            <div class="choosePlan annuallyPaln">
                              <div class="donateForm donatePlanHeading">
                                <div class="monthlyPlan">
                                  <div class="monthlyPlanTitle">{{$premium_plan->plan_type}}</div>
                                </div>
                              </div>
                              <div class="donateForm monthlyPlan">
                                <div class="row pricePlanRow">
                                  <div class="col-6">
                                    <div class="planPriceLeft">
                                      <div class="planPrice">{!! $premium_plan->currency_symbol !!}{{ $premium_plan->plan_amount }}</div>
                                      <div class="planPriceTag">a year</div>
                                    </div>
                                  </div>
                                  <div class="col-6">
                                    <div class="planPriceRight">
                            @if($premium_plan->is_active==0)
                                      <!-- <div class="btn-Edit-save" id="paypal-button-container2" style="margin-left:-30px;">
                                        
                                      </div> -->
                                      <button class="btn btn-Plan" onclick="open_modal('select-payment-for-plans','plan_id={{$premium_plan->id}}');">Start Now!</button>
                                      <div class="planBtnTag">Cancel anytime!</div>
                                      
                              @else
                                <button class="planBtnTag" onclick="execute('api/cancel-plan','plan_id={{$premium_plan->id}}&user_id={{Session::get('userdata')['id']}}')">Cancel anytime!</button>
                              @endif
                            
                                      
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                    @else
                            <div class="choosePlan">
                              <div class="donateForm donatePlanHeading">
                                <div class="monthlyPlan">
                                  <div class="monthlyPlanTitle">{{$premium_plan->plan_type}}</div>
                                </div>
                              </div>
                              <div class="donateForm monthlyPlan">
                                <div class="row pricePlanRow">
                                  <div class="col-6">
                                    <div class="planPriceLeft">
                                      <div class="planPrice">{!! $premium_plan->currency_symbol !!}{{ $premium_plan->plan_amount }}</div>
                                      <div class="planPriceTag">per month</div>
                                    </div>
                                  </div>
                                  <div class="col-6">
                                    <div class="planPriceRight">
                                      <!-- <div class="btn-Edit-save">
                                        <button class="btn btn-Edit btn-Plan" onclick="execute('create-squareup-payment','amount={{$premium_plan->plan_amount}}&payment_for=monthly')">Pay Now</button>
                                      </div> -->
                              @if($premium_plan->is_active==0)
                                      <!-- <div class="btn-Edit-save" id="paypal-button-container3" style="margin-left:-30px;">
                                        
                                      </div> -->
                                      <button class="btn btn-Plan" onclick="open_modal('select-payment-for-plans','plan_id={{$premium_plan->id}}');">Start Now!</button>
                                      <div class="planBtnTag">Cancel anytime!</div>
                                      
                              @else
                                
                                <button class="planBtnTag" onclick="execute('api/cancel-plan','plan_id={{$premium_plan->id}}&user_id={{Session::get('userdata')['id']}}')">Cancel anytime!</button>
                              @endif
                                      
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                    @endif
                  @endforeach
                @endif
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@include('inc.footer')
<script>
$(document).ready(function(){
  $(".openPlan").click(function(){
    $(".wcPremium").addClass("planClose");
    $(".wcPremiumPlan").removeClass("planClose");
  });
  $(".closePanIcon").click(function(){
    $(".wcPremiumPlan").addClass("planClose");
    $(".wcPremium").removeClass("planClose");
  });
});
</script>
@if($plan_sess && !empty($plan_sess))
<script>
  var plan_data = `user_id={{Session::get('userdata')['id']}}&plan_id=`+'{{ $plan_sess }}';
  execute('api/choose-plan',plan_data);
</script>
@endif
{{-- test client id --}}
{{-- AcwL9AXx5h4aZ8VnStkVAo6DxjXtkiNg11f40SZy7TBx_Dy0s79rJc4xK87QnlfY_VDQYn6Keua6rOJ- --}}
{{-- live client id --}}
{{-- AYHDiS8TDnzx5Tpr16oDWmKIn2XGhLFqFZJ-3GWBVja6eD7jXqLKH6Dpq2TaSiLpdzPvbVSK8mxKv-JU --}}
{{-- <script src="https://www.paypal.com/sdk/js?client-id=AcwL9AXx5h4aZ8VnStkVAo6DxjXtkiNg11f40SZy7TBx_Dy0s79rJc4xK87QnlfY_VDQYn6Keua6rOJ-&disable-funding=card&currency=USD&enable-funding=venmo" data-sdk-integration-source="button-factory"></script> --}}

<script src="https://www.paypal.com/sdk/js?client-id=AcwL9AXx5h4aZ8VnStkVAo6DxjXtkiNg11f40SZy7TBx_Dy0s79rJc4xK87QnlfY_VDQYn6Keua6rOJ-&vault=true&disable-funding=card" data-sdk-integration-source="button-factory"></script>
