<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>What's Common | {{ $privacy->page_title }}</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  @include('inc.css')
<link href="css/toastr.min.css" rel="stylesheet">
<body>
  <section id="login" class="signUpBg">
    <div class="container">
      <div class="AllLogin">
        <div class="loginForm" style="max-width: fit-content;">
          <div class="loginLogo">
            <img src="img/signUpLogo.png" class="img-fluid" alt="">
          </div>

          <div class="signUpHeading">{{ $privacy->page_title }}</div>
          <div class="signUpForm" style="text-align: justify !important;">{!! $privacy->page_content !!}</div>
        </div>
      </div>
    </div>
    <div class="loginClose" style="background: #3b8dcf;border-radius: 50%;">
      <a href="{{ url('/') }}"><img src="img/loginClose.png" class="img-fluid" alt=""></a>
    </div>
  </section>
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  <script src="js/toastr.min.js"></script>

  @include('inc.script')
</body>

</html>