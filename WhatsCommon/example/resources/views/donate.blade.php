@include('inc.header')
<style type="text/css">
.priceRangeOnlyActive{
  /*border: 1px solid #3b71b9;*/
  border: 1px solid #fff;
    background: #3b71b9;
    color: #fff;
}
</style>
<div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                <div class="leftSlidePan {{ Request::get('menu')==1 ? '' : 'closePan'}}">
                  <a href="#" class="panelslideOpenButton {{ Request::get('menu')==1 ? 'panelslideCloseButton' : ''}}"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                    <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                      <div class="settingLeftTitle">Information</div>
                      <ul class="settingMenu mb-0 list-unstyled">
                        <li class="settingMenuLi">
                          <a class="settingMenu" href="{{ url('feedback') }}"><img src="{{ url('new-design/img/feedback.png') }}" class="img-fluid menuIcon" alt="">Feedback</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu" href="{{ url('contact-us') }}"><img src="{{ url('new-design/img/contact.png') }}" class="img-fluid menuIcon" alt="">Contact Us</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu " href="{{ url('about-us') }}"><img src="{{ url('new-design/img/about.png') }}" class="img-fluid menuIcon" alt="">About Us</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu " href="{{ url('premium-plans') }}"><img src="{{ url('new-design/img/premium.png') }}" class="img-fluid menuIcon" alt="">WC Premium</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu settingMenuActive" href="{{ url('donate') }}"><img src="{{ url('new-design/img/donate-a.png') }}" class="img-fluid menuIcon" alt="">Voluntary Gift</a>
                        </li>
                        <li class="settingMenuLi">
                          <a class="settingMenu " style="color: #ed1c24;" href="{{ url('logout') }}"><img src="{{ url('new-design/img/logout.png') }}" class="img-fluid menuIcon" alt="">Log Out</a>
                        </li>
                      </ul>
                    </div>
                    <!-- <a href="{{ url('logout') }}" class="logOut" style="margin-top: -40px;"><img src="{{ url('new-design/img/logout.png') }}" class="img-fluid menuIcon" alt=""> Log Out</a> -->
                </div>
                <div class="midPan">
                  <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                    <div class="row infoBodyRow">
                      <div class="col-md-5 infoBodyCol">
                          <div class="innerHome">
                            <a href="{{ url('home') }}"><img src="{{ url('new-design/img/home.png') }}" class="img-fluid" alt=""></a>
                          </div>
                        <div class="feedbackImg">
                          <img src="{{ url('new-design/img/donateImg.png') }}" class="img-fluid" alt="">
                        </div>
                      </div>
                      <div class="col-md-7 infoBodyCol feedbackColFixed midHightControl">
                        <div class="feedbackRight donteCol windowHeight windowHeightMid">
                          <div class="feedbackTitle"><span>Voluntary Gift</span></div>

                          <form class="donateForm xhr_form" method="post" action="donate-form" id="donate-form">
                            @csrf
                            <div class="donateFormTitle">Choose amount</div>
                            <div class="feedbackTitle2">Voluntary Gifts help keep cost low. Thank YOU!</div>
                            <div class="donateFormOnly">
                              <div class="form-group">
                                <select class="form-control" id="sel1" name="currency_id">
                      @if($currencies)
                        @foreach($currencies as $currency)
                                <option value="{{ $currency->_currency_id }}" {{ ($currency_data && $currency_data->_currency_id == $currency->_currency_id) ? 'selected' : '' }} data-amounts="{{ $currency->donation_amounts }}" data-symbol="{{ $currency->currency_symbol }}">{{ $currency->currency_name }}</option>
                        @endforeach
                      @endif
                            
                                </select>
                                <p id="currency_id_error" class="text-danger"></p>
                              </div>
                              <input type="hidden" name="donation_amount" id="donation_amount" value="">
                              <div class="priceRange">
                                <div class="row priceRangeRow" id="amounts_div">
              @if($currencies)
                @php
                  if($currency_data){
                    $currency_symbol = $currency_data->currency_symbol;
                    $amounts = $currency_data->donation_amounts ? $currency_data->donation_amounts : [];
                  }else{
                    $currency_symbol = $currencies[0]->currency_symbol;
                    $amounts = $currencies[0]->donation_amounts ? $currencies[0]->donation_amounts : [];
                  }
                  
                @endphp
                @if($amounts)
                  @foreach($amounts as $amount)
                                  <div class="col-md-4 priceRangeCol">
                                    <div class="priceRangeOnly" id="{{ $amount->_da_id }}" title="{{ $amount->da_amount }}"><span>{!! $currency_symbol !!} </span> {{ $amount->da_amount }}</div>
                                  </div>
                  @endforeach
                @endif
              @endif
                                  <p id="donation_amount_error" class="text-danger"></p>
                                  <div class="col-md-12 priceRangeCol">
                                    <div class="priceRangeOnly" id="custom" title="custom">Custom</div>
                                  </div>
                                </div>
                                <div class="row priceRangeRow" id="custom_div" style="display: none;">
                                  <div class="col-md-12 priceRangeCol">
                                    <input type="text" name="custom_amount" id="custom_amount" class="form-control" placeholder="Enter Custom Amount">
                                    <p id="custom_amount_error" class="text-danger"></p>
                                  </div>
                                </div>
                                <div class="row priceRangeRow">
                                  <div class="col-md-12 priceRangeCol">
                                    <div class="btn-Edit-save">
                                      <button class="btn btn-Edit btn-save" type="submit">Next</button>
                                      <!-- <div id="smart-button-container">
      <div style="text-align: center;">
        <div id="paypal-button-container"></div>
      </div>
    </div> -->
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </form>
{{-- test client id --}}
{{-- AcwL9AXx5h4aZ8VnStkVAo6DxjXtkiNg11f40SZy7TBx_Dy0s79rJc4xK87QnlfY_VDQYn6Keua6rOJ- --}}
{{-- live client id --}}
{{-- AYHDiS8TDnzx5Tpr16oDWmKIn2XGhLFqFZJ-3GWBVja6eD7jXqLKH6Dpq2TaSiLpdzPvbVSK8mxKv-JU --}}
  <script src="https://www.paypal.com/sdk/js?client-id=AcwL9AXx5h4aZ8VnStkVAo6DxjXtkiNg11f40SZy7TBx_Dy0s79rJc4xK87QnlfY_VDQYn6Keua6rOJ-&disable-funding=card&currency={{$currency_data ? $currency_data->currency_shortname : 'USD'}}" data-sdk-integration-source="button-factory"></script>
                         
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@include('inc.footer')

<script type="text/javascript">
$(document).ready(function(){
  $('#smart-button-container #description').val('WhatsCommon Donations');
@if(Request::get('amount'))
$('#donation_amount').val('custom');
$('#amount').val('{{ Request::get('amount') }}');
$('#custom_amount').val('{{ Request::get('amount') }}');
$('#custom_div').show();
$(".priceRangeOnly").each(function() {
  $(this).removeClass('priceRangeOnlyActive');
});
$('#custom').addClass('priceRangeOnlyActive');
@endif
@if(Request::get('amount') && Request::get('currency') && Request::get('user_id'))
/*jQuery('div.paypal-button.large').click();*/
  $('#donate-form').submit();
@endif
@if(isset($donation_success) && !empty($donation_success))
toastr.success('{{ $donation_success }}');
@endif
});
$(document).on('change','#sel1',function(){
  var currency = $(this).val();
  var page_url = site_url('donate/?currency='+currency);
  $(location).attr("href", page_url);
  /*var amounts = $(this).find(':selected').data('amounts');

  var symbol = $(this).find(':selected').data('symbol');
  var amounts_div = '';
  if(amounts){

    $.each( amounts, function( index, amount ) {
      amounts_div += `<div class="col-md-4 priceRangeCol">
                                    <div class="priceRangeOnly" id="`+amount._da_id+`" title="`+amount.da_amount+`"><span>`+symbol+` </span> `+amount.da_amount+`</div>
                                  </div>`;
    });
  }

  amounts_div += `<div class="col-md-12 priceRangeCol">
                                    <div class="priceRangeOnly" id="custom" title="custom">Custom</div>
                                  </div>`;

  $('#amounts_div').html(amounts_div);
  $('#custom_div').hide();*/
});

$(document).on('click','.priceRangeOnly',function(){
  var amount_id = $(this).attr('id');
  if($(this).hasClass('priceRangeOnlyActive')==false){
    $(".priceRangeOnly").each(function() {
      $(this).removeClass('priceRangeOnlyActive');
    });
    $('#'+amount_id).addClass('priceRangeOnlyActive');
    $('#donation_amount').val($('#'+amount_id).attr('title'));
    if($('#'+amount_id).attr('title')=='custom'){
      $('#custom_amount').val('');
      $('#custom_div').show();
    }else{
      $('#custom_div').hide();
      $('#amount').val($('#'+amount_id).attr('title'));
    }
  }
});
</script>
