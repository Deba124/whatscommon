<ul class="list-unstyled mb-0 lifeEventMenu">
  <li class="lifeEventMenuLi lifeEventPersonal {{ (!Request::get('type') || Request::get('type')==1) ? 'lifeEventMenuLiActive' : ''}} ">
    <a href="{{ url('create-lifeevent') }}" class="lifeEventMenuLink">
      <span class="lifeEventMenuText">Personal</span>
    </a>
  </li>
@if(Session::get('userdata')['user_age'] && Session::get('userdata')['user_age']>=18)
  <li class="lifeEventMenuLi lifeEventDating {{ Request::get('type')==2 ? 'lifeEventMenuLiActive' : ''}}">
    <a href="{{ url('create-lifeevent/?type=2') }}" class="lifeEventMenuLink">
      <span class="lifeEventMenuText">Dating</span>
    </a>
  </li>
@endif
  <li class="lifeEventMenuLi lifeEventAdoption {{ Request::get('type')==3 ? 'lifeEventMenuLiActive' : ''}}">
    <a href="{{ url('create-lifeevent/?type=3') }}" class="lifeEventMenuLink">
      <span class="lifeEventMenuText">Adoption</span>
    </a>
  </li>
  <li class="lifeEventMenuLi lifeEventTravel {{ Request::get('type')==4 ? 'lifeEventMenuLiActive' : ''}}">
    <a href="{{ url('create-lifeevent/?type=4') }}" class="lifeEventMenuLink">
      <span class="lifeEventMenuText">Travel</span>
    </a>
  </li>
  <li class="lifeEventMenuLi lifeEventMilitary {{ Request::get('type')==5 ? 'lifeEventMenuLiActive' : ''}}">
    <a href="{{ url('create-lifeevent/?type=5') }}" class="lifeEventMenuLink">
      <span class="lifeEventMenuText">Military</span>
    </a>
  </li>
  <li class="lifeEventMenuLi lifeEventEducation {{ Request::get('type')==6 ? 'lifeEventMenuLiActive' : ''}}">
    <a href="{{ url('create-lifeevent/?type=6') }}" class="lifeEventMenuLink">
      <span class="lifeEventMenuText">Education</span>
    </a>
  </li>
  <li class="lifeEventMenuLi lifeEventWork {{ Request::get('type')==7 ? 'lifeEventMenuLiActive' : ''}}">
    <a href="{{ url('create-lifeevent/?type=7') }}" class="lifeEventMenuLink">
      <span class="lifeEventMenuText">Work</span>
    </a>
  </li>
  <li class="lifeEventMenuLi lifeEventPets {{ Request::get('type')==8 ? 'lifeEventMenuLiActive' : ''}}">
    <a href="{{ url('create-lifeevent/?type=8') }}" class="lifeEventMenuLink">
      <span class="lifeEventMenuText">Pets</span>
    </a>
  </li>
  <li class="lifeEventMenuLi lifeEventUserConnect {{ Request::get('type')==11 ? 'lifeEventMenuLiActive' : ''}}">
    <a href="{{ url('create-lifeevent/?type=11') }}" class="lifeEventMenuLink ">
      <span class="lifeEventMenuText">Username Connect</span>
    </a>
  </li>
  <li class="lifeEventMenuLi lifeEventL_F {{ Request::get('type')==9 ? 'lifeEventMenuLiActive' : ''}}">
    <a href="{{ url('create-lifeevent/?type=9') }}" class="lifeEventMenuLink ">
      <span class="lifeEventMenuText">Lost/Found</span>
    </a>
  </li>
  <li class="lifeEventMenuLi lifeEventDoppelganger {{ Request::get('type')==10 ? 'lifeEventMenuLiActive' : ''}}">
    <a href="#" class="lifeEventMenuLink" onclick="open_modal('coming-soon');">
      <span class="lifeEventMenuText">Doppelganger</span>
    </a>
  </li> 
  <li class="lifeEventMenuLi lifeEventDnaMacth">
    <a href="#" class="lifeEventMenuLink" onclick="open_modal('coming-soon');">
      <span class="lifeEventMenuText">DNA Match</span>
    </a>
  </li>
  <li class="lifeEventMenuLi lifeEventActivities {{ Request::get('type')==12 ? 'lifeEventMenuLiActive' : ''}}">
    <a href="{{ url('create-lifeevent/?type=12') }}" class="lifeEventMenuLink">
      <span class="lifeEventMenuText">Activities</span>
    </a>
  </li>
</ul>