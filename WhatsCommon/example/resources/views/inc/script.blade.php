  <!-- Vendor JS Files -->
{{--   <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/aos/aos.js"></script> --}}

  <!-- Template Main JS File -->
{{--   <script src="assets/js/main.js"></script> --}}

  <!-- parallax -->
  <!-- <script src="assets/js/parallax.js"></script> -->
  <!-- <script src="assets/js/jquery.bxslider.js"></script> -->

  <script>
  function addKeyword(){
  var keyword = $('#add_keyword').val();
  var key_split = keyword.split(',');
  var split_count = 0;
  var keywords = $('#keywords').val();
  var split_str = keywords.split(",");
  split_count = split_str.length+1;
  console.log('split count => '+split_count);
  $.each( key_split, function( index, value ) {
    value = value.trim();
    
    if (split_str.indexOf(value) === -1) {
      
      $('#keywords_div').append('<div class="addFieldItem keywords" id="'+split_count+'" title="'+value+'">'+value+' <span class="removeField" onclick="removeKeyword('+split_count+');"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>');
      split_count += 1;
      console.log('split count2 => '+split_count);
    }
  });
  $('#add_keyword').val('');
  checkKeywords();
}

function checkKeywords(){
  var keywords = [];
  /*$('#keywords_div').html('');*/
  counter = 1;
  $(".keywords").each(function() {
    var keyval = $(this).attr('title');
    keywords.push(keyval);
  });
  $('#keywords_div').html('');
  for(i=0;i<keywords.length;++i){
    $('#keywords_div').append('<div class="addFieldItem keywords" id="'+counter+'" title="'+keywords[i]+'">'+keywords[i]+' <span class="removeField" onclick="removeKeyword('+counter+');"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>');
      counter += 1;
  }
  var keyword = keywords.join(',');
  $('#keywords').val(keyword);
  console.log('keywords => '+keyword);
}
function removeKeyword(id){
  $('#'+id).remove();
  checkKeywords();
}

function addKeyword2(){
  var keyword = $('#add_keyword2').val();
  var key_split = keyword.split(',');
  var split_count = 0;
  var keywords = $('#keywords2').val();
  var split_str = keywords.split(",");
  split_count = split_str.length+1;
  console.log('split count => '+split_count);
  $.each( key_split, function( index, value ) {
    value = value.trim();
    
    if (split_str.indexOf(value) === -1) {
      
      $('#keywords_div2').append('<div class="addFieldItem keywords" id="'+split_count+'" title="'+value+'">'+value+' <span class="removeField" onclick="removeKeyword2('+split_count+');"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>');
      split_count += 1;
      console.log('split count2 => '+split_count);
    }
  });
  $('#add_keyword2').val('');
  checkKeywords2();
}

function checkKeywords2(){
  var keywords = [];
  /*$('#keywords_div').html('');*/
  counter = 1;
  $(".keywords").each(function() {
    var keyval = $(this).attr('title');
    keywords.push(keyval);
  });
  $('#keywords_div2').html('');
  for(i=0;i<keywords.length;++i){
    $('#keywords_div2').append('<div class="addFieldItem keywords" id="'+counter+'" title="'+keywords[i]+'">'+keywords[i]+' <span class="removeField" onclick="removeKeyword2('+counter+');"><img src="new-design/img/removeField.png" class="img-fluid" alt=""></span></div>');
      counter += 1;
  }
  var keyword = keywords.join(',');
  $('#keywords2').val(keyword);
  console.log('keywords => '+keyword);
}
function removeKeyword2(id){
  $('#'+id).remove();
  checkKeywords2();
}
  $(document).ready(function(){
    $('#login').css('minHeight',$(window).height());
  });
  function site_url(page_name=''){
    var site_url = '<?php echo url('/');  ?>';
    site_url = site_url+'/'+page_name;
    console.log(site_url);
    return site_url;
  }
  $(document).on('submit' ,'.xhr_form' , function(e){
    e.preventDefault();
    var token = $('input[name="_token"]').attr('value');
    //console.log(token);
    $('#loading').show();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token,
        'Api-Auth-Key' : '#whts_cmn_8080'
      }
    });
    var submit_id = $(this).attr('id');
    var action    = site_url($(this).attr('action'));
    var submit_btn_content = $('#'+submit_id+' button[type="submit"]').html();

    $('#'+submit_id+' button[type="submit"]').html("<i class='fa fa-spinner fa-spin'></i> Loading");
    $('#'+submit_id+' button[type="submit"]').attr('disabled',true);
    $.ajax({
      type : "POST",
      data : new FormData(this),
      contentType: false,   
      cache: false,             
      processData:false, 
      url  : action,
      success : function(response){
        $('#loading').hide();
        console.log(response);
        if(response.response_code==200){
          $('#responseDiv').show();
          if(response.response_msg){
            if(response.show_toast){
              toastr.success(response.response_msg);
            }
            /*toastr.success(response.response_msg);*/
            $('#responseDiv').html('<p class="text-success">'+response.response_msg+'</p>');
          }
          
          /*$(location).attr("href", site_url());*/
          if(response.close){
            $("#"+submit_id+"-modal").modal('hide');
          }
          if(response.evaluate){
            eval(response.evaluate);
          }
          if (response.redirect && response.time) {
            window.setTimeout(function(){window.location.href = response.redirect;}, response.time);
          }
          else if(response.redirect){
            $(location).attr("href", response.redirect); 
          }
          else if(response.time){
            window.setTimeout(function(){location.reload();}, response.time);
          }

          if(response.reponse_body.html_id){
            $('#'+response.reponse_body.html_id).html(response.reponse_body.html);
          }

          $('#'+submit_id+' button[type="submit"]').attr('disabled',false);
          $('#'+submit_id+' button[type="submit"]').html(submit_btn_content);
        }
      },error: function(jqXHR, textStatus, errorThrown){
        $('#loading').hide();
        /*console.log(textStatus + ": " + jqXHR.status + " " + errorThrown);*/
        /*console.log(jqXHR);*/
        console.log(jqXHR.responseJSON);
        toastr.error(jqXHR.responseJSON.response_msg);
        $('#'+submit_id+' button[type="submit"]').attr('disabled',false);
        $('#'+submit_id+' button[type="submit"]').html(submit_btn_content);
        $("p.text-danger").each(function() {
          $(this).html('');
        });
        if(jqXHR.responseJSON.errors){

          var err = jqXHR.responseJSON.errors;
          $.each(err, function(key, value){

            if (err[key]) {
              $("#"+key+"_error").html(err[key]);
            }
            else{
              $("#"+key+"_error").html(''); 
            }
          });
        }
      }
    });
  });
function open_modal(link, data){
  $('#loading').show();

  var url = site_url(link+'-modal');
  var token = $('meta[name="_token"]').attr('content');
  console.log(token);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token,
      'Api-Auth-Key' : '#whts_cmn_8080'
    }
  });
  $.ajax({
    type : "POST",
    data : data,
    url  : url,
    success : function(response){
      console.log(response);
      $('#loading').hide();
      
      if(response.response_code==200){
        //if ($("#"+link+"-modal").length!=0) {
          console.log('here=> '+ $("#"+link+"-modal").length);
          $("#"+link+"-modal").html('');
        //}
        jQuery('body').prepend(response.modal);
        $("#"+link+"-modal").modal('show');
      }else{
        /*toastr.error(response.response_msg);*/
      }
    },
    error: function(jqXHR, textStatus, errorThrown){
      console.log(jqXHR.responseJSON);
      $('#loading').hide();
      /*toastr.error(response.response_msg);*/
    }
  });
}
function execute(link, data){
  $('#loading').show();
  var token = $('input[name="_token"]').attr('value');
  console.log(token);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token,
      'Api-Auth-Key' : '#whts_cmn_8080'
    }
  });
  var url = site_url(link);

  $.ajax({
    type : "POST",
    data : data,
    url  : url,
    success:function(response){
      /*console.log(response);
      return false;*/
      $('#loading').hide();
      if(response.response_code==200 || response.response_code==202){
        if(response.response_msg){
          if(response.show_toast){
            toastr.success(response.response_msg);
          }
          //toastr.success(response.response_msg);
        }

        if(response.reponse_body.html_id){
          $('#'+response.reponse_body.html_id).html(response.reponse_body.html);
        }

        if(response.close){
          $('#'+response.close).modal('hide');
        }

        if(response.evaluate){
          eval(response.evaluate);
        }
        
        if (response.redirect && response.time) {
          window.setTimeout(function(){window.location.href = response.redirect;}, response.time);
        }
        else if(response.redirect){
          $(location).attr("href", response.redirect); 
        }
        else if(response.time){
          window.setTimeout(function(){location.reload();}, response.time);
        }/*else{
          location.reload();
        }*/
      }
    },error: function(jqXHR, textStatus, errorThrown){
      $('#loading').hide();
      console.log(jqXHR.responseJSON);
      /*toastr.error(jqXHR.responseJSON.response_msg);*/
    }
  });
}

function showHidePassword(id){
  if($('#'+id).attr('type')=='password'){
    $('#'+id).attr('type','text');
    $('#'+id+'-img').attr("src",'new-design/img/eye-closed.png');
  }else{
    $('#'+id).attr('type','password');
    $('#'+id+'-img').attr("src",'new-design/img/eye-open.png');
  }
}
  </script>
<script>
window.fbAsyncInit = function() {
    // FB JavaScript SDK configuration and setup
  FB.init({
    appId      : '1009081199908339', // FB App ID
    cookie     : true,  // enable cookies to allow the server to access the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v3.2' // use graph api version 2.8
  });
    
    // Check whether the user already logged in
  /*FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
        //display user data
        getFbUserData();
    }
  });*/
};

// Load the JavaScript SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Facebook login with JavaScript SDK
function fbLogin() {
  FB.login(function (response) {
    if (response.authResponse) {
        // Get and display the user profile data
        getFbUserData();
    } else {
        /*document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';*/
        console.log('User cancelled login or did not fully authorize.');
    }
  }, {scope: 'email'});
}

// Fetch the user profile data from facebook
function getFbUserData(){
  FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
  function (response) {
    console.log('response => ',response);
    var name = response.first_name +' '+ response.last_name;
    var user_data = {'social_type':'facebook','social_id':response.id,'email':response.email,'name':name};
    console.log('user_data => ',user_data);
    execute('social-login',user_data);
    /*document.getElementById('fbLink').setAttribute("onclick","fbLogout()");
    document.getElementById('fbLink').innerHTML = 'Logout from Facebook';
    document.getElementById('status').innerHTML = '<p>Thanks for logging in, ' + response.first_name + '!</p>';
    document.getElementById('userData').innerHTML = '<h2>Facebook Profile Details</h2><p><img src="'+response.picture.data.url+'"/></p><p><b>FB ID:</b> '+response.id+'</p><p><b>Name:</b> '+response.first_name+' '+response.last_name+'</p><p><b>Email:</b> '+response.email+'</p><p><b>Gender:</b> '+response.gender+'</p><p><b>FB Profile:</b> <a target="_blank" href="'+response.link+'">click to view profile</a></p>';*/
  });
}
</script>
<script type="text/javascript" src="https://appleid.cdn-apple.com/appleauth/static/jsapi/appleid/1/en_US/appleid.auth.js"></script>
        
<script type="text/javascript">
var nonce = '{{ alpha_numeric_rand() }}';
console.log('nonce=> '+nonce);
AppleID.auth.init({
    clientId : 'com.whatscommon.app.serviceid',
    scope : 'name email',
    redirectURI : '{{ url("/") }}',
    state : nonce,
    nonce : nonce,
    usePopup : true
});
document.addEventListener('AppleIDSignInOnSuccess', function(data) {
  console.log('success=> ');
  console.log(data);
var jwtcode = parseJwt(data.detail.authorization.id_token);
console.log(jwtcode);
var email = jwtcode.email;
var social_id = jwtcode.sub;
if(data.detail.user){
  var name = data.detail.user.name.firstName +' '+ data.detail.user.name.lastName;
    var user_data = {'social_type':'apple','social_id':social_id,'email':email,'name':name};
    console.log('user_data => ',user_data);
    execute('social-login',user_data);
}else{
  /*console.log(data.detail.authorization.id_token);
  
  console.log(jwtcode);*/
  var user_data = {'social_type':'apple','social_id':social_id,'email':email};
  console.log('user_data => ',user_data);
  execute('apple-login',user_data);
}
});
function parseJwt (token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
};
</script>
<script src="https://apis.google.com/js/api:client.js"></script>
<script>
  var googleUser = {};
  var startApp = function() {
    gapi.load('auth2', function(){
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      auth2 = gapi.auth2.init({
        client_id: '156165348207-5h3p2j108m2apnsq5f5n1mv6uto0lvr9.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        // Request scopes in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
      attachSignin(document.getElementById('googleLoginBtn'));
    });
  };

  function attachSignin(element) {
    console.log(element.id);
    auth2.attachClickHandler(element, {},
        function(googleUser) {
          console.log('googleUser => ',googleUser);
          var profile = googleUser.getBasicProfile();
          var user_data = {'social_type':'google','social_id':profile.getId(),'email':profile.getEmail(),'name':profile.getName()};
          console.log('user_data => ',user_data);
          execute('social-login',user_data);
          /*document.getElementById('name').innerText = "Signed in: " +
              googleUser.getBasicProfile().getName();*/
        }, function(error) {
          /*alert(JSON.stringify(error, undefined, 2));*/
          console.log(JSON.stringify(error, undefined, 2));
        });
  }
</script>
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">
const strpp = Stripe('pk_test_51LKpnUH6F5jONBiWzxlY9NXMtFs7vmmvjG9449mKSQm3IkglRKBsPzoPVJJLGjr6GcjHgSGVcUkn0OEGmTsRHryu003Oauhhpj');
</script>
{{-- <script src="https://www.gstatic.com/firebasejs/5.11.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.11.1/firebase-auth.js"></script> --}}
<script type="module">
  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.9.4/firebase-app.js";
  import { TwitterAuthProvider, signInWithPopup } from "https://www.gstatic.com/firebasejs/9.9.4/firebase-auth.js";
  //import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.9.4/firebase-analytics.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  const firebaseConfig = {
    apiKey: "AIzaSyDL_PIKNUp__LeRyeff1oSSyS7qE4rqGEY",
    authDomain: "testwc-73cde.firebaseapp.com",
    projectId: "testwc-73cde",
    storageBucket: "testwc-73cde.appspot.com",
    messagingSenderId: "156165348207",
    appId: "1:156165348207:web:748fac85dd5753ab49a69a",
    measurementId: "G-QWL28D84PM"
  };

  // Initialize Firebase
  //const app = initializeApp(firebaseConfig);
  //const app = firebase.initializeApp(firebaseConfig);
  //const analytics = getAnalytics(app);

</script>
{{-- <script src="https://www.gstatic.com/firebasejs/9.9.4/firebase-auth.js"></script> --}}
{{-- <script src="https://www.gstatic.com/firebasejs/5.11.1/firebase-auth.js"></script> --}}
<script type="text/javascript">
function twitterSignin(){
  //var provider = new firebase.auth.TwitterAuthProvider();
    //firebase.auth().useDeviceLanguage();
    /*firebase.auth().*/signInWithPopup(TwitterAuthProvider).then(function(result) {
  // This gives you a the Twitter OAuth 1.0 Access Token and Secret.
  // You can use these server side with your app's credentials to access the Twitter API.
  var token = result.credential.accessToken;
  console.log(token);
  var secret = result.credential.secret;
  console.log(secret);
  // The signed-in user info.
 
  //document.querySelector("#sign-out").style.display="block";
  var user = result.user;
  console.log(user.email);
  //var userImage = document.querySelector("#user-image");
 
  // appending the user profile image
 
  /*var userPic = document.createElement("img");
  userPic.src=user.photoURL;
  userImage.append(userPic);*/
 
  // appending the user email address
 
  /*var userEmail = document.querySelector("#user-email");
  userEmail.innerHTML = user.displayName;*/
 
  console.log(user);
  // ...
}).catch(function(error) {
  // Handle Errors here.
  var errorCode = error.code;
  var errorMessage = error.message;
  // The email of the user's account used.
  var email = error.email;
  // The firebase.auth.AuthCredential type that was used.
  var credential = error.credential;
  // ...
});
}
</script>