{{-- <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link type="text/css" rel="stylesheet" href="css/image-uploader.min.css"> --}}
<style>
.lifeDraftLeftTitle {
  margin-top: -30px !important;
}
</style>
<div class="eventColHeading">Photo Gallery</div>
<div class="whatForm whereFrom">
  <div class="photoGalleryScroll">
    <div class="photoGallery" id="photoGallery" {{-- style="display: block;" --}}>
@if($images)
  @foreach($images as $image)
    <div class="photoGalleryOnly" id="{{ $image->_image_id }}">
      <img class="img-fluid photoGalleryImg"  src="{{ $image->image_url }}" title="{{ $image->image_name }}"/>
      <img src='new-design/img/removeField.png' class='img-fluid photoGalleryImgRemove' alt='' onclick='removeImage("{{ $image->_image_id }}");'>
    </div>
  @endforeach
@endif
      <div class="photoGalleryOnly">
        <img src="new-design/img/addGalleryImage.png" class="img-fluid photoGalleryImg toBeRemoved" alt="" onclick="chooseFile('img1');">
      </div>
    </div>
    <input type="hidden" id="image_counter" name="image_counter" value="0">
    <div id="file_uploaders" style="display: none;">
      <input type="file" name="images1" class="img_input" id="img1" accept=".jpg,.png,.jpeg" onchange="readURL(this);">
    </div>
  </div>
</div>
<div class="whatForm whatFormBtnAll">
  <div class="whatFormBtnSE">
    <input type="hidden" id="is_draft" name="is_draft" value="{{ $event_data->event_is_draft }}">
  @if($event_data->event_is_draft==1)
    <button class="btn whatFormBtn w-100" onclick="$('#is_draft').val(0);">Publish</button>
  @else
    <button class="btn whatFormBtn btnUnpublish w-100" onclick="$('#is_draft').val(1);">Unpublish</button>
  @endif
  </div>

  <div class="btn-Edit-save btn-feedback btn-premium">
    <button class="btn btn-Edit btn-save">what'scommon <br><span>upload your life event</span></button>
  </div>
</div>
<script type="text/javascript">
function chooseFile(id){
  console.log('id=> '+id);
  $("#"+id).click();
}
function removeThis(id){
  $('#'+id).remove();
}
function removeImage(id){
  $('#'+id).remove();
  var token = $('input[name="_token"]').attr('value');
  console.log(token);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token,
      'Api-Auth-Key' : '#whts_cmn_8080'
    }
  });
  var action = site_url('api/delete-image');
  var _event_id = "{{ $event_data->_event_id }}";
  var user_id = "{{Session::get('userdata')['id']}}";
  $.ajax({
  type : "POST",
  data : "user_id="+user_id+"&_event_id="+_event_id+"&_image_id="+id,
  url  : action,
  success:function(response){
    //console.log(result);
    if(response.response_code==200){
      toastr.success(response.response_msg);
      /*if(response.reponse_body.img_src){
        $('#profile_img').attr('src', response.reponse_body.img_src);
      }*/
    }
  },error: function(jqXHR, textStatus, errorThrown){
    console.log(jqXHR.responseJSON);
    toastr.error(jqXHR.responseJSON.response_msg);
    if(jqXHR.responseJSON.errors){
      var err = jqXHR.responseJSON.errors;
      $.each(err, function(key, value){
        toastr.error(err[key]);
      });
    }
  }
  });
}
var image_counter = 1;
var storedFiles = [];
var add_img = '';
var add_uploader = '';
function readURL(input) {
  if (input.files && input.files[0]) {
    var inp_size = (input.files[0].size)/(1024*1024);
    console.log('size =>'+inp_size);
    if(inp_size<=10){
      var reader = new FileReader();
      reader.readAsDataURL(input.files[0]);
      reader.onload = function (e) {
          /*$('#profile_img').attr('src', e.target.result);*/
        var this_id = input.id;
        console.log('id===> '+this_id);
        $('.toBeRemoved').parent(".photoGalleryOnly").remove();
        image_counter++;
        add_img = `<div class="photoGalleryOnly">
          <img src="new-design/img/addGalleryImage.png" class="img-fluid photoGalleryImg toBeRemoved" alt="" onclick="chooseFile('`+image_counter+`');">
        </div>`;
        add_uploader = '<input type="file" name="images'+image_counter+'" onchange="readURL(this);" class="img_input" id="'+image_counter+'" accept=".jpg,.png,.jpeg" >';
        var uploaded = "<div class=\"photoGalleryOnly\">" +
              "<img class=\"img-fluid photoGalleryImg\" src=\"" + e.target.result + "\" title=\"" + e.target.name + "\"/>" +
              "<img src='new-design/img/removeField.png' class='img-fluid photoGalleryImgRemove' alt='' onclick='removeThis("+this_id+")'>" +
              "</div>";
        $('#photoGallery').prepend(uploaded);
        $(".photoGalleryImgRemove").click(function(){
          $(this).parent(".photoGalleryOnly").remove();
        });
        $('#photoGallery').append(add_img);
        $('#file_uploaders').append(add_uploader);
        $('#image_counter').val(image_counter-1);
        
      }
    }else{
      toastr.error('Image should be less than 10MB');
    }
  }
}
function checkFieldError(){
  $(".forredline").each(function() {
    if($(this).val()==''){
      $(this).addClass('fieldError');
    }else{
      $(this).removeClass('fieldError');
    }
  });
}
$(document).ready(function(){
  checkFieldError();
});
$(document).on('change','.forredline',function(){
  checkFieldError();
});
</script>