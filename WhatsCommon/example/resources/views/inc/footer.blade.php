</div>
    </div>
  </section>




  <!-- Vendor JS Files -->
  <script src="{{ url('new-design/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ url('new-design/assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="{{ url('new-design/js/rangeslider.js') }}"></script>
  <script src="{{ url('new-design/assets/vendor/picker/js/picker.js') }}"></script>
  <script src="{{ url('js/toastr.min.js') }}"></script>
  <script src="{{ url('js/jquery.magnify.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.9.6/lottie.min.js" integrity="sha512-yAr4fN9WZH6hESbOwoFZGtSgOP+LSZbs/JeoDr02pOX4yUFfI++qC9YwIQXIGffhnzliykJtdWTV/v3PxSz8aw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script>
    var animation = bodymovin.loadAnimation({
      container: document.getElementById('loading'),
      renderer: 'svg',
      loop: true,
      autoplay: true,
      path: '{{ url("animation/whatscommon.json") }}'
    });
    $(document).ready(function(){
      //section height depends on screen resolution
      $('#loading').hide();
      $('select').change(function(){
        $(this).addClass('color-bloack');
      });
      if ($(window).width() < 951) {
    //   $('.windowHeightLeft').css('max-height',$(window).height()-30);
    //   $('.windowHeightMid').css('max-height',$(window).height()-30);
    //   $('.windowHeightRight').css('max-height',$(window).height()-30);
    //   $('.windowHeightLeft').css('height',$(window).height()-30);
    //   $('.windowHeightMid').css('height',$(window).height()-30);
    //   $('.windowHeightRight').css('height',$(window).height()-30);
        $('.windowHeightLeft').css('height',$(window).height()-130);
        $('.windowHeightMid').css('height',$(window).height()-130);
      }
      else {
        // $('.windowHeightLeft').css('max-height',$(window).height()-110);
        // $('.windowHeightMid').css('max-height',$(window).height()-110);
        // $('.windowHeightRight').css('max-height',$(window).height()-110);
        // $('.windowHeightLeft').css('height',$(window).height()-110);
        // $('.windowHeightMid').css('height',$(window).height()-110);
        // $('.windowHeightRight').css('height',$(window).height()-110);
        $('.windowHeightLeft').css('height',$(window).height()-180);
        $('.windowHeightMid').css('height',$(window).height()-180);
      }

      $(".leftSlidePan .panelslideOpenButton").click(function(e) {
        e.preventDefault();
        $(".leftSlidePan").toggleClass("closePan");
        $(this).toggleClass("panelslideCloseButton");
        $(".panelslideOpenButton").removeAttr('style');
        $(this).css('z-index', 999);
        //alert('k');
      });
      $(".rightSlidePan .panelslideOpenButton").click(function(e) {
        e.preventDefault();
        $(".rightSlidePan").toggleClass("closePan");
        $(this).toggleClass("panelslideCloseButton");
        $(".panelslideOpenButton").removeAttr('style');
        $(this).css('z-index', 999);
        /*alert('k');*/
      });

      $(".feedDismissedUndo").click(function(){
        var feed_id = $(this).data('feed_id');
        $("#feedShown-"+feed_id).slideDown("fast");
        $("#feedDismissed-"+feed_id).removeClass("showDismissedUndo");
      });
      $(".dismissButton").click(function(){
        var feed_id = $(this).data('feed_id');
        console.log('feed_id=>'+feed_id);
        $("#feedShown-"+feed_id).slideUp("fast");
        $("#feedDismissed-"+feed_id).addClass("showDismissedUndo");
      });
      $(".notificationsOn").click(function(){
        $(".NotificationsAll").toggleClass("eventsNotificationOff");
      });


      $('input[type=range]').rangeslider({
          polyfill : false
      });

      /*$(".myReverseLookup").click(function(){
        $(".mainCenterOpen").addClass("removeMainCenter");
        $(".reverseLookupOnly").removeClass("removeMainCenter");
      });
      $(".reverseLookupBack").click(function(){
        $(".reverseLookupOnly").addClass("removeMainCenter");
        $(".mainCenterOpen").removeClass("removeMainCenter");
      });*/

      $(".myReverseLookup").click(function(){
        $(".mainCenterOpen").addClass("removeMainCenter");
        $(".reverseLookupOnlyOne").removeClass("removeMainCenter");
      });
      $(".reverseLookupBackOne").click(function(){
        $(".reverseLookupOnlyOne").addClass("removeMainCenter");
        $(".mainCenterOpen").removeClass("removeMainCenter");
      });

      $(".searchReverseLookup").click(function(){
        $(".reverseLookupOnlyOne").addClass("removeMainCenter");
        $(".reverseLookupOnlyTwo").removeClass("removeMainCenter");
      });
      $(".reverseLookupBackTwo").click(function(){
        $(".reverseLookupOnlyTwo").addClass("removeMainCenter");
        $(".reverseLookupOnlyOne").removeClass("removeMainCenter");
      });
      
      $(".myLostFound").click(function(){
        $(".mainCenterOpen").addClass("removeMainCenter");
        $(".lostFoundOnly").removeClass("removeMainCenter");
      });
      $(".lostFoundBack").click(function(){
        $(".lostFoundOnly").addClass("removeMainCenter");
        $(".mainCenterOpen").removeClass("removeMainCenter");
      });

      /*$(".myDoppelganger").click(function(){
        $(".mainCenterOpen").addClass("removeMainCenter");
        $(".doppelgangerOnly").removeClass("doppelgangerClose");
      });
      $(".doppelgangerBack").click(function(){
        $(".doppelgangerOnly").addClass("doppelgangerClose");
        $(".mainCenterOpen").removeClass("removeMainCenter");
      });*/
      $(".activeFilterOn").click(function(){
        $(".myFilterAll").toggleClass("eventsFilterOff");
      });
      $(".eventsOnly").click(function(){
        $(this).toggleClass("activeEvents");
      });
      $(".datepicker").datepicker({
        format: "mm-dd-yyyy",
        autoclose: true,
      });
      $('#ex-multiselect').picker();
      $('[data-magnify]').magnify({
        headerToolbar: [
          /*'minimize',
          'maximize',*/
          'close'
        ],
        footerToolbar: [
          'prev',
          'next',
          /*'zoomIn',
          'zoomOut',
          'fullscreen',
          'actualSize',
          'rotateLeft',
          'rotateRight',
          'myCustomButton'*/
        ],
        modalWidth: 800,
        modalHeight: 400,
        finalHeight: 800,
      });

      $(document).on('change','#items',function(){
  var sizes = $(this).find(':selected').data('sizes');
  console.log('sizes => ');
  console.log(sizes);
  var item_sizes = '<option value="" hidden>Select </option>';
  if(sizes){

    $.each( sizes, function( index, size ) {
      item_sizes += '<option value="'+size.is_name+'">'+size.is_name+'</option>';
    });
  }

  $('#sizes').html(item_sizes);
});
      getMessageRequests();
    });
  </script>
<script>
    // $(document).ready(function(){
    //   $('#login').css('minHeight',$(window).height());
    // });
var lmt=10;
var offst=0;
var load_more = true;
    $(document).ready(function(){
      //section height depends on screen resolution

      $(".addOtherBank").click(function(){
        $(".financialDetailsOpen").addClass("removeBank");
        $(".financialDetailsClose").removeClass("removeBank");
      });
      $(".closeOtherBank").click(function(){
        $(".financialDetailsClose").addClass("removeBank");
        $(".financialDetailsOpen").removeClass("removeBank");
      });

      toastr.options = {
        "closeButton"       : false,
        "debug"             : false,
        "newestOnTop"       : true,
        "progressBar"       : true,
        "positionClass"     : "toast-bottom-right",
        "preventDuplicates" : false,
        "onclick"           : null,
        "showDuration"      : "300",
        "hideDuration"      : "1000",
        "timeOut"           : "5000",
        "extendedTimeOut"   : "1000",
        "showEasing"        : "swing",
        "hideEasing"        : "linear",
        "showMethod"        : "fadeIn",
        "hideMethod"        : "fadeOut"
      };
    });
  </script>
@include('inc.script')

<script>
/*var lmt=10;
var offst=0;*/
$(document).on('click', '.search_type', function(){
  var search_type = '';

  $('.search_type').each(function(){
    $(this).removeClass('activeEvents');
  });
  if($(this).attr('id')=='search_type_1'){
    search_type='life_event';
    $(this).addClass('activeEvents');
    $(".life_event_type").each(function() {
      $(this).removeClass('pointer-none');
    });
    $(".search_category").each(function() {
      $(this).removeClass('pointer-none');
    });
  }
  if($(this).attr('id')=='search_type_2'){
    search_type='global_feed';
    $(this).addClass('activeEvents');
    /*$(".life_event_type").each(function() {
      $(this).addClass('pointer-none');
      $(this).removeClass('activeEvents')
    });
    $(".search_category").each(function() {
      $(this).addClass('pointer-none');
      $(this).removeClass('activeEvents')
    });
    $('#life_event_type').val('');
    $('#search_category').val('');*/
  }
  console.log(search_type);
  $('#search_type').val(search_type);
});

$(document).on('click', '.life_event_type', function(){
  var life_event_types = [];
  console.log('clicked');
  console.log('life_event_types => '+life_event_types);
  $(".life_event_type").each(function() {

    if($(this).hasClass('activeEvents')){
      console.log('has class');
      if($(this).attr('id')=='life_event_type_all'){
        console.log('all');
        life_event_types = ['1','2','3','4','5','6','7','8','9','11'];
        //$(this).removeClass('activeEvents');
        $(".life_event_type").each(function() {
          //$(this).removeClass('activeEvents');
          if(!$(this).hasClass('activeEvents')){
            $(this).addClass('activeEvents');
          }
        });
      }else{
        //$('#life_event_type_all').removeClass('activeEvents');
        if($(this).attr('id')=='life_event_type_1'){
          life_event_types.push('1');
        }
        if($(this).attr('id')=='life_event_type_2'){
          life_event_types.push('2');
        }
        if($(this).attr('id')=='life_event_type_3'){
          life_event_types.push('3');
        }
        if($(this).attr('id')=='life_event_type_4'){
          life_event_types.push('4');
        }
        if($(this).attr('id')=='life_event_type_5'){
          life_event_types.push('5');
        }
        if($(this).attr('id')=='life_event_type_6'){
          life_event_types.push('6');
        }
        if($(this).attr('id')=='life_event_type_7'){
          life_event_types.push('7');
        }
        if($(this).attr('id')=='life_event_type_8'){
          life_event_types.push('8');
        }
        if($(this).attr('id')=='life_event_type_9'){
          life_event_types.push('9');
        }
        if($(this).attr('id')=='life_event_type_11'){
          life_event_types.push('11');
        }
      }
      
    }
  });
  life_event_type = life_event_types.join(',');
  console.log(life_event_type);
  $('#life_event_type').val(life_event_type);
});
$(document).on('click', '.search_category', function(){
  var search_categories = [];
  console.log('clicked');
  console.log('search_categories => '+search_categories);
  $(".search_category").each(function() {

    if($(this).hasClass('activeEvents')){
      console.log('has class');
      if($(this).attr('id')=='search_category_all'){
        search_categories = ['what','where','when'];
        $(".search_category").each(function() {
          if(!$(this).hasClass('activeEvents')){
            $(this).addClass('activeEvents');
          }
        });
      }else{
        if($(this).attr('id')=='search_category_what'){
          search_categories.push('what');
        }
        if($(this).attr('id')=='search_category_where'){
          search_categories.push('where');
        }
        if($(this).attr('id')=='search_category_when'){
          search_categories.push('when');
        }
      }
    }
  });
  search_category = search_categories.join(',');
  console.log(search_category);
  $('#search_category').val(search_category);
});
/*var limit=10;
var offset=0;*/
function searchFeed(search=true,type='',search_type=''){
  if(!$('#feedFilter').hasClass('eventsFilterOff')){
    console.log('not have');
    $('#feedFilter').addClass('eventsFilterOff');
  }
  var req = '';
  var user_id = '{{ Session::get("userdata")["id"] }}';
  load_more = true;
  lmt=10;
  offst=0;
  if(!$(".rightSlidePan").hasClass('closePan')){
    $(".rightSlidePan").addClass("closePan");
  }else{
    $(".rightSlidePan").removeClass("closePan");
  }
  if(!$(".rightSlidePan .panelslideOpenButton").hasClass('panelslideCloseButton')){
    $(".rightSlidePan .panelslideOpenButton").addClass("panelslideCloseButton");
  }else{
    $(".rightSlidePan .panelslideOpenButton").removeClass("panelslideCloseButton");
  }
  $(".rightSlidePan .panelslideOpenButton").css('z-index', 999);
  /*console.log('user_id=>'+user_id);*/
  if(search){
    var keywords = $('#keywords').val();
    if(search_type==''){
      var search_type = $('#search_type').val();
    }
    var search_query = $('#search_query').val();
    var life_event_type = $('#life_event_type').val();
    var search_category = $('#search_category').val();
    req = 'user_id='+user_id+'&keywords='+keywords+'&search_query='+search_query+'&life_event_type='+life_event_type+'&search_category='+search_category+'&search_from=web&search_type='+search_type;
    var feed_title = 'Search Feed';
  }else{
    if(type!='matched'){
      if(search_type==''){
        search_type = 'life_event';
      }
      req = 'user_id='+user_id+'&life_event_type='+type+'&search_from=web&search_type='+search_type;
      var feed_title = '';
      if(type==1){
        feed_title = 'Personal Feed';
      }
      if(type==2){
        feed_title = 'Dating Feed';
      }
      if(type==3){
        feed_title = 'Adoption Feed';
      }
      if(type==4){
        feed_title = 'Travel Feed';
      }
      if(type==5){
        feed_title = 'Military Feed';
      }
      if(type==6){
        feed_title = 'Education Feed';
      }
      if(type==7){
        feed_title = 'Career Feed';
      }
      if(type==8){
        feed_title = 'Pets Feed';
      }
      if(type==9){
        feed_title = 'Lost & Found Feed';
      }
      if(type==10){
        feed_title = 'Doppelganger Feed';
      }
      if(type==11){
        feed_title = 'Username Connect Feed';
      }
    }else{
      req = 'user_id='+user_id+'&search_from=web&search_type=matched';
      var feed_title = 'Match Feed';
    }
    
  }
  $('#loading').show();
  var token = $('input[name="_token"]').attr('value');
  console.log(token);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token
    }
  });
  $.ajax({
    type : "POST",
    data : req/*+'&limit='=lmt+'&offset='+offst*/,
    url  : site_url('api/feeds'),
    success : function(response){
      console.log(response);
      $('#loading').hide();
      if(response.response_code==200){
        $('#responseDiv').show();
        //toastr.success(response.response_msg);
        $('#responseDiv').html('<p class="text-success">'+response.response_msg+'</p>');
        /*$(location).attr("href", site_url());*/
        /*if(response.reponse_body.feeds){
          $('#feed_title').html(feed_title);
          var feeds_div = '';
          $.each(response.reponse_body.feeds, function(key, value){
            
            feeds_div += '';
          });
        }*/
        if(response.reponse_body.feed_div){
          $('#feed_div').html(response.reponse_body.feed_div);
          if(response.reponse_body.feed_title && response.reponse_body.feed_title!=''){
            $('#feed_title').html(response.reponse_body.feed_title);
          }else{
            $('#feed_title').html(feed_title);
          }
          $('#feed_count').html(response.reponse_body.feed_count);
          offst = parseInt(response.reponse_body.limit)+parseInt(response.reponse_body.offset);
          lmt = response.reponse_body.limit;
          console.log('offset => '+offst);
          $('#req').val(req);
          $('#offst').val(offst);
          $('#lmt').val(lmt);
          //if(offst>)
        }
        if (response.redirect && response.time) {
          window.setTimeout(function(){window.location.href = response.redirect;}, response.time);
        }
        else if(response.redirect){
          $(location).attr("href", response.redirect); 
        }
        else if(response.time){
          window.setTimeout(function(){location.reload();}, response.time);
        }
      }
    },error: function(jqXHR, textStatus, errorThrown){
      /*console.log(textStatus + ": " + jqXHR.status + " " + errorThrown);*/
      /*console.log(jqXHR);*/
      $('#loading').hide();
      console.log(jqXHR.responseJSON);
      toastr.error(jqXHR.responseJSON.response_msg);
      $('#'+submit_id+' button[type="submit"]').attr('disabled',false);
      $('#'+submit_id+' button[type="submit"]').html(submit_btn_content);
      if(jqXHR.responseJSON.errors){
        var err = jqXHR.responseJSON.errors;
        $.each(err, function(key, value){

          if (err[key]) {
            $("#"+key+"_error").html(err[key]);
          }
          else{
            $("#"+key+"_error").html(''); 
          }
        });
      }
    }
  });
}

function loadMoreFeeds(){
  load_more=false;
  let lmt = $('#lmt').val();
  let offst = $('#offst').val();
  let req = $('#req').val();
  let req2 = req+'&limit='+lmt+'&offset='+offst;
  console.log('lmt=> ', lmt);
  console.log('offst=> ', offst);
  console.log('req=> ', req);
  $('#loading').show();
  var token = $('input[name="_token"]').attr('value');
  console.log(token);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token
    }
  });
  $.ajax({
    type : "POST",
    data : req2,
    url  : site_url('api/feeds'),
    success : function(response){
      console.log(response);
      $('#loading').hide();
      if(response.response_code==200){
        $('#responseDiv').show();
        //toastr.success(response.response_msg);load_more
        $('#responseDiv').html('<p class="text-success">'+response.response_msg+'</p>');
        if(response.reponse_body.feeds && response.reponse_body.feeds.length>0){
          if(response.reponse_body.feed_div){
            load_more=true;
            $('#feed_div').append(response.reponse_body.feed_div);
            if(response.reponse_body.feed_title && response.reponse_body.feed_title!=''){
              $('#feed_title').html(response.reponse_body.feed_title);
            }else{
              $('#feed_title').html(feed_title);
            }
            $('#feed_count').html(response.reponse_body.feed_count);
            offst = parseInt(response.reponse_body.limit)+parseInt(response.reponse_body.offset);
            /*lmt = response.reponse_body.limit;*/
            console.log('offset => '+offst);
            /*$('#req').val(req);*/
            $('#offst').val(offst);
            /*$('#lmt').val(lmt);*/
          }
        }else{
          load_more=false;
        }

        if (response.redirect && response.time) {
          window.setTimeout(function(){window.location.href = response.redirect;}, response.time);
        }
        else if(response.redirect){
          $(location).attr("href", response.redirect); 
        }
        else if(response.time){
          window.setTimeout(function(){location.reload();}, response.time);
        }
      }
    },error: function(jqXHR, textStatus, errorThrown){
      /*console.log(textStatus + ": " + jqXHR.status + " " + errorThrown);*/
      /*console.log(jqXHR);*/
      $('#loading').hide();
      console.log(jqXHR.responseJSON);
      toastr.error(jqXHR.responseJSON.response_msg);
      $('#'+submit_id+' button[type="submit"]').attr('disabled',false);
      $('#'+submit_id+' button[type="submit"]').html(submit_btn_content);
      if(jqXHR.responseJSON.errors){
        var err = jqXHR.responseJSON.errors;
        $.each(err, function(key, value){

          if (err[key]) {
            $("#"+key+"_error").html(err[key]);
          }
          else{
            $("#"+key+"_error").html(''); 
          }
        });
      }
    }
  });
}

function makeDefault(form_id){
  $('#'+form_id+' input[name=is_default]').val(1);
  /*$('#'+form_id).submit();*/
}
function getMessageCounts(){
  var token = $('input[name="_token"]').attr('value');
  /*console.log(token);*/
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token
    }
  });
  $.ajax({
    type : "GET",
    url  : site_url('messages/get-message-counts'),
    success : function(response){
      /*console.log(response);*/
      if(response.response_code==200){
        if(response.reponse_body.msgCount!='0'){
          $('#msgCount').html(response.reponse_body.msgCount);
          $('#msgCount').show(); 
        }else{
          $('#msgCount').html('');
          $('#msgCount').hide();
        }
      }
    },error: function(jqXHR, textStatus, errorThrown){
      console.log(jqXHR.responseJSON);
      toastr.error(jqXHR.responseJSON.response_msg);
    }
  });
}

function getMessageRequests(){
  var token = $('input[name="_token"]').attr('value');
  /*console.log(token);*/
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token
    }
  });
  $.ajax({
    type : "POST",
    url  : site_url('messages/get-message-requests'),
    success : function(response){
      /*console.log(response);*/
      if(response.response_code==200){
        /*$('#msgCount').html(response.reponse_body.msgCount);*/
        $('#msgreqDiv').html(response.reponse_body.dialogsList);
        if(response.reponse_body.msgCount > 0){
          $('#msgreqCount').show();
          $('#msgreqCount').html(response.reponse_body.msgCount);
        }else{
          $('#msgreqCount').hide();
          $('#msgreqCount').html('');
        }
      }
    },error: function(jqXHR, textStatus, errorThrown){
      console.log(jqXHR.responseJSON);
      toastr.error(jqXHR.responseJSON.response_msg);
    }
  });
}
</script>
<script type="text/javascript">
$(document).ready(function(){
  /*$('#load_more').on('hover',function(){
    console.log('hover called');
  });*/
  $("#load_more").hover(function(){
    console.log('hover called');
    if(load_more){
      loadMoreFeeds();
    }
  });
});
</script>
<!-- <script src="https://unpkg.com/quickblox@2.13.0/quickblox.min.js"></script>-->
@if(!Request::is('messages'))
<script type="text/javascript">
  setInterval(function(){ getMessageCounts(); }, 6000);
/*  let div = document.getElementById("feed_div");
console.log('div.offsetHeight => '+div.offsetHeight);
console.log('div.scrollTop => '+div.scrollTop);
console.log('div.innerHeight => '+$('#feed_div').innerHeight());
if (div.scrollTop == $('#feed_div').innerHeight()) {
  console.log('at Bottom');
}*/

$(function(){
  $(window).scroll(function(){
    //var aTop = $('#feed_div').height();
    console.log('scroll called');
    //if($(this).scrollTop()>=aTop){
        //alert('header just passed.');
        // instead of alert you can use to show your ad
        // something like $('#footAd').slideup();
    //}
  });
});
/*$("#feed_div").scroll( function(){
   console.log('Event Fired');
});*/

/*var QB = require('node_modules/quickblox/quickblox.min');
var APPLICATION_ID = 92154;
var AUTH_KEY = "CDpYBCU4vYjG3cJ";
var AUTH_SECRET = "YsUX6uGDZJwXbZJ";
var ACCOUNT_KEY = "5CuZCzcpeweYyz1V_6zN";
var CONFIG = { debug: true };

QB.init(APPLICATION_ID, AUTH_KEY, AUTH_SECRET, ACCOUNT_KEY, CONFIG);
console.log(QB);*/
</script>
@endif

</body>

</html>