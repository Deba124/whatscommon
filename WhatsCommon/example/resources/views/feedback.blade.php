@include('inc.header')
{{-- <link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.0/css/star-rating.min.css" media="all" rel="stylesheet" type="text/css" />

with v4.1.0 Krajee SVG theme is used as default (and must be loaded as below) - include any of the other theme CSS files as mentioned below (and change the theme property of the plugin)
<link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.0/themes/krajee-svg/theme.css" media="all" rel="stylesheet" type="text/css" /> --}}
<style type="text/css">
.clear-rating{
  display: none !important;
}
/* Rating Star Widgets Style */
.rating-stars ul {
  list-style-type:none;
  padding:0;
  
  -moz-user-select:none;
  -webkit-user-select:none;
}
.rating-stars ul > li.star {
  display:inline-block;
  
}

/* Idle State of the stars */
.rating-stars ul > li.star > i.fa {
  font-size:50px; /* Change the size of the stars */
  color:#ccc; /* Color on idle state */
}

/* Hover state of the stars */
.rating-stars ul > li.star.hover > i.fa {
  color:#f2d370;
}

/* Selected state of the stars */
.rating-stars ul > li.star.selected > i.fa {
  color:#f2d370;
}
</style>
<div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                <div class="leftSlidePan {{ Request::get('menu')==1 ? '' : 'closePan'}}">
                  <a href="#" class="panelslideOpenButton {{ Request::get('menu')==1 ? 'panelslideCloseButton' : ''}}"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                  <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                    <div class="settingLeftTitle">Information</div>
                    <ul class="settingMenu mb-0 list-unstyled">
                      <li class="settingMenuLi">
                        <a class="settingMenu settingMenuActive" href="{{ url('feedback') }}"><img src="new-design/img/feedback-a.png" class="img-fluid menuIcon" alt="">Feedback</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="{{ url('contact-us') }}"><img src="new-design/img/contact.png" class="img-fluid menuIcon" alt="">Contact Us</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="{{ url('about-us') }}"><img src="new-design/img/about.png" class="img-fluid menuIcon" alt="">About Us</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="{{ url('premium-plans') }}"><img src="new-design/img/premium.png" class="img-fluid menuIcon" alt="">WC Premium</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="{{ url('donate') }}"><img src="new-design/img/donate.png" class="img-fluid menuIcon" alt="">Voluntary Gift</a>
                      </li>
                      <li class="settingMenuLi">
                          <a class="settingMenu " style="color: #ed1c24;" href="{{ url('logout') }}"><img src="{{ url('new-design/img/logout.png') }}" class="img-fluid menuIcon" alt="">Log Out</a>
                        </li>
                    </ul>
                  </div>
                  <!-- <a href="{{ url('logout') }}" class="logOut" style="margin-top: -40px;"><img src="new-design/img/logout.png" class="img-fluid menuIcon" alt=""> Log Out</a> -->
                </div>
                <div class="midPan">
                  <div class="connectionsBody infoBodyRight windowHeight windowHeightMid">
                    <div class="row infoBodyRow">
                      <div class="col-md-5 infoBodyCol">
                          <div class="innerHome">
                            <a href="{{ url('home') }}"><img src="new-design/img/home.png" class="img-fluid" alt=""></a>
                          </div>
                        <div class="feedbackImg">
                          <img src="new-design/img/feedbackImg.png" class="img-fluid" alt="">
                        </div>
                      </div>
                      <div class="col-md-7 infoBodyCol feedbackColFixed midHightControl">
                        <div class="feedbackRight windowHeight windowHeightMid">
                          <div class="feedbackTitle">Send Us <span>Feedbacks!</span></div>
                          <div class="feedbackTitle2">We love to hear from you!</div>
                          

                          {{-- <form class="feedbackForm xhr_form" method="post" action="api/feedback" id="submit-feedback"> --}}
                            <form class="feedbackForm xhr_form" method="POST" id="submit-feedback" action="api/feedback">
                            @csrf
                            <input type="hidden" name="id" value="{{Session::get('userdata')['id']}}">
                            <div class="feedbackStar">
                              {{-- <img src="new-design/img/feedbackStar.png" alt="" class="img-fluid"> --}}
                              <div class='rating-stars text-center'>
                                <ul id='stars'>
                                  <li class='star' data-value='1'>
                                    <i class='fa fa-star fa-fw'></i>
                                  </li>
                                  <li class='star' data-value='2'>
                                    <i class='fa fa-star fa-fw'></i>
                                  </li>
                                  <li class='star' data-value='3'>
                                    <i class='fa fa-star fa-fw'></i>
                                  </li>
                                  <li class='star' data-value='4'>
                                    <i class='fa fa-star fa-fw'></i>
                                  </li>
                                  <li class='star' data-value='5'>
                                    <i class='fa fa-star fa-fw'></i>
                                  </li>
                                </ul>
                              </div>
                              <p class="text-danger" id="stars_error"></p>
                            </div>
                            <input type="hidden" name="stars" id="rating">
              @if($feedbacktypes)
                            <div class="row">
                @foreach($feedbacktypes as $type)
                              <div class="col-md-6">
                                <label class="feedBackLabel">{{ $type->feed_type }}
                                  <input type="radio" name="type" value="{{ $type->feed_id }}">
                                  <span class="checkmark"></span>
                                </label>
                              </div>
                @endforeach
                              <p class="text-danger" id="type_error"></p>
                            </div>
              @endif
                            <div class="form-group">
                              <textarea class="form-control" rows="4" id="comment" name="comment" placeholder="If you love us, refer a friend! If we fell short please let us know how to do better!"></textarea>
                              <p class="text-danger" id="comment_error"></p>
                            </div>

                            <div class="btn-Edit-save btn-feedback">
                              <button class="btn btn-Edit btn-save" type="submit">Send Feedback</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@include('inc.footer')
{{-- <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.0/js/star-rating.min.js" type="text/javascript"></script>

with v4.1.0 Krajee SVG theme is used as default (and must be loaded as below) - include any of the other theme JS files as mentioned below (and change the theme property of the plugin)
<script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.0/themes/krajee-svg/theme.js"></script> --}}
<script type="text/javascript">
/*$(document).ready(function(){
  $("#input-id").rating();
});*/
$(document).ready(function(){
  
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
    /*var msg = "";
    if (ratingValue > 1) {
        msg = "Thanks! You rated this " + ratingValue + " stars.";
    }
    else {
        msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
    }*/
    $('#rating').val(ratingValue);
    
  });
  
  
});
</script>