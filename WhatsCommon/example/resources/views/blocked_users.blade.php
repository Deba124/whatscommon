@include('inc.header')
<div class="innerBodyOnly">
    <div class="container-fluid">
        <div class="settingBody">
            <div class="position-relative settingFlip">
                <div class="leftSlidePan {{ Request::get('menu')==1 ? '' : 'closePan'}}">
                	<a href="#" class="panelslideOpenButton {{ Request::get('menu')==1 ? 'panelslideCloseButton' : ''}}"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                  	<div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
	                  	<div class="settingLeftTitle">Settings</div>
	                  	<ul class="settingMenu mb-0 list-unstyled">
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu" href="{{ url('settings') }}"><img src="new-design/img/profileIcon.png" class="img-fluid menuIcon" alt="">Profile</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu" href="{{ url('change-password') }}"><img src="new-design/img/change-password.png" class="img-fluid menuIcon" alt="">Change Password</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu" href="{{ url('security') }}"><img src="new-design/img/security.png" class="img-fluid menuIcon" alt="">Security and Privacy</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu settingMenuActive" href="{{ url('blocked-users') }}"><img src="new-design/img/blocking-a.png" class="img-fluid menuIcon" alt="">Blocking</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu" href="{{ url('notification-settings') }}"><img src="new-design/img/notifications.png" class="img-fluid menuIcon" alt="">Notifications</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu" href="{{ url('link-accounts') }}"><img src="new-design/img/link-account.png" class="img-fluid menuIcon" alt="">Link Account</a>
	                  		</li>
	                  		<li class="settingMenuLi">
	                  			<a class="settingMenu" href="{{ url('message-settings') }}"><img src="new-design/img/message-setting.png" class="img-fluid menuIcon" alt="">Message Settings</a>
	                  		</li>
	                  		<li class="settingMenuLi">
		                        <a class="settingMenu" href="{{ url('feed-settings') }}"><img src="new-design/img/global-feed.png" class="img-fluid menuIcon" alt="">Feed Settings</a>
		                    </li>
	                  	</ul>
                	</div>
                </div>
                <div class="midPan">
                  	<div class="connectionsBody windowHeight windowHeightMid blockBg">
	                    <div class="bankDetailsTitle">Block Users</div>
	                    <div class="searchEvents">
	                    	@csrf
                        	<div class="input-group">
                          		<input type="text" id="search_user" class="form-control" placeholder="Type a name">
                          	</div>
	                    </div>
	                    <div class="searchEvents" id="search_results" style="margin-top: -25px;background: aliceblue;border-radius: 25px;max-height: 300px;overflow-y: auto;">
	                    	{{-- <div class="profileForm bankForm">
	                    		<ul class="linkAccountList">
                                <li>
                                    <span>
                                        <img src="new-design/img/blockDot.png" class="img-fluid" alt="">
                                        Aiony Haust
                                    </span>
                                    <div class="unblockText">Unblock</div>
                                </li>
                            </ul>
	                    	</div> --}}
	                    </div>

	                    <div class="bankcardTab">
	                      	<div class="">
		                        <div id="" class="">
		                          	<div class="profileForm bankForm">
			                            <ul class="linkAccountList" id="blocked_users_list">
			                    @if($blocked_users)
			                    	@foreach($blocked_users as $user)
			                                <li>
			                                    <span>
			                                        <img src="new-design/img/blockDot.png" class="img-fluid" alt="" >
			                                        {{ $user->firstName }} {{ $user->lastName }}
			                                    </span>
			                                    <a class="unblockText" onclick="execute('api/unblock-user','user_id={{Session::get("userdata")["id"]}}&block_user_id={{ $user->id }}');">Unblock</a>
			                                </li>
			                        @endforeach
			                    @endif
			                            </ul>
		                          	</div>
		                        </div>
	                      	</div>
	                    </div>
                  	</div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('inc.footer') 
<script type="text/javascript">
$(document).ready(function(){
	$('#search_user').on('keyup', function() {
		var search_val = this.value;
	    if(search_val.length>=3){
	    	var token = $('input[name="_token"]').attr('value');
			/*console.log(token);*/
			$.ajaxSetup({
			    headers: {
			      'X-CSRF-TOKEN': token
			    }
			});
			$.ajax({
			    type : "POST",
			    data : "user_id={{Session::get("userdata")["id"]}}&search_val="+search_val,
			    url  : site_url('api/search-user'),
			    success : function(response){
			    	console.log(response);
			    	if(response.response_code==200){
			        
			        	/*toastr.success(response.response_msg);*/
			        	$('#search_results').html(response.reponse_body.search_results);
				        /*if(response.reponse_body.search_results){
				        	
				        }*/
				        if (response.redirect && response.time) {
				        	window.setTimeout(function(){window.location.href = response.redirect;}, response.time);
				        }
				        else if(response.redirect){
				        	$(location).attr("href", response.redirect); 
				        }
				        else if(response.time){
				        	window.setTimeout(function(){location.reload();}, response.time);
				        }
			   		}
			    },error: function(jqXHR, textStatus, errorThrown){
			      	toastr.error(jqXHR.responseJSON.response_msg);
			    }
			});
	    }
	});
});
</script>