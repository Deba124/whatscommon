@include('inc.header')
<style type="text/css">
.connectionsAvtarName span{
  color: #3b71b9;
  font-size: 20px;
}
.innerHeader {
    margin-bottom: 0px;
}
</style>
<div class="messageBodyOnly">
          <div class="">
            <div class="messageAll position-relative">
              <div class="messageLeft leftSlidePan ">
                <a href="#" class="panelslideOpenButton panelslideCloseButton"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                <div class="">
                    <div class="msgHeading tabMsgPadding">
                      <div class="messageLeftTitle">Messages</div>
        
                      <div id="msgSettingsBtn" style="position: absolute;right:20px;@if(Session::get('delete_on')) display: none; @endif" class="pull-right" >
                     
                      {{-- <a href="{{ url('message-settings') }}" class="msgHeadingLink"><img style="margin-top: -15px;" src="new-design/img/msgSetting.png" class="img-fluid" alt=""></a> --}}
                      <a href="javascript:void;" class="msgHeadingLink" style="font-size: 30px;padding-top: 5px;color: #3b71b9;margin-left: 25px;" onclick="showDeleteMessages();"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                      </div>
                      
                      <div id="doneBtn" class="pull-right" style="position: absolute;right:20px;@if(!Session::get('delete_on')) display: none;@endif margin-left: 70px;" >
                        <a href="javascript:void;" style="color: #3b71b9;font-weight: 800;" class="msgHeadingLink" onclick="hideDeleteMessages();">Done</a>
                      </div>
                    </div>
                    <!-- <ul class="nav nav-tabs connerctMatched" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#connected">Connected</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#matched">Matched</a>
                      </li>
                    </ul> -->

                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div id="connected" class="tab-pane active">
                        
                        <div class="searchEvents tabMsgPadding">
                          <form action="">
                            <div class="input-group">
                              <input type="text" class="form-control" placeholder="Search" id="search_name">
                                <!-- <div class="input-group-append">
                                  <button class="btn btn-eventSearch" type="submit"><img src="new-design/img/search3.png" class="img-fluid" alt=""></button>  
                                 </div> -->
                            </div>
                          </form>
                        </div>

                        <div class="msgPersonScroll" style="position: relative;">
              
                          <ul class="nav nav-tabs msgPerson" role="tablist" id="dialogsList">
      @php
        $sender_id = Session::get('userdata')['quick_blox_id'];
      @endphp
              @if(isset($dialogs->items) && $dialogs->items)
                @foreach($dialogs->items as $dialog)

@php
        
$reciever_id = ($dialog->occupants_ids[0]==$sender_id) ? $dialog->occupants_ids[1] : $dialog->occupants_ids[0];
$reciever_data = getUserDetailsByQuickID($reciever_id);
$name = $reciever_data ? $reciever_data->firstName.' '.$reciever_data->lastName : $dialog->name;
$image = ($reciever_data && $reciever_data->profile_photo_path) ? $reciever_data->profile_photo_path : url('new-design/img/profile_placeholder.jpg');
$timing = NULL;
$user_id = $reciever_data ? $reciever_data->id : 0;
if($dialog->last_message && $dialog->last_message_date_sent){
  $timing = DateTime_Diff(date('Y-m-d H:i:s',$dialog->last_message_date_sent));
}
$rec_username = $reciever_data ? $reciever_data->username : '';
$dialog_id = $dialog->_id;
$messages = quickGetMessage($token,$dialog_id);
if($messages && $messages->items){
  $msg = FALSE;
  foreach($messages->items as $item){
    if($item->sender_id==$sender_id){
      $msg = TRUE;
    }
  }
  if(!$msg){
    continue;
  }
}else{
  continue;
}
@endphp
                            <li class="connectionsTabDetails nav-item ">
                              <a class="nav-link openMidPanButton" data-toggle="tab" onclick="getMessage('{{ $dialog->_id }}','{{ $reciever_id }}','{{ $name }}','{{ $image }}',true,'{{ $user_id }}',true,'{{ $rec_username }}');" href="javascript:void;">
                                <div class="connectionsAvtarImg">
                                  <img src="{{ $image }}" class="img-fluid" alt="">
                  @if($dialog->unread_messages_count>0)
                    <div class="msgCount">{{ $dialog->unread_messages_count }}</div>
                  @endif
                                </div>
                                <div class="connectionsAvtarDetails">
                                  <div class="connectionsAvtarName">{{ $name }}</div>
                                  <div class="connectionsAvtarId @if($dialog->unread_messages_count>0) unreadMsg @endif">{{ $dialog->last_message ? $dialog->last_message : 'attachment' }}</div>
                                  @if($timing)
                                    <div class="connectionsAvtarId connectionsAvtarMstTime">about {{$timing}} ago</div>
                                  @endif
                                </div>
                                @if(Session::get('delete_on')==TRUE)
                                <a href="javascript:void;" class="delete-chat" title="Delete chat?" style="height: 20px;    position: absolute;right: 5px;" id="{{ $dialog->_id }}" ><img src="{{ url('img/removeField.png') }}"></a>
                                @endif
                              </a>
                            </li>
                            <!-- onclick="execute('messages/delete-chat','dialog_id={{ $dialog->_id }}')" -->
                @endforeach
              @endif
                          </ul>
              
                          <div class="addPersonMsg" onclick="open_modal('create-dialog');"><img src="new-design/img/addPersonMsg.png" class="img-fluid" alt=""></div>
                        </div>
                      </div>
                      
                    </div>
                  </div>
              </div>
              <div class="messageRight midPan">
                <div class="windowHeight">
                  <div class="tab-content">
                    <div id="messageDiv" class="tab-pane ">
                      <div class="messageHeader" >
                        <div class="connectionsTabDetails">
                          <a id="messageHeader" href="#">
                          <div class="connectionsAvtarImg">
                            <img id="messageImg" src="https://newcastlebeach.org/images/human-images-2.jpg" class="img-fluid" alt="">
                          </div>
                          <div class="connectionsAvtarDetails">
                            <div class="connectionsAvtarName" id="messageName">Aiony Haust</div>
                          </div>
                        </a>
                        </div>
                      </div>
                      <div class="msgBody">
                        <div class="msgBodyContain msgBodyHeight" id="msgBodyContainer">
                        </div>

                        <div class="textTypeBox">
                          <div class="searchEvents">
                            @csrf
                            <div class="input-group">
                              <div class="input-group-append">
                                <a class="btn btn-eventSearch btn-addGallery" style="margin-top: 0px;" href="javascript:void;" onclick="$('#file_upload').click();"><img src="new-design/img/addGallery.png" class="img-fluid" alt=""></a>
                                <input type="file" class="img_input" id="file_upload" accept=".jpg,.png,.jpeg,audio/*,video/*" onchange="uploadFile();" style="display: none;">
                              </div>
                              <input type="text" id="messageTxt" name="messageTxt" class="form-control" placeholder="Type your message here">
                              <input type="hidden" name="sender_id" id="sender_id" value="{{ $sender_id }}">
                              <input type="hidden" name="reciever_id" id="reciever_id" value="0">
                              <input type="hidden" name="dialog_id" id="dialog_id" value="0">
                              <input type="hidden" id="user_id" value="0">

                              <div class="input-group-append">
                                <button class="btn btn-eventSearch btn-sendMsg" type="submit" onclick="sendMessage();"><img src="new-design/img/sendMsg.png" class="img-fluid" alt=""></button>  
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <div>
            </div>
          </div>
        </div>
      </div>
    </div>
@include('inc.footer')
<script type="text/javascript">
$(document).ready(function(){
  $('.openMidPanButton').click(function(e){
    e.preventDefault();

    /*console.log('hi');*/
    if(!$(".leftSlidePan").hasClass('closePan')){
      $(".leftSlidePan").addClass("closePan");
    }
    if(!$(".leftSlidePan .panelslideOpenButton").hasClass('panelslideCloseButton')){
      $(".leftSlidePan .panelslideOpenButton").addClass("panelslideCloseButton");
    }else{
      $(".leftSlidePan .panelslideOpenButton").removeClass("panelslideCloseButton");
    }
    /*$(".panelslideOpenButton").removeAttr('style');*/
    $(".leftSlidePan .panelslideOpenButton").css('z-index', 999);
  });
  $('.delete-chat').click(function(){
    var dialog_id = $(this).attr('id');
    if(confirm("Are you sure you want to delete this?")){
      execute('messages/delete-chat','dialog_id='+dialog_id);
    }
    else{
      return false;
    }
  });
});
function showDeleteMessages(){
  $('#msgSettingsBtn').hide();
  $('#doneBtn').show();
  $('#loading').show();
  var token = $('input[name="_token"]').attr('value');
  console.log(token);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token
    }
  });
  $.ajax({
    type : "POST",
    url  : site_url('messages/show-hide-delete-messages'),
    success : function(response){
      console.log(response);
      $('#loading').hide();
      if(response.response_code==200){
        $('#dialogsList').html(response.reponse_body.dialogsList);
        $('#msgCount').html(response.reponse_body.msgCount);
      }
    },error: function(jqXHR, textStatus, errorThrown){
      console.log(jqXHR.responseJSON);
      toastr.error(jqXHR.responseJSON.response_msg);
    }
  });
}
function hideDeleteMessages(){
  $('#msgSettingsBtn').show();
  $('#doneBtn').hide();
  var token = $('input[name="_token"]').attr('value');
  console.log(token);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token
    }
  });
  $.ajax({
    type : "POST",
    url  : site_url('messages/show-hide-delete-messages'),
    success : function(response){
      console.log(response);
      if(response.response_code==200){
        $('#dialogsList').html(response.reponse_body.dialogsList);
        $('#msgCount').html(response.reponse_body.msgCount);
      }
    },error: function(jqXHR, textStatus, errorThrown){
      console.log(jqXHR.responseJSON);
      toastr.error(jqXHR.responseJSON.response_msg);
    }
  });
}
</script>
<script>
var dial_id = $('#dialog_id').val();
var reciev_id = $('#reciever_id').val();
var user_id = $('#user_id').val();
var willScroll = true;
var refresh = true;
$('.att-player').on('click',function(){
  console.log('clicked');
});
$(document).ready(function(){
      //section height depends on screen resolution
    if ($(window).width() < 951) {
       $('.msgBodyHeight').css('height',$(window).height()-212);
       $('.messageLeft').css('height',$(window).height()-50);
    }
    else {
       $('.msgBodyHeight').css('height',$(window).height()-251);
       $('.messageLeft').css('height',$(window).height()-70);
    }
    /*$('.windowHeight').css('height',$(window).height()-70);*/
    $('.msgPersonScroll').css('height',$(window).height()-173);
    
  setInterval(function(){ refreshMessages(); }, 3000);
});
function refreshMessages(){
  dial_id = $('#dialog_id').val();
  reciev_id = $('#reciever_id').val();
  user_id = $('#user_id').val();
  search_name = $('#search_name').val().toLowerCase();
  var token = $('input[name="_token"]').attr('value');
  console.log(token);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token
    }
  });
  $.ajax({
    type : "POST",
    url  : site_url('messages/get-dialogs'),
    data : "search_name="+search_name,
    success : function(response){
      console.log(response);
      if(response.response_code==200){
        $('#dialogsList').html(response.reponse_body.dialogsList);
        $('#msgCount').html(response.reponse_body.msgCount);
        var dialogs = response.reponse_body.dialogs;
        if(dialogs.items){
          $.each( dialogs.items, function( key, dialog ) {
            console.log('dialog=> '+dialog);
            if(dialog.unread_messages_count>0 && dialog._id==dial_id){
              getMessage(dial_id,reciev_id,'','',true,user_id,true);
            }
          });
        }
      }
    },error: function(jqXHR, textStatus, errorThrown){
      console.log(jqXHR.responseJSON);
      toastr.error(jqXHR.responseJSON.response_msg);
    }
  });
  /*if($('.att-player').paused){
    refresh = true;
    console.log('true');
  }else{
    refresh = false;
    console.log('false');
  }*/
  /*if(dial_id!=0 && reciev_id!=0 && refresh==true){
    getMessage(dial_id,reciev_id,'','',false,user_id,refresh);
  }*/
}
function getMessage(dialog_id,reciever_id,reciever_name='',reciever_img='',willScroll=true,user_id=0,ref=true,rec_username=''){
  $('#dialog_id').val(dialog_id);
  $('#reciever_id').val(reciever_id);
  $('#user_id').val(user_id);
  refresh = ref;
  if(dial_id!=dialog_id){
    console.log('dial_id => '+dial_id);
    console.log('dialog_id => '+dialog_id);
    console.log('reciever_id => '+reciever_id);
    $('#loading').show();
    $('#messageTxt').val('');
  }
  
  var token = $('input[name="_token"]').attr('value');
  /*console.log(token);*/
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token
    }
  });
  $.ajax({
    type : "POST",
    data : "dialog_id="+dialog_id,
    url  : site_url('messages/get-message'),
    success : function(response){

      /*console.log(response);*/
      if(reciever_name){
        if(rec_username!=''){
          reciever_name += '<br><span>@'+rec_username+'<span>';
        }
        $('#messageName').html(reciever_name);
      }
      if(reciever_img){
        $('#messageImg').attr('src',reciever_img);
      }

      $('#messageHeader').attr('href','{{ url("connections/?user=") }}'+user_id);
      
      $('#messageDiv').addClass('active');
      
      if(response.response_code==200){
        $('#msgBodyContainer').html(response.reponse_body.msgBodyContainer);
        $('#loading').hide();
        if(willScroll && refresh==true){
          $("#msgBodyContainer").animate({ scrollTop: 5000000 }, 1000);
        }
      }
    },error: function(jqXHR, textStatus, errorThrown){
      /*console.log(jqXHR.responseJSON);*/
      toastr.error(jqXHR.responseJSON.response_msg);
    }
  });
}
function uploadFile(){
  formdata = new FormData();
  if($('#file_upload').prop('files').length > 0){
    file =$('#file_upload').prop('files')[0];
    formdata.append("file_upload", file);
  }
  formdata.append("dialog_id", $('#dialog_id').val());
  formdata.append("reciever_id", $('#reciever_id').val());
  var token = $('input[name="_token"]').attr('value');
    /*console.log(token);*/
  $('#loading').show();
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': token
    }
  });

  $.ajax({
    url: site_url('messages/upload-file'),
    type: "POST",
    data: formdata,
    processData: false,
    contentType: false,
    success: function (response) {
      $('#file_upload').val('');
      console.log(response);
      if(response.response_code==200){
        $('#loading').hide();
        $('#msgBodyContainer').html(response.reponse_body.msgBodyContainer);
        $("#msgBodyContainer").animate({ scrollTop: 5000000 }, 1000);
      }
    },error: function(jqXHR, textStatus, errorThrown){
      console.log(jqXHR);
        /*console.log(jqXHR.responseJSON);*/
        /*toastr.error(jqXHR.responseJSON.response_msg);*/
      }
  });
}

function sendMessage(){
  var dialog_id = $('#dialog_id').val();
  var reciever_id = $('#reciever_id').val();
  var sender_id = $('#sender_id').val();
  var messageTxt = $('#messageTxt').val();
  if(messageTxt!=''){
    var token = $('input[name="_token"]').attr('value');
    $('#loading').show();
    /*console.log(token);*/
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': token
      }
    });
    $('#messageTxt').val('');
    $.ajax({
      type : "POST",
      data : "dialog_id="+dialog_id+'&messageTxt='+messageTxt+'&reciever_id='+reciever_id,
      url  : site_url('messages/send-message'),
      success : function(response){
        /*console.log(response);*/
        if(response.response_code==200){
          $('#loading').hide();
          $('#msgBodyContainer').html(response.reponse_body.msgBodyContainer);
          $("#msgBodyContainer").animate({ scrollTop: 5000000 }, 1000); 
          /*getMessage(dialog_id,reciever_id,'','');*/
        }
      },error: function(jqXHR, textStatus, errorThrown){
        /*console.log(jqXHR.responseJSON);*/
        toastr.error(jqXHR.responseJSON.response_msg);
      }
    });
  }
}
function changeDate(dateVar,id){
  /*console.log('got date => '+dateVar);*/
  var date = new Date(dateVar);
  /*console.log(date.toString());*/
  $('#'+id).html(date.toLocaleTimeString());
}

/*$("#msgBodyContainer").scroll(function() {
  console.log('scrolled');
  if(dial_id==$('#dialog_id').val()){
    console.log('same');
    willScroll = false;
  }else{
    willScroll = true;
  }
});*/
$(document).on('keyup change blur','#search_name', function(){
  var search_name = $(this).val().toLowerCase();
  if(search_name.length >=3){
    refreshMessages();
  }
});
</script>
@php
if($evaluate){
@endphp
<script type="text/javascript">
getMessage('{{ $evaluate["dialog_id"] }}','{{ $evaluate["reciever_id"] }}','{{ $evaluate["name"] }}','{{ $evaluate["image"] }}',true,'{{ $evaluate["user_id"] }}',true,'{{ $evaluate["username"] }}');
</script>
@php
}
@endphp

<script type="text/javascript">
//Bind keypress event to textbox
$('#messageTxt').keypress(function(event){
  var keycode = (event.keyCode ? event.keyCode : event.which);
  if(keycode == '13'){
    //alert('You pressed a "enter" key in textbox'); 
    console.log('You pressed a "enter" key in textbox');
    sendMessage();
  }
  event.stopPropagation();
});
</script>