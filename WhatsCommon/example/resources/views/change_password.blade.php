@include('inc.header')
<div class="innerBodyOnly">
          <div class="container-fluid">
            <div class="settingBody">
              <div class="position-relative settingFlip">
                <div class="leftSlidePan {{ Request::get('menu')==1 ? '' : 'closePan'}}">
                  <a href="#" class="panelslideOpenButton {{ Request::get('menu')==1 ? 'panelslideCloseButton' : ''}}"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
                  <div class="settingBodyLeft leftPan windowHeight windowHeightLeft">
                    <div class="settingLeftTitle">Settings</div>
                    <ul class="settingMenu mb-0 list-unstyled">
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="{{ url('settings') }}"><img src="new-design/img/profileIcon.png" class="img-fluid menuIcon" alt="">Profile</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu settingMenuActive" href="{{ url('change-password') }}"><img src="new-design/img/change-password-a.png" class="img-fluid menuIcon" alt="">Change Password</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="{{ url('security') }}"><img src="new-design/img/security.png" class="img-fluid menuIcon" alt="">Security and Privacy</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="{{ url('blocked-users') }}"><img src="new-design/img/blocking.png" class="img-fluid menuIcon" alt="">Blocking</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="{{ url('notification-settings') }}"><img src="new-design/img/notifications.png" class="img-fluid menuIcon" alt="">Notifications</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="{{ url('link-accounts') }}"><img src="new-design/img/link-account.png" class="img-fluid menuIcon" alt="">Link Account</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="{{ url('message-settings') }}"><img src="new-design/img/message-setting.png" class="img-fluid menuIcon" alt="">Message Settings</a>
                      </li>
                      <li class="settingMenuLi">
                        <a class="settingMenu" href="{{ url('feed-settings') }}"><img src="new-design/img/global-feed.png" class="img-fluid menuIcon" alt="">Feed Settings</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="midPan">
                  <div class="connectionsBody windowHeight windowHeightMid passDetails">
                    <div class="bankDetailsTitle">Change Password</div>
                    <div class="bankcardTab">
                      
                      <div class="">
                        <div id="" class="">
                          <div class="profileForm bankForm">
                            <form method="post" class="xhr_form" action="api/change-password" id="change-password">
                              @csrf
                              <div class="row">
                                <div class="col-md-12">
                                  <input type="hidden" name="user_id" value="{{Session::get('userdata')['id']}}">
                                  <div class="form-group">
                                    <label for="cPassword">Current Password</label>
                                    <input type="password" class="form-control" id="cPassword" placeholder="" 
                                    name="old_password">
                                    <p class="text-danger" id="old_password_error"></p>
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label for="nPassword">New Password</label>
                                    <input type="password" class="form-control" id="nPassword" placeholder="" 
                                    name="new_password">
                                    <p class="text-danger" id="new_password_error"></p>
                                  </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                      <label for="renPassword">Re-type New Password</label>
                                      <input type="password" class="form-control" id="renPassword" placeholder=" " 
                                      name="cnf_password">
                                      <p class="text-danger" id="cnf_password_error"></p>
                                    </div>
                                </div>
                                {{-- <div class="col-md-12 text-center">
                                    <a href="forgot-password.html" class="forgotpassLink">Forgotten Password?</a>
                                </div> --}}
                              </div>
                              <div class="btn-Edit-save">
                                <button type="submit" class="btn btn-Edit btn-save" style="width: 158px;">Save Changes</button>
                              </div>
                            </form>
                          </div>
                        </div>
                        
                      </div>

                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@include('inc.footer')