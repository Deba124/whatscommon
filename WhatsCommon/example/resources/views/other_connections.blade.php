@include('inc.header')
@php
$user_id = Session::get('userdata')['id'];
@endphp
<div class="innerBodyOnly">
  <div class="container-fluid">
    <div class="connectionsBody">
      <div class="position-relative connectionFlip">
        <div class="leftSlidePan {{ Request::get('user') ? 'closePan' : ''}} windowHeightLeft" style="overflow: auto;">
          <a href="#" class="panelslideOpenButton {{ !Request::get('user') ? 'panelslideCloseButton' : ''}}"><img src="new-design/img/life_event_menu_arrow.png" alt=""></a>
          <div class="connectionsDetailsInfoName connectionTitleResponsive" style="display: block;text-align: center;margin-top: 15px;margin-bottom: 15px;"> {{ $user_data->firstName ? $user_data->firstName."'s" : "" }} Connections</div>
          <div class="connectionsLeft leftPan windowHeight ">
            <ul class="nav nav-tabs connectionsTab" role="tablist">
          @if($connections)
            @php 
              //$char = 'a';
              $connect_arr = [];
              foreach($connections as $i => $connection){
                $char = substr(strtolower($connection->firstName),0,1);
                if(isset($connect_arr[$char])){
                  array_push($connect_arr[$char],$connection);
                }else{
                  $connect_arr[$char][0] = $connection;
                }
              }
            @endphp
            @if($connect_arr && !empty($connect_arr))

              @php $i=0; @endphp

              @foreach($connect_arr as $key => $n_connections)
                <li class="alphabaticallyDetails">
                  <div class="alphabaticallyConnections">{{ $key }}</div>      
                </li>
                @if(!empty($n_connections))

                  @foreach($n_connections as $j => $connection)
                  <li class="nav-item">
                    <a class="nav-link {{ ((!Request::get('user') && $i==0) || Request::get('user')==$connection->id)? 'active' : '' }} openMidPanButton" data-toggle="tab" href="#connection{{ $connection->id }}">
                      <div class="connectionsTabDetails">
                        <div class="connectionsAvtarImg">
                          <img src="{{ $connection->profile_photo_path }}" class="img-fluid" alt="">
                        </div>
                        <div class="connectionsAvtarDetails">
                          <div class="connectionsAvtarName">{{ $connection->firstName }} {{ $connection->lastName }}</div>
                          <div class="connectionsAvtarId">{{'@'}}{{ $connection->username }}</div>
                          {{-- <div class="connectionsAvtarId"> {{ $connection->event_type }} </div> --}}
                        </div>
                        <div class="connectionsAvtarInfo">
                          <!-- <img src="new-design/img/connectionsCall.png" class="img-fluid connectionsCall" alt="">
                          <img src="new-design/img/connectionsMsg.png" class="img-fluid connectionsMsg" alt=""> -->
                        {{-- @if($connection->events_type_count && $connection->events_type_count>0)  <div class="connectionsAvtarInfoCount">{{ $connection->events_type_count }}</div>@endif --}}
                        </div>
                      </div>  
                    </a>
                  </li>
                  @php $i++; @endphp
                  @endforeach
                @endif
              @endforeach
            @endif
          @else
            <li class="nav-item">
              <a class="nav-link active openMidPanButton" data-toggle="tab" href="#connection">
                <div class="connectionsTabDetails">
                  <div class="connectionsAvtarDetails">
                    <div class="connectionsAvtarName">No Connection Available</div>
                  </div>
                </div>  
              </a>
            </li>
          @endif
            </ul>
          </div>
        </div>
        <div class="midPan windowHeight windowHeightMid">
          <div class="connectionsRight">
            <div class="tab-content">
          @if($connections)
            
              @foreach($connections as $i => $connection)
              <div id="connection{{ $connection->id }}" class="tab-pane {{ ((!Request::get('user') && $i==0) || Request::get('user')==$connection->id) ? 'active' : 'fade' }}">
                <div class="connectionsAllDetails">
                  @if($connection->created_at)<div class="memberJoin" style="position: relative;right: 0;left: 30vw;">Member since {{ format_date($connection->created_at) }}</div>@endif
                  <div class="connectionsAllDetailsTop" style="background: #FFF;padding: 10px 5px;">
                    <div class="connectionsDetailsAvtarImg">
                      <img src="{{ $connection->profile_photo_path }}" class="img-fluid" alt="">
                    </div>

                    <div class="connectionsDetailsInfo">
                      <div class="connectionsDetailsInfoName"><a style="color: inherit;" href="{{ $connection->id==$user_id ? url('my-profile') : url('user-profile?user='.$connection->id) }}" >{{ $connection->firstName }} {{ $connection->lastName }}</a></div>
                      <div class="connectionsDetailsInfoEvents myEvenntsAll">
                        <div class="myEvennts">
                          @if($connection->show_connection_list)
                          <a style="all: unset;cursor: pointer;" href="{{ $connection->id==$user_id ? url('connections') : url('other-connections/?u='.$connection->id) }}">
                            <span class="eventsNumbers"> {{ $connection->connection_count }}  </span> <span class="eventsText">Connections</span>
                          </a>
                          @else
                          <span class="eventsNumbers"> {{ $connection->connection_count }}  </span> <span class="eventsText">Connections</span>
                          @endif
                        </div>
                        <div>
                          <span class="eventsNumbers">{{ $connection->life_events_count }}</span> <span class="eventsText">Life Events</span>
                        </div>
                        <div>
                          <span class="eventsNumbers">{{ $connection->matched_count }}</span> <span class="eventsText">Event Matches</span>
                        </div>
                      </div>
                      <div class="connectionsDetailsInfoId">{{'@'}}{{ $connection->username }}</div>
                      <div class="connectionsDetailsPlace">{{ $connection->city_name }} {{ ($connection->city_name && $connection->province_name) ? ',' : '' }} {{ $connection->province_name }} {{ ($connection->country_name && $connection->province_name) ? ',' : '' }} {{ $connection->country_name }}</div>
                      @if($connection->bio)<div class="connectionsDetailsPlace connectionsDetailsUser">{{ $connection->bio }}</div>@endif
                    @if($connection->id!=$user_id)
                      <a class="btn btn-following" onclick="execute('api/follow-unfollow','user_id={{$user_id}}&followed_user={{$connection->id}}')">{{ $connection->is_following==1 ? 'Following' : 'Follow' }}</a>
                      @if($connection->quick_blox_id)
                      <a class="btn btn-following btn-message" href="{{ url('messages?qb='.$connection->quick_blox_id) }}">Message</a>
                      @endif
                      <a class="btn btn-following" onclick="execute('api/block-user','user_id={{$user_id}}&block_user_id={{ $connection->id }}')">Block</a>
                    @endif
                    </div>
                  </div>

                  <div class="connectionsAllDetailsBody">
                  @if($connection->show_match_feed)
                    @if($connection->feeds)
                    
                      @foreach($connection->feeds as $feed)
                      <div class="globalFeedList" style="padding: 25px 10px;border-bottom: 3px solid #fff;">

                      <div class="connectedInfo">
                        <div class="position-relative pr-0 feedImg">
                          <div class=""><img src="{{ $feed->user1_profile_photo ? $feed->user1_profile_photo : 'new-design/img/profile_placeholder.jpg' }}" class="img-fluid" alt=""></div>
                          <div class=""><img src="{{ $feed->user2_profile_photo ? $feed->user2_profile_photo : 'new-design/img/profile_placeholder.jpg' }}" class="img-fluid" alt=""></div>
                          <img src="new-design/img/feedadd.png" class="feedadd" alt="">
                        </div>
                        <div class="rightPanText">
                          <p>
                            @if($feed->user1_id==Session::get('userdata')['id'])
                            <a href="{{ url('my-profile') }}"> 
                              <i>You </i> 
                            </a> are {{ $feed->feed_type }} with <a href="{{ url('user-profile/?feed='.$feed->_feed_id.'&user='.$feed->user2_id) }}"> 
                              <i>{{ $feed->user2_firstName }} {{ $feed->user2_lastName }} </i> 
                            </a>

                            @elseif($feed->user2_id==Session::get('userdata')['id'])
                            <a href="{{ url('user-profile/?feed='.$feed->_feed_id.'&user='.$feed->user1_id) }}"> 
                              <i>{{ $feed->user1_firstName }} {{ $feed->user1_lastName }} </i> 
                            </a> is {{ $feed->feed_type }} with <a href="{{ url('my-profile') }}"> 
                              <i>You </i> 
                            </a>
                            @else
                            <a href="{{ url('user-profile/?feed='.$feed->_feed_id.'&user='.$feed->user1_id) }}"> 
                              <i>{{ $feed->user1_firstName }} {{ $feed->user1_lastName }} </i> 
                            </a> is {{ $feed->feed_type }} with
                            <a href="{{ url('user-profile/?feed='.$feed->_feed_id.'&user='.$feed->user2_id) }}"> 
                              <i>{{ $feed->user2_firstName }} {{ $feed->user2_lastName }} </i> 
                            </a>
                            @endif
                              
                            </p>
                          <span>About {{ DateTime_Diff($feed->feed_created) }} ago</span>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="feedInfo position-relative">
                                @if($feed->feed_what)
                                <p>
                                    <span>What:</span> 
                                    <span style="color: #91d639;">{{$feed->feed_event_type}}</span>
                                </p>
                                <p class="pl-5"> {{ $feed->feed_what }}</p>
                                @endif
                                @if($feed->feed_where)<p><span>Where:  </span> {{ $feed->feed_where }}</p>@endif
                                @if($feed->feed_when)<p><span>When: </span> {{ $feed->feed_when }}</p>@endif
                                @if($feed->feed_keywords)<p><span>W5: </span> {{ $feed->feed_keywords }}</p>@endif
                                <div class="greenCountFeed">{{ $feed->feed_match_count }}</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                @if($feed->feed_images && count($feed->feed_images)>0)
                @php

                @endphp
                        @php
                    $counter = count($feed->feed_images);
                    $remaining = 0;
                    if($counter>3){
                      $remaining = $counter-2;
                    }
                    @endphp
                        <div class="uploadPhotoHeading">Uploaded photos:</div>
                        <div class="galleryViewAll">
                                  @if($counter==1)
                                    <div class="galleryViewImg1">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-12 galleryViewCol">
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto03.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[0]->fi_img_name }}" href="{{ $feed->feed_images[0]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[0]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  @elseif($counter==2)  
                                    <div class="galleryViewImg2">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto03.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[0]->fi_img_name }}" href="{{ $feed->feed_images[0]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[0]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto01.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[1]->fi_img_name }}" href="{{ $feed->feed_images[1]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[1]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  @elseif($counter==3)     
                                    <div class="galleryViewImg3">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              {{-- <img src="img/feedfoto03.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[0]->fi_img_name }}" href="{{ $feed->feed_images[0]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[0]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto02.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[1]->fi_img_name }}" href="{{ $feed->feed_images[1]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[1]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                          <div class="galleryViewImg">
                                            {{-- <img src="img/feedfoto01.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[2]->fi_img_name }}" href="{{ $feed->feed_images[2]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[2]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  @else
                                    <div class="galleryViewImg3 galleryViewImg4">
                                      <div class="row galleryViewRow">
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              {{-- <img src="img/feedfoto03.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[0]->fi_img_name }}" href="{{ $feed->feed_images[0]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[0]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                        </div>
                                        <div class="col-md-6 galleryViewCol">
                                          <div class="galleryViewImg">
                                              {{-- <img src="img/feedfoto02.png" class="img-fluid" alt=""> --}}
                                            <a data-magnify="gallery" data-caption="{{ $feed->feed_images[1]->fi_img_name }}" href="{{ $feed->feed_images[1]->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                                              <img src="{{ $feed->feed_images[1]->fi_img_url }}" class="img-fluid" width="100%">
                                            </a>
                                          </div>
                                          <div class="galleryViewImg">
                                            <img src="new-design/img/feedfoto01.png" class="img-fluid" alt="">
                                            <div class="galleryViewImgMore">{{ $remaining }}+</div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  @endif
                                  </div>

                        {{-- <div class="feedPhoto">
                    @foreach($feed->feed_images as $img)
                      
                          <a data-magnify="gallery" data-caption="{{ $img->fi_img_name }}" href="{{ $img->fi_img_url }}" data-group="{{$feed->_feed_id}}">
                            <img src="{{ $img->fi_img_url }}" class="img-fluid" width="100%">
                          </a>
                      
                    @endforeach
                            
                        </div> --}}
                @endif
                        
                        </div>
                        <div class="col-md-12">
                        <div class="row text-center rightPanBtn" style="display: block;">
                            <a href="#" class="dismissButton" onclick="execute('api/hide-feed','user_id={{ Session::get('userdata')['id'] }}&_feed_id={{ $feed->_feed_id }}&request_from=my-profile');"><img src="new-design/img/dismissIcon.png" class="img-fluid" alt=""> Dismiss</a>
                            <!-- <a href="#" class="tagstarButton"> Tagster Social</a> -->
                        </div>
                      </div>
                      </div>
                        <div class="row">
                        

                      </div>

                        <!-- <img src="new-design/img/mapfeed.png" class="img-fluid" alt="" width="100%"> -->
                  

                    </div>
                      @endforeach
                    @endif
                  @endif

                  </div>
                </div>
              </div>
              @endforeach
          @endif
              
            </div>    
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('inc.footer')
<script type="text/javascript">
$(document).ready(function(){
  $('.openMidPanButton').click(function(e){
    e.preventDefault();
    if(!$(".leftSlidePan").hasClass('closePan')){
      $(".leftSlidePan").addClass("closePan");
    }
    if(!$(".leftSlidePan .panelslideOpenButton").hasClass('panelslideCloseButton')){
      $(".leftSlidePan .panelslideOpenButton").addClass("panelslideCloseButton");
    }else{
      $(".leftSlidePan .panelslideOpenButton").removeClass("panelslideCloseButton");
    }
    /*$(".panelslideOpenButton").removeAttr('style');*/
    $(".leftSlidePan .panelslideOpenButton").css('z-index', 999);
  });
});
    $(document).ready(function(){    
        if ($(window).width() < 951) {
        $('.windowHeightLeft').css('height',$(window).height()-153);
        $('.windowHeightMid').css('height',$(window).height()-90);
      }
      else {
        $('.windowHeightLeft').css('height',$(window).height()-150);
        $('.windowHeightMid').css('height',$(window).height()-150);
      }
    });
</script>