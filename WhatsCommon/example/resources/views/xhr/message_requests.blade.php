<style type="text/css">
.NotificationsTitle {
  font-size: 16px !important;
}
.blockBtn{
  cursor: pointer;
}
</style>
@php
  $counter = 0;
@endphp
<ul class="nav feedTab" style="justify-content: flex-start !important;margin-top: -30px;margin-bottom: 25px;">
  <li><a class="active" style="font-size: 12px;" data-toggle="tab" href="#mRequest">Message Requests</a></li>
  <li class="feedTabLine">|</li>
  <li><a data-toggle="tab" style="font-size: 12px;" href="#cRequest">Connection Requests</a></li>
</ul>
<div class="tab-content">
  <div id="mRequest" class="tab-pane fade in active">
@if(isset($dialogs->items) && $dialogs->items)
  @foreach($dialogs->items as $dialog)

@php
$sender_id = Session::get('userdata')['quick_blox_id'];
$my_id = Session::get('userdata')['id'];
$force = ($dialog->user_id==$sender_id) ? 1 : 0;
$reciever_id = ($dialog->occupants_ids[0]==$sender_id) ? $dialog->occupants_ids[1] : $dialog->occupants_ids[0];
$reciever_data = getUserDetailsByQuickID($reciever_id);
if($reciever_data && $reciever_data->is_blocked==1){
  continue;
}
$name = $reciever_data ? $reciever_data->firstName.' '.$reciever_data->lastName : $dialog->name;
$image = ($reciever_data && $reciever_data->profile_photo_path) ? $reciever_data->profile_photo_path : url('new-design/img/profile_placeholder.jpg');
$timing = NULL;
$user_id = $reciever_data ? $reciever_data->id : 0;
if($dialog->last_message && $dialog->last_message_date_sent){
  $timing = DateTime_Diff(date('Y-m-d H:i:s',$dialog->last_message_date_sent));
}
$dialog_id = $dialog->_id;
$messages = quickGetMessage($token,$dialog_id);
if($messages && $messages->items){
  $msg = FALSE;
  foreach($messages->items as $item){
    if($item->sender_id==$sender_id){
      $msg = TRUE;
    }
  }
  if($msg){
    continue;
  }
}else{
  continue;
}
if(!$dialog->last_message){
  continue;
}
$rec_username = $reciever_data ? $reciever_data->username : '';
if($search_name && strpos(strtolower($name), $search_name) === false){
  continue; 
}

$counter++;
@endphp
  <div class="notificationCard">
    <div class="notificationDetails">
      <div class="notificationCardAvtar">
        <img src="{{ $image }}" class="img-fluid" alt="">
      </div>
      <div class="notificationAvtarInfo">
        <div class="notificationAvtarName">{{ $name }}</div>
        <div class="notificationAvtarMsg">“{{ $dialog->last_message ? $dialog->last_message : '' }}”</div>
        <div class="notificationAvtarMsg">
          <span>{{$timing}} ago</span>
        </div>
      </div>
    </div>
    <div class="notificationsBtn">
      <a href="#" class="blockBtn" onclick="execute('api/block-user','user_id={{$my_id}}&block_user_id={{ $user_id }}')">Block</a>
      <a href="#" class="blockBtn replayBtn">Reply Later</a>
      <a href="{{ url('messages?qb='.$reciever_id) }}" class="blockBtn replayNow">Reply Now</a>
    </div>
  </div>
  @endforeach
@endif
  </div>
  <div id="cRequest" class="tab-pane fade">
@if($connection_requests)
  @foreach($connection_requests as $connection_request)
    @php
      $my_id = Session::get('userdata')['id'];
      $name = $connection_request->firstName.' '.$connection_request->lastName;
      $image = ($connection_request->profile_photo_path) ? $connection_request->profile_photo_path : url('new-design/img/profile_placeholder.jpg');
      $timing = DateTime_Diff(date('Y-m-d H:i:s',strtotime($connection_request->created)));
      $counter++;
    @endphp
  <div class="notificationCard">
    <div class="notificationDetails">
      <div class="notificationCardAvtar">
        <img src="{{ $image }}" class="img-fluid" alt="">
      </div>
      <div class="notificationAvtarInfo">
        <div class="notificationAvtarName">{{ $name }}</div>
        <div class="notificationAvtarMsg">Wants to connect with you</div>
        <div class="notificationAvtarMsg">
          <span>{{$timing}} ago</span>
        </div>
      </div>
    </div>
    <div class="notificationsBtn">
      
      <a class="blockBtn replayNow" onclick="execute('api/accept-reject-request','user_id={{$my_id}}&_request_id={{$connection_request->_request_id}}&request_status=1')">Accept </a>
      <a class="blockBtn" onclick="if(confirm('Are you sure you want to decline?')){execute('api/accept-reject-request','user_id={{$my_id}}&_request_id={{$connection_request->_request_id}}&request_status=2');}">Decline</a>
    </div>
  </div> 
  @endforeach
@endif
  </div>
</div>

<script type="text/javascript">
$('#msgreqCount').html("{{ $counter }}");
$('.replayBtn').on('click',function(){

  if($(this).hasClass('replayGray')==false){
    $(this).addClass('replayGray');
    $(this).attr('disabled',true);
  }
});
</script>