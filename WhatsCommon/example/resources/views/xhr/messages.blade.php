@php
	$date = '';
	$sender_id = Session::get('userdata')['quick_blox_id'];
	$sender_image = Session::get('userdata')['profile_photo_path'] ? Session::get('userdata')['profile_photo_path'] : url('new-design/img/profile_placeholder.jpg');
@endphp

@if($messages)
	@php
		$reciever_id = $messages[0]->sender_id==$sender_id ? $messages[0]->recipient_id : $messages[0]->sender_id;
		$reciever_data = getUserDetailsByQuickID($reciever_id);
		$reciever_image = ($reciever_data && $reciever_data->profile_photo_path) ? $reciever_data->profile_photo_path : url('new-design/img/profile_placeholder.jpg');
	@endphp
	@foreach($messages as $message)
		@php
			if($date != date('Y-m-d',strtotime($message->created_at))){
				$date = date('Y-m-d',strtotime($message->created_at));
				if($date==date('Y-m-d')){
					echo '<div class="msgTime">Today</div>';
				}else if($date==date('Y-m-d',strtotime("-1 days"))){
					echo '<div class="msgTime">Yesterday</div>';
				}else{
					echo '<div class="msgTime">'.format_date($date).'</div>';
				}
			}
		@endphp
		
			<div class="@if($message->sender_id==$sender_id) {{ 'chatBoxOdd' }} @else {{'chatBoxEven'}} @endif">
                <div class="chatPersonBox">
                  	<div class="connectionsTabDetails">
            @if($message->sender_id==$sender_id)
            	@if($message->message)
	                    <div class="chatTextAll">
	                      	<div class="chatText">{{ $message->message }}</div>
	                    </div>
	            @endif

	            @if($message->attachments && !empty($message->attachments))
	            	@foreach($message->attachments as $attachment)
	            	@php
            			$attach_url = 'https://api.quickblox.com/blobs/'.$attachment->id.'?token='.$token;
            			if($attachment->type=='image'){
            				echo '<a href="javascript:void" onclick="open_modal(\'show-image-from-url\',\'image_link='.$attach_url.'\');" download >';
            				echo '<img style="max-width: 150px !important;margin-left: 15px;" class="img-fluid" src="'.$attach_url.'" >';
            				echo '</a>';
            				/*echo '<a href="javascript:void" onclick="open_modal(`show-image-from-url`,`image_link='.$attach_url.'`);" download >';
            				echo '<img style="max-width: 150px !important;margin-right: 15px;" class="img-fluid" src="'.$attach_url.'">';
            				echo '</a>';*/
            			}elseif($attachment->type=='Audio'){
            				echo'<audio controls style="margin-right: 15px;" class="att-player">
									<source src="'.$attach_url.'" type="audio/ogg">
								</audio>';
            			}elseif($attachment->type=='video'){
            				echo '<video width="320" height="240" controls style="margin-right: 15px;" class="att-player">
								  <source src="'.$attach_url.'" type="video/mp4">
								</video>';
            			}
	            	@endphp
	            	@endforeach
	            @endif
	                    <div class="connectionsAvtarImg">
	                      <img src="@if($message->sender_id==$sender_id) {{ $sender_image }} @else {{$reciever_image}} @endif" class="img-fluid" alt="">
	                    </div>
	        @else
	        			<div class="connectionsAvtarImg">
	                      <img src="@if($message->sender_id==$sender_id) {{ $sender_image }} @else {{$reciever_image}} @endif" class="img-fluid" alt="">
	                    </div>
	            @if($message->message)
	                    <div class="chatTextAll">
	                      	<div class="chatText">{{ $message->message }}</div>
	                    </div>
	            @endif

	            @if($message->attachments && !empty($message->attachments))
	            	@foreach($message->attachments as $attachment)
	            		@php
            			$attach_url = 'https://api.quickblox.com/blobs/'.$attachment->id.'?token='.$token;
            			if($attachment->type=='image'){
            				echo '<a href="javascript:void" onclick="open_modal(\'show-image-from-url\',\'image_link='.$attach_url.'\');" download >';
            				echo '<img style="max-width: 150px !important;margin-left: 15px;" class="img-fluid" src="'.$attach_url.'" >';
            				echo '</a>';
            				/*echo `<a href="javascript:void" onclick="open_modal('show-image-from-url','image_link=`.$attach_url.`');" download ><img style="max-width: 150px !important;margin-right: 15px;" class="img-fluid" src="`.$attach_url.`"></a>`;*/
            			}elseif($attachment->type=='Audio'){
            				echo'<audio controls style="margin-left: 15px;" class="att-player">
									<source src="'.$attach_url.'" type="audio/ogg">
								</audio>';
            			}elseif($attachment->type=='video'){
            				echo '<video width="320" height="240" controls style="margin-left: 15px;" class="att-player">
								  <source src="'.$attach_url.'" type="video/mp4">
								</video>';
            			}
	            	@endphp
	            	@endforeach
	            @endif
	        @endif
                  	</div>
                  	<div class="msgTime sentMsgTime" id="{{ $message->_id }}"><script>changeDate('{{$message->created_at}}','{{ $message->_id }}');</script></div>
                </div>
            </div>
		
	@endforeach
@endif
<script type="text/javascript">
$('.att-player').click(function(){
  console.log('clicked');
});
</script>