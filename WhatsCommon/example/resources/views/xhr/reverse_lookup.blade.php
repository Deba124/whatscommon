<div class="addBankBackIcon reverseLookupBackTwo text-left">
  <img src="new-design/img/back.png" class="img-fluid my-0" alt="">
</div>
<div class="connectionsDetailsInfo">
    <div class="connectionsDetailsInfoName">What’sCommon</div>
    <div class="connectionsDetailsInfoEvents" style="margin-bottom: -6px;">
        <span class="eventsText orangeText mr-0">Reverse Lookup</span>
    </div>
</div>

@if($users)
  @foreach($users as $user)
  <div class="reverseLookupMatch">
        <div class="matchHolder matchHolderShadow">
        <div class="connectionsAllDetailsTop">
          <div class="connectionsDetailsAvtarImg">
            <img src=" {{ $user->profile_photo_path ? $user->profile_photo_path : url('public/new-design/img/profile_placeholder.jpg') }} " class="img-fluid" alt="">
          </div>

          <div class="connectionsDetailsInfo">
            <div class="connectionsDetailsInfoName" style="color:#000;">{{$user->firstName}} {{$user->lastName}}</div>
            <div class="connectionsDetailsInfoEvents myEvenntsAll">
              <div class="myEvennts mt-1">
                <span class="eventsText" style="font-weight: 600;">In Common:</span>
              </div>
            </div>
            <div class="reverseLookuEmail">{{$user->email}}</div>
            <div class="reverseLookuCall">{{ $user->isd_code }} {{$user->phone}}</div>
            <div class="reverseLookuDate">{{format_date2($user->dob)}}</div>
            <div class="reverseLookuPlace">{{ $user->country_name }}</div>
            <div class="reverseLookupBtn mt-3" style="display: flex;">
          
              <a class="btn btn-following" href="{{ url('user-profile/?user='.$user->id) }}">Visit</a>
            @if(!empty($user->quick_blox_id))
              <a class="btn btn-following btn-message" href="{{ url('messages/?qb='.$user->quick_blox_id) }}">Message</a>
            @endif

            </div>
          </div>

          <div class="greenCountFeed">{{$user->match_count}}</div>
        </div>
        </div>
    </div>
  @endforeach
@else
  <div class="reverseLookupMatch">
    <p class="text-center text-danger">No matches found</p>
  </div>
@endif
<script>
$(document).ready(function(){
  $(".reverseLookupBackTwo").click(function(){
    $(".reverseLookupOnlyTwo").addClass("removeMainCenter");
    $(".reverseLookupOnlyOne").removeClass("removeMainCenter");
  });
});
</script>