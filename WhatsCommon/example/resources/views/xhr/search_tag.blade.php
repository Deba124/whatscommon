<div class="matchHolder matchHolderShadow tagSearchFilter">
  <div class="tagSearchFilterAll">
  @if($country)
    <div class="tagSearchFilterOnly">
      <img src="new-design/img/formCountry.png" class="img-fluid mr-2" alt=""> {{ $country }}
      <img src="new-design/img/removeField.png" class="img-fluid tagFilterremove" alt="">
    </div>
  @endif
  @if($state)
    <div class="tagSearchFilterOnly">
      <img src="new-design/img/formState.png" class="img-fluid mr-2" alt=""> {{ $state }}
      <img src="new-design/img/removeField.png" class="img-fluid tagFilterremove" alt="">
    </div>
  @endif
  @if($tag_number)
    <div class="tagSearchFilterOnly">
      <img src="new-design/img/formTag.png" class="img-fluid mr-2" alt=""> {{ $tag_number }}
      {{-- <img src="new-design/img/removeField.png" class="img-fluid tagFilterremove" alt=""> --}}
    </div>
  @endif
  </div>
</div>

<div class="tagSearchTitle">All results ({{ count($events) }})</div>
@if($events)
@foreach($events as $event)
<div class="matchHolder matchHolderShadow">
  <div class="connectionsAllDetailsTop">
    <div class="connectionsDetailsAvtarImg">
      <img src="{{ $event->profile_photo_path ? $event->profile_photo_path : url('new-design/img/profile_placeholder.jpg') }}" class="img-fluid" alt="">
    </div>

    <div class="connectionsDetailsInfo">
      <div class="connectionsDetailsInfoName" style="color:#000;">{{ $event->firstName }} {{ $event->lastName }}</div>
      <div class="connectionsDetailsInfoEvents myEvenntsAll">
        <div class="myEvennts mt-1">
          <span class="eventsText" style="font-weight: 600;">In Common:</span>
        </div>
      </div>
    @if($country == $event->event_country)
      <div class="reverseLookuPlace">{{ $event->event_country }}</div>
    @endif
    @if($state == $event->event_state)
      <div class="reverseLookuPlace tagSearchState">{{ $event->event_state }}</div>
    @endif
      <div class="reverseLookuPlace tagSearchTag">{{ $event->event_tag_number }}</div>
      <div class="reverseLookupBtn mt-3">
        <a class="btn btn-following" href="{{ url('user-profile/?user='.$event->id) }}">Visit</a>
    @if($event->quick_blox_id)
        <a class="btn btn-following btn-message" href="{{ url('messages/?qb='.$event->quick_blox_id) }}">Message</a>
    @endif
      </div>
    </div>

    <!-- <div class="greenCountFeed">4</div> -->
  </div>
</div>
@endforeach
@else
<div class="matchHolder matchHolderShadow">
  <div class="connectionsAllDetailsTop">
    <div class="connectionsDetailsInfo">
      <div class="connectionsDetailsInfoName" style="color:#000;">No Results Found</div>
    </div>
  </div>
</div>
@endif