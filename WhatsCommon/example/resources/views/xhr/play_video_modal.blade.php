<div class="modal fade" id="play-video-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="max-width: 75vw;">
    <div class="modal-content">
      <div class="modal-header text-center" style="display: block;background: #3b71b9;">
        <h5 class="modal-title" id="exampleModalLabel">What'sCommon</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -45px;">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe id="playerID"
          width="100%" 
          height="315" 
          src="{{ $category_data->event_video_link }}" 
          frameborder="0" 
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
          allowfullscreen>
        </iframe>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$('#play-video-modal').on('hidden.bs.modal', function () {
  console.log('clicked')
  /*$('#playerID').get(0).stopVideo();*/
  $("#playerID").attr("src", $("#playerID").attr("src"));
});
</script>