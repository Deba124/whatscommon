<option value="">Select City</option>
@if($cities)
	@foreach($cities as $city)
	@if($for_life_event && $for_life_event==1)
		<option value="{{ $city->city_name }}" data-id="{{ $city->_city_id }}">{{ $city->city_name }}</option>
	@else
		<option value="{{ $city->_city_id }}">{{ $city->city_name }}</option>
	@endif
	@endforeach
@endif