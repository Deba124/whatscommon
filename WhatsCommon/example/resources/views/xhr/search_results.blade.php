@if($users)
<div class="profileForm bankForm">
	<ul class="linkAccountList">
	@foreach($users as $user)
	    <li style="border-bottom: 1px solid black;">
	        <span>
	            <img src="{{ $user->profile_photo_path ? $user->profile_photo_path : url('public/new-design/img/profile_placeholder.jpg') }}" class="img-fluid" alt="" style="height: 25px;width: 25px;border-radius: 50%;">
	            {{ $user->firstName }} {{ $user->lastName }}
	        </span>
	        <a class="unblockText" onclick="execute('api/block-user','user_id={{$user_id}}&block_user_id={{ $user->id }}')">Block</a>
	    </li>
	@endforeach
	</ul>
</div>
@endif