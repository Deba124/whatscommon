@php
  $title = $feed_data->user1_firstName.' '.$feed_data->user1_lastName.' is '.$feed_data->feed_type.' with '.$feed_data->user2_firstName.' '.$feed_data->user2_lastName.' in WhatsCommon';
  $og_url = url('feed-details?feed='.$feed_data->_feed_id);
@endphp
<!-- Modal starts -->       
<div class="modal fade" id="share-feed-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background: #fff;color: #000;justify-content: center;">
        <h5 class="modal-title" id="exampleModalLabel-3" style="margin-left: 70px;font-weight: 700;">Share with friends and family</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <p class="text-center" style="font-weight: 200;">To change link accounts, Settings > Link accounts</p>
      @if(isset($user_data->user_share_fb) && $user_data->user_share_fb==1)
        <div class="row">
          <div class="col-md-6">
            <div class="form-group p-3" style="font-weight: 600;">
              <span class="pull-left" style="margin-top: -10px;margin-right: 10px;"><img class="img-fluid" src="{{ url('new-design/img/facebook_icon1.png') }}" ></span>Facebook Post
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group p-3">
              <a class="btn btn-following btn-message" href="https://www.facebook.com/sharer/sharer.php?u={{$og_url}}&quote={{$title}}" target="new" style="margin-top: -9px;"><img src="{{ url('new-design/img/shares_ic3.png') }}"> Share</a>
            </div>
          </div>
        </div>
      @endif
      @if(isset($user_data->user_share_insta) && $user_data->user_share_insta==1)
        {{-- <div class="row">
          <div class="col-md-6">
            <div class="form-group p-3" style="font-weight: 600;">
              <span class="pull-left" style="margin-top: -10px;margin-right: 10px;"><img class="img-fluid" src="{{ url('new-design/img/instagram_icon21.png') }}" ></span>Instagram Story
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group p-3">
              <a class="btn btn-following btn-message" style="margin-top: -9px;cursor: not-allowed;"><img src="{{ url('new-design/img/shares_ic3.png') }}"> Share</a>
            </div>
          </div>
        </div> --}}
      @endif
      @if(isset($user_data->user_share_twitter) && $user_data->user_share_twitter==1)
        <div class="row">
          <div class="col-md-6">
            <div class="form-group p-3" style="font-weight: 600;">
              <span class="pull-left" style="margin-top: -10px;margin-right: 10px;"><img class="img-fluid" src="{{ url('new-design/img/twitter_icon1.png') }}" ></span>Twitter
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group p-3">
              <a class="btn btn-following btn-message" href="https://twitter.com/intent/tweet?url={{$og_url}}&text={{$title}}" target="new" style="margin-top: -9px;"><img src="{{ url('new-design/img/shares_ic3.png') }}"> Share</a>
            </div>
          </div>
        </div>
      @endif
      @if(isset($user_data->user_share_tagster) && $user_data->user_share_tagster==1)
        <div class="row">
          <div class="col-md-6">
            <div class="form-group p-3" style="font-weight: 600;">
              <span class="pull-left" style="margin-top: -10px;margin-right: 10px;"><img class="img-fluid" src="{{ url('new-design/img/tagster_icon1.png') }}" ></span>Tagster
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group p-3">
              <a class="btn btn-following btn-message" href="https://tagster.com/" target="new" style="margin-top: -9px;"><img src="{{ url('new-design/img/shares_ic3.png') }}"> Share</a>
            </div>
          </div>
        </div>
      @endif
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->