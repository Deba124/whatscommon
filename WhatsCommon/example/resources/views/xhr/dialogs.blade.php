@if(isset($dialogs->items) && $dialogs->items)
  @foreach($dialogs->items as $dialog)

@php
$sender_id = Session::get('userdata')['quick_blox_id'];
$force = ($dialog->user_id==$sender_id) ? 1 : 0;
$reciever_id = ($dialog->occupants_ids[0]==$sender_id) ? $dialog->occupants_ids[1] : $dialog->occupants_ids[0];
$reciever_data = getUserDetailsByQuickID($reciever_id);
$name = $reciever_data ? $reciever_data->firstName.' '.$reciever_data->lastName : $dialog->name;
$image = ($reciever_data && $reciever_data->profile_photo_path) ? $reciever_data->profile_photo_path : url('new-design/img/profile_placeholder.jpg');
$timing = NULL;
$user_id = $reciever_data ? $reciever_data->id : 0;
if($dialog->last_message && $dialog->last_message_date_sent){
  $timing = DateTime_Diff(date('Y-m-d H:i:s',$dialog->last_message_date_sent));
}
$dialog_id = $dialog->_id;
$messages = quickGetMessage($token,$dialog_id);
if($messages && $messages->items){
  $msg = FALSE;
  foreach($messages->items as $item){
    if($item->sender_id==$sender_id){
      $msg = TRUE;
    }
  }
  if(!$msg){
    continue;
  }
}else{
  continue;
}
$rec_username = $reciever_data ? $reciever_data->username : '';
if($search_name && strpos(strtolower($name), $search_name) === false){
  continue; 
}
@endphp
    <li class="connectionsTabDetails nav-item ">
      <a class="nav-link openMidPanButton" data-toggle="tab" onclick="getMessage('{{ $dialog->_id }}','{{ $reciever_id }}','{{ $name }}','{{ $image }}',true,'{{$user_id}}',true,'{{ $rec_username }}');" href="javascript:void;">
        <div class="connectionsAvtarImg">
          <img src="{{ $image }}" class="img-fluid" alt="">
@if($dialog->unread_messages_count>0)
<div class="msgCount">{{ $dialog->unread_messages_count }}</div>
@endif
        </div>
        <div class="connectionsAvtarDetails">
          <div class="connectionsAvtarName">{{ $name }}</div>
          <div class="connectionsAvtarId @if($dialog->unread_messages_count>0) unreadMsg @endif">
            {{ $dialog->last_message ? $dialog->last_message : 'attachment' }}
          </div>
          @if($timing)
            <span class=" connectionsAvtarId connectionsAvtarMstTime">about {{$timing}} ago</span>
          @endif
        </div>
        @if(Session::get('delete_on')==TRUE)
        <a href="javascript:void;" class="delete-chat" title="Delete chat?" style="height: 20px;position: absolute;right: 5px;" id="{{ $dialog->_id }}" ><img src="{{ url('img/removeField.png') }}"></a>
        @endif
      </a>
    </li>
  @endforeach
@endif
<script type="text/javascript">
$(document).ready(function(){
  $('.openMidPanButton').click(function(e){
    e.preventDefault();

    /*console.log('hi');*/
    if(!$(".leftSlidePan").hasClass('closePan')){
      $(".leftSlidePan").addClass("closePan");
    }
    if(!$(".leftSlidePan .panelslideOpenButton").hasClass('panelslideCloseButton')){
      $(".leftSlidePan .panelslideOpenButton").addClass("panelslideCloseButton");
    }else{
      $(".leftSlidePan .panelslideOpenButton").removeClass("panelslideCloseButton");
    }
    /*$(".panelslideOpenButton").removeAttr('style');*/
    $(".leftSlidePan .panelslideOpenButton").css('z-index', 999);
  });
});
</script>