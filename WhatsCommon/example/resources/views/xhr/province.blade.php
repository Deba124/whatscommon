<option value="" >Select State</option>
@if($provinces)
	@foreach($provinces as $province)
		@if($for_life_event && $for_life_event==1)
			<option value="{{ $province->province_name }}" data-id="{{ $province->_province_id }}">{{ $province->province_name }}</option>
		@else
			<option value="{{ $province->_province_id }}">{{ $province->province_name }}</option>
		@endif
	@endforeach
@endif