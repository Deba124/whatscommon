<div class="modal fade" id="delete-user-modal" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true"
style="margin-top: 100px;">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header modal-colored-header bg-danger">
      <h4 class="modal-title">Delete Account</h4>
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      
    </div>
    <div class="modal-body">
      <p>Your Account data will be deleted permanently from What'sCommon. This operation is not reversible. Are you sure ?</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Let me think</button>
      <button type="button" class="btn btn-danger" disabled="disabled" id="delete-btn" onclick="execute('api/delete-user', 'user_id={{ Session::get("userdata")["id"] }}');"> <span class="delete_counter" id="counter">( 10 )</span> Delete</button>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
var seconds = parseInt(10);
var interval = setInterval(function() {
  if(seconds != 0){
    seconds--;
    $('.delete_counter').html('( '+seconds+' )');
  }else{
    $('#delete-btn').attr('disabled',false);
    $('.delete_counter').html('');
  }

}, 1000);
</script>