 <!-- Modal starts -->       
<div class="modal fade" id="show-image-from-url-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document" style="margin-top: 10%;">
    <div class="modal-content" style="border-radius: 20px;padding: 0px 30px;">
      <div class="modal-header" style="background: #fff;border: none;padding-top: 20px;">
        {{-- <h5 class="modal-title" id="exampleModalLabel-3">Select Payment Mode</h5> --}}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          {{-- <span aria-hidden="true">&times;</span> --}}
          <img src="{{ url('new-design/img/close-blue_ic.png') }}" style="position: absolute;top: -15px;right: -13px;">
        </button>
      </div>
      <div class="modal-body" style="margin-top: -35px;">
        <div class="form-group text-center">
          <img src="{{ $image_link }}" class="img-fluid" >
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->