 <!-- Modal starts -->       
<div class="modal fade" id="add-master-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
@php
$name = ucwords(str_replace("_"," ","$db_name"));
$label = ucwords(str_replace("_"," ","$column_name"));
@endphp
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 20px;overflow: hidden;">
      <div class="modal-header" style="background: #497bbe;">
        <h5 class="modal-title" id="exampleModalLabel-3">Add {{ $name }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" class="form-horizontal xhr_form" id="add-master" action="api/add-master">
          @csrf
          <div class="form-group whatForm">
            <label>Enter {{ $label }} <span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="column_value" style="border:1px solid #f0f0f0;background-color:#fff !important;text-align: center;background: none; ">
            <p class="text-danger" id="column_value_error"></p>
            <input type="hidden" name="user_id" value="{{ $user_id }}">
            <input type="hidden" name="db_name" value="{{ $db_name }}">
            <input type="hidden" name="column_name" value="{{ $column_name }}">
            <input type="hidden" name="dependent_name" value="{{ $dependent_name }}">
            <input type="hidden" name="dependent_value" value="{{ $dependent_value }}">
            <input type="hidden" name="status_column" value="{{ $status_column }}">
            <input type="hidden" name="is_dependent" value="{{ $is_dependent }}">
          </div>
          
          <div class="form-group whatForm text-center">
            <button type="submit" class="btn btn-success" style="width: 125px;"> Submit</button>
            <button type="button" class="btn btn-info" data-dismiss="modal" style="width: 125px;margin-left: 15px;">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->