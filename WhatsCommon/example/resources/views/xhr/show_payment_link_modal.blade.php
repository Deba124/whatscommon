 <!-- Modal starts -->       
<div class="modal fade" id="show-payment-link-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document" style="margin-top: 10%;">
    <div class="modal-content" style="border-radius: 20px;padding: 0px 30px;">
      <div class="modal-header" style="background: #fff;border: none;padding-top: 20px;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <img src="{{ url('new-design/img/close-blue_ic.png') }}" style="position: absolute;top: -15px;right: -13px;">
        </button>
      </div>
      <div class="modal-body" style="margin-top: -35px;">
        <form id="payment-form">
          <div id="payment-element">
            <!-- Elements will create form elements here -->
          </div>
          <button id="submit">Subscribe</button>
          <div><p id="error-message" class="text-danger"></p>
            <!-- Display error message to your customers here -->
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->

<script>
$(document).ready(function(){

const options2 = {
  clientSecret: '{{ $clientSecret }}',
  // Fully customizable with appearance API.
  appearance: {/*...*/},
};

// Set up Stripe.js and Elements to use in checkout form, passing the client secret obtained in step 5
const elements = strpp.elements(options2);

// Create and mount the Payment Element
const paymentElement = elements.create('payment');
paymentElement.mount('#payment-element');
const form = document.getElementById('payment-form');

form.addEventListener('submit', async (event) => {
  event.preventDefault();

  const {error} = await strpp.confirmPayment({
    //`Elements` instance that was used to create the Payment Element
    elements,
    confirmParams: {
      return_url: "{{ url('payment-success') }}",
    }
  });

  if (error) {
    // This point will only be reached if there is an immediate error when
    // confirming the payment. Show error to your customer (for example, payment
    // details incomplete)
    const messageContainer = document.querySelector('#error-message');
    messageContainer.textContent = error.message;
  } else {
    // Your customer will be redirected to your `return_url`. For some payment
    // methods like iDEAL, your customer will be redirected to an intermediate
    // site first to authorize the payment, then redirected to the `return_url`.
  }
});
});

</script>