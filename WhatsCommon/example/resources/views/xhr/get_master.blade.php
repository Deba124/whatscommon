@if($db_name=='vehicle_model')
<label for="">Model</label>
<select class="form-control formModel" name="vehicle_model" id="vehicle_model">
    <option value="" hidden="">Select Model</option>
    @foreach($vehicle_model as $model)
    @php
    	$selected = (isset($inserted_id) && $inserted_id==$model->_model_id) ? 'selected' : '';
    @endphp 
    <option value="{{ $model->model_name }}" {{ $selected }} data-id="{{ $model->_model_id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{ $model->model_name }}</option>
    @endforeach
    <option value="add">Add</option>
</select>
<img src="new-design/img/selectIcon.png" class="img-fluid selectIcon" alt="">
@elseif($db_name=='games')
	<select class="form-control selectCenter formGame " id="game_name" name="game_name">
    	<option value="" hidden="">Select Game or Activity</option>
   	@if($games)
      	@foreach($games as $game)
	      	@php
		    	$selected = (isset($inserted_id) && $inserted_id==$game->_game_id) ? 'selected' : '';
		    @endphp
        <option option="{{ $game->game_name }}" data-game_teams="{{ $game->game_teams }}" {{ $selected }} data-id="{{ $game->_game_id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{$game->game_name}}</option>
      	@endforeach
    @endif
        <option value="add">Add</option>
    </select>
@elseif($db_name=='game_teams')
	<select class="form-control selectCenter formTeam " id="team_name" name="team_name">
        <option value="" hidden="">Select Team/Group</option>
        @if($game_teams)
      	@foreach($game_teams as $team)
	      	@php
		    	$selected = (isset($inserted_id) && $inserted_id==$team->_team_id) ? 'selected' : '';
		    @endphp
        <option option="{{ $team->team_name }}" {{ $selected }} data-id="{{ $team->_team_id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{$team->team_name}}</option>
      	@endforeach
    @endif
        <option value="add">Add</option>
    </select>
@endif

@if($db_name=='genders')
  <label for="">Gender</label>
  <select class="form-control formGender" name="gender" id="gender">
    <option value="" hidden>Select</option>
  @if($genders)
    @foreach($genders as $gender)
      @php
        $selected = (isset($inserted_id) && $inserted_id==$gender->_gender_id) ? 'selected' : '';
      @endphp
    <option option="{{ $gender->gender_name }}" {{ $selected }} data-id="{{ $gender->_gender_id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{$gender->gender_name}}</option>
    @endforeach
  @endif
    <option value="add">Add</option>
  </select>
@endif

@if($db_name=='place_of_birth')
  <label for="">Place of Birth</label>
  <select class="form-control formBarthPlace" name="birth_place" id="place_of_birth">
    <option value="" hidden>Select</option>
  @if($place_of_birth)
    @foreach($place_of_birth as $place)
      @php
        $selected = (isset($inserted_id) && $inserted_id==$place->_place_id) ? 'selected' : '';
      @endphp
    <option option="{{ $place->place_name }}" {{ $selected }} data-id="{{ $place->_place_id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{$place->place_name}}</option>
    @endforeach
  @endif
    <option value="add">Add</option>
  </select>
@endif

@if($db_name=='adoption_status')
  <label for="">Status</label>
  <select class="form-control formBarthPlace" name="adoption_status" id="adoption_status">
    <option value="" hidden>Select</option>
  @if($adoption_status)
    @foreach($adoption_status as $status)
      @php
        $selected = (isset($inserted_id) && $inserted_id==$status->_status_id) ? 'selected' : '';
      @endphp
    <option option="{{ $status->status_name }}" {{ $selected }} data-id="{{ $status->_status_id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{$status->status_name}}</option>
    @endforeach
  @endif
    <option value="add">Add</option>
  </select>
@endif

@if($db_name=='home_types')
  <label for="">Type of Home</label>
  <select class="form-control selectCenter" name="type_of_home" id="home_types">
    <option value="" hidden>Select</option>
  @if($home_types)
    @foreach($home_types as $home_type)
      @php
        $selected = (isset($inserted_id) && $inserted_id==$home_type->id) ? 'selected' : '';
      @endphp
    <option option="{{ $home_type->home_type_name }}" {{ $selected }} data-id="{{ $home_type->id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{$home_type->home_type_name}}</option>
    @endforeach
  @endif
    <option value="add">Add</option>
  </select>
@endif

@if($db_name=='home_styles')
  <label for="">Style</label>
  <select class="form-control selectCenter" name="home_style" id="home_styles">
    <option value="" >Select</option>
  @if($home_styles)
    @foreach($home_styles as $home_style)
      @php
        $selected = (isset($inserted_id) && $inserted_id==$home_style->id) ? 'selected' : '';
      @endphp
    <option option="{{ $home_style->home_style_name }}" {{ $selected }} data-id="{{ $home_style->id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{$home_style->home_style_name}}</option>
    @endforeach
  @endif
    <option value="add">Add</option>
  </select>
@endif

@if($db_name=='vehicle_makers')
  <label for="">Make</label>
  <select class="form-control formMake" id="vehicle_maker" name="vehicle_maker">
    <option value="" >Select</option>
  @if($vehicle_makers)
    @foreach($vehicle_makers as $maker)
    <option option="{{ $maker->maker_name }}" data-id="{{ $maker->_maker_id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{$maker->maker_name}}</option>
    @endforeach
  @endif
    <option value="add">Add</option>
  </select>
@endif

@if($db_name=='occupations')
  <label for="">Category</label>
  <select class="form-control formCategory" id="category" name="category">
    <option value="">Select</option>
  @if($occupations)
    @foreach($occupations as $occupation)
    <option option="{{ $occupation->occupation_name }}" data-id="{{ $occupation->_occupation_id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{$occupation->occupation_name}}</option>
    @endforeach
  @endif
    <option value="add">Add</option>
  </select>
@endif

@if($db_name=='industries')
  <label for="">Industry</label>
  <select class="form-control formIndustry" id="industry" name="industry">
    <option value="" >Select</option>
  @if($industries)
    @foreach($industries as $industry)
    <option option="{{ $industry->industry_name }}" data-id="{{ $industry->_industry_id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{$industry->industry_name}}</option>
    @endforeach
  @endif
    <option value="add">Add</option>
  </select>
@endif

@if($db_name=='marital_status')
  <label for="">Status</label>
  <select class="form-control formStatus" id="marital_status" name="marital_status">
    <option value="" >Select</option>
  @if($marital_status)
    @foreach($marital_status as $industry)
    <option option="{{ $industry->ms_name }}" data-id="{{ $industry->_ms_id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{$industry->ms_name}}</option>
    @endforeach
  @endif
    <option value="add">Add</option>
  </select>
@endif

@if($db_name=='relations')
  <label for="">Relation</label>
  <select class="form-control formRelationship" id="relations" name="relationship">
    <option value="" >Select</option>
  @if($relations)
    @foreach($relations as $industry)
    <option option="{{ $industry->relation_name }}" data-id="{{ $industry->_relation_id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{$industry->relation_name}}</option>
    @endforeach
  @endif
    <option value="add">Add</option>
  </select>
@endif

@if($db_name=='ethnicities')
  <label for="">Ethnicity</label>
  <select class="form-control formEthnicity" id="ethnicity" name="ethnicity">
    <option value="" hidden>Select</option>
  @if($ethnicities)
    @foreach($ethnicities as $ethnicity)
    <option option="{{ $ethnicity->ethnicity_name }}" data-id="{{ $ethnicity->_ethnicity_id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{$ethnicity->ethnicity_name}}</option>
    @endforeach
  @endif
    <option value="add">Add</option>
  </select>
@endif

@if($db_name=='races')
  <label for="">Race</label>
  <select class="form-control formRace" name="race" id="race">
    <option value="" hidden>Select</option>
  @if($races)
    @foreach($races as $race)
    <option option="{{ $race->race_name }}" data-id="{{ $race->_race_id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{$race->race_name}}</option>
    @endforeach
  @endif
    <option value="add">Add</option>
  </select>
@endif

@if($db_name=='travel_modes')
  <label for="">Mode</label>
  <select class="form-control formAirport " id="travel_modes" name="airport">
    <option value="" hidden>Select</option>
  @if($travel_modes)
    @foreach($travel_modes as $mode)
    <option option="{{ $mode->mode_name }}" data-id="{{ $mode->_mode_id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{$mode->mode_name}}</option>
    @endforeach
  @endif
    <option value="add">Add</option>
  </select>
@endif

@if($db_name=='location_types')
  <label for="">Location Type</label>
  <select class="form-control formLocationType" id="location_type" name="location_type">
    <option value="" hidden>Select</option>
  @if($location_types)
    @foreach($location_types as $lt)
    <option option="{{ $lt->lt_name }}" data-id="{{ $lt->_lt_id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{$lt->lt_name}}</option>
    @endforeach
  @endif
    <option value="add">Add</option>
  </select>
@endif

@if($db_name=='hair_colors')
  <label for="">Hair Color</label>
  <select class="form-control formHairColor" id="hair_color" name="hair_color">
    <option value="" hidden>Select</option>
  @if($hair_colors)
    @foreach($hair_colors as $hc)
    <option option="{{ $hc->hc_name }}" data-id="{{ $hc->_hc_id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{$hc->hc_name}}</option>
    @endforeach
  @endif
    <option value="add">Add</option>
  </select>
@endif

@if($db_name=='eye_colors')
  <label for="">Eye Color</label>
  <select class="form-control formEyeColor" name="eye_color" id="eye_color">
    <option value="" hidden>Select</option>
  @if($eye_colors)
    @foreach($eye_colors as $ec)
    <option option="{{ $ec->ec_name }}" data-id="{{ $ec->_ec_id }}" data-ins="{{ (isset($inserted_id) && $inserted_id) ? $inserted_id : 'not found' }}">{{$ec->ec_name}}</option>
    @endforeach
  @endif
    <option value="add">Add</option>
  </select>
@endif