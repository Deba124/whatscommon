 <!-- Modal starts -->       
<div class="modal fade" id="donate-success-modal"  data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-3" aria-hidden="true">
  <div class="modal-dialog" role="document" style="margin-top: 10%;">
    <div class="modal-content" style="border-radius: 20px;padding: 0px 30px;">
      <div class="modal-header" style="background: #fff;border: none;padding-top: 20px;">
        {{-- <h5 class="modal-title" id="exampleModalLabel-3">Select Payment Mode</h5> --}}
        {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <img src="{{ url('new-design/img/close-blue_ic.png') }}" style="position: absolute;top: -15px;right: -13px;">
        </button> --}}
      </div>
      <div class="modal-body" style="margin-top: -35px;">
        <div class="form-group text-center">
          {{-- <div class="btn-Edit-save">
            <button class="btn btn-Edit btn-Plan btn-block" style="height: 45px;border-radius: 23px;background: #0070ba;width: 100%;font-weight: 500;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;" 
              onclick="execute('create-squareup-payment','amount={{$amount}}&payment_for={{$payment_for}}&redirect_url={{$redirect_url}}')">Pay with <span style="font-style: italic;font-weight: 800;font-size: 20px;">Square</span></button>
          </div> --}}
          <img src="{{ url('new-design/img/thankyou_ic.png') }}" class="img-fluid" >
        </div>
        <div class="form-group text-center" style="margin-top: -25px;">
          <span style="color: #0070ba;font-size: 35px;font-weight: 800;margin-left: 10px;">{!! $currency_data->currency_symbol !!}{{ $amount }}</span>
          <p style="font-size: 18px;"><b>Thank you so much</b>, your donation was successfully submitted.</p>
        </div>
        <div class="form-group text-center" >
          <a class="btn btn-Edit btn-Plan btn-block" style="height: 45px;border-radius: 10px;background: #0070ba;width: 100%;font-weight: 500;font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color: #fff;text-transform: uppercase;padding-top: 22px;" href="{{ url('donate') }}">Ok</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->