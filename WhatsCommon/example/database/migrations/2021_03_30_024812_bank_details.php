<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BankDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_details', function (Blueprint $table) {
            $table->id();
            $table->string('bank_type');
            $table->string('bank_email')->nullable();
            $table->string('bank_password')->nullable();
            $table->string('bank_ac_or_card_no')->nullable();
            $table->string('bank_card_holder_name')->nullable();
            $table->string('bank_card_expiry')->nullable();
            $table->string('bank_card_cvv')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_routing_no')->nullable();
            $table->integer('bank_user_id');
            $table->integer('bank_is_default')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_details');
    }
}
