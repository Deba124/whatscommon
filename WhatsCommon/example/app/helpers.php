<?php
use Illuminate\Support\Facades\DB;
date_default_timezone_set('UTC');

if ( ! function_exists('onlyTime')) {
  function onlyTime($date){
      return date('g:i a', strtotime($date));
  }
}

if ( ! function_exists('formatDate')) {
	function formatDate($date){
  		return date('F j, Y, g:i a', strtotime($date));
 	}
}

if ( ! function_exists('format_date')) {
	function format_date($date){
		if (!empty($date) && $date!=NULL && $date!='0000-00-00') {
			return date('d M Y', strtotime($date));
		}
  		return false;
 	}
}

if ( ! function_exists('format_date2')) {
  function format_date2($date){
    if (!empty($date) && $date!=NULL && $date!='0000-00-00') {
      return date('M d, Y', strtotime($date));
    }
      return false;
  }
}

if ( ! function_exists('day_and_date')) {
	function day_and_date($date){
		if (!empty($date) && $date!=NULL && $date!='0000-00-00') {
			return date('l, jS F, Y', strtotime($date));
		}
  		return false;
 	}
}

if ( ! function_exists('db_date_format')) {
	function db_date_format($date){

		return (!empty($date)) ? date('Y-m-d', strtotime($date)) : NULL;
	}	
}

if ( ! function_exists('web_date_format')) {
	function web_date_format($date){

		return (!empty($date)) ? date('m-d-Y', strtotime($date)) : NULL;
	}	

}

if ( ! function_exists('seconds_diff')) {
	function seconds_diff($date1, $date2){
  		//date_default_timezone_set("Asia/Kolkata");

  		$date1 = strtotime($date1);
  		$date2 = strtotime($date2);

  		return $seconds_diff = round(abs($date2 - $date1));
 	}
}


if ( ! function_exists('DateTime_Diff')) {
	function DateTime_Diff($occurrence_Date){

    	$current_Date    = strtotime(date("Y-m-d H:i:s"));

    	$occurrence_Date = strtotime($occurrence_Date);

    	$datetime_diff = round(abs($current_Date - $occurrence_Date));

        if ($datetime_diff <= 60) {
           
            $time = $datetime_diff." Seconds";
        }
        elseif ($datetime_diff > 60 && $datetime_diff <= 3600) {
           
            $time = round($datetime_diff/60)." mins";
        }
        elseif ($datetime_diff > 3600 && $datetime_diff <= 86400) {
            $time = round($datetime_diff/3600)." hours";
        }
        elseif ($datetime_diff > 86400 && $datetime_diff <= 86400*13) {
            $time = round($datetime_diff/86400)." days";
        }
        elseif ($datetime_diff > 86400*13 && $datetime_diff <= 86400*31) {
            $time = round($datetime_diff/(86400*7))." week";
        }
        elseif ($datetime_diff > 86400*31 && $datetime_diff <= 86400*365) {
            $time = round($datetime_diff/(86400*30))." month";
        }
        elseif ($datetime_diff > 86400*365) {
            $time = round($datetime_diff/(86400*365))." year";
        }

        return $time;
    }
}

if ( ! function_exists('DateTime_Diff2')) {
  function DateTime_Diff2($occurrence_Date){

    $current_Date    = strtotime(date("Y-m-d H:i:s"));
    $occurrence_Date = strtotime($occurrence_Date);
    $datetime_diff = round(abs($current_Date - $occurrence_Date));

    //return $datetime_diff;

    if (date("Y-m-d")==date("Y-m-d",$occurrence_Date)) {
      $time = "Today";
    }
    elseif (date("Y-m-d", strtotime("yesterday")) == date("Y-m-d",$occurrence_Date)) {
      $time = "Yesterday";
    }else{
      $time = date('m-d-Y', $occurrence_Date);
    }

    return $time;
  }
}

DEFINE('APPLICATION_ID', '95998'); // Enter your application ID (zabingo)---> 92154
DEFINE('AUTH_KEY', "3vbh2KHhhVGuYWg"); // Enter yur auth key (zabingo)---> CDpYBCU4vYjG3cJ
DEFINE('AUTH_SECRET', "C8TtrgjdBfB39Ff"); // Enter your secret key (zabingo)---> YsUX6uGDZJwXbZJ
DEFINE('QB_API_ENDPOINT', "https://api.quickblox.com/"); // API Endpoints URL 
// User credentials
DEFINE('USER_LOGIN', "appigator@appigators.com"); // Admin Username (zabingo)---> developments.zabingo@gmail.com
DEFINE('USER_PASSWORD', "12345678"); // Admin Password (zabingo)---> 12345678

function qb_login($user_email=NULL,$user_password=NULL){

  if(!$user_email){
    $user_email = USER_LOGIN;
  }
  if(!$user_password){
    $user_password = USER_PASSWORD;
  }
  $nonce = rand();
  $timestamp = time();
  $signature_string = "application_id=" . APPLICATION_ID . "&auth_key=" . AUTH_KEY . "&nonce=" . $nonce . "&timestamp=" . $timestamp . "&user[login]=" . $user_email . "&user[password]=" . $user_password;

  $signature = hash_hmac('sha1', $signature_string, AUTH_SECRET);

  // Build post body
  $post_body = http_build_query(array(
      'application_id' => APPLICATION_ID,
      'auth_key' => AUTH_KEY,
      'timestamp' => $timestamp,
      'nonce' => $nonce,
      'signature' => $signature,
      'user[login]' => $user_email,
      'user[password]' => $user_password
  ));

  // Configure cURL
  $curl = curl_init();
  /*curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'/');*/
  $QB_PATH_SESSION = 'session.json';
  curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT . '/' . $QB_PATH_SESSION);
  curl_setopt($curl, CURLOPT_POST, true); // Use POST
  curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body); // Setup post body
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Receive server response
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // Receive server response
  // Execute request and read response
  $response = curl_exec($curl);
  // Close connection
  curl_close($curl);
  // Check errors
  if ($response) {
    return json_decode($response);
  } else {
    return false;
  }
}

function getUserByEmail($token,$email){
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users/by_email.json?email='.$email);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'QuickBlox-REST-API-Version: 0.1.0',
      'QB-Token: ' . $token
  ));
  $response = curl_exec($curl);
  curl_close($curl);
  if ($response) {
    return json_decode($response);
  } else {
    return false;
  }
}

function quickAddUsers($token,$username,$password,$email,$name=NULL) {
  $post_body = http_build_query(array(
      'user[login]' => $username,
      'user[password]' => $password,
      'user[email]' => $email,
      'user[full_name]' => $name
  ));
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users.json');
  curl_setopt($curl, CURLOPT_POST, true);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Accept: application/json',
      'Content-Type: application/x-www-form-urlencoded',
      'QuickBlox-REST-API-Version: 0.1.0',
      'QB-Token: ' . $token
  ));
  $response = curl_exec($curl);
  curl_close($curl);
  if ($response) {
    return json_decode($response);
  } else {
    return false;
  }
}

function qbDeleteUser($token,$user_id){
  $curl = curl_init();
  
  curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users/'.$user_id.'.json');
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
  
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'QuickBlox-REST-API-Version: 0.1.0',
      'QB-Token: ' . $token
  ));
  $response = curl_exec($curl);
  $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
  curl_close($curl);
  if ($response) {
    return [json_decode($response),$httpcode];
  } else {
    return false;
  }
  /*curl_close($curl);
  if ($response) {
    return json_decode($response);
  } else {
    return false;
  }*/
}

function quickGetDialog($token) {
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Dialog.json');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'QuickBlox-REST-API-Version: 0.1.0',
      'QB-Token: ' . $token
  ));
  $response = curl_exec($curl);
  curl_close($curl);
  if ($response) {
    return json_decode($response);
  } else {
    return false;
  }
  
}

function quickGetMessage($token, $dialogId) {
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Message.json?chat_dialog_id=' . $dialogId.'&sort_desc=_id');//&limit=30

  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'QuickBlox-REST-API-Version: 0.1.0',
      'QB-Token: ' . $token
  ));
  $response = curl_exec($curl);
  curl_close($curl);
  if ($response) {
    return json_decode($response);
  } else {
    return false;
  }
  
}

function quickSendMessage($token,$dialogId,$message='',$attachments = array()) {
  $post_body = http_build_query(array(
                                        'chat_dialog_id'  =>  $dialogId,
                                        'message'         =>  $message,
                                        'send_to_chat'    =>  1,
                                        'markable'        =>  1,
                                        'attachments'     =>  $attachments
                                    ));
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Message.json');
  curl_setopt($curl, CURLOPT_POST, true);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Accept: application/json',
      'Content-Type: application/x-www-form-urlencoded',
      'QuickBlox-REST-API-Version: 0.1.0',
      'QB-Token: ' . $token
  ));
  $response = curl_exec($curl);
  $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
  curl_close($curl);
  if ($response) {
    return [json_decode($response),$httpcode];
  } else {
    return false;
  }
}

function quickCreateEvent($token,$user_ids,$message){
  /*$post_body = "{"event":{"notification_type":"push","environment":"production","event_type":"one_shot","message":".$message.","user":{"ids*":".$user_ids."}}}";*/
  $post_body =  [
                  "event" => [
                    "notification_type" =>  "push",
                    "environment"       =>  "development",
                    "event_type"        =>  "one_shot",
                    "message"           =>  "payload=$message",
                    "payload"              =>  "payload=$message",
                    "user"  =>[
                      "ids"            =>  "$user_ids"
                    ],
                    /*"push_type"         =>  "gcm",*/
                  ]
                ];
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'events.json');
  curl_setopt($curl, CURLOPT_POST, true);
  curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($post_body));
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Accept: application/json',
      'Content-Type: application/json',
      /*'QuickBlox-REST-API-Version: 0.1.0',*/
      'QB-Token: ' . $token
  ));
  $response = curl_exec($curl);
  $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
  curl_close($curl);
  if ($response) {
    return [json_decode($response),$httpcode];
  } else {
    return false;
  }
}

function quickCreateDialog($token,$occupants_ids) {
  $post_body = http_build_query(array(
                                        'type'          => 3,
                                        'occupants_ids' => $occupants_ids
                                    ));
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Dialog.json');
  curl_setopt($curl, CURLOPT_POST, true);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Accept: application/json',
      'Content-Type: application/x-www-form-urlencoded',
      'QuickBlox-REST-API-Version: 0.1.0',
      'QB-Token: ' . $token
  ));
  $response = curl_exec($curl);
  curl_close($curl);
  if ($response) {
    return json_decode($response);
  } else {
    return false;
  }
}

function qbDeleteDialog($token,$dialog_id,$force=0){
  $curl = curl_init();
  
  curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Dialog/'.$dialog_id.'.json');
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
  if($force==1){
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, ['force' => 1]);
  }
  
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'QuickBlox-REST-API-Version: 0.1.0',
      'QB-Token: ' . $token
  ));
  $response = curl_exec($curl);
  curl_close($curl);
  if ($response) {
    return json_decode($response);
  } else {
    return false;
  }
}

function getUnreadMessageCount($token,$dialogIds=NULL){
  $curl = curl_init();
  $dial_param = $dialogIds ? '?chat_dialog_ids='.$dialogIds : '';
  curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Message/unread.json'.$dial_param);

  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'QuickBlox-REST-API-Version: 0.1.0',
      'QB-Token: ' . $token
  ));
  $response = curl_exec($curl);
  curl_close($curl);
  if ($response) {
    return json_decode($response);
  } else {
    return false;
  }
}

function getAttachmentURLByID($token,$attachment_id){
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'blobs/'.$attachment_id.'/download.json');

  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'QuickBlox-REST-API-Version: 0.1.0',
      'QB-Token: ' . $token
  ));
  $response = curl_exec($curl);
  curl_close($curl);
  if ($response) {
    return json_decode($response);
  } else {
    return false;
  }
}

function createFile($token,$file_name,$file_type){
  $post_body = http_build_query(array(
      'blob[content_type]' => $file_type,
      'blob[name]' => $file_name/*,
      'blob[public]' => true*/
  ));
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'blobs.json');
  curl_setopt($curl, CURLOPT_POST, true);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Accept: application/json',
      'Content-Type: application/x-www-form-urlencoded',
      'QuickBlox-REST-API-Version: 0.1.0',
      'QB-Token: ' . $token
  ));
  $response = curl_exec($curl);
  curl_close($curl);
  if ($response) {
    return json_decode($response);
  } else {
    return false;
  }
}

function qbUploadFile($token,$blob_param,$file_tmp,$file_type,$file_name){
  
  $post = array(
    "file"=>curl_file_create($file_tmp, $file_type, $file_name)
  );

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $blob_param);
  curl_setopt($ch, CURLOPT_POST,1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
  $response = curl_exec($ch);
  $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  $err = curl_error($ch);
  /*echo '<br> '.$httpcode.' <br>';*/
  if ($response) {
    return $httpcode/*json_decode($response)*/;
  } else {
    return false;
  }
  /*if ($err)
    echo "cURL Error #:" . $err;
  else
    echo $response.'<br/>';*/
}

function qbUploadFileOld($token,$blob_param,$file_tmp,$file_type,$file_name){
  $filePath = '@/' . $file_tmp;
  $cfile = new CURLFile($file_tmp,$file_type,$file_name);

  $curl = curl_init();
  /*if (function_exists('curl_file_create')) { // php 5.5+
    $cFile = curl_file_create($file_tmp);
  } else { // 
    $cFile = '@' . realpath($file_tmp);
    curl_setopt($curl, CURLOPT_SAFE_UPLOAD, false);
  }*/

  /*$filePath = fopen($file_tmp, 'r');*/
  $post_body = http_build_query(array(
      'file' => $cfile
  ));
  curl_setopt($curl, CURLOPT_URL, urldecode($blob_param));
  curl_setopt($curl, CURLOPT_POST, true);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Accept: application/json',
      'Content-Type: application/x-www-form-urlencoded',
      'QuickBlox-REST-API-Version: 0.1.0',
      'QB-Token: ' . $token
  ));
  $response = curl_exec($curl);
  
  if ($response) {
    curl_close($curl);
    return json_decode($response);
  } else {
    
    if(curl_errno($curl)){
      print_r(curl_errno($curl));
    }
    curl_close($curl);
    return false;
  }
}

function qbMarkFileUploaded($token,$blob_id,$blob_size){
  $post_body = http_build_query(array(
      'blob[size]' => $blob_size
  ));
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'blobs/'.$blob_id.'/complete.json');
  /*curl_setopt($curl, CURLOPT_POST, true);*/
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
  curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Accept: application/json',
      'Content-Type: application/x-www-form-urlencoded',
      'QuickBlox-REST-API-Version: 0.1.0',
      'QB-Token: ' . $token
  ));
  $response = curl_exec($curl);
  $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
  curl_close($curl);
  if ($response) {
    return $httpcode/*json_decode($response)*/;
  } else {
    return false;
  }
}

function getUserDetailsByQuickID($user_quick_id){
  $my_id = Session::get('userdata')['id'];
  $user_data = DB::select("SELECT users.*, (SELECT IF ((SELECT COUNT(*) FROM blocked_users WHERE bu_from_id=$my_id AND bu_to_id=users.id) , 1, 0)) AS is_blocked FROM users WHERE users.quick_blox_id='".$user_quick_id."'");
  $user_data = ($user_data && !empty($user_data[0])) ? $user_data[0] : NULL;
  return $user_data;
  /*$user_data = DB::table('users')->where('quick_blox_id',$user_quick_id)->first();
  return $user_data;*/
}

DEFINE('FCM_AUTH_KEY', "AAAAJFwuG28:APA91bEYITgagKQujh3rE-uf1iVKGCDsbN_70M1nIvaTN6cI2yV_zS3-3AuZ_OctzjUmdcXKk4YykAF5gp0JFZPi7SaTzfo7xr_LrHn971Wbb5yuPOJs7eLQrDETAMQh0QBG9YsKMg9V"); // Enter your fcm auth key

function sendNotification($type,$to,$title,$body,$param=array()){
  $call_session = (isset($param['call_session']) && $param['call_session']) ? $param['call_session'] : NULL;
  $user_data = (isset($param['user_data']) && $param['user_data']) ? $param['user_data'] : NULL;
  $call_id = (isset($param['call_id']) && $param['call_id']) ? $param['call_id'] : NULL;
  if($type=='android'){
    $fields =[
              'to' => $to,
              'data'=>
                [
                  'title'         =>  $title,
                  /*'text'        =>  $body,*/
                  'body'          =>  $body,
                  'priority'      =>  'high',
                  'type'          =>  $param['type'],
                  'user_data'     =>  $user_data,
                  'call_session'  =>  $call_session,
                  'call_id'       =>  $call_id,
                /*'thumbnail'=> 'https://images.pexels.com/photos/2662116/pexels-photo-2662116.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'*/
                ]
            ];
        
  }else{
    $fields =[
                'to' => $to,
                'content_available' => TRUE,
                'mutable_content'   => TRUE,
                'priority'          => 'high',
                'notification'=>
                  [
                    'title'         => $title,
                    'body'          => $body,
                    'sound'         => 'default',
                    'icon'          => 'appicon',
                    'priority'      => 'high',
                    'badge'         =>  1,
                    'type'          =>  $param['type'],
                    'user_data'     =>  $user_data,
                    'call_session'  =>  $call_session,
                    'call_id'       =>  $call_id,
                  ]
              ];
  }

  $json = json_encode ($fields, JSON_UNESCAPED_SLASHES); 

  $ch = curl_init ();
  curl_setopt ( $ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
  curl_setopt ( $ch, CURLOPT_POST, true );
  curl_setopt ( $ch, CURLOPT_HTTPHEADER, [
              'Authorization: key=' . FCM_AUTH_KEY,
              'Content-Type: application/json'
             ] );
  curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
  curl_setopt ( $ch, CURLOPT_POSTFIELDS, $json );
  $response = curl_exec ( $ch );

  curl_close ( $ch );

  return json_decode($response , TRUE);
}
DEFINE('SMS_API_KEY_OLD', "oj7qx1fo27gzaey"); // Enter your sms api key
DEFINE('SMS_API_SECRET', "zhblqpicjo2561i"); // Enter your sms api secret
DEFINE('SMS_FROM', "17248542233"); // Enter your contact number
DEFINE('SMS_URL_OLD', "https://www.thetexting.com/rest/sms/json"); // Enter sms url
//messagebird api
DEFINE('SMS_API_KEY', "P5UOqJgOQF8qPfQqXd3koYtZy");
DEFINE('SMS_URL', "https://rest.messagebird.com/messages"); // Enter sms url
function sendSms($to,$text){
  
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, SMS_URL);
  $post_body = array(
      'recipients'  =>  $to,
      'originator'  =>  'WhatsCommon',
      'body'        =>  $text
  );
   curl_setopt($curl, CURLOPT_POST, true);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
  
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
    'Authorization: AccessKey '.SMS_API_KEY,
  ));
  $response = curl_exec($curl);

  if ($response) {
    curl_close($curl);
    return json_decode($response);
  } else {
    
    if(curl_errno($curl)){
      print_r(curl_errno($curl));
    }
    curl_close($curl);
    return false;
  }
}

DEFINE('PAYPAL_CLIENT_ID', "AcwL9AXx5h4aZ8VnStkVAo6DxjXtkiNg11f40SZy7TBx_Dy0s79rJc4xK87QnlfY_VDQYn6Keua6rOJ-"); 
// Enter paypal client id
DEFINE('PAYPAL_CLIENT_SECRET', "EFZbbgiYNkkAn7VlsijtjzazZrkjKv6v9BHW13H3gzTwCLd7nhgYLZyCdjhVjDvSjj6EV6RjeSk2JksI"); 
// Enter your sms api key
DEFINE('PAYPAL_URL', "https://api-m.sandbox.paypal.com/v1/");
function cancelSubcription($subcription_id,$accessToken){
  $post_body = json_encode(['reason' => 'Not satisfied with the service']);
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, PAYPAL_URL.'billing/subscriptions/'.$subcription_id.'/cancel');
  curl_setopt($curl, CURLOPT_POST, true);
  /*curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");*/
  curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Accept-Language: en_US',
      'Authorization: Bearer '.$accessToken
  ));
  $response = curl_exec($curl);
  $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
  curl_close($curl);
  //echo $httpcode;
  if ($response) {
    return $httpcode /*json_decode($response)*/;
  } else {
    return false;
  }
}

function getAccessToken(){
  $curl = curl_init();
  
  $post_body = "grant_type=client_credentials";
  $enc = base64_encode(PAYPAL_CLIENT_ID.':'.PAYPAL_CLIENT_SECRET);
  curl_setopt($curl, CURLOPT_URL, PAYPAL_URL.'oauth2/token');
  curl_setopt($curl, CURLOPT_POST, true);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Accept: application/json',
      'Accept-Language: en_US',
      'Authorization: Basic '.$enc,
      'Content-Type: application/x-www-form-urlencoded'
  ));
  $response = curl_exec($curl);

  if ($response) {
    curl_close($curl);
    return json_decode($response);
  } else {
    
    if(curl_errno($curl)){
      print_r(curl_errno($curl));
    }
    curl_close($curl);
    return false;
  }
}

function getSubscriptionDetails($subcription_id,$accessToken){
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, PAYPAL_URL.'billing/subscriptions/'.$subcription_id);
  
  
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      /*'Accept: application/json',*/
      'Content-Type: application/json',
      'Authorization: Bearer '.$accessToken
  ));
  $response = curl_exec($curl);

  if ($response) {
    curl_close($curl);
    return json_decode($response);
  } else {
    
    if(curl_errno($curl)){
      print_r(curl_errno($curl));
    }
    curl_close($curl);
    return false;
  }
}

function alpha_numeric_rand(){
  $str_result = '0123456789abcdefghijklmnopqrstuvwxyz';
  $str1 = substr(str_shuffle($str_result), 0, 8);
  $str2 = substr(str_shuffle($str_result), 0, 4);
  $str3 = substr(str_shuffle($str_result), 0, 4);
  return $str1.$str2.$str3;
}