<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use View;
use Validator;
use Illuminate\Support\Facades\Mail;

class NewsLetter extends Controller
{
    function subscriptionsList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $newsletter_subcriptions = DB::table('newsletter_subcriptions')->orderBy('_ns_id','desc')->get();

        return View::make("admin/newsletter_subcriptions")->with(['newsletter_subcriptions' => $newsletter_subcriptions]);
    }

    function newsLetterList(Request $request){
    	if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
    	$newsletter_templates = DB::table('newsletter_templates')->get();

    	return View::make("admin/newsletter_list")->with(['newsletter_templates' => $newsletter_templates]);
    }

    function addNewsLetter(Request $request){
    	if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }

        return View::make("admin/add_newsletter");
    }

    function createNewsLetter(Request $request){
    	if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }

        $rules =[
                    'nt_title'   	=>  'required',
                    'nt_content'   	=>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
        	$done = DB::table('newsletter_templates')->insert([
                                                        'nt_title' 	=> 	$request->nt_title,
                                                        'nt_content'=>	$request->nt_content
                                                    ]);

            if(!$done){
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else{
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'News Letter is created',
                                'reponse_body'  =>  "null",
                                'redirect'		=>	url('admin/news-letters')
                            ], 200);
            }
        }
    }

    function editNewsLetter($_nt_id){
    	/*if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }*/
    	$nt_data = DB::table('newsletter_templates')->where('_nt_id',$_nt_id)->first();

    	if(!$nt_data){
    		return redirect('admin/news-letters');
    	}
    	return View::make("admin/edit_newsletter")->with(['nt_data' => $nt_data]);
    }

    function updateNewsLetter(Request $request){
    	if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }

        $rules =[
                    'nt_title'   	=>  'required',
                    'nt_content'   	=>  'required',
                    '_nt_id'		=>	'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
        	$done = DB::table('newsletter_templates')->where('_nt_id',$request->_nt_id)->update([
                                                        'nt_title' 	=> 	$request->nt_title,
                                                        'nt_content'=>	$request->nt_content
                                                    ]);

            if(!$done){
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else{
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'News Letter is updated',
                                'reponse_body'  =>  "null",
                                'redirect'		=>	url('admin/news-letters')
                            ], 200);
            }
        }
    }

    function sendNewsLetterModal(Request $request){
        $rules =[
                    '_nt_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $nt_data = DB::table('newsletter_templates')->where('_nt_id', $request->_nt_id)->first();
            $users = DB::table('newsletter_subcriptions')->get();
            $html = view('admin.xhr.send_newsletter_modal',['users' => $users, 'nt_data'=> $nt_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function subsribeToUs(Request $request){
        $rules =[
                    'email'   =>   'required|email:filter'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $check_user = DB::table('newsletter_subcriptions')->where('ns_user_email',$request->email)->first();
            if(!$check_user){
                DB::table('newsletter_subcriptions')->insert(['ns_user_email' => $request->email]);
            }

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Thank you for subscribing! <br>Always be on the lookout for updates and announcements.',
                                'reponse_body'  =>  'null',
                                'show_toast'    =>  TRUE,
                                'time'          =>  3000,
                                'redirect'      =>  url('/')
                            ],200);
        }
    }
    
    function sendNewsLetter(Request $request){
        $rules =[
                    '_nt_id'   =>   'required',
                    'users'    =>   'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $nt_data = DB::table('newsletter_templates')->where('_nt_id', $request->_nt_id)->first();
            
            if($request->users){
                
                foreach($request->users as $i => $email){
                    $data = [
                            'subject'   =>  $nt_data->nt_title,
                            'name'      =>  'WhatsCommon',
                            'content'   =>  $nt_data->nt_content,
                            'email'     =>  $email
                        ];
                    Mail::send('newsletter_email', $data, function($message) use ($data) {
                        $message->to($data['email'])->subject($data['subject']);
                    });
                }
                
            }

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'NewsLetter Sent Successfully',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
        }
    }
}
