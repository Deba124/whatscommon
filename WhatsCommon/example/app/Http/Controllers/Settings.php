<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Userdetail;
use App\Models\BankDetail;
use App\Models\NotificationSetting;
use Validator;
use View;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\InAppPurchase;
use App\Http\Controllers\Payment;

class Settings extends Controller
{

	function index(Request $request){
		if (!$request->session()->has('userdata')) {
            return redirect('login');
        }

        $user_id = $request->session()->get('userdata')['id'];
        $userdetails = DB::select("SELECT userdetails.*, users.username, users.profile_photo_path, countries.country_name, provinces.province_name, cities.city_name
          FROM userdetails
          INNER JOIN users ON users.id=userdetails.id
          LEFT JOIN countries ON userdetails.country=countries._country_id
          LEFT JOIN provinces ON userdetails.state = provinces._province_id
          LEFT JOIN cities ON userdetails.city=cities._city_id
          WHERE userdetails.id = '".$user_id."'
          ");
        $countries = DB::select("SELECT * FROM countries ORDER BY country_name ASC");
        $provinces = [];
        if($userdetails && $userdetails[0] && $userdetails[0]->country){
        	$provinces = DB::select("SELECT * FROM provinces INNER JOIN countries ON countries._country_id=provinces.province_country WHERE countries._country_id='".$userdetails[0]->country."' ORDER BY province_name ASC");
        }
        $cities = [];
        if($userdetails && $userdetails[0] && $userdetails[0]->state){
        	$cities = DB::select("SELECT * FROM cities INNER JOIN provinces ON provinces._province_id=cities.city_province WHERE provinces._province_id='".$userdetails[0]->state."' ORDER BY city_name ASC");
        }
        /*print_r($userdetails);exit();*/
        return View::make("profile_settings")->with(['countries'=> $countries, 'provinces' => $provinces, 'cities'=> $cities, 'userdetails' => $userdetails ? $userdetails[0] : []]);
	}

	function changeProfilePicture(Request $request){
		if (!$request->session()->has('userdata')) {
            return redirect('login');
        }

        $user_id = $request->session()->get('userdata')['id'];

        $rules =[
			        'user_picture' => 	'required|mimes:jpg,jpeg,png|max:2048'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){

			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);

		}else{
			$fileName = time().'_'.$request->user_picture->getClientOriginalName();
            /*$filePath = $request->file('user_picture')->storeAs('profilePictures', $fileName, 'public');*/
            $request->user_picture->move(public_path('profilePictures'), $fileName);
            $profUrl = url('/profilePictures/'.$fileName);
            $dbUpdate = User::where('id',$user_id)->update(["profile_photo_path"=>$profUrl]);
            if(!$dbUpdate)
            {
                return response([
                    'response_code'=>'401',
                	'response_msg'=> 'update to db failed',
                	'reponse_body' => "null"
                ], 401);
            }

            return response([
            'response_code'=>'200',
                      'response_msg'=> 'profile picture updated',
                      'reponse_body' => ['img_src'=>$profUrl]
            ]);
		}
	}

	function updateProfile(Request $request){
		if (!$request->session()->has('userdata')) {
            return redirect('login');
        }

        //echo $request->dob;exit();

        $user_id = $request->session()->get('userdata')['id'];
        $userdetails = DB::select("SELECT userdetails.*, users.username, users.profile_photo_path, countries.country_name, provinces.province_name, cities.city_name
          FROM userdetails
          INNER JOIN users ON users.id=userdetails.id
          LEFT JOIN countries ON userdetails.country=countries._country_id
          LEFT JOIN provinces ON userdetails.state = provinces._province_id
          LEFT JOIN cities ON userdetails.city=cities._city_id
          WHERE userdetails.id = '".$user_id."'
          ");
        $userdetails = $userdetails ? $userdetails[0] : [];
        $u_unique = ($userdetails && $userdetails->username!=$request->username) ? '|unique:users,username' : NULL;
        $e_unique = ($userdetails && $userdetails->email!=$request->email) ? '|unique:users,email' : NULL;
        
        $rules =[
			        'firstName' => 	'required',
			        'lastName'	=>	'required',
			        'username'	=>	'required'.$u_unique,
			        'email'		=>	'required'.$e_unique,
			        'phone' 	=>	'required',
			        'dob' 		=>	'required',
			        'isd_code'	=>	'required',
			        'bio'		=>	'nullable|max:100',
			        'country'	=>	'nullable',
			        'state'		=>	'nullable',
			        'city'		=>	'nullable',
			        'zip'		=>	'nullable',
			        'street'	=>	'nullable'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){

			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);

		}else{
	
			$t = date_create_from_format("m-d-Y",$request->dob);
			$dob = date_format($t,"Y-m-d");
			$dob_day = date('d',strtotime($dob));
			$dob_month = date('m',strtotime($dob));
			$dob_year = date('Y',strtotime($dob));
			
			DB::table('userdetails')->where('id',$user_id)->update([
				'firstName'		=>	$request->firstName,
				'lastName'		=>	$request->lastName,
				'email'			=>	$request->email,
				'phone'			=>	$request->phone,
				'dob'			=>	$dob,
				'mi'			=>	$request->mi,
				'isd_code'		=>	$request->isd_code,
				'dob_day'		=>	$dob_day,
				'dob_month'		=>	$dob_month,
				'dob_year'		=>	$dob_year,
				'bio'			=>	$request->bio ? $request->bio : NULL,
				'country'		=>	$request->country,
				'state'			=>	$request->state,
				'city'			=>	$request->city,
				'zip'			=>	$request->zip,
				'street'		=>	$request->street
			]);

			DB::table('users')->where('id',$user_id)->update([
				'firstName'		=>	$request->firstName,
				'lastName'		=>	$request->lastName,
				'username' 		=>	$request->username,
				'email'			=>	$request->email,
				'phone'			=>	$request->phone,
				'mi'			=>	$request->mi,
				'isd_code'		=>	$request->isd_code
			]);

			$user = User::where('id',$user_id)->first();

			$data = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
                ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                ->leftJoin('cities','userdetails.city','=','cities._city_id')
                ->select('userdetails.*', DB::raw('TIMESTAMPDIFF(YEAR, userdetails.dob, CURRENT_DATE) AS user_age'), 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                ->where('userdetails.id', $user->id)
                ->first();

            $counts = DB::select("SELECT 
        (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
        (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count,
        (SELECT SUM(personal_count + dating_count + adoption_count + travel_count + military_count + school_count + career_count + pets_count + lost_count + doppel_count)) AS matched_count
        ");

      		$premium_user = DB::select("SELECT IF((SELECT COUNT(id) FROM chosen_plans 
                            WHERE cp_user_id='".$user->id."'
                            AND cp_is_expired=0 
                            AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_premium");
		    if($data && $user)
		        $newData = array_merge($data->toArray(),$user->toArray());
		    $newData['counts'] = $counts;
		    $newData['user_is_premium'] = $premium_user[0]->is_premium ? $premium_user[0]->is_premium : 0;

		    if(empty($newData['quick_blox_id'])){
        		$qb_session = qb_login();
		        if($qb_session && isset($qb_session->session) && $qb_session->session){
		          	$token = $qb_session->session->token;
		          	$newUser = quickAddUsers($token,$newData['email'],'12345678',$newData['email'],$newData['firstName'].' '.$newData['lastName']);
		          	if(isset($newUser->user) && $newUser->user){
		            	$quick_blox_id = $newUser->user->id;
		            	User::where('id',$newData['id'])->update(['quick_blox_id'=>$quick_blox_id]);
		              	$qb_session = qb_login($newData['email'],'12345678');
		              	if($qb_session && isset($qb_session->session) && $qb_session->session){
		                	$token = $qb_session->session->token;
		                	$newData['qb_token'] = $token;
		              	}
		            
		          	}else{
		            	$existing_user = getUserByEmail($token,$newData['email']);
		            if(isset($existing_user->user) && $existing_user->user){
		              	$quick_blox_id = $existing_user->user->id;
		              	User::where('id',$newData['id'])->update(['quick_blox_id'=>$quick_blox_id]);
		              	$qb_session = qb_login($newData['email'],'12345678');
		              	if($qb_session && isset($qb_session->session) && $qb_session->session){
		                	$token = $qb_session->session->token;
		                	$newData['qb_token'] = $token;
		              	}
		            }
		        }
	        }
		    }else{
		        $qb_session = qb_login($newData['email'],'12345678');
		        if($qb_session && isset($qb_session->session) && $qb_session->session){
		          $token = $qb_session->session->token;
		          $newData['qb_token'] = $token;
		        }
		    }
      
        	$request->session()->put('userdata', $newData);
      

			return response([
                    			'response_code'	=>	'200',
                         		'response_msg'	=> 	'Profile updated successfully',
                         		'reponse_body' 	=> 	NULL,
                         		'redirect'		=>	url('settings')
                         	], 200);
		}
	}

	function financialSettings(Request $request){
		if (!$request->session()->has('userdata')) {
            return redirect('login');
        }

        $user_id = $request->session()->get('userdata')['id'];

        $banks = BankDetail::where('bank_user_id',$user_id)->orderBy('bank_is_default','DESC')->get();

        return View::make("financial_settings")->with(['banks' => $banks]);
	}

	function securityAndPrivacy(Request $request){
		if (!$request->session()->has('userdata')) {
            return redirect('login');
        }

        $user_id = $request->session()->get('userdata')['id'];
        $settings = User::where('id',$user_id)->first();
        return View::make("security_settings")->with(['settings' => $settings]);
	}

	public function feedSettings(Request $request){
		if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
		$user_id = $request->session()->get('userdata')['id'];
        $settings = User::where('id',$user_id)->first();
        return View::make("feed_settings")->with(['settings' => $settings]);
	}

	public function updateFeedSetting(Request $request){

		if (!$request->session()->has('userdata')) {
            return redirect('login');
        }

        $user_id = $request->session()->get('userdata')['id'];

		$show_personal_feed = ($request->show_personal_feed && $request->show_personal_feed==1) ? 1 : 0;
		$show_dating_feed = ($request->show_dating_feed && $request->show_dating_feed==1) ? 1 : 0;
		$show_adoption_feed = ($request->show_adoption_feed && $request->show_adoption_feed==1) ? 1 : 0;
		$show_travel_feed = ($request->show_travel_feed && $request->show_travel_feed==1) ? 1 : 0;
		$show_military_feed = ($request->show_military_feed && $request->show_military_feed==1) ? 1 : 0;
		$show_education_feed = ($request->show_education_feed && $request->show_education_feed==1) ? 1 : 0;
		$show_career_feed = ($request->show_career_feed && $request->show_career_feed==1) ? 1 : 0;
		$show_pets_feed = ($request->show_pets_feed && $request->show_pets_feed==1) ? 1 : 0;
		$show_lostfound_feed = ($request->show_lostfound_feed && $request->show_lostfound_feed==1) ? 1 : 0;
		$show_droppel_feed = ($request->show_droppel_feed && $request->show_droppel_feed==1) ? 1 : 0;
		$show_username_feed = ($request->show_username_feed && $request->show_username_feed==1) ? 1 : 0;
		$show_activity_feed = ($request->show_activity_feed && $request->show_activity_feed==1) ? 1 : 0;

		User::where('id',$user_id)->update(	[
												
												'show_personal_feed'		=>	$show_personal_feed,
												'show_dating_feed'			=>	$show_dating_feed,
												'show_adoption_feed'		=>	$show_adoption_feed,
												'show_travel_feed'			=>	$show_travel_feed,
												'show_military_feed'		=>	$show_military_feed,
												'show_education_feed'		=>	$show_education_feed,
												'show_career_feed'			=>	$show_career_feed,
												'show_pets_feed'			=>	$show_pets_feed,
												'show_lostfound_feed'		=>	$show_lostfound_feed,
												'show_droppel_feed'			=>	$show_droppel_feed,
												'show_username_feed'		=>	$show_username_feed,
												'show_activity_feed'		=>	$show_activity_feed,
											]);
		$settings = User::where('id',$user_id)->first();
		return response([
                      		'response_code'=>'200',
                			'response_msg'=> 'Feed settings updated successfully',
                  			'reponse_body' => ['settings' => $settings]
                  		], 200);
    	
    }

	function updateSecuritySettings(Request $request){
		if (!$request->session()->has('userdata')) {
            return redirect('login');
        }

        $user_id = $request->session()->get('userdata')['id'];

        

		/*$two_authentication = ($request->two_authentication && $request->two_authentication==1) ? 1 : 0;*/
		$sign_touch_id = ($request->sign_touch_id && $request->sign_touch_id==1) ? 1 : 0;
		$sign_face_id = ($request->sign_face_id && $request->sign_face_id==1) ? 1 : 0;
		$privacy_email = ($request->privacy_email && $request->privacy_email==1) ? 1 : 0;
		$privacy_phone = ($request->privacy_phone && $request->privacy_phone==1) ? 1 : 0;
		$privacy_birthday = ($request->privacy_birthday && $request->privacy_birthday==1) ? 1 : 0;
		/*$privacy_location = ($request->privacy_location && $request->privacy_location==1) ? 1 : 0;
		$privacy_share_connection = ($request->privacy_share_connection && $request->privacy_share_connection==1) ? 1 : 0;*/
		$show_connection_list = ($request->show_connection_list && $request->show_connection_list==1) ? 1 : 0;
		$show_match_feed = ($request->show_match_feed && $request->show_match_feed==1) ? 1 : 0;
		$show_connection_feed = ($request->show_connection_feed && $request->show_connection_feed==1) ? 1 : 0;

		User::where('id',$user_id)->update(	[
												/*"two_authentication"		=>	$two_authentication,*/
												'sign_touch_id'				=>	$sign_touch_id,
												'sign_face_id'				=>	$sign_face_id,
												'privacy_email'				=>	$privacy_email,
												'privacy_phone'				=>	$privacy_phone,
												'privacy_birthday'			=>	$privacy_birthday,
												'show_connection_list'		=>	$show_connection_list,
												'show_match_feed'			=>	$show_match_feed,
												'show_connection_feed'		=>	$show_connection_feed
											]);
		$settings = User::where('id',$user_id)->first();
		return response([
                      		'response_code'=>'200',
                			'response_msg'=> 'Security settings updated successfully',
                  			'reponse_body' => ['settings' => $settings]
                  		], 200);
	}

	function messageSettings(Request $request){
		if (!$request->session()->has('userdata')) {
            return redirect('login');
        }

        $user_id = $request->session()->get('userdata')['id'];
        $settings = User::where('id',$user_id)->first();
        return View::make("message_settings")->with(['settings' => $settings]);
	}

	function updateMessageSettings(Request $request){
		if (!$request->session()->has('userdata')) {
            return redirect('login');
        }

        $user_id = $request->session()->get('userdata')['id'];

		$allow_notification = ($request->allow_notification && $request->allow_notification==1) ? 1 : 0;
		$save_media = ($request->save_media && $request->save_media==1) ? 1 : 0;

		User::where('id',$user_id)->update(	[
												"allow_notification"		=>	$allow_notification,
												'save_media'				=>	$save_media
											]);
		$settings = User::where('id',$user_id)->first();
		return response([
                      		'response_code'=>'200',
                			'response_msg'=> 'Message settings updated successfully',
                  			'reponse_body' => ['settings' => $settings]
                  		], 200);
	}

	function notificationSettings(Request $request){
		if (!$request->session()->has('userdata')) {
            return redirect('login');
        }

        $user_id = $request->session()->get('userdata')['id'];

        $notifications = NotificationSetting::where('ns_user_id',$user_id)->first();
        return View::make("notification_settings")->with(['notifications' => $notifications]);
	}

	function updateNotificationSettings(Request $request){
		if (!$request->session()->has('userdata')) {
            return redirect('login');
        }

        $user_id = $request->session()->get('userdata')['id'];

        $notifications = NotificationSetting::where('ns_user_id',$user_id)->first();

		/*$message_setting = ($request->message_setting && $request->message_setting==1) ? 1 : 0;*/
		$share_setting = ($request->share_setting && $request->share_setting==1) ? 1 : 0;
		$new_connection_setting = ($request->new_connection_setting && $request->new_connection_setting==1) ? 1 : 0;
		$new_match_setting = ($request->new_match_setting && $request->new_match_setting==1) ? 1 : 0;

		if($notifications){
			NotificationSetting::where('ns_user_id',$user_id)->update([
											/*'ns_message'		=>	$message_setting,*/
											'ns_shares'			=>	$share_setting,
											'ns_new_connection'	=>	$new_connection_setting,
											'ns_new_match'		=>	$new_match_setting
										]);
		}else{

			NotificationSetting::insert([
											'ns_user_id'		=>	$user_id,
											/*'ns_message'		=>	$message_setting,*/
											'ns_shares'			=>	$share_setting,
											'ns_new_connection'	=>	$new_connection_setting,
											'ns_new_match'		=>	$new_match_setting
										]);
		}
		$notifications = NotificationSetting::where('ns_user_id',$user_id)->first();

        return response([
                      		'response_code'=>'200',
                			'response_msg'=> 'Notification settings updated successfully',
                  			'reponse_body' => ['notification_settings' => $notifications]
                  		], 200);
	}

	function linkAccounts(Request $request){
		if (!$request->session()->has('userdata')) {
            return redirect('login');
        }

        $user_id = $request->session()->get('userdata')['id'];

        $settings = User::where('id',$user_id)->first();
        return View::make("link_accounts")->with(['settings' => $settings]);
	}

	function updateLinkAccounts(Request $request){
		if (!$request->session()->has('userdata')) {
            return redirect('login');
        }

        $user_id = $request->session()->get('userdata')['id'];

		$user_share_fb = ($request->user_share_fb && $request->user_share_fb==1) ? 1 : 0;
		$user_share_twitter = ($request->user_share_twitter && $request->user_share_twitter==1) ? 1 : 0;
		$user_share_insta = ($request->user_share_insta && $request->user_share_insta==1) ? 1 : 0;
		$user_share_tagster = ($request->user_share_tagster && $request->user_share_tagster==1) ? 1 : 0;

		User::where('id',$user_id)->update(	[
												"user_share_fb"			=>	$user_share_fb,
												'user_share_twitter'	=>	$user_share_twitter,
												"user_share_insta"		=>	$user_share_insta,
												"user_share_tagster"	=>	$user_share_tagster
											]);
		$settings = User::where('id',$user_id)->first();
		return response([
                      		'response_code'=>'200',
                			'response_msg'=> 'Link accounts settings updated successfully',
                  			'reponse_body' => ['settings' => $settings]
                  		], 200);
	}

	function blockedUsers(Request $request){
		if (!$request->session()->has('userdata')) {
            return redirect('login');
        }

        $user_id = $request->session()->get('userdata')['id'];
        $blocked_users = DB::table('blocked_users')
        					->join('users','users.id','=','blocked_users.bu_to_id')
        					->where('bu_from_id',$user_id)
        					->get();

        return View::make("blocked_users")->with(['blocked_users' => $blocked_users]);
	}

	function blockedUsersList(Request $request){
		$rules =[
			        'user_id' 			=> 	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$blocked_users = DB::table('blocked_users')
        					->join('users','users.id','=','blocked_users.bu_to_id')
        					->where('bu_from_id',$request->user_id)
        					->get();
        	return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'Blocked users fetched successfully',
                  				'reponse_body' 	=> 	['blocked_users' => $blocked_users],
                  				'redirect'		=>	url('blocked-users')
                  			], 200);
		}
	}

	function searchUser(Request $request){
		$rules =[
			        'user_id' 			=> 	'required',
			        'search_val'		=>	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$users = DB::select("SELECT * FROM users WHERE id!='".$request->user_id."' AND id NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id='".$request->user_id."') AND (firstName like '%".$request->search_val."%' OR lastName like '%".$request->search_val."%')");
			$search_results = '';
			
			$search_results = view('xhr.search_results',['user_id' => $request->user_id,'users' => $users])->render();
            return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'Users fetched successfully',
                  				'reponse_body' 	=> 	['users' => $users,'search_results' => $search_results]
                  			], 200);
		}
	}

	function blockUser(Request $request){
		$rules =[
			        'user_id' 			=> 	'required',
			        'block_user_id'		=>	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$check_block = 	DB::table('blocked_users')
								->where('bu_from_id', $request->user_id)
								->where('bu_to_id', $request->block_user_id)
								->first();
			if($check_block){
				return response([
		                    		'response_code'	=>	'203',
		                         	'response_msg'	=> 	'user already blocked',
		                         	'reponse_body' 	=> 	'null'
		                        ], 401);
			}

			DB::table('blocked_users')->insert(['bu_from_id' => $request->user_id, 'bu_to_id' => $request->block_user_id]);
			$blocked_users = DB::table('blocked_users')
	        					->join('users','users.id','=','blocked_users.bu_to_id')
	        					->where('bu_from_id',$request->user_id)
	        					->get();
			return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'User blocked successfully',
                  				'reponse_body' 	=> 	['blocked_users' => $blocked_users],
                  				'redirect'		=>	url('blocked-users')
                  			], 200);
		}
	}

	function unblockUser(Request $request){
		$rules =[
			        'user_id' 			=> 	'required',
			        'block_user_id'		=>	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$check_block = 	DB::table('blocked_users')
								->where('bu_from_id', $request->user_id)
								->where('bu_to_id', $request->block_user_id)
								->first();
			if(!$check_block){
				return response([
		                    		'response_code'	=>	'203',
		                         	'response_msg'	=> 	'invalid request',
		                         	'reponse_body' 	=> 	'null'
		                        ], 401);
			}

			DB::table('blocked_users')->where('_bu_id',$check_block->_bu_id)->delete();

			$blocked_users = DB::table('blocked_users')
	        					->join('users','users.id','=','blocked_users.bu_to_id')
	        					->where('bu_from_id',$request->user_id)
	        					->get();
			return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'User unblocked successfully',
                  				'reponse_body' 	=> 	['blocked_users' => $blocked_users],
                  				'redirect'		=>	url('blocked-users')
                  			], 200);
		}
	}

	function changePassword(Request $request){
		if (!$request->session()->has('userdata')) {
            return redirect('login');
        }

        $user_id = $request->session()->get('userdata')['id'];

        return View::make("change_password");
	}

	function updatePassword(Request $request){
		$rules =[
			        'user_id' 		=> 	'required',
			        'old_password'	=>	'required',
			        'new_password'	=>	'required',
			        'cnf_password'	=>	'required|same:new_password'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$pass =  User::where('id',$request->user_id)->first();
			if(Hash::check($request->old_password,$pass->password)){
                User::where('id',$request->user_id)->update([
                    'password'=>Hash::make($request->new_password)
                    ]);
                return response([
                    			'response_code'	=>	'200',
                         		'response_msg'	=> 	'password updated successfully',
                         		'reponse_body' 	=> 	NULL,
                         		'redirect'		=>	url('change-password')
                         	], 200);
               // $this->getUsers($request);
            }
            return response([
                    		'response_code'	=>	'203',
                         	'response_msg'	=> 	'Current password is wrong',
                         	'reponse_body' 	=> 	'null'
                        ], 401);
		}
	}

    function findBankDetails(Request $request){
    	$rules =[
			        'user_id' 	=> 	'required',
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$banks = BankDetail::where('bank_user_id',$request->user_id)->get();

			return response([
                				'response_code'	=>	'200',
                         		'response_msg'	=> 	'Financial settings fetched successfully',
                         		'reponse_body' 	=> 	$banks
                        	], 200);
		}
    }

    function addBankDetails(Request $request){

    	$rules =[
			        'type' 		=> 	'required',
			        'user_id' 	=> 	'required',
			    ];
		if($request->type=='card'){

			$rules['card_number'] 		=	'required';
			$rules['card_expiry_date'] 	=	'required';
			$rules['card_cvv'] 			=	'required';
			$rules['card_holder_name'] 	=	'required';

		}else if($request->type=='paypal'){

			$rules['paypal_email'] 		=	'required';
			$rules['paypal_password'] 	=	'required';

		}else if($request->type=='bank'){

			$rules['card_holder_name'] 	=	'required';
			$rules['bank_name'] 		=	'required';
			$rules['account_number'] 	=	'required';
			$rules['routing_number'] 	=	'required';

		}
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{

			if($request->type=='card'){
				$bank_details = [
									'bank_type'				=>	'card',
									'bank_user_id'			=>	$request->user_id,
									'bank_is_default'		=>	($request->is_default && $request->is_default==1) ? 1 : 0,
									'bank_ac_or_card_no'	=>	$request->card_number,
									'bank_card_holder_name'	=>	$request->card_holder_name,
									'bank_card_expiry'		=>	$request->card_expiry_date,
									'bank_card_cvv'			=>	$request->card_cvv
								];
			}else if($request->type=='paypal'){
				$bank_details = [
									'bank_type'				=>	'paypal',
									'bank_user_id'			=>	$request->user_id,
									'bank_is_default'		=>	($request->is_default && $request->is_default==1) ? 1 : 0,
									'bank_email'			=>	$request->paypal_email,
									'bank_password'			=>	$request->paypal_password
								];
			}else if($request->type=='bank'){
				$bank_details = [
									'bank_type'				=>	'bank',
									'bank_user_id'			=>	$request->user_id,
									'bank_is_default'		=>	($request->is_default && $request->is_default==1) ? 1 : 0,
									'bank_card_holder_name'	=>	$request->card_holder_name,
									'bank_ac_or_card_no'	=>	$request->account_number,
									'bank_name'				=>	$request->bank_name,
									'bank_routing_no'		=>	$request->routing_number
								];
			}

			if($request->is_default && $request->is_default==1){
				BankDetail::where('bank_user_id',$request->user_id)->update(['bank_is_default'=>0]);
			}

			BankDetail::insert($bank_details);
			$banks = BankDetail::where('bank_user_id',$request->user_id)->get();

			return response([
                				'response_code'	=>	'200',
                         		'response_msg'	=> 	'Financial setting updated successfully',
                         		'reponse_body' 	=> 	$banks,
                         		'redirect'		=>	url('financial-settings')
                        	], 200);
		}
    	
    }

    function deleteBank(Request $request){
    	$rules =[
			        'bank_id' 		=> 	'required',
			        'user_id' 		=> 	'required',
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$bank_data = BankDetail::where('bank_user_id',$request->user_id)->where('id',$request->bank_id)->first();
			if($bank_data){
				
				BankDetail::where('bank_user_id',$request->user_id)->where('id',$request->bank_id)->delete();
				$banks = BankDetail::where('bank_user_id',$request->user_id)->get();

				return response([
	                				'response_code'	=>	'200',
	                         		'response_msg'	=> 	'Bank details deleted successfully',
	                         		'reponse_body' 	=> 	$banks,
                         			'redirect'		=>	url('financial-settings')

	                        	], 200);
			}else{
				return response([
				                    'response_code'	=>	'201',
						            'response_msg'	=> 	'Invalid details',
						            'reponse_body' 	=> 	"null"
				                ], 401);
			}
		}
    }

    function makeBankAsDefault(Request $request){
    	$rules =[
			        'bank_id' 		=> 	'required',
			        'user_id' 		=> 	'required',
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$bank_data = BankDetail::where('bank_user_id',$request->user_id)->where('id',$request->bank_id)->first();
			if($bank_data){
				BankDetail::where('bank_user_id',$request->user_id)->update(['bank_is_default'=>0]);
				BankDetail::where('bank_user_id',$request->user_id)->where('id',$request->bank_id)->update(['bank_is_default'=>1]);
				$banks = BankDetail::where('bank_user_id',$request->user_id)->get();

				return response([
	                				'response_code'	=>	'200',
	                         		'response_msg'	=> 	'Default bank set successfully',
	                         		'reponse_body' 	=> 	$banks,
	                         		'redirect'		=>	url('financial-settings')
	                        	], 200);
			}else{
				return response([
				                    'response_code'	=>	'201',
						            'response_msg'	=> 	'Invalid details',
						            'reponse_body' 	=> 	"null"
				                ], 401);
			}
		}
    }

    function addDonation(Request $request){

    	$rules =[
			        'currency_id' 			=> 	'required',
			        'user_id' 				=> 	'required',
			        'donation_amount'		=>	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$done = DB::table('donations')->insert([
				        	'donation_currency'			=> 	$request->currency_id,
				        	'donation_amount' 			=> 	$request->donation_amount,
				        	'donation_user_id' 			=> 	$request->user_id,
				        	'donation_bank_id' 			=> 	$request->bank_id ? $request->bank_id : NULL,
				        	'donation_payment_status'	=>	'successful',
				        	'donation_payment'			=>	$request->payment ? json_encode($request->payment,TRUE) : NULL
              			]);

            if(!$done)
            {
                return response([
                        		'response_code'	=>	'201',
                      			'response_msg'	=> 	'invalid details',
                      			'reponse_body' 	=> 	"null"
                      		], 401);
            }
            else
            {
                return response([
                          		'response_code'	=>'200',
                    			'response_msg'	=> 'Donation is created',
                      			'reponse_body' 	=> "null",
                      			'time'			=> 1500,
                      			'redirect' 		=> url('donate')		
                      		], 200);
            }
		}
    }

    function getNotificationSetting(Request $request){
    	$rules =[
			        'user_id' 	=> 	'required',
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$notifications = NotificationSetting::where('ns_user_id',$request->user_id)->first();

			return response([
                				'response_code'	=>	'200',
                         		'response_msg'	=> 	'Notification settings fetched successfully',
                         		'reponse_body' 	=> 	$notifications
                        	], 200);
		}
    }

    function setNotificationSetting(Request $request){
    	$rules =[
			        /*'message_setting' 		=> 	'required',*/
			        'user_id' 				=> 	'required',
			        'share_setting'			=>	'required',
			        'new_connection_setting'=>	'required',
			        'new_match_setting'		=>	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$notifications = NotificationSetting::where('ns_user_id',$request->user_id)->first();

			/*$message_setting = ($request->message_setting && $request->message_setting==1) ? 1 : 0;*/
			$share_setting = ($request->share_setting && $request->share_setting==1) ? 1 : 0;
			$new_connection_setting = ($request->new_connection_setting && $request->new_connection_setting==1) ? 1 : 0;
			$new_match_setting = ($request->new_match_setting && $request->new_match_setting==1) ? 1 : 0;

			if($notifications){
				NotificationSetting::where('ns_user_id',$request->user_id)->update([
												/*'ns_message'		=>	$message_setting,*/
												'ns_shares'			=>	$share_setting,
												'ns_new_connection'	=>	$new_connection_setting,
												'ns_new_match'		=>	$new_match_setting
											]);
			}else{

				NotificationSetting::insert([
												'ns_user_id'		=>	$request->user_id,
												/*'ns_message'		=>	$message_setting,*/
												'ns_shares'			=>	$share_setting,
												'ns_new_connection'	=>	$new_connection_setting,
												'ns_new_match'		=>	$new_match_setting
											]);
			}
			$notifications = NotificationSetting::where('ns_user_id',$request->user_id)->first();

            return response([
                          		'response_code'=>'200',
                    			'response_msg'=> 'Notification settings updated successfully',
                      			'reponse_body' => ['notification_settings' => $notifications]
                      		], 200);
		}
    }

    function setSecuritySetting(Request $request){
    	$rules =[
			        'two_authentication' 		=> 	'required',
			        'user_id' 					=> 	'required',
			        'sign_touch_id'				=>	'required',
			        'sign_face_id'				=>	'required',
			        'privacy_email'				=>	'required',
			        'privacy_phone'				=>	'required',
			        'privacy_birthday'			=>	'required',
			        'show_connection_list'		=>	'required',
			        'show_match_feed'			=>	'required',
			        'show_connection_feed'		=>	'required',
			        /*'privacy_location'			=>	'required',
			        'privacy_share_connection'	=>	'required'*/
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$two_authentication = ($request->two_authentication && $request->two_authentication==1) ? 1 : 0;
			$sign_touch_id = ($request->sign_touch_id && $request->sign_touch_id==1) ? 1 : 0;
			$sign_face_id = ($request->sign_face_id && $request->sign_face_id==1) ? 1 : 0;
			$privacy_email = ($request->privacy_email && $request->privacy_email==1) ? 1 : 0;
			$privacy_phone = ($request->privacy_phone && $request->privacy_phone==1) ? 1 : 0;
			$privacy_birthday = ($request->privacy_birthday && $request->privacy_birthday==1) ? 1 : 0;
			$show_connection_list = ($request->show_connection_list && $request->show_connection_list==1) ? 1 : 0;
			$show_match_feed = ($request->show_match_feed && $request->show_match_feed==1) ? 1 : 0;
			$show_connection_feed = ($request->show_connection_feed && $request->show_connection_feed==1) ? 1 : 0;
			/*$privacy_location = ($request->privacy_location && $request->privacy_location==1) ? 1 : 0;
			$privacy_share_connection = ($request->privacy_share_connection && $request->privacy_share_connection==1) ? 1 : 0;*/

			User::where('id',$request->user_id)->update(	[
													"two_authentication"		=>	$two_authentication,
													'sign_touch_id'				=>	$sign_touch_id,
													'sign_face_id'				=>	$sign_face_id,
													'privacy_email'				=>	$privacy_email,
													'privacy_phone'				=>	$privacy_phone,
													'privacy_birthday'			=>	$privacy_birthday,
													'show_connection_list'		=>	$show_connection_list,
													'show_match_feed'			=>	$show_match_feed,
													'show_connection_feed'		=>	$show_connection_feed,
													/*'privacy_location'			=>	$privacy_location,
													'privacy_share_connection'	=>	$privacy_share_connection*/
												]);
			$settings = User::where('id',$request->user_id)->first();
			return response([
	                      		'response_code'=>'200',
	                			'response_msg'=> 'Security settings updated successfully',
	                  			'reponse_body' => ['settings' => $settings]
	                  		], 200);
		}
    }

    public function setFeedSetting(Request $request){
    	$rules =[
			        'show_personal_feed' 		=> 	'required',
			        'user_id' 					=> 	'required',
			        'show_dating_feed'				=>	'required',
			        'show_adoption_feed'				=>	'required',
			        'show_travel_feed'				=>	'required',
			        'show_military_feed'				=>	'required',
			        'show_education_feed'			=>	'required',
			        'show_career_feed'		=>	'required',
			        'show_pets_feed'			=>	'required',
			        'show_lostfound_feed'		=>	'required',
			        'show_droppel_feed'		=>	'required',
			        'show_username_feed'		=>	'required',
			        'show_activity_feed'		=>	'required',
			        
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$show_personal_feed = ($request->show_personal_feed && $request->show_personal_feed==1) ? 1 : 0;
			$show_dating_feed = ($request->show_dating_feed && $request->show_dating_feed==1) ? 1 : 0;
			$show_adoption_feed = ($request->show_adoption_feed && $request->show_adoption_feed==1) ? 1 : 0;
			$show_travel_feed = ($request->show_travel_feed && $request->show_travel_feed==1) ? 1 : 0;
			$show_military_feed = ($request->show_military_feed && $request->show_military_feed==1) ? 1 : 0;
			$show_education_feed = ($request->show_education_feed && $request->show_education_feed==1) ? 1 : 0;
			$show_career_feed = ($request->show_career_feed && $request->show_career_feed==1) ? 1 : 0;
			$show_pets_feed = ($request->show_pets_feed && $request->show_pets_feed==1) ? 1 : 0;
			$show_lostfound_feed = ($request->show_lostfound_feed && $request->show_lostfound_feed==1) ? 1 : 0;
			$show_droppel_feed = ($request->show_droppel_feed && $request->show_droppel_feed==1) ? 1 : 0;
			$show_username_feed = ($request->show_username_feed && $request->show_username_feed==1) ? 1 : 0;
			$show_activity_feed = ($request->show_activity_feed && $request->show_activity_feed==1) ? 1 : 0;

			User::where('id',$request->user_id)->update(	[
													"show_personal_feed"		=>	$show_personal_feed,
													'show_dating_feed'			=>	$show_dating_feed,
													'show_adoption_feed'		=>	$show_adoption_feed,
													'show_travel_feed'			=>	$show_travel_feed,
													'show_military_feed'		=>	$show_military_feed,
													'show_education_feed'		=>	$show_education_feed,
													'show_career_feed'			=>	$show_career_feed,
													'show_pets_feed'			=>	$show_pets_feed,
													'show_lostfound_feed'		=>	$show_lostfound_feed,
													'show_droppel_feed'			=>	$show_droppel_feed,
													'show_username_feed'		=>	$show_username_feed,
													'show_activity_feed'		=>	$show_activity_feed,
													
												]);
			$settings = User::where('id',$request->user_id)->first();
			return response([
	                      		'response_code'=>'200',
	                			'response_msg'=> 'Feed Settings updated successfully',
	                  			'reponse_body' => ['settings' => $settings]
	                  		], 200);
		}
    }

    function setMatchSetting(Request $request){
    	/*print_r($_POST);exit();*/
    	$rules =[
			        'user_id' 			=> 	'required',
			        'match_personal' 	=> 	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_dating'		=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_adoption'	=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_travel'		=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_military'	=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_education'	=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_work'		=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_pet'			=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_lostfound'	=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_droppel'		=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_username'	=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_activity'	=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{

			User::where('id',$request->user_id)->update(	[
													"match_personal"	=>	$request->match_personal,
													'match_dating'		=>	$request->match_dating,
													'match_adoption'	=>	$request->match_adoption,
													'match_travel'		=>	$request->match_travel,
													'match_military'	=>	$request->match_military,
													'match_education'	=>	$request->match_education,
													'match_work'		=>	$request->match_work,
													'match_pet'			=>	$request->match_pet,
													'match_lostfound'	=>	$request->match_lostfound,
													'match_droppel'		=>	$request->match_droppel,
													'match_username'	=>	$request->match_username,
													'match_activity'	=>	$request->match_activity
												]);
			$settings = User::where('id',$request->user_id)->first();
			return response([
	                      		'response_code'	=>	'200',
	                			'response_msg'	=> 	'Match setting updated successfully',
	                  			'reponse_body' 	=> 	['settings' => $settings],
	                  			'redirect'		=>	url('home')
	                  		], 200);
		}
    }

    function getMatchSettings(Request $request){
    	/*print_r($_POST);exit();*/
    	$rules =[
			        'user_id' 			=> 	'required',
			        /*'match_personal' 	=> 	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_dating'		=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_adoption'	=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_travel'		=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_military'	=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_education'	=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_work'		=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_pet'			=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_lostfound'	=>	'required|in:1,2,3,4,5,6,7,8,9,10',
			        'match_droppel'		=>	'required|in:1,2,3,4,5,6,7,8,9,10'*/
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{

			$settings = User::where('id',$request->user_id)->first();
			$counts_res = DB::select("SELECT (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=1 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."') AND feed_match_count>='".$settings->match_personal."') AS personal_count,
    (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=2 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."') AND feed_match_count>='".$settings->match_dating."') AS dating_count,
    (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=3 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."') AND feed_match_count>='".$settings->match_adoption."') AS adoption_count,
    (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=4 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."') AND feed_match_count>='".$settings->match_travel."') AS travel_count,
    (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=5 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."') AND feed_match_count>='".$settings->match_military."') AS military_count,
    (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=6 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."') AND feed_match_count>='".$settings->match_education."') AS school_count,
    (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=7 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."') AND feed_match_count>='".$settings->match_work."') AS career_count,
    (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=8 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."') AND feed_match_count>='".$settings->match_pet."') AS pets_count,
    (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=9 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."') AND feed_match_count>='".$settings->match_lostfound."') AS lost_count,
    (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=10 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."') AND feed_match_count>='".$settings->match_droppel."') AS doppel_count,
    (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=11 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."') AND feed_match_count>='".$settings->match_username."') AS username_count
FROM users WHERE id='".$request->user_id."'");
			$counts_data = $counts_res ? $counts_res[0] : [];
			$total_matches = 0;
			if($counts_data && isset($counts_data->personal_count)){
				$total_matches = $counts_data->personal_count + $counts_data->dating_count + $counts_data->adoption_count + $counts_data->travel_count + $counts_data->military_count + $counts_data->school_count + $counts_data->career_count + $counts_data->pets_count + $counts_data->lost_count + $counts_data->doppel_count + $counts_data->username_count;
			}
			$settings->total_matches = $total_matches;
			return response([
	                      		'response_code'	=>	'200',
	                			'response_msg'	=> 	'Match setting fetched successfully',
	                  			'reponse_body' 	=> 	['settings' => $settings],
	                  			'redirect'		=>	url('home')
	                  		], 200);
		}
    }

    function setMessageSetting(Request $request){
    	$rules =[
			        'user_id' 			=> 	'required',
			        'allow_notification'=> 	'required',
			        'save_media'		=>	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$allow_notification = ($request->allow_notification && $request->allow_notification==1) ? 1 : 0;
			$save_media = ($request->save_media && $request->save_media==1) ? 1 : 0;
			User::where('id',$request->user_id)->update(	[
													"allow_notification"	=>	$allow_notification,
													'save_media'			=>	$save_media
												]);
			$settings = User::where('id',$request->user_id)->first();
			return response([
	                      		'response_code'=>'200',
	                			'response_msg'=> 'Message setting updated successfully',
	                  			'reponse_body' => ['settings' => $settings]
	                  		], 200);
		}
    }

    function setLinkAccountSetting(Request $request){
    	$rules =[
			        'user_id' 				=> 	'required',
			        'user_share_fb'			=> 	'required',
			        'user_share_twitter'	=>	'required',
			        'user_share_insta'		=> 	'required',
			        'user_share_tagster'	=> 	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$user_share_fb = ($request->user_share_fb && $request->user_share_fb==1) ? 1 : 0;
			$user_share_twitter = ($request->user_share_twitter && $request->user_share_twitter==1) ? 1 : 0;
			$user_share_insta = ($request->user_share_insta && $request->user_share_insta==1) ? 1 : 0;
			$user_share_tagster = ($request->user_share_tagster && $request->user_share_tagster==1) ? 1 : 0;
			User::where('id',$request->user_id)->update(	[
													"user_share_fb"			=>	$user_share_fb,
													'user_share_twitter'	=>	$user_share_twitter,
													'user_share_insta'		=>	$user_share_insta,
													'user_share_tagster'	=>	$user_share_tagster,
												]);
			$settings = User::where('id',$request->user_id)->first();
			return response([
	                      		'response_code'=>'200',
	                			'response_msg'=> 'Link account setting updated successfully',
	                  			'reponse_body' => ['settings' => $settings]
	                  		], 200);
		}
    }

    function getContactSettings(Request $request){
    	$contact_us = DB::table('cms_contact')->find(1);
    	$html = view('admin.xhr.get_contact_setting_modal',['contact_us' => $contact_us])->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function updateContactSettings(Request $request){
    	$rules =[
			        'cmsc_address' 				=> 	'required',
			        'cmsc_phone' 				=> 	'required',
			        'cmsc_email' 				=> 	'required',
			        'cmsc_facebook_link' 		=> 	'required',
			        'cmsc_youtube_link' 		=> 	'required',
			        'cmsc_linkedin_link' 		=> 	'required',
			        'cmsc_insta_link' 			=> 	'required',
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			DB::table('cms_contact')->where('id',1)->update([
													"cmsc_address"		=>	$request->cmsc_address,
													'cmsc_phone'		=>	$request->cmsc_phone,
													'cmsc_email'		=>	$request->cmsc_email,
													'cmsc_facebook_link'=>	$request->cmsc_facebook_link,
													'cmsc_youtube_link'	=>	$request->cmsc_youtube_link,
													'cmsc_linkedin_link'=>	$request->cmsc_linkedin_link,
													'cmsc_insta_link'	=>	$request->cmsc_insta_link
												]);
			
			return response([
	                      		'response_code'	=>	'200',
	                			'response_msg'	=> 	'Contact settings updated successfully',
	                  			'reponse_body'  =>  "null",
                                'close'         =>  TRUE
	                  		], 200);
		}
    }

    function getPremiumPlans(Request $request){

    	$rules =[
			        'user_id' 				=> 	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			//$plans = DB::table('premium_plans')->get();

			$user_active_plans = DB::table('chosen_plans')
									->join('premium_plans','premium_plans.id','=','chosen_plans.cp_plan_id')
									->selectRaw('chosen_plans.*,premium_plans.plan_type')
									->where('cp_user_id',$request->user_id)->where('cp_is_expired',0)->first();

			if(!empty($user_active_plans) && in_array($user_active_plans->cp_inapp_type,['google','apple','paypal','stripe']) && $user_active_plans->plan_type!='lifetime'){

				if($user_active_plans->cp_inapp_type=='google'){
					$response  = (new InAppPurchase)->googleSubscriptionCheck($user_active_plans->cp_inapp_productid,$user_active_plans->cp_inapp_token);
					if($response==FALSE){
						DB::table('chosen_plans')->where('cp_user_id',$request->user_id)->update([
							'cp_is_expired'		=>1,
							'cp_expiry_date' 	=> date('Y-m-d')
						]);
					}
				}elseif($user_active_plans->cp_inapp_type=='paypal'){
	                $res = getAccessToken();
	                $accessToken = $res->access_token;
	                $paypal_res = getSubscriptionDetails($user_active_plans->cp_inapp_productid,$accessToken);
	                if($paypal_res->status!='ACTIVE'){
	                    DB::table('chosen_plans')->where('cp_user_id',$user_id)->update([
	                        'cp_is_expired'     =>1,
	                        'cp_expiry_date'    => date('Y-m-d')
	                    ]);
	                }
	            }else if($user_active_plans->cp_inapp_type=='stripe'){
	                $response  = (new Payment)->checkStripeSubsription($user_active_plans->cp_inapp_productid);
	                /*echo '<pre>';
	                print_r($response);exit;*/
	                if($response->status!='active'){
	                    DB::table('chosen_plans')->where('cp_user_id',$user_id)->update([
	                        'cp_is_expired'     =>1,
	                        'cp_expiry_date'    => date('Y-m-d')
	                    ]);
	                }
	            }
			}

			$plans = DB::table('premium_plans')
					    ->selectRaw('*,(SELECT IF((SELECT COUNT(id) FROM chosen_plans WHERE cp_user_id='.$request->user_id.' AND cp_plan_id=premium_plans.id AND cp_is_expired=0 AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0)) AS is_active
					    	,(SELECT IF((SELECT COUNT(id) FROM chosen_plans WHERE cp_user_id='.$request->user_id.' AND cp_plan_id=premium_plans.id AND cp_is_expired=0 AND cp_inapp_type IS NOT NULL),1,0)) AS is_inapp,
					    	(SELECT cp_inapp_type FROM chosen_plans WHERE cp_user_id='.$request->user_id.' AND cp_plan_id=premium_plans.id AND cp_is_expired=0) AS cp_inapp_type
					    	')
					    ->join('currencies', 'premium_plans.plan_currency', '=', 'currencies._currency_id')
					    ->get();
	    	return response([
	                      		'response_code'=>'200',
	                			'response_msg'=> 'Premium Plans Fetched Successfully',
	                  			'reponse_body' => ['premium_plans' => $plans]
	                  		], 200);
		}
    	
    }

    function choosePlan(Request $request){
    	$req = isset($request->inapp_type) ? 'required|in:google,apple,paypal,stripe' : 'nullable';
    	$req2 = isset($request->inapp_token) ? 'required' : 'nullable';
    	$req3 = isset($request->inapp_productid) ? 'required' : 'nullable';
    	$rules =[
			        'plan_id' 				=> 	'required',
			        'user_id' 				=> 	'required',
			        'inapp_type'			=>	$req,
			        'inapp_token'			=>	$req2,
			        'inapp_productid'		=>	$req3,
			        'link_url'				=>	'nullable',
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$plan_data = DB::table('premium_plans')->find($request->plan_id);
			if($plan_data){
				
				if($plan_data->plan_type=='monthly'){
					$expiry_date = date('Y-m-d', strtotime('+1 month', strtotime(date('Y-m-d'))));
				}else if($plan_data->plan_type=='annually'){
					$expiry_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 365 day"));
				}else{
					$expiry_date = NULL;
				}

				$evaluate = NULL;

				if(isset($request->link_url) && $request->link_url){
					$evaluate = 'window.open("'.$request->link_url.'", "_blank");';
				}
				
				$chosen_plan = 	[
									'cp_user_id'		=>	$request->user_id,
									'cp_plan_id'		=>	$request->plan_id,
									'cp_expiry_date'	=> 	$expiry_date,
									'cp_inapp_type'		=>	$request->inapp_type,
									'cp_inapp_token'	=>	$request->inapp_token,
									'cp_inapp_productid'=>	$request->inapp_productid,
								];
				/*if(isset($request->inapp_type)){
					$chosen_plan['cp_inapp_type']	= $request->inapp_type;
					$chosen_plan['cp_inapp_token']	= $request->inapp_token;
					$chosen_plan['cp_inapp_productid']	= $request->inapp_productid;
				}*/

				/*DB::table('chosen_plans')->where('cp_user_id',$request->user_id)->update([
					'cp_is_expired'		=>1,
					'cp_expiry_date' 	=> date('Y-m-d')
				]);*/
				DB::table('chosen_plans')->where('cp_user_id',$request->user_id)->delete();

				DB::table('chosen_plans')->insert($chosen_plan);

				/*$plans = DB::table('premium_plans')
					    ->selectRaw('*,(SELECT IF((SELECT COUNT(id) FROM chosen_plans WHERE cp_user_id='.$request->user_id.' AND cp_plan_id=premium_plans.id AND cp_is_expired=0 AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0)) AS is_active')
					    ->join('currencies', 'premium_plans.plan_currency', '=', 'currencies._currency_id')
					    ->get();*/
				$plans = DB::table('premium_plans')
					    ->selectRaw('*,(SELECT IF((SELECT COUNT(id) FROM chosen_plans WHERE cp_user_id='.$request->user_id.' AND cp_plan_id=premium_plans.id AND cp_is_expired=0 AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0)) AS is_active
					    	,(SELECT IF((SELECT COUNT(id) FROM chosen_plans WHERE cp_user_id='.$request->user_id.' AND cp_plan_id=premium_plans.id AND cp_is_expired=0 AND cp_inapp_type IS NOT NULL),1,0)) AS is_inapp,
					    	(SELECT cp_inapp_type FROM chosen_plans WHERE cp_user_id='.$request->user_id.' AND cp_plan_id=premium_plans.id AND cp_is_expired=0) AS cp_inapp_type
					    	')
					    ->join('currencies', 'premium_plans.plan_currency', '=', 'currencies._currency_id')
					    ->get();
				if(isset($request->call_from) && $request->call_from=='web'){
					$request->session()->forget('plan_sess');
				}
				return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'Plan Chosen Successfully',
                  				'reponse_body' 	=> 	['premium_plans' => $plans],
                  				'redirect'		=> 	url('premium-plans'),
                  				'evaluate'		=>	$evaluate ? $evaluate : NULL
                  		], 200);
			}else{
				return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Invalid Details Supplied',
                         		'reponse_body' 	=> 	"null"
                        	], 401);
			}
		}
    }

    function cancelPlan(Request $request){
    	$rules =[
			        'plan_id' 				=> 	'required',
			        'user_id' 				=> 	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$plan_data = DB::table('chosen_plans')->where('cp_plan_id',$request->plan_id)->where('cp_user_id',$request->user_id)->where('cp_is_expired',0)->first();
			if($plan_data){
				if($plan_data->cp_inapp_type && $plan_data->cp_inapp_type=='paypal'){
					$res = getAccessToken();
	                $accessToken = $res->access_token;
	                $paypal_res = getSubscriptionDetails($plan_data->cp_inapp_productid,$accessToken);
	                if($paypal_res->status=='ACTIVE'){
	                    $cancel_res = cancelSubcription($plan_data->cp_inapp_productid,$accessToken);
	                }
				}else if($plan_data->cp_inapp_type && $plan_data->cp_inapp_type=='stripe'){
					$response  = (new Payment)->checkStripeSubsription($plan_data->cp_inapp_productid);
                
	                if($response->status=='active'){
	                    (new Payment)->cancelStripeSubsription($plan_data->cp_inapp_productid);
	                }
				}
				else if($plan_data->cp_inapp_type && $plan_data->cp_inapp_type=='google'){
					return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'Redirecting to the Google Play Store',
                  				'reponse_body' 	=> 	NULL,
                  				'redirect'		=>	'https://play.google.com/store/account/subscriptions'
                  		], 200);
				}
				DB::table('chosen_plans')->where('id',$plan_data->id)->update([
					'cp_is_expired'		=>1,
					'cp_expiry_date' 	=> date('Y-m-d')
				]);

				$plans = DB::table('premium_plans')
					    ->selectRaw('*,(SELECT IF((SELECT COUNT(id) FROM chosen_plans WHERE cp_user_id='.$request->user_id.' AND cp_plan_id=premium_plans.id AND cp_is_expired=0 AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0)) AS is_active')
					    ->join('currencies', 'premium_plans.plan_currency', '=', 'currencies._currency_id')
					    ->get();
				return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'Plan Cancelled Successfully',
                  				'reponse_body' 	=> 	['premium_plans' => $plans],
                  				'redirect'		=>	url('premium-plans')
                  		], 200);
			}else{
				return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Invalid Details Supplied',
                         		'reponse_body' 	=> 	"null"
                        	], 401);
			}
		}
    }

    function getCountries(Request $request){
    	$countries = DB::table('countries')->where('country_is_active',1)->orderBy('country_name','asc')->get();
    	return response([
                      		'response_code'=>'200',
                			'response_msg'=> 'Countries Fetched Successfully',
                  			'reponse_body' => ['countries' => $countries]
                  		], 200);
    }

    function getProvince(Request $request){

    	if($request->_country_id){
    		$provinces = DB::table('provinces')->where('province_is_active',1)->where('province_country',$request->_country_id)->orderBy('province_name','asc')->get();
    	}elseif($request->country_name){
    		/*$provinces = DB::table('provinces')->where('province_is_active',1)->where('province_country',$request->_country_id)->orderBy('province_name','asc')->get();*/
    		$provinces = DB::select("SELECT * FROM provinces province_is_active=1 AND province_country IN(SELECT DISTINCT _country_id FROM countries WHERE country_name LIKE '%".$request->country_name."%')");
    	}
    	else{
    		$provinces = DB::table('provinces')->where('province_is_active',1)->orderBy('province_name','asc')->get();
    	}

    	$province = view('xhr.province',['provinces' => $provinces, 'for_life_event' => $request->for_life_event ? 1 : 0])->render();

    	return response([
                      		'response_code'=>'200',
                			'response_msg'=> 'Provinces Fetched Successfully',
                  			'reponse_body' => ['provinces' => $provinces,'province' => $province]
                  		], 200);
    }

    function getCity(Request $request){

    	if($request->_province_id){
    		$cities = DB::table('cities')->where('city_is_active',1)->where('city_province',$request->_province_id)->orderBy('city_name','asc')->get();
    	}else if($request->province_name){
    		$cities = DB::select("SELECT * FROM cities city_is_active=1 AND city_province IN(SELECT DISTINCT _province_id FROM provinces WHERE province_name LIKE '%".$request->province_name."%')");
    	}
    	else{
    		$cities = DB::table('cities')->where('city_is_active',1)->orderBy('city_name','asc')->get();
    	}

    	$city = view('xhr.city',['cities' => $cities, 'for_life_event' => $request->for_life_event ? 1 : 0])->render();

    	return response([
                      		'response_code'=>'200',
                			'response_msg'=> 'Cities Fetched Successfully',
                  			'reponse_body' => ['cities' => $cities, 'city' => $city]
                  		], 200);
    }

    function getCurrencies(Request $request){
    	$currencies = DB::table('currencies')->get();
    	$amt = [];
        if($currencies){
            foreach($currencies as $currency){
                $donation_amounts = DB::table('donation_amounts')
                                    ->where('donation_amounts.da_currency',$currency->_currency_id)
                                    ->orderBy('da_amount','ASC')
                                    ->get();
                $currency->donation_amounts = $donation_amounts ? $donation_amounts : [];

                array_push($amt, $currency);
            }
        }
					
    	return response([
                      		'response_code'=>'200',
                			'response_msg'=> 'Currencies Fetched Successfully',
                  			'reponse_body' => ['currencies' => $amt]
                  		], 200);
    }
}
