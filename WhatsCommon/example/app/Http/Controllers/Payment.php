<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Square\SquareClient;
use Square\LocationsApi;
use Square\Exceptions\ApiException;
use Square\Http\ApiResponse;
use Square\Models\ListLocationsResponse;
use Square\Environment;
$client = NULL;
$location_id = '';
use Stripe;
class Payment extends Controller
{
    //
    public function __construct(){
    	$this->client = new SquareClient([
		    'accessToken' => 'EAAAEFZGjwNsgsp5OhE--BI574qoUVZl5n4CoLLGMIZJk4YLUmT2t7ks3So3wegh',
		    'environment' => Environment::SANDBOX,
		]);
		$this->location_id = 'L8ESE6MCAQRTC';
    }

    function createCheckOut($amount,$currncy='USD',$payment_for='Test Payment',$redirect_url='premium-plans'){
    	$idempotencykey_tax = $this->createIdEmpotencyKey();
    	$idempotencykey_disc = $this->createIdEmpotencyKey();
    	$base_price_money = new \Square\Models\Money();
		$base_price_money->setAmount((int)($amount*100));
		$base_price_money->setCurrency($currncy);

		$order_line_item = new \Square\Models\OrderLineItem('1');
		$order_line_item->setName($payment_for);
		$order_line_item->setBasePriceMoney($base_price_money);

		$line_items = [$order_line_item];
		$order1 = new \Square\Models\Order($this->location_id);
		$order1->setLineItems($line_items);

		$order = new \Square\Models\CreateOrderRequest();
		$order->setOrder($order1);
		$order->setIdempotencyKey($this->createIdEmpotencyKey());

		$body = new \Square\Models\CreateCheckoutRequest($this->createIdEmpotencyKey(), $order);
		$body->setAskForShippingAddress(false);
		$body->setRedirectUrl(url($redirect_url));

		/*$api_response = $client->getCheckoutApi()->createCheckout($this->location_id, $body);

		if ($api_response->isSuccess()) {
		    $result = $api_response->getResult();
		} else {
		    $errors = $api_response->getErrors();
		}*/

		$api_response = $this->client->getCheckoutApi()->createCheckout($this->location_id, $body);

		$response = [];
		if ($api_response->isSuccess()) {
		    $result = $api_response->getResult();
		    $response = ['success' => 1,'result'=>json_encode($result,JSON_UNESCAPED_SLASHES)];
		} else {
		    $errors = $api_response->getErrors();
		    $response = ['success' => 0,'result'=>json_encode($errors,JSON_UNESCAPED_SLASHES)];
		}

		return $response;
    }

    function createPayment($amount,$currncy='USD',$payment_for='Test Payment'){

    	
    	$amount_money = new \Square\Models\Money();
		$amount_money->setAmount($amount);
		$amount_money->setCurrency($currncy);

		$external_details = new \Square\Models\ExternalPaymentDetails('OTHER', $payment_for);

		$idempotencykey = $this->createIdEmpotencyKey();

		$body = new \Square\Models\CreatePaymentRequest(
		    'EXTERNAL',
		    $idempotencykey,
		    $amount_money
		);
		$body->setExternalDetails($external_details);

		$api_response = $this->client->getPaymentsApi()->createPayment($body);
		$response = [];
		if ($api_response->isSuccess()) {
		    $result = $api_response->getResult();
		    $response = ['success' => 1,'result'=>$result];
		} else {
		    $errors = $api_response->getErrors();
		    $response = ['success' => 0,'result'=>$errors];
		}

		return $response;
    }

    function createIdEmpotencyKey(){
    	$str_result = '0123456789abcdefghijklmnopqrstuvwxyz';
    	$str1 = substr(str_shuffle($str_result), 0, 8);
    	$str2 = substr(str_shuffle($str_result), 0, 4);
    	$str3 = substr(str_shuffle($str_result), 0, 4);
    	$str4 = substr(str_shuffle($str_result), 0, 4);
    	$str5 = substr(str_shuffle($str_result), 0, 12);

    	return $str1.'-'.$str2.'-'.$str3.'-'.$str4.'-'.$str5;
    }

    function createStripePayment($price_id,$success_url='payment-success',$cancel_url='payment-failure'){
    	$stripe = Stripe\Stripe::setApiKey('sk_test_51LKpnUH6F5jONBiWCDOKKqJcDOPQ8Y2QaMuP077mOdeU9CkxL3sTKTW2IfJ41vLAKLXbxrSqOuMeXtMg3RFyXFIJ00WzzAdo2y');

        $payout = Stripe\Checkout\Session::create([
			  'line_items' => 	[[
							    	'price' => $price_id,
							    	'quantity' => 1,
  								]],
			  					'mode' => 'payment',
			  					'success_url' => url($success_url),
			  					'cancel_url' => url($cancel_url),
			  					'automatic_tax' => ['enabled' => false,],
							]);
       	return $payout;
    }

    function createStripeSubscription($price_id,$customer_id,$success_url='payment-success',$cancel_url='payment-failure'){
    	$stripe = Stripe\Stripe::setApiKey('sk_test_51LKpnUH6F5jONBiWCDOKKqJcDOPQ8Y2QaMuP077mOdeU9CkxL3sTKTW2IfJ41vLAKLXbxrSqOuMeXtMg3RFyXFIJ00WzzAdo2y');

        $payout = Stripe\Subscription::create([
			  					'customer' => $customer_id,
					  			'items' => [
					    			['price' => $price_id],
					  			],
					  			'payment_behavior' => 'default_incomplete',
        						'payment_settings' => ['save_default_payment_method' => 'on_subscription'],
        						'expand' => ['latest_invoice.payment_intent'],
        						/*'collection_method' => 'send_invoice',
        						'days_until_due' => 0*/
							]);
       	return $payout;
    }

    function createStripeUser($email,$name=NULL){
    	
		$stripe = Stripe\Stripe::setApiKey('sk_test_51LKpnUH6F5jONBiWCDOKKqJcDOPQ8Y2QaMuP077mOdeU9CkxL3sTKTW2IfJ41vLAKLXbxrSqOuMeXtMg3RFyXFIJ00WzzAdo2y');
		$account = Stripe\Customer::create([
											  'description' => $email,
											  'name' => $name,
											  'email' => $email/*,
											  'capabilities' => [
											    'card_payments' => ['requested' => true],
											    'transfers' => ['requested' => true],
											  ],*/
											]);
		return 	$account;
    }

    function checkStripeSubsription($subscriptionID){
    	$stripe = Stripe\Stripe::setApiKey('sk_test_51LKpnUH6F5jONBiWCDOKKqJcDOPQ8Y2QaMuP077mOdeU9CkxL3sTKTW2IfJ41vLAKLXbxrSqOuMeXtMg3RFyXFIJ00WzzAdo2y');

    	$payout = Stripe\Subscription::retrieve($subscriptionID,[]);
       	return $payout;
    }

    function cancelStripeSubsription($subscriptionID){
    	/*$stripe = Stripe\Stripe::setApiKey('sk_test_51LKpnUH6F5jONBiWCDOKKqJcDOPQ8Y2QaMuP077mOdeU9CkxL3sTKTW2IfJ41vLAKLXbxrSqOuMeXtMg3RFyXFIJ00WzzAdo2y');

    	$payout = Stripe\Subscription::cancel($subscriptionID,[]);
       	return $payout;*/

       	$stripe = new \Stripe\StripeClient(
		  'sk_test_51LKpnUH6F5jONBiWCDOKKqJcDOPQ8Y2QaMuP077mOdeU9CkxL3sTKTW2IfJ41vLAKLXbxrSqOuMeXtMg3RFyXFIJ00WzzAdo2y'
		);
		$res = $stripe->subscriptions->cancel(
		  $subscriptionID,
		  []
		);

		return $res;
    }

}
