<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use View;
use Validator;

class Master extends Controller
{
    function GenderList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }

    	$genders = DB::table('genders')->get();

    	return View::make("admin/genders_list")->with(['genders' => $genders]);
    }

    function addGenderModal(Request $request){
        $html = view('admin.xhr.add_gender_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function addGender(Request $request){
        $rules =[
                    'gender_name'   =>  'required|unique:genders',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('genders')->insert([
                                                        'gender_name' => $request->gender_name
                                                    ]);

            if(!$done){
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else{
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Gender is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function activeInactiveGender(Request $request){
        $rules =[
                    '_gender_id'          =>  'required',
                    'gender_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $gender_is_active = $request->gender_is_active==1 ? 0 : 1;
            DB::table('genders')->where('_gender_id',$request->_gender_id)->update(['gender_is_active' => $gender_is_active]);
            $active_inactive = $gender_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Gender '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function editGenderModal(Request $request){
        $rules =[
                    '_gender_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $gender_data = DB::table('genders')->where('_gender_id', $request->_gender_id)->first();
            $html = view('admin.xhr.edit_gender_modal',['gender_data'=> $gender_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editGender(Request $request){
        $rules =[
                    'gender_name'     =>  'required',
                    '_gender_id'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('genders')->where('_gender_id', $request->_gender_id)->first();
            if($title_data){
                DB::table('genders')->where('_gender_id', $request->_gender_id)->update([
                                                        'gender_name'      =>  $request->gender_name
                                                    ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Gender is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    function RelationList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
    	$relations = DB::table('relations')->get();

    	return View::make("admin/relations_list")->with(['relations' => $relations]);
    }

    function addRelationModal(Request $request){
        $html = view('admin.xhr.add_relation_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function addRelation(Request $request){
        $rules =[
                    'relation_name'   =>  'required|unique:relations',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('relations')->insert([
                                                        'relation_name' => $request->relation_name
                                                    ]);

            if(!$done){
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else{
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Relation is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function activeInactiveRelation(Request $request){
        $rules =[
                    '_relation_id'          =>  'required',
                    'relation_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $relation_is_active = $request->relation_is_active==1 ? 0 : 1;
            DB::table('relations')->where('_relation_id',$request->_relation_id)->update(['relation_is_active' => $relation_is_active]);
            $active_inactive = $relation_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Relation '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function editRelationModal(Request $request){
        $rules =[
                    '_relation_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $relation_data = DB::table('relations')->where('_relation_id', $request->_relation_id)->first();
            $html = view('admin.xhr.edit_relation_modal',['relation_data'=> $relation_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editRelation(Request $request){
        $rules =[
                    'relation_name'     =>  'required',
                    '_relation_id'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('relations')->where('_relation_id', $request->_relation_id)->first();
            if($title_data){
                DB::table('relations')->where('_relation_id', $request->_relation_id)->update([
                                                        'relation_name'      =>  $request->relation_name
                                                    ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Relation is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    function HomeStyleList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
    	$home_styles = DB::table('home_styles')->get();

    	return View::make("admin/home_styles_list")->with(['home_styles' => $home_styles]);
    }

    function addHomeStyleModal(Request $request){

        $html = view('admin.xhr.add_home_style_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function addHomeStyle(Request $request){
        $rules =[
                    'home_style_name'   =>  'required|unique:home_styles',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('home_styles')->insert([
                                                        'home_style_name' => $request->home_style_name
                                                    ]);

            if(!$done)
            {
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else
            {
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Home style is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function activeInactiveHomeStyle(Request $request){
        $rules =[
                    '_home_style_id'          =>  'required',
                    'home_style_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $home_style_is_active = $request->home_style_is_active==1 ? 0 : 1;
            DB::table('home_styles')->where('id',$request->_home_style_id)->update(['home_style_is_active' => $home_style_is_active]);
            $active_inactive = $home_style_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Home style '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function editHomeStyleModal(Request $request){
        $rules =[
                    '_home_style_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $relation_data = DB::table('home_styles')->where('id', $request->_home_style_id)->first();
            $html = view('admin.xhr.edit_home_style_modal',['relation_data'=> $relation_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editHomeStyle(Request $request){
        $rules =[
                    'home_style_name'     =>  'required',
                    '_home_style_id'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('home_styles')->where('id', $request->_home_style_id)->first();
            if($title_data){
                DB::table('home_styles')->where('id', $request->_home_style_id)->update([
                                                        'home_style_name'      =>  $request->home_style_name
                                                    ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Home style is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    function HomeTypeList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
    	$home_types = DB::table('home_types')->get();

    	return View::make("admin/home_types_list")->with(['home_types' => $home_types]);
    }

    function addHomeTypeModal(Request $request){
        
        $html = view('admin.xhr.add_home_type_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function addHomeType(Request $request){
        $rules =[
                    'home_type_name'   =>  'required|unique:home_types',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('home_types')->insert([
                                                        'home_type_name' => $request->home_type_name
                                                    ]);

            if(!$done)
            {
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else
            {
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Home type is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function activeInactiveHomeType(Request $request){
        $rules =[
                    '_home_type_id'          =>  'required',
                    'home_type_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $home_type_is_active = $request->home_type_is_active==1 ? 0 : 1;
            DB::table('home_types')->where('id',$request->_home_type_id)->update(['home_type_is_active' => $home_type_is_active]);
            $active_inactive = $home_type_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Home type '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function editHomeTypeModal(Request $request){
        $rules =[
                    '_home_type_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $relation_data = DB::table('home_types')->where('id', $request->_home_type_id)->first();
            $html = view('admin.xhr.edit_home_type_modal',['relation_data'=> $relation_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editHomeType(Request $request){
        $rules =[
                    'home_type_name'     =>  'required',
                    '_home_type_id'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('home_types')->where('id', $request->_home_type_id)->first();
            if($title_data){
                DB::table('home_types')
                    ->where('id', $request->_home_type_id)
                    ->update([
                                'home_type_name'      =>  $request->home_type_name
                            ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Home type is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    function MaritalStatusList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
    	$marital_status = DB::table('marital_status')->get();

    	return View::make("admin/marital_status_list")->with(['marital_status' => $marital_status]);
    }

    function addMaritalStatusModal(Request $request){
        $html = view('admin.xhr.add_marital_status_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function addMaritalStatus(Request $request){
        $rules =[
                    'ms_name'   =>  'required|unique:marital_status',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('marital_status')->insert([
                                                        'ms_name' => $request->ms_name
                                                    ]);

            if(!$done)
            {
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else
            {
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Marital status is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function activeInactiveMaritalStatus(Request $request){
        $rules =[
                    '_ms_id'          =>  'required',
                    'ms_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $ms_is_active = $request->ms_is_active==1 ? 0 : 1;
            DB::table('marital_status')->where('_ms_id',$request->_ms_id)->update(['ms_is_active' => $ms_is_active]);
            $active_inactive = $ms_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Marital status '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function editMaritalStatusModal(Request $request){
        $rules =[
                    '_ms_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $relation_data = DB::table('marital_status')->where('_ms_id', $request->_ms_id)->first();
            $html = view('admin.xhr.edit_marital_status_modal',['relation_data'=> $relation_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editMaritalStatus(Request $request){
        $rules =[
                    'ms_name'     =>  'required',
                    '_ms_id'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('marital_status')->where('_ms_id', $request->_ms_id)->first();
            if($title_data){
                DB::table('marital_status')
                    ->where('_ms_id', $request->_ms_id)
                    ->update([
                                'ms_name'      =>  $request->ms_name
                            ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Marital status is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    function KeywordsList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
    	$keywords = DB::table('keywords')->get();

    	return View::make("admin/keywords_list")->with(['keywords' => $keywords]);
    }

    function VehicleTypeList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
    	$vehicle_types = DB::table('vehicle_types')->get();

    	return View::make("admin/vehicle_types_list")->with(['vehicle_types' => $vehicle_types]);
    }

    function addVehicleTypeModal(Request $request){
        $html = view('admin.xhr.add_vehicle_type_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function addVehicleType(Request $request){
        $rules =[
                    'vehicle_type_name'   =>  'required|unique:vehicle_types',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('vehicle_types')->insert([
                                                        'vehicle_type_name'         => $request->vehicle_type_name,
                                                        'vehicle_type_subcategory'  => 2
                                                    ]);

            if(!$done)
            {
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else
            {
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Vehicle type is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function activeInactiveVehicleType(Request $request){
        $rules =[
                    '_vehicle_type_id'          =>  'required',
                    'vehicle_type_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $vehicle_type_is_active = $request->vehicle_type_is_active==1 ? 0 : 1;
            DB::table('vehicle_types')->where('id',$request->_vehicle_type_id)->update(['vehicle_type_is_active' => $vehicle_type_is_active]);
            $active_inactive = $vehicle_type_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Home type '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function editVehicleTypeModal(Request $request){
        $rules =[
                    '_vehicle_type_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $relation_data = DB::table('vehicle_types')->where('id', $request->_vehicle_type_id)->first();
            $html = view('admin.xhr.edit_vehicle_type_modal',['relation_data'=> $relation_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editVehicleType(Request $request){
        $rules =[
                    'vehicle_type_name'     =>  'required',
                    '_vehicle_type_id'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('vehicle_types')->where('id', $request->_vehicle_type_id)->first();
            if($title_data){
                DB::table('vehicle_types')
                    ->where('id', $request->_vehicle_type_id)
                    ->update([
                                'vehicle_type_name'      =>  $request->vehicle_type_name
                            ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Vehicle type is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    function VehicleMakerList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $vehicle_makers = DB::table('vehicle_makers')
                            ->join('vehicle_types', 'vehicle_types.id', '=', 'vehicle_makers.maker_vehicle_type')
                            ->orderBy('maker_vehicle_type','ASC')
                            ->get();
        $vehicle_types  = DB::table('vehicle_types')->get();

        if($request->type){
            $vehicle_makers = DB::table('vehicle_makers')
                                ->join('vehicle_types', 'vehicle_types.id', '=', 'vehicle_makers.maker_vehicle_type')
                                ->where('maker_vehicle_type',$request->type)
                                ->orderBy('maker_vehicle_type','ASC')
                                ->get();
        }

        return View::make("admin/vehicle_makers_list")->with(['vehicle_makers' => $vehicle_makers,'vehicle_types' => $vehicle_types]);
    }

    function addVehicleMakerModal(Request $request){
        $vehicle_types  = DB::table('vehicle_types')->get();
        $html = view('admin.xhr.add_vehicle_maker_modal',['vehicle_types'=>$vehicle_types])->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function addVehicleMaker(Request $request){
        $rules =[
                    'maker_name'            =>  'required',
                    'maker_vehicle_type'    =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('vehicle_makers')->insert([
                                                        'maker_name'            => $request->maker_name,
                                                        'maker_vehicle_type'    => $request->maker_vehicle_type
                                                    ]);

            if(!$done)
            {
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else
            {
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Vehicle type is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function activeInactiveVehicleMaker(Request $request){
        $rules =[
                    '_maker_id'          =>  'required',
                    'maker_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $maker_is_active = $request->maker_is_active==1 ? 0 : 1;
            DB::table('vehicle_makers')->where('_maker_id',$request->_maker_id)->update(['maker_is_active' => $maker_is_active]);
            $active_inactive = $maker_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Vehicle maker '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function editVehicleMakerModal(Request $request){
        $rules =[
                    '_maker_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $vehicle_types  = DB::table('vehicle_types')->get();
            $rank_data = DB::table('vehicle_makers')->where('_maker_id', $request->_maker_id)->first();
            $html = view('admin.xhr.edit_vehicle_maker_modal',['rank_data'=> $rank_data,'vehicle_types'=>$vehicle_types])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editVehicleMaker(Request $request){
        $rules =[
                    'maker_name'        =>  'required',
                    'maker_vehicle_type'=>  'required',
                    '_maker_id'         =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('vehicle_makers')->where('_maker_id', $request->_maker_id)->first();
            if($title_data){
                DB::table('vehicle_makers')->where('_maker_id', $request->_maker_id)->update([
                                                        'maker_name'      =>  $request->maker_name,
                                                        'maker_vehicle_type'      =>  $request->maker_vehicle_type
                                                    ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Vehicle maker is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    function VehicleColorList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $vehicle_color = DB::table('vehicle_color')->get();

        return View::make("admin/vehicle_color_list")->with(['vehicle_color' => $vehicle_color]);
    }

    function addVehicleColorModal(Request $request){
        $html = view('admin.xhr.add_vehicle_color_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function addVehicleColor(Request $request){
        $rules =[
                    'color_name'   =>  'required|unique:vehicle_color',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('vehicle_color')->insert([
                                                        'color_name'         => $request->color_name
                                                    ]);

            if(!$done)
            {
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else
            {
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Vehicle Color is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function activeInactiveVehicleColor(Request $request){
        $rules =[
                    '_color_id'          =>  'required',
                    'color_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $color_is_active = $request->color_is_active==1 ? 0 : 1;
            DB::table('vehicle_color')->where('_color_id',$request->_color_id)->update(['color_is_active' => $color_is_active]);
            $active_inactive = $color_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Vehicle color '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function editVehicleColorModal(Request $request){
        $rules =[
                    '_color_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $relation_data = DB::table('vehicle_color')->where('_color_id', $request->_color_id)->first();
            $html = view('admin.xhr.edit_vehicle_color_modal',['relation_data'=> $relation_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editVehicleColor(Request $request){
        $rules =[
                    'color_name'     =>  'required',
                    '_color_id'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('vehicle_color')->where('_color_id', $request->_color_id)->first();
            if($title_data){
                DB::table('vehicle_color')
                    ->where('_color_id', $request->_color_id)
                    ->update([
                                'color_name'      =>  $request->color_name
                            ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Vehicle color is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    function IndustryList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $industries = DB::table('industries')->get();

        return View::make("admin/industries_list")->with(['industries' => $industries]);
    }

    function addIndustryModal(Request $request){
        $html = view('admin.xhr.add_industry_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function addIndustry(Request $request){
        $rules =[
                    'industry_name'   =>  'required|unique:industries',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('industries')->insert([
                                                        'industry_name'         => $request->industry_name
                                                    ]);

            if(!$done)
            {
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else
            {
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Industry is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function activeInactiveIndustry(Request $request){
        $rules =[
                    '_industry_id'          =>  'required',
                    'industry_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $industry_is_active = $request->industry_is_active==1 ? 0 : 1;
            DB::table('industries')->where('_industry_id',$request->_industry_id)->update(['industry_is_active' => $industry_is_active]);
            $active_inactive = $industry_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Industry '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function editIndustryModal(Request $request){
        $rules =[
                    '_industry_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $relation_data = DB::table('industries')->where('_industry_id', $request->_industry_id)->first();
            $html = view('admin.xhr.edit_industry_modal',['relation_data'=> $relation_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editIndustry(Request $request){
        $rules =[
                    'industry_name'     =>  'required',
                    '_industry_id'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('industries')->where('_industry_id', $request->_industry_id)->first();
            if($title_data){
                DB::table('industries')
                    ->where('_industry_id', $request->_industry_id)
                    ->update([
                                'industry_name'      =>  $request->industry_name
                            ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Industry is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    function CategoryList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $occupations = DB::table('occupations')->get();

        return View::make("admin/occupations_list")->with(['occupations' => $occupations]);
    }

    function addCategoryModal(Request $request){
        $html = view('admin.xhr.add_category_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function addCategory(Request $request){
        $rules =[
                    'occupation_name'   =>  'required|unique:occupations',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('occupations')->insert([
                                                        'occupation_name'         => $request->occupation_name
                                                    ]);

            if(!$done)
            {
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else
            {
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Category is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function activeInactiveOccupation(Request $request){
        $rules =[
                    '_occupation_id'          =>  'required',
                    'occupation_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $occupation_is_active = $request->occupation_is_active==1 ? 0 : 1;
            DB::table('occupations')->where('_occupation_id',$request->_occupation_id)->update(['occupation_is_active' => $occupation_is_active]);
            $active_inactive = $occupation_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Category '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function editCategoryModal(Request $request){
        $rules =[
                    '_occupation_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $relation_data = DB::table('occupations')->where('_occupation_id', $request->_occupation_id)->first();
            $html = view('admin.xhr.edit_category_modal',['relation_data'=> $relation_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editCategory(Request $request){
        $rules =[
                    'occupation_name'     =>  'required',
                    '_occupation_id'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('occupations')->where('_occupation_id', $request->_occupation_id)->first();
            if($title_data){
                DB::table('occupations')
                    ->where('_occupation_id', $request->_occupation_id)
                    ->update([
                                'occupation_name'      =>  $request->occupation_name
                            ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Category is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    function CountryList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
    	$countries = DB::table('countries')->get();

    	return View::make("admin/countries_list")->with(['countries' => $countries]);
    }

    function activeInactiveCountry(Request $request){
        $rules =[
                    '_country_id'          =>  'required',
                    'country_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $country_is_active = $request->country_is_active==1 ? 0 : 1;
            DB::table('countries')->where('_country_id',$request->_country_id)->update(['country_is_active' => $country_is_active]);
            $active_inactive = $country_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Country '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function ProvincesList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
    	if($request->_country_id){
    		$provinces = DB::table('provinces')->leftJoin('countries', 'countries._country_id', '=', 'provinces.province_country')->where('province_country',$request->_country_id)->get();
    	}else{
    		$provinces = DB::table('provinces')->leftJoin('countries', 'countries._country_id', '=', 'provinces.province_country')->get();
    	}

    	return View::make("admin/provinces_list")->with(['provinces' => $provinces]);
    }

    function addProvinceModal(Request $request){
        $countries = DB::table('countries')->get();
        $html = view('admin.xhr.add_province_modal',['countries' => $countries])->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function addProvince(Request $request){
        $rules =[
                    'province_name'     =>  'required',
                    'province_country'  =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('provinces')->insert([
                                                        'province_name'         =>  $request->province_name,
                                                        'province_country'      =>  $request->province_country
                                                    ]);

            if(!$done)
            {
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else
            {
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Province is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function activeInactiveProvince(Request $request){
        $rules =[
                    '_province_id'          =>  'required',
                    'province_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $province_is_active = $request->province_is_active==1 ? 0 : 1;
            DB::table('provinces')->where('_province_id',$request->_province_id)->update(['province_is_active' => $province_is_active]);
            $active_inactive = $province_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Province '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function CitiesList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
    	$cities =   DB::table('cities')
                        ->leftJoin('provinces', 'provinces._province_id', '=', 'cities.city_province')
                        ->leftJoin('countries', 'countries._country_id', '=', 'provinces.province_country')
                        ->get();

        if($request->_country_id){
            $cities = DB::table('cities')
                        ->leftJoin('provinces', 'provinces._province_id', '=', 'cities.city_province')
                        ->leftJoin('countries', 'countries._country_id', '=', 'provinces.province_country')
                        ->where('province_country',$request->_country_id)
                        ->get();
        }else if($request->_province_id){
            $cities = DB::table('cities')
                        ->leftJoin('provinces', 'provinces._province_id', '=', 'cities.city_province')
                        ->leftJoin('countries', 'countries._country_id', '=', 'provinces.province_country')
                        ->where('city_province',$request->_province_id)
                        ->get();
        }

    	return View::make("admin/cities_list")->with(['cities' => $cities]);
    }

    function addCityModal(Request $request){
        $provinces = DB::table('provinces')->leftJoin('countries', 'countries._country_id', '=', 'provinces.province_country')->get();
        $html = view('admin.xhr.add_city_modal',['provinces' => $provinces])->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function addCity(Request $request){
        $rules =[
                    'city_name'         =>  'required',
                    'city_province'     =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('cities')->insert([
                                                        'city_name'         =>  $request->city_name,
                                                        'city_province'      =>  $request->city_province
                                                    ]);

            if(!$done)
            {
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else
            {
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'City is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function activeInactiveCity(Request $request){
        $rules =[
                    '_city_id'          =>  'required',
                    'city_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $city_is_active = $request->city_is_active==1 ? 0 : 1;
            DB::table('cities')->where('_city_id',$request->_city_id)->update(['city_is_active' => $city_is_active]);
            $active_inactive = $city_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'City '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function StatesList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $cities = DB::table('cities')->get();

        return View::make("admin/cities_list")->with(['cities' => $cities]);
    }

    function donationAmountsList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }

        $currencies = DB::table('currencies')->get();

        if($request->currency){
            $donation_amounts = DB::table('donation_amounts')
                                    ->join('currencies','donation_amounts.da_currency','=','currencies._currency_id')
                                    ->where('currencies._currency_id',$request->currency)
                                    ->orderBy('currency_name','ASC')
                                    ->orderBy('da_amount','ASC')
                                    ->get();
        }else{
            $donation_amounts = DB::table('donation_amounts')
                                    ->join('currencies','donation_amounts.da_currency','=','currencies._currency_id')
                                    ->orderBy('currency_name','ASC')
                                    ->orderBy('da_amount','ASC')
                                    ->get();
        }
        return View::make("admin/donation_amount_list")->with(['currencies' => $currencies, 'donation_amounts' => $donation_amounts]);
    }

    function addDonationAmountModal(Request $request){
        $currencies = DB::table('currencies')->get();
        $html = view('admin.xhr.add_donation_amount_modal',['currencies' => $currencies])->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function addDonationAmount(Request $request){
        $rules =[
                    'da_currency'   =>  'required',
                    'da_amount'     =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('donation_amounts')->insert([
                                                        'da_currency'    =>  $request->da_currency,
                                                        'da_amount'      =>  $request->da_amount
                                                    ]);

            if(!$done)
            {
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else
            {
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Amount is added',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function editDonationAmountModal(Request $request){
        $rules =[
                    '_da_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $currencies = DB::table('currencies')->get();
            $donation_data = DB::table('donation_amounts')->where('_da_id', $request->_da_id)->first();
            $html = view('admin.xhr.edit_donation_amount_modal',['donation_data'=> $donation_data,'currencies' => $currencies])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editDonationAmount(Request $request){
        $rules =[
                    'da_currency'   =>  'required',
                    'da_amount'     =>  'required',
                    '_da_id'        =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $donation_data = DB::table('donation_amounts')->where('_da_id', $request->_da_id)->first();
            if($donation_data){
                DB::table('donation_amounts')->where('_da_id', $request->_da_id)->update([
                                                        'da_currency'    =>  $request->da_currency,
                                                        'da_amount'      =>  $request->da_amount
                                                    ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Amount is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    public function afterSchoolsList(Request $request){

        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $after_schools = DB::table('after_schools')->get();

        return View::make("admin/after_schools_list")->with(['after_schools' => $after_schools]);
    }

    public function addAfterSchoolsModal(Request $request){
        $html = view('admin.xhr.add_after_schools_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    public function addAfterSchools(Request $request){
        $rules =[
                    'as_name'   =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('after_schools')->insert([
                                                        'as_name'    =>  $request->as_name
                                                    ]);
            if(!$done){
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else{
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Amount is added',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    public function activeInactiveAfterSchools(Request $request){
        $rules =[
                    '_as_id'          =>  'required',
                    'as_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $as_is_active = $request->as_is_active==1 ? 0 : 1;
            DB::table('after_schools')->where('_as_id',$request->_as_id)->update(['as_is_active' => $as_is_active]);
            $active_inactive = $as_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'After school '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function editAfterSchoolsModal(Request $request){
        $rules =[
                    '_as_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $range_data = DB::table('after_schools')->where('_as_id', $request->_as_id)->first();
            $html = view('admin.xhr.edit_after_schools_modal',['range_data'=> $range_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editAfterSchools(Request $request){
        $rules =[
                    'as_name'     =>  'required',
                    '_as_id'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('after_schools')->where('_as_id', $request->_as_id)->first();
            if($title_data){
                DB::table('after_schools')->where('_as_id', $request->_as_id)->update([
                                                        'as_name'      =>  $request->as_name
                                                    ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'After school is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    public function ageRangesList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $age_ranges = DB::table('age_ranges')->get();

        return View::make("admin/age_ranges_list")->with(['age_ranges' => $age_ranges]);
    }

    public function activeInactiveAgeRanges(Request $request){
        $rules =[
                    '_range_id'          =>  'required',
                    'range_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $range_is_active = $request->range_is_active==1 ? 0 : 1;
            DB::table('age_ranges')->where('_range_id',$request->_range_id)->update(['range_is_active' => $range_is_active]);
            $active_inactive = $range_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Age range '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    public function addAgeRangeModal(Request $request){
        $html = view('admin.xhr.add_age_range_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    public function addAgeRange(Request $request){
        $rules =[
                    'range_name'   =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('age_ranges')->insert([
                                                        'range_name'    =>  $request->range_name
                                                    ]);
            if(!$done){
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else{
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Age range is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function editAgeRangeModal(Request $request){
        $rules =[
                    '_range_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $range_data = DB::table('age_ranges')->where('_range_id', $request->_range_id)->first();
            $html = view('admin.xhr.edit_age_range_modal',['range_data'=> $range_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editAgeRange(Request $request){
        $rules =[
                    'range_name'     =>  'required',
                    '_range_id'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('age_ranges')->where('_range_id', $request->_range_id)->first();
            if($title_data){
                DB::table('age_ranges')->where('_range_id', $request->_range_id)->update([
                                                        'range_name'      =>  $request->range_name
                                                    ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Age range is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    public function militaryBranchesList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $military_branches = DB::table('military_branches')->orderBy('mbranch_name', 'asc')->get();

        return View::make("admin/military_branches_list")->with(['military_branches' => $military_branches]);
    }

    public function addMilitaryBranchModal(Request $request){
        $html = view('admin.xhr.add_military_branch_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    public function addMilitaryBranch(Request $request){
        $rules =[
                    'mbranch_name'   =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('military_branches')->insert([
                                                        'mbranch_name'    =>  $request->mbranch_name
                                                    ]);
            if(!$done){
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else{
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Branch is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function editMilitaryBranchModal(Request $request){
        $rules =[
                    '_mbranch_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $branch_data = DB::table('military_branches')->where('_mbranch_id', $request->_mbranch_id)->first();
            $html = view('admin.xhr.edit_military_branch_modal',['branch_data'=> $branch_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editMilitaryBranch(Request $request){
        $rules =[
                    'mbranch_name'     =>  'required',
                    '_mbranch_id'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('military_branches')->where('_mbranch_id', $request->_mbranch_id)->first();
            if($title_data){
                DB::table('military_branches')->where('_mbranch_id', $request->_mbranch_id)->update([
                                                        'mbranch_name'      =>  $request->mbranch_name
                                                    ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Branch is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    public function militaryRanksList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        if($request->branch_id){
            $military_ranks = DB::table('military_ranks')
                                ->join('military_branches','military_ranks.rank_branch_id','=','military_branches._mbranch_id')
                                ->where('rank_branch_id','=',$request->branch_id)
                                ->orderBy('mbranch_name', 'asc')
                                ->orderBy('rank_name', 'asc')
                                ->get();
        }else{
            $military_ranks = DB::table('military_ranks')
                                ->join('military_branches','military_ranks.rank_branch_id','=','military_branches._mbranch_id')
                                ->orderBy('mbranch_name', 'asc')
                                ->orderBy('rank_name', 'asc')
                                ->get();
        }
        

        return View::make("admin/military_ranks_list")->with(['military_ranks' => $military_ranks]);
    }

    public function addMilitaryRankModal(Request $request){
        $military_branches = DB::table('military_branches')->orderBy('mbranch_name', 'asc')->get();
        $html = view('admin.xhr.add_military_rank_modal',['military_branches' => $military_branches])->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    public function addMilitaryRank(Request $request){
        $rules =[
                    'rank_name'         =>  'required',
                    'rank_branch_id'    =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('military_ranks')->insert([
                                                        'rank_name'        =>  $request->rank_name,
                                                        'rank_branch_id'   =>  $request->rank_branch_id
                                                    ]);
            if(!$done){
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else{
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Rank is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function editMilitaryRankModal(Request $request){
        $rules =[
                    '_rank_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $military_branches = DB::table('military_branches')->orderBy('mbranch_name', 'asc')->get();
            $rank_data = DB::table('military_ranks')->where('_rank_id', $request->_rank_id)->first();
            $html = view('admin.xhr.edit_military_rank_modal',['rank_data'=> $rank_data,'military_branches'=>$military_branches])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editMilitaryRank(Request $request){
        $rules =[
                    'rank_name'     =>  'required',
                    'rank_branch_id'     =>  'required',
                    '_rank_id'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('military_ranks')->where('_rank_id', $request->_rank_id)->first();
            if($title_data){
                DB::table('military_ranks')->where('_rank_id', $request->_rank_id)->update([
                                                        'rank_name'      =>  $request->rank_name,
                                                        'rank_branch_id'      =>  $request->rank_branch_id
                                                    ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Rank is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    public function militaryBasesList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $military_bases = DB::table('military_bases')->orderBy('base_name', 'asc')->get();

        return View::make("admin/military_bases_list")->with(['military_bases' => $military_bases]);
    }

    public function addMilitaryBaseModal(Request $request){
        $html = view('admin.xhr.add_military_base_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    public function addMilitaryBase(Request $request){
        $rules =[
                    'base_name'         =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('military_bases')->insert([
                                                        'base_name'        =>  $request->base_name
                                                    ]);
            if(!$done){
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else{
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Base is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function editMilitaryBaseModal(Request $request){
        $rules =[
                    '_base_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $base_data = DB::table('military_bases')->where('_base_id', $request->_base_id)->first();
            $html = view('admin.xhr.edit_military_base_modal',['base_data'=> $base_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editMilitaryBase(Request $request){
        $rules =[
                    'base_name'     =>  'required',
                    '_base_id'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('military_bases')->where('_base_id', $request->_base_id)->first();
            if($title_data){
                DB::table('military_bases')->where('_base_id', $request->_base_id)->update([
                                                        'base_name'      =>  $request->base_name
                                                    ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Base is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    public function militaryUnitsList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $military_units = DB::table('military_units')->orderBy('unit_name', 'asc')->get();

        return View::make("admin/military_units_list")->with(['military_units' => $military_units]);
    }

    public function addMilitaryUnitModal(Request $request){
        $html = view('admin.xhr.add_military_unit_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    public function addMilitaryUnit(Request $request){
        $rules =[
                    'unit_name'         =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('military_units')->insert([
                                                        'unit_name'        =>  $request->unit_name
                                                    ]);
            if(!$done){
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else{
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Unit is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function editMilitaryUnitModal(Request $request){
        $rules =[
                    '_unit_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $unit_data = DB::table('military_units')->where('_unit_id', $request->_unit_id)->first();
            $html = view('admin.xhr.edit_military_unit_modal',['unit_data'=> $unit_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editMilitaryUnit(Request $request){
        $rules =[
                    'unit_name'     =>  'required',
                    '_unit_id'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('military_units')->where('_unit_id', $request->_unit_id)->first();
            if($title_data){
                DB::table('military_units')->where('_unit_id', $request->_unit_id)->update([
                                                        'unit_name'      =>  $request->unit_name
                                                    ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Unit is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    public function militaryMosList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $military_mos = DB::table('military_mos')->orderBy('mos_name', 'asc')->get();

        return View::make("admin/military_mos_list")->with(['military_mos' => $military_mos]);
    }

    public function addMilitaryMosModal(Request $request){
        $html = view('admin.xhr.add_military_mos_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    public function addMilitaryMos(Request $request){
        $rules =[
                    'mos_name'         =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('military_mos')->insert([
                                                        'mos_name'        =>  $request->mos_name
                                                    ]);
            if(!$done){
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else{
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Mos is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function editMilitaryMosModal(Request $request){
        $rules =[
                    '_mos_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $mos_data = DB::table('military_mos')->where('_mos_id', $request->_mos_id)->first();
            $html = view('admin.xhr.edit_military_mos_modal',['mos_data'=> $mos_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editMilitaryMos(Request $request){
        $rules =[
                    'mos_name'     =>  'required',
                    '_mos_id'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('military_mos')->where('_mos_id', $request->_mos_id)->first();
            if($title_data){
                DB::table('military_mos')->where('_mos_id', $request->_mos_id)->update([
                                                        'mos_name'      =>  $request->mos_name
                                                    ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Mos is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    public function militaryMosTitleList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $military_mos_titles = DB::table('military_mos_titles')->orderBy('mtitle_name', 'asc')->get();

        return View::make("admin/military_mos_titles_list")->with(['military_mos_titles' => $military_mos_titles]);
    }

    public function addMilitaryMosTitleModal(Request $request){
        $html = view('admin.xhr.add_military_mos_title_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    public function addMilitaryMosTitle(Request $request){
        $rules =[
                    'mtitle_name'         =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('military_mos_titles')->insert([
                                                        'mtitle_name'        =>  $request->mtitle_name
                                                    ]);
            if(!$done){
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else{
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Mos title is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function editMilitaryMosTitleModal(Request $request){
        $rules =[
                    '_mtitle_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('military_mos_titles')->where('_mtitle_id', $request->_mtitle_id)->first();
            $html = view('admin.xhr.edit_military_mos_title_modal',['title_data'=> $title_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editMilitaryMosTitle(Request $request){
        $rules =[
                    'mtitle_name'     =>  'required',
                    '_mtitle_id'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('military_mos_titles')->where('_mtitle_id', $request->_mtitle_id)->first();
            if($title_data){
                DB::table('military_mos_titles')->where('_mtitle_id', $request->_mtitle_id)->update([
                                                        'mtitle_name'      =>  $request->mtitle_name
                                                    ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Title is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    public function testimonialsList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $testimonials = DB::table('testimonials')->orderBy('_testimonial_id', 'desc')->get();

        return View::make("admin/testimonials_list")->with(['testimonials' => $testimonials]);
    }

    public function addTestimonialModal(Request $request){
        $html = view('admin.xhr.add_testimonial_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    public function addTestimonial(Request $request){
        $rules =[
                    'testimonial_username'         =>  'required',
                    'testimonial_useraddress'      =>  'required',
                    'testimonial_text'             =>  'required',
                    'testimonial_userimage'        =>  'required|mimes:jpg,jpeg,png|max:2048'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $testimonial_userimage = NULL;
            
            $fileName = 'testimonials_'.time().'_'.$request->testimonial_userimage->getClientOriginalName();
            $request->testimonial_userimage->move(public_path('img/upload'), $fileName);
            $testimonial_userimage = 'img/upload/'.$fileName;
            $done = DB::table('testimonials')->insert([
                            'testimonial_username'          =>  $request->testimonial_username,
                            'testimonial_useraddress'       =>  $request->testimonial_useraddress,
                            'testimonial_text'              =>  $request->testimonial_text,
                            'testimonial_userimage'         =>  $testimonial_userimage
                                                    ]);
            if(!$done){
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else{
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Testimonial is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function editTestimonialModal(Request $request){
        $rules =[
                    '_testimonial_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $testimonial_data = DB::table('testimonials')->where('_testimonial_id', $request->_testimonial_id)->first();
            $html = view('admin.xhr.edit_testimonial_modal',['testimonial_data'=> $testimonial_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    public function editTestimonial(Request $request){
        $req = 'nullable';
        if($_FILES['testimonial_userimage'] && !empty($_FILES['testimonial_userimage']['name'])){
            $req = 'required|mimes:jpg,jpeg,png|max:2048';
        }
        $rules =[
                    'testimonial_username'         =>  'required',
                    'testimonial_useraddress'      =>  'required',
                    'testimonial_text'             =>  'required',
                    '_testimonial_id'              =>  'required',
                    'testimonial_userimage'        =>  $req
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{

            $update_data = ['testimonial_username'          =>  $request->testimonial_username,
                            'testimonial_useraddress'       =>  $request->testimonial_useraddress,
                            'testimonial_text'              =>  $request->testimonial_text];

            if($_FILES['testimonial_userimage'] && !empty($_FILES['testimonial_userimage']['name'])){
                $fileName = 'testimonials_'.time().'_'.$request->testimonial_userimage->getClientOriginalName();
                $request->testimonial_userimage->move(public_path('img/upload'), $fileName);
                $testimonial_userimage = 'img/upload/'.$fileName;
                $update_data['testimonial_userimage'] = $testimonial_userimage;
            }
            
            
            $done = DB::table('testimonials')->where('_testimonial_id', $request->_testimonial_id)->update($update_data);
            if(!$done){
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else{
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Testimonial is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    public function activeInactiveTestimonial(Request $request){
        $rules =[
                    '_testimonial_id'          =>  'required',
                    'testimonial_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $testimonial_is_active = $request->testimonial_is_active==1 ? 0 : 1;
            DB::table('testimonials')->where('_testimonial_id',$request->_testimonial_id)->update(['testimonial_is_active' => $testimonial_is_active]);
            $active_inactive = $testimonial_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Testimonial '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function primaryNonPrimaryTestimonial(Request $request){
        $rules =[
                    '_testimonial_id'          =>  'required',
                    'testimonial_is_primary'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $testimonial_is_primary = $request->testimonial_is_primary==1 ? 1 : 0;
            if($testimonial_is_primary==1){
                DB::table('testimonials')->update(['testimonial_is_primary' => 0]);
                DB::table('testimonials')->where('_testimonial_id',$request->_testimonial_id)->update(['testimonial_is_primary' => 1]);
            }else{
                DB::table('testimonials')->where('_testimonial_id',$request->_testimonial_id)->update(['testimonial_is_primary' => 0]);
            }
            
            $active_inactive = $testimonial_is_primary==1?'is made primary':'is made non-primary';


            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Testimonial '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function gamesList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $games = DB::table('games')->orderBy('_game_id', 'desc')->get();

        return View::make("admin/games_list")->with(['games' => $games]);
    }

    function addGameModal(Request $request){
        $html = view('admin.xhr.add_game_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function addGame(Request $request){
        $rules =[
                    'game_name'         =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            
            $done = DB::table('games')->insert([
                                'game_name'          =>  $request->game_name
                            ]);
            if(!$done){
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else{
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Game is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function editGameModal(Request $request){
        $rules =[
                    '_game_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $game_data = DB::table('games')->where('_game_id', $request->_game_id)->first();
            $html = view('admin.xhr.edit_game_modal',['game_data'=> $game_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editGame(Request $request){
        $rules =[
                    'game_name'     =>  'required',
                    '_game_id'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('games')->where('_game_id', $request->_game_id)->first();
            if($title_data){
                DB::table('games')->where('_game_id', $request->_game_id)->update([
                                                        'game_name'      =>  $request->game_name
                                                    ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Game is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    function activeInactiveGame(Request $request){
        $rules =[
                    '_game_id'          =>  'required',
                    'game_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $game_is_active = $request->game_is_active==1 ? 0 : 1;
            DB::table('games')->where('_game_id',$request->_game_id)->update(['game_is_active' => $game_is_active]);
            $active_inactive = $game_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Game '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function gameTeamsList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $games = DB::table('games')->orderBy('_game_id', 'desc')->get();
        if($request->_game_id){
            $game_teams = DB::table('game_teams')->join('games','game_teams.team_gameid','=','games._game_id')->where('team_gameid',$request->_game_id)->orderBy('_team_id', 'desc')->get();
        }else{
            $game_teams = DB::table('game_teams')->join('games','game_teams.team_gameid','=','games._game_id')->orderBy('_team_id', 'desc')->get();
        }

        return View::make("admin/game_teams_list")->with(['games' => $games, 'game_teams' => $game_teams]);
    }

    function addGameTeamModal(Request $request){
        $games = DB::table('games')->where('game_is_active',1)->orderBy('game_name', 'asc')->get();
        $html = view('admin.xhr.add_game_team_modal',['games' => $games])->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function addGameTeam(Request $request){
        $rules =[
                    'team_name'            =>  'required',
                    'team_gameid'    =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('game_teams')->insert([
                                                        'team_name'            => $request->team_name,
                                                        'team_gameid'    => $request->team_gameid
                                                    ]);

            if(!$done)
            {
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else
            {
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Team/Group is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function editGameTeamModal(Request $request){
        $rules =[
                    '_team_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $games = DB::table('games')->where('game_is_active',1)->orderBy('game_name', 'asc')->get();
            $team_data = DB::table('game_teams')->where('_team_id', $request->_team_id)->first();
            $html = view('admin.xhr.edit_game_team_modal',['team_data'=> $team_data,'games'=>$games])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editGameTeam(Request $request){
        $rules =[
                    'team_name'        =>  'required',
                    'team_gameid'=>  'required',
                    '_team_id'         =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('game_teams')->where('_team_id', $request->_team_id)->first();
            if($title_data){
                DB::table('game_teams')->where('_team_id', $request->_team_id)->update([
                                                        'team_name'      =>  $request->team_name,
                                                        'team_gameid'      =>  $request->team_gameid
                                                    ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Team/Group is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    function activeInactiveGameTeam(Request $request){
        $rules =[
                    '_team_id'          =>  'required',
                    'team_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $team_is_active = $request->team_is_active==1 ? 0 : 1;
            DB::table('game_teams')->where('_team_id',$request->_team_id)->update(['team_is_active' => $team_is_active]);
            $active_inactive = $team_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Team/Group '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }
}
