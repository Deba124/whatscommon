<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use View;
use Validator;

class Notifications extends Controller
{
	
    function index(Request $request){
		if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $notifications = DB::table('notifications')
		        			->join('device_tokens','notifications.notification_device','=','device_tokens._device_id')
		        			->join('users','notifications.notification_user','=','users.id')
		        			->select('notifications.*', 'device_tokens.device_token', 'device_tokens.device_type', 'users.firstName', 'users.lastName', 'users.profile_photo_path')
		        			->orderBy('notifications._notification_id','desc')
		        			->limit(1000)
		        			->get();
        return View::make("admin/notifications")->with(['notifications' => $notifications]);
    }

    function addNotificationModal(Request $request){
    	$users = DB::table('users')
        			->join('device_tokens','users.id','=','device_tokens.device_user_id')
        			->get();
    	$html = view('admin.xhr.add_notification_modal',['users' => $users])->render();

    	return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function addNotification(Request $request){
        
    	$rules =[
                    'device_id'         	=>  'required',
                    'notification_title'    =>  'required',
                    'notification_content'	=>  'required',
                    'notification_type'     =>  'required|in:connection,match,test'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            /*print_r($_POST);exit();*/
            $counter = 0;
            if($request->device_id){
                foreach($request->device_id as $i => $device){
                    $device_data = DB::table('device_tokens')->where('_device_id',$device)->first();
                    $noti_response = sendNotification($device_data->device_type,$device_data->device_token,$request->notification_title,$request->notification_content,['type' => $request->notification_type]);
                    $notification_data =[
                                            'notification_user'     =>  $device_data->device_user_id,
                                            'notification_device'   =>  $device_data->_device_id,
                                            'notification_title'    =>  $request->notification_title,
                                            'notification_content'  =>  $request->notification_content,
                                            'notification_status'   =>  $noti_response['success']==1 ? 'success' : 'failed',
                                            'notification_response' =>  json_encode($noti_response)
                                        ];
                    DB::table('notifications')->insert($notification_data);
                    $counter++;
                }
            }
        	
        	/*print_r($noti_response);*/
        	if($counter>0){
        		return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Notification Sent Successfully',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
        	}else{
        		return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        	}

        }
    }

    
}
