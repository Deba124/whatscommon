<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Page;
use View;

class Pages extends Controller
{
    function PagesList(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
    	$pages = Page::all();

    	return View::make("admin/pages_list")->with(['pages' => $pages]);
    }

    public function EditPage($_page_id){
        /*if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }*/
    	$page = Page::find($_page_id);
    	return View::make("admin/edit_page")->with(['page' => $page]);
    }

    public function UpdatePage(Request $request){
    	$validated = $request->validate([
	        '_page_id' 		=> 	'required|exists:pages,id',
	        'page_title' 	=> 	'required',
	        'page_content'	=>	'required'
	    ]);

	    $page = Page::find($request->_page_id);
	    if($page){
	    	Page::where('id',$request->_page_id)->update([
															'page_title'	=>	$request->page_title,
															'page_content'	=>	$request->page_content
														]);
	    	return response([
								'response_code'	=>	'200',
                 				'response_msg'	=> 	'Page Content Updated Successfully',
                 				'reponse_body' 	=> 	'null',
                 				'redirect'		=>	url('admin/pages')
                 			],200);
	    }
	    return response([
                        	'response_code'	=>	'203',
                     		'response_msg'	=> 	'Invalid Data Supplied',
                     		'reponse_body' 	=> 	"null"
                     	], 401);
    }
}
