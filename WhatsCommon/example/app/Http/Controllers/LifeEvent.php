<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Userdetail;
use App\Models\BankDetail;
use App\Models\NotificationSetting;
use Validator;
use Illuminate\Validation\Rule;
use View;

class LifeEvent extends Controller
{
    function getSubcategories(Request $request){
    	$rules =[
			        'type_id' 	=> 	'required',
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$what = DB::table('life_event_sub_categories')->where('sub_cat_event_type_id', $request->type_id)->get();
			$response_data["what"] = $what;
			if($request->type_id==3){
				$response_data['adoption_status'] = DB::table('adoption_status')->orderBy('status_name', 'asc')->get();
				$response_data['place_of_birth'] = DB::table('place_of_birth')->orderBy('place_name', 'asc')->get();
				$response_data['genders'] = DB::table('genders')->where('gender_is_active',1)->orderBy('gender_name', 'asc')->get();
			}else if($request->type_id==4){
				$response_data['location_types'] = DB::table('location_types')->orderBy('lt_name', 'asc')->get();
				$response_data['travel_modes'] = DB::table('travel_modes')->where('mode_is_active',1)->orderBy('mode_name', 'asc')->get();
			}else if($request->type_id==5){
				$response_data['genders'] = DB::table('genders')->where('gender_is_active',1)->orderBy('gender_name', 'asc')->get();
				$response_data['military_bases'] = DB::table('military_bases')->orderBy('base_name', 'asc')->get();
				$military_branches = DB::table('military_branches')->orderBy('mbranch_name', 'asc')->get();
				$response_data['military_units'] = DB::table('military_units')->orderBy('unit_name', 'asc')->get();
				$response_data['military_mos'] = DB::table('military_mos')->orderBy('mos_name', 'asc')->get();
				$response_data['military_mos_titles'] = DB::table('military_mos_titles')->orderBy('mtitle_name', 'asc')->get();
				$types = [];
				foreach ($military_branches as $key => $value) {
					$military_ranks = DB::table('military_ranks')->where('rank_branch_id',$value->_mbranch_id)->orderBy('rank_name', 'asc')->get();
					$value->military_ranks = $military_ranks;
					array_push($types, $value);
				}
				$response_data['military_branches'] = $types;
				$response_data['age_ranges'] = DB::table('military_age_range')->orderBy('m_range_name', 'asc')->get();
			}else if($request->type_id==6){
				$response_data['subjects'] 		= DB::table('subjects')->orderBy('subject_name', 'asc')->get();
				$response_data['curriculars'] 	= DB::table('curriculars')->orderBy('curricular_name', 'asc')->get();
				$response_data['organizations'] = DB::table('organizations')->orderBy('organization_name', 'asc')->get();
				$response_data['events'] 		= DB::table('events')->orderBy('event_name', 'asc')->get();
				$response_data['after_schools'] = DB::table('after_schools')->orderBy('as_name', 'asc')->get();
			}
			else if(in_array($request->type_id,[7,8,9])){
				if($request->type_id==7){
					$response_data['industries'] = DB::table('industries')->where('industry_is_active',1)->orderBy('industry_name', 'asc')->get();
					$response_data['occupations'] = DB::table('occupations')->where('occupation_is_active',1)->orderBy('occupation_name', 'asc')->get();
					$response_data['career_levels'] = DB::table('career_levels')->orderBy('cl_name', 'asc')->get();
				}else if($request->type_id==8){
					$species = DB::table('species')->orderBy('species_name', 'asc')->get();
					$response_data['pet_colors'] = DB::table('pet_colors')->orderBy('pc_name', 'asc')->get();
					$response_data['pet_sizes'] = DB::table('pet_sizes')->orderBy('ps_name', 'asc')->get();
					$response_data['pet_tempers'] = DB::table('pet_tempers')->orderBy('pt_name', 'asc')->get();
					$response_data['genders'] = ["Male","Female","Unknown"];
					
					$types = [];
					foreach ($species as $key => $value) {
						$breeds = DB::table('breeds')->where('breed_species_id',$value->_species_id)->orderBy('breed_name', 'asc')->get();
						$value->breeds = $breeds;
						array_push($types, $value);
					}
					$response_data['species'] = $types;
				}else{
					$items = DB::table('items')->orderBy('item_name', 'asc')->get();
					$response_data['indoors'] = DB::table('indoors')->orderBy('indoor_name', 'asc')->get();
					$response_data['inmotions'] = DB::table('inmotions')->orderBy('inmotion_name', 'asc')->get();
					$response_data['materials'] = DB::table('materials')->orderBy('material_name', 'asc')->get();
					$response_data['outdoors'] = DB::table('outdoors')->orderBy('outdoor_name', 'asc')->get();
					$response_data['others'] = [];
					
					$types = [];
					foreach ($items as $key => $value) {
						$item_sizes = DB::table('item_sizes')->where('is_item_id',$value->_item_id)->orderBy('is_name', 'asc')->get();
						$value->item_sizes = $item_sizes;
						array_push($types, $value);
					}
					$response_data['items'] = $types;
				}
			}else if($request->type_id==11){
				$games = DB::table('games')->orderBy('game_name', 'asc')->get();
					
				$types = [];
				foreach ($games as $key => $value) {
					$game_teams = DB::table('game_teams')->where('team_gameid',$value->_game_id)->orderBy('team_name', 'asc')->get();
					$value->game_teams = $game_teams;
					array_push($types, $value);
				}
				$response_data['games'] = $types;
			}/*else if($request->type_id==12){
				$response_data['indoors'] = DB::table('indoors')->orderBy('indoor_name', 'asc')->get();
			}*/
			return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'What Fetched Successfully',
                  				'reponse_body' 	=> 	$response_data
                  			], 200);
		}
    }

    function getDetailsForSubCategory(Request $request){
    	$rules =[
			        'sub_category_id' 	=> 	'required',
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$response = [];
			if($request->sub_category_id==1){

				$home_types = DB::table('home_types')->where('home_type_is_active',1)/*->where('home_type_subcategory', $request->sub_category_id)*/->orderBy('home_type_name', 'asc')->get();
				$home_styles = DB::table('home_styles')->where('home_style_is_active',1)/*->where('home_style_subcategory', $request->sub_category_id)*/->orderBy('home_style_name', 'asc')->get();
				$response['home_types'] = $home_types;
				$response['home_styles'] = $home_styles;
				
			}else if($request->sub_category_id==2){
				$vehicle_types = DB::table('vehicle_types')->where('vehicle_type_is_active',1)/*->where('vehicle_type_subcategory', $request->sub_category_id)*/->orderBy('vehicle_type_name', 'asc')->get();
				$vehicle_color = DB::table('vehicle_color')->where('color_is_active',1)->orderBy('color_name', 'asc')->get();
				/*$response['vehicle_types'] = $vehicle_types;*/
				$types = [];
				foreach ($vehicle_types as $key => $value) {
					$makers = DB::table('vehicle_makers')->where('maker_is_active',1)->where('maker_vehicle_type',$value->id)->orderBy('maker_name', 'asc')->get();
					$value->makers = $makers;
					array_push($types, $value);
				}
				$response['vehicle_types'] = $types;
				$response['vehicle_colors'] = $vehicle_color;
			}else if($request->sub_category_id==3){
				$response['genders'] = DB::table('genders')->where('gender_is_active',1)->orderBy('gender_name', 'asc')->get();
				$response['relations'] = DB::table('relations')->where('relation_is_active',1)->orderBy('relation_name', 'asc')->get();
				$response['marital_status'] = DB::table('marital_status')->where('ms_is_active',1)->orderBy('ms_name', 'asc')->get();
				$response['occupations'] = DB::table('occupations')->where('occupation_is_active',1)->orderBy('occupation_name', 'asc')->get();
				$response['industries'] = DB::table('industries')->where('industry_is_active',1)->orderBy('industry_name', 'asc')->get();

			}else if(in_array($request->sub_category_id,[4,5])){

				$response['genders'] = DB::table('genders')->where('gender_is_active',1)->orderBy('gender_name', 'asc')->get();
				$response['ethnicities'] = DB::table('ethnicities')->orderBy('ethnicity_name', 'asc')->get();
				$response['races'] = DB::table('races')->orderBy('race_name', 'asc')->get();
				$response['nationalities'] = DB::table('countries')->where('country_is_active',1)->orderBy('country_name', 'asc')->get();
				$response['orientations'] = DB::table('orientations')->orderBy('_orientation_id', 'asc')->get();
				$response['body_styles'] = DB::table('body_styles')->orderBy('bs_name', 'asc')->get();
				$response['skin_tonnes'] = DB::table('skin_tonnes')->orderBy('st_name', 'asc')->get();
				$response['hair_colors'] = DB::table('hair_colors')->orderBy('hc_name', 'asc')->get();
				$response['eye_colors'] = DB::table('eye_colors')->orderBy('ec_name', 'asc')->get();
				$response['politicals'] = DB::table('politicals')->orderBy('political_name', 'asc')->get();
				$response['religions'] = DB::table('religions')->orderBy('religion_name', 'asc')->get();
				$response['age_ranges'] = DB::table('age_ranges')->orderBy('range_name', 'asc')->get();
				$response['interests'] = DB::table('interests')->orderBy('interest_name', 'asc')->get();
				$response['radius'] = DB::table('radius')/*->orderBy('interest_name', 'asc')*/->get();

				$response['availabilities'] = DB::table('availabilities')->orderBy('availability_name', 'asc')->get();
				
				if($request->sub_category_id==5){
					$response['occupations'] = DB::table('occupations')->where('occupation_is_active',1)->orderBy('occupation_name', 'asc')->get();
					
					$response['facial_hairs'] = DB::table('facial_hairs')->orderBy('fh_name', 'asc')->get();
					$response['body_hairs'] = DB::table('body_hairs')->orderBy('bh_name', 'asc')->get();
					$response['tattoos'] = DB::table('tattoos')->orderBy('tattoo_name', 'asc')->get();
					$response['foods'] = DB::table('foods')->orderBy('food_name', 'asc')->get();
					$response['food_types'] = DB::table('food_types')->orderBy('ft_name', 'asc')->get();
					$response['sports'] = DB::table('sports')->orderBy('sport_name', 'asc')->get();
					$response['zodiac_signs'] = DB::table('zodiac_signs')->orderBy('zodiac_name', 'asc')->get();
					$response['financials'] = DB::table('financial_status')->orderBy('fs_name', 'asc')->get();/*["Average","Stable","Rich"];*/
					$response['relationships'] = ["Boyfriend/Girlfriend","Husband/Wife"];
					$response['affiliations'] = DB::table('affiliations')->orderBy('affiliation_name', 'asc')->get();
					$response['weight_general'] = DB::table('weight_general')->orderBy('wg_name', 'asc')->get();//["Underweight","Normal","Overweight"];
					$response['height_general'] = DB::table('height_general')->orderBy('hg_name', 'asc')->get();//["Little","Short","Medium","Rather tall","Tall","Very tall"];

				}
			}
			else if(in_array($request->sub_category_id,[30,31])){
				$response['activity_types'] = DB::table('activity_types')->where('act_type_event_id',$request->sub_category_id)->where('act_type_is_active',1)->get();
				$games = DB::table('games')->orderBy('game_name', 'asc')->get();
					
				$types = [];
				foreach ($games as $key => $value) {
					$game_teams = DB::table('game_teams')->where('team_gameid',$value->_game_id)->orderBy('team_name', 'asc')->get();
					$value->game_teams = $game_teams;
					array_push($types, $value);
				}
				$response['games'] = $types;
				$response['location_types'] = DB::table('location_types')->orderBy('lt_name', 'asc')->get();
			}/*else if(in_array($request->sub_category_id,[6,7,8])){
				$response['adoption_status'] = DB::table('adoption_status')->orderBy('status_name', 'asc')->get();
				$response['place_of_birth'] = DB::table('place_of_birth')->orderBy('place_name', 'asc')->get();
			}
			else if(in_array($request->sub_category_id,[9,10,11,12])){
				$response['location_types'] = DB::table('location_types')->orderBy('lt_name', 'asc')->get();
			}else if(in_array($request->sub_category_id,[16,17,18,19,20,21,22,23,24,25])){
				$response['subjects'] = DB::table('subjects')->orderBy('subject_name', 'asc')->get();
				$response['curriculars'] = DB::table('curriculars')->orderBy('curricular_name', 'asc')->get();
				$response['organizations'] = DB::table('organizations')->orderBy('organization_name', 'asc')->get();
				$response['events'] = DB::table('events')->orderBy('event_name', 'asc')->get();
				$response['after_schools'] = DB::table('after_schools')->orderBy('as_name', 'asc')->get();
			}*/


			return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'Details Fetched Successfully',
                  				'reponse_body' 	=> 	$response
                  			], 200);
		}
    }

    function lifeEventUpload(Request $request){
    	//print_r($request->event_pictures);exit();
    	$rules =[
			        'type_id' 			=> 	'required',
			        /*'sub_category_id'	=>	'required',*/
			        'user_id'			=>	'required'
			    ];
		ini_set('post_max_size', '64M');
		ini_set('upload_max_filesize', '64M');
		if(!in_array($request->type_id,[5,7,8,9,11])){
			$rules['sub_category_id'] = 'required';
		}
		if($request->type_id==1){
			if($request->sub_category_id==1){
				$rules['type_of_home'] = 'nullable';
				$rules['home_style'] = 'nullable';
				$rules['country'] = 'nullable';
				$rules['state'] = 'nullable';
				$rules['city'] = 'nullable';
				$rules['street'] = 'nullable';
				$rules['zip'] = 'nullable';
				$rules['when_from_month'] = 'nullable';
				$rules['when_from_day'] = 'nullable';
				$rules['when_from_year'] = 'nullable';
				$rules['when_to_month'] = 'nullable';
				$rules['when_to_day'] = 'nullable';
				$rules['when_to_year'] = 'nullable';
				$rules['email'] = 'nullable';
				$rules['phone'] = 'nullable';
				/*$rules['from_date'] = 'nullable';
				$rules['to_date'] = 'nullable|date';*/
				/*$rules['time'] = 'required';
				$rules['time_ampm'] = 'required';*/
			}else if($request->sub_category_id==2){
				$rules['tag_number'] = 'required';
				$rules['type_of_vehicle'] = 'nullable';
				$rules['vehicle_maker'] = 'nullable';
				$rules['vehicle_model'] = 'nullable';
				$rules['year_manufactured'] = 'nullable';
				$rules['vehicle_color'] = 'nullable';
				$rules['country'] = 'nullable';
				$rules['state'] = 'nullable';
				$rules['city'] = 'nullable';
				$rules['street'] = 'nullable';
				$rules['zip'] = 'nullable';
				$rules['when_from_month'] = 'nullable';
				$rules['when_from_day'] = 'nullable';
				$rules['when_from_year'] = 'nullable';
				$rules['when_to_month'] = 'nullable';
				$rules['when_to_day'] = 'nullable';
				$rules['when_to_year'] = 'nullable';
				/*$rules['time'] = 'required';
				$rules['time_ampm'] = 'required';*/
				$rules['email'] = 'nullable';
				$rules['phone'] = 'nullable';
			}else if($request->sub_category_id==3){
				$rules['first_name'] = 'required';
				$rules['last_name'] = 'required';
				$rules['age'] = 'nullable';
				$rules['gender'] = 'nullable';
				$rules['relationship'] = 'nullable';
				$rules['marital_status'] = 'nullable';
				$rules['industry'] = 'nullable';
				$rules['category'] = 'nullable';
				$rules['country'] = 'nullable';
				$rules['state'] = 'nullable';
				$rules['city'] = 'nullable';
				$rules['street'] = 'nullable';
				$rules['zip'] = 'nullable';
				$rules['email'] = 'nullable';
				$rules['phone'] = 'nullable';

				$rules['when_family_birth_month'] = 'nullable';
				$rules['when_family_birth_day'] = 'nullable';
				$rules['when_family_birth_year'] = 'nullable';

				$rules['when_family_wedding_month'] = 'nullable';
				$rules['when_family_wedding_day'] = 'nullable';
				$rules['when_family_wedding_year'] = 'nullable';

				$rules['when_family_passed_month'] = 'nullable';
				$rules['when_family_passed_day'] = 'nullable';
				$rules['when_family_passed_year'] = 'nullable';

				/*$rules['birthday'] = 'nullable|date';
				$rules['anniversary'] = 'nullable|date';
				$rules['passed'] = 'nullable|date';*/
			}
		}else if($request->type_id==2 && in_array($request->sub_category_id,[4,5])){
			$rules['gender'] = 'required';
			$rules['age'] = 'nullable';
			$rules['ethnicity'] = 'nullable';
			$rules['race'] = 'nullable';
			$rules['nationality'] = 'nullable';
			$rules['orientation'] = 'nullable';
			$rules['body_style'] = 'nullable';
			$rules['skin_tonne'] = 'nullable';
			$rules['hair_color'] = 'nullable';
			$rules['eye_color'] = 'nullable';
			$rules['political'] = 'nullable';
			$rules['religion'] = 'nullable';
			$rules['looking_for'] = 'nullable';
			$rules['age_range'] = 'nullable';
			$rules['interests'] = 'nullable';

			if($request->sub_category_id==5){
				$rules['height'] = 'nullable';
				$rules['height_measure'] = 'nullable';
				$rules['height_general'] = 'nullable';
				$rules['weight'] = 'nullable';
				$rules['weight_measure'] = 'nullable';
				$rules['weight_general'] = 'nullable';
				$rules['facial_hair'] = 'nullable';
				$rules['body_hair'] = 'nullable';
				$rules['glasses'] = 'nullable';
				$rules['party'] = 'nullable';
				$rules['smoke'] = 'nullable';
				$rules['drink'] = 'nullable';
				$rules['tattoo'] = 'nullable';
				$rules['tattoo_type'] = 'nullable';
				$rules['fav_food'] = 'nullable';
				$rules['food_type'] = 'nullable';
				$rules['sport'] = 'nullable';
				$rules['fav_team'] = 'nullable';
				$rules['zodiac_sign'] = 'nullable';
				$rules['birth_stone'] = 'nullable';
				$rules['affiliation'] = 'nullable';
				$rules['career'] = 'nullable';
				$rules['financial'] = 'nullable';
				$rules['relationship'] = 'nullable';
			}

			$rules['country'] = 'nullable';
			$rules['state'] = 'nullable';
			$rules['city'] = 'nullable';
			$rules['radius'] = 'nullable';

			$rules['timeframe'] = 'nullable';
			$rules['time_month'] = 'nullable';
				
		}else if($request->type_id==3 /*&& in_array($request->sub_category_id,[6,7,8])*/){
			
			$rules['birth_country'] = 'nullable';
			$rules['birth_state'] = 'nullable';
			$rules['birth_city'] = 'nullable';
			$rules['birth_street'] = 'nullable';
			$rules['birth_zip'] = 'nullable';

			$rules['country'] = 'nullable';
			$rules['state'] = 'nullable';
			$rules['city'] = 'nullable';
			$rules['street'] = 'nullable';
			$rules['zip'] = 'nullable';

			$rules['birth_name'] = 'nullable';
			$rules['bio_mother'] = 'nullable';
			$rules['bio_father'] = 'nullable';
			$rules['weight'] = 'nullable';
			$rules['weight_measure'] = 'nullable';
			$rules['height'] = 'nullable';
			$rules['height_measure'] = 'nullable';
			$rules['birth_place'] = 'nullable';
			$rules['adoption_status'] = 'nullable';
			$rules['birth_date'] = 'nullable';
			/*$rules['time'] = 'required';
			$rules['time_ampm'] = 'required';*/
		}else if($request->type_id==4 /*&& in_array($request->sub_category_id,[9,10,11,12])*/){

			$rules['airport'] = 'nullable';
			$rules['airline'] = 'nullable';
			$rules['flight_number'] = 'nullable';
			$rules['row'] = 'nullable';
			$rules['seat'] = 'nullable';
			$rules['location_type'] = 'nullable';
			$rules['location_name'] = 'required';

			$rules['country'] = 'nullable';
			$rules['state'] = 'nullable';
			$rules['city'] = 'nullable';
			$rules['street'] = 'nullable';
			$rules['zip'] = 'nullable';

			$rules['when_from_month'] = 'nullable';
			$rules['when_from_day'] = 'nullable';
			$rules['when_from_year'] = 'nullable';
			$rules['when_to_month'] = 'nullable';
			$rules['when_to_day'] = 'nullable';
			$rules['when_to_year'] = 'nullable';
			$rules['dep_time'] = 'nullable';
			$rules['dep_time_ampm'] = 'nullable';
			$rules['arr_time'] = 'nullable';
			$rules['arr_time_ampm'] = 'nullable';
		}else if($request->type_id==5){

			$rules['military_branch'] = 'required';
			$rules['gender'] = 'nullable';
			$rules['age'] = 'nullable';
			$rules['military_unit1'] = 'nullable';
			$rules['military_unit2'] = 'nullable';
			$rules['military_unit3'] = 'nullable';
			$rules['military_unit4'] = 'nullable';
			$rules['military_unit5'] = 'nullable';
			$rules['military_unit6'] = 'nullable';
			$rules['military_rank'] = 'nullable';
			$rules['military_mos'] = 'nullable';
			$rules['military_title'] = 'nullable';

			$rules['country'] = 'required';
			$rules['state'] = 'nullable';
			$rules['city'] = 'nullable';
			$rules['street'] = 'nullable';
			$rules['zip'] = 'nullable';
			$rules['military_base'] = 'nullable';
			$rules['military_order'] = 'nullable';

			$rules['when_from_month'] = 'nullable';
			$rules['when_from_day'] = 'nullable';
			$rules['when_from_year'] = 'nullable';
			$rules['when_to_month'] = 'nullable';
			$rules['when_to_day'] = 'nullable';
			$rules['when_to_year'] = 'nullable';
		}else if($request->type_id==6 /*&& in_array($request->sub_category_id,[16,17,18,19,20,21,22,23,24,25])*/){

			$rules['school_name'] = 'required';
			$rules['school_sorority'] = 'nullable';
			$rules['school_teacher'] = 'nullable';
			$rules['school_roomno'] = 'nullable';
			$rules['school_subject'] = 'nullable';
			$rules['school_curriculars'] = 'nullable';
			$rules['school_organization'] = 'nullable';
			$rules['school_event'] = 'nullable';
			$rules['school_afterschool'] = 'nullable';

			$rules['country'] = 'nullable';
			$rules['state'] = 'nullable';
			$rules['city'] = 'nullable';
			$rules['street'] = 'nullable';
			$rules['zip'] = 'nullable';

			$rules['when_from_month'] = 'nullable';
			$rules['when_from_day'] = 'nullable';
			$rules['when_from_year'] = 'nullable';
			$rules['when_to_month'] = 'nullable';
			$rules['when_to_day'] = 'nullable';
			$rules['when_to_year'] = 'nullable';
			/*$rules['time'] = 'required';
			$rules['time_ampm'] = 'required';*/
		}else if($request->type_id==7){
			$rules['career_name'] = 'required';
			$rules['career_category'] = 'nullable';
			$rules['career_title'] = 'nullable';
			$rules['career_level'] = 'nullable';

			$rules['country'] = 'nullable';
			$rules['state'] = 'nullable';
			$rules['city'] = 'nullable';
			$rules['street'] = 'nullable';
			$rules['zip'] = 'nullable';

			$rules['when_from_month'] = 'nullable';
			$rules['when_from_day'] = 'nullable';
			$rules['when_from_year'] = 'nullable';
			$rules['when_to_month'] = 'nullable';
			$rules['when_to_day'] = 'nullable';
			$rules['when_to_year'] = 'nullable';
			/*$rules['time'] = 'required';
			$rules['time_ampm'] = 'required';*/
		}else if($request->type_id==8){
			$required = (isset($request->search_for) && $request->search_for==1) ? 'required' : 'nullable';
			$rules['pet_name'] = 'nullable';
			$rules['pet_breed'] = $required;
			$rules['pet_species'] = $required;
			$rules['gender'] = $required;
			$rules['pet_temperment'] = 'nullable';
			$rules['pet_color'] = $required;
			$rules['pet_size'] = 'nullable';
			$rules['pet_license_number'] = 'nullable';
			$rules['pet_collar'] = 'nullable';

			$rules['pet_veterinary'] = 'nullable';
			$rules['pet_medical'] = 'nullable';

			$rules['country'] = 'nullable';
			$rules['state'] = 'nullable';
			$rules['city'] = 'nullable';
			$rules['street'] = 'nullable';
			$rules['zip'] = 'nullable';

			/*$rules['from_date'] = 'required|date';
			$rules['to_date'] = 'required|date';
			$rules['time'] = 'required';
			$rules['time_ampm'] = 'required';*/
		}else if($request->type_id==9){
			$rules['item_name'] = 'required';
			$rules['item_size'] = 'nullable';
			$rules['item_material'] = 'nullable';
			$rules['outdoor'] = 'nullable';
			$rules['indoor'] = 'nullable';
			$rules['inmotion'] = 'nullable';
			$rules['other'] = 'nullable';

			$rules['country'] = 'nullable';
			$rules['state'] = 'nullable';
			$rules['city'] = 'nullable';
			$rules['street'] = 'nullable';
			$rules['zip'] = 'nullable';

			$rules['when_from_month'] = 'nullable';
			$rules['when_from_day'] = 'nullable';
			$rules['when_from_year'] = 'nullable';
			$rules['when_to_month'] = 'nullable';
			$rules['when_to_day'] = 'nullable';
			$rules['when_to_year'] = 'nullable';
			/*$rules['time'] = 'required';
			$rules['time_ampm'] = 'required';*/
		}else if($request->type_id==11){
			$rules['game_name'] = 'required';
			$rules['team_name'] = 'required';
			$rules['username'] = 'required';
			
			$rules['when_from_month'] = 'nullable';
			$rules['when_from_day'] = 'nullable';
			$rules['when_from_year'] = 'nullable';
		}else if($request->type_id==12){
			$rules['game_name'] = 'required';
			$rules['team_name'] = 'required';
			$rules['activity_type'] = 'required';
			$rules['location_type'] = 'required';
			$rules['location_name'] = 'required';
			$rules['entertainer'] = 'nullable';
			$rules['key_person'] = 'nullable';
			
			$rules['row'] = 'nullable';
			$rules['seat'] = 'nullable';

			$rules['country'] = 'nullable';
			$rules['state'] = 'nullable';
			$rules['city'] = 'nullable';
			$rules['street'] = 'nullable';
			$rules['zip'] = 'nullable';

			$rules['when_from_month'] = 'nullable';
			$rules['when_from_day'] = 'nullable';
			$rules['when_from_year'] = 'nullable';
			$rules['when_to_month'] = 'nullable';
			$rules['when_to_day'] = 'nullable';
			$rules['when_to_year'] = 'nullable';
			$rules['time'] = 'nullable';
			$rules['time_ampm'] = 'nullable';
		}


		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			if($request->login_device_id && $request->login_device_type && $request->login_token){
				/*$check = DB::table('login_tokens')
		                  ->where('login_user_id',$request->user_id)
		                  ->where('login_device_type', $request->login_device_type)
		                  ->where('login_token','=',$request->login_token)
		                  ->first();*/
		        $select = DB::select("SELECT * FROM login_tokens WHERE login_user_id='".$request->user_id."' AND login_device_type='".$request->login_device_type."' AND login_token='".$request->login_token."' LIMIT 1");
		        /*$select = "SELECT * FROM login_tokens WHERE login_user_id='".$request->user_id."' AND login_device_type='".$request->login_device_type."' AND login_token='".$request->login_token."' LIMIT 1";*/
		        $check = $select ? $select[0]  : NULL;
		        //echo $select;exit();
		        if(!$check){
		        	return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'invalid request',
                         		'reponse_body' 	=> 	NULL
                        	], 401);
		        }else{
		        	/*if($check->login_token!=$request->login_token){
		        		return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Unauthenticated request',
                         		'reponse_body' 	=> 	NULL
                        	], 401);
		        	}*/
		        	$current_Date    = strtotime(date("Y-m-d H:i:s"));

			    	$occurrence_Date = strtotime($check->login_created);

			    	$datetime_diff = round(abs($current_Date - $occurrence_Date));

			    	if($datetime_diff >=120){
			    		return response([
                				'response_code'	=>	'202',
                         		'response_msg'	=> 	'token expired',
                         		'reponse_body' 	=> 	NULL
                        	], 401);
			    	}
		        }
			}
			$life_event_data=[
								'event_type_id'			=>	$request->type_id,
								'event_subcategory_id'	=>	$request->sub_category_id,
								'event_user_id'			=>	$request->user_id,
								'event_is_draft'		=>	isset($request->is_draft) && $request->is_draft==1 ? 1 : 0
							];
			$life_event_where = [];
			$life_event_when = [];

			if($request->type_id==1){
				if($request->sub_category_id==1){
					$life_event_data['event_home_type'] 	= 	$request->type_of_home;
					$life_event_data['event_home_style'] 	= 	$request->home_style;
					$life_event_when = 	[
											/*'when_from' 	=>	$request->from_date,
											'when_to'		=>	$request->to_date,*/
											'when_from_month' 	=>	$request->when_from_month,
											'when_from_day'		=>	$request->when_from_day,
											'when_from_year' 	=>	$request->when_from_year,
											'when_to_month'		=>	$request->when_to_month,
											'when_to_day' 		=>	$request->when_to_day,
											'when_to_year'		=>	$request->when_to_year,
											/*'when_time'		=>	$request->time,
											'when_time_ampm'=>	$request->time_ampm*/
										];
					$life_event_where=	[
											'where_country'	=>	$request->country,
											'where_state'	=>	$request->state,
											'where_city'	=>	$request->city,
											'where_street'	=>	$request->street,
											'where_zip'		=>	$request->zip,
											'where_email'	=>	$request->email,
											'where_phone'	=>	$request->phone,
											'where_isd'		=>	$request->isd_code
										];
				}else if($request->sub_category_id==2){
					$life_event_data['event_tag_number']	=	$request->tag_number;
					$life_event_data['event_vehicle_type']	=	$request->type_of_vehicle;
					$life_event_data['event_vehicle_maker']	=	$request->vehicle_maker;
					$life_event_data['event_vehicle_model']	=	$request->vehicle_model;
					$life_event_data['event_vehicle_year']	=	$request->year_manufactured;
					$life_event_data['event_vehicle_color']	=	$request->vehicle_color;

					$life_event_when = 	[
											/*'when_from' 	=>	$request->from_date,
											'when_to'		=>	$request->to_date,*/
											'when_from_month' 	=>	$request->when_from_month,
											'when_from_day'		=>	$request->when_from_day,
											'when_from_year' 	=>	$request->when_from_year,
											'when_to_month'		=>	$request->when_to_month,
											'when_to_day' 		=>	$request->when_to_day,
											'when_to_year'		=>	$request->when_to_year,
											/*'when_time'		=>	$request->time,
											'when_time_ampm'=>	$request->time_ampm*/
										];
					$life_event_where=	[
											'where_country'	=>	$request->country,
											'where_state'	=>	$request->state,
											'where_city'	=>	$request->city,
											'where_street'	=>	$request->street,
											'where_zip'		=>	$request->zip,
											'where_email'	=>	$request->email,
											'where_phone'	=>	$request->phone,
											'where_isd'		=>	$request->isd_code
										];
				}else if($request->sub_category_id==3){
					$life_event_data['event_family_firstname']	=	$request->first_name;
					$life_event_data['event_family_lastname']	=	$request->last_name;
					$life_event_data['event_age']	=	$request->age;
					$life_event_data['event_gender']	=	$request->gender;
					$life_event_data['event_family_relation']	=	$request->relationship;
					$life_event_data['event_family_marital_status']	=	$request->marital_status;
					$life_event_data['event_family_industry']	=	$request->industry;
					$life_event_data['event_family_category']	=	$request->category;

					$life_event_when = 	[
											/*'when_family_birthday' 		=>	$request->birthday,
											'when_family_wedding'		=>	$request->anniversary,
											'when_family_passed'		=>	$request->passed*/
											'when_family_birth_day' 		=>	$request->when_family_birth_day,
											'when_family_birth_month'		=>	$request->when_family_birth_month,
											'when_family_birth_year'		=>	$request->when_family_birth_year,

											'when_family_wedding_day' 		=>	$request->when_family_wedding_day,
											'when_family_wedding_month'		=>	$request->when_family_wedding_month,
											'when_family_wedding_year'		=>	$request->when_family_wedding_year,

											'when_family_passed_day' 		=>	$request->when_family_passed_day,
											'when_family_passed_month'		=>	$request->when_family_passed_month,
											'when_family_passed_year'		=>	$request->when_family_passed_year
										];
					$life_event_where=	[
											'where_country'	=>	$request->country,
											'where_state'	=>	$request->state,
											'where_city'	=>	$request->city,
											'where_street'	=>	$request->street,
											'where_zip'		=>	$request->zip,
											'where_email'	=>	$request->email,
											'where_phone'	=>	$request->phone,
											'where_isd'		=>	$request->isd_code
										];
				}

			}else if($request->type_id==2 && in_array($request->sub_category_id,[4,5])){
				$life_event_data['event_age'] 					= $request->age;
				$life_event_data['event_gender'] 				= $request->gender;
				$life_event_data['event_ethnicity'] 			= $request->ethnicity;
				$life_event_data['event_race'] 					= $request->race;
				$life_event_data['event_nationality'] 			= $request->nationality;
				$life_event_data['event_orientation'] 			= $request->orientation;
				$life_event_data['event_body_style'] 			= $request->body_style;
				$life_event_data['event_skintone']				= $request->skin_tonne;
				$life_event_data['event_hair_color'] 			= $request->hair_color;
				$life_event_data['event_eye_color'] 			= $request->eye_color;
				$life_event_data['event_political'] 			= $request->political;
				$life_event_data['event_religion'] 				= $request->religion;
				$life_event_data['event_looking_for'] 			= $request->looking_for;
				$life_event_data['event_age_range'] 			= $request->age_range;
				$life_event_data['event_interest'] 				= $request->interests;
				$life_event_data['event_dating_search_for'] 	= (isset($request->search_for) && $request->search_for==1) ? 1 : 0;
				if($request->sub_category_id==5){
					$life_event_data['event_height'] 			= $request->height;
					$life_event_data['event_height_measure'] 	= $request->height_measure;
					$life_event_data['event_height_general'] 	= $request->height_general;
					$life_event_data['event_weight'] 			= $request->weight;
					$life_event_data['event_weight_measure'] 	= $request->weight_measure;
					$life_event_data['event_weight_general'] 	= $request->weight_general;
					$life_event_data['event_facial_hair'] 		= $request->facial_hair;
					$life_event_data['event_body_hair'] 		= $request->body_hair;
					$life_event_data['event_glasses'] 			= $request->glasses;
					$life_event_data['event_party'] 			= $request->party;
					$life_event_data['event_smoke'] 			= $request->smoke;
					$life_event_data['event_drink'] 			= $request->drink;
					$life_event_data['event_tattoos'] 			= $request->tattoo;
					$life_event_data['event_tattoo_type'] 		= $request->tattoo_type;
					$life_event_data['event_fav_food'] 			= $request->fav_food;
					$life_event_data['event_food_type'] 		= $request->food_type;
					$life_event_data['event_sports'] 			= $request->sport;
					$life_event_data['event_fav_team'] 			= $request->fav_team;
					$life_event_data['event_zodiac'] 			= $request->zodiac_sign;
					$life_event_data['event_birth_stone'] 		= $request->birth_stone;
					$life_event_data['event_affiliations'] 		= $request->affiliation;
					$life_event_data['event_career'] 			= $request->career;
					$life_event_data['event_financial'] 		= $request->financial;
					$life_event_data['event_family_relation'] 	= $request->relationship;
					$life_event_data['event_dating_specific'] 	= 1;
				}

				$life_event_when = 	[
										'when_timeframe' 	=>	$request->timeframe,
										'when_month'		=>	$request->time_month
									];
				$life_event_where=	[
										'where_country'	=>	$request->country,
										'where_state'	=>	$request->state,
										'where_city'	=>	$request->city,
										'where_radius'	=>	$request->radius
									];
			}else if($request->type_id==3 /*&& in_array($request->sub_category_id,[6,7,8])*/){
				$life_event_data['event_bio_mother'] = $request->bio_mother;
				$life_event_data['event_bio_father'] = $request->bio_father;
				$life_event_data['event_birthname'] = $request->birth_name;
				if(isset($request->user_is_parent) && in_array($request->user_is_parent, [1,2,3])){
					if($request->user_is_parent==1){
						$life_event_data['event_user_is_mother'] = 1;
					}elseif($request->user_is_parent==2){
						$life_event_data['event_user_is_father'] = 1;
					}else{
						$life_event_data['event_user_is_child'] = 1;
					}
				}else{
					$life_event_data['event_user_is_mother'] = isset($request->user_is_mother) && $request->user_is_mother==1 ? 1 : 0;
					$life_event_data['event_user_is_father'] = isset($request->user_is_father) && $request->user_is_father==1 ? 1 : 0;
				}
				
				$life_event_data['event_weight'] = $request->weight;
				$life_event_data['event_weight_measure'] = $request->weight_measure;
				$life_event_data['event_height'] = $request->height;
				$life_event_data['event_height_measure'] = $request->height_measure;
				$life_event_data['event_time_of_birth'] = $request->time_of_birth;
				$life_event_data['event_time_of_birth_ampm'] = $request->time_of_birth_ampm;
				$life_event_data['event_gender'] 				= 	$request->gender;

				$life_event_when = 	[
										/*'when_from' 	=>	$request->birth_date,*/
										'when_from_month' 	=>	$request->when_from_month,
										'when_from_day'		=>	$request->when_from_day,
										'when_from_year' 	=>	$request->when_from_year,
										/*'when_time'		=>	$request->time,
										'when_time_ampm'=>	$request->time_ampm*/
									];
				$life_event_where=	[
										'where_birth_place'		=>	$request->birth_place,
										'where_birth_country'	=>	$request->birth_country,
										'where_birth_state'		=>	$request->birth_state,
										'where_birth_city'		=>	$request->birth_city,
										'where_birth_street'	=>	$request->birth_street,
										'where_birth_zip'		=>	$request->birth_zip,

										'where_adoption_status'	=>	$request->adoption_status,
										'where_country'			=>	$request->country,
										'where_state'			=>	$request->state,
										'where_city'			=>	$request->city,
										'where_street'			=>	$request->street,
										'where_zip'				=>	$request->zip,
									];
			}else if($request->type_id==4 /*&& in_array($request->sub_category_id,[9,10,11,12])*/){
				$life_event_data['event_airport'] 		= $request->airport;
				$life_event_data['event_flight_number'] = $request->flight_number;
				$life_event_data['event_airline'] 		= $request->airline;
				$life_event_data['event_flight_row'] 	= $request->row;
				$life_event_data['event_flight_seat']	= $request->seat;

				$life_event_when = 	[
										/*'when_from' 			=>	$request->from_date,*/
										'when_from_month' 	=>	$request->when_from_month,
										'when_from_day'		=>	$request->when_from_day,
										'when_from_year' 	=>	$request->when_from_year,
										'when_to_month'		=>	$request->when_to_month,
										'when_to_day' 		=>	$request->when_to_day,
										'when_to_year'		=>	$request->when_to_year,
										'when_time'				=>	$request->dep_time,
										'when_time_ampm'		=>	$request->dep_time_ampm,
										/*'when_to' 				=>	$request->to_date,*/
										'when_arrival_time'		=>	$request->arr_time,
										'when_arrival_time_ampm'=>	$request->arr_time_ampm
									];
				$life_event_where=	[
										'where_location_type'	=>	$request->location_type,
										'where_hotel_name'		=>	$request->location_name,
										'where_country'			=>	$request->country,
										'where_state'			=>	$request->state,
										'where_city'			=>	$request->city,
										'where_street'			=>	$request->street,
										'where_zip'				=>	$request->zip
									];
			}else if($request->type_id==5){
				$life_event_data['event_military_branch'] 		= 	$request->military_branch;
				$life_event_data['event_gender'] 				= 	$request->gender;
				$life_event_data['event_age'] 					= 	$request->age;
				$life_event_data['event_military_unit1'] 		= 	$request->military_unit1;
				$life_event_data['event_military_unit2']		= 	$request->military_unit2;
				$life_event_data['event_military_unit3']		= 	$request->military_unit3;
				$life_event_data['event_military_unit4']		= 	$request->military_unit4;
				$life_event_data['event_military_unit5']		= 	$request->military_unit5;
				$life_event_data['event_military_unit6']		= 	$request->military_unit6;
				$life_event_data['event_military_rank']			= 	$request->military_rank;
				$life_event_data['event_military_mos']			= 	$request->military_mos;
				$life_event_data['event_military_title']		= 	$request->military_title;

				$life_event_data['event_dating_search_for'] 	= (isset($request->search_for) && $request->search_for==1) ? 1 : 0;

				$life_event_when = 	[
										/*'when_from' 			=>	$request->from_date,
										
										'when_to' 				=>	$request->to_date*/
										'when_from_month' 	=>	$request->when_from_month,
										'when_from_day'		=>	$request->when_from_day,
										'when_from_year' 	=>	$request->when_from_year,
										'when_to_month'		=>	$request->when_to_month,
										'when_to_day' 		=>	$request->when_to_day,
										'when_to_year'		=>	$request->when_to_year,
									];
				$life_event_where=	[
										'where_country'			=>	$request->country,
										'where_state'			=>	$request->state,
										'where_city'			=>	$request->city,
										'where_street'			=>	$request->street,
										'where_zip'				=>	$request->zip,
										'where_base'			=>	$request->military_base,
										'where_orders'			=>	$request->military_order
									];
			}else if($request->type_id==6 /*&& in_array($request->sub_category_id,[16,17,18,19,20,21,22,23,24,25,28,29])*/){
				$life_event_data['event_school_name'] 			= $request->school_name;
				$life_event_data['event_school_sorority'] 		= $request->school_sorority;
				$life_event_data['event_school_teacher'] 		= $request->school_teacher;
				$life_event_data['event_school_roomno'] 		= $request->school_roomno;
				$life_event_data['event_school_subject']		= $request->school_subject;
				$life_event_data['event_school_curriculars']	= $request->school_curriculars;
				$life_event_data['event_school_organization']	= $request->school_organization;
				$life_event_data['event_school_event']			= $request->school_event;
				$life_event_data['event_school_afterschool']	= $request->school_afterschool;
				$life_event_data['event_school_is_graduated'] 	= (isset($request->is_graduated) && $request->is_graduated==1) ? 1 : 0;

				$life_event_when = 	[
										/*'when_from' 			=>	$request->from_date,
										'when_to' 				=>	$request->to_date*/
										'when_from_month' 	=>	$request->when_from_month,
										'when_from_day'		=>	$request->when_from_day,
										'when_from_year' 	=>	$request->when_from_year,
										'when_to_month'		=>	$request->when_to_month,
										'when_to_day' 		=>	$request->when_to_day,
										'when_to_year'		=>	$request->when_to_year,
									];
				$life_event_where=	[
										'where_country'			=>	$request->country,
										'where_state'			=>	$request->state,
										'where_city'			=>	$request->city,
										'where_street'			=>	$request->street,
										'where_zip'				=>	$request->zip
									];
			}else if($request->type_id==7){
				$life_event_data['event_career_name'] 			= $request->career_name;
				$life_event_data['event_career_category'] 		= $request->career_category;
				$life_event_data['event_career_title'] 			= $request->career_title;
				$life_event_data['event_career_level'] 			= $request->career_level;

				$life_event_when = 	[
										/*'when_from' 			=>	$request->from_date,
										'when_to' 				=>	$request->to_date*/
										'when_from_month' 	=>	$request->when_from_month,
										'when_from_day'		=>	$request->when_from_day,
										'when_from_year' 	=>	$request->when_from_year,
										'when_to_month'		=>	$request->when_to_month,
										'when_to_day' 		=>	$request->when_to_day,
										'when_to_year'		=>	$request->when_to_year,
									];
				$life_event_where=	[
										'where_country'			=>	$request->country,
										'where_state'			=>	$request->state,
										'where_city'			=>	$request->city,
										'where_street'			=>	$request->street,
										'where_zip'				=>	$request->zip
									];
			}else if($request->type_id==8){
				$life_event_data['event_pet_name'] 			= $request->pet_name;
				$life_event_data['event_pet_breed'] 		= $request->pet_breed;
				$life_event_data['event_pet_species'] 		= $request->pet_species;
				$life_event_data['event_gender'] 			= $request->gender;
				$life_event_data['event_pet_temperment']	= $request->pet_temperment;
				$life_event_data['event_pet_color']			= $request->pet_color;
				$life_event_data['event_pet_size']			= $request->pet_size;
				$life_event_data['event_pet_license_number']= $request->pet_license_number;
				$life_event_data['event_pet_collar']		= $request->pet_collar;
				$life_event_data['event_pet_veterinary']	= $request->pet_veterinary;
				$life_event_data['event_pet_medical']		= $request->pet_medical;
				$life_event_data['event_dating_search_for'] 	= (isset($request->search_for) && $request->search_for==1) ? 1 : 0;

				$life_event_where=	[
										'where_country'			=>	$request->country,
										'where_state'			=>	$request->state,
										'where_city'			=>	$request->city,
										'where_street'			=>	$request->street,
										'where_zip'				=>	$request->zip
									];
			}else if($request->type_id==9){
				$life_event_data['event_item_name'] 			= $request->item_name;
				$life_event_data['event_item_size'] 			= $request->item_size;
				$life_event_data['event_item_material'] 		= $request->item_material;
				$life_event_data['event_outdoor'] 				= $request->outdoor;
				$life_event_data['event_indoor'] 				= $request->indoor;
				$life_event_data['event_inmotion'] 				= $request->inmotion;
				$life_event_data['event_other'] 				= $request->other;
				$life_event_data['event_dating_search_for'] 	= (isset($request->search_for) && $request->search_for==1) ? 1 : 0;

				$life_event_when = 	[
										/*'when_from' 			=>	$request->from_date,
										'when_to' 				=>	$request->to_date*/
										'when_from_month' 	=>	$request->when_from_month,
										'when_from_day'		=>	$request->when_from_day,
										'when_from_year' 	=>	$request->when_from_year,
										'when_to_month'		=>	$request->when_to_month,
										'when_to_day' 		=>	$request->when_to_day,
										'when_to_year'		=>	$request->when_to_year,
									];
				$life_event_where=	[
										'where_country'			=>	$request->country,
										'where_state'			=>	$request->state,
										'where_city'			=>	$request->city,
										'where_street'			=>	$request->street,
										'where_zip'				=>	$request->zip
									];
			}else if($request->type_id==11){
				$life_event_data['event_game'] 				= $request->game_name;
				$life_event_data['event_gameteam'] 			= $request->team_name;
				$life_event_data['event_username'] 			= $request->username;
				$life_event_data['event_dating_search_for'] = (isset($request->search_for) && $request->search_for==1) ? 1 : 0;

				$life_event_when = 	[
										'when_from_month' 	=>	$request->when_from_month,
										'when_from_day'		=>	$request->when_from_day,
										'when_from_year' 	=>	$request->when_from_year,
									];
			}else if($request->type_id==12){
				$life_event_data['event_game'] 				= $request->game_name;
				$life_event_data['event_gameteam'] 			= $request->team_name;

				$life_event_data['event_activity_type'] 	= $request->activity_type;
				$life_event_data['event_entertainer'] 		= $request->entertainer;
				$life_event_data['event_key_person'] 		= $request->key_person;

				$life_event_data['event_flight_row'] 		= $request->row;
				$life_event_data['event_flight_seat']		= $request->seat;

				$life_event_when = 	[
										/*'when_from' 			=>	$request->from_date,*/
										'when_from_month' 	=>	$request->when_from_month,
										'when_from_day'		=>	$request->when_from_day,
										'when_from_year' 	=>	$request->when_from_year,
										'when_to_month'		=>	$request->when_to_month,
										'when_to_day' 		=>	$request->when_to_day,
										'when_to_year'		=>	$request->when_to_year,
										'when_time'				=>	$request->dep_time,
										'when_time_ampm'		=>	$request->dep_time_ampm,
										/*'when_to' 				=>	$request->to_date,*/
										'when_arrival_time'		=>	$request->time,
										'when_arrival_time_ampm'=>	$request->time_ampm
									];
				$life_event_where=	[
										'where_location_type'	=>	$request->location_type,
										'where_hotel_name'		=>	$request->location_name,
										'where_country'			=>	$request->country,
										'where_state'			=>	$request->state,
										'where_city'			=>	$request->city,
										'where_street'			=>	$request->street,
										'where_zip'				=>	$request->zip
									];
			}

			/*if($request->user_id==59){
				return response([
                				'response_code'	=>	'200',
                         		'response_msg'	=> 	'Life Event Uploaded successfully',
                         		'reponse_body' 	=> 	'null',
                         		'redirect'		=>	url('home')
                        	], 200);
				exit();
			}*/
			$life_event_id = DB::table('life_events')->insertGetId($life_event_data);
			if(!in_array($request->type_id,[8,10])){
				$life_event_when['when_event_id'] = $life_event_id;
				DB::table('life_event_when')->insert($life_event_when);
			}
			if(!in_array($request->type_id,[10,11])){
				$life_event_where['where_event_id'] = $life_event_id;
				DB::table('life_event_where')->insert($life_event_where);
			}
			
			if($request->event_pictures){

				$images = explode('@WhatscommonArnabSarbeswar',$request->event_pictures);
				if(is_array($images)){
					foreach ($images as $key => $image) {
						$image_name = 'life_events_'.time().rand(1,9999).'.png';
						$image_path = public_path('img/upload/'.$image_name);
						$image_url = url('img/upload/'.$image_name);
						$poc =  \Image::make($image)->save($image_path);
						DB::table('life_event_images')->insert(	[
																	'image_event_id' 	=> $life_event_id,
																	'image_name' 		=> $image_name,
																	'image_url'			=> $image_url
																]);
					}
				}
				
			}

			if($request->image_counter && $request->image_counter>0){
				/*print_r($_FILES);exit();*/
				for($i=1; $i<=$request->image_counter; $i++){
					
					$req_name = 'images'.$i;
					if(isset($_FILES[$req_name]) && !empty($_FILES[$req_name])){
						$image_name = 'life_events_web_'.time().rand(1,9999).'.png';
						$image_path = public_path('img/upload/'.$image_name);
						$image_url = url('img/upload/'.$image_name);
			            /*$filePath = $request->file('user_picture')->storeAs('profilePictures', $fileName, 'public');*/
			            $request->{$req_name}->move(public_path('img/upload'), $image_name);
			            /*$profUrl = url('/profilePictures/'.$fileName);*/
			            DB::table('life_event_images')->insert(	[
																		'image_event_id' 	=> $life_event_id,
																		'image_name' 		=> $image_name,
																		'image_url'			=> $image_url
																]);
					}
					/*$fileName = time().'_'.$request->{$req_name}->getClientOriginalName();*/
					
				}
			}

			if($request->event_keywords ){
				$keywords = explode(',',$request->event_keywords);
				if(is_array($keywords)){
					foreach($keywords as $key => $keyword){
						$key_data = DB::table('keywords')->select('keyid')->where('userkeys',$keyword)->first();
						if(!$key_data){
							$keyid = DB::table('keywords')->insertGetId(['userkeys' => $keyword]);
						}else{
							$keyid = $key_data->keyid;
						}
						DB::table('life_event_keywords')->insert(['lek_event_id' => $life_event_id, 'lek_key_id'=>$keyid]);
					}
				}
			}
			if(!$request->is_draft || (isset($request->is_draft) && $request->is_draft==0)){
				$this->matchEvent($life_event_id, $request->type_id, $request->user_id);
			}
			
			if($request->add_another && $request->add_another==1){
				$redirect = url('create-lifeevent/?type='.$request->type_id);
			}else if(($request->show_life_event && $request->show_life_event==1)){
				$redirect = url('home/?type='.$request->type_id);
			}else{
				$redirect = url('home');
			}
			return response([
                				'response_code'	=>	'200',
                         		'response_msg'	=> 	'Life Event Uploaded successfully',
                         		'reponse_body' 	=> 	'null',
                         		'redirect'		=>	$redirect
                        	], 200);
		}
    }

    public function reverseLookup(Request $request){
    	$rules =['user_id' => 'required'];
		if(!$request->email && !$request->phone){
			$rules['email'] = 'required';
			$rules['phone'] = 'required';
		}
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$where = '';
			if($request->email && $request->phone){
				$where .= ' AND users.email="'.$request->email.'" OR users.phone="'.$request->phone.'"';
			}else if($request->email && !$request->phone){
				$where .= ' AND users.email="'.$request->email.'"';
			}else if(!$request->email && $request->phone){
				$where .= ' AND users.phone="'.$request->phone.'"';
			}

			if($request->dob){
				$where .= ' OR userdetails.dob = "'.$request->dob.'"';
			}

			if($request->dob_day && $request->dob_month && $request->dob_year){
				$where .= ' OR (userdetails.dob_day = "'.$request->dob_day.'" AND userdetails.dob_month = "'.$request->dob_month.'" AND userdetails.dob_year = "'.$request->dob_year.'")';
			}if($request->dob_day && $request->dob_month && !$request->dob_year){
				$where .= ' OR (userdetails.dob_day = "'.$request->dob_day.'" AND userdetails.dob_month = "'.$request->dob_month.'")';
			}
			if($request->dob_day && !$request->dob_month && $request->dob_year){
				$where .= ' OR (userdetails.dob_day = "'.$request->dob_day.'" AND userdetails.dob_year = "'.$request->dob_year.'")';
			}
			if(!$request->dob_day && $request->dob_month && $request->dob_year){
				$where .= ' OR (userdetails.dob_month = "'.$request->dob_month.'" AND userdetails.dob_year = "'.$request->dob_year.'")';
			}
			if($request->dob_month && !$request->dob_day && !$request->dob_year){
				$where .= ' OR userdetails.dob_month = "'.$request->dob_month.'"';
			}
			if($request->dob_year && !$request->dob_month && !$request->dob_day){
				$where .= ' OR userdetails.dob_year = "'.$request->dob_year.'"';
			}
			if(!$request->dob_year && !$request->dob_month && $request->dob_day){
				$where .= ' OR userdetails.dob_day = "'.$request->dob_day.'"';
			}

			if($request->country){
				$where .= ' OR userdetails.country = "'.$request->country.'"';
			}

			if($request->state){
				$where .= ' OR userdetails.state = "'.$request->state.'"';
			}

			if($request->city){
				$where .= ' OR userdetails.city = "'.$request->city.'"';
			}

			if($request->street){
				$where .= ' OR userdetails.street = "'.$request->street.'"';
			}

			if($request->zip){
				$where .= ' OR userdetails.zip = "'.$request->zip.'"';
			}

			//echo $where;
			/*$users = DB::table('users')
						->join('userdetails', 'users.id', '=', 'userdetails.id')
						->leftJoin('countries','userdetails.country','=','countries._country_id')
                		->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                		->leftJoin('cities','userdetails.city','=','cities._city_id')
                		->select('userdetails.*', 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                		->where('userdetails.id', '!=', $request->user_id)
                		->orWhereRaw($where)
                		->get();*/
            $users = DB::select('SELECT userdetails.*, users.profile_photo_path, countries.country_name, provinces.province_name,cities.city_name, 0 AS match_count, users.quick_blox_id, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status =1 AND ((request_from_user="'.$request->user_id.'" AND request_to_user=users.id) OR (request_from_user=users.id AND request_to_user="'.$request->user_id.'"))), 1, 0)) AS is_connected FROM users 
            						INNER JOIN userdetails ON users.id=userdetails.id
            						LEFT JOIN countries ON userdetails.country=countries._country_id
            						LEFT JOIN provinces ON userdetails.state=provinces._province_id
            						LEFT JOIN cities ON userdetails.city=cities._city_id
            						WHERE userdetails.id!="'.$request->user_id.'" AND users.user_is_active=1'.$where.'
            					');
            $reverse_lookup_div = view('xhr.reverse_lookup',['users' => $users])->render();

            return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'Users Fetched Successfully',
                  				'reponse_body' 	=> 	[
                  										'users' 	=> 	$users,
                  										'html' 		=> 	$reverse_lookup_div,
                  										'html_id'	=>	'reverse-lookup-div'
                  								]
                  			], 200);
		}
    }

    public function matchEvent($event_id, $event_type, $event_user_id){
    	$event_data = DB::table('life_events')->leftJoin('life_event_sub_categories','life_events.event_subcategory_id','=','life_event_sub_categories.id')->where('_event_id',$event_id)->first();
    	$where_data = DB::table('life_event_where')->where('where_event_id',$event_id)->first();
    	$when_data  = DB::table('life_event_when')->where('when_event_id',$event_id)->first();
    	$pre_event_images = DB::table('life_event_images')->where('image_event_id',$event_id)->get();
    	$pre_keywords = DB::table('life_event_keywords')
    						->join('keywords','keywords.keyid','=','life_event_keywords.lek_key_id')
    						->where('lek_event_id',$event_id)
    						->get();
    	/*$life_events = 	DB::table('life_events')
    						->where('_event_id','!=',$event_id)
    						->where('event_type_id',$event_type)
    						->where('event_user_id','!=',$event_user_id)
    						->get();*/
    	$life_events = DB::select("SELECT * FROM life_events WHERE _event_id!='".$event_id."' AND event_type_id='".$event_type."' AND event_is_draft=0 AND event_user_id!='".$event_user_id."' AND event_user_id NOT IN(SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id='".$event_user_id."')");
    	
    	$pre_keywords_csv = [];
    	if($pre_keywords){
    		foreach ($pre_keywords as $key => $value) {
    			array_push($pre_keywords_csv,$value->keyid);
    		}
    	}
    	if($life_events){
    		foreach($life_events as $event){
    			$where = DB::table('life_event_where')->where('where_event_id',$event->_event_id)->first();
    			$when = DB::table('life_event_when')->where('when_event_id',$event->_event_id)->first();
    			$event_images = DB::table('life_event_images')->where('image_event_id',$event->_event_id)->get();
    			$event_keywords = DB::table('life_event_keywords')
    						->join('keywords','keywords.keyid','=','life_event_keywords.lek_key_id')
    						->where('lek_event_id',$event->_event_id)
    						->get();
    			$match_count = 0;
    			$match_what = [];
    			$match_when = '';
    			$match_where = [];
    			$match_keyword = [];
    			if($event_keywords && $pre_keywords_csv){
    				foreach($event_keywords as $key => $val){
    					if(in_array($val->keyid,$pre_keywords_csv)){
    						$match_count++;
                    		array_push($match_keyword,$val->userkeys);
    					}
    				}
    			}
    			//print_r($match_keyword);exit();
	    		/*if($where_data){
	    			if($where_data->where_birth_place && $where_data->where_birth_place==$where->where_birth_place){
	                    $match_count++;
	                    array_push($match_where,$where_data->where_birth_place);
	                }
	                if($where_data->where_adoption_status && $where_data->where_adoption_status==$where->where_adoption_status){
	                    $match_count++;
	                    array_push($match_where,$where_data->where_adoption_status);
	                }
	                if($where_data->where_location_type && $where_data->where_location_type==$where->where_location_type){
	                    $match_count++;
	                    array_push($match_where,$where_data->where_location_type);
	                }
	                if($where_data->where_hotel_name && $where_data->where_hotel_name==$where->where_hotel_name){
	                    $match_count++;
	                    array_push($match_where,$where_data->where_hotel_name);
	                }
	                if($where_data->where_base && $where_data->where_base==$where->where_base){
	                    $match_count++;
	                    array_push($match_where,$where_data->where_base);
	                }
	                if($where_data->where_orders && $where_data->where_orders==$where->where_orders){
	                    $match_count++;
	                    array_push($match_where,$where_data->where_orders);
	                }
	    			if($where_data->where_street && $where_data->where_street==$where->where_street){
	                    $match_count++;
	                    array_push($match_where,$where_data->where_street);
	                }
	                if($where_data->where_province && $where_data->where_province==$where->where_province){
	                    $match_count++;
	                    array_push($match_where,$where_data->where_province);
	                }
	                if($where_data->where_city && $where_data->where_city==$where->where_city){
	                    $match_count++;
	                    array_push($match_where,$where_data->where_city);
	                }
	                if($where_data->where_state && $where_data->where_state==$where->where_state){
	                    $match_count++;
	                    array_push($match_where,$where_data->where_state);
	                }
	                if($where_data->where_zip && $where_data->where_zip==$where->where_zip){
	                    $match_count++;
	                    array_push($match_where,'ZIP : '.$where_data->where_zip);
	                }
	                if($where_data->where_country && $where_data->where_country==$where->where_country){
	                    $match_count++;
	                    array_push($match_where,$where_data->where_country);
	                }
	            }*/
    			if($event_data->event_subcategory_id && $event_data->event_subcategory_id==$event->event_subcategory_id){
    				$match_count++;
    				array_push($match_what,$event_data->sub_cat_type_name);
    				if($event_data->event_subcategory_id==1){
    					if($event_data->event_home_type && $event_data->event_home_type==$event->event_home_type){
                            $match_count++;
                            array_push($match_what,$event_data->event_home_type);
                        }
                        if($event_data->event_home_style && $event_data->event_home_style==$event->event_home_style){
                            $match_count++;
                            array_push($match_what,$event_data->event_home_style);
                        }
    				}else if($event_data->event_subcategory_id==2){
                        if($event_data->event_tag_number && $event_data->event_tag_number==$event->event_tag_number){
                            $match_count++;
                            array_push($match_what,$event_data->event_tag_number);
                        }
                        if($event_data->event_vehicle_type && $event_data->event_vehicle_type==$event->event_vehicle_type){
                            $match_count++;
                            array_push($match_what,$event_data->event_vehicle_type);
                        }
                        if($event_data->event_vehicle_maker && $event_data->event_vehicle_maker==$event->event_vehicle_maker){
                            $match_count++;
                            array_push($match_what,$event_data->event_vehicle_maker);
                        }
                        if($event_data->event_vehicle_model && $event_data->event_vehicle_model==$event->event_vehicle_model){
                            $match_count++;
                            array_push($match_what,$event_data->event_vehicle_model);
                        }
                        if($event_data->event_vehicle_year && $event_data->event_vehicle_year==$event->event_vehicle_year){
                            $match_count++;
                            array_push($match_what,$event_data->event_vehicle_year);
                        }
                        if($event_data->event_vehicle_color && $event_data->event_vehicle_color==$event->event_vehicle_color){
                            $match_count++;
                            array_push($match_what,$event_data->event_vehicle_color);
                        }
                    }else if($event_data->event_subcategory_id==3){
                        if($event_data->event_family_firstname && $event_data->event_family_firstname==$event->event_family_firstname){
                            $match_count++;
                            array_push($match_what,$event_data->event_family_firstname);
                        }
                        if($event_data->event_family_lastname && $event_data->event_family_lastname==$event->event_family_lastname){
                            $match_count++;
                            array_push($match_what,$event_data->event_family_lastname);
                        }
                        if($event_data->event_age && $event_data->event_age==$event->event_age){
                            $match_count++;
                            array_push($match_what,$event_data->event_age.' years');
                        }
                        if($event_data->event_gender && $event_data->event_gender==$event->event_gender){
                            $match_count++;
                            array_push($match_what,$event_data->event_gender);
                        }
                        if($event_data->event_family_relation && $event_data->event_family_relation==$event->event_family_relation){
                            $match_count++;
                            array_push($match_what,$event_data->event_family_relation);
                        }
                        if($event_data->event_family_marital_status && $event_data->event_family_marital_status == $event->event_family_marital_status){
                            $match_count++;
                            array_push($match_what,$event_data->event_family_marital_status);
                        }
                        if($event_data->event_family_industry && $event_data->event_family_industry == $event->event_family_industry){
                            $match_count++;
                            array_push($match_what,$event_data->event_family_industry);
                        }
                        if($event_data->event_family_category && $event_data->event_family_category == $event->event_family_category){
                            $match_count++;
                            array_push($match_what,$event_data->event_family_category);
                        }
                    }else if(in_array($event_data->event_subcategory_id,[4,5])){
                        if($event_data->event_gender && $event_data->event_gender==$event->event_gender){
                            $match_count++;
                            array_push($match_what,$event_data->event_gender);
                        }
                        if($event_data->event_ethnicity && $event_data->event_ethnicity==$event->event_ethnicity){
                            $match_count++;
                            array_push($match_what,$event_data->event_ethnicity);
                        }
                        if($event_data->event_age && $event_data->event_age==$event->event_age){
                            $match_count++;
                            array_push($match_what,$event_data->event_age.' years');
                        }
                        if($event_data->event_race && $event_data->event_race==$event->event_race){
                            $match_count++;
                            array_push($match_what,$event_data->event_race);
                        }
                        if($event_data->event_nationality && $event_data->event_nationality==$event->event_nationality){
                            $match_count++;
                            array_push($match_what,$event_data->event_nationality);
                        }
                        if($event_data->event_orientation && $event_data->event_orientation == $event->event_orientation){
                            $match_count++;
                            array_push($match_what,$event_data->event_orientation);
                        }
                        if($event_data->event_body_style && $event_data->event_body_style == $event->event_body_style){
                            $match_count++;
                            array_push($match_what,$event_data->event_body_style);
                        }
                        if($event_data->event_skintone && $event_data->event_skintone == $event->event_skintone){
                            $match_count++;
                            array_push($match_what,$event_data->event_skintone);
                        }
                        if($event_data->event_hair_color && $event_data->event_hair_color == $event->event_hair_color){
                            $match_count++;
                            array_push($match_what,$event_data->event_hair_color);
                        }
                        if($event_data->event_eye_color && $event_data->event_eye_color == $event->event_eye_color){
                            $match_count++;
                            array_push($match_what,$event_data->event_eye_color);
                        }
                        if($event_data->event_political && $event_data->event_political == $event->event_political){
                            $match_count++;
                            array_push($match_what,$event_data->event_political);
                        }
                        if($event_data->event_religion && $event_data->event_religion == $event->event_religion){
                            $match_count++;
                            array_push($match_what,$event_data->event_religion);
                        }
                        if($event_data->event_looking_for && $event_data->event_looking_for == $event->event_looking_for){
                            $match_count++;
                            array_push($match_what,$event_data->event_looking_for);
                        }
                        if($event_data->event_age_range && $event_data->event_age_range == $event->event_age_range){
                            $match_count++;
                            array_push($match_what,$event_data->event_age_range);
                        }
                        if($event_data->event_subcategory_id==5){
                            if($event_data->event_height && $event_data->event_height_measure && $event_data->event_height == $event->event_height && $event_data->event_height_measure == $event->event_height_measure){
                                $match_count++;
                                array_push($match_what,$event_data->event_height.' '.$event_data->event_height_measure);
                            }
                            if($event_data->event_weight && $event_data->event_weight_measure && $event_data->event_weight == $event->event_weight && $event_data->event_weight_measure == $event->event_weight_measure){
                                $match_count++;
                                array_push($match_what,$event_data->event_weight.' '.$event_data->event_weight_measure);
                            }
                            if($event_data->event_height_general && $event_data->event_height_general == $event->event_height_general){
                                $match_count++;
                                array_push($match_what,$event_data->event_height_general);
                            }
                            if($event_data->event_weight_general && $event_data->event_weight_general == $event->event_weight_general){
                                $match_count++;
                                array_push($match_what,$event_data->event_weight_general);
                            }
                            if($event_data->event_facial_hair && $event_data->event_facial_hair == $event->event_facial_hair){
                                $match_count++;
                                array_push($match_what,$event_data->event_facial_hair);
                            }
                            if($event_data->event_body_hair && $event_data->event_body_hair == $event->event_body_hair){
                                $match_count++;
                                array_push($match_what,$event_data->event_body_hair);
                            }
                            if($event_data->event_tattoo_type && $event_data->event_tattoo_type == $event->event_tattoo_type){
                                $match_count++;
                                array_push($match_what,$event_data->event_tattoo_type);
                            }
                            if($event_data->event_fav_food && $event_data->event_fav_food == $event->event_fav_food){
                                $match_count++;
                                array_push($match_what,$event_data->event_fav_food);
                            }
                            if($event_data->event_food_type && $event_data->event_food_type == $event->event_food_type){
                                $match_count++;
                                array_push($match_what,$event_data->event_food_type);
                            }
                            if($event_data->event_sports && $event_data->event_sports == $event->event_sports){
                                $match_count++;
                                array_push($match_what,$event_data->event_sports);
                            }
                            if($event_data->event_fav_team && $event_data->event_fav_team == $event->event_fav_team){
                                $match_count++;
                                array_push($match_what,$event_data->event_fav_team);
                            }
                            if($event_data->event_zodiac && $event_data->event_zodiac == $event->event_zodiac){
                                $match_count++;
                                array_push($match_what,$event_data->event_zodiac);
                            }
                            if($event_data->event_birth_stone && $event_data->event_birth_stone == $event->event_birth_stone){
                                $match_count++;
                                array_push($match_what,$event_data->event_birth_stone);
                            }
                            if($event_data->event_affiliations && $event_data->event_affiliations == $event->event_affiliations){
                                $match_count++;
                                array_push($match_what,$event_data->event_affiliations);
                            }
                            if($event_data->event_career && $event_data->event_career == $event->event_career){
                                $match_count++;
                                array_push($match_what,$event_data->event_career);
                            }
                            if($event_data->event_financial && $event_data->event_financial == $event->event_financial){
                                $match_count++;
                                array_push($match_what,$event_data->event_financial);
                            }
                            if($event_data->event_family_relation && $event_data->event_family_relation == $event->event_family_relation){
                                $match_count++;
                                array_push($match_what,$event_data->event_family_relation);
                            }
                        }
                    }
                    else if(in_array($event_data->event_subcategory_id,[6,7,8])){
                        if($event_data->event_bio_mother && $event_data->event_bio_mother == $event->event_bio_mother){
                            $match_count++;
                            array_push($match_what,$event_data->event_bio_mother);
                        }
                        if($event_data->event_bio_father && $event_data->event_bio_father == $event->event_bio_father){
                            $match_count++;
                            array_push($match_what,$event_data->event_bio_father);
                        }
                        if($event_data->event_birthname && $event_data->event_birthname == $event->event_birthname){
                            $match_count++;
                            array_push($match_what,$event_data->event_birthname);
                        }
                        if($event_data->event_weight && $event_data->event_weight_measure && $event_data->event_weight == $event->event_weight && $event_data->event_weight_measure == $event->event_weight_measure){
                            $match_count++;
                            array_push($match_what,$event_data->event_weight.' '.$event_data->event_weight_measure);
                        }
                    }
                    else if(in_array($event_data->event_subcategory_id,[9,10,11,12])){
                    	if($event_data->event_airport && $event_data->event_airport == $event->event_airport){
                            $match_count++;
                            array_push($match_what,$event_data->event_airport);
                        }
                        if($event_data->event_flight_number && $event_data->event_flight_number == $event->event_flight_number){
                            $match_count++;
                            array_push($match_what,$event_data->event_flight_number);
                        }
                        if($event_data->event_airline && $event_data->event_airline == $event->event_airline){
                            $match_count++;
                            array_push($match_what,$event_data->event_airline);
                        }
                        if($event_data->event_flight_row && $event_data->event_flight_row == $event->event_flight_row){
                            $match_count++;
                            array_push($match_what,'flight row :'.$event_data->event_flight_row);
                        }
                        if($event_data->event_flight_seat && $event_data->event_flight_seat == $event->event_flight_seat){
                            $match_count++;
                            array_push($match_what,'seat no. :'.$event_data->event_flight_seat);
                        }
                    }else if(in_array($event_data->event_subcategory_id,[16,17,18,19,20,21,22,23,24,25])){
                    	if($event_data->event_school_name && $event_data->event_school_name == $event->event_school_name){
                            $match_count++;
                            array_push($match_what,$event_data->event_school_name);
                        }
                        if($event_data->event_school_sorority && $event_data->event_school_sorority == $event->event_school_sorority){
                            $match_count++;
                            array_push($match_what,$event_data->event_school_sorority);
                        }
                        if($event_data->event_school_teacher && $event_data->event_school_teacher == $event->event_school_teacher){
                            $match_count++;
                            array_push($match_what,$event_data->event_school_teacher);
                        }
                        if($event_data->event_school_roomno && $event_data->event_school_roomno == $event->event_school_roomno){
                            $match_count++;
                            array_push($match_what,'Room No. :'.$event_data->event_school_roomno);
                        }
                        if($event_data->event_school_subject && $event_data->event_school_subject == $event->event_school_subject){
                            $match_count++;
                            array_push($match_what,$event_data->event_school_subject);
                        }
                        if($event_data->event_school_curriculars && $event_data->event_school_curriculars == $event->event_school_curriculars){
                            $match_count++;
                            array_push($match_what,$event_data->event_school_curriculars);
                        }
                        if($event_data->event_school_organization && $event_data->event_school_organization == $event->event_school_organization){
                            $match_count++;
                            array_push($match_what,$event_data->event_school_organization);
                        }
                        if($event_data->event_school_event && $event_data->event_school_event == $event->event_school_event){
                            $match_count++;
                            array_push($match_what,$event_data->event_school_event);
                        }
                        if($event_data->event_school_afterschool && $event_data->event_school_afterschool == $event->event_school_afterschool){
                            $match_count++;
                            array_push($match_what,$event_data->event_school_afterschool);
                        }
                    }
    			}else{
                	if($event_data->event_type_id==5){
                		if($event_data->event_military_branch && $event_data->event_military_branch == $event->event_military_branch){
                            $match_count++;
                            array_push($match_what,$event_data->event_military_branch);
                        }
                        if($event_data->event_gender && $event_data->event_gender == $event->event_gender){
                            $match_count++;
                            array_push($match_what,$event_data->event_gender);
                        }
                        if($event_data->event_age && $event_data->event_age == $event->event_age){
                            $match_count++;
                            array_push($match_what,$event_data->event_age.' years');
                        }
                        if($event_data->event_military_unit1 && $event_data->event_military_unit1 == $event->event_military_unit1){
                            $match_count++;
                            array_push($match_what,'Unit1 : '.$event_data->event_military_unit1);
                        }
                        if($event_data->event_military_unit2 && $event_data->event_military_unit2 == $event->event_military_unit2){
                            $match_count++;
                            array_push($match_what,'Unit2 : '.$event_data->event_military_unit2);
                        }
                        if($event_data->event_military_unit3 && $event_data->event_military_unit3 == $event->event_military_unit3){
                            $match_count++;
                            array_push($match_what,'Unit3 : '.$event_data->event_military_unit3);
                        }
                        if($event_data->event_military_unit4 && $event_data->event_military_unit4 == $event->event_military_unit4){
                            $match_count++;
                            array_push($match_what,'Unit4 : '.$event_data->event_military_unit3);
                        }
                        if($event_data->event_military_unit5 && $event_data->event_military_unit5 == $event->event_military_unit5){
                            $match_count++;
                            array_push($match_what,'Unit5 : '.$event_data->event_military_unit3);
                        }
                        if($event_data->event_military_unit6 && $event_data->event_military_unit6 == $event->event_military_unit6){
                            $match_count++;
                            array_push($match_what,'Unit6 : '.$event_data->event_military_unit3);
                        }
                        if($event_data->event_military_rank && $event_data->event_military_rank == $event->event_military_rank){
                            $match_count++;
                            array_push($match_what,$event_data->event_military_rank);
                        }
                        if($event_data->event_military_mos && $event_data->event_military_mos == $event->event_military_mos){
                            $match_count++;
                            array_push($match_what,$event_data->event_military_mos);
                        }
                        if($event_data->event_military_title && $event_data->event_military_title == $event->event_military_title){
                            $match_count++;
                            array_push($match_what,$event_data->event_military_title);
                        }
                	}else if($event_data->event_type_id==7){
                		if($event_data->event_career_name && $event_data->event_career_name == $event->event_career_name){
                            $match_count++;
                            array_push($match_what,$event_data->event_career_name);
                        }
                        if($event_data->event_career_category && $event_data->event_career_category == $event->event_career_category){
                            $match_count++;
                            array_push($match_what,$event_data->event_career_category);
                        }
                        if($event_data->event_career_title && $event_data->event_career_title == $event->event_career_title){
                            $match_count++;
                            array_push($match_what,$event_data->event_career_title);
                        }
                        if($event_data->event_career_level && $event_data->event_career_level == $event->event_career_level){
                            $match_count++;
                            array_push($match_what,$event_data->event_career_level);
                        }
                	}else if($event_data->event_type_id==8){
                		if($event_data->event_pet_name && $event_data->event_pet_name == $event->event_pet_name){
                            $match_count++;
                            array_push($match_what,$event_data->event_pet_name);
                        }
                        if($event_data->event_pet_breed && $event_data->event_pet_breed == $event->event_pet_breed){
                            $match_count++;
                            array_push($match_what,$event_data->event_pet_breed);
                        }
                        if($event_data->event_pet_species && $event_data->event_pet_species == $event->event_pet_species){
                            $match_count++;
                            array_push($match_what,$event_data->event_pet_species);
                        }
                        if($event_data->event_gender && $event_data->event_gender == $event->event_gender){
                            $match_count++;
                            array_push($match_what,$event_data->event_gender);
                        }
                        if($event_data->event_pet_temperment && $event_data->event_pet_temperment == $event->event_pet_temperment){
                            $match_count++;
                            array_push($match_what,$event_data->event_pet_temperment);
                        }
                        if($event_data->event_pet_color && $event_data->event_pet_color == $event->event_pet_color){
                            $match_count++;
                            array_push($match_what,$event_data->event_pet_color);
                        }
                        if($event_data->event_pet_size && $event_data->event_pet_size == $event->event_pet_size){
                            $match_count++;
                            array_push($match_what,$event_data->event_pet_size);
                        }
                        if($event_data->event_pet_license_number && $event_data->event_pet_license_number == $event->event_pet_license_number){
                            $match_count++;
                            array_push($match_what,$event_data->event_pet_license_number);
                        }
                        if($event_data->event_pet_collar && $event_data->event_pet_collar == $event->event_pet_collar){
                            $match_count++;
                            array_push($match_what,$event_data->event_pet_collar);
                        }
                        if($event_data->event_pet_veterinary && $event_data->event_pet_veterinary == $event->event_pet_veterinary){
                            $match_count++;
                            array_push($match_what,$event_data->event_pet_veterinary);
                        }
                        if($event_data->event_pet_medical && $event_data->event_pet_medical == $event->event_pet_medical){
                            $match_count++;
                            array_push($match_what,$event_data->event_pet_medical);
                        }
                	}else if($event_data->event_type_id==9){
                		if($event_data->event_item_name && $event_data->event_item_name == $event->event_item_name){
                            $match_count++;
                            array_push($match_what,$event_data->event_item_name);
                        }
                        if($event_data->event_item_size && $event_data->event_item_size == $event->event_item_size){
                            $match_count++;
                            array_push($match_what,$event_data->event_item_size);
                        }
                        if($event_data->event_item_material && $event_data->event_item_material == $event->event_item_material){
                            $match_count++;
                            array_push($match_what,$event_data->event_item_material);
                        }
                        if($event_data->event_outdoor && $event_data->event_outdoor == $event->event_outdoor){
                            $match_count++;
                            array_push($match_what,$event_data->event_outdoor);
                        }
                        if($event_data->event_indoor && $event_data->event_indoor == $event->event_indoor){
                            $match_count++;
                            array_push($match_what,$event_data->event_indoor);
                        }
                        if($event_data->event_inmotion && $event_data->event_inmotion == $event->event_inmotion){
                            $match_count++;
                            array_push($match_what,$event_data->event_inmotion);
                        }
                        if($event_data->event_other && $event_data->event_other == $event->event_other){
                            $match_count++;
                            array_push($match_what,$event_data->event_other);
                        }
                	}
                	else if($event_data->event_type_id==11){
                		if($event_data->event_game && $event_data->event_game == $event->event_game){
                            $match_count++;
                            array_push($match_what,$event_data->event_game);
                        }
                        if($event_data->event_gameteam && $event_data->event_gameteam == $event->event_gameteam){
                            $match_count++;
                            array_push($match_what,$event_data->event_gameteam);
                        }
                        if($event_data->event_username && $event_data->event_username == $event->event_username){
                            $match_count++;
                            array_push($match_what,$event_data->event_username);
                        }
                    }
                }

    			if($match_count >= 2){
    				$feed_what = implode(', ', $match_what);
    				$feed_where = implode(', ', $match_where);
    				$feed_keywords = implode(', ',$match_keyword);
    				$feed_id = DB::table('feeds')->insertGetId([
    											'feed_lifeevent' 		=> 	$event_type,
    											'feed_user1'			=>	$event_data->event_user_id,
    											'feed_user2'			=>	$event->event_user_id,
    											'feed_match_count'		=>	$match_count,
    											'feed_what'				=>	$feed_what,
    											'feed_where'			=>	$feed_where,
    											'feed_keywords'			=>	$feed_keywords,
    											'feed_eventid'			=>	$event_data->_event_id,
    											'feed_matched_eventid'	=>	$event->_event_id
    										]);
    				if($pre_event_images){
    					foreach ($pre_event_images as $key => $img) {
    						DB::table('feed_images')->insert([
																'fi_feed_id' 	=> $feed_id,
																'fi_img_name'	=> $img->image_name,
																'fi_img_url'	=> $img->image_url
															]);
    					}
    				}
    				if($event_images){
    					foreach ($event_images as $key => $img) {
    						DB::table('feed_images')->insert([
																'fi_feed_id' 	=> $feed_id,
																'fi_img_name'	=> $img->image_name,
																'fi_img_url'	=> $img->image_url
															]);
    					}
    				}

    				$to_user1_data = DB::table('users')
										->join('device_tokens','users.id','=','device_tokens.device_user_id')
										->select('device_tokens.*')
										->where('users.id',$event_data->event_user_id)
										->get();
					$from_user1_data = DB::table('users')->where('users.id',$event->event_user_id)->first();
					if($to_user1_data && $from_user1_data){
						foreach($to_user1_data as $to_user){
							$title = "It's a match";
							$body = 'You got a match with '.$from_user1_data->firstName.' '.$from_user1_data->lastName;
							$noti_response = sendNotification($to_user->device_type,$to_user->device_token,$title,$body,['type' => 'match']);
							$notification_data =[
				        							'notification_user'		=>	$to_user->device_user_id,
				        							'notification_device'	=>	$to_user->_device_id,
				        							'notification_title'	=>	$title,
				        							'notification_content'	=>	$body,
				        							'notification_status'	=>	$noti_response['success']==1 ? 'success' : 'failed',
				        							'notification_response'	=>	json_encode($noti_response)
				        						];
				        	DB::table('notifications')->insert($notification_data);
						}
					}

					$to_user2_data = DB::table('users')
										->join('device_tokens','users.id','=','device_tokens.device_user_id')
										->select('device_tokens.*')
										->where('users.id',$event->event_user_id)
										->get();
					$from_user2_data = DB::table('users')->where('users.id',$event_data->event_user_id)->first();
					if($to_user2_data && $from_user2_data){
						foreach($to_user2_data as $to_user){
							$title = "It's a match";
							$body = 'You got a match with '.$from_user2_data->firstName.' '.$from_user2_data->lastName;
							$noti_response = sendNotification($to_user->device_type,$to_user->device_token,$title,$body,['type' => 'match']);
							$notification_data =[
				        							'notification_user'		=>	$to_user->device_user_id,
				        							'notification_device'	=>	$to_user->_device_id,
				        							'notification_title'	=>	$title,
				        							'notification_content'	=>	$body,
				        							'notification_status'	=>	$noti_response['success']==1 ? 'success' : 'failed',
				        							'notification_response'	=>	json_encode($noti_response)
				        						];
				        	DB::table('notifications')->insert($notification_data);
						}
					}
    			}

    		}
    	}
    }

    function addMasterModal(Request $request){
    	if($request->is_dependent && $request->is_dependent==1){
    		$rules =[
				        'user_id' 			=> 	'required',
				        'db_name'			=>	'required',
				        'column_name'		=>	'required',
				        'status_column'		=>	'required',
				        'dependent_name'	=>	'required',
				        'dependent_value'	=>	'required'
				    ];
    	}else{
    		$rules =[
				        'user_id' 		=> 	'required',
				        'db_name'		=>	'required',
				        'column_name'	=>	'required',
				        'status_column'	=>	'required',
				    ];
    	}

    	$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			
            $html = view('xhr.add_master_modal',[
													'db_name' 			=> $request->db_name,
													'user_id' 			=> $request->user_id,
													'column_name' 		=> $request->column_name,
													'status_column' 	=> $request->status_column,
													'dependent_name' 	=> $request->dependent_name ? $request->dependent_name : NULL,
													'dependent_value' 	=> $request->dependent_value ? $request->dependent_value : NULL,
													'is_dependent' 		=> $request->is_dependent==1 ? 1 : 0,
											])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
		}
    }

    function addMaster(Request $request){
    	if($request->is_dependent && $request->is_dependent==1){
    		//$unique =  ? '|unique:'.$request->db_name.','.$request->column_name : NULL;
    		if(!empty($request->db_name) && !empty($request->dependent_name) && !empty($request->dependent_value) && !empty($request->primary_key)){
    			$rules =[
				        'user_id' 			=> 	'required',
				        'db_name'			=>	'required',
				        'column_name'		=>	'required',
    					'column_value'		=>	'required|unique:'.$request->db_name.','.$request->column_name.',NULL,'.$request->primary_key.','.$request->dependent_name.','.$request->dependent_value,
				        'dependent_name'	=>	'required',
				        'dependent_value'	=>	'required'
				    ];
				}else{
					$rules =[
				        'user_id' 			=> 	'required',
				        'db_name'			=>	'required',
				        'column_name'		=>	'required',
				        'column_value'		=>	'required',
				        'dependent_name'	=>	'required',
				        'dependent_value'	=>	'required'
				    ];
				}
	    	
    	}else{
    		$unique = !empty($request->db_name) ? '|unique:'.$request->db_name.','.$request->column_name : NULL;
	    	$rules =[
				        'user_id' 		=> 	'required',
				        'db_name'		=>	'required',
				        'column_name'	=>	'required',
				        'column_value'	=>	'required'.$unique
				    ];
    	}
    	
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			if($request->is_dependent && $request->is_dependent==1){
				$inserted_id = DB::table($request->db_name)->insertGetId([	"$request->column_name" => 	ucwords(strtolower($request->column_value)),
														"$request->dependent_name" => $request->dependent_value
												]);
			}else{
				$inserted_id = DB::table($request->db_name)->insertGetId([	"$request->column_name" => 	ucwords(strtolower($request->column_value))
												]);
			}

			if($request->primary_key){
				$inserted_data = DB::table($request->db_name)->where("$request->primary_key",$inserted_id)->first();
			}else{
				$inserted_data = [];
			}
			if($request->is_dependent && $request->is_dependent==1){
				$db_name = DB::table($request->db_name)->where("$request->dependent_name", $request->dependent_value)->orderBy($request->column_name,'asc')->get();
			}else{
				$db_name = DB::table($request->db_name)->orderBy($request->column_name,'asc')->get();
			}
			
			if(in_array($request->db_name,['species','breeds'])){
				$species = DB::table('species')->orderBy('species_name', 'asc')->get();
				$types = [];
				foreach ($species as $key => $value) {
					$breeds = DB::table('breeds')->where('breed_species_id',$value->_species_id)->orderBy('breed_name', 'asc')->get();
					$value->breeds = $breeds;
					array_push($types, $value);
				}
				$db_name = $types;
				$request->db_name = 'species';
			}else if(in_array($request->db_name,['items','item_sizes'])){
				$items = DB::table('items')->orderBy('item_name', 'asc')->get();
				$types = [];
				foreach ($items as $key => $value) {
					$item_sizes = DB::table('item_sizes')->where('is_item_id',$value->_item_id)->orderBy('is_name', 'asc')->get();
					$value->item_sizes = $item_sizes;
					array_push($types, $value);
				}
				$db_name = $types;
				$request->db_name = 'items';
			}
			else if($request->db_name=='military_ranks'){

				$military_branches = DB::table('military_branches')->orderBy('mbranch_name', 'asc')->get();
				$types = [];
				foreach ($military_branches as $key => $value) {
					$military_ranks = DB::table('military_ranks')->where('rank_branch_id',$value->_mbranch_id)->orderBy('rank_name', 'asc')->get();
					$value->military_ranks = $military_ranks;
					array_push($types, $value);
				}
				$db_name = $types;
				$request->db_name = 'military_ranks';
			}else if(in_array($request->db_name,['vehicle_types','vehicle_makers'])){
				$vehicle_types = DB::table('vehicle_types')->orderBy('vehicle_type_name', 'asc')->get();
				$types = [];
				foreach ($vehicle_types as $key => $value) {
					$makers = DB::table('vehicle_makers')->where('maker_vehicle_type',$value->id)->orderBy('maker_name', 'asc')->get();
					$value->makers = $makers;
					array_push($types, $value);
				}
				$db_name = $types;
			}else if(in_array($request->db_name,['games','game_teams'])){
				$games = DB::table('games')->orderBy('game_name', 'asc')->get();
				$types = [];
				foreach ($games as $key => $value) {
					$game_teams = DB::table('game_teams')->where('team_gameid',$value->_game_id)->orderBy('team_name', 'asc')->get();
					$value->game_teams = $game_teams;
					array_push($types, $value);
				}
				$db_name = $types;
			}
			
			$evaluate = "execute('api/get-master','user_id=".$request->user_id."&db_name=".$request->db_name."&column_name=".$request->column_name."&status_column=".$request->status_column."&dependent_name=".$request->dependent_name."&dependent_value=".$request->dependent_value."&is_dependent=".$request->is_dependent."&inserted_id=".$inserted_id."');";
			
			return response([
            				'response_code'	=>	'200',
                     		'response_msg'	=> 	$request->column_value.' added successfully',
                     		'reponse_body' 	=> 	["$request->db_name"  => $db_name, 'inserted_id' => $inserted_id, 'inserted_data'=> $inserted_data],
                     		'evaluate'		=>	$evaluate,
                     		'close'			=>	TRUE
                    	], 200);
		}
    }

    function getMasterData(Request $request){
    	if($request->is_dependent && $request->is_dependent==1){
    		$rules =[
				        'user_id' 			=> 	'required',
				        'db_name'			=>	'required',
				        'column_name'		=>	'required',
				        'status_column'		=>	'required',
				        'dependent_name'	=>	'required',
				        'dependent_value'	=>	'required'
				    ];
    	}else{
    		$rules =[
				        'user_id' 		=> 	'required',
				        'db_name'		=>	'required',
				        'column_name'	=>	'required',
				        'status_column'	=>	'required',
				    ];
    	}

    	$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			if($request->is_dependent && $request->is_dependent==1){
				$db_name = DB::table($request->db_name)->where("$request->dependent_name", $request->dependent_value)->where("$request->status_column",1)->orderBy($request->column_name,'asc')->get();
				$get_master = view('xhr.get_master',[
													'db_name' 			=> 	$request->db_name,
													"$request->db_name" => 	$db_name,
													'user_id' 			=> 	$request->user_id,
													'column_name' 		=> 	$request->column_name,
													'status_column' 	=> 	$request->status_column,
													'dependent_name' 	=> 	$request->dependent_name,
													'dependent_value' 	=> 	$request->dependent_value,
													'is_dependent' 		=> 	$request->is_dependent,
													'inserted_id'		=>	$request->inserted_id ? $request->inserted_id : 0
											])->render();
			}else{
				if($request->db_name=='games'){
					$games = DB::table('games')->orderBy('game_name', 'asc')->get();
					
					$types = [];
					foreach ($games as $key => $value) {
						$game_teams = DB::table('game_teams')->where('team_gameid',$value->_game_id)->orderBy('team_name', 'asc')->get();
						$value->game_teams = $game_teams;
						array_push($types, $value);
					}

					$db_name = $types;

					$get_master = view('xhr.get_master',[
													'db_name' 			=> 	$request->db_name,
													"$request->db_name" => 	$types,
													'user_id' 			=> 	$request->user_id,
													'column_name' 		=> 	$request->column_name,
													'status_column' 	=> 	$request->status_column,
													'dependent_name' 	=> 	$request->dependent_name ? $request->dependent_name : NULL,
													'dependent_value' 	=> 	$request->dependent_value ? $request->dependent_value : NULL,
													'is_dependent' 		=> 	$request->is_dependent==1 ? 1 : 0,
													'inserted_id'		=>	$request->inserted_id ? $request->inserted_id : 0
											])->render();
				}else if($request->db_name=='vehicle_types'){

					$vehicle_types = DB::table('vehicle_types')->where('vehicle_type_is_active',1)->orderBy('vehicle_type_name', 'asc')->get();
					$types = [];
					foreach ($vehicle_types as $key => $value) {
						$makers = DB::table('vehicle_makers')->where('maker_vehicle_type',$value->id)->where('maker_is_active',1)->orderBy('maker_name', 'asc')->get();
						$value->makers = $makers;
						array_push($types, $value);
					}

					//$response['vehicle_types'] = $types;

					$db_name = $types;

					$get_master = view('xhr.get_master',[
													'db_name' 			=> 	$request->db_name,
													"$request->db_name" => 	$types,
													'user_id' 			=> 	$request->user_id,
													'column_name' 		=> 	$request->column_name,
													'status_column' 	=> 	$request->status_column,
													'dependent_name' 	=> 	$request->dependent_name ? $request->dependent_name : NULL,
													'dependent_value' 	=> 	$request->dependent_value ? $request->dependent_value : NULL,
													'is_dependent' 		=> 	$request->is_dependent==1 ? 1 : 0,
													'inserted_id'		=>	$request->inserted_id ? $request->inserted_id : 0
											])->render();
				}
				else{
					$db_name = DB::table($request->db_name)->where("$request->status_column",1)->orderBy($request->column_name,'asc')->get();
					$get_master = view('xhr.get_master',[
													'db_name' 			=> 	$request->db_name,
													"$request->db_name" => 	$db_name,
													'user_id' 			=> 	$request->user_id,
													'column_name' 		=> 	$request->column_name,
													'status_column' 	=> 	$request->status_column,
													'dependent_name' 	=> 	$request->dependent_name ? $request->dependent_name : NULL,
													'dependent_value' 	=> 	$request->dependent_value ? $request->dependent_value : NULL,
													'is_dependent' 		=> 	$request->is_dependent==1 ? 1 : 0,
													'inserted_id'		=>	$request->inserted_id ? $request->inserted_id : 0
											])->render();
				}
				
			}

			return response([
            				'response_code'	=>	'200',
                     		'response_msg'	=> 	$request->db_name.' fetched successfully',
                     		'reponse_body' 	=> 	["$request->db_name"  => $db_name,'html_id'=>$request->db_name.'_div','html'=>$get_master]
                    	], 200);
		}
    }

    function editLifeEvent(Request $request){
    	if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
        if(!$request->event || is_nan($request->event)){
        	return redirect('home');
        }

        $event_data = DB::table('life_events')->selectRaw("life_events.*, (SELECT GROUP_CONCAT(DISTINCT userkeys) FROM keywords WHERE keywords.keyid IN(SELECT DISTINCT lek_key_id FROM life_event_keywords WHERE lek_event_id=life_events._event_id )) AS event_keywords")->where('_event_id', $request->event)->where('event_user_id', $request->session()->get('userdata')['id'])->first();

        //print_r($event_data);exit;

        if(!$event_data){
        	return redirect('home');
        }

        $events = DB::table('life_events')->where('event_user_id', $request->session()->get('userdata')['id'])->where('event_is_draft',0)->get();
        $drafts = DB::table('life_events')->where('event_user_id', $request->session()->get('userdata')['id'])->where('event_is_draft',1)->get();

        $where_data = DB::table('life_event_where')->where('where_event_id', $event_data->_event_id)->first();
        $when_data  = DB::table('life_event_when')->where('when_event_id', $event_data->_event_id)->first();
        $images = DB::table('life_event_images')->where('image_event_id', $event_data->_event_id)->get();

        $response = [];
        $response['event_count'] = count($events) > 0 ? count($events) : 0;
        $response['draft_count'] = count($drafts) > 0 ? count($drafts) : 0;
        $response['event_data'] = $event_data;
        $response['where_data'] = $where_data;
        $response['when_data'] = $when_data;
        $response['images'] = $images;
        $response['countries'] = DB::table('countries')->where('country_is_active',1)->orderBy('country_name', 'asc')->get();
        $provinces = [];
        $cities = [];
        if($where_data){
        	if($where_data->where_country){
        		$provinces = DB::table('provinces')->join('countries', 'provinces.province_country', '=', 'countries._country_id')->where('countries.country_name',$where_data->where_country)->get();
        	}
        	if($where_data->where_state){
        		$cities = DB::table('cities')->join('provinces', 'cities.city_province', '=', 'provinces._province_id')->where('provinces.province_name',$where_data->where_state)->get();
        	}
        }
        $response['provinces'] = $provinces;
        $response['cities'] = $cities;
        if($event_data->event_type_id==1){

    		$response['what'] = DB::table('life_event_sub_categories')->where('sub_cat_event_type_id', 1)->get();
			$response['home_types'] = DB::table('home_types')->where('home_type_is_active',1)->orderBy('home_type_name', 'asc')->get();
			$response['home_styles'] = DB::table('home_styles')->where('home_style_is_active',1)->orderBy('home_style_name', 'asc')->get();
			$vehicle_types = DB::table('vehicle_types')->where('vehicle_type_is_active',1)->orderBy('vehicle_type_name', 'asc')->get();
			$response['vehicle_colors'] = DB::table('vehicle_color')->where('color_is_active',1)->orderBy('color_name', 'asc')->get();

			$types = [];
			foreach ($vehicle_types as $key => $value) {
				$makers = DB::table('vehicle_makers')->where('maker_vehicle_type',$value->id)->where('maker_is_active',1)->orderBy('maker_name', 'asc')->get();
				$value->makers = $makers;
				array_push($types, $value);
			}

			$response['vehicle_types'] = $types;

			$makers = [];

			if($event_data->event_vehicle_type){
				$makers = DB::table('vehicle_makers')->select('vehicle_makers.*')->join('vehicle_types', 'vehicle_makers.maker_vehicle_type', '=', 'vehicle_types.id')->where('vehicle_types.vehicle_type_name',$event_data->event_vehicle_type)->where('maker_is_active',1)->orderBy('maker_name', 'asc')->get();
			}

			$response['vehicle_makers'] = $makers;

			$vehicle_model = [];
			if($event_data->event_vehicle_maker){
				$vehicle_model = DB::table('vehicle_model')
									->join('vehicle_makers', 'vehicle_model.model_maker_id', '=', 'vehicle_makers._maker_id')
									->where('maker_name', $event_data->event_vehicle_maker)
									->where('model_is_active', 1)
									->get();
			}

			$response['vehicle_model'] = $vehicle_model;

			$response['genders'] = DB::table('genders')->where('gender_is_active',1)->orderBy('gender_name', 'asc')->get();
			$response['relations'] = DB::table('relations')->where('relation_is_active',1)->orderBy('relation_name', 'asc')->get();
			$response['marital_status'] = DB::table('marital_status')->where('ms_is_active',1)->orderBy('ms_name', 'asc')->get();
			$response['occupations'] = DB::table('occupations')->where('occupation_is_active',1)->orderBy('occupation_name', 'asc')->get();
			$response['industries'] = DB::table('industries')->where('industry_is_active',1)->orderBy('industry_name', 'asc')->get();

    		return View::make("life_events.edit.personal")->with($response);
    	}else if($event_data->event_type_id==2){

    		if(!$request->session()->get('userdata')['user_age'] || $request->session()->get('userdata')['user_age']<18){
    			return redirect('create-lifeevent');
    		}

    		$response['what'] = DB::table('life_event_sub_categories')->where('sub_cat_event_type_id', 2)->get();

    		$response['genders'] = DB::table('genders')->where('gender_is_active',1)->orderBy('gender_name', 'asc')->get();
			$response['ethnicities'] = DB::table('ethnicities')->orderBy('ethnicity_name', 'asc')->get();
			$response['races'] = DB::table('races')->orderBy('race_name', 'asc')->get();
			$response['nationalities'] = DB::table('countries')->where('country_is_active',1)->orderBy('country_name', 'asc')->get();
			$response['orientations'] = DB::table('orientations')->orderBy('_orientation_id', 'asc')->get();
			$response['body_styles'] = DB::table('body_styles')->orderBy('bs_name', 'asc')->get();
			$response['skin_tonnes'] = DB::table('skin_tonnes')->orderBy('st_name', 'asc')->get();
			$response['hair_colors'] = DB::table('hair_colors')->orderBy('hc_name', 'asc')->get();
			$response['eye_colors'] = DB::table('eye_colors')->orderBy('ec_name', 'asc')->get();
			$response['politicals'] = DB::table('politicals')->orderBy('political_name', 'asc')->get();
			$response['religions'] = DB::table('religions')->orderBy('religion_name', 'asc')->get();
			$response['age_ranges'] = DB::table('age_ranges')->orderBy('range_name', 'asc')->get();
			$response['interests'] = DB::table('interests')->orderBy('interest_name', 'asc')->get();

			$response['availabilities'] = DB::table('availabilities')->orderBy('availability_name', 'asc')->get();
			$response['occupations'] = DB::table('occupations')->where('occupation_is_active',1)->orderBy('occupation_name', 'asc')->get();
					
			$response['facial_hairs'] = DB::table('facial_hairs')->orderBy('fh_name', 'asc')->get();
			$response['body_hairs'] = DB::table('body_hairs')->orderBy('bh_name', 'asc')->get();
			$response['tattoos'] = DB::table('tattoos')->orderBy('tattoo_name', 'asc')->get();
			$response['foods'] = DB::table('foods')->orderBy('food_name', 'asc')->get();
			$response['food_types'] = DB::table('food_types')->orderBy('ft_name', 'asc')->get();
			$response['sports'] = DB::table('sports')->orderBy('sport_name', 'asc')->get();
			$response['zodiac_signs'] = DB::table('zodiac_signs')->orderBy('zodiac_name', 'asc')->get();
			$response['financials'] = DB::table('financial_status')->orderBy('fs_name', 'asc')->get();
			$response['relationships'] = ["Boyfriend/Girlfriend","Husband/Wife"];
			$response['affiliations'] = DB::table('affiliations')->orderBy('affiliation_name', 'asc')->get();
			$response['weight_general'] = DB::table('weight_general')->orderBy('wg_name', 'asc')->get();
			$response['height_general'] = DB::table('height_general')->orderBy('hg_name', 'asc')->get();
			$response['radius'] = DB::table('radius')/*->orderBy('hg_name', 'asc')*/->get();
			return View::make("life_events.edit.dating")->with($response);
    	}else if($event_data->event_type_id==3){
    		$response['what'] = DB::table('life_event_sub_categories')->where('sub_cat_event_type_id', 3)->get();
    		$response['adoption_status'] = DB::table('adoption_status')->orderBy('status_name', 'asc')->get();
			$response['place_of_birth'] = DB::table('place_of_birth')->orderBy('place_name', 'asc')->get();
			$response['genders'] = DB::table('genders')->where('gender_is_active',1)->orderBy('gender_name', 'asc')->get();
			$birth_provinces = [];
	        $birth_cities = [];
	        if($where_data){
	        	if($where_data->where_birth_country){
	        		$birth_provinces = DB::table('provinces')->join('countries', 'provinces.province_country', '=', 'countries._country_id')->where('countries.country_name',$where_data->where_birth_country)->get();
	        	}
	        	if($where_data->where_birth_state){
	        		$birth_cities = DB::table('cities')->join('provinces', 'cities.city_province', '=', 'provinces._province_id')->where('provinces.province_name',$where_data->where_birth_state)->get();
	        	}
	        }
	        $response['birth_provinces'] = $birth_provinces;
	        $response['birth_cities'] = $birth_cities;
			return View::make("life_events.edit.adoption")->with($response);
    	}else if($event_data->event_type_id==4){
    		$response['what'] = DB::table('life_event_sub_categories')->where('sub_cat_event_type_id', 4)->get();
    		$response['location_types'] = DB::table('location_types')->orderBy('lt_name', 'asc')->get();
    		$response['travel_modes'] = DB::table('travel_modes')->where('mode_is_active',1)->orderBy('mode_name', 'asc')->get();
    		return View::make("life_events.edit.travel")->with($response);
    	}else if($event_data->event_type_id==5){
    		$response['what'] = DB::table('life_event_sub_categories')->where('sub_cat_event_type_id', 5)->get();
    		$response['genders'] = DB::table('genders')->where('gender_is_active',1)->orderBy('gender_name', 'asc')->get();
			$response['military_bases'] = DB::table('military_bases')->orderBy('base_name', 'asc')->get();
			$military_branches = DB::table('military_branches')->orderBy('mbranch_name', 'asc')->get();
			$response['military_units'] = DB::table('military_units')->orderBy('unit_name', 'asc')->get();
			$response['military_mos'] = DB::table('military_mos')->orderBy('mos_name', 'asc')->get();
			$response['military_mos_titles'] = DB::table('military_mos_titles')->orderBy('mtitle_name', 'asc')->get();
			$types = [];
			foreach ($military_branches as $key => $value) {
				$military_ranks = DB::table('military_ranks')->where('rank_branch_id',$value->_mbranch_id)->orderBy('rank_name', 'asc')->get();
				$value->military_ranks = $military_ranks;
				array_push($types, $value);
			}
			$response['military_branches'] = $types;
			// $response['age_ranges'] = DB::table('age_ranges')->orderBy('range_name', 'asc')->get();
			$response['age_ranges'] = DB::table('military_age_range')->orderBy('m_range_name', 'asc')->get();
			$military_ranks = [];
			if($event_data->event_military_branch){
				$military_ranks = DB::table('military_ranks')->join('military_branches','military_ranks.rank_branch_id', '=', 'military_branches._mbranch_id')->where('mbranch_name',$event_data->event_military_branch)->orderBy('rank_name', 'asc')->get();
			}

			$response['military_ranks'] = $military_ranks;

			return View::make("life_events.edit.military")->with($response);
    	}else if($event_data->event_type_id==6){
    		$response['what'] = DB::table('life_event_sub_categories')->where('sub_cat_event_type_id', 6)->get();
    		$response['subjects'] 		= DB::table('subjects')->orderBy('subject_name', 'asc')->get();
			$response['curriculars'] 	= DB::table('curriculars')->orderBy('curricular_name', 'asc')->get();
			$response['organizations'] = DB::table('organizations')->orderBy('organization_name', 'asc')->get();
			$response['events'] 		= DB::table('events')->orderBy('event_name', 'asc')->get();
			$response['after_schools'] = DB::table('after_schools')->orderBy('as_name', 'asc')->get();
			return View::make("life_events.edit.education")->with($response);
    	}else if($event_data->event_type_id==7){
    		$response['what'] = DB::table('life_event_sub_categories')->where('sub_cat_event_type_id', 7)->get();
    		$response['industries'] = DB::table('industries')->where('industry_is_active',1)->orderBy('industry_name', 'asc')->get();
			$response['occupations'] = DB::table('occupations')->where('occupation_is_active',1)->orderBy('occupation_name', 'asc')->get();
			$response['career_levels'] = DB::table('career_levels')->orderBy('cl_name', 'asc')->get();
			return View::make("life_events.edit.work")->with($response);
    	}else if($event_data->event_type_id==8){
    		$species = DB::table('species')->orderBy('species_name', 'asc')->get();
			$response['pet_colors'] = DB::table('pet_colors')->orderBy('pc_name', 'asc')->get();
			$response['pet_sizes'] = DB::table('pet_sizes')->orderBy('_ps_id', 'desc')->get();
			$response['pet_tempers'] = DB::table('pet_tempers')->orderBy('pt_name', 'asc')->get();
			$response['genders'] = ["Male","Female","Unknown"];
			
			$types = [];
			foreach ($species as $key => $value) {
				$breeds = DB::table('breeds')->where('breed_species_id',$value->_species_id)->orderBy('breed_name', 'asc')->get();
				$value->breeds = $breeds;
				array_push($types, $value);
			}
			$response['species'] = $types;
			$breeds = [];
			if($event_data->event_pet_species){
				$breeds = DB::table('breeds')->join('species', 'breeds.breed_species_id', '=', 'species._species_id')->where('species_name',$event_data->event_pet_species)->orderBy('breed_name', 'asc')->get();
			}
			$response['breeds'] = $breeds;

			return View::make("life_events.edit.pets")->with($response);
    	}else if($event_data->event_type_id==9){
    		$items = DB::table('items')->orderBy('item_name', 'asc')->get();
			$response['indoors'] = DB::table('indoors')->orderBy('indoor_name', 'asc')->get();
			$response['inmotions'] = DB::table('inmotions')->orderBy('inmotion_name', 'asc')->get();
			$response['materials'] = DB::table('materials')->orderBy('material_name', 'asc')->get();
			$response['outdoors'] = DB::table('outdoors')->orderBy('outdoor_name', 'asc')->get();
			$response['others'] = [];
			
			$types = [];
			foreach ($items as $key => $value) {
				$item_sizes = DB::table('item_sizes')->where('is_item_id',$value->_item_id)->orderBy('is_name', 'asc')->get();
				$value->item_sizes = $item_sizes;
				array_push($types, $value);
			}
			$response['items'] = $types;
			$item_sizes = [];
			if($event_data->event_item_name){
				$item_sizes = DB::table('item_sizes')->join('items','item_sizes.is_item_id','=','items._item_id')->where('item_name',$event_data->event_item_name)->orderBy('is_name', 'asc')->get();
			}
			$response['item_sizes'] = $item_sizes;
			return View::make("life_events.edit.lost_and_found")->with($response);
    	}else if($event_data->event_type_id==11){
    		$games = DB::table('games')->orderBy('game_name', 'asc')->get();
					
			$types = [];
			foreach ($games as $key => $value) {
				$game_teams = DB::table('game_teams')->where('team_gameid',$value->_game_id)->orderBy('team_name', 'asc')->get();
				$value->game_teams = $game_teams;
				array_push($types, $value);
			}
			$response['games'] = $types;
			$game_teams = [];
			if($event_data->event_game){
				$game_teams = DB::table('game_teams')->join('games', 'game_teams.team_gameid', '=', 'games._game_id')->where('games.game_name', $event_data->event_game)->orderBy('team_name', 'asc')->get();
			}
			$response['game_teams'] = $game_teams;
    		return View::make("life_events.edit.username_connect")->with($response);
    	}else{
    		return redirect('home');
    	}
    }

    function createLifeEvent(Request $request){
    	if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
        $response = [];
        $response['countries'] = DB::table('countries')->where('country_is_active',1)->orderBy('country_name', 'asc')->get();
    	if(!$request->type || $request->type==1){

    		$response['what'] = DB::table('life_event_sub_categories')->where('sub_cat_event_type_id', 1)->get();
			$response['home_types'] = DB::table('home_types')->where('home_type_is_active',1)->orderBy('home_type_name', 'asc')->get();
			$response['home_styles'] = DB::table('home_styles')->where('home_style_is_active',1)->orderBy('home_style_name', 'asc')->get();
			$vehicle_types = DB::table('vehicle_types')->where('vehicle_type_is_active',1)->orderBy('vehicle_type_name', 'asc')->get();
			$response['vehicle_colors'] = DB::table('vehicle_color')->where('color_is_active',1)->orderBy('color_name', 'asc')->get();

			$types = [];
			foreach ($vehicle_types as $key => $value) {
				$makers = DB::table('vehicle_makers')->where('maker_vehicle_type',$value->id)->where('maker_is_active',1)->orderBy('maker_name', 'asc')->get();
				$value->makers = $makers;
				array_push($types, $value);
			}

			$response['vehicle_types'] = $types;

			$response['genders'] = DB::table('genders')->where('gender_is_active',1)->orderBy('gender_name', 'asc')->get();
			$response['relations'] = DB::table('relations')->where('relation_is_active',1)->orderBy('relation_name', 'asc')->get();
			$response['marital_status'] = DB::table('marital_status')->where('ms_is_active',1)->orderBy('ms_name', 'asc')->get();
			$response['occupations'] = DB::table('occupations')->where('occupation_is_active',1)->orderBy('occupation_name', 'asc')->get();
			$response['industries'] = DB::table('industries')->where('industry_is_active',1)->orderBy('industry_name', 'asc')->get();

    		return View::make("life_events.personal")->with($response);
    	}else if($request->type==2){

    		if(!$request->session()->get('userdata')['user_age'] || $request->session()->get('userdata')['user_age']<18){
    			return redirect('create-lifeevent');
    		}

    		$response['what'] = DB::table('life_event_sub_categories')->where('sub_cat_event_type_id', 2)->get();

    		$response['genders'] = DB::table('genders')->where('gender_is_active',1)->orderBy('gender_name', 'asc')->get();
			$response['ethnicities'] = DB::table('ethnicities')->orderBy('ethnicity_name', 'asc')->get();
			$response['races'] = DB::table('races')->orderBy('race_name', 'asc')->get();
			$response['nationalities'] = DB::table('countries')->where('country_is_active',1)->orderBy('country_name', 'asc')->get();
			$response['orientations'] = DB::table('orientations')->orderBy('_orientation_id', 'asc')->get();
			$response['body_styles'] = DB::table('body_styles')->orderBy('bs_name', 'asc')->get();
			$response['skin_tonnes'] = DB::table('skin_tonnes')->orderBy('st_name', 'asc')->get();
			$response['hair_colors'] = DB::table('hair_colors')->orderBy('hc_name', 'asc')->get();
			$response['eye_colors'] = DB::table('eye_colors')->orderBy('ec_name', 'asc')->get();
			$response['politicals'] = DB::table('politicals')->orderBy('political_name', 'asc')->get();
			$response['religions'] = DB::table('religions')->orderBy('religion_name', 'asc')->get();
			$response['age_ranges'] = DB::table('age_ranges')->orderBy('range_name', 'asc')->get();
			$response['interests'] = DB::table('interests')->orderBy('interest_name', 'asc')->get();

			$response['availabilities'] = DB::table('availabilities')->orderBy('availability_name', 'asc')->get();
			$response['occupations'] = DB::table('occupations')->where('occupation_is_active',1)->orderBy('occupation_name', 'asc')->get();
					
			$response['facial_hairs'] = DB::table('facial_hairs')->orderBy('fh_name', 'asc')->get();
			$response['body_hairs'] = DB::table('body_hairs')->orderBy('bh_name', 'asc')->get();
			$response['tattoos'] = DB::table('tattoos')->orderBy('tattoo_name', 'asc')->get();
			$response['foods'] = DB::table('foods')->orderBy('food_name', 'asc')->get();
			$response['food_types'] = DB::table('food_types')->orderBy('ft_name', 'asc')->get();
			$response['sports'] = DB::table('sports')->orderBy('sport_name', 'asc')->get();
			$response['zodiac_signs'] = DB::table('zodiac_signs')->orderBy('zodiac_name', 'asc')->get();
			$response['financials'] = DB::table('financial_status')->orderBy('fs_name', 'asc')->get();
			$response['relationships'] = ["Boyfriend/Girlfriend","Husband/Wife"];
			$response['affiliations'] = DB::table('affiliations')->orderBy('affiliation_name', 'asc')->get();
			$response['weight_general'] = DB::table('weight_general')->orderBy('wg_name', 'asc')->get();
			$response['height_general'] = DB::table('height_general')->orderBy('hg_name', 'asc')->get();
			$response['radius'] = DB::table('radius')/*->orderBy('hg_name', 'asc')*/->get();
			return View::make("life_events.dating")->with($response);
    	}else if($request->type==3){
    		$response['what'] = DB::table('life_event_sub_categories')->where('sub_cat_event_type_id', 3)->get();
    		$response['adoption_status'] = DB::table('adoption_status')->orderBy('status_name', 'asc')->get();
			$response['place_of_birth'] = DB::table('place_of_birth')->orderBy('place_name', 'asc')->get();
			$response['genders'] = DB::table('genders')->where('gender_is_active',1)->orderBy('gender_name', 'asc')->get();
			return View::make("life_events.adoption")->with($response);
    	}else if($request->type==4){
    		$response['what'] = DB::table('life_event_sub_categories')->where('sub_cat_event_type_id', 4)->get();
    		$response['location_types'] = DB::table('location_types')->orderBy('lt_name', 'asc')->get();
    		$response['travel_modes'] = DB::table('travel_modes')->where('mode_is_active',1)->orderBy('mode_name', 'asc')->get();
    		return View::make("life_events.travel")->with($response);
    	}else if($request->type==5){
    		$response['what'] = DB::table('life_event_sub_categories')->where('sub_cat_event_type_id', 5)->get();
    		$response['genders'] = DB::table('genders')->where('gender_is_active',1)->orderBy('gender_name', 'asc')->get();
			$response['military_bases'] = DB::table('military_bases')->orderBy('base_name', 'asc')->get();
			$military_branches = DB::table('military_branches')->orderBy('mbranch_name', 'asc')->get();
			$response['military_units'] = DB::table('military_units')->orderBy('unit_name', 'asc')->get();
			$response['military_mos'] = DB::table('military_mos')->orderBy('mos_name', 'asc')->get();
			$response['military_mos_titles'] = DB::table('military_mos_titles')->orderBy('mtitle_name', 'asc')->get();
			$types = [];
			foreach ($military_branches as $key => $value) {
				$military_ranks = DB::table('military_ranks')->where('rank_branch_id',$value->_mbranch_id)->orderBy('rank_name', 'asc')->get();
				$value->military_ranks = $military_ranks;
				array_push($types, $value);
			}
			$response['military_branches'] = $types;
			// $response['age_ranges'] = DB::table('age_ranges')->orderBy('range_name', 'asc')->get();
			$response['age_ranges'] = DB::table('military_age_range')->orderBy('m_range_name', 'asc')->get();

			return View::make("life_events.military")->with($response);
    	}else if($request->type==6){
    		$response['what'] = DB::table('life_event_sub_categories')->where('sub_cat_event_type_id', 6)->get();
    		$response['subjects'] 		= DB::table('subjects')->orderBy('subject_name', 'asc')->get();
			$response['curriculars'] 	= DB::table('curriculars')->orderBy('curricular_name', 'asc')->get();
			$response['organizations'] = DB::table('organizations')->orderBy('organization_name', 'asc')->get();
			$response['events'] 		= DB::table('events')->orderBy('event_name', 'asc')->get();
			$response['after_schools'] = DB::table('after_schools')->orderBy('as_name', 'asc')->get();
			return View::make("life_events.education")->with($response);
    	}else if($request->type==7){
    		$response['what'] = DB::table('life_event_sub_categories')->where('sub_cat_event_type_id', 7)->get();
    		$response['industries'] = DB::table('industries')->where('industry_is_active',1)->orderBy('industry_name', 'asc')->get();
			$response['occupations'] = DB::table('occupations')->where('occupation_is_active',1)->orderBy('occupation_name', 'asc')->get();
			$response['career_levels'] = DB::table('career_levels')->orderBy('cl_name', 'asc')->get();
			return View::make("life_events.work")->with($response);
    	}else if($request->type==8){
    		$species = DB::table('species')->orderBy('species_name', 'asc')->get();
			$response['pet_colors'] = DB::table('pet_colors')->orderBy('pc_name', 'asc')->get();
			$response['pet_sizes'] = DB::table('pet_sizes')->orderBy('_ps_id', 'desc')->get();
			$response['pet_tempers'] = DB::table('pet_tempers')->orderBy('pt_name', 'asc')->get();
			$response['genders'] = ["Male","Female","Unknown"];
			
			$types = [];
			foreach ($species as $key => $value) {
				$breeds = DB::table('breeds')->where('breed_species_id',$value->_species_id)->orderBy('breed_name', 'asc')->get();
				$value->breeds = $breeds;
				array_push($types, $value);
			}
			$response['species'] = $types;

			return View::make("life_events.pets")->with($response);
    	}else if($request->type==9){
    		$items = DB::table('items')->orderBy('item_name', 'asc')->get();
			$response['indoors'] = DB::table('indoors')->orderBy('indoor_name', 'asc')->get();
			$response['inmotions'] = DB::table('inmotions')->orderBy('inmotion_name', 'asc')->get();
			$response['materials'] = DB::table('materials')->orderBy('material_name', 'asc')->get();
			$response['outdoors'] = DB::table('outdoors')->orderBy('outdoor_name', 'asc')->get();
			$response['others'] = [];
			
			$types = [];
			foreach ($items as $key => $value) {
				$item_sizes = DB::table('item_sizes')->where('is_item_id',$value->_item_id)->orderBy('is_name', 'asc')->get();
				$value->item_sizes = $item_sizes;
				array_push($types, $value);
			}
			$response['items'] = $types;
			return View::make("life_events.lost_and_found")->with($response);
    	}else if($request->type==11){
    		$games = DB::table('games')->orderBy('game_name', 'asc')->get();
					
			$types = [];
			foreach ($games as $key => $value) {
				$game_teams = DB::table('game_teams')->where('team_gameid',$value->_game_id)->orderBy('team_name', 'asc')->get();
				$value->game_teams = $game_teams;
				array_push($types, $value);
			}
			$response['games'] = $types;
    		return View::make("life_events.username_connect")->with($response);
    	}else if($request->type==12){
    		$life_event_sub_categories = DB::table('life_event_sub_categories')->where('sub_cat_event_type_id',12)->get();
					
			$activity_type = [];
			foreach ($life_event_sub_categories as $key => $value) {
				$activity_types = DB::table('activity_types')->where('act_type_event_id',$value->id)->orderBy('act_type_name', 'asc')->get();
				$value->activity_types = $activity_types;
				array_push($activity_type, $value);
			}
			$response['life_event_sub_categories'] = $activity_type;

    		$games = DB::table('games')->orderBy('game_name', 'asc')->get();
					
			$types = [];
			foreach ($games as $key => $value) {
				$game_teams = DB::table('game_teams')->where('team_gameid',$value->_game_id)->orderBy('team_name', 'asc')->get();
				$value->game_teams = $game_teams;
				array_push($types, $value);
			}
			$response['games'] = $types;
			$response['location_types'] = DB::table('location_types')->orderBy('lt_name', 'asc')->get();
    		return View::make("life_events.activities")->with($response);
    	}else{
    		return redirect('home');
    	}

    }

    function TagSearch(Request $request){
    	$rules =[
    		'user_id' 		=> 	'required',
    		'tag_number'	=>	'required',
    		'country'		=>	'nullable',
    		'state'			=>	'nullable'
    	];

		/*if(!$request->email && !$request->phone){
			$rules['email'] = 'required';
			$rules['phone'] = 'required';
		}*/
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$user_data = User::find($request->user_id);
			$where = "WHERE l.event_is_draft=0 AND u.id != $request->user_id AND u.id NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id='".$request->user_id."') AND event_tag_number LIKE '%".$request->tag_number."%'";
			if($request->country){
				$where .=" AND where_country='".$request->country."'";
			}
			if($request->state){
				$where .=" AND where_state='".$request->state."'";
			}
			$select = "SELECT u.firstName, u.lastName, u.profile_photo_path, u.id, u.username, u.quick_blox_id, 
						(SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status =1 AND ((request_from_user=$request->user_id AND request_to_user=u.id) OR (request_from_user=u.id AND request_to_user=$request->user_id))), 1, 0)) AS is_connected,
                    	(SELECT IF ((SELECT COUNT(*) FROM user_follows WHERE follow_user=$request->user_id AND follow_following=u.id) , 1, 0)) AS is_following, 
                    	l._event_id, l.event_type_id, l.event_user_id, l.event_tag_number, l.event_vehicle_type, l.event_vehicle_maker, l.event_vehicle_model, l.event_vehicle_year, l.event_vehicle_color, l.event_is_draft, lew.where_country AS event_country, lew.where_state AS event_state, lew.where_city AS event_city
					FROM life_events l 
					INNER JOIN users u ON u.id=l.event_user_id
					INNER JOIN life_event_where lew ON l._event_id=lew.where_event_id
					$where ORDER BY u.firstName ASC";

			$events = DB::select($select);

			$feed_div = '';
			//if($request->search_from && $request->search_from=='web'){
			$feed_div = view('xhr.search_tag',['events' => $events, 'user_id' => $request->user_id, 'user_quick_blox_id' => $user_data->quick_blox_id, 'tag_number' => $request->tag_number, 'country'=> $request->country, 'state' => $request->state])->render();
			//}
            return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'Feeds fetched successfully',
                  				'reponse_body' 	=> 	['events' => $events,'html_id' => 'tag_search_result', 'html' => $feed_div]
                  			], 200);
		}
    }

    function lifeEventsList(Request $request){
    	$rules =[
    		'user_id' 		=> 	'required',
    		'is_draft'		=>	'required|in:0,1'
    	];

		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{

			$drafts = DB::table('life_events')
        			->join('life_event_categories','life_events.event_type_id','=','life_event_categories.id')
        			->leftJoin('life_event_sub_categories', 'life_events.event_subcategory_id', '=', 'life_event_sub_categories.id')
        			/*->leftJoin('life_event_where', 'life_events._event_id', '=', 'life_event_where.where_event_id')
        			->leftJoin('life_event_when', 'life_events._event_id', '=', 'life_event_when.when_event_id')*/
        			/*->select('life_events.*', 'life_event_categories.event_type', 'life_event_sub_categories.sub_cat_type_name')*/
        			->selectRaw("life_events.*, life_event_categories.event_type, life_event_sub_categories.sub_cat_type_name, (SELECT GROUP_CONCAT(DISTINCT userkeys) FROM keywords 
WHERE keywords.keyid IN(SELECT DISTINCT lek_key_id FROM life_event_keywords WHERE lek_event_id=life_events._event_id )) AS event_keywords, (SELECT GROUP_CONCAT(DISTINCT feed_user2) FROM feeds WHERE feed_eventid=life_events._event_id AND feed_type='matched' AND feed_user2 NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id=$request->user_id)) AS match_users, (SELECT COUNT(_feed_id) FROM feeds WHERE feed_eventid=life_events._event_id AND feed_type='matched' AND feed_user2 NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id=$request->user_id)) AS match_count")
        			->where('event_user_id', '=', $request->user_id)
        			->where('event_is_draft', '=', $request->is_draft)
        			->orderBy('life_events._event_id', 'desc')
        			->get();
        	$new_drafts = [];
	        if($drafts){
	        	foreach($drafts as $key => $draft){
	        		$images = DB::table('life_event_images')->where('image_event_id',$draft->_event_id)->get();
	        		$when = DB::table('life_event_when')->where('when_event_id',$draft->_event_id)->first();
	        		$where = DB::table('life_event_where')->where('where_event_id',$draft->_event_id)->first();
	        		$new_drafts[$key] = (object)[];
	        		$new_drafts[$key]->_event_id = $draft->_event_id;
	        		$new_drafts[$key]->event_type_id = $draft->event_type_id;
	        		$new_drafts[$key]->event_subcategory_id = $draft->event_subcategory_id;
	        		$new_drafts[$key]->event_type = $draft->event_type;
	        		$new_drafts[$key]->sub_cat_type_name = $draft->sub_cat_type_name;
	        		$new_drafts[$key]->event_is_draft = $draft->event_is_draft;
	        		$new_drafts[$key]->event_keywords = $draft->event_keywords;
	        		$new_drafts[$key]->match_count = $draft->match_count;
	        		$new_drafts[$key]->match_users = $draft->match_users;
	        		$new_drafts[$key]->what = $draft;
	        		$new_drafts[$key]->when = $when ? (object)$when : (object)[];
	        		$new_drafts[$key]->where = $where ? (object)$where : (object)[];
	        		$new_drafts[$key]->images = count($images) > 0 ? (object)$images : [];
	        		$users = [];
	        		if($draft->match_count > 0){
	        			$match_users = $draft->match_users;
	        			$users = DB::select("SELECT id,firstName, lastName, email, quick_blox_id, profile_photo_path, isd_code, phone, username FROM users WHERE id IN(".$match_users.")");
	        		}
	        		$new_drafts[$key]->users = count($users) > 0 ? $users : [];
	        		$new_drafts[$key]->match_count = count($users);
	        	}
	        }

	        return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'Life Events fetched successfully',
                  				'reponse_body' 	=> 	['events' => $new_drafts]
                  			], 200);
		}
    }

    function lifeEvents(Request $request){
    	if (!$request->session()->has('userdata')) {
            return redirect('login');
        }

        $user_id = $request->session()->get('userdata')['id'];

        $drafts = DB::table('life_events')
        			->join('life_event_categories','life_events.event_type_id','=','life_event_categories.id')
        			->leftJoin('life_event_sub_categories', 'life_events.event_subcategory_id', '=', 'life_event_sub_categories.id')
        			->leftJoin('life_event_where', 'life_events._event_id', '=', 'life_event_where.where_event_id')
        			->leftJoin('life_event_when', 'life_events._event_id', '=', 'life_event_when.when_event_id')
        			->selectRaw("life_events.*, life_event_categories.event_type, life_event_sub_categories.sub_cat_type_name, life_event_where.*, life_event_when.*, (SELECT GROUP_CONCAT(DISTINCT userkeys) FROM keywords 
WHERE keywords.keyid IN(SELECT DISTINCT lek_key_id FROM life_event_keywords WHERE lek_event_id=life_events._event_id )) AS event_keywords, life_events.created_at AS event_create_time, 
	(SELECT GROUP_CONCAT(DISTINCT feed_user2) FROM feeds WHERE (feed_eventid=life_events._event_id OR feed_matched_eventid=life_events._event_id) AND feed_type='matched' AND feed_user2 NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id=$user_id) AND feed_user2 !=$user_id)AS match_users1,
	(SELECT GROUP_CONCAT(DISTINCT feed_user1) FROM feeds WHERE (feed_eventid=life_events._event_id OR feed_matched_eventid=life_events._event_id) AND feed_type='matched' AND feed_user1 NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id=$user_id) AND feed_user1 !=$user_id)AS match_users2,
	(SELECT CONCAT_WS(',',match_users1,match_users2)) AS match_users,
	 (SELECT COUNT(_feed_id) FROM feeds WHERE (feed_eventid=life_events._event_id OR feed_matched_eventid=life_events._event_id) AND feed_type='matched' AND feed_user2 NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id=$user_id)) AS match_count")
        			->where('event_user_id', '=', $user_id)
        			->where('event_is_draft', '=', 0)
        			->orderBy('life_events._event_id', 'desc')
        			->get();

        $new_drafts = [];
        if($drafts){
        	foreach($drafts as $draft){
        		$images = DB::table('life_event_images')->where('image_event_id',$draft->_event_id)->get();
        		$draft->images = count($images) > 0 ? $images : [];
        		$users = [];
        		if($draft->match_count > 0 && !empty($draft->match_users)){
        			$match_users = implode(',', array_unique(explode(',', $draft->match_users)));
        			$users = DB::select("SELECT id,firstName, lastName, email, quick_blox_id, profile_photo_path, isd_code, phone, username FROM users WHERE id IN(".$match_users.")");
        		}
        		$draft->users = count($users) > 0 ? $users : [];
        		$draft->match_count = count($users);
        		array_push($new_drafts,$draft);
        	}
        }
        //echo '<pre>';print_r($new_drafts);exit;

        $draft = DB::table('life_events')->where('event_user_id', '=', $user_id)->where('event_is_draft', '=', 1)->get();
        $life_event = DB::table('life_events')->where('event_user_id', '=', $user_id)->where('event_is_draft', '=', 0)->get();

        return View::make("drafts")->with(['drafts' => $new_drafts,'draft_count' => count($draft),'life_event_count' => count($life_event)]);
    }

    function drafts(Request $request){
    	if (!$request->session()->has('userdata')) {
            return redirect('login');
        }

        $user_id = $request->session()->get('userdata')['id'];

        $drafts = DB::table('life_events')
        			->join('life_event_categories','life_events.event_type_id','=','life_event_categories.id')
        			->leftJoin('life_event_sub_categories', 'life_events.event_subcategory_id', '=', 'life_event_sub_categories.id')
        			->leftJoin('life_event_where', 'life_events._event_id', '=', 'life_event_where.where_event_id')
        			->leftJoin('life_event_when', 'life_events._event_id', '=', 'life_event_when.when_event_id')
        			->selectRaw("life_events.*, life_event_categories.event_type, life_event_sub_categories.sub_cat_type_name, life_event_where.*, life_event_when.*, (SELECT GROUP_CONCAT(DISTINCT userkeys) FROM keywords 
WHERE keywords.keyid IN(SELECT DISTINCT lek_key_id FROM life_event_keywords WHERE lek_event_id=life_events._event_id )) AS event_keywords, life_events.created_at AS event_create_time, (SELECT GROUP_CONCAT(DISTINCT feed_user2) FROM feeds WHERE (feed_eventid=life_events._event_id OR feed_matched_eventid=life_events._event_id) AND feed_type='matched' AND feed_user2 NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id=$user_id) AND feed_user2 !=$user_id)AS match_users1,
	(SELECT GROUP_CONCAT(DISTINCT feed_user1) FROM feeds WHERE (feed_eventid=life_events._event_id OR feed_matched_eventid=life_events._event_id) AND feed_type='matched' AND feed_user1 NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id=$user_id) AND feed_user1 !=$user_id)AS match_users2,
	(SELECT CONCAT_WS(',',match_users1,match_users2)) AS match_users,
	 (SELECT COUNT(_feed_id) FROM feeds WHERE (feed_eventid=life_events._event_id OR feed_matched_eventid=life_events._event_id) AND feed_type='matched' AND feed_user2 NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id=$user_id)) AS match_count")
        			->where('event_user_id', '=', $user_id)
        			->where('event_is_draft', '=', 1)
        			->orderBy('life_events._event_id', 'desc')
        			->get();

        $new_drafts = [];
        if($drafts){
        	foreach($drafts as $draft){
        		$images = DB::table('life_event_images')->where('image_event_id',$draft->_event_id)->get();
        		$draft->images = count($images) > 0 ? $images : [];
        		$users = [];
        		if($draft->match_count > 0 && !empty($draft->match_users)){
        			//$match_users = $draft->match_users;
        			$match_users = implode(',', array_unique(explode(',', $draft->match_users)));
        			$users = DB::select("SELECT id,firstName, lastName, email, quick_blox_id, profile_photo_path, isd_code, phone, username FROM users WHERE id IN(".$match_users.")");
        		}
        		$draft->users = count($users) > 0 ? $users : [];
        		array_push($new_drafts,$draft);
        	}
        }
       // echo '<pre>';print_r($new_drafts);exit;
        $life_event = DB::table('life_events')->where('event_user_id', '=', $user_id)->where('event_is_draft', '=', 0)->get();

        return View::make("drafts")->with(['drafts' => $new_drafts,'draft_count' => count($new_drafts),'life_event_count' => count($life_event)]);
    }

    function deleteLifeEvent(Request $request){
    	$rules =[
    		'user_id' 		=> 	'required',
    		'_event_id'		=>	'required'
    	];

		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$user_id = $request->user_id;
			$event_data = DB::table('life_events')->where('_event_id',$request->_event_id)->where('event_user_id', $request->user_id)->first();
			if(!$event_data){
				return response([
			        'response_code'=>'201',
			        'response_msg'=> 'Invalid data supplied',
			        'reponse_body' => "null"
			    ], 401);
			}

			DB::table('life_events')->where('_event_id',$event_data->_event_id)->delete();
            DB::table('life_event_images')->where('image_event_id',$event_data->_event_id)->delete();
            DB::table('life_event_when')->where('when_event_id',$event_data->_event_id)->delete();
            DB::table('life_event_where')->where('where_event_id',$event_data->_event_id)->delete();
            DB::table('feeds')->where('feed_eventid',$event_data->_event_id)->delete();
            $res = $event_data->event_is_draft==1 ? 'Draft' : 'Life Event';
            $redirect = $event_data->event_is_draft==1 ? url('drafts') : url('life-events');

            $drafts = DB::table('life_events')
        			->join('life_event_categories','life_events.event_type_id','=','life_event_categories.id')
        			->leftJoin('life_event_sub_categories', 'life_events.event_subcategory_id', '=', 'life_event_sub_categories.id')
        			/*->select('life_events.*', 'life_event_categories.event_type', 'life_event_sub_categories.sub_cat_type_name')*/
        			->selectRaw("life_events.*, life_event_categories.event_type, life_event_sub_categories.sub_cat_type_name, (SELECT GROUP_CONCAT(DISTINCT userkeys) FROM keywords 
WHERE keywords.keyid IN(SELECT DISTINCT lek_key_id FROM life_event_keywords WHERE lek_event_id=life_events._event_id )) AS event_keywords, (SELECT GROUP_CONCAT(DISTINCT feed_user2) FROM feeds WHERE (feed_eventid=life_events._event_id OR feed_matched_eventid=life_events._event_id) AND feed_type='matched' AND feed_user2 NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id=$user_id) AND feed_user2 !=$user_id)AS match_users1,
	(SELECT GROUP_CONCAT(DISTINCT feed_user1) FROM feeds WHERE (feed_eventid=life_events._event_id OR feed_matched_eventid=life_events._event_id) AND feed_type='matched' AND feed_user1 NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id=$user_id) AND feed_user1 !=$user_id)AS match_users2,
	(SELECT CONCAT_WS(',',match_users1,match_users2)) AS match_users,
	 (SELECT COUNT(_feed_id) FROM feeds WHERE (feed_eventid=life_events._event_id OR feed_matched_eventid=life_events._event_id) AND feed_type='matched' AND feed_user2 NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id=$user_id)) AS match_count")
        			->where('event_user_id', '=', $request->user_id)
        			->where('event_is_draft', '=', $event_data->event_is_draft)
        			->orderBy('life_events._event_id', 'desc')
        			->get();
        	$new_drafts = [];
	        if($drafts){
	        	foreach($drafts as $key => $draft){
	        		$images = DB::table('life_event_images')->where('image_event_id',$draft->_event_id)->get();
	        		$when = DB::table('life_event_when')->where('when_event_id',$draft->_event_id)->first();
	        		$where = DB::table('life_event_where')->where('where_event_id',$draft->_event_id)->first();
	        		$new_drafts[$key] = (object)[];
	        		$new_drafts[$key]->_event_id = $draft->_event_id;
	        		$new_drafts[$key]->event_type_id = $draft->event_type_id;
	        		$new_drafts[$key]->event_subcategory_id = $draft->event_subcategory_id;
	        		$new_drafts[$key]->event_type = $draft->event_type;
	        		$new_drafts[$key]->sub_cat_type_name = $draft->sub_cat_type_name;
	        		$new_drafts[$key]->event_is_draft = $draft->event_is_draft;
	        		$new_drafts[$key]->event_keywords = $draft->event_keywords;
	        		//$new_drafts[$key]->match_count = 0;
	        		$new_drafts[$key]->match_users = $draft->match_users;
	        		$new_drafts[$key]->what = $draft;
	        		$new_drafts[$key]->when = $when ? (object)$when : (object)[];
	        		$new_drafts[$key]->where = $where ? (object)$where : (object)[];
	        		$new_drafts[$key]->images = count($images) > 0 ? (object)$images : [];
	        		$users = [];
	        		if($draft->match_count > 0 && !empty($draft->match_users)){
	        			//$match_users = $draft->match_users;
	        			$match_users = implode(',', array_unique(explode(',', $draft->match_users)));
	        			$users = DB::select("SELECT id,firstName, lastName, email, quick_blox_id, profile_photo_path, isd_code, phone, username FROM users WHERE id IN(".$match_users.")");
	        			$new_drafts[$key]->match_users = $match_users;
	        		}
	        		$new_drafts[$key]->users = count($users) > 0 ? $users : [];
	        		$new_drafts[$key]->match_count = count($users);
	        	}
	        }
            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>   $res.' Deleted Successfully',
                                'reponse_body'  =>  ['events' => $new_drafts],
                                'redirect'      =>  $redirect
                            ],200);
		}
    }

    function unpublishLifeEvent(Request $request){
    	$rules =[
    		'user_id' 		=> 	'required',
    		'_event_id'		=>	'required'
    	];

		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$user_id = $request->user_id;
			$event_data = DB::table('life_events')->where('_event_id',$request->_event_id)->where('event_user_id', $request->user_id)->where('event_is_draft','=',0)->first();
			if(!$event_data){
				return response([
			        'response_code'=>'201',
			        'response_msg'=> 'Invalid data supplied',
			        'reponse_body' => "null"
			    ], 401);
			}
			DB::table('life_events')->where('_event_id',$event_data->_event_id)->update(['event_is_draft' => 1]);
			DB::table('feeds')->where('feed_eventid',$event_data->_event_id)->delete();
            $res = $event_data->event_is_draft==1 ? 'Draft' : 'Life Event';
            $redirect = $event_data->event_is_draft==1 ? url('drafts') : url('life-events');

            $drafts = DB::table('life_events')
        			->join('life_event_categories','life_events.event_type_id','=','life_event_categories.id')
        			->leftJoin('life_event_sub_categories', 'life_events.event_subcategory_id', '=', 'life_event_sub_categories.id')
        			->selectRaw("life_events.*, life_event_categories.event_type, life_event_sub_categories.sub_cat_type_name, (SELECT GROUP_CONCAT(DISTINCT userkeys) FROM keywords 
WHERE keywords.keyid IN(SELECT DISTINCT lek_key_id FROM life_event_keywords WHERE lek_event_id=life_events._event_id )) AS event_keywords, (SELECT GROUP_CONCAT(DISTINCT feed_user2) FROM feeds WHERE (feed_eventid=life_events._event_id OR feed_matched_eventid=life_events._event_id) AND feed_type='matched' AND feed_user2 NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id=$user_id) AND feed_user2 !=$user_id)AS match_users1,
	(SELECT GROUP_CONCAT(DISTINCT feed_user1) FROM feeds WHERE (feed_eventid=life_events._event_id OR feed_matched_eventid=life_events._event_id) AND feed_type='matched' AND feed_user1 NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id=$user_id) AND feed_user1 !=$user_id)AS match_users2,
	(SELECT CONCAT_WS(',',match_users1,match_users2)) AS match_users,
	 (SELECT COUNT(_feed_id) FROM feeds WHERE (feed_eventid=life_events._event_id OR feed_matched_eventid=life_events._event_id) AND feed_type='matched' AND feed_user2 NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id=$user_id)) AS match_count")
        			->where('event_user_id', '=', $request->user_id)
        			->where('event_is_draft', '=', $event_data->event_is_draft)
        			->orderBy('life_events._event_id', 'desc')
        			->get();
        	$new_drafts = [];
	        if($drafts){
	        	foreach($drafts as $key => $draft){
	        		$images = DB::table('life_event_images')->where('image_event_id',$draft->_event_id)->get();
	        		$when = DB::table('life_event_when')->where('when_event_id',$draft->_event_id)->first();
	        		$where = DB::table('life_event_where')->where('where_event_id',$draft->_event_id)->first();
	        		$new_drafts[$key] = (object)[];
	        		$new_drafts[$key]->_event_id = $draft->_event_id;
	        		$new_drafts[$key]->event_type_id = $draft->event_type_id;
	        		$new_drafts[$key]->event_subcategory_id = $draft->event_subcategory_id;
	        		$new_drafts[$key]->event_type = $draft->event_type;
	        		$new_drafts[$key]->sub_cat_type_name = $draft->sub_cat_type_name;
	        		$new_drafts[$key]->event_is_draft = $draft->event_is_draft;
	        		$new_drafts[$key]->event_keywords = $draft->event_keywords;
	        		//$new_drafts[$key]->match_count = 0;
	        		$new_drafts[$key]->match_users = $draft->match_users;
	        		$new_drafts[$key]->what = $draft;
	        		$new_drafts[$key]->when = $when ? (object)$when : (object)[];
	        		$new_drafts[$key]->where = $where ? (object)$where : (object)[];
	        		$new_drafts[$key]->images = count($images) > 0 ? (object)$images : [];
	        		$users = [];
	        		if($draft->match_count > 0 && !empty($draft->match_users)){
	        			//$match_users = $draft->match_users;
	        			$match_users = implode(',', array_unique(explode(',', $draft->match_users)));
	        			$users = DB::select("SELECT id,firstName, lastName, email, quick_blox_id, profile_photo_path, isd_code, phone, username FROM users WHERE id IN(".$match_users.")");
	        			$new_drafts[$key]->match_users = $match_users;
	        		}
	        		$new_drafts[$key]->users = count($users) > 0 ? $users : [];
	        		$new_drafts[$key]->match_count = count($users);
	        	}
	        }
            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>   $res.' Unpublished Successfully',
                                'reponse_body'  =>  ['events' => $new_drafts],
                                'redirect'      =>  $redirect
                            ],200);

		}
    }

    function updateLifeEvent(Request $request){
    	$rules =[
			        'type_id' 			=> 	'required',
			        'user_id'			=>	'required',
			        '_event_id'			=>	'required'
			    ];
		ini_set('post_max_size', '64M');
		ini_set('upload_max_filesize', '64M');
		if(!in_array($request->type_id,[5,7,8,9,11])){
			$rules['sub_category_id'] = 'required';
		}
		if($request->type_id==1){
			if($request->sub_category_id==1){
				$rules['type_of_home'] = 'nullable';
				$rules['home_style'] = 'nullable';
				$rules['country'] = 'nullable';
				$rules['state'] = 'nullable';
				$rules['city'] = 'nullable';
				$rules['street'] = 'nullable';
				$rules['zip'] = 'nullable';
				$rules['when_from_month'] = 'nullable';
				$rules['when_from_day'] = 'nullable';
				$rules['when_from_year'] = 'nullable';
				$rules['when_to_month'] = 'nullable';
				$rules['when_to_day'] = 'nullable';
				$rules['when_to_year'] = 'nullable';
				$rules['email'] = 'nullable';
				$rules['phone'] = 'nullable';

			}else if($request->sub_category_id==2){
				$rules['tag_number'] = 'required';
				$rules['type_of_vehicle'] = 'nullable';
				$rules['vehicle_maker'] = 'nullable';
				$rules['vehicle_model'] = 'nullable';
				$rules['year_manufactured'] = 'nullable';
				$rules['vehicle_color'] = 'nullable';
				$rules['country'] = 'nullable';
				$rules['state'] = 'nullable';
				$rules['city'] = 'nullable';
				$rules['street'] = 'nullable';
				$rules['zip'] = 'nullable';
				$rules['when_from_month'] = 'nullable';
				$rules['when_from_day'] = 'nullable';
				$rules['when_from_year'] = 'nullable';
				$rules['when_to_month'] = 'nullable';
				$rules['when_to_day'] = 'nullable';
				$rules['when_to_year'] = 'nullable';
				$rules['email'] = 'nullable';
				$rules['phone'] = 'nullable';
			}else if($request->sub_category_id==3){
				$rules['first_name'] = 'required';
				$rules['last_name'] = 'required';
				$rules['age'] = 'nullable';
				$rules['gender'] = 'nullable';
				$rules['relationship'] = 'nullable';
				$rules['marital_status'] = 'nullable';
				$rules['industry'] = 'nullable';
				$rules['category'] = 'nullable';
				$rules['country'] = 'nullable';
				$rules['state'] = 'nullable';
				$rules['city'] = 'nullable';
				$rules['street'] = 'nullable';
				$rules['zip'] = 'nullable';
				$rules['email'] = 'nullable';
				$rules['phone'] = 'nullable';

				$rules['when_family_birth_month'] = 'nullable';
				$rules['when_family_birth_day'] = 'nullable';
				$rules['when_family_birth_year'] = 'nullable';

				$rules['when_family_wedding_month'] = 'nullable';
				$rules['when_family_wedding_day'] = 'nullable';
				$rules['when_family_wedding_year'] = 'nullable';

				$rules['when_family_passed_month'] = 'nullable';
				$rules['when_family_passed_day'] = 'nullable';
				$rules['when_family_passed_year'] = 'nullable';

			}
		}else if($request->type_id==2 && in_array($request->sub_category_id,[4,5])){
			$rules['gender'] = 'required';
			$rules['age'] = 'nullable';
			$rules['ethnicity'] = 'nullable';
			$rules['race'] = 'nullable';
			$rules['nationality'] = 'nullable';
			$rules['orientation'] = 'nullable';
			$rules['body_style'] = 'nullable';
			$rules['skin_tonne'] = 'nullable';
			$rules['hair_color'] = 'nullable';
			$rules['eye_color'] = 'nullable';
			$rules['political'] = 'nullable';
			$rules['religion'] = 'nullable';
			$rules['looking_for'] = 'nullable';
			$rules['age_range'] = 'nullable';
			$rules['interests'] = 'nullable';

			if($request->sub_category_id==5){
				$rules['height'] = 'nullable';
				$rules['height_measure'] = 'nullable';
				$rules['height_general'] = 'nullable';
				$rules['weight'] = 'nullable';
				$rules['weight_measure'] = 'nullable';
				$rules['weight_general'] = 'nullable';
				$rules['facial_hair'] = 'nullable';
				$rules['body_hair'] = 'nullable';
				$rules['glasses'] = 'nullable';
				$rules['party'] = 'nullable';
				$rules['smoke'] = 'nullable';
				$rules['drink'] = 'nullable';
				$rules['tattoo'] = 'nullable';
				$rules['tattoo_type'] = 'nullable';
				$rules['fav_food'] = 'nullable';
				$rules['food_type'] = 'nullable';
				$rules['sport'] = 'nullable';
				$rules['fav_team'] = 'nullable';
				$rules['zodiac_sign'] = 'nullable';
				$rules['birth_stone'] = 'nullable';
				$rules['affiliation'] = 'nullable';
				$rules['career'] = 'nullable';
				$rules['financial'] = 'nullable';
				$rules['relationship'] = 'nullable';
			}

			$rules['country'] = 'nullable';
			$rules['state'] = 'nullable';
			$rules['city'] = 'nullable';
			$rules['radius'] = 'nullable';

			$rules['timeframe'] = 'nullable';
			$rules['time_month'] = 'nullable';
				
		}else if($request->type_id==3 /*&& in_array($request->sub_category_id,[6,7,8])*/){
			
			$rules['birth_country'] = 'nullable';
			$rules['birth_state'] = 'nullable';
			$rules['birth_city'] = 'nullable';
			$rules['birth_street'] = 'nullable';
			$rules['birth_zip'] = 'nullable';

			$rules['country'] = 'nullable';
			$rules['state'] = 'nullable';
			$rules['city'] = 'nullable';
			$rules['street'] = 'nullable';
			$rules['zip'] = 'nullable';

			$rules['birth_name'] = 'nullable';
			$rules['bio_mother'] = 'nullable';
			$rules['bio_father'] = 'nullable';
			$rules['weight'] = 'nullable';
			$rules['weight_measure'] = 'nullable';
			$rules['height'] = 'nullable';
			$rules['height_measure'] = 'nullable';
			$rules['birth_place'] = 'nullable';
			$rules['adoption_status'] = 'nullable';
			$rules['birth_date'] = 'nullable';
			
		}else if($request->type_id==4 /*&& in_array($request->sub_category_id,[9,10,11,12])*/){

			$rules['airport'] = 'nullable';
			$rules['airline'] = 'nullable';
			$rules['flight_number'] = 'nullable';
			$rules['row'] = 'nullable';
			$rules['seat'] = 'nullable';
			$rules['location_type'] = 'nullable';
			$rules['location_name'] = 'required';

			$rules['country'] = 'nullable';
			$rules['state'] = 'nullable';
			$rules['city'] = 'nullable';
			$rules['street'] = 'nullable';
			$rules['zip'] = 'nullable';

			$rules['when_from_month'] = 'nullable';
			$rules['when_from_day'] = 'nullable';
			$rules['when_from_year'] = 'nullable';
			$rules['when_to_month'] = 'nullable';
			$rules['when_to_day'] = 'nullable';
			$rules['when_to_year'] = 'nullable';
			$rules['dep_time'] = 'nullable';
			$rules['dep_time_ampm'] = 'nullable';
			$rules['arr_time'] = 'nullable';
			$rules['arr_time_ampm'] = 'nullable';
		}else if($request->type_id==5){

			$rules['military_branch'] = 'required';
			$rules['gender'] = 'nullable';
			$rules['age'] = 'nullable';
			$rules['military_unit1'] = 'nullable';
			$rules['military_unit2'] = 'nullable';
			$rules['military_unit3'] = 'nullable';
			$rules['military_unit4'] = 'nullable';
			$rules['military_unit5'] = 'nullable';
			$rules['military_unit6'] = 'nullable';
			$rules['military_rank'] = 'nullable';
			$rules['military_mos'] = 'nullable';
			$rules['military_title'] = 'nullable';

			$rules['country'] = 'required';
			$rules['state'] = 'nullable';
			$rules['city'] = 'nullable';
			$rules['street'] = 'nullable';
			$rules['zip'] = 'nullable';
			$rules['military_base'] = 'nullable';
			$rules['military_order'] = 'nullable';

			$rules['when_from_month'] = 'nullable';
			$rules['when_from_day'] = 'nullable';
			$rules['when_from_year'] = 'nullable';
			$rules['when_to_month'] = 'nullable';
			$rules['when_to_day'] = 'nullable';
			$rules['when_to_year'] = 'nullable';
		}else if($request->type_id==6 /*&& in_array($request->sub_category_id,[16,17,18,19,20,21,22,23,24,25])*/){

			$rules['school_name'] = 'required';
			$rules['school_sorority'] = 'nullable';
			$rules['school_teacher'] = 'nullable';
			$rules['school_roomno'] = 'nullable';
			$rules['school_subject'] = 'nullable';
			$rules['school_curriculars'] = 'nullable';
			$rules['school_organization'] = 'nullable';
			$rules['school_event'] = 'nullable';
			$rules['school_afterschool'] = 'nullable';

			$rules['country'] = 'nullable';
			$rules['state'] = 'nullable';
			$rules['city'] = 'nullable';
			$rules['street'] = 'nullable';
			$rules['zip'] = 'nullable';

			$rules['when_from_month'] = 'nullable';
			$rules['when_from_day'] = 'nullable';
			$rules['when_from_year'] = 'nullable';
			$rules['when_to_month'] = 'nullable';
			$rules['when_to_day'] = 'nullable';
			$rules['when_to_year'] = 'nullable';
			
		}else if($request->type_id==7){
			$rules['career_name'] = 'required';
			$rules['career_category'] = 'nullable';
			$rules['career_title'] = 'nullable';
			$rules['career_level'] = 'nullable';

			$rules['country'] = 'nullable';
			$rules['state'] = 'nullable';
			$rules['city'] = 'nullable';
			$rules['street'] = 'nullable';
			$rules['zip'] = 'nullable';

			$rules['when_from_month'] = 'nullable';
			$rules['when_from_day'] = 'nullable';
			$rules['when_from_year'] = 'nullable';
			$rules['when_to_month'] = 'nullable';
			$rules['when_to_day'] = 'nullable';
			$rules['when_to_year'] = 'nullable';
			
		}else if($request->type_id==8){
			$required = (isset($request->search_for) && $request->search_for==1) ? 'required' : 'nullable';
			$rules['pet_name'] = 'nullable';
			$rules['pet_breed'] = $required;
			$rules['pet_species'] = $required;
			$rules['gender'] = $required;
			$rules['pet_temperment'] = 'nullable';
			$rules['pet_color'] = $required;
			$rules['pet_size'] = 'nullable';
			$rules['pet_license_number'] = 'nullable';
			$rules['pet_collar'] = 'nullable';

			$rules['pet_veterinary'] = 'nullable';
			$rules['pet_medical'] = 'nullable';

			$rules['country'] = 'nullable';
			$rules['state'] = 'nullable';
			$rules['city'] = 'nullable';
			$rules['street'] = 'nullable';
			$rules['zip'] = 'nullable';

		}else if($request->type_id==9){
			$rules['item_name'] = 'required';
			$rules['item_size'] = 'nullable';
			$rules['item_material'] = 'nullable';
			$rules['outdoor'] = 'nullable';
			$rules['indoor'] = 'nullable';
			$rules['inmotion'] = 'nullable';
			$rules['other'] = 'nullable';

			$rules['country'] = 'nullable';
			$rules['state'] = 'nullable';
			$rules['city'] = 'nullable';
			$rules['street'] = 'nullable';
			$rules['zip'] = 'nullable';

			$rules['when_from_month'] = 'nullable';
			$rules['when_from_day'] = 'nullable';
			$rules['when_from_year'] = 'nullable';
			$rules['when_to_month'] = 'nullable';
			$rules['when_to_day'] = 'nullable';
			$rules['when_to_year'] = 'nullable';
			
		}else if($request->type_id==11){
			$rules['game_name'] = 'required';
			$rules['team_name'] = 'required';
			$rules['username'] = 'required';
			
			$rules['when_from_month'] = 'nullable';
			$rules['when_from_day'] = 'nullable';
			$rules['when_from_year'] = 'nullable';
		}else if($request->type_id==12){
			$rules['game_name'] = 'required';
			$rules['team_name'] = 'required';
			$rules['activity_type'] = 'required';
			$rules['location_type'] = 'required';
			$rules['location_name'] = 'required';
			$rules['entertainer'] = 'nullable';
			$rules['key_person'] = 'nullable';
			
			$rules['row'] = 'nullable';
			$rules['seat'] = 'nullable';

			$rules['country'] = 'nullable';
			$rules['state'] = 'nullable';
			$rules['city'] = 'nullable';
			$rules['street'] = 'nullable';
			$rules['zip'] = 'nullable';

			$rules['when_from_month'] = 'nullable';
			$rules['when_from_day'] = 'nullable';
			$rules['when_from_year'] = 'nullable';
			$rules['when_to_month'] = 'nullable';
			$rules['when_to_day'] = 'nullable';
			$rules['when_to_year'] = 'nullable';
			$rules['time'] = 'nullable';
			$rules['time_ampm'] = 'nullable';
		}

		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$event_data = DB::table('life_events')->where('_event_id',$request->_event_id)->where('event_user_id', $request->user_id)->first();
			if(!$event_data){
				return response([
			        'response_code'=>'201',
			        'response_msg'=> 'Invalid data supplied',
			        'reponse_body' => "null"
			    ], 401);
			}
			$event_images = DB::table('life_event_images')->where('image_event_id', $event_data->_event_id)->get();
			DB::table('life_events')->where('_event_id',$event_data->_event_id)->delete();
			DB::table('feeds')->where('feed_eventid',$event_data->_event_id)->delete();

			$life_event_data=[
								'event_type_id'			=>	$request->type_id,
								'event_subcategory_id'	=>	$request->sub_category_id,
								'event_user_id'			=>	$request->user_id,
								'event_is_draft'		=>	isset($request->is_draft) && $request->is_draft==1 ? 1 : 0
							];
			$life_event_where = [];
			$life_event_when = [];

			if($request->type_id==1){
				if($request->sub_category_id==1){
					$life_event_data['event_home_type'] 	= 	$request->type_of_home;
					$life_event_data['event_home_style'] 	= 	$request->home_style;
					$life_event_when = 	[
											/*'when_from' 	=>	$request->from_date,
											'when_to'		=>	$request->to_date,*/
											'when_from_month' 	=>	$request->when_from_month,
											'when_from_day'		=>	$request->when_from_day,
											'when_from_year' 	=>	$request->when_from_year,
											'when_to_month'		=>	$request->when_to_month,
											'when_to_day' 		=>	$request->when_to_day,
											'when_to_year'		=>	$request->when_to_year,
											/*'when_time'		=>	$request->time,
											'when_time_ampm'=>	$request->time_ampm*/
										];
					$life_event_where=	[
											'where_country'	=>	$request->country,
											'where_state'	=>	$request->state,
											'where_city'	=>	$request->city,
											'where_street'	=>	$request->street,
											'where_zip'		=>	$request->zip,
											'where_email'	=>	$request->email,
											'where_phone'	=>	$request->phone,
											'where_isd'		=>	$request->isd_code
										];
				}else if($request->sub_category_id==2){
					$life_event_data['event_tag_number']	=	$request->tag_number;
					$life_event_data['event_vehicle_type']	=	$request->type_of_vehicle;
					$life_event_data['event_vehicle_maker']	=	$request->vehicle_maker;
					$life_event_data['event_vehicle_model']	=	$request->vehicle_model;
					$life_event_data['event_vehicle_year']	=	$request->year_manufactured;
					$life_event_data['event_vehicle_color']	=	$request->vehicle_color;

					$life_event_when = 	[
											/*'when_from' 	=>	$request->from_date,
											'when_to'		=>	$request->to_date,*/
											'when_from_month' 	=>	$request->when_from_month,
											'when_from_day'		=>	$request->when_from_day,
											'when_from_year' 	=>	$request->when_from_year,
											'when_to_month'		=>	$request->when_to_month,
											'when_to_day' 		=>	$request->when_to_day,
											'when_to_year'		=>	$request->when_to_year,
											/*'when_time'		=>	$request->time,
											'when_time_ampm'=>	$request->time_ampm*/
										];
					$life_event_where=	[
											'where_country'	=>	$request->country,
											'where_state'	=>	$request->state,
											'where_city'	=>	$request->city,
											'where_street'	=>	$request->street,
											'where_zip'		=>	$request->zip,
											'where_email'	=>	$request->email,
											'where_phone'	=>	$request->phone,
											'where_isd'		=>	$request->isd_code
										];
				}else if($request->sub_category_id==3){
					$life_event_data['event_family_firstname']	=	$request->first_name;
					$life_event_data['event_family_lastname']	=	$request->last_name;
					$life_event_data['event_age']	=	$request->age;
					$life_event_data['event_gender']	=	$request->gender;
					$life_event_data['event_family_relation']	=	$request->relationship;
					$life_event_data['event_family_marital_status']	=	$request->marital_status;
					$life_event_data['event_family_industry']	=	$request->industry;
					$life_event_data['event_family_category']	=	$request->category;

					$life_event_when = 	[
											/*'when_family_birthday' 		=>	$request->birthday,
											'when_family_wedding'		=>	$request->anniversary,
											'when_family_passed'		=>	$request->passed*/
											'when_family_birth_day' 		=>	$request->when_family_birth_day,
											'when_family_birth_month'		=>	$request->when_family_birth_month,
											'when_family_birth_year'		=>	$request->when_family_birth_year,

											'when_family_wedding_day' 		=>	$request->when_family_wedding_day,
											'when_family_wedding_month'		=>	$request->when_family_wedding_month,
											'when_family_wedding_year'		=>	$request->when_family_wedding_year,

											'when_family_passed_day' 		=>	$request->when_family_passed_day,
											'when_family_passed_month'		=>	$request->when_family_passed_month,
											'when_family_passed_year'		=>	$request->when_family_passed_year
										];
					$life_event_where=	[
											'where_country'	=>	$request->country,
											'where_state'	=>	$request->state,
											'where_city'	=>	$request->city,
											'where_street'	=>	$request->street,
											'where_zip'		=>	$request->zip,
											'where_email'	=>	$request->email,
											'where_phone'	=>	$request->phone,
											'where_isd'		=>	$request->isd_code
										];
				}

			}else if($request->type_id==2 && in_array($request->sub_category_id,[4,5])){
				$life_event_data['event_age'] 					= $request->age;
				$life_event_data['event_gender'] 				= $request->gender;
				$life_event_data['event_ethnicity'] 			= $request->ethnicity;
				$life_event_data['event_race'] 					= $request->race;
				$life_event_data['event_nationality'] 			= $request->nationality;
				$life_event_data['event_orientation'] 			= $request->orientation;
				$life_event_data['event_body_style'] 			= $request->body_style;
				$life_event_data['event_skintone']				= $request->skin_tonne;
				$life_event_data['event_hair_color'] 			= $request->hair_color;
				$life_event_data['event_eye_color'] 			= $request->eye_color;
				$life_event_data['event_political'] 			= $request->political;
				$life_event_data['event_religion'] 				= $request->religion;
				$life_event_data['event_looking_for'] 			= $request->looking_for;
				$life_event_data['event_age_range'] 			= $request->age_range;
				$life_event_data['event_interest'] 				= $request->interests;
				$life_event_data['event_dating_search_for'] 	= (isset($request->search_for) && $request->search_for==1) ? 1 : 0;
				if($request->sub_category_id==5){
					$life_event_data['event_height'] 			= $request->height;
					$life_event_data['event_height_measure'] 	= $request->height_measure;
					$life_event_data['event_height_general'] 	= $request->height_general;
					$life_event_data['event_weight'] 			= $request->weight;
					$life_event_data['event_weight_measure'] 	= $request->weight_measure;
					$life_event_data['event_weight_general'] 	= $request->weight_general;
					$life_event_data['event_facial_hair'] 		= $request->facial_hair;
					$life_event_data['event_body_hair'] 		= $request->body_hair;
					$life_event_data['event_glasses'] 			= $request->glasses;
					$life_event_data['event_party'] 			= $request->party;
					$life_event_data['event_smoke'] 			= $request->smoke;
					$life_event_data['event_drink'] 			= $request->drink;
					$life_event_data['event_tattoos'] 			= $request->tattoo;
					$life_event_data['event_tattoo_type'] 		= $request->tattoo_type;
					$life_event_data['event_fav_food'] 			= $request->fav_food;
					$life_event_data['event_food_type'] 		= $request->food_type;
					$life_event_data['event_sports'] 			= $request->sport;
					$life_event_data['event_fav_team'] 			= $request->fav_team;
					$life_event_data['event_zodiac'] 			= $request->zodiac_sign;
					$life_event_data['event_birth_stone'] 		= $request->birth_stone;
					$life_event_data['event_affiliations'] 		= $request->affiliation;
					$life_event_data['event_career'] 			= $request->career;
					$life_event_data['event_financial'] 		= $request->financial;
					$life_event_data['event_family_relation'] 	= $request->relationship;
					$life_event_data['event_dating_specific'] 	= 1;
				}

				$life_event_when = 	[
										'when_timeframe' 	=>	$request->timeframe,
										'when_month'		=>	$request->time_month
									];
				$life_event_where=	[
										'where_country'	=>	$request->country,
										'where_state'	=>	$request->state,
										'where_city'	=>	$request->city,
										'where_radius'	=>	$request->radius
									];
			}else if($request->type_id==3 /*&& in_array($request->sub_category_id,[6,7,8])*/){
				$life_event_data['event_bio_mother'] = $request->bio_mother;
				$life_event_data['event_bio_father'] = $request->bio_father;
				$life_event_data['event_birthname'] = $request->birth_name;
				if(isset($request->user_is_parent) && in_array($request->user_is_parent, [1,2,3])){
					if($request->user_is_parent==1){
						$life_event_data['event_user_is_mother'] = 1;
					}elseif($request->user_is_parent==2){
						$life_event_data['event_user_is_father'] = 1;
					}else{
						$life_event_data['event_user_is_child'] = 1;
					}
				}else{
					$life_event_data['event_user_is_mother'] = isset($request->user_is_mother) && $request->user_is_mother==1 ? 1 : 0;
					$life_event_data['event_user_is_father'] = isset($request->user_is_father) && $request->user_is_father==1 ? 1 : 0;
				}
				
				$life_event_data['event_weight'] = $request->weight;
				$life_event_data['event_weight_measure'] = $request->weight_measure;
				$life_event_data['event_height'] = $request->height;
				$life_event_data['event_height_measure'] = $request->height_measure;
				$life_event_data['event_time_of_birth'] = $request->time_of_birth;
				$life_event_data['event_time_of_birth_ampm'] = $request->time_of_birth_ampm;
				$life_event_data['event_gender'] 				= 	$request->gender;

				$life_event_when = 	[
										/*'when_from' 	=>	$request->birth_date,*/
										'when_from_month' 	=>	$request->when_from_month,
										'when_from_day'		=>	$request->when_from_day,
										'when_from_year' 	=>	$request->when_from_year,
										/*'when_time'		=>	$request->time,
										'when_time_ampm'=>	$request->time_ampm*/
									];
				$life_event_where=	[
										'where_birth_place'		=>	$request->birth_place,
										'where_birth_country'	=>	$request->birth_country,
										'where_birth_state'		=>	$request->birth_state,
										'where_birth_city'		=>	$request->birth_city,
										'where_birth_street'	=>	$request->birth_street,
										'where_birth_zip'		=>	$request->birth_zip,

										'where_adoption_status'	=>	$request->adoption_status,
										'where_country'			=>	$request->country,
										'where_state'			=>	$request->state,
										'where_city'			=>	$request->city,
										'where_street'			=>	$request->street,
										'where_zip'				=>	$request->zip,
									];
			}else if($request->type_id==4 /*&& in_array($request->sub_category_id,[9,10,11,12])*/){
				$life_event_data['event_airport'] 		= $request->airport;
				$life_event_data['event_flight_number'] = $request->flight_number;
				$life_event_data['event_airline'] 		= $request->airline;
				$life_event_data['event_flight_row'] 	= $request->row;
				$life_event_data['event_flight_seat']	= $request->seat;

				$life_event_when = 	[
										/*'when_from' 			=>	$request->from_date,*/
										'when_from_month' 	=>	$request->when_from_month,
										'when_from_day'		=>	$request->when_from_day,
										'when_from_year' 	=>	$request->when_from_year,
										'when_to_month'		=>	$request->when_to_month,
										'when_to_day' 		=>	$request->when_to_day,
										'when_to_year'		=>	$request->when_to_year,
										'when_time'				=>	$request->dep_time,
										'when_time_ampm'		=>	$request->dep_time_ampm,
										/*'when_to' 				=>	$request->to_date,*/
										'when_arrival_time'		=>	$request->arr_time,
										'when_arrival_time_ampm'=>	$request->arr_time_ampm
									];
				$life_event_where=	[
										'where_location_type'	=>	$request->location_type,
										'where_hotel_name'		=>	$request->location_name,
										'where_country'			=>	$request->country,
										'where_state'			=>	$request->state,
										'where_city'			=>	$request->city,
										'where_street'			=>	$request->street,
										'where_zip'				=>	$request->zip
									];
			}else if($request->type_id==5){
				$life_event_data['event_military_branch'] 		= 	$request->military_branch;
				$life_event_data['event_gender'] 				= 	$request->gender;
				$life_event_data['event_age'] 					= 	$request->age;
				$life_event_data['event_military_unit1'] 		= 	$request->military_unit1;
				$life_event_data['event_military_unit2']		= 	$request->military_unit2;
				$life_event_data['event_military_unit3']		= 	$request->military_unit3;
				$life_event_data['event_military_unit4']		= 	$request->military_unit4;
				$life_event_data['event_military_unit5']		= 	$request->military_unit5;
				$life_event_data['event_military_unit6']		= 	$request->military_unit6;
				$life_event_data['event_military_rank']			= 	$request->military_rank;
				$life_event_data['event_military_mos']			= 	$request->military_mos;
				$life_event_data['event_military_title']		= 	$request->military_title;

				$life_event_data['event_dating_search_for'] 	= (isset($request->search_for) && $request->search_for==1) ? 1 : 0;

				$life_event_when = 	[
										/*'when_from' 			=>	$request->from_date,
										
										'when_to' 				=>	$request->to_date*/
										'when_from_month' 	=>	$request->when_from_month,
										'when_from_day'		=>	$request->when_from_day,
										'when_from_year' 	=>	$request->when_from_year,
										'when_to_month'		=>	$request->when_to_month,
										'when_to_day' 		=>	$request->when_to_day,
										'when_to_year'		=>	$request->when_to_year,
									];
				$life_event_where=	[
										'where_country'			=>	$request->country,
										'where_state'			=>	$request->state,
										'where_city'			=>	$request->city,
										'where_street'			=>	$request->street,
										'where_zip'				=>	$request->zip,
										'where_base'			=>	$request->military_base,
										'where_orders'			=>	$request->military_order
									];
			}else if($request->type_id==6 /*&& in_array($request->sub_category_id,[16,17,18,19,20,21,22,23,24,25])*/){
				$life_event_data['event_school_name'] 			= $request->school_name;
				$life_event_data['event_school_sorority'] 		= $request->school_sorority;
				$life_event_data['event_school_teacher'] 		= $request->school_teacher;
				$life_event_data['event_school_roomno'] 		= $request->school_roomno;
				$life_event_data['event_school_subject']		= $request->school_subject;
				$life_event_data['event_school_curriculars']	= $request->school_curriculars;
				$life_event_data['event_school_organization']	= $request->school_organization;
				$life_event_data['event_school_event']			= $request->school_event;
				$life_event_data['event_school_afterschool']	= $request->school_afterschool;
				$life_event_data['event_school_is_graduated'] 	= (isset($request->is_graduated) && $request->is_graduated==1) ? 1 : 0;

				$life_event_when = 	[
										/*'when_from' 			=>	$request->from_date,
										'when_to' 				=>	$request->to_date*/
										'when_from_month' 	=>	$request->when_from_month,
										'when_from_day'		=>	$request->when_from_day,
										'when_from_year' 	=>	$request->when_from_year,
										'when_to_month'		=>	$request->when_to_month,
										'when_to_day' 		=>	$request->when_to_day,
										'when_to_year'		=>	$request->when_to_year,
									];
				$life_event_where=	[
										'where_country'			=>	$request->country,
										'where_state'			=>	$request->state,
										'where_city'			=>	$request->city,
										'where_street'			=>	$request->street,
										'where_zip'				=>	$request->zip
									];
			}else if($request->type_id==7){
				$life_event_data['event_career_name'] 			= $request->career_name;
				$life_event_data['event_career_category'] 		= $request->career_category;
				$life_event_data['event_career_title'] 			= $request->career_title;
				$life_event_data['event_career_level'] 			= $request->career_level;

				$life_event_when = 	[
										/*'when_from' 			=>	$request->from_date,
										'when_to' 				=>	$request->to_date*/
										'when_from_month' 	=>	$request->when_from_month,
										'when_from_day'		=>	$request->when_from_day,
										'when_from_year' 	=>	$request->when_from_year,
										'when_to_month'		=>	$request->when_to_month,
										'when_to_day' 		=>	$request->when_to_day,
										'when_to_year'		=>	$request->when_to_year,
									];
				$life_event_where=	[
										'where_country'			=>	$request->country,
										'where_state'			=>	$request->state,
										'where_city'			=>	$request->city,
										'where_street'			=>	$request->street,
										'where_zip'				=>	$request->zip
									];
			}else if($request->type_id==8){
				$life_event_data['event_pet_name'] 			= $request->pet_name;
				$life_event_data['event_pet_breed'] 		= $request->pet_breed;
				$life_event_data['event_pet_species'] 		= $request->pet_species;
				$life_event_data['event_gender'] 			= $request->gender;
				$life_event_data['event_pet_temperment']	= $request->pet_temperment;
				$life_event_data['event_pet_color']			= $request->pet_color;
				$life_event_data['event_pet_size']			= $request->pet_size;
				$life_event_data['event_pet_license_number']= $request->pet_license_number;
				$life_event_data['event_pet_collar']		= $request->pet_collar;
				$life_event_data['event_pet_veterinary']	= $request->pet_veterinary;
				$life_event_data['event_pet_medical']		= $request->pet_medical;
				$life_event_data['event_dating_search_for'] = (isset($request->search_for) && $request->search_for==1) ? 1 : 0;

				$life_event_where=	[
										'where_country'			=>	$request->country,
										'where_state'			=>	$request->state,
										'where_city'			=>	$request->city,
										'where_street'			=>	$request->street,
										'where_zip'				=>	$request->zip
									];
			}else if($request->type_id==9){
				$life_event_data['event_item_name'] 			= $request->item_name;
				$life_event_data['event_item_size'] 			= $request->item_size;
				$life_event_data['event_item_material'] 		= $request->item_material;
				$life_event_data['event_outdoor'] 				= $request->outdoor;
				$life_event_data['event_indoor'] 				= $request->indoor;
				$life_event_data['event_inmotion'] 				= $request->inmotion;
				$life_event_data['event_other'] 				= $request->other;
				$life_event_data['event_dating_search_for'] 	= (isset($request->search_for) && $request->search_for==1) ? 1 : 0;

				$life_event_when = 	[
										/*'when_from' 			=>	$request->from_date,
										'when_to' 				=>	$request->to_date*/
										'when_from_month' 	=>	$request->when_from_month,
										'when_from_day'		=>	$request->when_from_day,
										'when_from_year' 	=>	$request->when_from_year,
										'when_to_month'		=>	$request->when_to_month,
										'when_to_day' 		=>	$request->when_to_day,
										'when_to_year'		=>	$request->when_to_year,
									];
				$life_event_where=	[
										'where_country'			=>	$request->country,
										'where_state'			=>	$request->state,
										'where_city'			=>	$request->city,
										'where_street'			=>	$request->street,
										'where_zip'				=>	$request->zip
									];
			}else if($request->type_id==11){
				$life_event_data['event_game'] 				= $request->game_name;
				$life_event_data['event_gameteam'] 			= $request->team_name;
				$life_event_data['event_username'] 			= $request->username;
				$life_event_data['event_dating_search_for'] = (isset($request->search_for) && $request->search_for==1) ? 1 : 0;

				$life_event_when = 	[
										'when_from_month' 	=>	$request->when_from_month,
										'when_from_day'		=>	$request->when_from_day,
										'when_from_year' 	=>	$request->when_from_year,
									];
			}else if($request->type_id==12){
				$life_event_data['event_game'] 				= $request->game_name;
				$life_event_data['event_gameteam'] 			= $request->team_name;

				$life_event_data['event_activity_type'] 	= $request->activity_type;
				$life_event_data['event_entertainer'] 		= $request->entertainer;
				$life_event_data['event_key_person'] 		= $request->key_person;

				$life_event_data['event_flight_row'] 		= $request->row;
				$life_event_data['event_flight_seat']		= $request->seat;

				$life_event_when = 	[
										/*'when_from' 			=>	$request->from_date,*/
										'when_from_month' 	=>	$request->when_from_month,
										'when_from_day'		=>	$request->when_from_day,
										'when_from_year' 	=>	$request->when_from_year,
										'when_to_month'		=>	$request->when_to_month,
										'when_to_day' 		=>	$request->when_to_day,
										'when_to_year'		=>	$request->when_to_year,
										'when_time'				=>	$request->dep_time,
										'when_time_ampm'		=>	$request->dep_time_ampm,
										/*'when_to' 				=>	$request->to_date,*/
										'when_arrival_time'		=>	$request->time,
										'when_arrival_time_ampm'=>	$request->time_ampm
									];
				$life_event_where=	[
										'where_location_type'	=>	$request->location_type,
										'where_hotel_name'		=>	$request->location_name,
										'where_country'			=>	$request->country,
										'where_state'			=>	$request->state,
										'where_city'			=>	$request->city,
										'where_street'			=>	$request->street,
										'where_zip'				=>	$request->zip
									];
			}

			$life_event_id = DB::table('life_events')->insertGetId($life_event_data);
			if(count($event_images)>0){
				foreach($event_images as $image){
					DB::table('life_event_images')->insert(	[
																'image_event_id' 	=> $life_event_id,
																'image_name' 		=> $image->image_name,
																'image_url'			=> $image->image_url
															]);
				}
			}

			if(!in_array($request->type_id,[8,10])){
				$life_event_when['when_event_id'] = $life_event_id;
				DB::table('life_event_when')->insert($life_event_when);
			}
			if(!in_array($request->type_id,[10,11])){
				$life_event_where['where_event_id'] = $life_event_id;
				DB::table('life_event_where')->insert($life_event_where);
			}
			
			if($request->event_pictures){

				$images = explode('@WhatscommonArnabSarbeswar',$request->event_pictures);
				if(is_array($images)){
					foreach ($images as $key => $image) {
						$image_name = 'life_events_'.time().rand(1,9999).'.png';
						$image_path = public_path('img/upload/'.$image_name);
						$image_url = url('img/upload/'.$image_name);
						$poc =  \Image::make($image)->save($image_path);
						DB::table('life_event_images')->insert(	[
																	'image_event_id' 	=> $life_event_id,
																	'image_name' 		=> $image_name,
																	'image_url'			=> $image_url
																]);
					}
				}
				
			}

			if($request->image_counter && $request->image_counter>0){
				/*print_r($_FILES);exit();*/
				for($i=1; $i<=$request->image_counter; $i++){
					
					$req_name = 'images'.$i;
					/*$size = $request->{$req_name}->getSize();
					echo ($size/(1024*1024));*/
					if(isset($_FILES[$req_name]) && !empty($_FILES[$req_name])){
						$image_name = 'life_events_web_'.time().rand(1,9999).'.png';
						$image_path = public_path('img/upload/'.$image_name);
						$image_url = url('img/upload/'.$image_name);
			            //$filePath = $request->file('user_picture')->storeAs('profilePictures', $fileName, 'public');
			            $request->{$req_name}->move(public_path('img/upload'), $image_name);
			            //$profUrl = url('/profilePictures/'.$fileName);
			            DB::table('life_event_images')->insert(	[
																		'image_event_id' 	=> $life_event_id,
																		'image_name' 		=> $image_name,
																		'image_url'			=> $image_url
																]);
					}
					//$fileName = time().'_'.$request->{$req_name}->getClientOriginalName();
					
				}
			}

			if($request->event_keywords ){
				$keywords = explode(',',$request->event_keywords);
				if(is_array($keywords)){
					foreach($keywords as $key => $keyword){
						$key_data = DB::table('keywords')->select('keyid')->where('userkeys',$keyword)->first();
						if(!$key_data){
							$keyid = DB::table('keywords')->insertGetId(['userkeys' => $keyword]);
						}else{
							$keyid = $key_data->keyid;
						}
						DB::table('life_event_keywords')->insert(['lek_event_id' => $life_event_id, 'lek_key_id'=>$keyid]);
					}
				}
			}
			if(!$request->is_draft || (isset($request->is_draft) && $request->is_draft==0)){
				$this->matchEvent($life_event_id, $request->type_id, $request->user_id);
			}
			
			if(($event_data->event_is_draft && $event_data->event_is_draft==1)){
				$redirect = url('drafts');
				$type = "Draft";
			}else{
				$redirect = url('life-events');
				$type = "Life Event";
			}
			return response([
                				'response_code'	=>	'200',
                         		'response_msg'	=> 	$type.' saved successfully',
                         		'reponse_body' 	=> 	'null',
                         		'redirect'		=>	$redirect
                        	], 200);
		}
    }

    function deleteImage(Request $request){
    	$rules =[
    		'user_id' 		=> 	'required',
    		'_event_id'		=>	'required',
    		'_image_id'		=>	'required'
    	];

		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$event_data = DB::table('life_events')->where('_event_id',$request->_event_id)->where('event_user_id', $request->user_id)->first();
			if(!$event_data){
				return response([
			        'response_code'=>'201',
			        'response_msg'=> 'Invalid data supplied',
			        'reponse_body' => "null"
			    ], 401);
			}

			$image_data = DB::table('life_event_images')->where('_image_id', $request->_image_id)->where('image_event_id', $request->_event_id)->first();
			if(!$image_data){
				return response([
			        'response_code'=>'201',
			        'response_msg'=> 'Invalid data supplied',
			        'reponse_body' => "null"
			    ], 401);
			}

			DB::table('life_event_images')->where('_image_id',$image_data->_image_id)->delete();

			return response([
                				'response_code'	=>	'200',
                         		'response_msg'	=> 	'Image Deleted Successfully',
                         		'reponse_body' 	=> 	'null'
                        	], 200);
		}
    }

    public function CategoryList(Request $request){
    	if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }

    	$life_event_categories = DB::table('life_event_categories')->get();

    	return View::make("admin/life_event_categories_list")->with(['life_event_categories' => $life_event_categories]);
    }

    public function editEventCategoryLinkModal(Request $request){
        $rules =[
                    'id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $category_data = DB::table('life_event_categories')->where('id', $request->id)->first();
            $html = view('admin.xhr.edit_event_category_link_modal',['category_data'=> $category_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    public function editEventCategoryLink(Request $request){
    	$rules =[
                    'event_video_link'     	=>  'required|url',
                    'id'      				=>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('life_event_categories')->where('id', $request->id)->first();
            if($title_data){
                DB::table('life_event_categories')->where('id', $request->id)->update([
                                                        'event_video_link'      =>  $request->event_video_link
                                                    ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Video Link is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    function uploadTest(Request $request){
    	print_r($_FILES);exit();
    }
}
