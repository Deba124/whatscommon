<?php

namespace App\Http\Controllers;

/*use Illuminate\Http\Request;*/
use Illuminate\Http\Request;
use App\Http\Controllers\DbHandler;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Userdetail;
use App\Mail\OrderShipped;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Validator;

class Login extends Controller
{
    function AdminLogin(Request $request){
    	/*$validated = $request->validate([
	        'admin_email' 		=> 	'required|email:filter',
	        'admin_password'	=> 	'required'
	    ]);*/

	    $rules =[
                    'admin_email'   =>  'required|email:filter',
                    'admin_password'=>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
        	trim($request->admin_email,' ');
		    $user= DB::table('admin_users')->where('admin_email', $request->admin_email)->first();

		    // print_r($data);
		    if (!$user || !Hash::check($request->admin_password, $user->admin_password)) {
		        return response([
		          'response_code'=>'201',
		          'response_msg'=> 'Invalid details',
		          'reponse_body' => "null"
		        ], 401);
		    }

		    $request->session()->put('admin_data', $user);

		    $response = [
		        'response_code'=>'200',
		        'response_msg'=> 'login successful',
		        'reponse_body' => $user,
		        'redirect'    =>  url('admin/dashboard')
		    ];

      		return response($response, 200);
        }
    }
}