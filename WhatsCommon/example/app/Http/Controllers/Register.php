<?php

namespace App\Http\Controllers;

/*use Illuminate\Http\Request;*/
use Illuminate\Http\Request;
use App\Http\Controllers\DbHandler;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Userdetail;
use App\Mail\OrderShipped;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Http\Controllers\Payment;
use View;

class Register extends Controller
{
	function index(){
    $response['countries'] = DB::select("SELECT DISTINCT country_isd FROM countries ORDER BY country_isd ASC");
		//return view("signup");
    return View::make("signup")->with($response);
	}

  public function newLogin(Request $request){
    //print_r($request);exit;
    $CheckHeader =$this->CheckRequestHeader($request);
            
    if(!$CheckHeader){
      return response([
        'response_code'=>'401',
        'response_msg'=> 'header missing or invalid',
        'reponse_body' => "null"
      ], 401);
    }
    //print_r($request);exit;
    $passreq = (isset($request->login_type) && $request->login_type=='normal') ? 'required' : '';
    $otpreq  = (isset($request->login_type) && $request->login_type=='otp') ? 'required' : '';
    $rules =[
              'email'       =>  'required',
              'login_type'  =>  'required',
              'password'    =>  $passreq,
              'otp'         =>  $otpreq
            ];
    $validate = Validator::make($request->all(),$rules);
    if($validate->fails()){
      return response([
                        'response_code' =>  '201',
                        'response_msg'  =>  'Not enough details are provided',
                        'reponse_body'  =>  "null",
                        'errors'    =>  $validate->errors()
                      ], 401);
    }else{
      trim($request->email,' ');
      $user= User::where('email', $request->email)
                  ->orWhere('username',$request->email)
                  ->orWhere('phone',$request->email)
                  ->first();
        
      if($request->login_type=='normal'){
        if (!$user || !Hash::check($request->password, $user->password)) {
          return response([
            'response_code'=>'201',
            'response_msg'=> 'Invalid details',
            'reponse_body' => "null"
          ], 401);
        }
      }else{
        if (!$user || $user->remember_token!=$request->otp) {
          return response([
            'response_code'=>'201',
            'response_msg'=> 'Invalid details',
            'reponse_body' => "null"
          ], 401);
        }
      }

      if($user->user_is_active==0){
        return response([
          'response_code'=>'201',
          'response_msg'=> 'You are not authorized to access. Please contact admin for login.',
          'reponse_body' => "null"
        ], 401);
      }

      User::where('id',$user->id)->update(['remember_token'=>NULL]);

      $data = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
                ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                ->leftJoin('cities','userdetails.city','=','cities._city_id')
                ->select('userdetails.*', DB::raw('TIMESTAMPDIFF(YEAR, userdetails.dob, CURRENT_DATE) AS user_age'), 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                ->where('userdetails.id', $user->id)
                ->first();

      //print_r($data);exit();

      /*$counts = DB::select("SELECT (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS matched_count,
            (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
            (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count");*/
      $counts = DB::select("SELECT 
        (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
        (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_personal_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_dating_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_adoption_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_travel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_military_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_school_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_career_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_pets_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_lost_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_doppel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=11 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS username_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=11 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_username_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=12 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS activity_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=12 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_activity_count,
        (SELECT SUM(personal_count + dating_count + adoption_count + travel_count + military_count + school_count + career_count + pets_count + lost_count + doppel_count + username_count + activity_count)) AS matched_count
        ");

      $premium_user = DB::select("SELECT IF((SELECT COUNT(id) FROM chosen_plans 
                            WHERE cp_user_id='".$user->id."'
                            AND cp_is_expired=0 
                            AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_premium");
      if($data && $user)
        $newData = array_merge($data->toArray(),$user->toArray());
      $newData['counts'] = $counts;
      $newData['user_is_premium'] = $premium_user[0]->is_premium ? $premium_user[0]->is_premium : 0;

      if(empty($newData['quick_blox_id'])){
        $qb_session = qb_login();
        if($qb_session && isset($qb_session->session) && $qb_session->session){
          $token = $qb_session->session->token;
          $newUser = quickAddUsers($token,$newData['email'],'12345678',$newData['email'],$newData['firstName'].' '.$newData['lastName']);
          if(isset($newUser->user) && $newUser->user){
            $quick_blox_id = $newUser->user->id;
            User::where('id',$newData['id'])->update(['quick_blox_id'=>$quick_blox_id]);
            /*$data_user = User::where('id',$newData['id'])->first();
            if($data && $data_user){
              $newData = [];
              $newData = array_merge($data->toArray(),$data_user->toArray());
              $newData['counts'] = $counts;*/
              $qb_session = qb_login($newData['email'],'12345678');
              if($qb_session && isset($qb_session->session) && $qb_session->session){
                $token = $qb_session->session->token;
                $newData['qb_token'] = $token;
              }
            //}
            
          }else{
            $existing_user = getUserByEmail($token,$newData['email']);
            if(isset($existing_user->user) && $existing_user->user){
              $quick_blox_id = $existing_user->user->id;
              User::where('id',$newData['id'])->update(['quick_blox_id'=>$quick_blox_id]);
              
              $qb_session = qb_login($newData['email'],'12345678');
              if($qb_session && isset($qb_session->session) && $qb_session->session){
                  
                $token = $qb_session->session->token;
                $newData['qb_token'] = $token;
              }
            }
          }
        }
      }else{
        $qb_session = qb_login($newData['email'],'12345678');
        if($qb_session && isset($qb_session->session) && $qb_session->session){
          $token = $qb_session->session->token;
          $newData['qb_token'] = $token;
        }
      }

      if(empty($newData['user_stripe_id'])){
        $res  = (new Payment)->createStripeUser($newData['email'],$newData['firstName'].' '.$newData['lastName']);
        if($res->id){
          User::where('id',$newData['id'])->update(['user_stripe_id'=>$res->id]);
          $newData['user_stripe_id'] = $res->id;
        }
      }
      $login_from = 'web';
      if($request->login_from && $request->login_from=='web'){
        $request->session()->put('userdata', $newData);
      }else{
        if($request->device_type){
          $login_from = $request->device_type;
        }
      }

      $check = DB::table('login_details')
                    ->where('login_user_id','=',$newData['id'])
                    ->where('login_from','=',$login_from)
                    ->where('login_date','=',date('Y-m-d'))
                    ->first();
      if(!$check){
        DB::table('login_details')->insert([
          'login_user_id'     =>  $newData['id'],
          'login_date'        =>  date('Y-m-d'),
          'login_from'        =>  $login_from
        ]);
      }else{
        DB::table('login_details')->where('_login_id',$check->_login_id)->update(['login_time' => date('Y-m-d H:i:s')]);
      }

      /*DB::table('login_details')->insert([
            'login_user_id'     =>  $newData['id'],
            'login_date'        =>  date('Y-m-d'),
            'login_from'        =>  $login_from
          ]);*/

      $response = [
        'response_code'=>'200',
        'response_msg'=> 'login successful',
        'reponse_body' => $newData,
        'redirect'    =>  url('home')
      ];

      return response($response, 200);
    }
  }

  function dologin(Request $request){
      $is_user_name = false;
      $CheckHeader =$this->CheckRequestHeader($request);
            
      if(!$CheckHeader){
        return response([
          'response_code'=>'401',
          'response_msg'=> 'header missing or invalid',
          'reponse_body' => "null"
        ], 401);
      }
      trim($request->email,' ');
      $user= User::where('email', $request->email)->first();
            
      if(!$user){
        $user = User::where('username', $request->email)->first();
        $is_user_name = true;
      }
                
            // print_r($data);
      if (!$user || !Hash::check($request->password, $user->password)) {
        return response([
          'response_code'=>'201',
          'response_msg'=> 'Invalid details',
          'reponse_body' => "null"
        ], 401);
      }

      if($user->user_is_active==0){
        return response([
          'response_code'=>'201',
          'response_msg'=> 'You are not authorized to access. Please contact admin for login.',
          'reponse_body' => "null"
        ], 401);
      }

            //$token = $user->createToken('my-app-token')->plainTextToken;
           //$data = Userdetail::where('email', $request->email)->first();
            /*$data = Userdetail::where('id', $user->id)->first();*/
      $data = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
                ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                ->leftJoin('cities','userdetails.city','=','cities._city_id')
                ->select('userdetails.*', DB::raw('TIMESTAMPDIFF(YEAR, userdetails.dob, CURRENT_DATE) AS user_age'), 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                ->where('userdetails.id', $user->id)
                ->first();
      /*$counts = DB::select("SELECT (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS matched_count,
(SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
(SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count");*/
$counts = DB::select("SELECT 
        (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
        (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count,
        (SELECT SUM(personal_count + dating_count + adoption_count + travel_count + military_count + school_count + career_count + pets_count + lost_count + doppel_count)) AS matched_count
        ");

      if(!$is_user_name){
        
        $data_user = User::where('email',$request->email)->first();
        if($data && $data_user)
        $newData = array_merge($data->toArray(),$data_user->toArray());
        $newData['counts'] = $counts;
      }
      else
      {
        $data_user = User::where('username',$request->email)->first();
        if($data && $data_user)
        $newData = array_merge($data->toArray(),$data_user->toArray());
        $newData['counts'] = $counts;
      }

      if(!$data)
      {
        $request->session()->put('userdata', $data_user);
        return response([
          'response_code' =>  '202',
          'response_msg'  =>  'details are incomplete',
          'reponse_body'  =>  $data_user,
          'redirect'      =>  url('signup-details')
        ], 202);
      }
      if(empty($newData['quick_blox_id'])){
        $qb_session = qb_login();
        if($qb_session && isset($qb_session->session) && $qb_session->session){
          $token = $qb_session->session->token;
          $newUser = quickAddUsers($token,$newData['email'],'12345678',$newData['email'],$newData['firstName'].' '.$newData['lastName']);
          if(isset($newUser->user) && $newUser->user){
            $quick_blox_id = $newUser->user->id;
            User::where('id',$newData['id'])->update(['quick_blox_id'=>$quick_blox_id]);
            $data_user = User::where('id',$newData['id'])->first();
            if($data && $data_user){
              $newData = [];
              $newData = array_merge($data->toArray(),$data_user->toArray());
              $newData['counts'] = $counts;
              $qb_session = qb_login($newData['email'],'12345678');
              if($qb_session && isset($qb_session->session) && $qb_session->session){
                $token = $qb_session->session->token;
                $newData['qb_token'] = $token;
              }
            }
            
          }
        }
      }else{
        $qb_session = qb_login($newData['email'],'12345678');
        if($qb_session && isset($qb_session->session) && $qb_session->session){
          $token = $qb_session->session->token;
          $newData['qb_token'] = $token;
        }
      }
      if(empty($newData['user_stripe_id'])){
        $res  = (new Payment)->createStripeUser($newData['email'],$newData['firstName'].' '.$newData['lastName']);
        if($res->id){
          User::where('id',$newData['id'])->update(['user_stripe_id'=>$res->id]);
          $newData['user_stripe_id'] = $res->id;
        }
      }
      if($request->login_from && $request->login_from=='web'){
        $request->session()->put('userdata', $newData);
      }

      $response = [
        'response_code'=>'200',
        'response_msg'=> 'login successful',
        'reponse_body' => $newData,
        'redirect'    =>  url('home')
      ];

      return response($response, 200);
  }

  function generate_username($name){
    $name = strtolower(trim($name));
    $check_avail = User::where('username',$name)->first();
    if(!$check_avail){
      return $name;
    }else{

      for($i=1;$i<999;$i++){
        $username = $name.$i;
        $check = User::where('username',$username)->first();
        if(!$check){
          return $username;
        }
      }
    }
  }

  function appleLogin(Request $request){
    $rules =[
            'social_type' =>  'required|in:facebook,google,twitter,apple',
            /*'social_id'   =>  'required'*/
            'email'       =>  'required|email:filter'
          ];
    $validate = Validator::make($request->all(),$rules);
    if($validate->fails()){
      return response([
                        'response_code' =>  '201',
                        'response_msg'  =>  'Not enough details are provided',
                        'reponse_body'  =>  "null",
                        'errors'    =>  $validate->errors()
                      ], 401);
    }else{
      $user= User::where('email', $request->email)/*->where('social_id','=',$request->social_id)*/->first();
      if($user){
        $data = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
                  ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                  ->leftJoin('cities','userdetails.city','=','cities._city_id')
                  ->select('userdetails.*', DB::raw('TIMESTAMPDIFF(YEAR, userdetails.dob, CURRENT_DATE) AS user_age'), 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                  ->where('userdetails.id', $user->id)
                  ->first();

        $counts = DB::select("SELECT 
        (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
        (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=11 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS username_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=12 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS activity_count,
        (SELECT SUM(personal_count + dating_count + adoption_count + travel_count + military_count + school_count + career_count + pets_count + lost_count + doppel_count + username_count + activity_count)) AS matched_count
        ");


        $premium_user = DB::select("SELECT IF((SELECT COUNT(id) FROM chosen_plans 
              WHERE cp_user_id='".$user->id."'
              AND cp_is_expired=0 
              AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_premium,
              IF((SELECT COUNT(id) FROM chosen_plans 
                WHERE cp_user_id='".$user->id."' AND cp_plan_id=3 AND cp_is_expired=0 
                AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_lifetime");

        if(!$data){

          DB::table('userdetails')->insert([
            'firstName'   =>  $user->firstName,
            'lastName'    =>  $user->lastName,
            'email'       =>  $user->email,
            'id'          =>  $user->id
          ]);

          $data = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
                  ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                  ->leftJoin('cities','userdetails.city','=','cities._city_id')
                  ->select('userdetails.*', DB::raw('TIMESTAMPDIFF(YEAR, userdetails.dob, CURRENT_DATE) AS user_age'), 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                  ->where('userdetails.id', $user->id)
                  ->first();
          
        }

        if($data && $user){
          $newData = array_merge($data->toArray(),$user->toArray());
          $newData['counts'] = $counts;
          $newData['user_is_premium'] = $premium_user[0]->is_premium ? $premium_user[0]->is_premium : 0;
          $newData['user_is_lifetime'] = $premium_user[0]->is_lifetime ? $premium_user[0]->is_lifetime : 0;
        }

        if(empty($newData['user_stripe_id'])){
          $res  = (new Payment)->createStripeUser($newData['email'],$newData['firstName'].' '.$newData['lastName']);
          if($res->id){
            User::where('id',$newData['id'])->update(['user_stripe_id'=>$res->id]);
            $newData['user_stripe_id'] = $res->id;
          }
        }

        $request->session()->put('userdata', $newData);

        $login_from = 'web';
      
        if($request->social_type){
          $login_from = $request->social_type;
        }

        $check = DB::table('login_details')
                    ->where('login_user_id','=',$newData['id'])
                    ->where('login_from','=',$login_from)
                    ->where('login_date','=',date('Y-m-d'))
                    ->first();
        if(!$check){
          DB::table('login_details')->insert([
            'login_user_id'     =>  $newData['id'],
            'login_date'        =>  date('Y-m-d'),
            'login_from'        =>  $login_from
          ]);
        }else{
          DB::table('login_details')->where('_login_id',$check->_login_id)->update(['login_time' => date('Y-m-d H:i:s')]);
        }
      

      /*DB::table('login_details')->insert([
            'login_user_id'     =>  $newData['id'],
            'login_date'        =>  date('Y-m-d'),
            'login_from'        =>  $login_from
          ]);*/

        $response = [
          'response_code'=>'200',
          'response_msg'=> 'login successful',
          'reponse_body' => $newData,
          'redirect'    =>  url('home')
        ];

        return response($response, 200);
      }else{
        return response([
                      'response_code'=>'201',
                      'response_msg'=> 'User not exist or invalid details',
                      'reponse_body' => "null"
                  ], 401);
      }
    }
  }

  function SocialLogin(Request $request){

    $rules =[
            'social_type' =>  'required|in:facebook,google,twitter,apple',
            'social_id'   =>  'required',
            'email'       =>  'required|email:filter',
            'name'        =>  'required',
            'image'       =>  'nullable'
          ];
    $validate = Validator::make($request->all(),$rules);
    if($validate->fails()){
      return response([
                        'response_code' =>  '201',
                        'response_msg'  =>  'Not enough details are provided',
                        'reponse_body'  =>  "null",
                        'errors'    =>  $validate->errors()
                      ], 401);
    }else{
      $user= User::where('email', $request->email)->first();
      if($user){
        $data = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
                  ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                  ->leftJoin('cities','userdetails.city','=','cities._city_id')
                  ->select('userdetails.*', DB::raw('TIMESTAMPDIFF(YEAR, userdetails.dob, CURRENT_DATE) AS user_age'), 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                  ->where('userdetails.id', $user->id)
                  ->first();

        $counts = DB::select("SELECT 
        (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
        (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=11 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS username_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=12 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS activity_count,
        (SELECT SUM(personal_count + dating_count + adoption_count + travel_count + military_count + school_count + career_count + pets_count + lost_count + doppel_count + username_count + activity_count)) AS matched_count
        ");

        $premium_user = DB::select("SELECT IF((SELECT COUNT(id) FROM chosen_plans 
  WHERE cp_user_id='".$user->id."'
  AND cp_is_expired=0 
  AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_premium");

        if(!$data){
          /*$user->counts = $counts;
          $user->user_is_premium = $premium_user[0]->is_premium ? $premium_user[0]->is_premium : 0;
          $request->session()->put('userdata', $user);
          return response([
            'response_code'=>'202',
            'response_msg'=> 'details are incomplete',
            'reponse_body' => $user,
            'redirect'      =>  url('signup-details')
          ], 202);*/
          DB::table('userdetails')->insert([
            'firstName'   =>  $user->firstName,
            'lastName'    =>  $user->lastName,
            'email'       =>  $user->email,
            'id'          =>  $user->id
          ]);

          $data = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
                  ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                  ->leftJoin('cities','userdetails.city','=','cities._city_id')
                  ->select('userdetails.*', DB::raw('TIMESTAMPDIFF(YEAR, userdetails.dob, CURRENT_DATE) AS user_age'), 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                  ->where('userdetails.id', $user->id)
                  ->first();
        }

        if($data && $user){
          $newData = array_merge($data->toArray(),$user->toArray());
          $newData['counts'] = $counts;
          $newData['user_is_premium'] = $premium_user[0]->is_premium ? $premium_user[0]->is_premium : 0;
        }

        if(empty($newData['quick_blox_id'])){
          
          $qb_session = qb_login();
          if($qb_session && isset($qb_session->session) && $qb_session->session){
            
            $token = $qb_session->session->token;
            $newUser = quickAddUsers($token,$newData['email'],'12345678',$newData['email'],$newData['firstName'].' '.$newData['lastName']);
            
            if(isset($newUser->user) && $newUser->user){
              
              $quick_blox_id = $newUser->user->id;
              User::where('id',$newData['id'])->update(['quick_blox_id'=>$quick_blox_id]);
              
              $qb_session = qb_login($newData['email'],'12345678');
              if($qb_session && isset($qb_session->session) && $qb_session->session){
                
                $token = $qb_session->session->token;
                $newData['qb_token'] = $token;
              }
              
            }else{
              $existing_user = getUserByEmail($token,$newData['email']);
              if(isset($existing_user->user) && $existing_user->user){
                $quick_blox_id = $existing_user->user->id;
                User::where('id',$newData['id'])->update(['quick_blox_id'=>$quick_blox_id]);
                
                $qb_session = qb_login($newData['email'],'12345678');
                if($qb_session && isset($qb_session->session) && $qb_session->session){
                  
                  $token = $qb_session->session->token;
                  $newData['qb_token'] = $token;
                }
              }
            }
          }
        }else{
          $qb_session = qb_login($newData['email'],'12345678');
          if($qb_session && isset($qb_session->session) && $qb_session->session){
            $token = $qb_session->session->token;
            $newData['qb_token'] = $token;
          }
        }
        
        if(empty($newData['user_stripe_id'])){
          $res  = (new Payment)->createStripeUser($newData['email'],$newData['firstName'].' '.$newData['lastName']);
          if($res->id){
            User::where('id',$newData['id'])->update(['user_stripe_id'=>$res->id]);
            $newData['user_stripe_id'] = $res->id;
          }
        }

        $request->session()->put('userdata', $newData);

        $login_from = 'web';
      
        if($request->social_type){
          $login_from = $request->social_type;
        }

        $check = DB::table('login_details')
                    ->where('login_user_id','=',$newData['id'])
                    ->where('login_from','=',$login_from)
                    ->where('login_date','=',date('Y-m-d'))
                    ->first();
        if(!$check){
          DB::table('login_details')->insert([
            'login_user_id'     =>  $newData['id'],
            'login_date'        =>  date('Y-m-d'),
            'login_from'        =>  $login_from
          ]);
        }else{
          DB::table('login_details')->where('_login_id',$check->_login_id)->update(['login_time' => date('Y-m-d H:i:s')]);
        }

      /*DB::table('login_details')->insert([
            'login_user_id'     =>  $newData['id'],
            'login_date'        =>  date('Y-m-d'),
            'login_from'        =>  $login_from
          ]);*/

        $response = [
          'response_code'=>'200',
          'response_msg'=> 'login successful',
          'reponse_body' => $newData,
          'redirect'    =>  url('home')
        ];

        return response($response, 200);
      }else{
        $nameArr = explode(' ',$request->name,2);
        $firstName = $nameArr[0];
        $lastName = $nameArr[1];
        $username = $this->generate_username(($firstName && strlen($firstName) > 2) ? $firstName : $lastName);
        $done = User::insertGetId([
                              'firstName'           =>  $firstName,
                              'lastName'            =>  $lastName,
                              'email'               =>  $request->email,
                              'profile_photo_path'  =>  $request->image ? $request->image : NULL,
                              'social_type'         =>  $request->social_type,
                              'social_id'           =>  $request->social_id,
                              'username'            =>  $username
                            ]);
        if(!$done){
            return response([
                      'response_code'=>'201',
                  'response_msg'=> 'User exists or invalid details',
                  'reponse_body' => "null"
                  ], 401);
        }
        else{
          DB::table('userdetails')->insert([
            'firstName'   =>  $firstName,
            'lastName'    =>  $lastName,
            'email'       =>  $request->email,
            'id'          =>  $done
          ]);
          $user = User::where('email',$request->email)->first();
          
        $counts = DB::select("SELECT 
        (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
        (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=11 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS username_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=12 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS activity_count,
        (SELECT SUM(personal_count + dating_count + adoption_count + travel_count + military_count + school_count + career_count + pets_count + lost_count + doppel_count + username_count + activity_count)) AS matched_count
        ");
          $user->counts = $counts;
          $premium_user = DB::select("SELECT IF((SELECT COUNT(id) FROM chosen_plans 
  WHERE cp_user_id='".$user->id."'
  AND cp_is_expired=0 
  AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_premium");
          $user->user_is_premium = $premium_user[0]->is_premium ? $premium_user[0]->is_premium : 0;
          if(empty($user->quick_blox_id)){
            $qb_session = qb_login();
            if($qb_session && isset($qb_session->session) && $qb_session->session){
              $token = $qb_session->session->token;
              $newUser = quickAddUsers($token,$user->email,'12345678',$user->email,$user->firstName.' '.$user->lastName);
              if(isset($newUser->user) && $newUser->user){
                $quick_blox_id = $newUser->user->id;
                User::where('id',$user->id)->update(['quick_blox_id'=>$quick_blox_id]);
                
                $qb_session = qb_login($user->email,'12345678');
                if($qb_session && isset($qb_session->session) && $qb_session->session){
                  $token = $qb_session->session->token;
                  $user->qb_token = $token;
                }
                
              }
            }
          }else{
            $qb_session = qb_login($user->email,'12345678');
            if($qb_session && isset($qb_session->session) && $qb_session->session){
              $token = $qb_session->session->token;
              $user->qb_token = $token;
            }
          }
          $request->session()->put('userdata', $user);
          return response([
            'response_code'=>'200',
            'response_msg'=> 'login successful',
            'reponse_body' => $user,
            'redirect'      =>  url('home')
          ], 202);
        }
      }
    }
  }

  function CheckRequestHeader(Request $request){
    if($request->hasHeader('Api-Auth-Key')){
      if($request->header('Api-Auth-Key') == '#whts_cmn_8080'){
        return true;
      }
      else{
        return false;
      }
    }
    return false;
  }

  function stepOne(Request $request){
    $validated = $request->validate([
      'email' 	  => 	'required|email:filter|unique:users|max:255',
      'username' 	=> 	'required|unique:users',
      'last_name'	=>	'required',
      'first_name'=>	'required',
      'password'	=>	'required',
      'cnf_pass' 	=>	'required|same:password',
      'phone'     =>  'required|numeric|unique:users',
      'isd_code'  =>  'required'
    ]);

    $done = DB::table('users')->insertGetId([
    			        'firstName' =>   $request->first_name,
                  'lastName'  =>   $request->last_name,
    			        'email' 	  =>   $request->email,
    			        'password' 	=>   Hash::make($request->password),
    			        'username' 	=>   $request->username,
    			        'phone'   	=>   $request->phone,
                  'isd_code'  =>   $request->isd_code,
    			        'mi'      	=>   $request->mi
              ]);

    if(!$done){
      return response([
                      	'response_code'	=>	'201',
			                	'response_msg'	=> 	'User exists or invalid details',
			                	'reponse_body' 	=> 	"null"
		                  ], 401);
    }else{
        /*$data = User::where('email', $request->email)->first();
        $request->session()->put('userdata', $data);*/
      DB::table('userdetails')->insert([
            'firstName'   =>  $request->first_name,
            'lastName'    =>  $request->last_name,
            'email'       =>  $request->email,
            'phone'       =>  $request->phone,
            'isd_code'    =>  $request->isd_code,
            'id'          =>  $done
          ]);
      $user = User::where('email',$request->email)->first();
          
      $counts = DB::select("SELECT 
        (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
        (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=11 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS username_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=12 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS activity_count,
        (SELECT SUM(personal_count + dating_count + adoption_count + travel_count + military_count + school_count + career_count + pets_count + lost_count + doppel_count + username_count + activity_count)) AS matched_count
        ");

        $data = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
                  ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                  ->leftJoin('cities','userdetails.city','=','cities._city_id')
                  ->select('userdetails.*', DB::raw('TIMESTAMPDIFF(YEAR, userdetails.dob, CURRENT_DATE) AS user_age'), 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                  ->where('userdetails.id', $user->id)
                  ->first();

        
      /*$user->counts = $counts;*/
          $premium_user = DB::select("SELECT IF((SELECT COUNT(id) FROM chosen_plans 
  WHERE cp_user_id='".$user->id."'
  AND cp_is_expired=0 
  AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_premium");
          /*$user->user_is_premium = $premium_user[0]->is_premium ? $premium_user[0]->is_premium : 0;*/
      if($data && $user){
        $newData = array_merge($data->toArray(),$user->toArray());
        $newData['counts'] = $counts;
        $newData['user_is_premium'] = $premium_user[0]->is_premium ? $premium_user[0]->is_premium : 0;
      }
          if(empty($user->quick_blox_id)){
            $qb_session = qb_login();
            if($qb_session && isset($qb_session->session) && $qb_session->session){
              $token = $qb_session->session->token;
              $newUser = quickAddUsers($token,$user->email,'12345678',$user->email,$user->firstName.' '.$user->lastName);
              if(isset($newUser->user) && $newUser->user){
                $quick_blox_id = $newUser->user->id;
                User::where('id',$user->id)->update(['quick_blox_id'=>$quick_blox_id]);
                
                $qb_session = qb_login($user->email,'12345678');
                if($qb_session && isset($qb_session->session) && $qb_session->session){
                  $token = $qb_session->session->token;
                  //$user->qb_token = $token;
                  $newData['qb_token'] = $token;
                }
                
              }
            }
          }else{
            $qb_session = qb_login($user->email,'12345678');
            if($qb_session && isset($qb_session->session) && $qb_session->session){
              $token = $qb_session->session->token;
              //$user->qb_token = $token;
              $newData['qb_token'] = $token;
            }
          }
          $request->session()->put('userdata', $newData);
          $login_from = 'web';

          /*DB::table('login_details')->insert([
            'login_user_id'     =>  $newData['id'],
            'login_date'        =>  date('Y-m-d'),
            'login_from'        =>  $login_from
          ]);*/
          $check = DB::table('login_details')
                    ->where('login_user_id','=',$newData['id'])
                    ->where('login_from','=',$login_from)
                    ->where('login_date','=',date('Y-m-d'))
                    ->first();
          if(!$check){
            DB::table('login_details')->insert([
              'login_user_id'     =>  $newData['id'],
              'login_date'        =>  date('Y-m-d'),
              'login_from'        =>  $login_from
            ]);
          }else{
            DB::table('login_details')->where('_login_id',$check->_login_id)->update(['login_time' => date('Y-m-d H:i:s')]);
          }
          return response([
            'response_code'=>'200',
            'response_msg'=> 'Registration Successful',
            'reponse_body' => $newData,
            'redirect'      =>  url('home')
          ], 202);

        /*return response([
                    			'response_code'	=>	'200',
                				  'response_msg'	=> 	'User is created',
                  				'reponse_body' 	=> 	$data,
                  				'redirect'		  =>	url('signup-details')
                  			], 200);*/
    }
  }

  function signupDetails(Request $request){
    if ($request->session()->has('userdata')) {
      $countries = DB::table('countries')->orderBy('country_name','asc')->get();
    	return view("signup_details")->with(['countries'=>$countries]);
    }else{
    	return redirect('signup');
    }
  }

    function stepTwo(Request $request){
    	$validated = $request->validate([
	        'email' 	=> 	'required|email:filter|unique:userdetails|max:255',
	        'phone'		=>	'required|numeric|digits:10',
	        'dob' 		=> 	'required|date',
          'country' =>  'required',
          'city'    =>  'required',
          'state'   =>  'required'
	        /*'last_name'	=>	'required',
	        'first_name'=>	'required',
	        'password'	=>	'required',
	        'cnf_pass' 	=>	'required|same:password'*/
	    ]);
    	$userdata = $request->session()->get('userdata');
	    $done =  DB::table('userdetails')->insert([
													 'firstName' =>   $userdata->first_name,
                            'lastName'  =>   $userdata->last_name,
												    'email' 	=> 	$request->email,
												    'dob' 		=> 	$request->dob,
												    'id' 		=> 	$userdata->id,
												    'phone' 	=> 	$request->phone,
												    'country' 	=> 	$request->country,
												    /*'state' 	=> 	$request->state,*/
												    'city' 		=> 	$request->city,
												    'province' 	=> 	$request->state,
												    'street' 	=> 	$request->street,
												    'zip' 		=> 	$request->zip,
												    'mi' 		=> 	$userdata->mi
										        ]);

        if(!$done){
            return response([
                      			'response_code'=>'201',
                  				'response_msg'=> 'User exists or invalid details',
                  				'reponse_body' => "null"
                  			], 401);
        }
        else{
        	$totalData = Userdetail::where('email',$request->email) -> first();
        	$request->session()->forget('userdata');
          if($request->keywords){
            $splittedKeys = explode(",",$request->keywords);
            $dat = DB::table('keywords')->select('userkeys')->get()->toarray();
            $temp='';
            
            foreach($dat as $key)
            {
              $temp .= $key->userkeys.',';
            }
            $temp = explode(',',$temp);
            if($dat)
            { 
              $i = 0;
              foreach($splittedKeys as $keys)
              {
                
                if(!in_array($keys,$temp))
                {
                  //die('not in array '.$keys.'in '.$temp[$i]);
                  DB::table('keywords')->insert(['userkeys'=>$keys]);
                }
                
                $keyid = DB::table('keywords')->select('keyid')->where('userkeys',$keys)->first();
                $temp1 = '';
                
                $userKeys = DB::table('keywordslinks')->select('keyid')->where('id',$userdata->id)->get();
                
                foreach($userKeys as $key)
                {
                    $temp1.= $key->keyid.',';
                }
                $temp1 = explode(',',$temp1);
                
                if($keyid)
                {
                  if(!in_array($keyid->keyid,$temp1)) 
                    DB::table('keywordslinks')->insert(['id'=>$userdata->id,'keyid'=>$keyid->keyid]);
                }
                $i++;
              }
              $keywords = DB::table('keywords')
                              ->selectRaw('keywords.*')
                              ->join('keywordslinks', 'keywords.keyid', '=', 'keywordslinks.keyid')
                              ->where('keywordslinks.id',$userdata->id)
                              ->get();
            }
          }
          return response([
                  			'response_code'	=>	'200',
                				'response_msg'	=> 	'User details inserted',
                				'reponse_body' 	=> 	$totalData,
                				'redirect'		=>	url('home')
                			], 200);
        
        }
    }

    function checkEmail(Request $request){
    	$validated = $request->validate([
	        'email' 	=> 	'required|email:filter',
	    ]);
    	$dat = DB::table("users")->select('id')->where('email',$request->email)->first();
	    if($dat && $dat->id>0)
        {
        	$rand = rand(100000,900000);
           	$data = [
          				'subject' 	=> 	'OTP GENERATED',
          				'name' 		=> 	'WhatsCommon',
          				'email' 	=> 	$request->email,
          				'content' 	=> 	$rand
        			];
        	User::where('email',$request->email)->update(['remember_token'=>$rand]);
        	Mail::send('otppage', $data, function($message) use ($data) {
          		$message->to($data['email'])->subject($data['subject']);
        	});
            // Mail::to($request->email)->send(new OrderShipped());
            $request->session()->put('id', $dat->id);
            return response([
                				'response_code'	=>	'200',
                				'response_msg'	=> 	'an otp has been sent',
                				'redirect'		=>	url('reset-password'),
                				'reponse_body' 	=>	$dat],200);
        }

        return response([
                        	'response_code'	=>	'203',
                     		'response_msg'	=> 	'address not found',
                     		'reponse_body' 	=> 	"null"
                     	], 401);
    }

  public function getOTP(Request $request){
    $rules =[
              'username'       =>  'required'
            ];
    $validate = Validator::make($request->all(),$rules);
    if($validate->fails()){
      return response([
                        'response_code' =>  '201',
                        'response_msg'  =>  'Not enough details are provided',
                        'reponse_body'  =>  "null",
                        'errors'        =>  $validate->errors()
                      ], 401);
    }else{
      $dat = DB::table("users")
                ->where('email',$request->username)
                ->orWhere('username',$request->username)
                ->orWhere('phone',$request->username)
                ->first();
      if($dat){
        $rand = rand(100000,900000);
        $content = $rand.' is your one time password for WhatsCommon login verification';
        $data = [
                'subject'   =>  'WhatsCommon Login OTP',
                'name'    =>  'WhatsCommon',
                'email'   =>  $dat->email,
                'content'   =>  $content
            ];
        User::where('id',$dat->id)->update(['remember_token'=>$rand]);
        Mail::send('otppage', $data, function($message) use ($data) {
            $message->to($data['email'])->subject($data['subject']);
        });
        if(!empty($dat->isd_code) && !empty($dat->phone)){
          $to = $dat->isd_code.$dat->phone;
          /*$text = $rand.' is your one time password for WhatsCommon login verification';*/
          $res = sendSms($to,$content);
          //print_r($res);exit();
        }
          // Mail::to($request->email)->send(new OrderShipped());
          /*$request->session()->put('id', $dat->id);*/
        $user = User::find($dat->id);
        return response([
                      'response_code' =>  '200',
                      'response_msg'  =>  'an otp has been sent',
                      /*'redirect'    =>  url('reset-password'),*/
                      'reponse_body'  =>  NULL],200);
      }
      return response([
                        'response_code' =>  '203',
                        'response_msg'  =>  'There are no such record exists with this data',
                        'reponse_body'  =>  "null"
                      ], 401);
    }
  }

    function setPassword(Request $request){
    	$validated = $request->validate([
	        'otp' 				=> 	'required',
	        'password'			=>	'required',
	        'confirm_password' 	=>	'required|same:password'
	    ]);

	    if ($request->session()->has('id')) {
	    	$id = $request->session()->get('id');
	    	$user_data = DB::table("users")->where('id',$id)->first();

	    	if($user_data){
	    		if($user_data->remember_token==$request->otp){
	    			User::where('id',$id)->update([
        											'password'			=>	Hash::make($request->password),
        											'remember_token'	=>	null
        										]);
	    			return response([
	    								'response_code'	=>	'200',
                         				'response_msg'	=> 	'password set successfully',
                         				'reponse_body' 	=> 	'null',
                         				'redirect'		=>	url('login')
                         			],200);
	    		}else{
	    			return response([
                        	'response_code'	=>	'203',
                     		'response_msg'	=> 	'Invalid OTP Entered',
                     		'reponse_body' 	=> 	"null"
                     	], 401);
	    		}
	    	}
	    }

	    return response([
                        	'response_code'	=>	'203',
                     		'response_msg'	=> 	'Invalid Request',
                     		'reponse_body' 	=> 	"null"
                     	], 401);
    }
}
