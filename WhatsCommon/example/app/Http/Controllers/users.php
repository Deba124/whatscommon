<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\DbHandler;
use Illuminate\Support\Facades\DB;
//use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Feedback;
use App\Models\Userdetail;
use App\Mail\OrderShipped;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use View;
use Validator;
//use App\Http\Views\Response;

class users extends Controller
{
    //
    function printMsg($data)
    {
        echo $data;
    }

    function CheckRequestHeader(Request $request)
    {
        if($request->hasHeader('Api-Auth-Key'))
        {
            if($request->header('Api-Auth-Key') == '#whts_cmn_8080')
            {
                return true;
            }
            else
                return false;
        }
        return false;
    }
    function gesture()
    {
    }
    public function userProfile(Request $request){
      $CheckHeader =$this->CheckRequestHeader($request);

      if(!$CheckHeader){
        return response([
          'response_code'=>'401',
          'response_msg'=> 'header missing or invalid',
          'reponse_body' => "null"
        ], 401);
      }

      $rules =[
                'user_id'     =>  'required',
                'other_user'  =>  'required'
              ];
      $validate = Validator::make($request->all(),$rules);
      if($validate->fails()){
        return response([
                          'response_code' =>  '201',
                          'response_msg'  =>  'Not enough details are provided',
                          'reponse_body'  =>  "null",
                          'errors'        =>  $validate->errors()
                        ], 401);
      }else{
        /*(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->other_user."') AND (feed_user1 = '".$request->other_user."' OR feed_user2 = '".$request->other_user."')) AS matched_count,*/
        $user_data = User::find($request->other_user);
        $userdetails = DB::select("SELECT userdetails.*,users.show_connection_list, users.show_match_feed, users.show_connection_feed, users.quick_blox_id, users.username, users.profile_photo_path, users.privacy_email, users.privacy_phone, users.privacy_birthday, users.created_at, countries.country_name, provinces.province_name, cities.city_name,
          (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status =1 AND ((request_from_user=$request->user_id AND request_to_user=$request->other_user) OR (request_from_user=$request->other_user AND request_to_user=$request->user_id))), 1, 0)) AS is_connected,
          (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status =0 AND ((request_from_user=$request->user_id AND request_to_user=$request->other_user) OR (request_from_user=$request->other_user AND request_to_user=$request->user_id))), 1, 0)) AS is_pending_connection,
          (SELECT request_from_user FROM connection_requests WHERE request_status =0 AND ((request_from_user=$request->user_id AND request_to_user=$request->other_user) OR (request_from_user=$request->other_user AND request_to_user=$request->user_id))) AS request_from_user,
          (SELECT _request_id FROM connection_requests WHERE request_status =0 AND ((request_from_user=$request->user_id AND request_to_user=$request->other_user) OR (request_from_user=$request->other_user AND request_to_user=$request->user_id))) AS _request_id,
        (SELECT IF ((SELECT COUNT(*) FROM user_follows WHERE follow_user=$request->user_id AND follow_following=$request->other_user) , 1, 0)) AS is_following,
        (SELECT IF ((SELECT COUNT(*) FROM blocked_users WHERE bu_from_id=$request->user_id AND bu_to_id=$request->other_user) , 1, 0)) AS is_blocked,
        (SELECT event_type FROM life_event_categories 
         INNER JOIN feeds ON feeds.feed_lifeevent=life_event_categories.id
         INNER JOIN connection_requests ON connection_requests.request_feed=feeds._feed_id
         WHERE connection_requests.request_status=1
         AND ((connection_requests.request_from_user=$request->user_id AND connection_requests.request_to_user=$request->other_user) OR (connection_requests.request_from_user=$request->other_user AND connection_requests.request_to_user=$request->user_id))
         ) AS event_type,
         (SELECT COUNT(DISTINCT feed_lifeevent) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND ((feed_user1 = '".$request->user_id."' AND feed_user2 = $request->other_user) OR (feed_user1 = $request->other_user AND feed_user2 = '".$request->user_id."'))) AS events_type_count,
         (SELECT GROUP_CONCAT(DISTINCT event_type) FROM life_event_categories WHERE life_event_categories.id IN(SELECT DISTINCT feed_lifeevent FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND ((feed_user1 = '".$request->user_id."' AND feed_user2 = $request->other_user) OR (feed_user1 = $request->other_user AND feed_user2 = '".$request->user_id."')) )) AS events_type_names,
         
          (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$request->other_user."') AS life_events_count,
          (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$request->other_user."' OR request_to_user='".$request->other_user."')) AS connection_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->other_user."') AND feed_lifeevent=1 AND (feed_user1 = '".$request->other_user."' OR feed_user2 = '".$request->other_user."')) AS personal_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->other_user."') AND feed_lifeevent=2 AND (feed_user1 = '".$request->other_user."' OR feed_user2 = '".$request->other_user."')) AS dating_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->other_user."') AND feed_lifeevent=3 AND (feed_user1 = '".$request->other_user."' OR feed_user2 = '".$request->other_user."')) AS adoption_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->other_user."') AND feed_lifeevent=4 AND (feed_user1 = '".$request->other_user."' OR feed_user2 = '".$request->other_user."')) AS travel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->other_user."') AND feed_lifeevent=5 AND (feed_user1 = '".$request->other_user."' OR feed_user2 = '".$request->other_user."')) AS military_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->other_user."') AND feed_lifeevent=6 AND (feed_user1 = '".$request->other_user."' OR feed_user2 = '".$request->other_user."')) AS school_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->other_user."') AND feed_lifeevent=7 AND (feed_user1 = '".$request->other_user."' OR feed_user2 = '".$request->other_user."')) AS career_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->other_user."') AND feed_lifeevent=8 AND (feed_user1 = '".$request->other_user."' OR feed_user2 = '".$request->other_user."')) AS pets_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->other_user."') AND feed_lifeevent=9 AND (feed_user1 = '".$request->other_user."' OR feed_user2 = '".$request->other_user."')) AS lost_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->other_user."') AND feed_lifeevent=10 AND (feed_user1 = '".$request->other_user."' OR feed_user2 = '".$request->other_user."')) AS doppel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->other_user."') AND feed_lifeevent=11 AND (feed_user1 = '".$request->other_user."' OR feed_user2 = '".$request->other_user."')) AS username_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->other_user."') AND feed_lifeevent=12 AND (feed_user1 = '".$request->other_user."' OR feed_user2 = '".$request->other_user."')) AS activity_count,
          (SELECT SUM(personal_count + dating_count + adoption_count + travel_count + military_count + school_count + career_count + pets_count + lost_count + doppel_count + username_count + activity_count)) AS matched_count
          FROM userdetails
          INNER JOIN users ON users.id=userdetails.id
          LEFT JOIN countries ON userdetails.country=countries._country_id
          LEFT JOIN provinces ON userdetails.state = provinces._province_id
          LEFT JOIN cities ON userdetails.city=cities._city_id
          WHERE userdetails.id = '".$request->other_user."'
          ");
      $connections = DB::select("SELECT users.firstName, users.lastName, users.profile_photo_path, users.id, users.username, users.quick_blox_id, userdetails.bio, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status =1 AND ((request_from_user=$request->other_user AND request_to_user=users.id) OR (request_from_user=users.id AND request_to_user=$request->other_user))), 1, 0)) AS is_connected
        FROM users 
        INNER JOIN userdetails ON userdetails.id=users.id
        WHERE users.id!='".$request->other_user."' 
        AND users.id NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id='".$request->other_user."')
        HAVING is_connected=1
        ");
      $select = 'SELECT feeds.*, u1.quick_blox_id AS user1_quick_blox_id, u2.quick_blox_id AS user2_quick_blox_id, u1.profile_photo_path AS user1_profile_photo, u1.firstName AS user1_firstName, u1.lastName AS user1_lastName, u2.profile_photo_path AS user2_profile_photo, u2.firstName AS user2_firstName, u2.lastName AS user2_lastName, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 1 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))), 1, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 0 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))),2,0)))) AS is_connected FROM feeds 
                  INNER JOIN users u1 ON u1.id=feeds.feed_user1
                  INNER JOIN users u2 ON u2.id=feeds.feed_user2
                  WHERE (feed_user1="'.$request->other_user.'" OR feed_user2="'.$request->other_user.'") 
                  AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id="'.$request->other_user.'") 
                  ORDER BY _feed_id DESC';
                //echo $select;exit;

        $feeds = DB::select($select);
        $new_feeds = [];
        if($feeds){
          foreach($feeds as $key => $val){
            $feed_images = DB::table('feed_images')->where('fi_feed_id',$val->_feed_id)->get();
            $val->feed_images = $feed_images;
            array_push($new_feeds, $val);
          }
        }
        $response = [
          'response_code' =>  '200',
          'response_msg'  =>  'user details fetched successful',
          'reponse_body'  =>  ['userdetails' => $userdetails, 'connections' => $connections, 'feeds' => $new_feeds],
          'redirect'      =>  url('home')
        ];

        return response($response, 200);
      }
    }

  public function getUserByQB(Request $request){
    /*$CheckHeader =$this->CheckRequestHeader($request);

    if(!$CheckHeader){
      return response([
        'response_code'=>'401',
        'response_msg'=> 'header missing or invalid',
        'reponse_body' => "null"
      ], 401);
    }*/

    $rules =[
              'user_id'     =>  'required',
              'other_user'  =>  'required'
            ];
    $validate = Validator::make($request->all(),$rules);
    if($validate->fails()){
      return response([
                        'response_code' =>  '201',
                        'response_msg'  =>  'Not enough details are provided',
                        'reponse_body'  =>  "null",
                        'errors'        =>  $validate->errors()
                      ], 401);
    }else{
      $user_data = DB::table('users')->where('quick_blox_id',$request->other_user)->first();
      if($user_data){
        $userdetails = DB::select("SELECT userdetails.*, users.quick_blox_id, users.profile_photo_path, users.privacy_email, users.privacy_phone, users.privacy_birthday, countries.country_name, provinces.province_name, cities.city_name,
          (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status =1 AND ((request_from_user=$request->user_id AND request_to_user=$user_data->id) OR (request_from_user=$user_data->id AND request_to_user=$request->user_id))), 1, 0)) AS is_connected,
        (SELECT IF ((SELECT COUNT(*) FROM user_follows WHERE follow_user=$request->user_id AND follow_following=$user_data->id) , 1, 0)) AS is_following,
        (SELECT IF ((SELECT COUNT(*) FROM blocked_users WHERE bu_from_id=$request->user_id AND bu_to_id=$user_data->id) , 1, 0)) AS is_blocked
          FROM userdetails
          INNER JOIN users ON users.id=userdetails.id
          LEFT JOIN countries ON userdetails.country=countries._country_id
          LEFT JOIN provinces ON userdetails.state = provinces._province_id
          LEFT JOIN cities ON userdetails.city=cities._city_id
          WHERE userdetails.id = '".$user_data->id."'
          ");
        $response = [
          'response_code' =>  '200',
          'response_msg'  =>  'user details fetched successful',
          'reponse_body'  =>  ['userdetails' => $userdetails],
          'redirect'      =>  url('home')
        ];

        return response($response, 200);
      }else{
        return response([
          'response_code'=>'401',
          'response_msg'=> 'Invalid data supplied',
          'reponse_body' => "null"
        ], 401);
      }
    }
  }
  public function logOut(Request $request){
    $CheckHeader =$this->CheckRequestHeader($request);

    if(!$CheckHeader){
      return response([
        'response_code' =>  '401',
        'response_msg'  =>  'header missing or invalid',
        'reponse_body'  =>  "null"
      ], 401);
    }

    $rules =[
              'user_id'     =>  'required',
              'device_type' =>  'required|in:android,ios'
            ];
    $validate = Validator::make($request->all(),$rules);
    if($validate->fails()){
      return response([
                        'response_code' =>  '201',
                        'response_msg'  =>  'Not enough details are provided',
                        'reponse_body'  =>  "null",
                        'errors'        =>  $validate->errors()
                      ], 401);
    }else{
      $to_user_data = DB::table('users')
                ->join('device_tokens','users.id','=','device_tokens.device_user_id')
                ->select('device_tokens.*')
                ->where('users.id',$request->user_id)
                ->where('device_tokens.device_type',$request->device_type)
                ->get();
      if($to_user_data && count($to_user_data)>0){
        DB::table('device_tokens')
          ->where('device_user_id',$request->user_id)
          ->where('device_type',$request->device_type)
          ->delete();
      }

      $response = [
          'response_code' =>  '200',
          'response_msg'  =>  'You have been logged out successfully',
          'reponse_body'  =>  NULL,
          'redirect'      =>  url('login')
      ];

      return response($response, 200);
    }
  }
  public function ProfileDetails(Request $request){
      $CheckHeader =$this->CheckRequestHeader($request);

      if(!$CheckHeader){
        return response([
          'response_code'=>'401',
          'response_msg'=> 'header missing or invalid',
          'reponse_body' => "null"
        ], 401);
      }

      $rules =[
                'user_id'     =>  'required'
              ];
      $validate = Validator::make($request->all(),$rules);
      if($validate->fails()){
        return response([
                          'response_code' =>  '201',
                          'response_msg'  =>  'Not enough details are provided',
                          'reponse_body'  =>  "null",
                          'errors'        =>  $validate->errors()
                        ], 401);
      }else{
        $user_data = User::find($request->user_id);

        $userdetails = DB::select("SELECT userdetails.*, users.quick_blox_id, users.profile_photo_path, users.privacy_email, users.privacy_phone, users.privacy_birthday, countries.country_name, provinces.province_name, cities.city_name,
          
          (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$request->user_id."') AS life_events_count,
          (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$request->user_id."' OR request_to_user='".$request->user_id."')) AS connection_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=1 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS personal_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=1 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS total_personal_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=2 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS dating_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=2 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS total_dating_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=3 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS adoption_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=3 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS total_adoption_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=4 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS travel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=4 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS total_travel_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=5 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS military_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=5 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS total_military_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=6 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS school_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=6 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS total_school_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=7 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS career_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=7 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS total_career_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=8 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS pets_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=8 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS total_pets_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=9 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS lost_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=9 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS total_lost_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=10 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS doppel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=10 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS total_doppel_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=11 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS username_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=11 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS total_username_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=12 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS activity_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=12 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS total_activity_count,

          (SELECT SUM(personal_count + dating_count + adoption_count + travel_count + military_count + school_count + career_count + pets_count + lost_count + doppel_count + username_count + activity_count)) AS matched_count
          FROM userdetails
          INNER JOIN users ON users.id=userdetails.id
          LEFT JOIN countries ON userdetails.country=countries._country_id
          LEFT JOIN provinces ON userdetails.state = provinces._province_id
          LEFT JOIN cities ON userdetails.city=cities._city_id
          WHERE userdetails.id = '".$request->user_id."'
          ");
        $response = [
          'response_code' =>  '200',
          'response_msg'  =>  'user details fetched successful',
          'reponse_body'  =>  ['userdetails' => $userdetails],
          'redirect'      =>  url('home')
        ];

        return response($response, 200);
      }
    }
  function generate_username($name){
    $name = strtolower(trim($name));
    $check_avail = User::where('username',$name)->first();
    if(!$check_avail){
      return $name;
    }else{

      for($i=1;$i<999;$i++){
        $username = $name.$i;
        $check = User::where('username',$username)->first();
        if(!$check){
          return $username;
        }
      }
    }
  }

  function appleLogin(Request $request){
    $rules =[
            'social_type' =>  'required|in:facebook,google,twitter,apple',
            /*'social_id'   =>  'required'*/
            'email'       =>  'required|email:filter'
          ];
    $validate = Validator::make($request->all(),$rules);
    if($validate->fails()){
      return response([
                        'response_code' =>  '201',
                        'response_msg'  =>  'Not enough details are provided',
                        'reponse_body'  =>  "null",
                        'errors'    =>  $validate->errors()
                      ], 401);
    }else{
      $user= User::where('email', $request->email)/*->where('social_id','=',$request->social_id)*/->first();
      if($user){
        $data = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
                  ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                  ->leftJoin('cities','userdetails.city','=','cities._city_id')
                  ->select('userdetails.*', DB::raw('TIMESTAMPDIFF(YEAR, userdetails.dob, CURRENT_DATE) AS user_age'), 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                  ->where('userdetails.id', $user->id)
                  ->first();

        $counts = DB::select("SELECT 
        (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
        (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_personal_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_dating_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_adoption_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_travel_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_military_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_school_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_career_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_pets_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_lost_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_doppel_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=11 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS username_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=11 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_username_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=12 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS activity_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=12 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_activity_count,

          (SELECT SUM(personal_count + dating_count + adoption_count + travel_count + military_count + school_count + career_count + pets_count + lost_count + doppel_count + username_count + activity_count)) AS matched_count
        ");


        $premium_user = DB::select("SELECT IF((SELECT COUNT(id) FROM chosen_plans 
              WHERE cp_user_id='".$user->id."'
              AND cp_is_expired=0 
              AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_premium,
              IF((SELECT COUNT(id) FROM chosen_plans 
                WHERE cp_user_id='".$user->id."' AND cp_plan_id=3 AND cp_is_expired=0 
                AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_lifetime");

        if(!$data){

          DB::table('userdetails')->insert([
            'firstName'   =>  $user->firstName,
            'lastName'    =>  $user->lastName,
            'email'       =>  $user->email,
            'id'          =>  $user->id
          ]);

          $data = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
                  ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                  ->leftJoin('cities','userdetails.city','=','cities._city_id')
                  ->select('userdetails.*', DB::raw('TIMESTAMPDIFF(YEAR, userdetails.dob, CURRENT_DATE) AS user_age'), 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                  ->where('userdetails.id', $user->id)
                  ->first();
          
        }

        if($data && $user){
          $newData = array_merge($data->toArray(),$user->toArray());
          $newData['counts'] = $counts;
          $newData['user_is_premium'] = $premium_user[0]->is_premium ? $premium_user[0]->is_premium : 0;
          $newData['user_is_lifetime'] = $premium_user[0]->is_lifetime ? $premium_user[0]->is_lifetime : 0;
        }

        $login_from = 'web';
      
        if($request->social_type){
          $login_from = $request->social_type;
        }

        $check = DB::table('login_details')
                    ->where('login_user_id','=',$newData['id'])
                    ->where('login_from','=',$login_from)
                    ->where('login_date','=',date('Y-m-d'))
                    ->first();
        if(!$check){
          DB::table('login_details')->insert([
            'login_user_id'     =>  $newData['id'],
            'login_date'        =>  date('Y-m-d'),
            'login_from'        =>  $login_from
          ]);
        }else{
          DB::table('login_details')->where('_login_id',$check->_login_id)->update(['login_time' => date('Y-m-d H:i:s')]);
        }
        /**/

        $response = [
          'response_code'=>'200',
          'response_msg'=> 'login successful',
          'reponse_body' => $newData,
          'redirect'    =>  url('home')
        ];

        return response($response, 200);
      }else{
        return response([
                      'response_code'=>'201',
                      'response_msg'=> 'User not exist or invalid details',
                      'reponse_body' => "null"
                  ], 401);
      }
    }
  }

  function SocialLogin(Request $request){
    /*$CheckHeader =$this->CheckRequestHeader($request);

    if(!$CheckHeader){
      return response([
        'response_code'=>'401',
        'response_msg'=> 'header missing or invalid',
        'reponse_body' => "null"
      ], 401);
    }*/

    $rules =[
            'social_type' =>  'required|in:facebook,google,twitter,apple',
            'social_id'   =>  'required',
            'email'       =>  'required|email:filter',
            'name'        =>  'required',
            'image'       =>  'nullable'
          ];
    $validate = Validator::make($request->all(),$rules);
    if($validate->fails()){
      return response([
                        'response_code' =>  '201',
                        'response_msg'  =>  'Not enough details are provided',
                        'reponse_body'  =>  "null",
                        'errors'    =>  $validate->errors()
                      ], 401);
    }else{
      $user= User::where('email', $request->email)->first();
      if($user){
        $data = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
                  ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                  ->leftJoin('cities','userdetails.city','=','cities._city_id')
                  ->select('userdetails.*', DB::raw('TIMESTAMPDIFF(YEAR, userdetails.dob, CURRENT_DATE) AS user_age'), 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                  ->where('userdetails.id', $user->id)
                  ->first();
                  /*(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS matched_count,*/
        $counts = DB::select("SELECT 
        (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
        (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_personal_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_dating_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_adoption_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_travel_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_military_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_school_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_career_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_pets_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_lost_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_doppel_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=11 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS username_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=11 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_username_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=12 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS activity_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=12 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_activity_count,

          (SELECT SUM(personal_count + dating_count + adoption_count + travel_count + military_count + school_count + career_count + pets_count + lost_count + doppel_count + username_count + activity_count)) AS matched_count
        ");


        $premium_user = DB::select("SELECT IF((SELECT COUNT(id) FROM chosen_plans 
  WHERE cp_user_id='".$user->id."'
  AND cp_is_expired=0 
  AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_premium,
  IF((SELECT COUNT(id) FROM chosen_plans 
                                    WHERE cp_user_id='".$user->id."' AND cp_plan_id=3 AND cp_is_expired=0 
                                AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_lifetime");

        if(!$data){

          DB::table('userdetails')->insert([
            'firstName'   =>  $user->firstName,
            'lastName'    =>  $user->lastName,
            'email'       =>  $user->email,
            'id'          =>  $user->id
          ]);

          $data = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
                  ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                  ->leftJoin('cities','userdetails.city','=','cities._city_id')
                  ->select('userdetails.*', DB::raw('TIMESTAMPDIFF(YEAR, userdetails.dob, CURRENT_DATE) AS user_age'), 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                  ->where('userdetails.id', $user->id)
                  ->first();
          
        }

        if($data && $user){
          $newData = array_merge($data->toArray(),$user->toArray());
          $newData['counts'] = $counts;
          $newData['user_is_premium'] = $premium_user[0]->is_premium ? $premium_user[0]->is_premium : 0;
          $newData['user_is_lifetime'] = $premium_user[0]->is_lifetime ? $premium_user[0]->is_lifetime : 0;
        }

        $login_from = 'web';
      
        if($request->social_type){
          $login_from = $request->social_type;
        }

        $check = DB::table('login_details')
                    ->where('login_user_id','=',$newData['id'])
                    ->where('login_from','=',$login_from)
                    ->where('login_date','=',date('Y-m-d'))
                    ->first();
        if(!$check){
          DB::table('login_details')->insert([
            'login_user_id'     =>  $newData['id'],
            'login_date'        =>  date('Y-m-d'),
            'login_from'        =>  $login_from
          ]);
        }else{
          DB::table('login_details')->where('_login_id',$check->_login_id)->update(['login_time' => date('Y-m-d H:i:s')]);
        }

        /*DB::table('login_details')->insert([
            'login_user_id'     =>  $newData['id'],
            'login_date'        =>  date('Y-m-d'),
            'login_from'        =>  $login_from
          ]);*/

        $response = [
          'response_code'=>'200',
          'response_msg'=> 'login successful',
          'reponse_body' => $newData,
          'redirect'    =>  url('home')
        ];

        return response($response, 200);
      }else{
        $nameArr = explode(' ',$request->name,2);
        $firstName = $nameArr[0];
        $lastName = $nameArr[1];
        $username = $this->generate_username(($firstName && strlen($firstName) > 2) ? $firstName : $lastName);
        $done = User::insertGetId([
                              'firstName'           =>  $firstName,
                              'lastName'            =>  $lastName,
                              'email'               =>  $request->email,
                              'profile_photo_path'  =>  $request->image ? $request->image : NULL,
                              'social_type'         =>  $request->social_type,
                              'social_id'           =>  $request->social_id,
                              'username'            =>  $username
                            ]);
        if(!$done){
            return response([
                      'response_code'=>'201',
                  'response_msg'=> 'User exists or invalid details',
                  'reponse_body' => "null"
                  ], 401);
        }
        else{
          DB::table('userdetails')->insert([
            'firstName'   =>  $firstName,
            'lastName'    =>  $lastName,
            'email'       =>  $request->email,
            'id'          =>  $done
          ]);
          $user = User::where('email',$request->email)->first();
          $data = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
                  ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                  ->leftJoin('cities','userdetails.city','=','cities._city_id')
                  ->select('userdetails.*', DB::raw('TIMESTAMPDIFF(YEAR, userdetails.dob, CURRENT_DATE) AS user_age'), 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                  ->where('userdetails.id', $user->id)
                  ->first();
          /*$counts = DB::select("SELECT (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS matched_count,
        (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
        (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count");*/
        $counts = DB::select("SELECT 
        (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
        (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_personal_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_dating_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_adoption_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_travel_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_military_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_school_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_career_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_pets_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_lost_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_doppel_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=11 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS username_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=11 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_username_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=12 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS activity_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=12 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_activity_count,

          (SELECT SUM(personal_count + dating_count + adoption_count + travel_count + military_count + school_count + career_count + pets_count + lost_count + doppel_count + username_count + activity_count)) AS matched_count
        ");
          /*$user->counts = $counts;*/
          $premium_user = DB::select("SELECT IF((SELECT COUNT(id) FROM chosen_plans 
  WHERE cp_user_id='".$user->id."'
  AND cp_is_expired=0 
  AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_premium,
  IF((SELECT COUNT(id) FROM chosen_plans 
                                    WHERE cp_user_id='".$user->id."' AND cp_plan_id=3 AND cp_is_expired=0 
                                AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_lifetime");
          $newData = [];
          if($data && $user){
            $newData = array_merge($data->toArray(),$user->toArray());
            $newData['counts'] = $counts;
            $newData['user_is_premium'] = $premium_user[0]->is_premium ? $premium_user[0]->is_premium : 0;
            $newData['user_is_lifetime'] = $premium_user[0]->is_lifetime ? $premium_user[0]->is_lifetime : 0;
          }
          /*$user->user_is_premium = $premium_user[0]->is_premium ? $premium_user[0]->is_premium : 0;*/
          return $response = [
            'response_code'=>'200',
            'response_msg'=> 'login successful',
            'reponse_body' => $newData,
            'redirect'    =>  url('home')
          ];
          /*return response([
            'response_code'=>'202',
            'response_msg'=> 'details are incomplete',
            'reponse_body' => $user,
            'redirect'      =>  url('signup-details')
          ], 202);*/
        }
      }
    }
  }

  public function getOTP(Request $request){
    $rules =[
              'username'       =>  'required'
            ];
    $validate = Validator::make($request->all(),$rules);
    if($validate->fails()){
      return response([
                        'response_code' =>  '201',
                        'response_msg'  =>  'Not enough details are provided',
                        'reponse_body'  =>  "null",
                        'errors'        =>  $validate->errors()
                      ], 401);
    }else{
      $dat = DB::table("users")
                ->where('email',$request->username)
                ->orWhere('username',$request->username)
                ->orWhere('phone',$request->username)
                ->first();
      if($dat){
        $rand = rand(100000,900000);
        $content = $rand.' is your one time password for WhatsCommon login verification';
        $data = [
                'subject'   =>  'WhatsCommon Login OTP',
                'name'    =>  'WhatsCommon',
                'email'   =>  $dat->email,
                'content'   =>  $content
            ];
        User::where('id',$dat->id)->update(['remember_token'=>$rand]);
        Mail::send('otppage', $data, function($message) use ($data) {
            $message->to($data['email'])->subject($data['subject']);
        });
        if(!empty($dat->isd_code) && !empty($dat->phone)){
          $to = $dat->isd_code.$dat->phone;
          /*$text = $rand.' is your one time password for WhatsCommon login verification';*/
          sendSms($to,$content);
        }
          // Mail::to($request->email)->send(new OrderShipped());
          /*$request->session()->put('id', $dat->id);*/
        $user = User::find($dat->id);
        return response([
                      'response_code' =>  '200',
                      'response_msg'  =>  'an otp has been sent',
                      /*'redirect'    =>  url('reset-password'),*/
                      'reponse_body'  =>  ['otp' => $rand]],200);
      }
      return response([
                        'response_code' =>  '203',
                        'response_msg'  =>  'There are no such record exists with this data',
                        'reponse_body'  =>  "null"
                      ], 401);
    }
  }

  public function otpBasedLogin(Request $request){
    $CheckHeader =$this->CheckRequestHeader($request);

    if(!$CheckHeader){
      return response([
        'response_code'=>'401',
        'response_msg'=> 'header missing or invalid',
        'reponse_body' => "null"
      ], 401);
    }

    $passreq = (isset($request->login_type) && $request->login_type=='normal') ? 'required' : '';
    $otpreq  = (isset($request->login_type) && $request->login_type=='otp') ? 'required' : '';
    $rules =[
              'email'       =>  'required',
              'login_type'  =>  'required',
              'password'    =>  $passreq,
              'otp'         =>  $otpreq
            ];
    $validate = Validator::make($request->all(),$rules);
    if($validate->fails()){
      return response([
                        'response_code' =>  '201',
                        'response_msg'  =>  'Not enough details are provided',
                        'reponse_body'  =>  "null",
                        'errors'    =>  $validate->errors()
                      ], 401);
    }else{

      trim($request->email,' ');
      $user= User::where('email', $request->email)
                  ->orWhere('username',$request->email)
                  ->orWhere('phone',$request->email)
                  ->first();

        
      if($request->login_type=='normal'){
        if (!$user || !Hash::check($request->password, $user->password)) {
          //print_r($user);exit;
          return response([
            'response_code'=>'201',
            'response_msg'=> 'Invalid details',
            'reponse_body' => "null"
          ], 401);
        }
      }else{
        if (!$user || $user->remember_token!=$request->otp) {
          return response([
            'response_code'=>'201',
            'response_msg'=> 'Invalid details',
            'reponse_body' => "null"
          ], 401);
        }
      }

      if($user->user_is_active==0){
        return response([
          'response_code'=>'201',
          'response_msg'=> 'You are not authorized to access. Please contact admin for login.',
          'reponse_body' => "null"
        ], 401);
      }

      User::where('id',$user->id)->update(['remember_token'=>NULL]);

      $data = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
                ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                ->leftJoin('cities','userdetails.city','=','cities._city_id')
                ->select('userdetails.*', DB::raw('TIMESTAMPDIFF(YEAR, userdetails.dob, CURRENT_DATE) AS user_age'), 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                ->where('userdetails.id', $user->id)
                ->first();

      /*$counts = DB::select("SELECT (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS matched_count,
            (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
            (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
            (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count");*/
          $counts = DB::select("SELECT 
        (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
        (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_personal_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_dating_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_adoption_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_travel_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_military_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_school_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_career_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_pets_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_lost_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_doppel_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=11 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS username_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=11 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_username_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=12 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS activity_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=12 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_activity_count,

          (SELECT SUM(personal_count + dating_count + adoption_count + travel_count + military_count + school_count + career_count + pets_count + lost_count + doppel_count + username_count + activity_count)) AS matched_count
        ");


$premium_user = DB::select("SELECT IF((SELECT COUNT(id) FROM chosen_plans 
  WHERE cp_user_id='".$user->id."'
  AND cp_is_expired=0 
  AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_premium,
  IF((SELECT COUNT(id) FROM chosen_plans 
                                    WHERE cp_user_id='".$user->id."' AND cp_plan_id=3 AND cp_is_expired=0 
                                AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_lifetime");

        /*$data_user = User::where('id',29)->first();*/
      if($data && $user)
        $newData = array_merge($data->toArray(),$user->toArray());
      $newData['counts'] = $counts;
      $newData['user_is_premium'] = $premium_user[0]->is_premium ? $premium_user[0]->is_premium : 0;
      $newData['user_is_lifetime'] = $premium_user[0]->is_lifetime ? $premium_user[0]->is_lifetime : 0;
      
      
      $response = [
        'response_code'=>'200',
        'response_msg'=> 'login successful',
        'reponse_body' => $newData,
        'redirect'    =>  url('home')
      ];

      return response($response, 200);
    }
  }

    function getUsers(Request $request){

      $is_user_name = false;
      $CheckHeader =$this->CheckRequestHeader($request);

      if(!$CheckHeader){
        return response([
          'response_code'=>'401',
          'response_msg'=> 'header missing or invalid',
          'reponse_body' => "null"
        ], 401);
      }
      trim($request->email,' ');
      $user= User::where('email', $request->email)->first();
      /*$user= User::where('id',29)->first();*/

      if(!$user){
        $user = User::where('username', $request->email)->first();
        $is_user_name = true;
      }

            // print_r($data);
      if (!$user || !Hash::check($request->password, $user->password)) {
        return response([
          'response_code'=>'201',
          'response_msg'=> 'Invalid details',
          'reponse_body' => "null"
        ], 401);
      }

      if($user->user_is_active==0){
        return response([
          'response_code'=>'201',
          'response_msg'=> 'You are not authorized to access. Please contact admin for login.',
          'reponse_body' => "null"
        ], 401);
      }

            //$token = $user->createToken('my-app-token')->plainTextToken;
           //$data = Userdetail::where('email', $request->email)->first();
            /*$data = Userdetail::where('id', $user->id)->first();*/
      $data = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
                ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                ->leftJoin('cities','userdetails.city','=','cities._city_id')
                ->select('userdetails.*', DB::raw('TIMESTAMPDIFF(YEAR, userdetails.dob, CURRENT_DATE) AS user_age'), 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                ->where('userdetails.id', $user->id)
                ->first();
      /*$counts = DB::select("SELECT (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS matched_count,
(SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
(SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count");*/
$counts = DB::select("SELECT 
        (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
        (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_personal_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_dating_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_adoption_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_travel_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_military_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_school_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_career_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_pets_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_lost_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_doppel_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=11 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS username_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=11 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_username_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=12 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS activity_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=12 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_activity_count,

          (SELECT SUM(personal_count + dating_count + adoption_count + travel_count + military_count + school_count + career_count + pets_count + lost_count + doppel_count + username_count + activity_count)) AS matched_count
        ");

      $premium_user = DB::select("SELECT IF((SELECT COUNT(id) FROM chosen_plans 
  WHERE cp_user_id='".$user->id."'
  AND cp_is_expired=0 
  AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_premium,
  IF((SELECT COUNT(id) FROM chosen_plans 
                                    WHERE cp_user_id='".$user->id."' AND cp_plan_id=3 AND cp_is_expired=0 
                                AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_lifetime");
/*print_r($premium_user);exit();*/
      if(!$is_user_name){

        $data_user = User::where('email',$request->email)->first();
        /*$data_user = User::where('id',29)->first();*/
        if($data && $data_user)
        $newData = array_merge($data->toArray(),$data_user->toArray());
        $newData['counts'] = $counts;
        $newData['user_is_premium'] = $premium_user[0]->is_premium ? $premium_user[0]->is_premium : 0;
        $newData['user_is_lifetime'] = $premium_user[0]->is_lifetime ? $premium_user[0]->is_lifetime : 0;
      }
      else
      {

        $data_user = User::where('username',$request->email)->first();
        /*$data_user = User::where('id',29)->first();*/
        if($data && $data_user)
        $newData = array_merge($data->toArray(),$data_user->toArray());
        $newData['counts'] = $counts;
        $newData['user_is_premium'] = $premium_user[0]->is_premium ? $premium_user[0]->is_premium : 0;
        $newData['user_is_lifetime'] = $premium_user[0]->is_lifetime ? $premium_user[0]->is_lifetime : 0;
      }


            //$data_user['profile']
      if(!$data)
      {
        return response([
          'response_code'=>'202',
          'response_msg'=> 'details are incomplete',
          'reponse_body' => $data_user
        ], 202);
      }
      $login_from = 'web';
      if($request->login_from && $request->login_from=='web'){
        $request->session()->put('userdata', $newData);
      }else{
        if($request->device_type){
          $login_from = $request->device_type;
        }
      }

      $check = DB::table('login_details')
                    ->where('login_user_id','=',$newData['id'])
                    ->where('login_from','=',$login_from)
                    ->where('login_date','=',date('Y-m-d'))
                    ->first();
      if(!$check){
        DB::table('login_details')->insert([
          'login_user_id'     =>  $newData['id'],
          'login_date'        =>  date('Y-m-d'),
          'login_from'        =>  $login_from
        ]);
      }else{
        DB::table('login_details')->where('_login_id',$check->_login_id)->update(['login_time' => date('Y-m-d H:i:s')]);
      }

      /*DB::table('login_details')->insert([
            'login_user_id'     =>  $newData['id'],
            'login_date'        =>  date('Y-m-d'),
            'login_from'        =>  $login_from
          ]);*/

      $response = [
        'response_code'=>'200',
        'response_msg'=> 'login successful',
        'reponse_body' => $newData,
        'redirect'    =>  url('home')
      ];

      return response($response, 200);

    }

  function setQuickBloxId(Request $request){
    $rules =[
              'user_id'       =>  'required',
              'quick_blox_id' =>  'required|unique:users,quick_blox_id'
            ];
    $validate = Validator::make($request->all(),$rules);
    if($validate->fails()){
      return response([
                        'response_code' =>  '201',
                        'response_msg'  =>  'Not enough details are provided',
                        'reponse_body'  =>  "null",
                        'errors'    =>  $validate->errors()
                      ], 401);
    }else{
      User::where('id',$request->user_id)->update(["quick_blox_id"=>$request->quick_blox_id]);
      $data = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
                ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                ->leftJoin('cities','userdetails.city','=','cities._city_id')
                ->select('userdetails.*', DB::raw('TIMESTAMPDIFF(YEAR, userdetails.dob, CURRENT_DATE) AS user_age'), 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                ->where('userdetails.id', $request->user_id)
                ->first();
      $user = User::find($request->user_id);
      /*$counts = DB::select("SELECT (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS matched_count,
(SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$request->user_id."') AS life_events_count,
(SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$request->user_id."' OR request_to_user='".$request->user_id."')) AS connection_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=1 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS personal_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=2 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS dating_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=3 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS adoption_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=4 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS travel_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=5 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS military_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=6 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS school_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=7 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS career_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=8 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS pets_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=9 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS lost_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=10 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS doppel_count");*/
$counts = DB::select("SELECT 
        (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
        (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_personal_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_dating_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_adoption_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_travel_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_military_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_school_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_career_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_pets_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_lost_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_doppel_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=11 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS username_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=11 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_username_count,

          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=12 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS activity_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=12 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS total_activity_count,

          (SELECT SUM(personal_count + dating_count + adoption_count + travel_count + military_count + school_count + career_count + pets_count + lost_count + doppel_count + username_count + activity_count)) AS matched_count
        ");

      
      $newData = array_merge($data->toArray(),$user->toArray());
      $newData['counts'] = $counts;
      $premium_user = DB::select("SELECT IF((SELECT COUNT(id) FROM chosen_plans 
  WHERE cp_user_id='".$request->user_id."'
  AND cp_is_expired=0 
  AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_premium,
  IF((SELECT COUNT(id) FROM chosen_plans 
                                    WHERE cp_user_id='".$request->user_id."' AND cp_plan_id=3 AND cp_is_expired=0 
                                AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_lifetime");
      $newData['user_is_premium'] = $premium_user[0]->is_premium ? $premium_user[0]->is_premium : 0;
      $newData['user_is_lifetime'] = $premium_user[0]->is_lifetime ? $premium_user[0]->is_lifetime : 0;
      $response = [
                    'response_code'=>'200',
                    'response_msg'=> 'Update successful',
                    'reponse_body' => $newData,
                    'redirect'    =>  url('home')
                  ];

      return response($response, 200);
    }
  }

  function setToken(Request $request){
    $rules =[
              'user_id'             =>  'required',
              'login_device_id'     =>  'required',
              'login_device_type'   =>  'required|in:android,ios,web'
            ];
    $validate = Validator::make($request->all(),$rules);
    if($validate->fails()){
    return response([
                        'response_code' =>  '201',
                        'response_msg'  =>  'Not enough details are provided',
                        'reponse_body'  =>  "null",
                        'errors'    =>  $validate->errors()
                    ], 401);
    }else{
      $check = DB::table('login_tokens')
                  ->where('login_user_id',$request->user_id)
                  ->where('login_device_type', $request->login_device_type)
                  ->first();
      $time = date('Y-m-d H:i:s');
      $timestamp = strtotime($time);
      $token = $request->login_device_type.'|'.$request->user_id.'|'.$request->login_device_id.'|'.$timestamp;
      if(!$check){
          DB::table('login_tokens')->insert([
                                              'login_user_id'     =>  $request->user_id,
                                              'login_device_type' =>  $request->login_device_type,
                                              'login_device_id'   =>  $request->login_device_id,
                                              'login_created'     =>  $time,
                                              'login_token'       =>  $token
                                            ]);
      }else{
          DB::table('login_tokens')->where('_login_id',$check->_login_id)->update([
                                    'login_device_id'   =>  $request->login_device_id,
                                    'login_created'     =>  $time,
                                    'login_token'       =>  $token
                                  ]);
      }
      $response = [
                      'response_code'=>'200',
                      'response_msg'=> 'Token Created',
                      'reponse_body' => ['token' => $token],
                      'redirect'    =>  url('home')
                  ];

      return response($response, 200);
    }
  }

  function refreshToken(Request $request){
    $rules =[
              'user_id'             =>  'required',
              'login_device_id'     =>  'required',
              'login_device_type'   =>  'required|in:android,ios,web',
              'login_token'         =>  'required'
            ];
    $validate = Validator::make($request->all(),$rules);
    if($validate->fails()){
      return response([
                        'response_code' =>  '201',
                        'response_msg'  =>  'Not enough details are provided',
                        'reponse_body'  =>  "null",
                        'errors'    =>  $validate->errors()
                    ], 401);
    }else{
      $check = DB::table('login_tokens')
                  ->where('login_user_id',$request->user_id)
                  ->where('login_device_type', $request->login_device_type)
                  ->first();
      $time = date('Y-m-d H:i:s');
      $timestamp = strtotime($time);
      $token = $request->login_device_type.'|'.$request->user_id.'|'.$request->login_device_id.'|'.$timestamp;
      if(!$check){
          DB::table('login_tokens')->insert([
                                              'login_user_id'     =>  $request->user_id,
                                              'login_device_type' =>  $request->login_device_type,
                                              'login_device_id'   =>  $request->login_device_id,
                                              'login_created'     =>  $time,
                                              'login_token'       =>  $token
                                            ]);
      }else{
          DB::table('login_tokens')->where('_login_id',$check->_login_id)->update([
                                    'login_device_id'   =>  $request->login_device_id,
                                    'login_created'     =>  $time,
                                    'login_token'       =>  $token
                                  ]);
      }
      $response = [
                      'response_code'=>'200',
                      'response_msg'=> 'Update successful',
                      'reponse_body' => ['token' => $token],
                      'redirect'    =>  url('home')
                  ];

      return response($response, 200);
    }
  }

    function setDeviceToken(Request $request){
        $rules =[
                'user_id'       =>  'required',
                'device_token'  =>  'required',
                'device_type'   =>  'required|in:android,ios'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
        return response([
                            'response_code' =>  '201',
                            'response_msg'  =>  'Not enough details are provided',
                            'reponse_body'  =>  "null",
                            'errors'    =>  $validate->errors()
                        ], 401);
        }else{
            $check = DB::table('device_tokens')
                        ->where('device_user_id',$request->user_id)
                        ->where('device_type', $request->device_type)
                        ->first();
            /*$time = date('Y-m-d H:i:s');
            $timestamp = strtotime($time);
            $token = $request->device_type.'|'.$request->user_id.'|'.$request->device_token.'|'.$timestamp;*/
            if(!$check){
                DB::table('device_tokens')->insert([
                                                        'device_user_id'    =>  $request->user_id,
                                                        'device_type'       =>  $request->device_type,
                                                        'device_token'      =>  $request->device_token,
                                                        /*'token_created'     =>  $time,
                                                        'login_token'       =>  $token*/
                                                    ]);
            }else{
                DB::table('device_tokens')->where('_device_id',$check->_device_id)->update([
                                          'device_token'      =>  $request->device_token,
                                          /*'token_created'     =>  $time,
                                          'login_token'       =>  $token*/
                                        ]);
            }
            $response = [
                            'response_code'=>'200',
                            'response_msg'=> 'Update successful',
                            'reponse_body' => NULL,
                            'redirect'    =>  url('home')
                        ];

            return response($response, 200);
        }
    }

    function changeProfilePicture(Request $request){
      $rules =[
                'user_picture' =>   'required|mimes:jpg,jpeg,png|max:2048',
                'user_id'       =>  'required',
              ];
      $validate = Validator::make($request->all(),$rules);
      if($validate->fails()){

        return response([
                          'response_code' =>  '201',
                          'response_msg'  =>  'Not enough details are provided',
                          'reponse_body'  =>  "null",
                          'errors'    =>  $validate->errors()
                        ], 401);

      }else{
        $user_id = $request->user_id;
        $fileName = time().'_'.$request->user_picture->getClientOriginalName();
              /*$filePath = $request->file('user_picture')->storeAs('profilePictures', $fileName, 'public');*/
        $request->user_picture->move(public_path('profilePictures'), $fileName);
        $profUrl = url('/profilePictures/'.$fileName);
        $dbUpdate = User::where('id',$user_id)->update(["profile_photo_path"=>$profUrl]);
        if(!$dbUpdate)
        {
             return response([
                'response_code'=>'401',
             'response_msg'=> 'update to db failed',
             'reponse_body' => "null"
             ], 401);
        }

        return response([
        'response_code'=>'200',
                  'response_msg'=> 'profile picture updated',
                  'reponse_body' => ['img_src'=>$profUrl]
        ]);
      }
    }

    function updateImage(Request $request)
    {
             $CheckHeader =$this->CheckRequestHeader($request);

             if(!$CheckHeader)
             {
                 return response([
                     'response_code'=>'401',
                 'response_msg'=> 'header missing or invalid',
                 'reponse_body' => "null"
                 ], 401);
             }
             $checkFirst = User::where('username',$request->username)->first();

             if(!$checkFirst)
             {
                return response([
                     'response_code'=>'401',
                 'response_msg'=> 'Invalid username',
                 'reponse_body' => "null"
                 ], 401);
             }

        $updateImage = $this->getProfImage($request);
        //

        if($updateImage)
        {
            $profUrl = url('/profilePictures/'.$request->username.'.png?'.rand(10000000,900000000));
            $dbUpdate = User::where('username',$request->username)->update(["profile_photo_path"=>$profUrl]);
            if(!$dbUpdate)
            {
                 return response([
                     'response_code'=>'401',
                 'response_msg'=> 'update to db failed',
                 'reponse_body' => "null"
                 ], 401);
            }

            return response([
            'response_code'=>'200',
                      'response_msg'=> 'profile picture updated',
                      'reponse_body' => $profUrl
            ]);
        }
        return response([
                     'response_code'=>'401',
                 'response_msg'=> 'image update failed',
                 'reponse_body' => "null"
                 ], 401);
    }

    function updateProfImage(Request $request){
      $user_id = $request->id;
      if(!empty($request->imageencoded)){
        $fileName = time().'_'.$request->imageencoded->getClientOriginalName();
        /*$filePath = $request->file('user_picture')->storeAs('profilePictures', $fileName, 'public');*/
        $request->imageencoded->move(public_path('profilePictures'), $fileName);
        $profUrl = url('/profilePictures/'.$fileName);
        $dbUpdate = User::where('id',$user_id)->update(["profile_photo_path"=>$profUrl]);
        if(!$dbUpdate)
        {
          return false;
        }else{
          return true;
        }
      }
      return false;
    }

    function getProfImage(Request $request)
    {
        $base64Encoded = $request->imageencoded;
        if(!empty($base64Encoded))
            {
                $usrName = $request->username;
              $poc =  \Image::make($base64Encoded)->save(public_path('profilePictures/'.$usrName.'.png'));
              if($poc)
              {
               return true;
               }
               return false;
            }

            return false;
    }
    function registerUser(Request $request)
    {
        // if(count($request->toArray()) <3 || count($request->toArray()) > 3)
        // {
        //     return response([
        //                  'response_code'=>'201',
        //              'response_msg'=> 'Invalid number of params',
        //              'reponse_body' => "null"
        //              ], 401);
        // }

       // die("first name is :".$request->firstName);


         $CheckHeader =$this->CheckRequestHeader($request);

             if(!$CheckHeader)
             {
                 return response([
                     'response_code'=>'401',
                 'response_msg'=> 'header missing or invalid',
                 'reponse_body' => "null"
                 ], 401);
             }

        $Existing = User::where('email',$request->email)->first();
        // return response([
        //             'response_code'=>'200',
        //         'response_msg'=> $request->fname,
        //         'reponse_body' => "null"
        //         ], 200);

        if($Existing)
        {
             return response([
                          'response_code'=>'201',
                      'response_msg'=> 'email exists already',
                      'reponse_body' => "null"
                      ]);
        }
        $Existing = User::where('username',$request->username)->first();
        if($Existing)
        {
            return response([
                          'response_code'=>'201',
                      'response_msg'=> 'username exists already',
                      'reponse_body' => "null"
                      ], 401);
        }

        //Saving profile image if exists
        $imageSavedStat = $this->getProfImage($request);

        $profUrl = url('/profilePictures/'.$request->username.'.png');//.rand(10000000,900000000)

        if(!$imageSavedStat)
        {
            $profUrl = url('/profilePictures/default.png');
        }

            //die("first name is :".$request->name);
            $dat = explode('@',$request->name);

            $done =  User::insertGetId([
          'firstName' => $dat[0],
          'lastName' => $dat[1],
          'email' => $request->email,
          'password' => Hash::make($request->password),
          'username' => $request->username,
          'isd_code'   => $request->isd_code,
          'phone'   => $request->phone,
          'mi'      => $request->mi,
          'profile_photo_path' => $profUrl
              ]);

            if(!$done)
            {
                return response([
                          'response_code'=>'201',
                      'response_msg'=> 'User exists or invalid details',
                      'reponse_body' => "null"
                      ], 401);
            }
            else
            {
              DB::table('userdetails')->insert([
                'firstName'   =>  $dat[0],
                'lastName'    =>  $dat[1],
                'email'       =>  $request->email,
                'id'          =>  $done,
                'isd_code'    =>  $request->isd_code ? $request->isd_code : NULL,
                'phone'       =>  $request->phone ? $request->phone : NULL,
                'mi'          =>  $request->mi ? $request->mi : NULL
              ]);
              $data = User::where('email',$request->email)->first();
               // $data['profile_photo_path'] = url('/profilePictures/'.$request->username.'.png');
                return response([
                          'response_code'=>'200',
                      'response_msg'=> 'User is created',
                      'reponse_body' => $data
                      ], 200);
            }


            return response([
                          'response_code'=>'201',
                      'response_msg'=> 'User exists or invalid details',
                      'reponse_body' => "null"
                      ], 401);

    }

    function registerDetails(Request $request)
    {
      $CheckHeader =$this->CheckRequestHeader($request);

      if(!$CheckHeader){
        return response([
                          'response_code' =>  '401',
                          'response_msg'  =>  'header missing or invalid',
                          'reponse_body'  =>  "null"
                        ], 401);
      }
      $Existing = Userdetail::where('email',$request->email)->first();

      if($Existing){
        /*return response([
                         'response_code'=>'201',
                     'response_msg'=> 'User exists already',
                     'reponse_body' => "null"
                     ], 401);*/
        $dat = explode('@',$request->name);
        $dob = NULL;
        if(!$request->dob && $request->dob_day && $request->dob_month && $request->dob_year){
          $dob = date('Y-m-d',strtotime($request->dob_year.'-'.$request->dob_month.'-'.$request->dob_day));
        }else if(!$request->dob && $request->dob_year && ($request->dob_day || $request->dob_month)){
          if($request->dob_day){
            $dob = date('Y-m-d',strtotime($request->dob_year.'-01-'.$request->dob_day));
          }else{
            $dob = date('Y-m-d',strtotime($request->dob_year.'-'.$request->dob_month.'-01'));
          }
        }else if(!$request->dob && $request->dob_year && !$request->dob_day && !$request->dob_month){
          $dob = date('Y-m-d',strtotime($request->dob_year.'-01-01'));
        }
        else if($request->dob && !empty($request->dob)){
          $dob = $request->dob;
        }
        DB::table('userdetails')->where('email',$request->email)->update([
          'firstName'   =>  $dat[0],
          'lastName'    =>  $dat[1],
          'email'       =>  $request->email,
          'dob'         =>  $dob,
          'dob_day'     =>  $request->dob_day ? $request->dob_day : NULL,
          'dob_month'   =>  $request->dob_month ? $request->dob_month : NULL,
          'dob_year'    =>  $request->dob_year  ? $request->dob_year : NULL,
          'isd_code'    =>  $request->isd_code ? $request->isd_code : NULL,
          'phone'       =>  $request->phone ? $request->phone : NULL,
          'country'     =>  $request->country ? $request->country : NULL,
          'state'       =>  $request->state ? $request->state : NULL,
          'city'        =>  $request->city ? $request->city : NULL,
          'province'    =>  $request->province ? $request->province : NULL,
          'street'      =>  $request->street ? $request->street : NULL,
          'zip'         =>  $request->zip ? $request->zip : NULL,
          'mi'          =>  $request->mi ? $request->mi : NULL
        ]);
        $totalData = Userdetail::where('email',$request->email) -> first();
        $loginData = User::where('email',$request->email)->first();
        $newData = array_merge($totalData->toArray(),$loginData->toArray());
        return response([
                          'response_code' =>  '200',
                          'response_msg'  =>  'User details updated',
                          'reponse_body'  =>  $newData
                        ], 200);
      }

      $dat = explode('@',$request->name);
      $dob = NULL;
      if(!$request->dob && $request->dob_day && $request->dob_month && $request->dob_year){
        $dob = date('Y-m-d',strtotime($request->dob_year.'-'.$request->dob_month.'-'.$request->dob_day));
      }else if(!$request->dob && $request->dob_year && ($request->dob_day || $request->dob_month)){
        if($request->dob_day){
          $dob = date('Y-m-d',strtotime($request->dob_year.'-01-'.$request->dob_day));
        }else{
          $dob = date('Y-m-d',strtotime($request->dob_year.'-'.$request->dob_month.'-01'));
        }
      }else if(!$request->dob && $request->dob_year && !$request->dob_day && !$request->dob_month){
        $dob = date('Y-m-d',strtotime($request->dob_year.'-01-01'));
      }
      else if($request->dob && !empty($request->dob)){
        $dob = $request->dob;
      }
      $done =  DB::table('userdetails')->insert([
        'firstName'   =>  $dat[0],
        'lastName'    =>  $dat[1],
        'email'       =>  $request->email,
        'dob'         =>  $dob,
        'dob_day'     =>  $request->dob_day ? $request->dob_day : NULL,
        'dob_month'   =>  $request->dob_month ? $request->dob_month : NULL,
        'dob_year'    =>  $request->dob_year  ? $request->dob_year : NULL,
        'id'          =>  $request->id,
        'isd_code'    =>  $request->isd_code ? $request->isd_code : NULL,
        'phone'       =>  $request->phone ? $request->phone : NULL,
        'country'     =>  $request->country ? $request->country : NULL,
        'state'       =>  $request->state ? $request->state : NULL,
        'city'        =>  $request->city ? $request->city : NULL,
        'province'    =>  $request->province ? $request->province : NULL,
        'street'      =>  $request->street ? $request->street : NULL,
        'zip'         =>  $request->zip ? $request->zip : NULL,
        'mi'          =>  $request->mi ? $request->mi : NULL
      ]);

      if(!$done){
        return response([
                          'response_code' =>  '201',
                          'response_msg'  =>  'User exists or invalid details',
                          'reponse_body'  =>  "null"
                ], 401);
      }
      else
      {
        $totalData = Userdetail::where('email',$request->email) -> first();
        $loginData = User::where('email',$request->email)->first();
        $newData = array_merge($totalData->toArray(),$loginData->toArray());
        return response([
                          'response_code' =>  '200',
                          'response_msg'  =>  'User details inserted',
                          'reponse_body'  =>  $newData
                ], 200);
      }

      return response([
                        'response_code' =>  '201',
                        'response_msg'  =>  'User exists or invalid details',
                        'reponse_body'  =>  "null"
                ], 401);

    }
    function checkVarifiedEmail(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);

            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }

        if(empty($request->email))
        {
            return response([
                          'response_code'=>'201',
                      'response_msg'=> 'Field is blank',
                      'reponse_body' => "null"
                      ], 401);
        }

        $dat = DB::table("users")->where('email',$request->email)->first();

        if($dat){ 
          $rand = rand(100000,900000);
          $content = $rand.' is your one time password for WhatsCommon forgot password';
          $data = [
                    'subject' =>  'OTP GENERATED',
                    'name'    =>  'Whats common',
                    'email'   =>  $request->email,
                    'content' =>  $content
                  ];
          User::where('email',$request->email)->update(['remember_token'=>$rand]);
          Mail::send('otppage', $data, function($message) use ($data) {
            $message->to($data['email'])
            ->subject($data['subject']);
          });

          if(!empty($dat->isd_code) && !empty($dat->phone)){
            $to = $dat->isd_code.$dat->phone;
            /*$text = $rand.' is your one time password for WhatsCommon login verification';*/
            sendSms($to,$content);
          }
            // Mail::to($request->email)->send(new OrderShipped());
          return response([
                'response_code'=>'200',
                'response_msg'=> 'an otp has been sent',
                'reponse_body' =>$dat],200);
        }

        return response([
                         'response_code'=>'203',
                     'response_msg'=> 'There are no such record exists with this email',
                     'reponse_body' => "null"
                     ], 401);
    }
    function updateProfile(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);

            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }
        try{
          $request->validate([
            'email'=>'required',
            'id'=>'required',
            'name'=>'required',
            /*'dob'=>'required',*/
            //'isd_code'=>'required',
            'phone'=>'required',
            'bio'=>'nullable',
            /*'country'=>'required',
            'state'=>'required',
            'city'=>'required',
            'province'=>'required',
            'street'=>'required',
            'zip'=>'required',*/
            /*'mi'=>'required',*/
          ]);
        }
        catch(Exception $exception)
        {
          return response([
                            'response_code' =>  '204',
                            'response_msg'  =>  'Not enough details are provided',
                            'reponse_body'  =>  "null"
                          ], 401);
        }


        $totalData = Userdetail::where('id',$request->id) -> first();

        $dat = explode('@',$request->name);

        if($totalData)
        {
          $dob = NULL;
          if(!$request->dob && $request->dob_day && $request->dob_month && $request->dob_year){
            $dob = date('Y-m-d',strtotime($request->dob_year.'-'.$request->dob_month.'-'.$request->dob_day));
          }else if(!$request->dob && $request->dob_year && ($request->dob_day || $request->dob_month)){
            if($request->dob_day){
              $dob = date('Y-m-d',strtotime($request->dob_year.'-01-'.$request->dob_day));
            }else{
              $dob = date('Y-m-d',strtotime($request->dob_year.'-'.$request->dob_month.'-01'));
            }
          }else if(!$request->dob && $request->dob_year && !$request->dob_day && !$request->dob_month){
            $dob = date('Y-m-d',strtotime($request->dob_year.'-01-01'));
          }
          else if($request->dob && !empty($request->dob)){
            $dob = $request->dob;
          }
          DB::table('userdetails')->where('id',$request->id)->update([
            'email'       =>  $request->email,
            'firstName'   =>  $dat[0],
            'lastName'    =>  $dat[1],
            'dob'         =>  $dob,
            'dob_day'     =>  $request->dob_day ? $request->dob_day : NULL,
            'dob_month'   =>  $request->dob_month ? $request->dob_month : NULL,
            'dob_year'    =>  $request->dob_year  ? $request->dob_year : NULL,
            'isd_code'    =>  $request->isd_code ? $request->isd_code : NULL,
            'phone'       =>  $request->phone ? $request->phone : NULL,
            'country'     =>  $request->country ? $request->country : NULL,
            'state'       =>  $request->state ? $request->state : NULL,
            'city'        =>  $request->city ? $request->city : NULL,
            'province'    =>  $request->province ? $request->province : NULL,
            'street'      =>  $request->street ? $request->street : NULL,
            'zip'         =>  $request->zip ? $request->zip : NULL,
            'mi'          =>  $request->mi ? $request->mi : NULL,
            'bio'         =>  $request->bio ? $request->bio : NULL
          ]);


            /*$imageSavedStat = $this->getProfImage($request);*/
            $imageSavedStat = $this->updateProfImage($request);
            User::where('id',$request->id)->update(['phone'=>$request->phone,'firstName'=>$dat[0],'lastName'=>$dat[1],'mi'=>$request->mi]);
            /*$profUrl = url('/profilePictures/'.$request->username.'.png?'.rand(10000000,900000000));*/
            /*if($imageSavedStat)
            {
            
            User::where('id',$request->id)->update(['phone'=>$request->phone,'firstName'=>$dat[0],'lastName'=>$dat[1],'mi'=>$request->mi,'profile_photo_path'=>$profUrl]);
            }
            else
            {
                User::where('id',$request->id)->update(['phone'=>$request->phone,'firstName'=>$dat[0],'lastName'=>$dat[1],'mi'=>$request->mi]);
            }*/
        //$totalData = Userdetail::where('id',$request->id) -> first();
        $totalData = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
                ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                ->leftJoin('cities','userdetails.city','=','cities._city_id')
                ->select('userdetails.*', DB::raw('TIMESTAMPDIFF(YEAR, userdetails.dob, CURRENT_DATE) AS user_age'), 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                ->where('userdetails.id', $request->id)
                ->first();
            $user   = User::where('email',$request->email) -> first();
            $newData = array_merge($user->toArray(),$totalData->toArray());
        /*$counts = DB::select("SELECT (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->id."') AND (feed_user1 = '".$request->id."' OR feed_user2 = '".$request->id."')) AS matched_count,
(SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$request->id."') AS life_events_count,
(SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$request->id."' OR request_to_user='".$request->id."')) AS connection_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$request->id."' OR feed_user2 = '".$request->id."')) AS personal_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$request->id."' OR feed_user2 = '".$request->id."')) AS dating_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$request->id."' OR feed_user2 = '".$request->id."')) AS adoption_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$request->id."' OR feed_user2 = '".$request->id."')) AS travel_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$request->id."' OR feed_user2 = '".$request->id."')) AS military_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$request->id."' OR feed_user2 = '".$request->id."')) AS school_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$request->id."' OR feed_user2 = '".$request->id."')) AS career_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$request->id."' OR feed_user2 = '".$request->id."')) AS pets_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$request->id."' OR feed_user2 = '".$request->id."')) AS lost_count,
(SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$request->id."' OR feed_user2 = '".$request->id."')) AS doppel_count");*/
$counts = DB::select("SELECT 
        (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user->id."') AS life_events_count,
        (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user->id."' OR request_to_user='".$user->id."')) AS connection_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=1 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS personal_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS dating_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=3 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS adoption_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=4 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS travel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=5 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS military_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=6 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS school_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=7 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS career_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=8 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS pets_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=9 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS lost_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user->id."') AND feed_lifeevent=10 AND (feed_user1 = '".$user->id."' OR feed_user2 = '".$user->id."')) AS doppel_count,
        (SELECT SUM(personal_count + dating_count + adoption_count + travel_count + military_count + school_count + career_count + pets_count + lost_count + doppel_count)) AS matched_count
        ");

      $premium_user = DB::select("SELECT IF((SELECT COUNT(id) FROM chosen_plans 
  WHERE cp_user_id='".$request->id."'
  AND cp_is_expired=0 
  AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_premium,
  IF((SELECT COUNT(id) FROM chosen_plans 
                                    WHERE cp_user_id='".$request->id."' AND cp_plan_id=3 AND cp_is_expired=0 
                                AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_lifetime");
      $newData['counts'] = $counts;
      $newData['user_is_premium'] = $premium_user[0]->is_premium ? $premium_user[0]->is_premium : 0;
      $newData['user_is_lifetime'] = $premium_user[0]->is_lifetime ? $premium_user[0]->is_lifetime : 0;
        //if(!$imageSavedStat)
        //{
        //    $profUrl = url('/profilePictures/default.png');
        //}
            return response([
                             'response_code'=>'200',
                         'response_msg'=> 'data updated successfully',
                         'reponse_body' => $newData
                         ], 200);
        }
        else
        {
            return response([
                             'response_code'=>'203',
                         'response_msg'=> 'There are no such record exists with this email',
                         'reponse_body' => "null"
                         ], 401);
        }
    }

    function ChnagePassword(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);

            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }
        $request->validate([
            'old_password'=>'required',
            'new_password'=>'required',
            'id'          =>'required'
            ]);

        //DB::table('users')->select('password')->where('id',$request->id)
          $pass =  User::where('id',$request->id)->first();
        //return response([
        //    'body'=>$pass
        //    ]);

        if($pass)
        {
            if(Hash::check($request->old_password,$pass->password))
            {
                User::where('id',$request->id)->update([
                    'password'=>Hash::make($request->new_password)
                    ]);
                $pass =  User::where('id',$request->id)->first();
                return response([
                    'response_code'=>'200',
                         'response_msg'=> 'password updated successfully',
                         'reponse_body' => $pass
                         ], 200);
               // $this->getUsers($request);
            }
            return response([
                    'response_code'=>'203',
                         'response_msg'=> 'Current password is wrong',
                         'reponse_body' => 'null'
                         ], 401);
        }
        return response([
                    'response_code'=>'203',
                         'response_msg'=> 'no such record',
                         'reponse_body' => 'null'
                         ], 401);
    }
    function verifyOtp(Request $request)
    {
        //$CheckHeader =$this->CheckRequestHeader($request);

        //    if(!$CheckHeader)
        //    {
        //        return response([
        //            'response_code'=>'401',
        //        'response_msg'=> 'header missing or invalid',
        //        'reponse_body' => "null"
        //        ], 401);
        //    }
        //$request->validate([
        //    'otp'=>'required',
        //    'id'=>'required'
        //    ]);

            $dat = User::where('id',$request->id)->first();
            if($dat)
            {
                if($request->otp == $dat->remember_token)
                {
                    User::where('id',$request->id)->update(['remember_token'=>null]);
                    return true;
                }
                return false;
            }
            return false;

    }
    function newPassword(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);

            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }

            $validateOtp = $this->verifyOtp($request);

            if(!$validateOtp)
            {
                 return response(['response_code'=>'401',
                         'response_msg'=> 'Invalid otp or id',
                         'reponse_body' => 'null'],401);
            }
        if(!empty($request->new_password) && !empty($request->id))
        {
            User::where('id',$request->id)->update(['password'=>Hash::make($request->new_password)]);
            return response(['response_code'=>'200',
                         'response_msg'=> 'new password is set',
                         'reponse_body' => 'null'],200);
        }
    }

    function UsersList(Request $request){
      /*$users = DB::table('users')
                ->join('userdetails','users.id','=','userdetails.id')
                ->leftJoin('countries','userdetails.country','=','countries._country_id')
                ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
                ->leftJoin('cities','userdetails.city','=','cities._city_id')
                ->select('users.*', 'countries.country_name', 'provinces.province_name', 'cities.city_name')
                ->get();*/
      $countries = DB::table('countries')->orderBy('country_name','ASC')->get();
      $provinces = [];
      $cities = [];
      $where = '';
      if($request->_country_id){
        $where = ' WHERE userdetails.country='.$request->_country_id;
        $provinces = DB::table('provinces')->where('province_country',$request->_country_id)->orderBy('province_name','ASC')->get();
      }
      if($request->_province_id){
        $and_where = $where=='' ? ' WHERE' : ' AND';
        $where .= $and_where.' userdetails.state='.$request->_province_id;
        $cities = DB::table('cities')->where('city_province',$request->_province_id)->orderBy('city_name','ASC')->get();
      }
      if($request->_city_id){
        $and_where = $where=='' ? ' WHERE' : ' AND';
        $where .= $and_where.' userdetails.city='.$request->_city_id;
      }
      $users = DB::select('SELECT users.*, IF((SELECT COUNT(id) FROM chosen_plans 
                                              WHERE cp_user_id=users.id  AND cp_is_expired=0 AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_premium,
                            IF((SELECT COUNT(id) FROM chosen_plans WHERE cp_user_id=users.id AND cp_plan_id=3 AND cp_is_expired=0 AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_lifetime, countries.country_name, provinces.province_name, cities.city_name FROM users
                          INNER JOIN userdetails ON users.id=userdetails.id
                          LEFT JOIN countries ON userdetails.country=countries._country_id
                          LEFT JOIN provinces ON userdetails.state=provinces._province_id
                          LEFT JOIN cities ON userdetails.city=cities._city_id
                          '.$where.'
                          ORDER BY  is_premium DESC
        ');
      //dd($users);
      return View::make("admin/users_list")->with([
                                                  'title'     =>  'Users',
                                                  'users'     =>  $users,
                                                  'countries' =>  $countries,
                                                  'provinces' =>  $provinces,
                                                  'cities'    =>  $cities
                                                ]);
    }

    public function premiumNonPremiumUser(Request $request){
      $rules =[
                    '_user_id'          =>  'required',
                    'user_is_premium'    =>  'required|in:1,0'
                ];
      $validate = Validator::make($request->all(),$rules);
      if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
      }else{
      
        if($request->user_is_premium==1){
          $plan_data = DB::table('chosen_plans')->where('cp_user_id',$request->_user_id)->where('cp_is_expired',0)->first();
          if($plan_data){
            if($plan_data->cp_inapp_type && $plan_data->cp_inapp_type=='paypal'){
              $res = getAccessToken();
              $accessToken = $res->access_token;
              $paypal_res = getSubscriptionDetails($plan_data->cp_inapp_productid,$accessToken);
              if($paypal_res->status=='ACTIVE'){
                  $cancel_res = cancelSubcription($plan_data->cp_inapp_productid,$accessToken);
              }
            }else if($plan_data->cp_inapp_type && $plan_data->cp_inapp_type=='stripe'){
              $response  = (new Payment)->checkStripeSubsription($plan_data->cp_inapp_productid);
                    
              if($response->status=='active'){
                  (new Payment)->cancelStripeSubsription($plan_data->cp_inapp_productid);
              }
            }
            DB::table('chosen_plans')->where('id',$plan_data->id)->update([
              'cp_is_expired'   =>1,
              'cp_expiry_date'  => date('Y-m-d')
            ]);
          }
          $res_message = 'Removed Premium Subscription Successfully';
        }else{
          $chosen_plan =  [
                            'cp_user_id'      =>  $request->_user_id,
                            'cp_plan_id'      =>  3,
                            'cp_expiry_date'  =>  NULL,
                          ];
    
          /*DB::table('chosen_plans')->where('cp_user_id',$request->_user_id)->update([
            'cp_is_expired'   =>1,
            'cp_expiry_date'  => date('Y-m-d')
          ]);*/
          DB::table('chosen_plans')->where('cp_user_id',$request->_user_id)->delete();

          DB::table('chosen_plans')->insert($chosen_plan);
          $res_message = 'Made Premium Member Successfully';
        }
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  $res_message,
                            'reponse_body'  =>  NULL
                        ], 200);
      }
    }

    function SetUserLanguage(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);

            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }
        if(empty($request->id) || empty($request->lang))
        {
            return response(['response_code'=>'401',
                         'response_msg'=> 'proper form keys are not posted',
                         'reponse_body' => 'null'],401);
        }

        User::where('id',$request->id)->update(['userlang'=>$request->lang]);
        return response(['response_code'=>'200',
                         'response_msg'=> 'user language is set',
                         'reponse_body' => 'null'],200);
    }
    function submitFeedback(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);

            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }
        if(empty($request->id) || empty($request->stars) /*|| empty($request->comment)*/ || empty($request->type))
        {
            return response(['response_code'=>'401',
                         'response_msg'=> 'proper form keys are not posted',
                         'reponse_body' => 'null'],401);
        }

        $check = User::where('id',$request->id)->first();
        if(!$check)
        {
            return response(['response_code'=>'401',
                         'response_msg'=> 'id does not exist in database',
                         'reponse_body' => 'null'],401);
        }
      $data =   Feedback::where('id',$request->id)->first();
      if($data)
      {
          $override = Feedback::where('id',$request->id)->update(['stars'=>$request->stars,'comment'=>$request->comment,'type'=>$request->type]);
          return response(['response_code'=>'200',
                        'response_msg'  =>  'Thank you for your valuable feedback',
                        'reponse_body'  =>  'null',
                        'show_toast'    =>  TRUE,
                        'time'          =>  3000,
                        'redirect'      =>  url('feedback')],200);
      }
      else
      {
          Feedback::insert(['id'=>$request->id,'stars'=>$request->stars,'comment'=>$request->comment,'type'=>$request->type]);
          return response(['response_code'=>'200',
                         'response_msg'=> 'Thank you for your valuable feedback',
                         'reponse_body' => 'null',
                         'show_toast'   => TRUE,
                          'redirect'    =>  url('feedback')],200);
      }
    }

    function ConnectContact(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);

            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }
        $request->validate([
          'id'=>'required',
          'stat'=>'required|max:1'
        ]);

        $dat = User::where('id',$request->id)->first();
        if($dat)
        {
            User::where('id',$request->id)->update(['connect'=>$request->stat]);
             return response(['response_code'=>'200',
                         'response_msg'=> 'connect updated',
                         'reponse_body' => 'null'],200);
        }
         return response(['response_code'=>'401',
                         'response_msg'=> 'id does not exists',
                         'reponse_body' => 'null'],401);
    }

    function AllowDiscovery(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);

            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }
        $request->validate([
          'id'=>'required',
          'stat'=>'required|max:1'
        ]);

        $dat = User::where('id',$request->id)->first();
        if($dat)
        {
            User::where('id',$request->id)->update(['discovery'=>$request->stat]);
             return response(['response_code'=>'200',
                         'response_msg'=> 'discovery updated',
                         'reponse_body' => 'null'],200);
        }
         return response(['response_code'=>'401',
                         'response_msg'=> 'id does not exists',
                         'reponse_body' => 'null'],401);
    }

    function getUserKeywords(Request $request)
    {
        $CheckHeader =$this->CheckRequestHeader($request);

            if(!$CheckHeader)
            {
                return response([
                    'response_code'=>'401',
                'response_msg'=> 'header missing or invalid',
                'reponse_body' => "null"
                ], 401);
            }
        $request->validate([
          'id'=>'required',
          'keywords'=>'required'
        ]);

        $splittedKeys = explode(",",$request->keywords);

        $dat = DB::table('keywords')->select('userkeys')->get()->toarray();
        $temp='';

        foreach($dat as $key)
        {
            $temp .= $key->userkeys.',';
        }
        $temp = explode(',',$temp);

        if($dat)
        {
          $i = 0;
          foreach($splittedKeys as $keys)
          {

            if(!in_array($keys,$temp))
            {
                //die('not in array '.$keys.'in '.$temp[$i]);
                DB::table('keywords')->insert(['userkeys'=>$keys]);

            }

            $keyid = DB::table('keywords')->select('keyid')->where('userkeys',$keys)->first();
            $temp1 = '';

            $userKeys = DB::table('keywordslinks')->select('keyid')->where('id',$request->id)->get();

            foreach($userKeys as $key)
            {
                $temp1.= $key->keyid.',';
            }
            $temp1 = explode(',',$temp1);
            //die($keyid->keyid);
            // return response(['response_code'=>'200',
            //              'response_msg'=> $userKeys,
            //              'reponse_body' => 'null'],200);
            if($keyid)
            {
                if(!in_array($keyid->keyid,$temp1))
                DB::table('keywordslinks')->insert(['id'=>$request->id,'keyid'=>$keyid->keyid]);

            }
            $i++;
          }
          $keywords = DB::table('keywords')
                          ->selectRaw('keywords.*')
                          ->join('keywordslinks', 'keywords.keyid', '=', 'keywordslinks.keyid')
                          ->where('keywordslinks.id',$request->id)
                          ->get();
          return response([
                            'response_code'=>'200',
                            'response_msg'=> 'keywords updated successfully',
                            'reponse_body' => ['keywords' => $keywords]
                          ],200);
        }
        return response(['response_code'=>'203',
                         'response_msg'=> 'databse error',
                         'reponse_body' => 'null'],200);
    }

  function deleteKeyword(Request $request){
    /*$CheckHeader =$this->CheckRequestHeader($request);

    if(!$CheckHeader){
      return response([
                        'response_code' =>  '401',
                        'response_msg'  =>  'header missing or invalid',
                        'reponse_body'  =>  "null"
                      ], 401);
    }*/
    $rules =[
              'key_id'      =>  'required',
              'user_id'     =>  'required',
            ];
    $validate = Validator::make($request->all(),$rules);

    if($validate->fails()){
      return response([
                        'response_code' =>  '201',
                        'response_msg'  =>  'Not enough details are provided',
                        'reponse_body'  =>  "null",
                        'errors'        =>  $validate->errors()
                      ], 401);
    }else{
      $find_link = DB::table('keywordslinks')->where('keyid',$request->key_id)->where('id',$request->user_id)->first();
      if($find_link){
        DB::table('keywordslinks')->where('Sid',$find_link->Sid)->delete();
        $keywords = DB::table('keywords')
                          ->selectRaw('keywords.*')
                          ->join('keywordslinks', 'keywords.keyid', '=', 'keywordslinks.keyid')
                          ->where('keywordslinks.id',$request->user_id)
                          ->get();
          return response([
                            'response_code'=>'200',
                            'response_msg'=> 'keyword deleted successfully',
                            'reponse_body' => ['keywords' => $keywords]
                          ],200);

      }else{
        return response([
                          'response_code' =>  '201',
                          'response_msg'  =>  'Invalid details',
                          'reponse_body'  =>  "null"
                        ], 401);
      }
    }
  }

  public function deleteChat(Request $request){
    $rules =[
              'dialog_id'     =>  'required',
              'user_id'       =>  'required'
            ];
    $validate = Validator::make($request->all(),$rules);
    if($validate->fails()){

      return response([
                        'response_code' =>  '201',
                        'response_msg'  =>  'Not enough details are provided',
                        'reponse_body'  =>  "null",
                        'errors'        =>  $validate->errors()
                      ], 401);

    }else{
      $user_data = DB::table('users')->where('id',$request->user_id)->first();
      if(!empty($user_data)){
        $qb_session = qb_login($user_data->email,'12345678');
        if($qb_session && isset($qb_session->session) && $qb_session->session){
          $token = $qb_session->session->token;
          $dialog_id = $request->dialog_id;
          $force = $request->force ? $request->force : 0;
          $dialog_res = qbDeleteDialog($token,$dialog_id,$force);

          if(isset($dialog_res->errors) && $dialog_res->errors){
            return response([
                              'response_code' =>  '201',
                              'response_msg'  =>  $dialog_res->errors[0],
                              'reponse_body'  =>  $dialog_res->errors
                          ], 401);
          }else{
            return response([
                              'response_code' =>  '200',
                              'response_msg'  =>  'Chat deleted successfully',
                              'reponse_body'  =>  NULL,
                              'redirect'      =>  url('messages')
                          ], 200);
          }
        }else{
          return response([
                          'response_code' =>  '201',
                          'response_msg'  =>  'Invalid details provided',
                          'reponse_body'  =>  "null"
                        ], 401);
        }
        
      }else{
        return response([
                        'response_code' =>  '201',
                        'response_msg'  =>  'Invalid details provided',
                        'reponse_body'  =>  "null"
                      ], 401);
      }
      
    }
  }
}
