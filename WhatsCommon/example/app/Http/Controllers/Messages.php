<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use View;
use Validator;

class Messages extends Controller
{
    function index(Request $request){
    	if (!$request->session()->has('userdata')) {
            return redirect('login');
        }

        $token = isset($request->session()->get('userdata')['qb_token']) ? $request->session()->get('userdata')['qb_token'] : NULL;
        $dialogs = [];
        $evaluate = '';
        if($token){
            $user_qb_id = $request->session()->get('userdata')['quick_blox_id'];
            $user_id = $request->session()->get('userdata')['id'];
            /*echo $token;*/
            $dialogs = quickGetDialog($token);
            /*echo"<pre>";print_r($dialogs);echo"</pre>";exit();*/
            $new_dialogs = [];
            if($dialogs && isset($dialogs->items) && $dialogs->items){
                $items = [];
                foreach($dialogs->items as $dialog){
                    $reciever_id = ($dialog->occupants_ids[0]==$user_qb_id) ? $dialog->occupants_ids[1] : $dialog->occupants_ids[0];
                    $reciever_data = getUserDetailsByQuickID($reciever_id);
                    if(!empty($reciever_data)){
                        $block_check = DB::select("SELECT * FROM blocked_users WHERE (bu_from_id='".$reciever_data->id."' AND bu_to_id='".$user_id."') OR (bu_from_id='".$user_id."' AND bu_to_id='".$reciever_data->id."')");
                        if(empty($block_check)){
                            array_push($items, $dialog);
                        }
                    }
                    
                }
                
                $new_dialogs['items'] = $items;
            }
            
            if($request->qb){
                $reciever_id = $request->qb;
                $dialog_res = quickCreateDialog($token,$reciever_id);
                if(isset($dialog_res->_id) && $dialog_res->_id){
                    $reciever_data = getUserDetailsByQuickID($reciever_id);
                    $name = $reciever_data ? $reciever_data->firstName.' '.$reciever_data->lastName : $dialog_res->name;
                    $image = ($reciever_data && $reciever_data->profile_photo_path) ? $reciever_data->profile_photo_path : url('new-design/img/profile_placeholder.jpg');
                    $evaluate = ['dialog_id'=>$dialog_res->_id,'reciever_id'=>$reciever_id,'name'=>$name,'image'=>$image,'user_id' => $reciever_data->id,'username'=>$reciever_data->username];
                }
            }
        }else{
            $newData = $request->session()->get('userdata');
            $qb_session = qb_login();
            if($qb_session && isset($qb_session->session) && $qb_session->session){
            
                $token = $qb_session->session->token;
                $existing_user = getUserByEmail($token,$request->session()->get('userdata')['email']);
                if(isset($existing_user->user) && $existing_user->user){
                    $quick_blox_id = $existing_user->user->id;
                    User::where('id',$request->session()->get('userdata')['id'])->update(['quick_blox_id'=>$quick_blox_id]);
                    
                    $qb_session = qb_login($request->session()->get('userdata')['email'],'12345678');
                    if($qb_session && isset($qb_session->session) && $qb_session->session){
                      
                      $token = $qb_session->session->token;
                      $newData['qb_token'] = $token;
                      $request->session()->put('userdata', $newData);
                    }
                }
            }
        }
        

        return View::make("messages")->with(['dialogs'=>$dialogs,'evaluate'=>$evaluate,'token'=>$token]);
    }

    function getMessageCounts(Request $request){
        if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
        $token = isset($request->session()->get('userdata')['qb_token']) ? $request->session()->get('userdata')['qb_token'] : NULL;
        if(!$token){
            $newData = $request->session()->get('userdata');
            $qb_session = qb_login();
            if($qb_session && isset($qb_session->session) && $qb_session->session){
            
                $token = $qb_session->session->token;
                $existing_user = getUserByEmail($token,$request->session()->get('userdata')['email']);
                if(isset($existing_user->user) && $existing_user->user){
                    $quick_blox_id = $existing_user->user->id;
                    User::where('id',$request->session()->get('userdata')['id'])->update(['quick_blox_id'=>$quick_blox_id]);
                    
                    $qb_session = qb_login($request->session()->get('userdata')['email'],'12345678');
                    if($qb_session && isset($qb_session->session) && $qb_session->session){
                      
                      $token = $qb_session->session->token;
                      $newData['qb_token'] = $token;
                      $request->session()->put('userdata', $newData);
                    }
                }
            }
        }
        /*$token = $request->session()->get('userdata')['qb_token'];*/
        $counts = getUnreadMessageCount($token);
        $total = 0;
        if($counts && isset($counts->total)){
            $total = $counts->total > 9 ? '9+' : $counts->total;
        }
        
        /*print_r($counts);*/
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Message counts fetched successfully',
                            'reponse_body'  =>  ['msgCount' => $total]
                        ], 200);
    }

    function getDialogs(Request $request){
        if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
        $token = isset($request->session()->get('userdata')['qb_token']) ? $request->session()->get('userdata')['qb_token'] : NULL;
        if(!$token){
            $newData = $request->session()->get('userdata');
            $qb_session = qb_login();
            if($qb_session && isset($qb_session->session) && $qb_session->session){
            
                $token = $qb_session->session->token;
                $existing_user = getUserByEmail($token,$request->session()->get('userdata')['email']);
                if(isset($existing_user->user) && $existing_user->user){
                    $quick_blox_id = $existing_user->user->id;
                    User::where('id',$request->session()->get('userdata')['id'])->update(['quick_blox_id'=>$quick_blox_id]);
                    
                    $qb_session = qb_login($request->session()->get('userdata')['email'],'12345678');
                    if($qb_session && isset($qb_session->session) && $qb_session->session){
                      
                      $token = $qb_session->session->token;
                      $newData['qb_token'] = $token;
                      $request->session()->put('userdata', $newData);
                    }
                }
            }
        }
        /*$token = $request->session()->get('userdata')['qb_token'];*/
        $dialogs = quickGetDialog($token);
        $search_name = $request->search_name ? $request->search_name : NULL;
        $dialogsList = view('xhr.dialogs',['dialogs' => $dialogs, 'search_name' => $search_name,'token'=>$token])->render();
        $counts = getUnreadMessageCount($token);
        $total = 0;
        if(isset($counts->total)){
            $total = $counts->total > 9 ? '9+' : $counts->total;
        }
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Dialogs fetched successfully',
                            'reponse_body'  =>  [
                                                    'dialogsList'   =>  $dialogsList,
                                                    'msgCount'      =>  $total,
                                                    'dialogs'       =>  $dialogs
                                                ]
                        ], 200);
    }

    public function showHideDeleteMessages(Request $request){
        if(!$request->session()->has('delete_on')){
            $request->session()->put('delete_on',TRUE);
        }else{
            $request->session()->forget('delete_on');
        }

        $token = isset($request->session()->get('userdata')['qb_token']) ? $request->session()->get('userdata')['qb_token'] : NULL;
        if(!$token){
            $newData = $request->session()->get('userdata');
            $qb_session = qb_login();
            if($qb_session && isset($qb_session->session) && $qb_session->session){
            
                $token = $qb_session->session->token;
                $existing_user = getUserByEmail($token,$request->session()->get('userdata')['email']);
                if(isset($existing_user->user) && $existing_user->user){
                    $quick_blox_id = $existing_user->user->id;
                    User::where('id',$request->session()->get('userdata')['id'])->update(['quick_blox_id'=>$quick_blox_id]);
                    
                    $qb_session = qb_login($request->session()->get('userdata')['email'],'12345678');
                    if($qb_session && isset($qb_session->session) && $qb_session->session){
                      
                      $token = $qb_session->session->token;
                      $newData['qb_token'] = $token;
                      $request->session()->put('userdata', $newData);
                    }
                }
            }
        }
        /*$token = $request->session()->get('userdata')['qb_token'];*/
        $dialogs = quickGetDialog($token);
        $search_name = $request->search_name ? $request->search_name : NULL;
        $dialogsList = view('xhr.dialogs',['dialogs' => $dialogs, 'search_name' => $search_name,'token'=>$token])->render();
        $counts = getUnreadMessageCount($token);
        $total = 0;
        if(isset($counts->total)){
            $total = $counts->total > 9 ? '9+' : $counts->total;
        }
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Dialogs fetched successfully',
                            'reponse_body'  =>  [
                                                    'dialogsList'   =>  $dialogsList,
                                                    'msgCount'      =>  $total,
                                                    'dialogs'       =>  $dialogs
                                                ]
                        ], 200);
    }

    function getMessages(Request $request){
        if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
        $token = $request->session()->get('userdata')['qb_token'];
        /*echo 'token => '.$token;*/
        $rules =[
                    'dialog_id' =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);

        }else{
            $messages_res = quickGetMessage($token,$request->dialog_id);
            $messages = ($messages_res && $messages_res->items) ? array_reverse($messages_res->items) : [];

            /*print_r($messages);
            exit();*/
            /*$new_messages = [];
            if($messages){
                
                foreach($messages as $message){
                    
                    if($message->attachments && !empty($message->attachments)){
                        $attachments = [];
                        foreach($message->attachments as $attachment){
                            print_r($attachment);
                            $att_details = getAttachmentURLByID($token,$attachment->id);
                            echo 'here';
                            echo $att_details;
                            print_r($att_details);exit();
                            $attachment->blob = ($att_details && $att_details->blob) ? $att_details->blob : [];
                            array_push($attachments, $attachment);
                        }
                        $message->attachments = $attachments;
                    }
                    array_push($new_messages, $message);
                }
            }*/
            /*print_r($new_messages);exit();*/
            $msgBodyContainer = view('xhr.messages',['messages' => $messages,'token' => $token])->render();
            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Messages fetched successfully',
                                'reponse_body'  =>  ['msgBodyContainer' => $msgBodyContainer]
                            ], 200);
        }
    }

    function getMessageRequests(Request $request){
        if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
        $token = isset($request->session()->get('userdata')['qb_token']) ? $request->session()->get('userdata')['qb_token'] : NULL;
        $user_id = $request->session()->get('userdata')['id'];
        if(!$token){
            $newData = $request->session()->get('userdata');
            $qb_session = qb_login();
            if($qb_session && isset($qb_session->session) && $qb_session->session){
            
                $token = $qb_session->session->token;
                $existing_user = getUserByEmail($token,$request->session()->get('userdata')['email']);
                if(isset($existing_user->user) && $existing_user->user){
                    $quick_blox_id = $existing_user->user->id;
                    User::where('id',$user_id)->update(['quick_blox_id'=>$quick_blox_id]);
                    
                    $qb_session = qb_login($request->session()->get('userdata')['email'],'12345678');
                    if($qb_session && isset($qb_session->session) && $qb_session->session){
                      
                      $token = $qb_session->session->token;
                      $newData['qb_token'] = $token;
                      $request->session()->put('userdata', $newData);
                    }
                }
            }
        }
        /*$token = $request->session()->get('userdata')['qb_token'];*/
        $dialogs = quickGetDialog($token);
        /*$msgCount = (isset($dialogs->items) && $dialogs->items) ? count($dialogs->items) : 0;*/
        $counter = 0;
        $search_name = $request->search_name ? $request->search_name : NULL;
        if(isset($dialogs->items) && $dialogs->items){
            foreach($dialogs->items as $dialog){
                $sender_id = $request->session()->get('userdata')['quick_blox_id'];
                $my_id = $user_id;
                $force = ($dialog->user_id==$sender_id) ? 1 : 0;
                $reciever_id = ($dialog->occupants_ids[0]==$sender_id) ? $dialog->occupants_ids[1] : $dialog->occupants_ids[0];
                $reciever_data = getUserDetailsByQuickID($reciever_id);
                if($reciever_data && $reciever_data->is_blocked==1){
                  continue;
                }
                $dialog_id = $dialog->_id;
                $messages = quickGetMessage($token,$dialog_id);
                if($messages && $messages->items){
                  $msg = FALSE;
                  foreach($messages->items as $item){
                    if($item->sender_id==$sender_id){
                      $msg = TRUE;
                    }
                  }
                  if($msg){
                    continue;
                  }
                }else{
                  continue;
                }
                if(!$dialog->last_message){
                  continue;
                }
                $rec_username = $reciever_data ? $reciever_data->username : '';
                if($search_name && strpos(strtolower($name), $search_name) === false){
                  continue; 
                }

                $counter++;
            }
        }

        $connection_requests = DB::table('connection_requests')
                                ->join('users','users.id','=','connection_requests.request_from_user')
                                ->where('request_to_user',$user_id)
                                ->where('request_status',0)
                                ->orderBy('_request_id','desc')
                                ->get();

        $counter += count($connection_requests);
        
        $dialogsList = view('xhr.message_requests',['dialogs' => $dialogs, 'search_name' => $search_name,'token'=>$token,'connection_requests'=>$connection_requests])->render();
        
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Message requests fetched successfully',
                            'reponse_body'  =>  [
                                                    'dialogsList'   =>  $dialogsList,
                                                    'msgCount'      =>  $counter,
                                                    'dialogs'       =>  $dialogs
                                                ]
                        ], 200);
    }

    function uploadFile(Request $request){
        if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
        $token = $request->session()->get('userdata')['qb_token'];
        $dialog_id = $request->dialog_id;
        /*print_r($_FILES);*/
        /*print_r($request->dialog_id);*/
        if($_FILES['file_upload'] && !empty($_FILES['file_upload'])){
            $file_name = $_FILES['file_upload']['name'];
            $file_type = $_FILES['file_upload']['type'];
            $file_tmp = $_FILES['file_upload']['tmp_name'];
            $file_size = $_FILES['file_upload']['size'];
            $blob_data = createFile($token,$file_name,$file_type);
            $filename = basename($_FILES['file_upload']['name']);
            /*$fileName = time().'_'.$request->file_upload->getClientOriginalName();
            
            $request->file_upload->move(public_path('attachments'), $fileName);
            $profUrl = url('/attachments/'.$fileName);*/
            // print_r($blob_data);//exit();
            if($blob_data && $blob_data->blob){
                $blob_id = $blob_data->blob->id;
                $blob_uid = $blob_data->blob->uid;
                $param = $blob_data->blob->blob_object_access->params;
                /*echo $profUrl;*/
                $upload_res = qbUploadFile($token,$param,$file_tmp,$file_type,$filename);
                //if($upload_res){
                $complete = qbMarkFileUploaded($token,$blob_id,$file_size);

                   // if($complete){
                $blob_type = '';
                if(in_array($file_type, ['image/jpeg','image/png','image/jpg'])){
                    $blob_type = 'image';
                }else if(in_array($file_type, ['audio/mpeg','audio/vnd.wav','audio/mp4'])){
                    $blob_type = 'Audio';
                }else if(in_array($file_type, ['video/mp4','video/ogg','video/webm','application/mp4'])){
                    $blob_type = 'video';
                }

                // print_r($upload_res);

                if(in_array($blob_type, ['image','Audio','video'])){
                    $attachments[0] = ['type' => $blob_type,'id' => $blob_uid];

                    $sent_message = quickSendMessage($token,$dialog_id,'',$attachments);
                    /*print_r($sent_message);*/
                    $messages_res = quickGetMessage($token,$dialog_id);
                    $messages = ($messages_res && $messages_res->items) ? array_reverse($messages_res->items) : [];
                    /*print_r($messages);*/
                    $msgBodyContainer = view('xhr.messages',['messages' => $messages,'token' => $token])->render();
                    return response([
                                        'response_code' =>  '200',
                                        'response_msg'  =>  'Messages sent successfully',
                                        'reponse_body'  =>  ['msgBodyContainer' => $msgBodyContainer]
                                    ], 200);
                }
                    //}
               // }
                
                
            }
        }
    }

    function sendMessage(Request $request){
        if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
        $token = $request->session()->get('userdata')['qb_token'];

        $rules =[
                    'dialog_id'     =>  'required',
                    'messageTxt'    =>  'required',
                    'reciever_id'   =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);

        }else{
            $messageTxt = $request->messageTxt;
            $dialog_id = $request->dialog_id;
            $reciever_data = getUserDetailsByQuickID($request->reciever_id);
            $sent_message = quickSendMessage($token,$dialog_id,$messageTxt);
            /*$msg = base64_encode($reciever_data->firstName." ".$reciever_data->lastName." sent you message");*/
            $event = quickCreateEvent($token,$request->reciever_id,urlencode($messageTxt));
            /*print_r($event);exit();*/
            $messages_res = quickGetMessage($token,$dialog_id);
            $messages = ($messages_res && $messages_res->items) ? array_reverse($messages_res->items) : [];
            /*print_r($messages);*/
            $msgBodyContainer = view('xhr.messages',['messages' => $messages,'token' => $token])->render();
            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Messages sent successfully',
                                'reponse_body'  =>  ['msgBodyContainer' => $msgBodyContainer,'event'=>$event]
                            ], 200);
        }
    }

    function createDialogModal(Request $request){
        $user_id = $request->session()->get('userdata')['id'];

        $connections = DB::select("SELECT userdetails.*,users.username, users.quick_blox_id, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status =1 AND ((request_from_user=$user_id AND request_to_user=users.id) OR (request_from_user=users.id AND request_to_user=$user_id))), 1, 0)) AS is_connected
            FROM userdetails
            INNER JOIN users ON users.id=userdetails.id
            WHERE userdetails.id != $user_id
            AND userdetails.id NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id=$user_id)
            HAVING is_connected=1");

        $html = view('xhr.create_dialog_modal',['connections'=>$connections])->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function createDialog(Request $request){
        if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
        $token = $request->session()->get('userdata')['qb_token'];
        $sender_id = $request->session()->get('userdata')['quick_blox_id'];
        $rules =[
                    'reciever_id'     =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);

        }else{
            $reciever_id = $request->reciever_id;
            
            $dialog_res = quickCreateDialog($token,$reciever_id);
            if(isset($dialog_res->_id) && $dialog_res->_id){
                $reciever_data = getUserDetailsByQuickID($reciever_id);
                $name = $reciever_data ? $reciever_data->firstName.' '.$reciever_data->lastName : $dialog_res->name;
                $image = ($reciever_data && $reciever_data->profile_photo_path) ? $reciever_data->profile_photo_path : 'https://listimg.pinclipart.com/picdir/s/25-259252_special-school-nurse-person-placeholder-image-png-clipart.png';
                $evaluate = 'getMessage("'.$dialog_res->_id.'","'.$reciever_id.'","'.$name.'","'.$image.'");';
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Chat created successfully',
                                'reponse_body'  =>  NULL,
                                'evaluate'      =>  $evaluate,
                                'close'         =>  TRUE
                            ], 200);
            }else{
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid Request',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            /*print_r($dialog_res);*/
        }
    }

    function deleteChat(Request $request){
        if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
        $token = $request->session()->get('userdata')['qb_token'];
        $rules =[
                    'dialog_id'     =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);

        }else{
            $dialog_id = $request->dialog_id;
            $force = $request->force ? $request->force : 0;
            $dialog_res = qbDeleteDialog($token,$dialog_id,$force);

            if(isset($dialog_res->errors) && $dialog_res->errors){
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  $dialog_res->errors[0],
                                'reponse_body'  =>  $dialog_res->errors
                            ], 401);
            }else{
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Chat deleted successfully',
                                'reponse_body'  =>  NULL,
                                'redirect'      =>  url('messages')
                            ], 200);
            }
        }
    }

    function chatInit(Request $request){
    	
    }

    function quickAuth(Request $request) {
    	if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
    	$token = $request->session()->get('userdata')['qb_token'];
        /*if($token){
            echo '<pre>';
            print_r($token);
            echo '</pre>';
        }*/
        $dialogs = quickGetDialog($token);
        if($dialogs){
            echo '<pre>';
            print_r($dialogs);
            echo '</pre>';
        }
	}

	/*function quickGetDialog($token) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Dialog.json');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'QuickBlox-REST-API-Version: 0.1.0',
            'QB-Token: ' . $token
        ));
        $response = curl_exec($curl);
        if ($response) {
                return json_decode($response);
        } else {
                echo false;
        }
        curl_close($curl);
	}*/

}
