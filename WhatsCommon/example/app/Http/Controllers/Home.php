<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Page;
use App\Models\User;
use App\Models\Userdetail;
use View;
use Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Payment;
use Image;
use Response;
use Imdhemy\GooglePlay\Subscriptions\SubscriptionPurchase;
use Imdhemy\Purchases\Facades\Subscription;
//use Imdhemy\Purchases\Subscription;
class Home extends Controller
{
	public function __construct(){	
    }

    public function sendSMSMessage(Request $request){
        $rules =[
                    'to'   =>   'required',
                    'text' =>   'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $result = sendSms($request->to,$request->text);
            print_r($result);
        }
    }

    function adminDashboard(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }

        $select='SELECT (SELECT COUNT(*) FROM users) AS total_users, 
                    (SELECT COUNT(*) FROM life_events) AS total_life_event, 
                    (SELECT COUNT(*) FROM feeds WHERE feed_type="matched") AS total_matched
                ';
            //echo $select;exit;

        $counts = DB::select($select);
        /*$dates = [];*/
        $graph_counts = [];
        for($day=9;$day>=0;$day--){
            if($day==0){
                $date = date('Y-m-d');
            }else{
                $date = date('Y-m-d', strtotime("-$day days"));
            }
            $check_sel = '';
            if(isset($request->type) && in_array($request->type,[1,2,3,4,5,6,7,8,9,10,11,12])){
                $check_sel = ' ,(SELECT COUNT(*) FROM life_events WHERE created_at LIKE "%'.$date.'%" AND event_type_id='.$request->type.') AS check_count';
                //echo 'here';exit();
            }
            $select='SELECT (SELECT COUNT(*) FROM users WHERE created_at LIKE "%'.$date.'%") AS total_users, 
                    (SELECT COUNT(*) FROM life_events WHERE created_at LIKE "%'.$date.'%") AS total_life_event, 
                    (SELECT COUNT(*) FROM feeds WHERE feed_type="matched" AND feed_created LIKE "%'.$date.'%") AS total_matched,
                    (SELECT COUNT(*) FROM feeds WHERE feed_type="connected" AND feed_created LIKE "%'.$date.'%") AS total_connected
                    '.$check_sel;
            $g_counts = DB::select($select);
            array_push($graph_counts, [
                                        'date'            =>  $date,
                                        'user_count'      =>  $g_counts ? $g_counts[0]->total_users : 0,
                                        'event_count'     =>  $g_counts ? $g_counts[0]->total_life_event : 0,
                                        'match_count'     =>  $g_counts ? $g_counts[0]->total_matched : 0,
                                        'connection_count'=>  $g_counts ? $g_counts[0]->total_connected : 0,
                                        'check_count'     =>  ($g_counts && isset($g_counts[0]->check_count)) ? $g_counts[0]->check_count : 0
                                    ]);
        }
        //$first_day_of_year = date('Y-01-01');
        $lastYear = date("Y-m-d", strtotime("-1 years"));
        $logintype_graphs = DB::select("SELECT (SELECT COUNT(DISTINCT `login_user_id`) FROM login_details WHERE `login_from`='web' AND `login_date` BETWEEN '".$lastYear."' AND NOW()) AS web_count,
            (SELECT COUNT(DISTINCT `login_user_id`) FROM login_details WHERE `login_from`='android' AND `login_date` BETWEEN '".$lastYear."' AND NOW()) AS android_count,
            (SELECT COUNT(DISTINCT `login_user_id`) FROM login_details WHERE `login_from`='ios'  AND `login_date` BETWEEN '".$lastYear."' AND NOW()) AS ios_count,
            (SELECT COUNT(DISTINCT `login_user_id`) FROM login_details WHERE `login_from`='facebook'  AND `login_date` BETWEEN '".$lastYear."' AND NOW()) AS facebook_count,
            (SELECT COUNT(DISTINCT `login_user_id`) FROM login_details WHERE `login_from`='google'  AND `login_date` BETWEEN '".$lastYear."' AND NOW()) AS google_count,
            (SELECT COUNT(DISTINCT `login_user_id`) FROM login_details WHERE `login_from`='apple'  AND `login_date` BETWEEN '".$lastYear."' AND NOW()) AS apple_count,
            (SELECT SUM(web_count + android_count + ios_count + facebook_count + google_count + apple_count)) AS total_count");

        $match_by_type = DB::select("SELECT (SELECT COUNT(*) FROM feeds WHERE feed_type='matched' AND feed_lifeevent=1) AS personal_count,
            (SELECT COUNT(*) FROM feeds WHERE feed_type='matched' AND feed_lifeevent=2) AS dating_count,
            (SELECT COUNT(*) FROM feeds WHERE feed_type='matched' AND feed_lifeevent=3) AS adoption_count,
            (SELECT COUNT(*) FROM feeds WHERE feed_type='matched' AND feed_lifeevent=4) AS travel_count,
            (SELECT COUNT(*) FROM feeds WHERE feed_type='matched' AND feed_lifeevent=5) AS military_count,
            (SELECT COUNT(*) FROM feeds WHERE feed_type='matched' AND feed_lifeevent=6) AS school_count,
            (SELECT COUNT(*) FROM feeds WHERE feed_type='matched' AND feed_lifeevent=7) AS career_count,
            (SELECT COUNT(*) FROM feeds WHERE feed_type='matched' AND feed_lifeevent=8) AS pets_count,
            (SELECT COUNT(*) FROM feeds WHERE feed_type='matched' AND feed_lifeevent=9) AS lost_count,
            (SELECT COUNT(*) FROM feeds WHERE feed_type='matched' AND feed_lifeevent=10) AS dropel_count,
            (SELECT COUNT(*) FROM feeds WHERE feed_type='matched' AND feed_lifeevent=11) AS username_count");

        //$logintype_graphs = $logintype_graphs ? $logintype_graphs[0] : [];

        //print_r($logintype_graphs);exit();
        $feedback_where = '';
        if($request->time && in_array($request->time,['6','12'])){
            
            $lastYear = date("Y-m-d", strtotime("-".$request->time." months"));
            $feedback_where = " AND updated_at BETWEEN $lastYear AND NOW()";
        }
        $feedback_type_graph = DB::select("SELECT (SELECT COUNT(*) FROM feedback WHERE type=1 $feedback_where) AS perform_count,
            (SELECT COUNT(*) FROM feedback WHERE type=2 $feedback_where) AS bug_count,
            (SELECT COUNT(*) FROM feedback WHERE type=3 $feedback_where) AS general_count");

        $donation_feedback_counts = [];

        $this_month_day = date("Y-m-01");
        
        for($month=11;$month>=0;$month--){
            
            if($month==0){
                $date = date('Y-m');
            }else{
                $date = date('Y-m', strtotime("-$month months", strtotime($this_month_day)));
            }
            //echo $month.' '.$date.' '.date('Y-m-d', strtotime("-$month months",strtotime($this_month_day))).'<br>';
            
            $donations = DB::select("SELECT SUM(donation_amount) AS monthly_donation FROM donations WHERE donation_payment_status='successful' AND created_at like '%$date%'");
            $feedbacks = DB::select("SELECT COUNT(*) AS monthly_feedback FROM feedback WHERE updated_at LIKE '%$date%'");
            $donation = $donations ? $donations[0] : [];
            $feedback = $feedbacks ? $feedbacks[0] : [];
            array_push($donation_feedback_counts, [
                'date'                  =>  $date,
                'monthly_donation'      =>  (isset($donation->monthly_donation) && $donation->monthly_donation) ? $donation->monthly_donation : 0,
                'monthly_feedback'      =>  (isset($feedback->monthly_feedback) && $feedback->monthly_feedback) ? $feedback->monthly_feedback : 0,
            ]);
            
        }
        /*echo '<pre>';
        print_r($donation_feedback_counts);exit();*/
        $lastmonth = date("Y-m-d", strtotime("-30 days"));
        $yesterday = date("Y-m-d", strtotime("-1 days"));

        $users = DB::select("SELECT users.*,(SELECT IF((SELECT COUNT(id) FROM chosen_plans 
              WHERE cp_user_id=users.id
              AND cp_is_expired=0 
              AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0)) AS is_premium,
              (SELECT COUNT(_login_id) AS frequency FROM login_details 
                WHERE login_user_id =users.id
                AND login_date >= '".$lastmonth."' HAVING frequency>5 ) AS is_frequent,
                (SELECT COUNT(_login_id) AS frequency FROM login_details 
                WHERE login_user_id =users.id
                AND login_date >= '".$yesterday."' HAVING frequency>=1 ) AS is_active FROM users");
        $premium_user = 0;
        $non_prem_user = 0;
        $frequent_users = 0;
        $active_users = 0;
        if($users && count($users) > 0){
            foreach($users as $user){
                if($user->is_premium==1){
                    $premium_user +=1;
                }else{
                    $non_prem_user +=1;
                }
                if($user->is_frequent){
                    $frequent_users +=1;
                }
                if($user->is_active){
                    $active_users +=1;
                }
            }
        }
        $user_types =[  'premium_user'  =>  $premium_user,
                        'non_prem_user' =>  $non_prem_user,
                        'frequent_users'=>  $frequent_users,
                        'active_users'  =>  $active_users
                    ];

        $premium_users = DB::select("SELECT (SELECT COUNT(id) FROM chosen_plans 
                                   WHERE cp_user_id IN (SELECT id FROM users)
                                   AND cp_is_expired=0 
                                   AND cp_plan_id = 1
                                   AND cp_expiry_date >= CURDATE() ) AS monthly_counts,
                                   (SELECT COUNT(id) FROM chosen_plans 
                                   WHERE cp_user_id IN (SELECT id FROM users)
                                   AND cp_is_expired=0 
                                   AND cp_plan_id = 2
                                   AND cp_expiry_date >= CURDATE() ) AS yearly_counts,
                                   (SELECT COUNT(id) FROM chosen_plans 
                                   WHERE cp_user_id IN (SELECT id FROM users)
                                   AND cp_is_expired=0 
                                   AND cp_plan_id = 3
                                   AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)) AS lifetime_counts");
        $event_categories = DB::select("SELECT life_event_categories.*,
                                (SELECT COUNT(_request_id) FROM connection_requests WHERE request_status=1 
                                AND request_feed IN(SELECT _feed_id FROM feeds 
                                WHERE feed_lifeevent=life_event_categories.id))AS connections_count
                                FROM life_event_categories");
        $countries = DB::table('countries')->get();
        $demography = [];
        if($countries){
            foreach($countries as $country){
                $provinces = DB::select("SELECT provinces.*,(SELECT COUNT(id) FROM userdetails WHERE state=provinces.province_name) AS user_count,
                    (SELECT COUNT(id) FROM chosen_plans 
                        WHERE cp_user_id IN (SELECT id FROM userdetails WHERE state=provinces.province_name)
                        AND cp_is_expired=0 
                        AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)) AS premium_count,
                    (SELECT SUM(donation_amount) FROM donations WHERE donation_user_id IN(SELECT id FROM userdetails WHERE state=provinces.province_name)) AS donation
                FROM provinces
                WHERE province_country=$country->_country_id
                HAVING user_count > 0");
                if($provinces && count($provinces)>0){
                    $country->provinces = $provinces;
                    array_push($demography,$country);
                }
            }
        }
        
        //exit();
        /*echo '<pre>';
        print_r($revenue_arr);exit();*/

        $active_inactive = DB::select("SELECT (SELECT COUNT(id) FROM chosen_plans WHERE cp_is_expired=0) AS active,
(SELECT COUNT(id) FROM chosen_plans WHERE cp_is_expired=1) AS inactive");

        return View::make("admin.dashboard")->with([
                                                        'counts'                    =>  $counts[0],
                                                        'graph_counts'              =>  $graph_counts,
                                                        'logintype_graphs'          =>  $logintype_graphs[0],
                                                        'match_by_type'             =>  $match_by_type[0],
                                                        'feedback_type_graph'       =>  $feedback_type_graph[0],
                                                        'user_types'                =>  $user_types,
                                                        'donation_feedback_counts'  =>  $donation_feedback_counts,
                                                        'premium_users'             =>  $premium_users [0],
                                                        'event_categories'          =>  $event_categories,
                                                        'demography'                =>  $demography,
                                                        'active_inactive'           =>  $active_inactive[0]
                                                    ]);
    }

    public function refreshLifeEventGraph(Request $request){
        $rules =[
                    'event_type' =>  'required|in:all,1,2,3,4,5,6,7,8,9,10,11,12'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
          return response([
                            'response_code' =>  '201',
                            'response_msg'  =>  'Not enough details are provided',
                            'reponse_body'  =>  "null",
                            'errors'    =>  $validate->errors()
                          ], 401);
        }else{
            $graph_counts = [];
            for($day=9;$day>=0;$day--){
                if($day==0){
                    $date = date('Y-m-d');
                }else{
                    $date = date('Y-m-d', strtotime("-$day days"));
                }
                $check_sel = '';
                if(isset($request->event_type) && in_array($request->event_type,[1,2,3,4,5,6,7,8,9,10,11,12])){
                    $check_sel = ' ,(SELECT COUNT(*) FROM life_events WHERE created_at LIKE "%'.$date.'%" AND event_type_id='.$request->event_type.') AS check_count';
                    //echo 'here';exit();
                }
                $select='SELECT (SELECT COUNT(*) FROM users WHERE created_at LIKE "%'.$date.'%") AS total_users, 
                        (SELECT COUNT(*) FROM life_events WHERE created_at LIKE "%'.$date.'%") AS total_life_event, 
                        (SELECT COUNT(*) FROM feeds WHERE feed_type="matched" AND feed_created LIKE "%'.$date.'%") AS total_matched,
                        (SELECT COUNT(*) FROM feeds WHERE feed_type="connected" AND feed_created LIKE "%'.$date.'%") AS total_connected
                        '.$check_sel;
                $g_counts = DB::select($select);
                array_push($graph_counts, [
                                            'date'            =>  $date,
                                            'user_count'      =>  $g_counts ? $g_counts[0]->total_users : 0,
                                            'event_count'     =>  $g_counts ? $g_counts[0]->total_life_event : 0,
                                            'match_count'     =>  $g_counts ? $g_counts[0]->total_matched : 0,
                                            'connection_count'=>  $g_counts ? $g_counts[0]->total_connected : 0,
                                            'check_count'     =>  ($g_counts && isset($g_counts[0]->check_count)) ? $g_counts[0]->check_count : 0
                                        ]);
            }

            $html = view('admin.xhr.refresh_life_event_graph',['graph_counts' => $graph_counts,'event_type'=>$request->event_type])->render();
            return response([
                                    'response_code' =>  '200',
                                    'response_msg'  =>  'Modal Fetched Successfully',
                                    'reponse_body'  =>  ['graph'         =>  $html],
                            ],200);
        }
    }

    public function refreshFeedbackGraph(Request $request){
        $rules =[
                    'time' =>  'required|in:all,1,6,12'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
          return response([
                            'response_code' =>  '201',
                            'response_msg'  =>  'Not enough details are provided',
                            'reponse_body'  =>  "null",
                            'errors'    =>  $validate->errors()
                          ], 401);
        }else{
            $lastYear = date("Y-m-d", strtotime("-1 years"));
            $feedback_where = '';
            if($request->time && in_array($request->time,[1,6,12])){
                if($request->time==1){
                    $lastYear = date("Y-m-01", strtotime("-".$request->time." months"));
                    $feedback_where = " AND updated_at BETWEEN '".$lastYear."' AND NOW()";
                }else{
                    $lastYear = date("Y-m-d", strtotime("-".$request->time." months"));
                    $feedback_where = " AND updated_at BETWEEN '".$lastYear."' AND NOW()";
                }
                
            }
            //echo $feedback_where;exit();
            $feedback_type_graph = DB::select("SELECT (SELECT COUNT(*) FROM feedback WHERE type=1 $feedback_where) AS perform_count,
                (SELECT COUNT(*) FROM feedback WHERE type=2 $feedback_where) AS bug_count,
                (SELECT COUNT(*) FROM feedback WHERE type=3 $feedback_where) AS general_count");

            $html = view('admin.xhr.refresh_feedback_graph',['feedback_type_graph' => $feedback_type_graph[0]])->render();
            return response([
                                    'response_code' =>  '200',
                                    'response_msg'  =>  'Modal Fetched Successfully',
                                    'reponse_body'  =>  ['graph'         =>  $html],
                            ],200);
        }
    }

    public function refreshLoginLog(Request $request){
        $rules =[
                    'login_from' =>  'required|in:facebook,google,twitter,apple,web,ios,android',
                    'user_id'       =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
          return response([
                            'response_code' =>  '201',
                            'response_msg'  =>  'Not enough details are provided',
                            'reponse_body'  =>  "null",
                            'errors'    =>  $validate->errors()
                          ], 401);
        }else{
            //$login_from = 'web';
            $check = DB::table('login_details')
                        ->where('login_user_id','=',$request->user_id)
                        ->where('login_from','=',$request->login_from)
                        ->where('login_date','=',date('Y-m-d'))
                        ->first();
            if(!$check){
                DB::table('login_details')->insert([
                'login_user_id'     =>  $request->user_id,
                'login_date'        =>  date('Y-m-d'),
                'login_from'        =>  $request->login_from
              ]);
            }else{
                DB::table('login_details')->where('_login_id',$check->_login_id)->update(['login_time' => date('Y-m-d H:i:s')]);
            }

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Log refreshed successfully',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
        }
    }

    public function comingSoonModal(Request $request){
        $html = view('xhr.coming_soon_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function adminProfileModal(Request $request){
        $html = view('admin.xhr.admin_profile_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function adminProfileUpdate(Request $request){
        $rules =[
                    'admin_name'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $admin_id = $request->session()->get('admin_data')->id;
            DB::table('admin_users')->where('id',$admin_id)->update(['admin_name' => $request->admin_name]);
            $admin_data = DB::table('admin_users')->where('id',$admin_id)->first();
            $request->session()->put('admin_data', $admin_data);

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Profile is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
        }
    }

    function adminChangePasswordModal(Request $request){
        $html = view('admin.xhr.change_password_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function adminChangePassword(Request $request){
        $rules =[
                    'old_password'  =>  'required',
                    'new_password'  =>  'required',
                    'cnf_password'  =>  'required|same:new_password'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $admin_id = $request->session()->get('admin_data')->id;
            $pass =  DB::table('admin_users')->where('id',$admin_id)->first();
            if(Hash::check($request->old_password,$pass->admin_password)){
                DB::table('admin_users')->where('id',$admin_id)->update([
                    'admin_password'=>Hash::make($request->new_password)
                    ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'password updated successfully',
                                'reponse_body'  =>  NULL,
                                'close'         =>  TRUE
                            ], 200);
               // $this->getUsers($request);
            }
            return response([
                            'response_code' =>  '203',
                            'response_msg'  =>  'Current password is wrong',
                            'reponse_body'  =>  'null'
                        ], 401);
        }
    }

    function activeInactiveUser(Request $request){
        $rules =[
                    '_user_id'          =>  'required',
                    'user_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $user_is_active = $request->user_is_active==1 ? 0 : 1;
            User::where('id',$request->_user_id)->update(['user_is_active' => $user_is_active]);
            $active_inactive = $user_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'User '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function adminLogout(Request $request){
        if ($request->session()->has('admin_data')) {
            $request->session()->forget('admin_data');
        }
        //echo 'logOut called';
        return redirect('admin/login');
    }

    function login(Request $request){
        if ($request->session()->has('userdata')) {
            //return redirect('home');
            $request->session()->forget('userdata');
            $request->session()->flush();
        }
        $terms = Page::find(2);
        $privacy = Page::find(3);
        $cookie = Page::find(4);
        return View::make("login")->with(['terms'=>$terms,'privacy'=>$privacy,'cookie'=>$cookie]);
    }

    function privacyPolicy(Request $request){
        $privacy = Page::find(3);
        return View::make("privacy-policy")->with(['privacy'=>$privacy]);
    }

    function termsAndCondition(Request $request){
        $terms = Page::find(2);
        return View::make("terms-and-condition")->with(['terms'=>$terms]);
    }

    function landing(Request $request){
        $about = Page::find(1);
        $contact_us = DB::table('cms_contact')->find(1);
        $testimonials = DB::table('testimonials')->where('testimonial_is_active','=',1)->orderBy('testimonial_is_primary','desc')->get();
        $life_event_categories = DB::table('life_event_categories')->get();
        return View::make("index")->with(['about'=>$about, 'contact_us'=> $contact_us, 'testimonials' => $testimonials,'life_event_categories' => $life_event_categories]);
    }

    public function playVideoModal(Request $request){
        $rules =[
                    'id'    =>  'required|exists:life_event_categories,id'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{

            $category_data = DB::table('life_event_categories')->where('id',$request->id)->first();
            if($category_data){
                DB::table('life_event_categories')->where('id',$category_data->id)->update(['event_click_count' => ($category_data->event_click_count +1)]);
                $html = view('xhr.play_video_modal',['category_data'=>$category_data])->render();
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
            }else{
                return response([
                            'response_code' =>  '203',
                            'response_msg'  =>  'Invalid data supplied',
                            'reponse_body'  =>  'null'
                        ], 401);
            }
        }
    }

    function index(Request $request){
        if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
        // $this->_checkLogin($request);
    	/*echo '<pre>';
    	print_r($request->session()->get('userdata'));
    	echo '</pre>';exit();*/
        $response = [];
        $response['countries'] = DB::table('countries')->orderBy('country_name', 'asc')->get();
        $user_id = $request->session()->get('userdata')['id'];
        $user_data = User::find($user_id);
        $counts = DB::select("SELECT 
        (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$user_data->id."') AS life_events_count,
        (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$user_data->id."' OR request_to_user='".$user_data->id."')) AS connection_count,

        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=1 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS personal_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_personal."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=1 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS total_personal_count,

        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=2 AND (feed_user1 = '".$user_data->id."' OR feed_user2 = '".$user_data->id."')) AS dating_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_dating."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=2 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS total_dating_count,

        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=3 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS adoption_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_adoption."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=3 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS total_adoption_count,

        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=4 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS travel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_travel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=4 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS total_travel_count,

        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=5 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS military_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_military."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=5 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS total_military_count,

        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=6 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS school_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_education."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=6 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS total_school_count,

        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=7 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS career_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_work."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=7 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS total_career_count,

        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=8 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS pets_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_pet."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=8 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS total_pets_count,

        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=9 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS lost_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_lostfound."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=9 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS total_lost_count,

        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=10 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS doppel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_droppel."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=10 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS total_doppel_count,

        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=11 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS username_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_username."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=11 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS total_username_count,

        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND feed_match_count>='".$user_data->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=12 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS activity_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_match_count>='".$user_data->match_activity."' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_data->id."') AND feed_lifeevent=12 AND ((feed_user1 = '".$user_data->id."' AND feed_user1 IN(SELECT id FROM users)) OR (feed_user2 = '".$user_data->id."' AND feed_user2 IN(SELECT id FROM users)))) AS total_activity_count,

        (SELECT SUM(personal_count + dating_count + adoption_count + travel_count + military_count + school_count + career_count + pets_count + lost_count + doppel_count + username_count + activity_count)) AS matched_count
        ");
        /*$where = " AND (feed_type='connected' OR ((feed_lifeevent=1 AND feed_match_count>='".$user_data->match_personal."') OR (feed_lifeevent=2 AND feed_match_count>='".$user_data->match_dating."') OR (feed_lifeevent=3 AND feed_match_count>='".$user_data->match_adoption."') OR (feed_lifeevent=4 AND feed_match_count>='".$user_data->match_travel."') OR (feed_lifeevent=5 AND feed_match_count>='".$user_data->match_military."') OR (feed_lifeevent=6 AND feed_match_count>='".$user_data->match_education."') OR (feed_lifeevent=7 AND feed_match_count>='".$user_data->match_work."') OR (feed_lifeevent=8 AND feed_match_count>='".$user_data->match_pet."')))";*/

        $limit = $request->limit ? $request->limit : 10;
        $offset = $request->offset ? $request->offset : 0;

        $select = "SELECT feeds.*, life_event_categories.event_type AS feed_event_type,
                    u1.profile_photo_path AS user1_profile_photo, u1.firstName AS user1_firstName, u1.lastName AS user1_lastName, u1.quick_blox_id AS user1_quick_blox_id, u2.profile_photo_path AS user2_profile_photo, 
                    u2.firstName AS user2_firstName, u2.lastName AS user2_lastName,u2.quick_blox_id AS user2_quick_blox_id, 
                    (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 1 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))), 1, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 0 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))),2,0)))) AS is_connected FROM feeds 
                    INNER JOIN users u1 ON u1.id=feeds.feed_user1
                    INNER JOIN users u2 ON u2.id=feeds.feed_user2
                    INNER JOIN life_event_categories ON feeds.feed_lifeevent=life_event_categories.id
                    WHERE _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_id."') AND feed_user1 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id='".$user_id."') AND feed_user2 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id='".$user_id."') AND ((feed_lifeevent=1 AND feed_match_count>='".$user_data->match_personal."') OR (feed_lifeevent=2 AND feed_match_count>='".$user_data->match_dating."') OR (feed_lifeevent=3 AND feed_match_count>='".$user_data->match_adoption."') OR (feed_lifeevent=4 AND feed_match_count>='".$user_data->match_travel."') OR (feed_lifeevent=5 AND feed_match_count>='".$user_data->match_military."') OR (feed_lifeevent=6 AND feed_match_count>='".$user_data->match_education."') OR (feed_lifeevent=7 AND feed_match_count>='".$user_data->match_work."') OR (feed_lifeevent=8 AND feed_match_count>='".$user_data->match_pet."') OR (feed_lifeevent=9 AND feed_match_count>='".$user_data->match_lostfound."') OR (feed_lifeevent=11 AND feed_match_count>='".$user_data->match_username."')) ORDER BY _feed_id DESC LIMIT $offset,$limit
                                ";
        $select2 = "SELECT feeds.*, life_event_categories.event_type AS feed_event_type,
                    u1.profile_photo_path AS user1_profile_photo, u1.firstName AS user1_firstName, u1.lastName AS user1_lastName, u1.quick_blox_id AS user1_quick_blox_id, u2.profile_photo_path AS user2_profile_photo, 
                    u2.firstName AS user2_firstName, u2.lastName AS user2_lastName,u2.quick_blox_id AS user2_quick_blox_id, 
                    (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 1 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))), 1, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 0 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))),2,0)))) AS is_connected FROM feeds 
                    INNER JOIN users u1 ON u1.id=feeds.feed_user1
                    INNER JOIN users u2 ON u2.id=feeds.feed_user2
                    INNER JOIN life_event_categories ON feeds.feed_lifeevent=life_event_categories.id
                    WHERE _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_id."') AND feed_user1 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id='".$user_id."') AND feed_user2 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id='".$user_id."') AND ((feed_lifeevent=1 AND feed_match_count>='".$user_data->match_personal."') OR (feed_lifeevent=2 AND feed_match_count>='".$user_data->match_dating."') OR (feed_lifeevent=3 AND feed_match_count>='".$user_data->match_adoption."') OR (feed_lifeevent=4 AND feed_match_count>='".$user_data->match_travel."') OR (feed_lifeevent=5 AND feed_match_count>='".$user_data->match_military."') OR (feed_lifeevent=6 AND feed_match_count>='".$user_data->match_education."') OR (feed_lifeevent=7 AND feed_match_count>='".$user_data->match_work."') OR (feed_lifeevent=8 AND feed_match_count>='".$user_data->match_pet."') OR (feed_lifeevent=9 AND feed_match_count>='".$user_data->match_lostfound."') OR (feed_lifeevent=11 AND feed_match_count>='".$user_data->match_username."')) ORDER BY _feed_id DESC";

                /*(((feed_user1="'.$user_id.'" OR feed_user1 IN(SELECT DISTINCT follow_following FROM user_follows WHERE follow_user="'.$user_id.'")) AND feed_user1 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$user_id.'") AND feed_user2 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$user_id.'") AND (feed_user2 IN(SELECT DISTINCT follow_following FROM user_follows WHERE follow_user="'.$user_id.'") OR feed_user2 NOT IN(SELECT DISTINCT unfollow_to_user FROM user_unfollows WHERE unfollow_from_user="'.$user_id.'"))) OR (feed_user2="'.$user_id.'" AND feed_user1 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$user_id.'") AND feed_user2 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$user_id.'") AND (feed_user1 IN(SELECT DISTINCT follow_following FROM user_follows WHERE follow_user="'.$user_id.'") OR feed_user1 NOT IN(SELECT DISTINCT unfollow_to_user FROM user_unfollows WHERE unfollow_from_user="'.$user_id.'")))) AND */
            //echo $select;exit;

        $feeds = DB::select($select);
        $feeds2 = DB::select($select2);
        $new_feeds = [];
        if($feeds){
            foreach($feeds as $key => $val){
                $feed_images = DB::table('feed_images')->where('fi_feed_id',$val->_feed_id)->get();
                $val->feed_images = $feed_images;
                array_push($new_feeds, $val);
            }
        }
        $items = DB::table('items')->orderBy('item_name', 'asc')->get();
        $response['indoors'] = DB::table('indoors')->orderBy('indoor_name', 'asc')->get();
        $response['inmotions'] = DB::table('inmotions')->orderBy('inmotion_name', 'asc')->get();
        $response['materials'] = DB::table('materials')->orderBy('material_name', 'asc')->get();
        $response['outdoors'] = DB::table('outdoors')->orderBy('outdoor_name', 'asc')->get();
        $response['others'] = [];
        
        $types = [];
        foreach ($items as $key => $value) {
            $item_sizes = DB::table('item_sizes')->where('is_item_id',$value->_item_id)->orderBy('is_name', 'asc')->get();
            $value->item_sizes = $item_sizes;
            array_push($types, $value);
        }
        $response['items'] = $types;
        $response['feeds'] = $new_feeds;
        $response['user_quick_blox_id'] = $user_data->quick_blox_id;
        $response['user_data'] = $user_data;
        /*$feed_count = DB::select('SELECT COUNT(_feed_id) AS feed_count FROM feeds WHERE feed_type="matched"');*/
        //print_r($feed_count);exit();
        $response['feed_count'] = $feeds2 ? count($feeds2) : 0;
        $response['counts'] = $counts;        
        return View::make("home")->with($response);
    }

    function feedDetails(Request $request){
        if (!$request->feed) {
            return redirect('index');
        }
        $feed_id = $request->feed;
        $select='SELECT feeds.*, life_event_categories.event_type AS feed_event_type,
                    u1.profile_photo_path AS user1_profile_photo, u1.firstName AS user1_firstName, u1.lastName AS user1_lastName, u1.quick_blox_id AS user1_quick_blox_id, u2.profile_photo_path AS user2_profile_photo, 
                    u2.firstName AS user2_firstName, u2.lastName AS user2_lastName,u2.quick_blox_id AS user2_quick_blox_id, 
                    (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 1 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))), 1, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 0 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))),2,0)))) AS is_connected FROM feeds 
                    INNER JOIN users u1 ON u1.id=feeds.feed_user1
                    INNER JOIN users u2 ON u2.id=feeds.feed_user2
                    INNER JOIN life_event_categories ON feeds.feed_lifeevent=life_event_categories.id
                    WHERE _feed_id="'.$feed_id.'"
                ';
            //echo $select;exit;

        $feeds = DB::select($select);
        $feeds = $feeds ? $feeds[0] : [];
        $feed_images = [];
        if($feeds){
            $feed_images = DB::table('feed_images')->where('fi_feed_id',$feeds->_feed_id)->get();
            $feeds->feed_images = $feed_images;
        }else{
            return redirect('index');
        }

        $response['feed'] = $feeds;
        return View::make("feed_details")->with($response);
    }

    function shareFeedModal(Request $request){
        set_time_limit(6000);
        $rules =[
                    'feed_id'       =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            /*$feed_data = DB::table('feeds')->where('_feed_id',$request->feed_id)->first();*/
            $select='SELECT feeds.*, life_event_categories.event_type AS feed_event_type,
                        u1.profile_photo_path AS user1_profile_photo, u1.firstName AS user1_firstName, u1.lastName AS user1_lastName, u1.quick_blox_id AS user1_quick_blox_id, u2.profile_photo_path AS user2_profile_photo, 
                        u2.firstName AS user2_firstName, u2.lastName AS user2_lastName,u2.quick_blox_id AS user2_quick_blox_id, 
                        (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 1 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))), 1, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 0 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))),2,0)))) AS is_connected FROM feeds 
                        INNER JOIN users u1 ON u1.id=feeds.feed_user1
                        INNER JOIN users u2 ON u2.id=feeds.feed_user2
                        INNER JOIN life_event_categories ON feeds.feed_lifeevent=life_event_categories.id
                        WHERE _feed_id="'.$request->feed_id.'"
                    ';
                //echo $select;exit;

            $feeds = DB::select($select);

            $feed_data = $feeds ? $feeds[0] : [];
            $user_data = DB::table('users')->where('id',$request->session()->get('userdata')['id'])->first();

            if($feed_data){
                if(!$feed_data->feed_screenshot){
                    //print_r($feed_data);exit();
                    $width = 500;
                    $height = 400;

                    // creating a canvas
                    $canvas = Image::canvas($width, $height, '#fff');

                    // pass the right full path to the file. Remember that $path is a path inside app/public !
                    $image = Image::make(public_path("img/full_size_logo_updated2.png"))->resize($width, $height, 
                        function ($constraint) {
                            $constraint->aspectRatio();
                    });

                    $canvas->insert($image, 'top');
                    //echo 'here1 ';

                    if($feed_data->user1_profile_photo){
                        $file = $feed_data->user1_profile_photo;
                        $file_headers = @get_headers($file);
                        if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                            $user1_img = 'new-design/img/profile_placeholder.jpg';
                        }
                        else {
                            $user1_img = 'profilePictures/'.basename($feed_data->user1_profile_photo);
                        }
                    }else{
                        $user1_img = 'new-design/img/profile_placeholder.jpg';
                    }



                    $image2 = Image::make(public_path($user1_img))->resize(250, 250);

                    $canvas->insert($image2, 'bottom-left');

                    //echo 'here2 ';

                    if($feed_data->user2_profile_photo){
                        $file2 = $feed_data->user2_profile_photo;
                        $file_headers2 = @get_headers($file2);
                        if(!$file_headers2 || $file_headers2[0] == 'HTTP/1.1 404 Not Found') {
                            $user2_img = 'new-design/img/profile_placeholder.jpg';
                        }
                        else {
                            $user2_img = 'profilePictures/'.basename($feed_data->user2_profile_photo);
                        }
                    }else{
                        $user2_img = 'new-design/img/profile_placeholder.jpg';
                    }

                    /*$user2_img = $feeds->user2_profile_photo ? $feeds->user2_profile_photo : public_path('new-design/img/profile_placeholder.jpg');*/

                    $image3 = Image::make(public_path($user2_img))->resize(250, 250);

                    $canvas->insert($image3, 'bottom-right');

                    //echo 'here3 ';

                    $image4 = Image::make(public_path("img/plus_button_with_imageholder6.png"))->resize($width, $height, 
                        function ($constraint) {
                            $constraint->aspectRatio();
                    });

                    $canvas->insert($image4, 'bottom');

                    // pass the full path. Canvas overwrites initial image with a logo
                    $canvas->save(public_path("feedScreenShots/".$request->feed_id.".png"));

                    //echo 'here4 ';

                    DB::table('feeds')->where('_feed_id',$request->feed_id)->update(['feed_screenshot' => "feedScreenShots/".$request->feed_id.".png"]);
                }
                $share_url = url('feed-details?feed='.$request->feed_id);
                if($user_data->id==$feed_data->feed_user1){
                    $to_user_data = DB::table('users')
                                        ->join('device_tokens','users.id','=','device_tokens.device_user_id')
                                        ->join('notification_settings','users.id','=','notification_settings.ns_user_id')
                                        ->select('device_tokens.*','notification_settings.ns_shares')
                                        ->where('users.id',$feed_data->feed_user2)
                                        ->get();
                    if($to_user_data){
                        foreach($to_user_data as $to_user){
                            if($to_user->ns_shares==1){
                                $title = 'Feed Share';
                                $body = $user_data->firstName.' '.$user_data->lastName.' shared your feed '.$share_url;
                                $noti_response = sendNotification($to_user->device_type,$to_user->device_token,$title,$body,['type' => 'share']);
                                $notification_data =[
                                                        'notification_user'     =>  $to_user->device_user_id,
                                                        'notification_device'   =>  $to_user->_device_id,
                                                        'notification_title'    =>  $title,
                                                        'notification_content'  =>  $body,
                                                        'notification_status'   =>  $noti_response['success']==1 ? 'success' : 'failed',
                                                        'notification_response' =>  json_encode($noti_response)
                                                    ];
                                DB::table('notifications')->insert($notification_data);
                            }
                        }
                    }
                }else if($user_data->id==$feed_data->feed_user2){
                    $to_user_data = DB::table('users')
                                        ->join('device_tokens','users.id','=','device_tokens.device_user_id')
                                        ->join('notification_settings','users.id','=','notification_settings.ns_user_id')
                                        ->select('device_tokens.*','notification_settings.ns_shares')
                                        ->where('users.id',$feed_data->feed_user1)
                                        ->get();
                    if($to_user_data){
                        foreach($to_user_data as $to_user){
                            if($to_user->ns_shares==1){
                                $title = 'Feed Share';
                                $body = $user_data->firstName.' '.$user_data->lastName.' shared your feed '.$share_url;
                                $noti_response = sendNotification($to_user->device_type,$to_user->device_token,$title,$body,['type' => 'share']);
                                $notification_data =[
                                                        'notification_user'     =>  $to_user->device_user_id,
                                                        'notification_device'   =>  $to_user->_device_id,
                                                        'notification_title'    =>  $title,
                                                        'notification_content'  =>  $body,
                                                        'notification_status'   =>  $noti_response['success']==1 ? 'success' : 'failed',
                                                        'notification_response' =>  json_encode($noti_response)
                                                    ];
                                DB::table('notifications')->insert($notification_data);
                            }
                        }
                    }
                }else{
                    $to_user_data1 = DB::table('users')
                                        ->join('device_tokens','users.id','=','device_tokens.device_user_id')
                                        ->join('notification_settings','users.id','=','notification_settings.ns_user_id')
                                        ->select('device_tokens.*','notification_settings.ns_shares')
                                        ->where('users.id',$feed_data->feed_user1)
                                        ->get();
                    $to_user_data2 = DB::table('users')
                                        ->join('device_tokens','users.id','=','device_tokens.device_user_id')
                                        ->join('notification_settings','users.id','=','notification_settings.ns_user_id')
                                        ->select('device_tokens.*','notification_settings.ns_shares')
                                        ->where('users.id',$feed_data->feed_user2)
                                        ->get();
                    if($to_user_data1){
                        foreach($to_user_data1 as $to_user){
                            if($to_user->ns_shares==1){
                                $title = 'Feed Share';
                                $body = $user_data->firstName.' '.$user_data->lastName.' shared your feed '.$share_url;
                                $noti_response = sendNotification($to_user->device_type,$to_user->device_token,$title,$body,['type' => 'share']);
                                $notification_data =[
                                                        'notification_user'     =>  $to_user->device_user_id,
                                                        'notification_device'   =>  $to_user->_device_id,
                                                        'notification_title'    =>  $title,
                                                        'notification_content'  =>  $body,
                                                        'notification_status'   =>  $noti_response['success']==1 ? 'success' : 'failed',
                                                        'notification_response' =>  json_encode($noti_response)
                                                    ];
                                DB::table('notifications')->insert($notification_data);
                            }
                        }
                    }
                    if($to_user_data2){
                        foreach($to_user_data2 as $to_user){
                            if($to_user->ns_shares==1){
                                $title = 'Feed Share';
                                $body = $user_data->firstName.' '.$user_data->lastName.' shared your feed '.$share_url;
                                $noti_response = sendNotification($to_user->device_type,$to_user->device_token,$title,$body,['type' => 'share']);
                                $notification_data =[
                                                        'notification_user'     =>  $to_user->device_user_id,
                                                        'notification_device'   =>  $to_user->_device_id,
                                                        'notification_title'    =>  $title,
                                                        'notification_content'  =>  $body,
                                                        'notification_status'   =>  $noti_response['success']==1 ? 'success' : 'failed',
                                                        'notification_response' =>  json_encode($noti_response)
                                                    ];
                                DB::table('notifications')->insert($notification_data);
                            }
                        }
                    }
                }
                

                $html = view('xhr.share_feed_modal',['feed_data' => $feed_data,'user_data'=>$user_data])->render();
                return response([
                                    'response_code' =>  '200',
                                    'response_msg'  =>  'Modal Fetched Successfully',
                                    'reponse_body'  =>  'null',
                                    'modal'         =>  $html
                                ],200);
            }else{
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
        }
    }

    function saveFeedScreenFromURL(Request $request){
        /*set_time_limit(6000);
        $url = $request->url;

        $opts = array('http'=>array('header' => "User-Agent:MyAgent/1.0\r\n")); 
        //Basically adding headers to the request
        $context = stream_context_create($opts);
  
        $file_name = 'feedScreenShots/'.$request->feed_id.'.png';
        $file = public_path($file_name);
          
        // Function to write image into file
        file_put_contents($file, file_get_contents($url,false,$context));
          
        echo "File downloaded!";*/
        set_time_limit(6000);
        $rules =[
                    'feed_id'           =>  'required',
                    'user_id'           =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{

            $feed_id = $request->feed_id;
            $user_data = DB::table('users')->where('id',$request->user_id)->first();
            $select='SELECT feeds.*, life_event_categories.event_type AS feed_event_type,
                        u1.profile_photo_path AS user1_profile_photo, u1.firstName AS user1_firstName, u1.lastName AS user1_lastName, u1.quick_blox_id AS user1_quick_blox_id, u2.profile_photo_path AS user2_profile_photo, 
                        u2.firstName AS user2_firstName, u2.lastName AS user2_lastName,u2.quick_blox_id AS user2_quick_blox_id, 
                        (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 1 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))), 1, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 0 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))),2,0)))) AS is_connected FROM feeds 
                        INNER JOIN users u1 ON u1.id=feeds.feed_user1
                        INNER JOIN users u2 ON u2.id=feeds.feed_user2
                        INNER JOIN life_event_categories ON feeds.feed_lifeevent=life_event_categories.id
                        WHERE _feed_id="'.$feed_id.'"
                    ';
                //echo $select;exit;

            $feeds = DB::select($select);

            $feeds = $feeds ? $feeds[0] : [];

            if($feeds){
                /*print_r($feeds);*/
                $feed_data = $feeds;
                $width = 500;
                $height = 400;

                // creating a canvas
                $canvas = Image::canvas($width, $height, '#fff');

                // pass the right full path to the file. Remember that $path is a path inside app/public !
                $image = Image::make(public_path("img/full_size_logo_updated2.png"))->resize($width, $height, 
                    function ($constraint) {
                        $constraint->aspectRatio();
                });

                $canvas->insert($image, 'top');

                if($feeds->user1_profile_photo){
                    $file = $feeds->user1_profile_photo;
                    $file_headers = @get_headers($file);
                    if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                        $user1_img = 'new-design/img/profile_placeholder.jpg';
                    }
                    else {
                        $user1_img = 'profilePictures/'.basename($feeds->user1_profile_photo);
                    }
                }else{
                    $user1_img = 'new-design/img/profile_placeholder.jpg';
                }

                $image2 = Image::make(public_path($user1_img))->resize(250, 250, 
                    /*function ($constraint) {
                        $constraint->aspectRatio();
                }*/);

                $canvas->insert($image2, 'bottom-left');

                if($feeds->user2_profile_photo){
                    $file2 = $feeds->user2_profile_photo;
                    $file_headers2 = @get_headers($file2);
                    if(!$file_headers2 || $file_headers2[0] == 'HTTP/1.1 404 Not Found') {
                        $user2_img = 'new-design/img/profile_placeholder.jpg';
                    }
                    else {
                        $user2_img = 'profilePictures/'.basename($feeds->user2_profile_photo);
                    }
                }else{
                    $user2_img = 'new-design/img/profile_placeholder.jpg';
                }

                /*$user2_img = $feeds->user2_profile_photo ? $feeds->user2_profile_photo : public_path('new-design/img/profile_placeholder.jpg');*/

                $image3 = Image::make(public_path($user2_img))->resize(250, 250, 
                    /*function ($constraint) {
                        $constraint->aspectRatio();
                }*/);

                $canvas->insert($image3, 'bottom-right');

                $image4 = Image::make(public_path("img/plus_button_with_imageholder6.png"))->resize($width, $height, 
                    function ($constraint) {
                        $constraint->aspectRatio();
                });

                $canvas->insert($image4, 'bottom');

                // pass the full path. Canvas overwrites initial image with a logo
                $canvas->save(public_path("feedScreenShots/".$request->feed_id.".png"));

                DB::table('feeds')->where('_feed_id',$request->feed_id)->update(['feed_screenshot' => "feedScreenShots/".$request->feed_id.".png"]);

                $title = $feeds->user1_firstName.' '.$feeds->user1_lastName.' is '.$feeds->feed_type.' with '.$feeds->user2_firstName.' '.$feeds->user2_lastName.' in WhatsCommon';

                $share_url  = url('feed-details?feed='.$request->feed_id);

                if($user_data->id==$feed_data->feed_user1){
                    $to_user_data = DB::table('users')
                                        ->join('device_tokens','users.id','=','device_tokens.device_user_id')
                                        ->join('notification_settings','users.id','=','notification_settings.ns_user_id')
                                        ->select('device_tokens.*','notification_settings.ns_shares')
                                        ->where('users.id',$feed_data->feed_user2)
                                        ->get();
                    if($to_user_data){
                        foreach($to_user_data as $to_user){
                            if($to_user->ns_shares==1){
                                $title = 'Feed Share';
                                $body = $user_data->firstName.' '.$user_data->lastName.' shared your feed '.$share_url;
                                $noti_response = sendNotification($to_user->device_type,$to_user->device_token,$title,$body,['type' => 'share']);
                                $notification_data =[
                                                        'notification_user'     =>  $to_user->device_user_id,
                                                        'notification_device'   =>  $to_user->_device_id,
                                                        'notification_title'    =>  $title,
                                                        'notification_content'  =>  $body,
                                                        'notification_status'   =>  $noti_response['success']==1 ? 'success' : 'failed',
                                                        'notification_response' =>  json_encode($noti_response)
                                                    ];
                                DB::table('notifications')->insert($notification_data);
                            }
                        }
                    }
                }else if($user_data->id==$feed_data->feed_user2){
                    $to_user_data = DB::table('users')
                                        ->join('device_tokens','users.id','=','device_tokens.device_user_id')
                                        ->join('notification_settings','users.id','=','notification_settings.ns_user_id')
                                        ->select('device_tokens.*','notification_settings.ns_shares')
                                        ->where('users.id',$feed_data->feed_user1)
                                        ->get();
                    if($to_user_data){
                        foreach($to_user_data as $to_user){
                            if($to_user->ns_shares==1){
                                $title = 'Feed Share';
                                $body = $user_data->firstName.' '.$user_data->lastName.' shared your feed '.$share_url;
                                $noti_response = sendNotification($to_user->device_type,$to_user->device_token,$title,$body,['type' => 'share']);
                                $notification_data =[
                                                        'notification_user'     =>  $to_user->device_user_id,
                                                        'notification_device'   =>  $to_user->_device_id,
                                                        'notification_title'    =>  $title,
                                                        'notification_content'  =>  $body,
                                                        'notification_status'   =>  $noti_response['success']==1 ? 'success' : 'failed',
                                                        'notification_response' =>  json_encode($noti_response)
                                                    ];
                                DB::table('notifications')->insert($notification_data);
                            }
                        }
                    }
                }else{
                    $to_user_data1 = DB::table('users')
                                        ->join('device_tokens','users.id','=','device_tokens.device_user_id')
                                        ->join('notification_settings','users.id','=','notification_settings.ns_user_id')
                                        ->select('device_tokens.*','notification_settings.ns_shares')
                                        ->where('users.id',$feed_data->feed_user1)
                                        ->get();
                    $to_user_data2 = DB::table('users')
                                        ->join('device_tokens','users.id','=','device_tokens.device_user_id')
                                        ->join('notification_settings','users.id','=','notification_settings.ns_user_id')
                                        ->select('device_tokens.*','notification_settings.ns_shares')
                                        ->where('users.id',$feed_data->feed_user2)
                                        ->get();
                    if($to_user_data1){
                        foreach($to_user_data1 as $to_user){
                            if($to_user->ns_shares==1){
                                $title = 'Feed Share';
                                $body = $user_data->firstName.' '.$user_data->lastName.' shared your feed '.$share_url;
                                $noti_response = sendNotification($to_user->device_type,$to_user->device_token,$title,$body,['type' => 'share']);
                                $notification_data =[
                                                        'notification_user'     =>  $to_user->device_user_id,
                                                        'notification_device'   =>  $to_user->_device_id,
                                                        'notification_title'    =>  $title,
                                                        'notification_content'  =>  $body,
                                                        'notification_status'   =>  $noti_response['success']==1 ? 'success' : 'failed',
                                                        'notification_response' =>  json_encode($noti_response)
                                                    ];
                                DB::table('notifications')->insert($notification_data);
                            }
                        }
                    }
                    if($to_user_data2){
                        foreach($to_user_data2 as $to_user){
                            if($to_user->ns_shares==1){
                                $title = 'Feed Share';
                                $body = $user_data->firstName.' '.$user_data->lastName.' shared your feed '.$share_url;
                                $noti_response = sendNotification($to_user->device_type,$to_user->device_token,$title,$body,['type' => 'share']);
                                $notification_data =[
                                                        'notification_user'     =>  $to_user->device_user_id,
                                                        'notification_device'   =>  $to_user->_device_id,
                                                        'notification_title'    =>  $title,
                                                        'notification_content'  =>  $body,
                                                        'notification_status'   =>  $noti_response['success']==1 ? 'success' : 'failed',
                                                        'notification_response' =>  json_encode($noti_response)
                                                    ];
                                DB::table('notifications')->insert($notification_data);
                            }
                        }
                    }
                }

                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Share URL Created Successfully',
                                'reponse_body'  =>  [
                                                        'share_url'         =>  $share_url,
                                                        'feed_screenshot'   =>  url("feedScreenShots/".$request->feed_id.".png"),
                                                        'title'             =>  $title 
                                                    ]
                            ], 200);

            }else{
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
        }
    }

    function saveFeedScreen(Request $request){
        /*if($request->imgBase64){
            echo $request->imgBase64;
        }*/

        $rules =[
                    'imgBase64'         =>  'required',
                    'feed_id'           =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{

            $image_parts = explode(";base64,", $request->imgBase64);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $file_name = 'feedScreenShots/'.$request->feed_id.'.'.$image_type;
            $file = public_path($file_name);
            file_put_contents($file, $image_base64);

            DB::table('feeds')->where('_feed_id',$request->feed_id)->update(['feed_screenshot'=>$file_name]);
        }

    }

    function userProfile(Request $request){
        if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
        /*$user_id = $request->session()->get('userdata')['id'];*/
        $user_id = $request->user ? $request->user : NULL;
        if($user_id && $user_id!=$request->session()->get('userdata')['id']){
            $feed_data = NULL;
            if($request->feed){
                $select = 'SELECT feeds.*, life_event_categories.event_type AS feed_event_type,
                    u1.profile_photo_path AS user1_profile_photo, u1.firstName AS user1_firstName, u1.lastName AS user1_lastName, u1.quick_blox_id AS user1_quick_blox_id, u2.profile_photo_path AS user2_profile_photo, 
                    u2.firstName AS user2_firstName, u2.lastName AS user2_lastName,u2.quick_blox_id AS user2_quick_blox_id, 
                    (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 1 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))), 1, 0)) AS is_connected FROM feeds 
                    INNER JOIN users u1 ON u1.id=feeds.feed_user1
                    INNER JOIN users u2 ON u2.id=feeds.feed_user2
                    INNER JOIN life_event_categories ON feeds.feed_lifeevent=life_event_categories.id
                    WHERE (feed_user1="'.$user_id.'" OR feed_user2="'.$user_id.'") AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id="'.$user_id.'") AND _feed_id="'.$request->feed.'"
                                ';

                $res = DB::select($select);
                $feed_data = $res ? $res[0] : NULL;
            }
            
            //$other_user = $feed_data->feed_user1==$user_id ? $feed_data->feed_user2 : $feed_data->feed_user1;
            $my_id = $request->session()->get('userdata')['id'];
            $result = DB::select("SELECT userdetails.*,users.username, users.quick_blox_id, users.created_at, users.show_connection_list, users.show_match_feed, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status =1 AND ((request_from_user=$my_id AND request_to_user=users.id) OR (request_from_user=users.id AND request_to_user=$my_id))), 1, 0)) AS is_connected,
                (SELECT IF ((SELECT COUNT(*) FROM user_follows WHERE follow_user=$my_id AND follow_following=userdetails.id) , 1, 0)) AS is_following,
                (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status =0 AND ((request_from_user=$my_id AND request_to_user=$user_id) OR (request_from_user=$user_id AND request_to_user=$my_id))), 1, 0)) AS is_pending_connection,
          (SELECT request_from_user FROM connection_requests WHERE request_status =0 AND ((request_from_user=$my_id AND request_to_user=$user_id) OR (request_from_user=$user_id AND request_to_user=$my_id))) AS request_from_user,
          (SELECT _request_id FROM connection_requests WHERE request_status =0 AND ((request_from_user=$my_id AND request_to_user=$user_id) OR (request_from_user=$user_id AND request_to_user=$my_id))) AS _request_id,
          (SELECT IF ((SELECT COUNT(*) FROM blocked_users WHERE bu_from_id=$my_id AND bu_to_id=$user_id) , 1, 0)) AS is_blocked,
                 (SELECT event_type FROM life_event_categories 
                 INNER JOIN feeds ON feeds.feed_lifeevent=life_event_categories.id
                 INNER JOIN connection_requests ON connection_requests.request_feed=feeds._feed_id
                 WHERE connection_requests.request_status=1
                 AND ((connection_requests.request_from_user=$my_id AND connection_requests.request_to_user=users.id) OR (connection_requests.request_from_user=users.id AND connection_requests.request_to_user=$my_id))
                 ) AS event_type,
                 (SELECT COUNT(DISTINCT feed_lifeevent) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=$my_id) AND ((feed_user1 = $my_id AND feed_user2 = userdetails.id) OR (feed_user1 = userdetails.id AND feed_user2 = $my_id))) AS events_type_count,
                 (SELECT GROUP_CONCAT(DISTINCT event_type) FROM life_event_categories WHERE life_event_categories.id IN(SELECT DISTINCT feed_lifeevent FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=$my_id) AND ((feed_user1 = $my_id AND feed_user2 = userdetails.id) OR (feed_user1 = userdetails.id AND feed_user2 = $my_id)) )) AS events_type_names,
                  users.profile_photo_path, countries.country_name, provinces.province_name, cities.city_name, (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS matched_count,
          (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id=userdetails.id) AS life_events_count,
          (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user=userdetails.id OR request_to_user=userdetails.id)) AS connection_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=1 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS personal_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=2 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS dating_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=3 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS adoption_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=4 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS travel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=5 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS military_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=6 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS school_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=7 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS career_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=8 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS pets_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=9 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS lost_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=10 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS doppel_count
          FROM userdetails
          INNER JOIN users ON users.id=userdetails.id
          LEFT JOIN countries ON userdetails.country=countries._country_id
          LEFT JOIN provinces ON userdetails.state = provinces._province_id
          LEFT JOIN cities ON userdetails.city=cities._city_id
          WHERE userdetails.id = $user_id");
            $connection = $result ? $result[0] : [];
            //echo '<pre>';print_r($connection);exit();
            $countries = DB::table('countries')->where('country_is_active',1)->orderBy('country_name','asc')->get();

            $where = ' AND (feed_user1="'.$user_id.'" OR feed_user2="'.$user_id.'")';
            $select2 = 'SELECT feeds.*, life_event_categories.event_type AS feed_event_type,
                    u1.id AS user1_id, u1.profile_photo_path AS user1_profile_photo, u1.firstName AS user1_firstName, u1.lastName AS user1_lastName, u1.quick_blox_id AS user1_quick_blox_id, 
                    u2.id AS user2_id, u2.profile_photo_path AS user2_profile_photo, u2.firstName AS user2_firstName, u2.lastName AS user2_lastName, u2.quick_blox_id AS user2_quick_blox_id, 
                    (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 1 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))), 1, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 0 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))),2,0)))) AS is_connected
                    FROM feeds 
                    INNER JOIN users u1 ON u1.id=feeds.feed_user1
                    INNER JOIN users u2 ON u2.id=feeds.feed_user2
                    INNER JOIN life_event_categories ON feeds.feed_lifeevent=life_event_categories.id
                        WHERE _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id="'.$my_id.'") AND feed_user1 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$my_id.'") AND feed_user2 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$my_id.'") AND feed_type="matched" '.$where.' ORDER BY _feed_id DESC
                      ';
            //echo $select;exit;
      
            $feeds2 = DB::select($select2);
            $new_feeds2 = [];
            if($feeds2){
              foreach($feeds2 as $key => $val){
                $feed_images = DB::table('feed_images')->where('fi_feed_id',$val->_feed_id)->get();
                $val->feed_images = $feed_images;
                array_push($new_feeds2, $val);
              }
            }
        
            return View::make("user_profile")->with(['connection'=>$connection, 'feeds' => $new_feeds2,'user_id' => $user_id,'countries' => $countries,'my_id'=>$my_id, 'feed_data' => $feed_data]);
        }else{
            return redirect('home');
        }
        
    }

    function myProfile(Request $request){
        if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
        $user_id = $request->session()->get('userdata')['id'];
        //dd($request->session()->get('userdata'));

        $user_data = User::find($user_id);
        $where = " AND ((feed_lifeevent=1 AND feed_match_count>='".$user_data->match_personal."') OR (feed_lifeevent=2 AND feed_match_count>='".$user_data->match_dating."') OR (feed_lifeevent=3 AND feed_match_count>='".$user_data->match_adoption."') OR (feed_lifeevent=4 AND feed_match_count>='".$user_data->match_travel."') OR (feed_lifeevent=5 AND feed_match_count>='".$user_data->match_military."') OR (feed_lifeevent=6 AND feed_match_count>='".$user_data->match_education."') OR (feed_lifeevent=7 AND feed_match_count>='".$user_data->match_work."') OR (feed_lifeevent=8 AND feed_match_count>='".$user_data->match_pet."') OR (feed_lifeevent=9 AND feed_match_count>='".$user_data->match_lostfound."') OR (feed_lifeevent=11 AND feed_match_count>='".$user_data->match_username."'))";

        $select = 'SELECT feeds.*, life_event_categories.event_type AS feed_event_type, u1.id AS user1_id,
                    u1.profile_photo_path AS user1_profile_photo, u1.firstName AS user1_firstName, u1.lastName AS user1_lastName, u1.quick_blox_id AS user1_quick_blox_id, u2.id AS user2_id, u2.profile_photo_path AS user2_profile_photo, u2.firstName AS user2_firstName, u2.lastName AS user2_lastName,u2.quick_blox_id AS user2_quick_blox_id, 
                    (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 1 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))), 1, 0)) AS is_connected FROM feeds 
                    INNER JOIN users u1 ON u1.id=feeds.feed_user1
                    INNER JOIN users u2 ON u2.id=feeds.feed_user2
                    INNER JOIN life_event_categories ON feeds.feed_lifeevent=life_event_categories.id
                    WHERE (feed_user1="'.$user_id.'" OR feed_user2="'.$user_id.'") AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id="'.$user_id.'") '.$where.' ORDER BY _feed_id DESC
                                ';
                    /*AND feed_user2 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$user_id.'") AND (feed_user2 IN(SELECT DISTINCT follow_following FROM user_follows WHERE follow_user="'.$user_id.'") OR feed_user2 NOT IN(SELECT DISTINCT unfollow_to_user FROM user_unfollows WHERE unfollow_from_user="'.$user_id.'"))) OR (feed_user2="'.$user_id.'" AND feed_user1 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$user_id.'") AND feed_user2 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$user_id.'") AND (feed_user1 IN(SELECT DISTINCT follow_following FROM user_follows WHERE follow_user="'.$user_id.'") OR feed_user1 NOT IN(SELECT DISTINCT unfollow_to_user FROM user_unfollows WHERE unfollow_from_user="'.$user_id.'")))) AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id="'.$user_id.'")*/
            //echo $select;exit;

        $feeds = DB::select($select);
        $new_feeds = [];
        if($feeds){
            foreach($feeds as $key => $val){
                $feed_images = DB::table('feed_images')->where('fi_feed_id',$val->_feed_id)->get();
                $val->feed_images = $feed_images;
                array_push($new_feeds, $val);
            }
        }
        $response = [];
        $response['feeds'] = $new_feeds;
        $response['user_quick_blox_id'] = $user_data->quick_blox_id;
        $premium_user = DB::select("SELECT IF((SELECT COUNT(id) FROM chosen_plans 
                                    WHERE cp_user_id='".$user_data->id."' AND cp_is_expired=0 
                                AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_premium");
        $user_data->user_is_premium = $premium_user[0]->is_premium ? $premium_user[0]->is_premium : 0;
        $response['user_data'] = $user_data;

        return View::make("my_profile")->with($response);
    }

    function feedback(Request $request){
        if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
        $user_id = $request->session()->get('userdata')['id'];
        $feedbacktypes = DB::table('feedbacktypes')->get();
        return View::make("feedback")->with(['feedbacktypes'=>$feedbacktypes]);
    }

    function contactUs(Request $request){
        if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
        $user_id = $request->session()->get('userdata')['id'];
        return View::make("contact_us");
    }

    function aboutUs(Request $request){
        if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
        $user_id = $request->session()->get('userdata')['id'];
        $page_info = Page::find(1);
        return View::make("about_us")->with(['page_info' => $page_info]);   
    }

    function premiumPlans(Request $request){
        if($request->user_id){
            $data = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
            ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
            ->leftJoin('cities','userdetails.city','=','cities._city_id')
            ->select('userdetails.*', 'countries.country_name', 'provinces.province_name', 'cities.city_name')
            ->where('userdetails.id', $request->user_id)
            ->first();

            $counts = DB::select("SELECT (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS matched_count,
        (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$request->user_id."') AS life_events_count,
        (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$request->user_id."' OR request_to_user='".$request->user_id."')) AS connection_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=1 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS personal_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=2 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS dating_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=3 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS adoption_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=4 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS travel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=5 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS military_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=6 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS school_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=7 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS career_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=8 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS pets_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=9 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS lost_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=10 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS doppel_count");

            $data_user = User::where('id',$request->user_id)->first();
            if($data && $data_user){
                $newData = array_merge($data->toArray(),$data_user->toArray());
                $newData['counts'] = $counts;
                $qb_session = qb_login($newData['email'],'12345678');
                if($qb_session && $qb_session->session){
                  $token = $qb_session->session->token;
                  $newData['qb_token'] = $token;
                }
                $request->session()->put('userdata', $newData);
            }else{
                return redirect('login');
            }
        }
        if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
        $user_id = $request->session()->get('userdata')['id'];
        $user_active_plans = DB::table('chosen_plans')
                                    ->join('premium_plans','premium_plans.id','=','chosen_plans.cp_plan_id')
                                    ->selectRaw('chosen_plans.*,premium_plans.plan_type')
                                    ->where('cp_user_id',$user_id)->where('cp_is_expired',0)->first();

        if(!empty($user_active_plans) && in_array($user_active_plans->cp_inapp_type,['google','apple','paypal','stripe']) && $user_active_plans->plan_type!='lifetime'){

            if($user_active_plans->cp_inapp_type=='google'){
                $response  = (new InAppPurchase)->googleSubscriptionCheck($user_active_plans->cp_inapp_productid,$user_active_plans->cp_inapp_token);
                if($response==FALSE){
                    DB::table('chosen_plans')->where('cp_user_id',$user_id)->update([
                        'cp_is_expired'     =>1,
                        'cp_expiry_date'    => date('Y-m-d')
                    ]);
                }
            }elseif($user_active_plans->cp_inapp_type=='paypal'){
                $res = getAccessToken();
                $accessToken = $res->access_token;
                $paypal_res = getSubscriptionDetails($user_active_plans->cp_inapp_productid,$accessToken);
                if($paypal_res->status!='ACTIVE'){
                    DB::table('chosen_plans')->where('cp_user_id',$user_id)->update([
                        'cp_is_expired'     =>1,
                        'cp_expiry_date'    => date('Y-m-d')
                    ]);
                }
            }else if($user_active_plans->cp_inapp_type=='stripe'){
                $response  = (new Payment)->checkStripeSubsription($user_active_plans->cp_inapp_productid);
                /*echo '<pre>';
                print_r($response);exit;*/
                if($response->status!='active'){
                    DB::table('chosen_plans')->where('cp_user_id',$user_id)->update([
                        'cp_is_expired'     =>1,
                        'cp_expiry_date'    => date('Y-m-d')
                    ]);
                }
            }
        }
        $plans = DB::table('premium_plans')
                        ->selectRaw('*,(SELECT IF((SELECT COUNT(id) FROM chosen_plans WHERE cp_user_id='.$user_id.' AND cp_plan_id=premium_plans.id AND cp_is_expired=0 AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0)) AS is_active,
                            (SELECT IF((SELECT COUNT(id) FROM chosen_plans WHERE cp_user_id='.$user_id.' AND cp_plan_id=premium_plans.id AND cp_is_expired=0 AND cp_inapp_type IS NOT NULL),1,0)) AS is_inapp,
                            (SELECT cp_inapp_type FROM chosen_plans WHERE cp_user_id='.$user_id.' AND cp_plan_id=premium_plans.id AND cp_is_expired=0) AS cp_inapp_type
                            ')
                        ->join('currencies', 'premium_plans.plan_currency', '=', 'currencies._currency_id')
                        ->get();
        $premium_user = DB::select("SELECT IF((SELECT COUNT(id) FROM chosen_plans 
  WHERE cp_user_id='".$user_id."'
  AND cp_is_expired=0 
  AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_premium,
  IF((SELECT COUNT(id) FROM chosen_plans 
                                    WHERE cp_user_id='".$user_id."' AND cp_plan_id=3 AND cp_is_expired=0 
                                AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0) AS is_lifetime");

        $plan_sess = NULL;

        if($request->session()->has('plan_sess') && $request->checkoutId && $request->transactionId){

            $plan_sess = $request->session()->get('plan_sess')['plan_id'];
            $request->session()->forget('plan_sess');
        }

        return View::make("premium_plans")->with(['premium_plans' => $plans,'is_lifetime' => $premium_user[0]->is_lifetime ? $premium_user[0]->is_lifetime : 0,'plan_sess'=>$plan_sess]);   
    }

    function createStripePayment(Request $request){

        $rules =[
                    'plan_id'       =>  'required',
                    'payment_for'   =>  'required',
                    'redirect_url'  =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $plan_data = DB::table('premium_plans')->where('id',$request->plan_id)->first();
            if($plan_data){
                $checkout_url = NULL;
                $evaluate = NULL;
                $close = 'select-payment-for-plans-modal';
                if($plan_data->plan_type=='lifetime'){
                    $response  = (new Payment)->createStripePayment($plan_data->plan_stripe_id);
                    $checkout_url = $response->url;
                    $plan_sess = ['plan_id' => $request->plan_id];
                    $request->session()->put('plan_sess',$plan_sess);   
                }else{
                    $user_stripe_id = $request->session()->get('userdata')['user_stripe_id'];
                    $user_id = $request->session()->get('userdata')['id'];
                    $response = (new Payment)->createStripeSubscription($plan_data->plan_stripe_id,$user_stripe_id);

                    $evaluate = 'open_modal("show-payment-link","subscriptionId='.$response->id.'&clientSecret='.$response->latest_invoice->payment_intent->client_secret.'");';
                    $plan_sess =[
                                    'plan_id'           =>  $request->plan_id,
                                    'user_id'           =>  $user_id,
                                    'inapp_type'        =>  'stripe',
                                    'inapp_productid'   =>  $response->id,
                                    'inapp_token'       =>  $response->latest_invoice->payment_intent->client_secret
                                ];
                    $request->session()->put('plan_sess',$plan_sess);
                    //print_r($response);exit();
                    //$hosted_invoice_url = $response->latest_invoice->hosted_invoice_url;
                    /*$evaluate = 'open_modal("show-payment-link","link_url='.$hosted_invoice_url.'");';*/
                    /*$evaluate = 'window.open("'.$hosted_invoice_url.'", "_blank");';*/
                    /*$evaluate = 'execute("api/choose-plan","plan_id='.$plan_data->id.'&user_id='.$user_id.'&inapp_type=stripe&inapp_productid='.$response->id.'&link_url='.$hosted_invoice_url.'");';*/
                    /*echo $hosted_invoice_url;exit();*/
                }

                //$response  = (new Payment)->createStripePayment($plan_data->plan_stripe_id);
                
                //print_r($response);exit();
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Redirecting to check out page',
                                'reponse_body'  =>  "null",
                                'redirect'      =>  $checkout_url ? $checkout_url : NULL,
                                'evaluate'      =>  $evaluate ? $evaluate : NULL,
                                'close'         =>  $close
                            ], 200);
                
            }else{
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details provided',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
        }
        
    }

    function showPaymentLinkModal(Request $request){
        $rules =[
                    'subscriptionId'    =>  'required',
                    'clientSecret'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{

            $html = view('xhr.show_payment_link_modal',['subscriptionId'=>$request->subscriptionId,'clientSecret'=>$request->clientSecret])->render();
            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function createStripePaymentForDonation(Request $request){
        $rules =[
                    'stripe_id'       =>  'required',
                    /*'payment_for'   =>  'required',
                    'redirect_url'  =>  'required'*/
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $donation_data = DB::table('donation_amounts')
                                ->where('da_stripeid',$request->stripe_id)->first();
            if($donation_data){
                $response  = (new Payment)->createStripePayment($donation_data->da_stripeid);
                //print_r($response);exit();

                $donate_sess = ['currency_id' => $donation_data->da_currency,'donation_amount'=>$donation_data->da_amount];
                $request->session()->put('donate_sess',$donate_sess);
                $checkout_url = $response->url;

                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Redirecting to check out page',
                                'reponse_body'  =>  "null",
                                'redirect'      =>  $checkout_url
                            ], 200);
                
            }else{
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details provided',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
        }
    }

    function createSquareupPayment(Request $request){
        /*$response  = (new Payment)->createCheckOut($request->amount,'USD',$request->payment_for,$request->redirect_url);*/
        $response  = (new Payment)->createStripePayment($request->amount,'USD',$request->redirect_url);
        print_r($response);exit();
        if($response['success']==1){
            /*$checkout = $response['result']->checkout;
            echo '<pre>';
            print_r($checkout);*/
            $result = json_decode($response['result']);
            $checkout = $result->checkout;
            $currency_data = DB::table('currencies')->where('currency_shortname','=','USD')->first();
            if($request->redirect_url=='donate'){
                $donate_sess = ['currency_id' => $currency_data->_currency_id,'donation_amount'=>$request->amount,'checkout_id' => $checkout->id];
                $request->session()->put('donate_sess',$donate_sess);
            }else if($request->redirect_url=='premium-plans'){
                $plan_sess = ['plan_id' => $request->plan_id];
                $request->session()->put('plan_sess',$plan_sess);
            }

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Redirecting to check out page',
                                'reponse_body'  =>  "null",
                                'redirect'      =>  $checkout->checkout_page_url       
                            ], 200);
            /*echo '<pre>';
            print_r($checkout);*/
        }else{
            echo '<pre>';
            print_r($response);
        }
        /*echo '<pre>';
        print_r($response);*/

    }

    function submitDonation(Request $request){
        $rules =[
                    'currency_id'       =>  'required',
                    'donation_amount'   =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $payment = [
                            'payment_gateway'   =>  'stripe'
                        ];
            DB::table('donations')->insert([
                'donation_currency'         =>  $request->currency_id,
                'donation_amount'           =>  $request->donation_amount,
                'donation_user_id'          =>  $request->session()->get('userdata')['id'],
                'donation_payment_status'   =>  'successful',
                'donation_payment'          =>  json_encode($payment,TRUE)
            ]);

            $request->session()->forget('donate_sess');

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Thank you for your donation',
                                'reponse_body'  =>  ['donation'=>'success'],
                                'redirect'      =>  url('donate')
                        ], 200);
        }
    }

    function choosePlan(Request $request){
        $req = (isset($request->inapp_type) && $request->inapp_type) ? 'required|in:google,apple,paypal,stripe' : 'nullable';
        $req2 = (isset($request->inapp_token) && $request->inapp_token) ? 'required' : 'nullable';
        $req3 = (isset($request->inapp_productid) && $request->inapp_productid) ? 'required' : 'nullable';
        $rules =[
                    'plan_id'               =>  'required',
                    'user_id'               =>  'required',
                    'inapp_type'            =>  $req,
                    'inapp_token'           =>  $req2,
                    'inapp_productid'       =>  $req3,
                    'link_url'              =>  'nullable',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $plan_data = DB::table('premium_plans')->find($request->plan_id);
            if($plan_data){
                
                if($plan_data->plan_type=='monthly'){
                    $expiry_date = date('Y-m-d', strtotime('+1 month', strtotime(date('Y-m-d'))));
                }else if($plan_data->plan_type=='annually'){
                    $expiry_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 365 day"));
                }else{
                    $expiry_date = NULL;
                }
                
                $chosen_plan =  [
                                    'cp_user_id'        =>  $request->user_id,
                                    'cp_plan_id'        =>  $request->plan_id,
                                    'cp_expiry_date'    =>  $expiry_date,
                                    'cp_inapp_type'     =>  $request->inapp_type,
                                    'cp_inapp_token'    =>  $request->inapp_token,
                                    'cp_inapp_productid'=>  $request->inapp_productid,
                                ];

                DB::table('chosen_plans')->where('cp_user_id',$request->user_id)->update([
                    'cp_is_expired'     =>1,
                    'cp_expiry_date'    => date('Y-m-d')
                ]);

                DB::table('chosen_plans')->insert($chosen_plan);

                $plans = DB::table('premium_plans')
                        ->selectRaw('*,(SELECT IF((SELECT COUNT(id) FROM chosen_plans WHERE cp_user_id='.$request->user_id.' AND cp_plan_id=premium_plans.id AND cp_is_expired=0 AND (cp_expiry_date >= CURDATE() OR cp_expiry_date IS NULL)),1,0)) AS is_active
                            ,(SELECT IF((SELECT COUNT(id) FROM chosen_plans WHERE cp_user_id='.$request->user_id.' AND cp_plan_id=premium_plans.id AND cp_is_expired=0 AND cp_inapp_type IS NOT NULL),1,0)) AS is_inapp,
                            (SELECT cp_inapp_type FROM chosen_plans WHERE cp_user_id='.$request->user_id.' AND cp_plan_id=premium_plans.id AND cp_is_expired=0) AS cp_inapp_type
                            ')
                        ->join('currencies', 'premium_plans.plan_currency', '=', 'currencies._currency_id')
                        ->get();
                
                $request->session()->forget('plan_sess');
                
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Plan Chosen Successfully',
                                'reponse_body'  =>  ['premium_plans' => $plans],
                                'redirect'      =>  url('premium-plans')
                        ], 200);
            }else{
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid Details Supplied',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
        }
    }

    function donate(Request $request){

        if($request->user_id){
            $data = Userdetail::leftJoin('countries','userdetails.country','=','countries._country_id')
            ->leftJoin('provinces','userdetails.state','=','provinces._province_id')
            ->leftJoin('cities','userdetails.city','=','cities._city_id')
            ->select('userdetails.*', 'countries.country_name', 'provinces.province_name', 'cities.city_name')
            ->where('userdetails.id', $request->user_id)
            ->first();

            $counts = DB::select("SELECT (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS matched_count,
        (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id='".$request->user_id."') AS life_events_count,
        (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user='".$request->user_id."' OR request_to_user='".$request->user_id."')) AS connection_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=1 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS personal_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=2 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS dating_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=3 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS adoption_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=4 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS travel_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=5 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS military_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=6 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS school_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=7 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS career_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=8 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS pets_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=9 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS lost_count,
        (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND feed_lifeevent=10 AND (feed_user1 = '".$request->user_id."' OR feed_user2 = '".$request->user_id."')) AS doppel_count");

            $data_user = User::where('id',$request->user_id)->first();
            if($data && $data_user){
                $newData = array_merge($data->toArray(),$data_user->toArray());
                $newData['counts'] = $counts;
                $qb_session = qb_login($newData['email'],'12345678');
                if($qb_session && $qb_session->session){
                  $token = $qb_session->session->token;
                  $newData['qb_token'] = $token;
                }
                $request->session()->put('userdata', $newData);
            }else{
                return redirect('login');
            }
        }
        if (!$request->session()->has('userdata')) {
            return redirect('login');    
        }
        $user_id = $request->session()->get('userdata')['id'];
        $currency_data = [];
        if($request->currency && is_numeric($request->currency)){
            $currency_data = DB::table('currencies')->where('_currency_id',$request->currency)->first();
            if(!$currency_data){
                return redirect('donate');
            }

            $donation_amounts = DB::table('donation_amounts')
                                    ->where('donation_amounts.da_currency',$currency_data->_currency_id)
                                    ->orderBy('da_amount','ASC')
                                    ->get();
            $currency_data->donation_amounts = $donation_amounts ? $donation_amounts : [];
        }
        $currencies = DB::table('currencies')->get();
        $amt = [];
        if($currencies){
            foreach($currencies as $currency){
                $donation_amounts = DB::table('donation_amounts')
                                    ->where('donation_amounts.da_currency',$currency->_currency_id)
                                    ->orderBy('da_amount','ASC')
                                    ->get();
                $currency->donation_amounts = $donation_amounts ? $donation_amounts : [];

                array_push($amt, $currency);
            }
        }
        $donation_success = '';
        if($request->session()->has('donate_sess') && $request->checkoutId && $request->transactionId){

            $payment = [
                            'payment_gateway'   =>  'squareup',
                            'checkout_id'       =>  $request->session()->get('donate_sess')['checkout_id'],
                            'transaction_id'    =>  $request->transactionId
                        ];
            DB::table('donations')->insert([
                'donation_currency'         =>  $request->session()->get('donate_sess')['currency_id'],
                'donation_amount'           =>  $request->session()->get('donate_sess')['donation_amount'],
                'donation_user_id'          =>  $request->session()->get('userdata')['id'],
                'donation_payment_status'   =>  'successful',
                'donation_payment'          =>  json_encode($payment,TRUE)
            ]);

            $request->session()->forget('donate_sess');
            $donation_success = 'Thank you for the donation';
        }

        return View::make("donate")->with(['currencies' => $amt,'currency_data'=>$currency_data,'donation_success'=>$donation_success]);   
    }

    function donateForm(Request $request){
        $cus_required = ($request->donation_amount && $request->donation_amount=='custom') ? 'required|' : 'nullable';
        $rules =[
                    'donation_amount'   =>  'required',
                    'currency_id'       =>  'required',
                    'custom_amount'     =>  $cus_required
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $donation_amount = $request->donation_amount=='custom' ? $request->custom_amount : $request->donation_amount;
            $evaluate = 'open_modal("select-payment-mode","currency_id='.$request->currency_id.'&donation_amount='.$donation_amount.'")';
            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  '',
                                'reponse_body'  =>  'null',
                                'evaluate'      =>  $evaluate
                            ],200);
        }
    }

    public function selectPaymentForPlansModal(Request $request){
        $rules =[
                    'plan_id'   =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $plan_data = DB::table('premium_plans')
                            ->join('currencies', 'premium_plans.plan_currency', '=', 'currencies._currency_id')
                            ->where('id',$request->plan_id)
                            ->first();

            $html = view('xhr.select_payment_for_plans_modal',['currency_data' => $plan_data,'amount'=>$plan_data->plan_amount,'payment_for' => 'Premium Plans','redirect_url'=>'premium-plans','user_id'=>$request->session()->get('userdata')['id']])->render();
            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function selectPaymentModeModal(Request $request){
        $rules =[
                    'donation_amount'   =>  'required',
                    'currency_id'       =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $currency_data = DB::table('currencies')->where('_currency_id',$request->currency_id)->first();

            $donation_data = DB::table('donation_amounts')
                                ->where('da_currency',$request->currency_id)
                                ->where('da_amount',$request->donation_amount)
                                ->first();


            $html = view('xhr.select_payment_mode_modal',['currency_data' => $currency_data,'amount'=>$request->donation_amount,'payment_for' => 'Donation','redirect_url'=>'donate','user_id'=>$request->session()->get('userdata')['id'],'donation_data'=>$donation_data])->render();
            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function logOut(Request $request){
        /*if ($request->session()->has('userdata')) {
            
        }*/
        $request->session()->forget('userdata');
        $request->session()->flush();
        echo 'logOut called';
        return redirect('login');
    }

    public function curlCheck(Request $request){

        $connections = DB::table('connection_requests')->where('request_status',1)->get();
        if($connections){
            $follow_arr = [];
            foreach($connections as $connect){
                $con1_check = DB::table('user_follows')
                                ->where('follow_user',$connect->request_from_user)
                                ->where('follow_following',$connect->request_to_user)
                                ->first();
                $con2_check = DB::table('user_follows')
                                ->where('follow_user',$connect->request_to_user)
                                ->where('follow_following',$connect->request_from_user)
                                ->first();
                if(!$con1_check){
                    array_push($follow_arr, [
                                                'follow_user'       =>  $connect->request_from_user,
                                                'follow_following'  =>  $connect->request_to_user
                                ]);
                }
                if(!$con2_check){
                    array_push($follow_arr, [
                                                'follow_user'       =>  $connect->request_to_user,
                                                'follow_following'  =>  $connect->request_from_user
                                ]);
                }
            }
            if($follow_arr){
                DB::table('user_follows')->insert($follow_arr);
            }
        }
        $followed = DB::table('user_follows')->get();
        echo '<pre>';
        print_r($followed);
        echo '</pre>';
        /*set_time_limit(18000);
        $countries = DB::table('countries')->where('_country_id','>',210)->where('_country_id','<=',250)->get();
        $cities = [];
        foreach ($countries as $key => $country) {
            $states = DB::table('provinces')->where('province_country',$country->_country_id)->get();
            if($states){
                foreach ($states as $key => $province) {
                    $crl = curl_init('https://api.countrystatecity.in/v1/countries/'.$country->country_code.'/states/'.$province->province_short_name.'/cities');
                    curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($crl, CURLINFO_HEADER_OUT, true);
                    
                    curl_setopt($crl, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'X-CSCAPI-KEY: bkF4Qnk5UkRjTnRFMkdITjMzSnlSdjlrbExoUUZCRk1xRXZ1WWNneg==' )
                    );
                    
                    $result = curl_exec($crl);
                    
                    if ($result === false) {
                      print_r('Curl error: ' . curl_error($crl));
                    } else {
                        $data = json_decode($result,TRUE);
                        foreach($data as $key => $city){
                            array_push($cities, [
                                                    'city_name'         =>  $city['name'],
                                                    'city_province'     =>  $province->_province_id
                                                ]);
                        }
                    }
                    curl_close($crl);
                }
            }
            
        }
        echo '<pre>';
        print_r($cities);
        echo '<pre>';
        if($cities){
            DB::table('cities')->insert($cities);
        }*/
    }

    function getPaymentUrl(Request $request){
        $req = (isset($request->payment_for) && $request->payment_for=='premium-plans') ? 'required' : 'nullable';
        $rules =[
                    'amount'        =>  'required',
                    'currency_id'   =>  'required',
                    'user_id'       =>  'required',
                    'payment_for'   =>  'required|in:donate,premium-plans',
                    'plan'          =>  $req
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            
            $res_url = url('do-payment/?user='.$request->user_id.'&currency='.$request->currency_id.'&amount='.$request->amount.'&payment_for='.$request->payment_for);
            if($request->payment_for=='premium-plans'){
                $res_url = url('do-payment/?user='.$request->user_id.'&currency='.$request->currency_id.'&amount='.$request->amount.'&payment_for='.$request->payment_for.'&plan='.$request->plan);
            }
            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'URL Fetched Successfully',
                                'reponse_body'  =>  ['res_url'=>$res_url]
                            ],200);
        }
    }

    function doPayment(Request $request){
        if($request->user && $request->currency && $request->amount && in_array($request->payment_for,['donate','premium-plans'])){
            $currency_data = DB::table('currencies')->where('_currency_id',$request->currency)->first();
            if($request->payment_for=='premium-plans' && $request->plan){
                $currency_data = DB::table('premium_plans')
                            ->join('currencies', 'premium_plans.plan_currency', '=', 'currencies._currency_id')
                            ->where('id',$request->plan)
                            ->first();
            }
            $user_data = DB::table('users')->where('id',$request->user)->first();
            return View::make("do_payment")->with(['currency_data'=>$currency_data,'amount'=>$request->amount,'user_data'=>$user_data]);
        }else{
            redirect('home');
        }
    }

    function donateSuccessModal(Request $request){
        $rules =[
                    'amount'            =>  'required',
                    'currency_id'       =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $currency_data = DB::table('currencies')->where('_currency_id',$request->currency_id)->first();

            $html = view('xhr.donate_success_modal',['currency_data' => $currency_data,'amount'=>$request->amount])->render();
            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function showImageFromUrlModal(Request $request){
        $rules =[
                    'image_link'            =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{

            $html = view('xhr.show_image_from_url_modal',['image_link'=>$request->image_link])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function paymentSuccessModal(Request $request){

        $html = view('xhr.payment_success_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function paymentFailureModal(Request $request){
        $html = view('xhr.payment_failure_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function PaymentSuccess(Request $request){
        if($request->payment_intent && $request->payment_intent_client_secret && $request->redirect_status){
            if($request->redirect_status!='succeeded'){
                return redirect('payment-failure');
            }
        }
        return View::make("payment_success");
    }
    function PaymentFailure(Request $request){
        return View::make("payment_failure");
    }

    function _checkLogin(Request $request){
        // echo 'check login called';
        if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
    }

    function deleteUserModal(Request $request){
        $html = view('xhr.delete_user_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function deleteUser(Request $request){
        $rules =[
                    'user_id'            =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $user_data = DB::table('users')->where('id',$request->user_id)->first();
            if(!$user_data){
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid User',
                                'reponse_body'  =>  "null",
                                'errors'        =>  NULL
                            ], 401);
            }else{
                if($user_data->quick_blox_id){
                    $qb_session = qb_login($user_data->email,'12345678');
                    if($qb_session && isset($qb_session->session) && $qb_session->session){
                        //echo 'here';
                        $token = $qb_session->session->token;
                        $del_res = qbDeleteUser($token,$user_data->quick_blox_id);
                        //print_r($del_res);exit;
                    }
                }

                User::where('id',$user_data->id)->delete();
                Userdetail::where('id',$user_data->id)->delete();
                DB::table('connection_requests')
                    ->where('request_from_user',$user_data->id)
                    ->orWhere('request_to_user',$user_data->id)
                    ->delete();
                DB::table('feeds')
                    ->where('feed_user1',$user_data->id)
                    ->orWhere('feed_user2',$user_data->id)
                    ->delete();
                DB::table('life_events')
                    ->where('event_user_id',$user_data->id)
                    ->delete();
                DB::table('user_follows')
                    ->where('follow_user',$user_data->id)
                    ->orWhere('follow_following',$user_data->id)
                    ->delete();
                DB::table('user_unfollows')
                    ->where('unfollow_from_user',$user_data->id)
                    ->orWhere('unfollow_to_user',$user_data->id)
                    ->delete();
                DB::table('blocked_users')
                    ->where('bu_from_id',$user_data->id)
                    ->orWhere('bu_to_id',$user_data->id)
                    ->delete();
                DB::table('chosen_plans')
                    ->where('cp_user_id',$user_data->id)
                    ->delete();


                return response([
                                    'response_code' =>  '200',
                                    'response_msg'  =>  'User Deleted Successfully',
                                    'reponse_body'  =>  'null',
                                    'redirect'      =>  url('logout')
                                ],200);
            }
        }
    }

    function getUsers(Request $request){
        $rules =[
                    'user_ids'            =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $users = DB::select("SELECT id,firstName, lastName, email, quick_blox_id, profile_photo_path, isd_code, phone, username FROM users WHERE id IN(".$request->user_ids.")");

            return response([
                                    'response_code' =>  '200',
                                    'response_msg'  =>  'Users Fetched Successfully',
                                    'reponse_body'  =>  ['users' => $users]
                                ],200);
        }
    }

    function cancelsubscription(Request $request){
        $res = getAccessToken();
        /*echo '<pre>';
        print_r($res);
        echo '</pre>';exit();*/
        $accessToken = $res->access_token;
        $result = getSubscriptionDetails('I-A23GJG76P5CG',$accessToken);
        echo $result->status;
        echo '<pre>';
        print_r($result);

        echo '</pre>';exit();
        $res = cancelSubcription('I-7JJMGVHG837L', $accessToken);
        echo '<pre>';
        print_r($res);
        echo '</pre>';
    }


    function getSubscriptionInfo(Request $request){

    }
}
