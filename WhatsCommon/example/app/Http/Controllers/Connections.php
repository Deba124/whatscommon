<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Userdetail;
use Validator;
use View;

class Connections extends Controller
{

	function index(Request $request){
		if (!$request->session()->has('userdata')) {
            return redirect('login');
        }
        $user_id = $request->session()->get('userdata')['id'];
        $select = "SELECT userdetails.*,users.username, users.quick_blox_id, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status =1 AND ((request_from_user=$user_id AND request_to_user=users.id) OR (request_from_user=users.id AND request_to_user=$user_id))), 1, 0)) AS is_connected,
        	(SELECT IF ((SELECT COUNT(*) FROM user_follows WHERE follow_user=$user_id AND follow_following=userdetails.id) , 1, 0)) AS is_following,
				 (SELECT event_type FROM life_event_categories 
				 INNER JOIN feeds ON feeds.feed_lifeevent=life_event_categories.id
				 INNER JOIN connection_requests ON connection_requests.request_feed=feeds._feed_id
				 WHERE connection_requests.request_status=1
				 AND ((connection_requests.request_from_user=$user_id AND connection_requests.request_to_user=users.id) OR (connection_requests.request_from_user=users.id AND connection_requests.request_to_user=$user_id))
				 ) AS event_type,
				 (SELECT COUNT(DISTINCT feed_lifeevent) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=$user_id) AND ((feed_user1 = $user_id AND feed_user2 = userdetails.id) OR (feed_user1 = userdetails.id AND feed_user2 = $user_id))) AS events_type_count,
				 (SELECT GROUP_CONCAT(DISTINCT event_type) FROM life_event_categories WHERE life_event_categories.id IN(SELECT DISTINCT feed_lifeevent FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=$user_id) AND ((feed_user1 = $user_id AND feed_user2 = userdetails.id) OR (feed_user1 = userdetails.id AND feed_user2 = $user_id)) )) AS events_type_names,
				  users.profile_photo_path, countries.country_name, provinces.province_name, cities.city_name, (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS matched_count,
          (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id=userdetails.id) AS life_events_count,
          (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user=userdetails.id OR request_to_user=userdetails.id)) AS connection_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=1 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS personal_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=2 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS dating_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=3 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS adoption_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=4 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS travel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=5 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS military_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=6 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS school_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=7 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS career_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=8 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS pets_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=9 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS lost_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=userdetails.id) AND feed_lifeevent=10 AND (feed_user1 = userdetails.id OR feed_user2 = userdetails.id)) AS doppel_count
          FROM userdetails
          INNER JOIN users ON users.id=userdetails.id
          LEFT JOIN countries ON userdetails.country=countries._country_id
          LEFT JOIN provinces ON userdetails.state = provinces._province_id
          LEFT JOIN cities ON userdetails.city=cities._city_id
          WHERE userdetails.id != $user_id
          AND userdetails.id NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id=$user_id)
				HAVING is_connected=1
			ORDER BY userdetails.firstName ASC
				";
		$select2 = "SELECT users.firstName, users.lastName, users.profile_photo_path, users.id, users.username, users.quick_blox_id, users.show_connection_list, users.show_match_feed, users.show_connection_feed, users.created_at,userdetails.bio, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status =1 AND ((request_from_user=$user_id AND request_to_user=users.id) OR (request_from_user=users.id AND request_to_user=$user_id))), 1, 0)) AS is_connected,
                    (SELECT IF ((SELECT COUNT(*) FROM user_follows WHERE follow_user=$user_id AND follow_following=users.id) , 1, 0)) AS is_following,
                     (SELECT event_type FROM life_event_categories 
                     INNER JOIN feeds ON feeds.feed_lifeevent=life_event_categories.id
                     INNER JOIN connection_requests ON connection_requests.request_feed=feeds._feed_id
                     WHERE connection_requests.request_status=1
                     AND ((connection_requests.request_from_user=$user_id AND connection_requests.request_to_user=users.id) OR (connection_requests.request_from_user=users.id AND connection_requests.request_to_user=$user_id))
                     ) AS event_type,
                     (SELECT COUNT(DISTINCT feed_lifeevent) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_id."') AND ((feed_user1 = '".$user_id."' AND feed_user2 = users.id) OR (feed_user1 = users.id AND feed_user2 = '".$user_id."'))) AS events_type_count,
                     (SELECT GROUP_CONCAT(DISTINCT event_type) FROM life_event_categories WHERE life_event_categories.id IN(SELECT DISTINCT feed_lifeevent FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$user_id."') AND ((feed_user1 = '".$user_id."' AND feed_user2 = users.id) OR (feed_user1 = users.id AND feed_user2 = '".$user_id."')) )) AS events_type_names,
countries.country_name, provinces.province_name, cities.city_name, (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND (feed_user1 = users.id OR feed_user2 = users.id)) AS matched_count,
          (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id=users.id) AS life_events_count,
          (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user=users.id OR request_to_user=users.id)) AS connection_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=1 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS personal_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=2 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS dating_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=3 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS adoption_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=4 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS travel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=5 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS military_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=6 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS school_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=7 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS career_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=8 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS pets_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=9 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS lost_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=10 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS doppel_count
                    FROM users 
          INNER JOIN userdetails ON users.id=userdetails.id
          LEFT JOIN countries ON userdetails.country=countries._country_id
          LEFT JOIN provinces ON userdetails.state = provinces._province_id
          LEFT JOIN cities ON userdetails.city=cities._city_id
                    WHERE users.id!='".$user_id."' 
                    AND users.id NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id='".$user_id."')
                    HAVING is_connected=1
          ORDER BY users.firstName ASC
          ";
        $connections = DB::select($select2);
        $new_connections = [];
        if(count($connections)>0){
          foreach ($connections as $key => $connection) {

            $where = ' AND (feed_user1="'.$connection->id.'" OR feed_user2="'.$connection->id.'")';
            $select2 = 'SELECT feeds.*, life_event_categories.event_type AS feed_event_type,
                    u1.id AS user1_id, u1.profile_photo_path AS user1_profile_photo, u1.firstName AS user1_firstName, u1.lastName AS user1_lastName, u1.quick_blox_id AS user1_quick_blox_id, 
                    u2.id AS user2_id, u2.profile_photo_path AS user2_profile_photo, u2.firstName AS user2_firstName, u2.lastName AS user2_lastName, u2.quick_blox_id AS user2_quick_blox_id, 
                    (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 1 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))), 1, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 0 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))),2,0)))) AS is_connected
                     FROM feeds 
                    INNER JOIN users u1 ON u1.id=feeds.feed_user1
                    INNER JOIN users u2 ON u2.id=feeds.feed_user2
                    INNER JOIN life_event_categories ON feeds.feed_lifeevent=life_event_categories.id
                        WHERE _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id="'.$connection->id.'") AND feed_user1 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$connection->id.'") AND feed_user2 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$connection->id.'") AND feed_type="matched" '.$where.' ORDER BY _feed_id DESC
                      ';
            //echo $select;exit;
      
            $feeds2 = DB::select($select2);
            $new_feeds2 = [];
            if($feeds2){
              foreach($feeds2 as $key => $val){
                $feed_images = DB::table('feed_images')->where('fi_feed_id',$val->_feed_id)->get();
                $val->feed_images = $feed_images;
                array_push($new_feeds2, $val);
              }
            }
            $connection->feeds = $new_feeds2;
            array_push($new_connections, $connection);
          }
        }
        /*echo $select;
		echo '<pre>';
		print_r($connections);echo '</pre>';exit();*/
    return View::make("connections")->with(['connections' => $new_connections,'user_id' => $user_id]);
	}

  public function otherConnections(Request $request){
    if (!$request->session()->has('userdata')) {
      return redirect('login');
    }
    $user_id = $request->u ? $request->u : NULL;
    $my_id = $request->session()->get('userdata')['id'];
    if($user_id){
      $user_data = DB::table('users')->where('id',$user_id)->first();
      if(!$user_data){
        return redirect('home');
      }

      $select2 = "SELECT users.firstName, users.lastName, users.profile_photo_path, users.id, users.username, users.quick_blox_id, users.show_connection_list, users.show_match_feed, users.show_connection_feed,users.created_at,userdetails.bio, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status =1 AND ((request_from_user=$user_id AND request_to_user=users.id) OR (request_from_user=users.id AND request_to_user=$user_id))), 1, 0)) AS is_connected,
                (SELECT IF ((SELECT COUNT(*) FROM user_follows WHERE follow_user=$my_id AND follow_following=users.id) , 1, 0)) AS is_following, 
                countries.country_name, provinces.province_name, cities.city_name, 
                (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND (feed_user1 = users.id OR feed_user2 = users.id)) AS matched_count,
          (SELECT IFNULL(COUNT(*),0) FROM life_events WHERE event_user_id=users.id) AS life_events_count,
          (SELECT IFNULL(COUNT(*),0) FROM connection_requests WHERE request_status=1 AND (request_from_user=users.id OR request_to_user=users.id)) AS connection_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=1 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS personal_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=2 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS dating_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=3 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS adoption_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=4 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS travel_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=5 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS military_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=6 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS school_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=7 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS career_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=8 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS pets_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=9 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS lost_count,
          (SELECT IFNULL(COUNT(*),0) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id=users.id) AND feed_lifeevent=10 AND (feed_user1 = users.id OR feed_user2 = users.id)) AS doppel_count
                    FROM users 
          INNER JOIN userdetails ON users.id=userdetails.id
          LEFT JOIN countries ON userdetails.country=countries._country_id
          LEFT JOIN provinces ON userdetails.state = provinces._province_id
          LEFT JOIN cities ON userdetails.city=cities._city_id
                    WHERE users.id!='".$user_id."' 
                    AND users.id NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id='".$user_id."')
                    HAVING is_connected=1
          ORDER BY users.firstName ASC
          ";
        $connections = DB::select($select2);
        $new_connections = [];
        if(count($connections)>0){
          foreach ($connections as $key => $connection) {

            $where = ' AND (feed_user1="'.$connection->id.'" OR feed_user2="'.$connection->id.'")';
            $select2 = 'SELECT feeds.*, life_event_categories.event_type AS feed_event_type,
                    u1.id AS user1_id, u1.profile_photo_path AS user1_profile_photo, u1.firstName AS user1_firstName, u1.lastName AS user1_lastName, u1.quick_blox_id AS user1_quick_blox_id, 
                    u2.id AS user2_id, u2.profile_photo_path AS user2_profile_photo, u2.firstName AS user2_firstName, u2.lastName AS user2_lastName, u2.quick_blox_id AS user2_quick_blox_id, 
                    (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 1 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))), 1, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 0 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))),2,0)))) AS is_connected
                     FROM feeds 
                    INNER JOIN users u1 ON u1.id=feeds.feed_user1
                    INNER JOIN users u2 ON u2.id=feeds.feed_user2
                    INNER JOIN life_event_categories ON feeds.feed_lifeevent=life_event_categories.id
                        WHERE _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id="'.$my_id.'") AND feed_user1 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$my_id.'") AND feed_user2 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$my_id.'") AND feed_type="matched" '.$where.' ORDER BY _feed_id DESC
                      ';
            //echo $select;exit;
      
            $feeds2 = DB::select($select2);
            $new_feeds2 = [];
            if($feeds2){
              foreach($feeds2 as $key => $val){
                $feed_images = DB::table('feed_images')->where('fi_feed_id',$val->_feed_id)->get();
                $val->feed_images = $feed_images;
                array_push($new_feeds2, $val);
              }
            }
            $connection->feeds = $new_feeds2;
            array_push($new_connections, $connection);
          }
        }
      return View::make("other_connections")->with(['connections' => $new_connections,'user_data' => $user_data]);
    }else{
      return redirect('home');
    }
  }
  public function connectionRequest(Request $request){
    $rules =[
			        'user_id' 			=> 	'required',
			        'connect_user_id'	=>	'required',
			        '_feed_id'			=>	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$connection = DB::select("SELECT * FROM users 
										WHERE id IN(SELECT request_to_user FROM connection_requests 
											WHERE request_from_user='".$request->user_id."' AND request_to_user='".$request->connect_user_id."' AND request_status!=2) 
										OR id IN(SELECT request_from_user FROM connection_requests 
											WHERE request_to_user='".$request->user_id."' AND request_from_user='".$request->connect_user_id."' AND request_status!=2)");
			if($connection){
				return response([
			          'response_code'=>'201',
			          'response_msg'=> 'Request already sent',
			          'reponse_body' => "null"
			        ], 401);
			}else{
				$_request_id=DB::table('connection_requests')->insertGetId([	'request_from_user' => 	$request->user_id,
															'request_to_user'	=>	$request->connect_user_id,
															'request_feed'	=>	$request->_feed_id
														]);
				$to_user_data = DB::table('users')
									->join('device_tokens','users.id','=','device_tokens.device_user_id')
									->select('device_tokens.*')
									->where('users.id',$request->connect_user_id)
									->get();
				$from_user_data = DB::table('users')->where('users.id',$request->user_id)->first();
				if($to_user_data){
					foreach($to_user_data as $to_user){
						$title = 'Connection Request';
						$body = $from_user_data->firstName.' '.$from_user_data->lastName.' wants to connect with you';
						$noti_response = sendNotification($to_user->device_type,$to_user->device_token,$title,$body,['type' => 'connection']);
						$notification_data =[
			        							'notification_user'		=>	$to_user->device_user_id,
			        							'notification_device'	=>	$to_user->_device_id,
			        							'notification_title'	=>	$title,
			        							'notification_content'	=>	$body,
			        							'notification_status'	=>	$noti_response['success']==1 ? 'success' : 'failed',
			        							'notification_response'	=>	json_encode($noti_response)
			        						];
			        	DB::table('notifications')->insert($notification_data);
					}
				}

				return response([
                				'response_code'	=>	'200',
                         		'response_msg'	=> 	'Connection requested successfully',
                         		'reponse_body' 	=> 	['_request_id' => $_request_id],
                         		'time'			=>	1000
                        	], 200);
			}
		}
  }

    public function activityList(Request $request){
    	$rules =[
			        'user_id' 			=> 	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$activities = DB::select("SELECT users.id, users.firstName, users.lastName, users.profile_photo_path, connection_requests.created, connection_requests._request_id FROM users INNER JOIN connection_requests ON connection_requests.request_from_user=users.id WHERE connection_requests.request_to_user='".$request->user_id."' AND request_status=0 ORDER BY connection_requests._request_id DESC");
			return response([
                				'response_code'	=>	'200',
                         		'response_msg'	=> 	'Activities fetched successfully',
                         		'reponse_body' 	=> 	['activities' => $activities]
                        	], 200);
		}
    }

    public function acceptRejectRequest(Request $request){
    	$rules =[
			        'user_id' 			=> 	'required',
			        '_request_id'		=>	'required',
			        'request_status'	=>	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$requestdata = 	DB::table('connection_requests')
								->join('feeds','feeds._feed_id','=','connection_requests.request_feed')
								->where('_request_id', $request->_request_id)
								->where('request_to_user', $request->user_id)
								->where('request_status','=',0)
								->first();
			if(!$requestdata){
				return response([
			        'response_code'=>'201',
			        'response_msg'=> 'Invalid request',
			        'reponse_body' => "null"
			    ], 401);
			}else{
				$request_accept_time = $request->request_status==1 ? date('Y-m-d H:i:s') : NULL;
				DB::table('connection_requests')->where('_request_id',$request->_request_id)->update(['request_status' => $request->request_status, 'request_accept_time' => $request_accept_time]);

				if($request->request_status==1){
					$feed_data =[
									'feed_type'			        =>	'connected',
									'feed_lifeevent'	      =>	$requestdata->feed_lifeevent,
                  'feed_eventid'          =>  $requestdata->feed_eventid,
                  'feed_matched_eventid'  =>  $requestdata->feed_matched_eventid,
									'feed_user1'		        =>	$request->user_id,
									'feed_user2'		        => 	$requestdata->request_from_user,
									'feed_match_count'	    =>	$requestdata->feed_match_count,
									'feed_what'			        =>	$requestdata->feed_what,
									'feed_where'		        =>	$requestdata->feed_where,
									'feed_when'			        =>	$requestdata->feed_when,
									'feed_keywords'		      =>	$requestdata->feed_keywords,
									'feed_why'			        =>	$requestdata->feed_why
								];
					$_feed_id = DB::table('feeds')->insertGetId($feed_data);
					$pre_feed_images = DB::table('feed_images')->where('fi_feed_id',$requestdata->_feed_id)->get();
					if($pre_feed_images){
						foreach($pre_feed_images as $key => $img){
							DB::table('feed_images')->insert([
																'fi_feed_id' 	=> $_feed_id,
																'fi_img_name'	=> $img->fi_img_name,
																'fi_img_url'	=> $img->fi_img_url
															]);
						}
					}

					$to_user_data = DB::table('users')
										->join('device_tokens','users.id','=','device_tokens.device_user_id')
										->select('device_tokens.*')
										->where('users.id',$requestdata->request_from_user)
										->get();
					$from_user_data = DB::table('users')->where('users.id',$request->user_id)->first();
					if($to_user_data){
						foreach($to_user_data as $to_user){
							$title = 'Connection Request';
							$body = $from_user_data->firstName.' '.$from_user_data->lastName.' accepted your connection request';
							/*$noti_response = sendNotification($to_user->device_type,$to_user->device_token,$title,$body,['type' => 'connection']);
							$notification_data =[
				        							'notification_user'		=>	$to_user->device_user_id,
				        							'notification_device'	=>	$to_user->_device_id,
				        							'notification_title'	=>	$title,
				        							'notification_content'	=>	$body,
				        							'notification_status'	=>	$noti_response['success']==1 ? 'success' : 'failed',
				        							'notification_response'	=>	json_encode($noti_response)
				        						];
				        	DB::table('notifications')->insert($notification_data);*/
						}
					}

					$con1_check = DB::table('user_follows')
	                                ->where('follow_user',$requestdata->request_from_user)
	                                ->where('follow_following',$requestdata->request_to_user)
	                                ->first();
	                $con2_check = DB::table('user_follows')
	                                ->where('follow_user',$requestdata->request_to_user)
	                                ->where('follow_following',$requestdata->request_from_user)
	                                ->first();
	                if(!$con1_check){
	           
	                   	DB::table('user_follows')->insert([
	                                                'follow_user'       =>  $requestdata->request_from_user,
	                                                'follow_following'  =>  $requestdata->request_to_user
	                                ]);
	                }
	                if(!$con2_check){
	                    DB::table('user_follows')->insert([
	                                                'follow_user'       =>  $requestdata->request_to_user,
	                                                'follow_following'  =>  $requestdata->request_from_user
	                                ]);
	                }
				}

				$status = $request->request_status==1 ? 'accepted' : 'rejected';

				$activities = DB::select("SELECT users.id, users.firstName, users.lastName, users.profile_photo_path, connection_requests.created, connection_requests._request_id FROM users INNER JOIN connection_requests ON connection_requests.request_from_user=users.id WHERE connection_requests.request_to_user='".$request->user_id."' AND request_status=0 ORDER BY connection_requests._request_id DESC");

				return response([
                				'response_code'	=>	'200',
                         		'response_msg'	=> 	'Request '.$status.' successfully',
                         		'reponse_body' 	=> 	['activities' => $activities],
                         		'time'			=>	1000
                        	], 200);
			}
		}
    }

    public function cancelRequest(Request $request){
    	$rules =[
			        'user_id' 			=> 	'required',
			        '_request_id'		=>	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$requestdata = 	DB::table('connection_requests')
								->join('feeds','feeds._feed_id','=','connection_requests.request_feed')
								->where('_request_id', $request->_request_id)
								->where('request_from_user', $request->user_id)
								->where('request_status','=',0)
								->first();
			if(!$requestdata){
				return response([
			        'response_code'=>'201',
			        'response_msg'=> 'Invalid request',
			        'reponse_body' => "null"
			    ], 401);
			}else{
				DB::table('connection_requests')->where('_request_id',$request->_request_id)->delete();

				$activities = DB::select("SELECT users.id, users.firstName, users.lastName, users.profile_photo_path, connection_requests.created, connection_requests._request_id FROM users INNER JOIN connection_requests ON connection_requests.request_from_user=users.id WHERE connection_requests.request_to_user='".$request->user_id."' AND request_status=0 ORDER BY connection_requests._request_id DESC");

				return response([
                				'response_code'	=>	'200',
                         		'response_msg'	=> 	'Request cancelled successfully',
                         		'reponse_body' 	=> 	['activities' => $activities],
                         		'time'			=>	1000
                        	], 200);
			}
		}
    }

    function connectionsList(Request $request){
    	$rules =[
			        'user_id' 			=> 	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$connections = DB::select("SELECT users.firstName, users.lastName, users.profile_photo_path, users.id, users.username, users.quick_blox_id, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status =1 AND ((request_from_user=$request->user_id AND request_to_user=users.id) OR (request_from_user=users.id AND request_to_user=$request->user_id))), 1, 0)) AS is_connected,
				(SELECT IF ((SELECT COUNT(*) FROM user_follows WHERE follow_user=$request->user_id AND follow_following=users.id) , 1, 0)) AS is_following,
				 (SELECT event_type FROM life_event_categories 
				 INNER JOIN feeds ON feeds.feed_lifeevent=life_event_categories.id
				 INNER JOIN connection_requests ON connection_requests.request_feed=feeds._feed_id
				 WHERE connection_requests.request_status=1
				 AND ((connection_requests.request_from_user=$request->user_id AND connection_requests.request_to_user=users.id) OR (connection_requests.request_from_user=users.id AND connection_requests.request_to_user=$request->user_id))
				 ) AS event_type,
				 (SELECT COUNT(DISTINCT feed_lifeevent) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND ((feed_user1 = '".$request->user_id."' AND feed_user2 = users.id) OR (feed_user1 = users.id AND feed_user2 = '".$request->user_id."'))) AS events_type_count,
				 (SELECT GROUP_CONCAT(DISTINCT event_type) FROM life_event_categories WHERE life_event_categories.id IN(SELECT DISTINCT feed_lifeevent FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND ((feed_user1 = '".$request->user_id."' AND feed_user2 = users.id) OR (feed_user1 = users.id AND feed_user2 = '".$request->user_id."')) )) AS events_type_names
				FROM users 
				WHERE id!='".$request->user_id."' 
				AND id NOT IN(SELECT bu_to_id FROM blocked_users WHERE bu_from_id='".$request->user_id."')
				HAVING is_connected=1
				");

			return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'Connections fetched successfully',
                  				'reponse_body' 	=> 	['connections' => $connections]
                  			], 200);
		}
    }

    public function followUnfollow(Request $request){
    	$rules =[
			        'user_id' 			=> 	'required',
			        'followed_user'		=>	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$check_follow = DB::table('user_follows')
								->where('follow_user', $request->user_id)
								->where('follow_following', $request->followed_user)
								->first();
			if($check_follow){
				DB::table('user_follows')->where('_follow_id',$check_follow->_follow_id)->delete();
				DB::table('user_unfollows')->insert([
								'unfollow_from_user'	=> 	$request->user_id,
								'unfollow_to_user'		=>	$request->followed_user
							]);
				$followed = 'unfollowed';
			}else{
				$unfoll_check = DB::table('user_unfollows')
									->where('unfollow_from_user', $request->user_id)
									->where('unfollow_to_user', $request->followed_user)
									->first();
				if($unfoll_check){
					DB::table('user_unfollows')->where('_unfollow_id',$unfoll_check->_unfollow_id)->delete();
				}
				DB::table('user_follows')
					->insert([
						'follow_user'		=> 	$request->user_id,
						'follow_following'	=>	$request->followed_user
				]);
				$followed = 'followed';
			}

			$connections = DB::select("SELECT users.firstName, users.lastName, users.profile_photo_path, users.id, users.username, users.quick_blox_id, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status =1 AND ((request_from_user=$request->user_id AND request_to_user=users.id) OR (request_from_user=users.id AND request_to_user=$request->user_id))), 1, 0)) AS is_connected,
				(SELECT IF ((SELECT COUNT(*) FROM user_follows WHERE follow_user=$request->user_id AND follow_following=users.id) , 1, 0)) AS is_following,
				 (SELECT event_type FROM life_event_categories 
				 INNER JOIN feeds ON feeds.feed_lifeevent=life_event_categories.id
				 INNER JOIN connection_requests ON connection_requests.request_feed=feeds._feed_id
				 WHERE connection_requests.request_status=1
				 AND ((connection_requests.request_from_user=$request->user_id AND connection_requests.request_to_user=users.id) OR (connection_requests.request_from_user=users.id AND connection_requests.request_to_user=$request->user_id))
				 ) AS event_type,
				 (SELECT COUNT(DISTINCT feed_lifeevent) FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND ((feed_user1 = '".$request->user_id."' AND feed_user2 = users.id) OR (feed_user1 = users.id AND feed_user2 = '".$request->user_id."'))) AS events_type_count,
				 (SELECT GROUP_CONCAT(DISTINCT event_type) FROM life_event_categories WHERE life_event_categories.id IN(SELECT DISTINCT feed_lifeevent FROM feeds WHERE feed_type='matched' AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id='".$request->user_id."') AND ((feed_user1 = '".$request->user_id."' AND feed_user2 = users.id) OR (feed_user1 = users.id AND feed_user2 = '".$request->user_id."')) )) AS events_type_names
				FROM users 
				WHERE id='".$request->followed_user."'
				");

			return response([
                      	'response_code'	=>	'200',
                				'response_msg'	=> 	"User $followed successfully",
                  			'reponse_body' 	=> 	['connections' => $connections ? $connections[0] : []],
                        'time'          =>  1000
                  		], 200);
		}
    }

    public function feedsList(Request $request){

    	//print_r($request->user_id);exit();
    	$rules =[
  			        'user_id' 			  => 	'required',
  			        'search_query'		=>	'nullable',
  			        'life_event_type'	=>	'nullable',
  			        'keywords'			  => 	'nullable',
  			        'search_category'	=>	'nullable',
  			        'search_type'		  =>	'nullable',
                'limit'           =>  'nullable',
                'offset'          =>  'nullable'
		          ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$user_data = User::find($request->user_id);
      $limit = $request->limit ? $request->limit : 10;
      $offset = $request->offset ? $request->offset : 0;
      $feed_title = '';
			if(isset($request->search_type) && $request->search_type=='life_event'){
				/*$where = ' AND (((feed_user1="'.$request->user_id.'" OR feed_user1 IN(SELECT DISTINCT follow_following FROM user_follows WHERE follow_user="'.$request->user_id.'")) AND (feed_user2 IN(SELECT DISTINCT follow_following FROM user_follows WHERE follow_user="'.$request->user_id.'") OR feed_user2 NOT IN(SELECT DISTINCT unfollow_to_user FROM user_unfollows WHERE unfollow_from_user="'.$request->user_id.'"))) OR (feed_user2="'.$request->user_id.'" AND feed_user1 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$request->user_id.'") AND feed_user2 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$request->user_id.'") AND (feed_user1 IN(SELECT DISTINCT follow_following FROM user_follows WHERE follow_user="'.$request->user_id.'") OR feed_user1 NOT IN(SELECT DISTINCT unfollow_to_user FROM user_unfollows WHERE unfollow_from_user="'.$request->user_id.'")))) ';*/
        $where = ' AND (feed_user1="'.$request->user_id.'" OR feed_user2="'.$request->user_id.'")';
			}else if(isset($request->search_type) && $request->search_type=='matched'){
        $where = ' AND (feed_user1="'.$request->user_id.'" OR feed_user2="'.$request->user_id.'") AND feed_type="matched"';
        $feed_title = 'Match Feed';
      }
      else{
				$where = '';
			}
			
			if($request->keywords){
				$keys = explode(',',$request->keywords);
				$where .= " AND (";
				$or = '';
				foreach($keys as $k => $val){
					if($k >0){
						$or = ' OR';
					}
					$where .= $or." feed_keywords like '%".$val."%'";
				}
				$where .= ")";
				//$where .= " AND FIND_IN_SET(feed_keywords, '".$request->keywords."')";
        $feed_title = 'Keyword Feed';
			}

			if(!$request->search_category && $request->search_query){
				$where .= " AND (feed_what like '%".$request->search_query."%' OR feed_where like '%".$request->search_query."%' OR feed_when like '%".$request->search_query."%' OR feed_why like '%".$request->search_query."%' OR feed_keywords like '%".$request->search_query."%')";
			}elseif($request->search_category && $request->search_query){
				$where .= " AND (";
				$categories = explode(',',$request->search_category);
				$where .= " feed_keywords like '%".$request->search_query."%'";
				foreach ($categories as $key => $value) {
					//if($key > 0){
					 $or = ' OR';
					//}
					if($value=='what'){
						$where .= $or." feed_what like '%".$request->search_query."%'";
					}
					elseif($value=='where'){
						$where .= $or." feed_where like '%".$request->search_query."%'";
					}
					elseif($value=='when'){
						$where .= $or." feed_when like '%".$request->search_query."%'";
					}
					elseif($value=='why'){
						$where .= $or." feed_why like '%".$request->search_query."%'";
					}
				}

				$where .= ")";
        $feed_title = 'Filter Feed';
			}

			if($request->life_event_type){
				$where .= " AND feed_lifeevent IN(".$request->life_event_type.")";
				$types = explode(',', $request->life_event_type);

				$where .= " AND (";
				foreach ($types as $key => $value) {
					$or = $key==0 ? '' : 'OR';
					if($value==1){
						$where .= " $or ((feed_lifeevent=1 AND feed_match_count>='".$user_data->match_personal."'))";
            $feed_title = 'Personal Feed';
					}
          //feed_type='connected' OR 
					if($value==2){
						$where .= " $or ((feed_lifeevent=2 AND feed_match_count>='".$user_data->match_dating."'))";
            $feed_title = 'Dating Feed';
					}
          //feed_type='connected' OR 
					if($value==3){
						$where .= " $or ((feed_lifeevent=3 AND feed_match_count>='".$user_data->match_adoption."'))";
            $feed_title = 'Adoption Feed';
					}
          //feed_type='connected' OR 
					if($value==4){
						$where .= " $or ((feed_lifeevent=4 AND feed_match_count>='".$user_data->match_travel."'))";
            $feed_title = 'Travel Feed';
					}
          //feed_type='connected' OR 
					if($value==5){
						$where .= " $or ((feed_lifeevent=5 AND feed_match_count>='".$user_data->match_military."'))";
            $feed_title = 'Military Feed';
					}
          //feed_type='connected' OR 
					if($value==6){
						$where .= " $or ((feed_lifeevent=6 AND feed_match_count>='".$user_data->match_education."'))";
            $feed_title = 'Education Feed';
					}
          //feed_type='connected' OR 
					if($value==7){
						$where .= " $or ((feed_lifeevent=7 AND feed_match_count>='".$user_data->match_work."'))";
            $feed_title = 'Career Feed';
					}
          //feed_type='connected' OR 
					if($value==8){
						$where .= " $or ((feed_lifeevent=8 AND feed_match_count>='".$user_data->match_pet."'))";
            $feed_title = 'Pets Feed';
					}
          //feed_type='connected' OR 
					if($value==9){
						$where .= " $or ((feed_lifeevent=9 AND feed_match_count>='".$user_data->match_lostfound."'))";
            $feed_title = 'Lost & Found Feed';
					}
          //feed_type='connected' OR 
          if($value==11){
            $where .= " $or ( (feed_lifeevent=11  AND feed_match_count>='".$user_data->match_username."'))";
            $feed_title = 'Username Connect Feed';
          }
          if($value==12){
            $where .= " $or ( (feed_lifeevent=12 AND feed_match_count>='".$user_data->match_activity."'))";
            $feed_title = 'Activity Feed';
          }
          //feed_type='connected' OR

				}
				$where .= " )";
        if(count($types)>1){
          $feed_title = 'Filter Feed';
        }
			}else{
        //feed_type='connected' OR 
				$where .= " AND (((feed_lifeevent=1 AND feed_match_count>='".$user_data->match_personal."') OR (feed_lifeevent=2 AND feed_match_count>='".$user_data->match_dating."') OR (feed_lifeevent=3 AND feed_match_count>='".$user_data->match_adoption."') OR (feed_lifeevent=4 AND feed_match_count>='".$user_data->match_travel."') OR (feed_lifeevent=5 AND feed_match_count>='".$user_data->match_military."') OR (feed_lifeevent=6 AND feed_match_count>='".$user_data->match_education."') OR (feed_lifeevent=7 AND feed_match_count>='".$user_data->match_work."') OR (feed_lifeevent=8 AND feed_match_count>='".$user_data->match_pet."') OR (feed_lifeevent=9 AND feed_match_count>='".$user_data->match_lostfound."') OR (feed_lifeevent=11 AND feed_match_count >= '".$user_data->match_username."') OR (feed_lifeevent=12 AND feed_match_count >= '".$user_data->match_activity."')))";//
        if(!$request->keywords && !in_array($request->search_type,['life_event','matched'])){
          $feed_title = 'Global Feed';
        }
			}


			$select = 'SELECT feeds.*, life_event_categories.event_type AS feed_event_type,
                    u1.profile_photo_path AS user1_profile_photo, u1.firstName AS user1_firstName, u1.lastName AS user1_lastName, u1.quick_blox_id AS user1_quick_blox_id, u2.profile_photo_path AS user2_profile_photo, 
                    u2.firstName AS user2_firstName, u2.lastName AS user2_lastName,u2.quick_blox_id AS user2_quick_blox_id, 
                    (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 1 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))), 1, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 0 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))),2,0)))) AS is_connected,
                    (SELECT COUNT(_feed_id) FROM feeds WHERE _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id="'.$request->user_id.'") '.$where.') AS feed_count
                     FROM feeds 
                    INNER JOIN users u1 ON u1.id=feeds.feed_user1
                    INNER JOIN users u2 ON u2.id=feeds.feed_user2
                    INNER JOIN life_event_categories ON feeds.feed_lifeevent=life_event_categories.id
            						WHERE _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id="'.$request->user_id.'") AND feed_user1 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$request->user_id.'") AND feed_user2 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$request->user_id.'") '.$where.' ORDER BY _feed_id DESC LIMIT '.$offset.','.$limit.'
            					';
      //echo $select;exit();
      $select2 = 'SELECT feeds.*, life_event_categories.event_type AS feed_event_type,
                    u1.profile_photo_path AS user1_profile_photo, u1.firstName AS user1_firstName, u1.lastName AS user1_lastName, u1.quick_blox_id AS user1_quick_blox_id, u2.profile_photo_path AS user2_profile_photo, 
                    u2.firstName AS user2_firstName, u2.lastName AS user2_lastName,u2.quick_blox_id AS user2_quick_blox_id, 
                    (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 1 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))), 1, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 0 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))),2,0)))) AS is_connected,
                    (SELECT COUNT(_feed_id) FROM feeds WHERE _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id="'.$request->user_id.'") '.$where.') AS feed_count
                     FROM feeds 
                    INNER JOIN users u1 ON u1.id=feeds.feed_user1
                    INNER JOIN users u2 ON u2.id=feeds.feed_user2
                    INNER JOIN life_event_categories ON feeds.feed_lifeevent=life_event_categories.id
                        WHERE _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id="'.$request->user_id.'") AND feed_user1 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$request->user_id.'") AND feed_user2 NOT IN (SELECT DISTINCT bu_to_id FROM blocked_users WHERE bu_from_id="'.$request->user_id.'") '.$where.' ORDER BY _feed_id DESC
                      ';
            //echo $select;exit;

			$feeds = DB::select($select);
      $feeds2 = DB::select($select2);
			//$feed_count = isset($feeds[0]->feed_count) ? $feeds[0]->feed_count : 0;
			$new_feeds = [];
      $new_feeds2 = [];
			if($feeds){
				foreach($feeds as $key => $val){
					$feed_images = DB::table('feed_images')->where('fi_feed_id',$val->_feed_id)->get();
					$val->feed_images = $feed_images;
					array_push($new_feeds, $val);
				}
			}
      if($feeds2){
        foreach($feeds2 as $key => $val){
          $feed_images = DB::table('feed_images')->where('fi_feed_id',$val->_feed_id)->get();
          $val->feed_images = $feed_images;
          array_push($new_feeds2, $val);
        }
      }
      $feed_count = $feeds2 ? count($feeds2) : 0;
			/*print_r($new_feeds);*//*exit();*/
			$feed_div = '';

			if($request->search_from && $request->search_from=='web'){
				$feed_div = view('xhr.search_feed',['feeds' => $new_feeds, 'user_id' => $request->user_id, 'user_quick_blox_id' => $user_data->quick_blox_id,'feed_count'=>$feed_count])->render();
			}
        return response([
                      		'response_code'	=>	'200',
                				  'response_msg'	=> 	'Feeds fetched successfully',
                  				'reponse_body' 	=> 	[
                                                'feeds'     =>  $new_feeds,
                                                'feed_div'  =>  $feed_div,
                                                'feed_count'=>  $feed_count,
                                                'feed_title'=>  $feed_title,
                                                'limit'     =>  $limit,
                                                'offset'    =>  $offset
                                              ]
                  			], 200);
		  }
    }

    public function hideFeed(Request $request){
    	$rules =[
			        'user_id' 	=> 	'required',
			        '_feed_id'	=>	'required'
			    ];
  		$validate = Validator::make($request->all(),$rules);
  		if($validate->fails()){
  			return response([
                  				'response_code'	=>	'201',
                           		'response_msg'	=> 	'Not enough details are provided',
                           		'reponse_body' 	=> 	"null",
                           		'errors'		=>	$validate->errors()
                          	], 401);
  		}else{

  			$check_feed = DB::table('hidden_feeds')
  							->where('hf_user_id', $request->user_id)
  							->where('hf_feed_id', $request->_feed_id)
  							->first();
  			if($check_feed){
  				return response([
  			        'response_code'=>'201',
  			        'response_msg'=> 'Already hidden',
  			        'reponse_body' => "null"
  			    ], 401);
  			}else{
  				DB::table('hidden_feeds')->insert(['hf_user_id' => $request->user_id,'hf_feed_id' => $request->_feed_id]);

  				$where = ' WHERE (feed_user1="'.$request->user_id.'" OR feed_user2="'.$request->user_id.'") AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id="'.$request->user_id.'")';
  				if($request->keywords){
  					$keys = explode(',',$request->keywords);
  					$where .= " AND (";
  					$or = '';
  					foreach($keys as $k => $val){
  						if($k >0){
  							$or = ' OR';
  						}
  						$where .= $or." feed_keywords like '%".$val."%'";
  					}
  					$where .= ")";
  					//$where .= " AND FIND_IN_SET(feed_keywords, '".$request->keywords."')";
  				}

  				if(!$request->search_category && $request->search_query){
  					$where .= " AND (feed_what like '%".$request->search_query."%' OR feed_where like '%".$request->search_query."%' OR feed_when like '%".$request->search_query."%' OR feed_why like '%".$request->search_query."%')";
  				}elseif($request->search_category && $request->search_query){
  					$where .= " AND (";
  					$categories = explode(',',$request->search_category);
  					$or = '';
  					foreach ($categories as $key => $value) {
  						if($key > 0){
  							$or = ' OR';
  						}
  						if($value=='what'){
  							$where .= $or." feed_what like '%".$request->search_query."%'";
  						}
  						elseif($value=='where'){
  							$where .= $or." feed_where like '%".$request->search_query."%'";
  						}
  						elseif($value=='when'){
  							$where .= $or." feed_when like '%".$request->search_query."%'";
  						}
  						elseif($value=='why'){
  							$where .= $or." feed_why like '%".$request->search_query."%'";
  						}
  					}

  					$where .= ")";
  				}

  				if($request->life_event_type){
  					$where .= " AND feed_lifeevent IN(".$request->life_event_type.")";
  				}

  				$select = 'SELECT feeds.*, u1.quick_blox_id AS user1_quick_blox_id, u2.quick_blox_id AS user2_quick_blox_id, u1.profile_photo_path AS user1_profile_photo, u1.firstName AS user1_firstName, u1.lastName AS user1_lastName, u2.profile_photo_path AS user2_profile_photo, u2.firstName AS user2_firstName, u2.lastName AS user2_lastName, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 1 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))), 1, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 0 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))),2,0)))) AS is_connected FROM feeds 
  	            						INNER JOIN users u1 ON u1.id=feeds.feed_user1
  	            						INNER JOIN users u2 ON u2.id=feeds.feed_user2
  	            						'.$where.' ORDER BY _feed_id DESC
  	            					';
  	            //echo $select;exit;

  				$feeds = DB::select($select);
  				$new_feeds = [];
  				if($feeds){
  					foreach($feeds as $key => $val){
  						$feed_images = DB::table('feed_images')->where('fi_feed_id',$val->_feed_id)->get();
  						$val->feed_images = $feed_images;
  						array_push($new_feeds, $val);
  					}
  				}
          if(isset($request->request_from) && $request->request_from=='my-profile'){
            return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Feed dismissed successfully',
                            'reponse_body'  =>  ['feeds' => $new_feeds],
                            'time'          =>  1000
                          ], 200);
          }
          return response([
  	                      	'response_code'	=>	'200',
  	                				'response_msg'	=> 	'Feed dismissed successfully',
  	                  			'reponse_body' 	=> 	['feeds' => $new_feeds],
  	                  				/*'time'			=>	1000*/
  	                  		], 200);
  			}
  		}
    }

    public function unhideFeed(Request $request){
    	$rules =[
			        'user_id' 	=> 	'required',
			        '_feed_id'	=>	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{

			$check_feed = DB::table('hidden_feeds')
							->where('hf_user_id', $request->user_id)
							->where('hf_feed_id', $request->_feed_id)
							->first();
			if($check_feed){
				
				DB::table('hidden_feeds')->where('_hf_id',$check_feed->_hf_id)->delete();
				$where = ' WHERE (feed_user1="'.$request->user_id.'" OR feed_user2="'.$request->user_id.'") AND _feed_id NOT IN(SELECT DISTINCT hf_feed_id FROM hidden_feeds WHERE hf_user_id="'.$request->user_id.'")';
				if($request->keywords){
					$keys = explode(',',$request->keywords);
					$where .= " AND (";
					$or = '';
					foreach($keys as $k => $val){
						if($k >0){
							$or = ' OR';
						}
						$where .= $or." feed_keywords like '%".$val."%'";
					}
					$where .= ")";
					//$where .= " AND FIND_IN_SET(feed_keywords, '".$request->keywords."')";
				}

				if(!$request->search_category && $request->search_query){
					$where .= " AND (feed_what like '%".$request->search_query."%' OR feed_where like '%".$request->search_query."%' OR feed_when like '%".$request->search_query."%' OR feed_why like '%".$request->search_query."%')";
				}elseif($request->search_category && $request->search_query){
					$where .= " AND (";
					$categories = explode(',',$request->search_category);
					$or = '';
					foreach ($categories as $key => $value) {
						if($key > 0){
							$or = ' OR';
						}
						if($value=='what'){
							$where .= $or." feed_what like '%".$request->search_query."%'";
						}
						elseif($value=='where'){
							$where .= $or." feed_where like '%".$request->search_query."%'";
						}
						elseif($value=='when'){
							$where .= $or." feed_when like '%".$request->search_query."%'";
						}
						elseif($value=='why'){
							$where .= $or." feed_why like '%".$request->search_query."%'";
						}
					}

					$where .= ")";
				}

				if($request->life_event_type){
					$where .= " AND feed_lifeevent IN(".$request->life_event_type.")";
				}

				$select = 'SELECT feeds.*, u1.quick_blox_id AS user1_quick_blox_id, u2.quick_blox_id AS user2_quick_blox_id, u1.profile_photo_path AS user1_profile_photo, u1.firstName AS user1_firstName, u1.lastName AS user1_lastName, u2.profile_photo_path AS user2_profile_photo, u2.firstName AS user2_firstName, u2.lastName AS user2_lastName, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 1 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))), 1, (SELECT IF ((SELECT COUNT(*) FROM connection_requests WHERE request_status = 0 AND ((request_from_user=u1.id AND request_to_user=u2.id) OR (request_from_user=u2.id AND request_to_user=u1.id))),2,0)))) AS is_connected FROM feeds 
	            						INNER JOIN users u1 ON u1.id=feeds.feed_user1
	            						INNER JOIN users u2 ON u2.id=feeds.feed_user2
	            						'.$where.' ORDER BY _feed_id DESC
	            					';
	            //echo $select;exit;

				$feeds = DB::select($select);
				$new_feeds = [];
				if($feeds){
					foreach($feeds as $key => $val){
						$feed_images = DB::table('feed_images')->where('fi_feed_id',$val->_feed_id)->get();
						$val->feed_images = $feed_images;
						array_push($new_feeds, $val);
					}
				}

	            return response([
	                      			'response_code'	=>	'200',
	                				'response_msg'	=> 	'Feed dismiss undoed successfully',
	                  				'reponse_body' 	=> 	['feeds' => $new_feeds],
	                  				/*'time'			=>	1000*/
	                  			], 200);
			}else{
				return response([
			        'response_code'=>'201',
			        'response_msg'=> 'Invalid request',
			        'reponse_body' => "null"
			    ], 401);
				
			}
		}
    }

    public function addCall(Request $request){
    	$rules =[
			        'user_id' 			=> 	'required',
			        'reciever_id'		=>	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{

			$decoded_data = [];
			if(isset($request->call_session) && $request->call_session){
				/*print_r($request->call_session);exit();*/
				$decoded_data = json_decode($request->call_session,TRUE);
			}
			$call_session = isset($request->call_session) ? $request->call_session : NULL;
			$log_session_id = ($decoded_data && isset($decoded_data['id'])) ?  $decoded_data['id'] : NULL;
			if($call_session){
				$call_session = json_encode($call_session,JSON_UNESCAPED_SLASHES);
			}
			$call_id = DB::table('call_logs')->insertGetId(['log_caller_id' => $request->user_id,'log_reciever_id' => $request->reciever_id,'log_session' => $call_session,'log_session_id'=>$log_session_id]);

			$select = 'SELECT call_logs.*, u1.username AS caller_username, u1.quick_blox_id AS caller_quick_blox_id, u2.username AS reciever_username, u2.quick_blox_id AS reciever_quick_blox_id, u1.profile_photo_path AS caller_profile_photo, u1.firstName AS caller_firstName, u1.lastName AS caller_lastName, u2.profile_photo_path AS reciever_profile_photo, u2.firstName AS reciever_firstName, u2.lastName AS reciever_lastName, (SELECT IF (call_logs.log_caller_id="'.$request->user_id.'","dialled","recieved")) AS call_type FROM call_logs 
	            						INNER JOIN users u1 ON u1.id=call_logs.log_caller_id
	            						INNER JOIN users u2 ON u2.id=call_logs.log_reciever_id
	            						WHERE (call_logs.log_caller_id="'.$request->user_id.'" OR call_logs.log_reciever_id="'.$request->user_id.'") ORDER BY _log_id DESC
	            					';
	            //echo $select;exit;

			$call_logs = DB::select($select);

			$to_user_data = DB::table('users')
								->join('device_tokens','users.id','=','device_tokens.device_user_id')
								->select('device_tokens.*')
								->where('users.id',$request->reciever_id)
								->get();
			$from_user_data = DB::table('users')->where('users.id',$request->user_id)->first();
			if($to_user_data){
				foreach($to_user_data as $to_user){
					$param =[
								'type' 			=> 	'call',
								'user_data'		=>	$from_user_data,
								'call_session'	=>	$request->call_session,
								'call_id'		=>	$call_id
							];
					$title = 'Incoming Call';
					$body = 'You have incoming call from '.$from_user_data->firstName.' '.$from_user_data->lastName;
					$noti_response = sendNotification($to_user->device_type,$to_user->device_token,$title,$body,$param);
					$notification_data =[
		        							'notification_user'		=>	$to_user->device_user_id,
		        							'notification_device'	=>	$to_user->_device_id,
		        							'notification_title'	=>	$title,
		        							'notification_content'	=>	$body,
		        							'notification_status'	=>	$noti_response['success']==1 ? 'success' : 'failed',
		        							'notification_response'	=>	json_encode($noti_response)
		        						];
		        	DB::table('notifications')->insert($notification_data);
				}
			}

            return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'Call added successfully',
                  				'reponse_body' 	=> 	['call_logs' 	=> 	$call_logs
                  									],
                  				'time'			=>	1000
                  			], 200);
		}
    }

    public function updateCallStatus(Request $request){
    	$rules =[
			        'log_session_id' 	=> 	'required',
			        'call_status'		=>	'required|in:recieved,ended'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			if($request->call_status=='recieved'){
				DB::table('call_logs')->where('log_session_id',$request->log_session_id)->update(['log_status' => $request->call_status, 'log_recieve_time' => date('Y-m-d H:i:s')]);
			}else{
				DB::table('call_logs')->where('log_session_id',$request->log_session_id)->update(['log_status' => $request->call_status, 'log_end_time' => date('Y-m-d H:i:s')]);
			}

			return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'Call '.$request->call_status.' successfully',
                  				'reponse_body' 	=> 	NULL,
                  				'time'			=>	1000
                  			], 200);
		}
    }

    public function getCallDetails(Request $request){
    	$rules =[
			        'log_session_id' 			=> 	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$call_details = DB::table('call_logs')->where('log_session_id',$request->log_session_id)
								->first();
			return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'Call details fetched successfully',
                  				'reponse_body' 	=> 	['call_details' => $call_details],
                  				'time'			=>	1000
                  			], 200);
		}
    }

    public function callLogs(Request $request){
    	$rules =[
			        'user_id' 			=> 	'required'
			    ];
		$validate = Validator::make($request->all(),$rules);
		if($validate->fails()){
			return response([
                				'response_code'	=>	'201',
                         		'response_msg'	=> 	'Not enough details are provided',
                         		'reponse_body' 	=> 	"null",
                         		'errors'		=>	$validate->errors()
                        	], 401);
		}else{
			$select = 'SELECT call_logs.*, u1.username AS caller_username, u1.quick_blox_id AS caller_quick_blox_id, u2.username AS reciever_username, u2.quick_blox_id AS reciever_quick_blox_id, u1.profile_photo_path AS caller_profile_photo, u1.firstName AS caller_firstName, u1.lastName AS caller_lastName, u2.profile_photo_path AS reciever_profile_photo, u2.firstName AS reciever_firstName, u2.lastName AS reciever_lastName, (SELECT IF (call_logs.log_caller_id="'.$request->user_id.'","dialled","recieved")) AS call_type FROM call_logs 
	            						INNER JOIN users u1 ON u1.id=call_logs.log_caller_id
	            						INNER JOIN users u2 ON u2.id=call_logs.log_reciever_id
	            						WHERE (call_logs.log_caller_id="'.$request->user_id.'" OR call_logs.log_reciever_id="'.$request->user_id.'") ORDER BY _log_id DESC LIMIT 100
	            					';
	            //echo $select;exit;

			$call_logs = DB::select($select);

            return response([
                      			'response_code'	=>	'200',
                				'response_msg'	=> 	'Call logs fetched successfully',
                  				'reponse_body' 	=> 	['call_logs' => $call_logs],
                  				'time'			=>	1000
                  			], 200);
		}
    }
}
