<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use View;
use Validator;
use App\Models\Feedback;

class Information extends Controller
{
    function feedbacks(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
    	$feedbacks= Feedback::join('feedbacktypes', 'feedbacktypes.feed_id', '=', 'feedback.type')
    						->join('users','users.id','=','feedback.id')
    						->select('feedback.*', 'feedbacktypes.feed_type', 'users.firstName', 'users.lastName', 'users.profile_photo_path')
    						->orderBy('feedback.id','desc')
    						->get();
    	return View::make("admin/feedbacks")->with(['feedbacks' => $feedbacks]);
    }

    function contacts(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
    	$contacts = DB::table('contactedpeople')->orderBy('created_at','desc')->get();
    	return View::make("admin/contacts")->with(['contacts' => $contacts]);
    }

    function submitUserQuery(Request $request){
        $rules =[
                    'query_name'    =>  'required',
                    'query_mobile'  =>  'nullable|numeric',
                    'query_email'   =>  'nullable|email',
                    'query_subject' =>  'required',
                    'query_message' =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            DB::table('user_queries')->insert([
                                                'query_name'    => $request->query_name,
                                                'query_mobile'  => $request->query_mobile,
                                                'query_email'   => $request->query_email,
                                                'query_subject' => $request->query_subject,
                                                'query_message' => $request->query_message
                                            ]);
            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'We will get back or An email to follow up will follow',
                                'reponse_body'  =>  NULL,
                                'show_toast'    =>  TRUE,
                                'time'          =>  3000,
                                'redirect'      =>  url('/')
                            ], 200);
        }
    }

    function userQueries(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $user_queries = DB::table('user_queries')->orderBy('created_at','desc')->get();
        return View::make("admin/user_queries")->with(['user_queries' => $user_queries]);
    }

    function userDonations(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $donations= DB::table('donations')
                            ->join('currencies', 'currencies._currency_id', '=', 'donations.donation_currency')
                            ->join('users','users.id','=','donations.donation_user_id')
                            ->leftJoin('bank_details','bank_details.id','=','donations.donation_bank_id')
                            ->select('donations.*', 'currencies.currency_name', 'currencies.currency_shortname', 'currencies.currency_symbol', 'users.firstName', 'users.lastName', 'users.profile_photo_path','bank_details.bank_name')
                            ->orderBy('donations.id','desc')
                            ->get();
        return View::make("admin/user_donations")->with(['donations' => $donations]);
    }

    function languages(Request $request){
        if (!$request->session()->has('admin_data')) {
            return redirect('admin/login');
        }
        $languages = DB::table('languages')->orderBy('lang_id','desc')->get();
        return View::make("admin/languages")->with(['languages' => $languages]);
    }

    function addLanguageModal(Request $request){
        $html = view('admin.xhr.add_language_modal')->render();
        return response([
                            'response_code' =>  '200',
                            'response_msg'  =>  'Modal Fetched Successfully',
                            'reponse_body'  =>  'null',
                            'modal'         =>  $html
                        ],200);
    }

    function addLanguage(Request $request){
        $rules =[
                    'lang_name'     =>  'required|unique:languages',
                    'short_name'    =>  'required|unique:languages',
                    'flag_url'      =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $done = DB::table('languages')->insert([
                                                        'lang_name'     => $request->lang_name,
                                                        'short_name'    => $request->short_name,
                                                        'flag_url'      => $request->flag_url
                                                    ]);

            if(!$done){
                return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
            }
            else{
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Language is created',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }
        }
    }

    function activeInactiveLanguage(Request $request){
        $rules =[
                    'lang_id'          =>  'required',
                    'lang_is_active'    =>  'required|in:1,0'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $lang_is_active = $request->lang_is_active==1 ? 0 : 1;
            DB::table('languages')->where('lang_id',$request->lang_id)->update(['lang_is_active' => $lang_is_active]);
            $active_inactive = $lang_is_active==1?'activated':'inactivated';

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Language '.$active_inactive.' successfully',
                                'reponse_body'  =>  NULL
                            ], 200);
        }
    }

    function editLanguageModal(Request $request){
        $rules =[
                    'lang_id'   =>  'required',
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $language_data = DB::table('languages')->where('lang_id', $request->lang_id)->first();
            $html = view('admin.xhr.edit_language_modal',['language_data'=> $language_data])->render();

            return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Modal Fetched Successfully',
                                'reponse_body'  =>  'null',
                                'modal'         =>  $html
                            ],200);
        }
    }

    function editLanguage(Request $request){
        $rules =[
                    'lang_name'     =>  'required',//|unique:languages,lang_name,NULL,lang_id,'.$request->lang_id
                    'short_name'    =>  'required',//|unique:languages,short_name,NULL,lang_id,'.$request->lang_id
                    'flag_url'      =>  'required',
                    'lang_id'       =>  'required'
                ];
        $validate = Validator::make($request->all(),$rules);
        if($validate->fails()){
            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Not enough details are provided',
                                'reponse_body'  =>  "null",
                                'errors'        =>  $validate->errors()
                            ], 401);
        }else{
            $title_data = DB::table('languages')->where('lang_id', $request->lang_id)->first();
            if($title_data){
                DB::table('languages')->where('lang_id', $request->lang_id)->update([
                                                        'lang_name'     => $request->lang_name,
                                                        'short_name'    => $request->short_name,
                                                        'flag_url'      => $request->flag_url
                                                    ]);
                return response([
                                'response_code' =>  '200',
                                'response_msg'  =>  'Language is updated',
                                'reponse_body'  =>  "null",
                                'close'         =>  TRUE
                            ], 200);
            }

            return response([
                                'response_code' =>  '201',
                                'response_msg'  =>  'Invalid details',
                                'reponse_body'  =>  "null"
                            ], 401);
        }
    }

    public function reports(Request $request){
        
        $first_donation = DB::table("donations")->orderBy('id','ASC')->first();
        $first_premium = DB::table("chosen_plans")->orderBy('id','ASC')->first();
        $from_date = NULL;
        $to_date = date('Y-m-d');
        if($first_donation && $first_premium){
            if($first_donation->created_at && $first_premium->created_at){
                if(strtotime($first_donation->created_at) > strtotime($first_premium->created_at)){
                    $from_date = date('Y-m-d',strtotime($first_premium->created_at));
                }else{
                    $from_date = date('Y-m-d',strtotime($first_donation->created_at));
                }
            }else if($first_donation->created_at && !$first_premium->created_at){
                $from_date = date('Y-m-d',strtotime($first_premium->created_at));
            }else if(!$first_donation->created_at && $first_premium->created_at){
                $from_date = date('Y-m-d',strtotime($first_donation->created_at));
            }
        }
        if($request->from){
            $from_date = date('Y-m-d',strtotime($request->from));
        }
        if($request->to){
            $to_date = date('Y-m-d',strtotime($request->to));
        }
        $revenue_arr = [];
        while(strtotime($to_date) >= strtotime($from_date)){
            $reven_donation = DB::select("SELECT SUM(donation_amount) AS monthly_donation, COUNT(id) AS don_count FROM donations  WHERE DATE_FORMAT(created_at,'%Y-%m')='".date('Y-m',strtotime($from_date))."' GROUP BY DATE_FORMAT(created_at,'%Y-%m')");
            $monthly_plan = DB::select("SELECT SUM(premium_plans.plan_amount) AS monthly_plans,COUNT(chosen_plans.id)AS plan_count, DATE_FORMAT(chosen_plans.created_at,'%Y-%m') AS monthly2 FROM chosen_plans
                    INNER JOIN premium_plans ON premium_plans.id=chosen_plans.cp_plan_id
                    WHERE DATE_FORMAT(chosen_plans.created_at,'%Y-%m')='".date('Y-m',strtotime($from_date))."' AND premium_plans.id=1
                    GROUP BY monthly2");
            $yearly_plan = DB::select("SELECT SUM(premium_plans.plan_amount) AS yearly_plans,COUNT(chosen_plans.id)AS plan_count, DATE_FORMAT(chosen_plans.created_at,'%Y-%m') AS monthly2 FROM chosen_plans
                    INNER JOIN premium_plans ON premium_plans.id=chosen_plans.cp_plan_id
                    WHERE DATE_FORMAT(chosen_plans.created_at,'%Y-%m')='".date('Y-m',strtotime($from_date))."' AND premium_plans.id=2
                    GROUP BY monthly2");
            $lifetime_plan = DB::select("SELECT SUM(premium_plans.plan_amount) AS lifetime_plans,COUNT(chosen_plans.id)AS plan_count, DATE_FORMAT(chosen_plans.created_at,'%Y-%m') AS monthly2 FROM chosen_plans
                    INNER JOIN premium_plans ON premium_plans.id=chosen_plans.cp_plan_id
                    WHERE DATE_FORMAT(chosen_plans.created_at,'%Y-%m')='".date('Y-m',strtotime($from_date))."' AND premium_plans.id=3
                    GROUP BY monthly2");
            $total_plan = 0;
            $total_plan_count = 0;
            if($monthly_plan && $monthly_plan[0]->monthly_plans){
                $total_plan += $monthly_plan[0]->monthly_plans;
                $total_plan_count += $monthly_plan[0]->plan_count;
            }

            if($yearly_plan && $yearly_plan[0]->yearly_plans){
                $total_plan += $yearly_plan[0]->yearly_plans;
                $total_plan_count += $yearly_plan[0]->plan_count;
            }

            if($lifetime_plan && $lifetime_plan[0]->lifetime_plans){
                $total_plan += $lifetime_plan[0]->lifetime_plans;
                $total_plan_count += $lifetime_plan[0]->plan_count;
            }

            if(($reven_donation && $reven_donation[0]->monthly_donation) || ($total_plan>0)){
                $don_count = isset($reven_donation[0]->don_count) ? $reven_donation[0]->don_count : 0;
                //$plan_count = isset($reven_plan[0]->plan_count) ? $reven_plan[0]->plan_count : 0;
                $total_count = $don_count + $total_plan_count;
                array_push($revenue_arr,[
                                            'date'              =>  $from_date,
                                            'monthly_donation'  =>  isset($reven_donation[0]->monthly_donation) ? $reven_donation[0]->monthly_donation : 0,
                                            'monthly_plans'     =>  $total_plan>0 ? $total_plan : 0,
                                            'total_count'       =>  $total_count,
                                            'plan_arr'          =>  [
                                                                        'monthly_plan' => ($monthly_plan && $monthly_plan[0]->monthly_plans) ? $monthly_plan[0]->monthly_plans : 0,
                                                                        'yearly_plan' => ($yearly_plan && $yearly_plan[0]->yearly_plans) ? $yearly_plan[0]->yearly_plans : 0,
                                                                        'lifetime_plan' => ($lifetime_plan && $lifetime_plan[0]->lifetime_plans) ? $lifetime_plan[0]->lifetime_plans : 0,
                                                                    ]
                                        ]);
            }
            
            $from_date = date('Y-m-d',strtotime("+1 months", strtotime($from_date)));
        }
       // dd($revenue_arr);

        return View::make("admin/reports")->with(['revenue_arr' =>  $revenue_arr]);
    }
}
