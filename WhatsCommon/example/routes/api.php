<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\users;
use App\Http\Controllers\AppInformations;
use App\Http\Controllers\Settings;
use App\Http\Controllers\LifeEvent;
use App\Http\Controllers\Connections;
use App\Http\Controllers\Home;
use App\Http\Controllers\Register;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post("login",[users::class,'getUsers']);
Route::post("get-otp",[users::class,'getOTP']);
//Route::post("new-login",[users::class,'otpBasedLogin']);
Route::post("new-login",[Register::class,'newLogin']);
Route::post("social-login",[users::class,'SocialLogin']);
Route::post("apple-login",[users::class,'appleLogin']);
Route::post("registernew",[users::class,'registerUser']);
Route::post("regDetails",[users::class,'registerDetails']);
Route::post("forgotPass",[users::class,'checkVarifiedEmail']);
Route::post("updateDetails",[users::class,'updateProfile']);
Route::post("changePassword",[users::class,'ChnagePassword']);
Route::post("change-password",[Settings::class,'updatePassword']);
Route::post("checkOtp",[users::class,'verifyOtp']);
Route::post("updateDp",[users::class,'updateImage']);
Route::post("change-profile-picture",[users::class,'changeProfilePicture']);
Route::post("newPass",[users::class,'newPassword']);
Route::post("setLang",[users::class,'SetUserLanguage']);
Route::post("setUserKeys",[users::class,'getUserKeywords']);
Route::post("feedback",[users::class,'submitFeedback']);
Route::post("connect",[users::class,'ConnectContact']);
Route::post("discovery",[users::class,'AllowDiscovery']);
Route::get("appInfo",[AppInformations::class,'GetAppInfo']);
Route::get("saveImage",[users::class,'getProfImage']);
Route::post("contact-us",[AppInformations::class,'ContactUs']);
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post("get-financial-setting",[Settings::class,'findBankDetails']);
Route::post("set-financial-setting",[Settings::class,'addBankDetails']);
Route::post("donate",[Settings::class,'addDonation']);
Route::post("get-notification-setting",[Settings::class,'getNotificationSetting']);
Route::post("set-notification-setting",[Settings::class,'setNotificationSetting']);
Route::post("get-premium-plans",[Settings::class,'getPremiumPlans']);
Route::post("choose-plan",[Settings::class,'choosePlan']);
Route::post("cancel-plan",[Settings::class,'cancelPlan']);
Route::get("countries",[Settings::class,'getCountries']);
Route::post("provinces",[Settings::class,'getProvince']);
Route::post("cities",[Settings::class,'getCity']);
Route::get("currencies",[Settings::class,'getCurrencies']);
Route::post("set-default-bank",[Settings::class,'makeBankAsDefault']);
Route::post("delete-bank",[Settings::class,'deleteBank']);
Route::post("delete-keyword",[users::class,'deleteKeyword']);
Route::post("get-lifeevent-what",[LifeEvent::class,'getSubcategories']);
Route::post("get-lifeevent-what-details",[LifeEvent::class,'getDetailsForSubCategory']);
Route::post("life-event-upload",[LifeEvent::class,'lifeEventUpload']);
Route::post("reverse-lookup",[LifeEvent::class,'reverseLookup']);
Route::post("search-tag",[LifeEvent::class,'TagSearch']);
Route::post("life-events",[LifeEvent::class,'lifeEventsList']);
Route::post("delete-life-event",[LifeEvent::class,'deleteLifeEvent']);
Route::post("connection-request",[Connections::class,'connectionRequest']);
Route::post("activities",[Connections::class,'activityList']);
Route::post("accept-reject-request",[Connections::class,'acceptRejectRequest']);
Route::post("cancel-request",[Connections::class,'cancelRequest']);
Route::post("feeds",[Connections::class,'feedsList']);
Route::post("hide-feed",[Connections::class,'hideFeed']);
Route::post("unhide-feed",[Connections::class,'unhideFeed']);
Route::post("user-profile",[users::class,'userProfile']);
Route::post("get-user-by-qb",[users::class,'getUserByQB']);
Route::post("blocked-users",[Settings::class,'blockedUsersList']);
Route::post("search-user",[Settings::class,'searchUser']);
Route::post("block-user",[Settings::class,'blockUser']);
Route::post("unblock-user",[Settings::class,'unblockUser']);
Route::post("add-master",[LifeEvent::class,'addMaster']);
Route::post("get-master",[LifeEvent::class,'getMasterData']);
Route::post("set-quick-blox-id",[users::class,'setQuickBloxId']);
Route::post("connections",[Connections::class,'ConnectionsList']);
Route::post("set-security-setting",[Settings::class,'setSecuritySetting']);
Route::post("set-feed-setting",[Settings::class,'setFeedSetting']);
Route::post("set-match-setting",[Settings::class,'setMatchSetting']);
Route::post("set-message-setting",[Settings::class,'setMessageSetting']);
Route::post("set-device-token",[users::class,'setDeviceToken']);
Route::post("set-token",[users::class,'setToken']);
Route::post("refresh-token",[users::class,'refreshToken']);
Route::post("refresh-login-log",[Home::class,'refreshLoginLog']);
Route::post("match-settings",[Settings::class,'getMatchSettings']);
Route::post("set-link-account-setting",[Settings::class,'setLinkAccountSetting']);
Route::post("follow-unfollow",[Connections::class,'followUnfollow']);
Route::post("profile",[users::class,'ProfileDetails']);
Route::post("get-share-url",[Home::class,'saveFeedScreenFromURL']);
Route::post("add-call",[Connections::class,'addCall']);
Route::post("call-logs",[Connections::class,'callLogs']);
Route::post("update-call-status",[Connections::class,'updateCallStatus']);
Route::post("get-call-details",[Connections::class,'getCallDetails']);
Route::post("delete-chat",[users::class,'deleteChat']);
Route::post("logout",[users::class,'logOut']);
Route::post("send-sms",[Home::class,'sendSMSMessage']);
Route::post("get-payment-url",[Home::class,'getPaymentUrl']);
Route::post("delete-user",[Home::class,'deleteUser']);
Route::post("get-users",[Home::class,'getUsers']);
Route::post("unpublish-life-event",[LifeEvent::class,'unpublishLifeEvent']);
Route::post("save-life-event",[LifeEvent::class,'updateLifeEvent']);
Route::post("delete-image",[LifeEvent::class,'deleteImage']);