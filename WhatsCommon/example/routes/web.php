<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\users;
use App\Http\Controllers\DbHandler;
use App\Http\Controllers\Register;
use App\Http\Controllers\Pages;
use App\Http\Controllers\Login;
use App\Http\Controllers\Master;
use App\Http\Controllers\Home;
use App\Http\Controllers\Settings;
use App\Http\Controllers\Information;
use App\Http\Controllers\Connections;
use App\Http\Controllers\LifeEvent;
use App\Http\Controllers\Messages;
use App\Http\Controllers\NewsLetter;
use App\Http\Controllers\Notifications;
use App\Http\Controllers\InAppPurchase;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
/*Route::get('/', function () {
    return view('index');
});*/
/*Route::get('login', function () {
    return view('login');
});*/
/*Route::get('home', function () {
    return view('home');
});*/
Route::get('/', [Home::class,'landing']);
Route::get('login', [Home::class,'login']);
Route::get('privacy-policy', [Home::class,'privacyPolicy']);
Route::get('terms-and-condition', [Home::class,'termsAndCondition']);
Route::post('dologin', [Register::class,'dologin']);
Route::post("get-otp",[Register::class,'getOTP']);
Route::post('new-login', [Register::class,'newLogin']);
Route::post('social-login', [Register::class,'SocialLogin']);
Route::post("apple-login",[Register::class,'appleLogin']);
Route::get('home', [Home::class,'index']);
Route::get('settings', [Settings::class,'index']);
Route::get('financial-settings', [Settings::class,'financialSettings']);
Route::get('change-password', [Settings::class,'changePassword']);
Route::get('notification-settings', [Settings::class,'notificationSettings']);
Route::post('set-notification-setting', [Settings::class,'updateNotificationSettings']);
Route::get('link-accounts', [Settings::class,'linkAccounts']);
Route::post('set-link-account', [Settings::class,'updateLinkAccounts']);
Route::post('update-profile', [Settings::class,'updateProfile']);
Route::post('change-profile-picture', [Settings::class,'changeProfilePicture']);
Route::get('security', [Settings::class,'securityAndPrivacy']);
Route::post('set-security-setting', [Settings::class,'updateSecuritySettings']);
Route::get('feed-settings', [Settings::class,'feedSettings']);
Route::post('set-feed-setting', [Settings::class,'updateFeedSetting']);
Route::get('message-settings', [Settings::class,'messageSettings']);
Route::post('set-message-setting', [Settings::class,'updateMessageSettings']);
Route::get('blocked-users', [Settings::class,'blockedUsers']);
Route::get('feedback', [Home::class,'feedback']);
Route::get('contact-us', [Home::class,'contactUs']);
Route::get('about-us', [Home::class,'aboutUs']);
Route::get('premium-plans', [Home::class,'premiumPlans']);
Route::post('create-stripe-payment', [Home::class,'createStripePayment']);
Route::post('show-payment-link-modal',[Home::class,'showPaymentLinkModal']);
Route::post('create-stripe-payment-for-donation', [Home::class,'createStripePaymentForDonation']);
Route::post('submit-donation', [Home::class,'submitDonation']);
Route::post('choose-plan', [Home::class,'choosePlan']);
Route::post('create-squareup-payment', [Home::class,'createSquareupPayment']);
Route::get('donate', [Home::class,'donate']);
Route::post('donate-form', [Home::class,'donateForm']);
Route::post('select-payment-mode-modal',[Home::class,'selectPaymentModeModal']);
Route::post('select-payment-for-plans-modal',[Home::class,'selectPaymentForPlansModal']);
Route::get('connections', [Connections::class,'index']);
Route::get('other-connections', [Connections::class,'otherConnections']);
Route::get('messages', [Messages::class,'index']);
Route::get('messages/chat-init', [Messages::class,'quickAuth']);
Route::post('messages/get-message', [Messages::class,'getMessages']);
Route::post('messages/send-message', [Messages::class,'sendMessage']);
Route::post('create-dialog-modal', [Messages::class,'createDialogModal']);
Route::post('messages/create-dialog', [Messages::class,'createDialog']);
Route::get('messages/get-message-counts', [Messages::class,'getMessageCounts']);
Route::post('messages/get-dialogs', [Messages::class,'getDialogs']);
Route::post('messages/upload-file', [Messages::class,'uploadFile']);
Route::post('messages/delete-chat', [Messages::class,'deleteChat']);
Route::post('messages/get-message-requests', [Messages::class,'getMessageRequests']);
Route::post('messages/show-hide-delete-messages', [Messages::class,'showHideDeleteMessages']);
Route::get('create-lifeevent', [LifeEvent::class,'createLifeEvent']);
Route::post("add-master-modal",[LifeEvent::class,'addMasterModal']);
Route::get('life-events', [LifeEvent::class,'lifeEvents']);
Route::get('edit-life-event', [LifeEvent::class,'editLifeEvent']);
Route::get('drafts', [LifeEvent::class,'drafts']);
Route::get('edit-drafts', [LifeEvent::class,'editLifeEvent']);
Route::get('user-profile', [Home::class,'userProfile']);
Route::get('my-profile', [Home::class,'myProfile']);
Route::get('feed-details', [Home::class,'feedDetails']);
Route::post('save-feed-screen', [Home::class,'saveFeedScreen']);
/*Route::post('save-feed-screen-from-url', [Home::class,'saveFeedScreenFromURL']);*/
Route::post('share-feed-modal', [Home::class,'shareFeedModal']);
Route::post('subscribe-to-newsletter', [NewsLetter::class,'subsribeToUs']);
Route::post('coming-soon-modal', [Home::class,'comingSoonModal']);
Route::post('donate-success-modal', [Home::class,'donateSuccessModal']);
Route::get('do-payment', [Home::class,'doPayment']);
Route::get('payment-success', [Home::class,'PaymentSuccess']);
Route::get('payment-failure', [Home::class,'PaymentFailure']);
Route::post('payment-success-modal', [Home::class,'paymentSuccessModal']);
Route::post('payment-failure-modal', [Home::class,'paymentFailureModal']);
Route::get('test-access', [InAppPurchase::class,'testAccess']);
Route::get('cancel-subscription', [Home::class,'cancelsubscription']);
Route::post('show-image-from-url-modal', [Home::class,'showImageFromUrlModal']);
Route::post('delete-user-modal', [Home::class,'deleteUserModal']);
Route::post('play-video-modal', [Home::class,'playVideoModal']);
/*Route::post('life-event-upload-test', [LifeEvent::class,'uploadTest']);*/
/*Route::get('signup', function () {
    return view('signup');
});*/
Route::get('signup', [Register::class,'index']);
Route::get('logout', [Home::class,'logOut']);
Route::get('curl-check', [Home::class,'curlCheck']);
/*Route::get('signup-details', function () {
    return view('signup_details');
});*/
Route::get('signup-details', [Register::class,'signupDetails']);
Route::get('forgot-password', function () {
    return view('forgot_password');
});
Route::post("register",[Register::class,'stepOne']);
Route::post("register-step-two",[Register::class,'stepTwo']);
Route::post("check-email",[Register::class,'checkEmail']);
Route::get('reset-password', function () {
    return view('set_password');
});
Route::post("set-password",[Register::class,'setPassword']);
//Route::get("userdat",[users::class,'printMsg']);

Route::get("userDet",[DbHandler::class,'getUsersFromDb']);

/*Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');*/
Route::get('admin/login', function () {
    return view('admin.login');
});
/*Route::get('admin/dashboard', function () {
    return view('admin.dashboard');
});*/

Route::get("admin/users",[users::class,'UsersList']);
Route::get("admin/pages",[Pages::class,'PagesList']);
Route::get("admin/edit-page/{_page_id}",[Pages::class,'EditPage']);
Route::post("admin/update-page",[Pages::class,'UpdatePage']);
Route::post("admin/submit-login",[Login::class,'AdminLogin']);
Route::get("admin/feedbacks",[Information::class,'feedbacks']);
Route::get("admin/contacts",[Information::class,'contacts']);
Route::post("submit-user-query",[Information::class,'submitUserQuery']);
Route::get("admin/user-queries",[Information::class,'userQueries']);
Route::get("admin/donations",[Information::class,'userDonations']);
Route::post('admin/get-contact-setting-modal', [Settings::class,'getContactSettings']);
Route::post('admin/update-contact-setting', [Settings::class,'updateContactSettings']);
Route::get("admin/dashboard",[Home::class,'adminDashboard']);
Route::get("admin",[Home::class,'adminDashboard']);
Route::post("admin/profile-modal",[Home::class,'adminProfileModal']);
Route::post("admin/update-profile",[Home::class,'adminProfileUpdate']);
Route::get("admin/logout",[Home::class,'adminLogout']);
Route::post("admin/change-password-modal",[Home::class,'adminChangePasswordModal']);
Route::post("admin/change-password",[Home::class,'adminChangePassword']);
Route::post("admin/active-inactive-user",[Home::class,'activeInactiveUser']);
Route::get("admin/subscriptions",[NewsLetter::class,'subscriptionsList']);
Route::get("admin/news-letters",[NewsLetter::class,'newsLetterList']);
Route::get("admin/add-news-letter",[NewsLetter::class,'addNewsLetter']);
Route::post("admin/create-news-letter",[NewsLetter::class,'createNewsLetter']);
Route::get("admin/edit-news-letter/{_nt_id}",[NewsLetter::class,'editNewsLetter']);
Route::post("admin/update-news-letter",[NewsLetter::class,'updateNewsLetter']);
Route::post("admin/send-news-letter-modal",[NewsLetter::class,'sendNewsLetterModal']);
Route::post("admin/send-news-letter",[NewsLetter::class,'sendNewsLetter']);
Route::get('admin/notifications',[Notifications::class,'index']);
Route::post("admin/add-notification-modal",[Notifications::class,'addNotificationModal']);
Route::post("admin/add-notification",[Notifications::class,'addNotification']);
Route::get("admin/genders",[Master::class,'GenderList']);
Route::post("admin/add-gender-modal",[Master::class,'addGenderModal']);
Route::post("admin/add-gender",[Master::class,'addGender']);
Route::post("admin/edit-gender-modal",[Master::class,'editGenderModal']);
Route::post("admin/edit-gender",[Master::class,'editGender']);
Route::get("admin/relations",[Master::class,'RelationList']);
Route::post("admin/add-relation-modal",[Master::class,'addRelationModal']);
Route::post("admin/add-relation",[Master::class,'addRelation']);
Route::post("admin/edit-relation-modal",[Master::class,'editRelationModal']);
Route::post("admin/edit-relation",[Master::class,'editRelation']);
Route::get("admin/countries",[Master::class,'CountryList']);
Route::get("admin/provinces",[Master::class,'ProvincesList']);
Route::get("admin/cities",[Master::class,'CitiesList']);
Route::get("admin/home-styles",[Master::class,'HomeStyleList']);
Route::post("admin/add-home-style-modal",[Master::class,'addHomeStyleModal']);
Route::post("admin/add-home-style",[Master::class,'addHomeStyle']);
Route::post("admin/edit-home-style-modal",[Master::class,'editHomeStyleModal']);
Route::post("admin/edit-home-style",[Master::class,'editHomeStyle']);
Route::get("admin/home-types",[Master::class,'HomeTypeList']);
Route::post("admin/add-home-type-modal",[Master::class,'addHomeTypeModal']);
Route::post("admin/add-home-type",[Master::class,'addHomeType']);
Route::post("admin/edit-home-type-modal",[Master::class,'editHomeTypeModal']);
Route::post("admin/edit-home-type",[Master::class,'editHomeType']);
Route::get("admin/marital-status",[Master::class,'MaritalStatusList']);
Route::post("admin/add-marital-status-modal",[Master::class,'addMaritalStatusModal']);
Route::post("admin/add-marital-status",[Master::class,'addMaritalStatus']);
Route::post("admin/edit-marital-status-modal",[Master::class,'editMaritalStatusModal']);
Route::post("admin/edit-marital-status",[Master::class,'editMaritalStatus']);
Route::get("admin/vehicle-types",[Master::class,'VehicleTypeList']);
Route::post("admin/add-vehicle-type-modal",[Master::class,'addVehicleTypeModal']);
Route::post("admin/add-vehicle-type",[Master::class,'addVehicleType']);
Route::post("admin/edit-vehicle-type-modal",[Master::class,'editVehicleTypeModal']);
Route::post("admin/edit-vehicle-type",[Master::class,'editVehicleType']);
Route::get("admin/vehicle-makers",[Master::class,'VehicleMakerList']);
Route::post("admin/add-vehicle-maker-modal",[Master::class,'addVehicleMakerModal']);
Route::post("admin/add-vehicle-maker",[Master::class,'addVehicleMaker']);
Route::post("admin/edit-vehicle-maker-modal",[Master::class,'editVehicleMakerModal']);
Route::post("admin/edit-vehicle-maker",[Master::class,'editVehicleMaker']);
Route::get("admin/vehicle-colors",[Master::class,'VehicleColorList']);
Route::post("admin/add-vehicle-color-modal",[Master::class,'addVehicleColorModal']);
Route::post("admin/add-vehicle-color",[Master::class,'addVehicleColor']);
Route::post("admin/edit-vehicle-color-modal",[Master::class,'editVehicleColorModal']);
Route::post("admin/edit-vehicle-color",[Master::class,'editVehicleColor']);
Route::get("admin/industries",[Master::class,'IndustryList']);
Route::post("admin/add-industry-modal",[Master::class,'addIndustryModal']);
Route::post("admin/add-industry",[Master::class,'addIndustry']);
Route::post("admin/edit-industry-modal",[Master::class,'editIndustryModal']);
Route::post("admin/edit-industry",[Master::class,'editIndustry']);
Route::get("admin/categories",[Master::class,'CategoryList']);
Route::post("admin/add-category-modal",[Master::class,'addCategoryModal']);
Route::post("admin/add-category",[Master::class,'addCategory']);
Route::post("admin/edit-category-modal",[Master::class,'editCategoryModal']);
Route::post("admin/edit-category",[Master::class,'editCategory']);
Route::post("admin/add-province-modal",[Master::class,'addProvinceModal']);
Route::post("admin/add-province",[Master::class,'addProvince']);
Route::post("admin/add-city-modal",[Master::class,'addCityModal']);
Route::post("admin/add-city",[Master::class,'addCity']);
Route::get("admin/donation-amounts",[Master::class,'donationAmountsList']);
Route::post("admin/add-donation-amount-modal",[Master::class,'addDonationAmountModal']);
Route::post("admin/add-donation-amount",[Master::class,'addDonationAmount']);
Route::post("admin/edit-donation-amount-modal",[Master::class,'editDonationAmountModal']);
Route::post("admin/edit-donation-amount",[Master::class,'editDonationAmount']);
Route::post("admin/active-inactive-gender",[Master::class,'activeInactiveGender']);
Route::post("admin/active-inactive-relation",[Master::class,'activeInactiveRelation']);
Route::post("admin/active-inactive-marital-status",[Master::class,'activeInactiveMaritalStatus']);
Route::post("admin/active-inactive-home-style",[Master::class,'activeInactiveHomeStyle']);
Route::post("admin/active-inactive-home-type",[Master::class,'activeInactiveHomeType']);
Route::post("admin/active-inactive-vehicle-type",[Master::class,'activeInactiveVehicleType']);
Route::post("admin/active-inactive-vehicle-maker",[Master::class,'activeInactiveVehicleMaker']);
Route::post("admin/active-inactive-vehicle-color",[Master::class,'activeInactiveVehicleColor']);
Route::post("admin/active-inactive-industry",[Master::class,'activeInactiveIndustry']);
Route::post("admin/active-inactive-occupation",[Master::class,'activeInactiveOccupation']);
Route::post("admin/active-inactive-country",[Master::class,'activeInactiveCountry']);
Route::post("admin/active-inactive-province",[Master::class,'activeInactiveProvince']);
Route::post("admin/active-inactive-city",[Master::class,'activeInactiveCity']);
Route::get("admin/after-schools",[Master::class,'afterSchoolsList']);
Route::post("admin/add-after-schools-modal",[Master::class,'addAfterSchoolsModal']);
Route::post("admin/add-after-schools",[Master::class,'addAfterSchools']);
Route::post("admin/active-inactive-after-schools",[Master::class,'activeInactiveAfterSchools']);
Route::post("admin/edit-after-schools-modal",[Master::class,'editAfterSchoolsModal']);
Route::post("admin/edit-after-schools",[Master::class,'editAfterSchools']);
Route::get("admin/age-ranges",[Master::class,'ageRangesList']);
Route::post("admin/add-age-range-modal",[Master::class,'addAgeRangeModal']);
Route::post("admin/add-age-range",[Master::class,'addAgeRange']);
Route::post("admin/active-inactive-age-ranges",[Master::class,'activeInactiveAgeRanges']);
Route::post("admin/edit-age-range-modal",[Master::class,'editAgeRangeModal']);
Route::post("admin/edit-age-range",[Master::class,'editAgeRange']);
Route::get("admin/military-branches",[Master::class,'militaryBranchesList']);
Route::post("admin/add-military-branch-modal",[Master::class,'addMilitaryBranchModal']);
Route::post("admin/add-military-branch",[Master::class,'addMilitaryBranch']);
Route::post("admin/edit-military-branch-modal",[Master::class,'editMilitaryBranchModal']);
Route::post("admin/edit-military-branch",[Master::class,'editMilitaryBranch']);
Route::get("admin/military-ranks",[Master::class,'militaryRanksList']);
Route::post("admin/add-military-rank-modal",[Master::class,'addMilitaryRankModal']);
Route::post("admin/add-military-rank",[Master::class,'addMilitaryRank']);
Route::post("admin/edit-military-rank-modal",[Master::class,'editMilitaryRankModal']);
Route::post("admin/edit-military-rank",[Master::class,'editMilitaryRank']);
Route::get("admin/military-bases",[Master::class,'militaryBasesList']);
Route::post("admin/add-military-base-modal",[Master::class,'addMilitaryBaseModal']);
Route::post("admin/add-military-base",[Master::class,'addMilitaryBase']);
Route::post("admin/edit-military-base-modal",[Master::class,'editMilitaryBaseModal']);
Route::post("admin/edit-military-base",[Master::class,'editMilitaryBase']);
Route::get("admin/military-units",[Master::class,'militaryUnitsList']);
Route::post("admin/add-military-unit-modal",[Master::class,'addMilitaryUnitModal']);
Route::post("admin/add-military-unit",[Master::class,'addMilitaryUnit']);
Route::post("admin/edit-military-unit-modal",[Master::class,'editMilitaryUnitModal']);
Route::post("admin/edit-military-unit",[Master::class,'editMilitaryUnit']);
Route::get("admin/military-mos",[Master::class,'militaryMosList']);
Route::post("admin/add-military-mos-modal",[Master::class,'addMilitaryMosModal']);
Route::post("admin/add-military-mos",[Master::class,'addMilitaryMos']);
Route::post("admin/edit-military-mos-modal",[Master::class,'editMilitaryMosModal']);
Route::post("admin/edit-military-mos",[Master::class,'editMilitaryMos']);
Route::get("admin/military-mos-title",[Master::class,'militaryMosTitleList']);
Route::post("admin/add-military-mos-title-modal",[Master::class,'addMilitaryMosTitleModal']);
Route::post("admin/add-military-mos-title",[Master::class,'addMilitaryMosTitle']);
Route::post("admin/edit-military-mos-title-modal",[Master::class,'editMilitaryMosTitleModal']);
Route::post("admin/edit-military-mos-title",[Master::class,'editMilitaryMosTitle']);
Route::get("admin/testimonials",[Master::class,'testimonialsList']);
Route::post("admin/add-testimonial-modal",[Master::class,'addtestimonialModal']);
Route::post("admin/add-testimonial",[Master::class,'addtestimonial']);
Route::post("admin/edit-testimonial-modal",[Master::class,'edittestimonialModal']);
Route::post("admin/edit-testimonial",[Master::class,'edittestimonial']);
Route::post("admin/active-inactive-testimonial",[Master::class,'activeInactiveTestimonial']);
Route::post("admin/primary-nonprimary-testimonial",[Master::class,'primaryNonPrimaryTestimonial']);
Route::get("admin/games",[Master::class,'gamesList']);
Route::post("admin/add-game-modal",[Master::class,'addGameModal']);
Route::post("admin/add-game",[Master::class,'addGame']);
Route::post("admin/edit-game-modal",[Master::class,'editGameModal']);
Route::post("admin/edit-game",[Master::class,'editGame']);
Route::post("admin/active-inactive-game",[Master::class,'activeInactiveGame']);
Route::get("admin/game-teams",[Master::class,'gameTeamsList']);
Route::post("admin/add-game-team-modal",[Master::class,'addGameTeamModal']);
Route::post("admin/add-game-team",[Master::class,'addGameTeam']);
Route::post("admin/edit-game-team-modal",[Master::class,'editGameTeamModal']);
Route::post("admin/edit-game-team",[Master::class,'editGameTeam']);
Route::post("admin/active-inactive-game-team",[Master::class,'activeInactiveGameTeam']);
Route::get("admin/languages",[Information::class,'languages']);
Route::post("admin/add-language-modal",[Information::class,'addLanguageModal']);
Route::post("admin/add-language",[Information::class,'addLanguage']);
Route::post("admin/edit-language-modal",[Information::class,'editLanguageModal']);
Route::post("admin/edit-language",[Information::class,'editLanguage']);
Route::post("admin/active-inactive-language",[Information::class,'activeInactiveLanguage']);
Route::get("admin/reports",[Information::class,'reports']);
Route::post('admin/refresh-life-event-graph',[Home::class, 'refreshLifeEventGraph']);
Route::post('admin/refresh-feedback-graph',[Home::class, 'refreshFeedbackGraph']);
Route::get('admin/life-event-categories',[LifeEvent::class, 'CategoryList']);
Route::post("admin/edit-event-category-link-modal",[LifeEvent::class,'editEventCategoryLinkModal']);
Route::post("admin/edit-event-category-link",[LifeEvent::class,'editEventCategoryLink']);
Route::post("admin/premium-nonpremium-user",[users::class,'premiumNonPremiumUser']);